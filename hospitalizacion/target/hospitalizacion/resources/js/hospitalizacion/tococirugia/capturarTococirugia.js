/**
 * var log se implenta generar log4j mediante la libreria importada en el head para deshabilitar utilizar var log =
 * log4javascript.getNullLogger(); y comentar var log = log4javascript.getDefaultLogger();
 */
// var log = log4javascript.getDefaultLogger();
var log = log4javascript.getNullLogger();

$(document).ready(
        function() {

            btn_click(datosTococirugiaForm);

            var $_FORM = "#datosTococirugiaForm\\:";
            var $_FORM_SF = "datosTococirugiaForm:";

            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idFechaAtencion", "rcFechaAtencion", 10);
            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idNss", "rcNss", 10);
            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idSala", "rcSala", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumeroPaciente", "rcMapeoPaciente", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idTotalNacidos", "rcTotalNacidos", 1);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idGesta", "rcGesta", 2);
            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idHoraParto", "rcHoraParto", 5);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idMatricula", "rcMatricula", 10);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCama", "rcCama", 4);
            validateInputChangeOrPressEnterTab($_FORM + "idNumeroCama", "rcMapeoCama");
            validateSelectOneMenuPressEnterTab($_FORM + "idSelectAtencion", $_FORM_SF + "idSelectAtencion",
                    "rcAtencion");
            validateSelectOneMenuPressEnterTab($_FORM + "idSelectTipoParto", $_FORM_SF + "idSelectTipoParto",
                    "rcTipoParto");
            validateSelectOneMenuPressEnterTab($_FORM + "idSelectEpisiotomia", $_FORM_SF + "idSelectEpisiotomia",
            		"rcEpisiotomia");
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idMetodoPlanificacion", $_FORM_SF
                    + "idMetodoPlanificacion", "rcMetodoPlanificacion", 2);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumeroCantidad", "rcMapeoCantidad", 1);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNoCama", "rcCama", 4);
            // escDialog();
            cierradialogo();
            validateSelectOneMenuPressEnterTab($_FORM + "idTablaSelectNacidoModal", $_FORM_SF
                    + "idTablaSelectNacidoModal", "rcTablaSelectNacido");
            validateSelectOneMenuPressEnterTab($_FORM + "idTablaSelectSexoModal", $_FORM_SF + "idTablaSelectSexoModal",
                    "rcTablaSelectSexo");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idTablaPesoModal", "rcTablaSelectPeso", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idTablaTallaModal", "rcTablaSelectTalla", 2);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idTablaPerimetroCefalicoModal",
                    "rcTablaSelectPerimetroCefalico", 2);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idTablaSemanasGestacionModal",
                    "rcTablaSelectSemanasGestacion", 2);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idTablaApgar1Modal", "rcTablaSelectApgar1", 1);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idTablaApgar5Modal", "rcTablaSelectApgar5", 1);
            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idTablaHoraDefuncionModal",
                    "rcTablaSelectHoraDefuncion", 5);
            //setColorEncabezado();
            configuraPantalla(4,2,true);
        });

function verificarDialogo() {

    rcObtenerPaciente();
}

function actualizarFocoGuardar() {

    var foco = $('[id="datosTococirugiaForm:idHiddenFoco"]').val();
    if (foco == "datosTococirugiaForm:idGuardar") {
        $('[id="datosTococirugiaForm:idGuardar"]').focus();
    }
}

function actualizarHiddens() {

    rcActualizarHiddens();
}

function cierradialogo() {

    $(document).keyup(function(e) {

        if (e.keyCode == 27) { // esc code is 27
            cerrarModal();
            cerrarModalCama();
            cerrarModalPaciente();
            cerrarModalCantidad();
        }
    });

}

function cerrarModal() {

    var modal = $('[id="datosTococirugiaForm:idHiddenModal"]').val();
    if (modal == "CONFIRMACION") {
        PF('idDlgreciennacido').hide();
        limpiarValorTotalNacido();
        $('[id="datosTococirugiaForm:idTotalNacidos"]').focus();

    }
}

function cerrarModalCama() {

    var modal = $('[id="datosTococirugiaForm:idHiddenBanderaModalCama"]').val();
    if (modal == "BANDERAMODALCAMA") {
        PF('dlgCamaPacientes').hide();
        limpiarValorCama();
        $('[id="datosTococirugiaForm:idCama"]').focus();

    }
}

function cerrarModalPaciente() {

    var modal = $('[id="datosTococirugiaForm:idHiddenBanderaModalPaciente"]').val();
    if (modal == "BANDERAMODALPACIENTE") {
        PF('dlgPacientes').hide();
        limpiarValorPaciente();
        $('[id="datosTococirugiaForm:idNss"]').focus();

    }
}

function cerrarModalCantidad() {

    var modal = $('[id="datosTococirugiaForm:idHiddenBanderaModalCantidad"]').val();
    if (modal == "BANDERAMODALCANTIDAD") {
        PF('idDlgCantidad').hide();
        limpiarAutocompletePlanificacion();
        $('[id="datosTococirugiaForm:idMetodoPlanificacion_input"]').focus();

    }
}

function limpiarHoraParto(){
	 document.getElementById('datosTococirugiaForm:idHoraParto').value = "";
}


function limpiarValorTotalNacido() {

    document.getElementById('datosTococirugiaForm:idTotalNacidos').value = "";
}

function limpiarValorPaciente() {

    document.getElementById('datosTococirugiaForm:idNss').value = "";
}

function limpiarValorCama() {

    document.getElementById('datosTococirugiaForm:idCama').value = "";
}

function limpiarAutocompletePlanificacion() {

    document.getElementById('datosTococirugiaForm:idMetodoPlanificacion_input').value = "";
}

function ocultarEpisiotomia(){
	var rowLabel = document.getElementById('datosTococirugiaForm:idLabelEpisiotomia');
	rowLabel.style.display = 'none';
	
	var rowDiv = document.getElementById('datosTococirugiaForm:idSelectEpisiotomia');
	rowDiv.style.display = 'none';
}

function mostrarEpisiotomia(){
	var rowLabel = document.getElementById('datosTococirugiaForm:idLabelEpisiotomia');
	rowLabel.style.display = '';
	
	var rowDiv = document.getElementById('datosTococirugiaForm:idSelectEpisiotomia');
	rowDiv.style.display = '';
}

var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if ((e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223)) {
                        return false;
                    }
                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }
                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                    if (e.keyCode == 226) {
                        return false;
                    }
                    if (shiftPresionado && e.keyCode == 226) {
                        return false;
                    }
                });

function setColorEncabezado(){
    setClassEncabezadoPieDePagina('encabezadoCaptura','pieDePaginaCaptura');
}