// var log = log4javascript.getDefaultLogger();
var log = log4javascript.getNullLogger();

$(document).ready(
        function() {

            btn_click("#datosMovimientosIntraForm")
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#datosMovimientosIntraForm\\:idFechaAtencion",
                    "rcFechaAtencion", 10);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosMovimientosIntraForm\\:idNss", "rcNss", 10);
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#datosMovimientosIntraForm\\:idHoraAnterior", "rcHoraAnterior", 5);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosMovimientosIntraForm\\:idNumeroPaciente",
                    "rcMapeoPaciente", 4);
            validateAutocompletePressEnterTabOrMaxlength("#datosMovimientosIntraForm\\:idEspecialidadActual",
                    "datosMovimientosIntraForm:idEspecialidadActual", "rcValidarEspecialidad", 4);
            validateInputChangeOrPressEnterTab("#datosMovimientosIntraForm\\:idCamaModificada", "rcCamaModificada")
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#datosMovimientosIntraForm\\:idHoraActual", "rcHoraActual", 5);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosMovimientosIntraForm\\:idMatricula", "rcMatricula", 10);
            escDialog();
            //setColorEncabezado();
            configuraPantalla(4,2,true);
        });

function verificarDialogo() {

    if ($("#datosMovimientosIntraForm\\:banderaNssHidden").val().toLowerCase() === 'true') {
        rcObtenerPaciente();
    }
}

$(document).on("keydown", "input:enabled", function(e) {

    if (e.ctrlKey && e.altKey) {
        if (e.which) {
            return false;
        }
    }

    if ((e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223)) {
        return false;

    }
});

function activarGuaradar() {

    var foco = $('[id="datosMovimientosIntraForm:idHiddenFoco"]').val();
    if (foco == "idGuardar") {
        $('[id="datosMovimientosIntraForm:idGuardar"]').focus();
    }
}

var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }

                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                });

function setColorEncabezado() {

    setClassEncabezadoPieDePagina('encabezadoCaptura', 'pieDePaginaCaptura');
}
