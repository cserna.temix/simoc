/**
 * 
 */
/**
 * var log se implenta generar log4j mediante la libreria importada en el head para deshabilitar utilizar var log =
 * log4javascript.getNullLogger(); y comentar var log = log4javascript.getDefaultLogger();
 */
// var log = log4javascript.getDefaultLogger();
var log = log4javascript.getNullLogger();

$(document).ready(
        function() {

            escDialog();
            var $_FORM = "#informacionPacienteForm\\:";
            var $_FORM_SF = "informacionPacienteForm:";

            btn_click("#informacionPacienteForm")

            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idFechaEgreso", "rcFechaEgreso", 10);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNss", "rcNss", 10);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNoCama", "rcCama", 5);
            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idHoraEgreso", "rcHoraEgreso", 5);
            validateSelectOneMenuPressEnterTab($_FORM + "idTipoEgreso", $_FORM_SF + "idTipoEgreso", "rcTipoEgreso");
            validateSelectOneMenuPressEnterTab($_FORM + "idMotivoEgreso", $_FORM_SF + "idMotivoEgreso", "rcMotivoEgreso");
            validateSelectOneMenuPressEnterTab($_FORM + "idRiesgoTrabajo", $_FORM_SF + "idRiesgoTrabajo", "rcRiesgoTrabajo");
            validateSelectOneMenuPressEnterTab($_FORM + "idEnvioa", $_FORM_SF + "idEnvioa", "rcEnvioA");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagnosticoInicial", "rcDiagnosticoInicial", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagnosticoEgreso", "rcDiagnosticoEgreso", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagnosticoEgresoPri", "rcDiagnosticoEgresoPri",
                    4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagnosticoEgresoseg", "rcDiagnosticoEgresoseg",
                    4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagnosticoEgresoTer", "rcDiagnosticoEgresoTer",
                    4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagnosticoEgresoCuarto", "rcDiagnosticoEgresoCuarto",
                    4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagnosticoEgresoQuinto", "rcDiagnosticoEgresoQuinta",
                    4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idProcedimientoCama", "rcNumProcedimientoCama",
                    1);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumeroRecetas", "rcNumRecetas",2);
            validateInputChangeOrMaxlength($_FORM + "idProcedimientoModalCama", "rcClaveProcedimiento",4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCausaDirectaDefuncion",
                    "rcCausaDirectaDefuncion", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCausaDefuncion", "rcCausaDefuncion", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCausaSecDefuncion", "rcCausaSecDefuncion", 4);
            validateSelectOneMenuPressEnterTab($_FORM + "idEgresoDefuncion", $_FORM_SF + "idEgresoDefuncion", "rcEgresoDefuncion");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idPrimeraComplicacionIntrahospitalaria",
                    "rcPrimeraComplicacionIntrahospitalaria", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idSegundaComplicacionIntrahospitalaria",
                    "rcSegundaComplicacionIntrahospitalaria", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idTerceraComplicacionIntrahospitalaria",
                    "rcTerceraComplicacionIntrahospitalaria", 4);
            validateSelectOneMenuPressEnterTab($_FORM + "idMotivoAlta", $_FORM_SF + "idMotivoAlta", "rcMotivoAlta");
            validateSelectOneMenuPressEnterTab($_FORM + "idAlimentacionRecNacido", $_FORM_SF
                    + "idAlimentacionRecNacido", "rcAlimentacionRecNacido");
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacUno", $_FORM_SF
                    + "idProcedimientoRecNacUno", "rcProcedimientoRecNacUno", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacDos", $_FORM_SF
                    + "idProcedimientoRecNacDos", "rcProcedimientoRecNacDos", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacTres", $_FORM_SF
                    + "idProcedimientoRecNacTres", "rcProcedimientoRecNacTres", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacCuatro", $_FORM_SF
                    + "idProcedimientoRecNacCuatro", "rcProcedimientoRecNacCuatro", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacCinco", $_FORM_SF
                    + "idProcedimientoRecNacCinco", "rcProcedimientoRecNacCinco", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacSeis", $_FORM_SF
                    + "idProcedimientoRecNacSeis", "rcProcedimientoRecNacSeis", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacSiete", $_FORM_SF
                    + "idProcedimientoRecNacSiete", "rcProcedimientoRecNacSiete", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacOcho", $_FORM_SF
                    + "idProcedimientoRecNacOcho", "rcProcedimientoRecNacOcho", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacNueve", $_FORM_SF
                    + "idProcedimientoRecNacNueve", "rcProcedimientoRecNacNueve", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacDiez", $_FORM_SF
                    + "idProcedimientoRecNacDiez", "rcProcedimientoRecNacDiez", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacOnce", $_FORM_SF
                    + "idProcedimientoRecNacOnce", "rcProcedimientoRecNacOnce", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idProcedimientoRecNacDoce", $_FORM_SF
                    + "idProcedimientoRecNacDoce", "rcProcedimientoRecNacDoce", 2);
            validateSelectOneMenuPressEnterTab($_FORM + "idTamiz", $_FORM_SF + "idTamiz", "rcTamiz");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idPesoEgreso", "rcPesoEgreso", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCamaHosp", "rcCamaHosp", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idMatricula", "rcMatricula", 10);
            validateInputChangeOrPressEnterTab($_FORM + "idJustificacionEgreso", "rcJustificacionEgreso");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumeroPaciente", "rcMapeoPaciente", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumeroCama", "rcMapeoCama", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumeroCantidad", "rcMapeoCantidad", 4);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idMetodoPlanificacion", $_FORM_SF
                    + "idMetodoPlanificacion", "rcMetodoPlanificacion", 2);
            //setColorEncabezado();
            configuraPantalla(4,2,true);

        });

function verificarDialogo() {

    rcObtenerPaciente();
}

function escDialog() {

    $(document).keyup(function(e) {

        if (e.keyCode == 27) { // esc code is 27
            PF('dlgPacientes').hide();
            PF('dlgProcedimientoCama').hide();
            cerrarModalCama();
          
        }
    });

}

function cerrarModalCama() {

    var modal = $('[id="informacionPacienteForm:idHiddenBanderaModalCama"]').val();
    if (modal == "BANDERAMODALCAMA") {
        PF('dlgCamaPacientes').hide();
        limpiarValorCama();

    }
}

function cerrarTimerModalProcedimientos() {
    setTimeout("reiniciaModalProcedimientos()", 1000);
       
}

function mostrarTimerModalProcedimientos() {
    setTimeout("mostrarModalProcedimientos()", 1000);
   
}

function reiniciaModalProcedimientos(){
	PF('dlgProcedimientoCama').hide();
	rcLimpiaCamposModalCama();
	
}

function mostrarModalProcedimientos(){
	PF('dlgProcedimientoCama').show();
	rcLimpiaCamposModalCama();
}


function limpiarValorCama() {
    rcLimpiaCamposModal();
}

function setColorEncabezado(){
    setClassEncabezadoPieDePagina('encabezadoCaptura','pieDePaginaCaptura');
}

function deshabilitaEnter() {

    btn_click("#informacionPacienteForm");
}

function actualizarFocoGuardar() {

    var foco = $('[id="informacionPacienteForm:idHiddenFoco"]').val();
    if (foco == "informacionPacienteForm:idGuardar") {
        $('[id="informacionPacienteForm:idGuardar"]').focus();
    } else {
        $('[id="informacionPacienteForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

function desactivarBotonGuardar(){
		 $('[id="informacionPacienteForm:idGuardar"]').addClass('ui-state-disabled').attr('disabled', 'disabled');//desabilita boton
}


var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }

                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }

                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                });

$(document).on("keydown", "textarea:enabled", function(e) {

    if (e.ctrlKey && e.altKey && e.which) {
           return false;
    }
});
