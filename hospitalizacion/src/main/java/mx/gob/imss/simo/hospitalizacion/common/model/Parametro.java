package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class Parametro {

    private String claveParametro;
    private String referenciaParametro;
    private String descripcionParametro;

}
