package mx.gob.imss.simo.hospitalizacion.seguridad.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.hospitalizacion.common.model.PersonalOperativo;
import mx.gob.imss.simo.hospitalizacion.common.model.Rol;
import mx.gob.imss.simo.hospitalizacion.seguridad.repository.PersonalOperativoRepository;
import mx.gob.imss.simo.hospitalizacion.seguridad.service.PersonalOperativoService;

@Service
public class PersonalOperativoServiceImpl implements PersonalOperativoService {

    @Autowired
    PersonalOperativoRepository personalOperativoRepository;

    @Override
    public PersonalOperativo obtenerPersonalOperativoPorUsuarioAD(String usuario) {

        return personalOperativoRepository.obtenerPersonalOperativoPorUsuarioAD(usuario);
    }

    @Override
    public List<Rol> obtenerRolesPorUsuarioAD(String usuario) {

        return personalOperativoRepository.obtenerRolesPorUsuarioAD(usuario);
    }

    @Override
    public void actualizarIntento(String usuario, Integer intento) {

        personalOperativoRepository.actualizarIntento(usuario, intento);

    }

    @Override
    public void actualizarBloqueo(String usuario, Integer intento) {

        personalOperativoRepository.actualizarBloqueo(usuario, intento);

    }

    public void setPersonalOperativoRepository(PersonalOperativoRepository personalOperativoRepository) {

        this.personalOperativoRepository = personalOperativoRepository;
    }

}
