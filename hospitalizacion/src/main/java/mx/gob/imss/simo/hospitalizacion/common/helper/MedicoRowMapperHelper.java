package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Medico;

public class MedicoRowMapperHelper extends BaseHelper implements RowMapper<Medico> {

    @Override
    public Medico mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        Medico medico = new Medico();

        medico.setCveMatricula(resultSet.getString(SQLColumnasConstants.CVE_MATRICULA));
        medico.setRefNombre(resultSet.getString(SQLColumnasConstants.REF_NOMBRE));
        medico.setRefApellidoPaterno(resultSet.getString(SQLColumnasConstants.REF_APELLIDO_PATERNO));
        medico.setRefApellidoMaterno(resultSet.getString(SQLColumnasConstants.REF_APELLIDO_MATERNO));
        medico.setRefTipoContratacion(resultSet.getString(SQLColumnasConstants.REF_TIPO_CONTRATACION));
        medico.setCveDelegacionImss(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
        medico.setFecConsultaSiap(resultSet.getDate(SQLColumnasConstants.FEC_CONSULTA_SIAP));
        medico.setIndActivo(resultSet.getBoolean(SQLColumnasConstants.IND_ACTIVO));
        medico.setIndSiap(resultSet.getBoolean(SQLColumnasConstants.IND_SIAP));

        return medico;
    }

}
