package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.ReporteGenerado;

public class ReporteGeneradoRowMapperHelper extends BaseHelper implements RowMapper<ReporteGenerado> {

    @Override
    public ReporteGenerado mapRow(ResultSet resultSet, int rowId) throws SQLException {

        ReporteGenerado reporte = new ReporteGenerado();
        reporte.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        reporte.setTipoReporte(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_REPORTE));
        reporte.setFecGeneracion(resultSet.getDate(SQLColumnasConstants.FEC_GENERACION));
        return reporte;
    }

}
