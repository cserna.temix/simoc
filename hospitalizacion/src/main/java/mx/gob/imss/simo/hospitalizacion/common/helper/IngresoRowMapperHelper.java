package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;

public class IngresoRowMapperHelper extends BaseHelper implements RowMapper<Ingreso> {

    @Override
    public Ingreso mapRow(ResultSet resultSet, int rowId) throws SQLException {

        Ingreso ingreso = new Ingreso();
        ingreso.setCveIngreso(resultSet.getLong(SQLColumnasConstants.CVE_INGRESO));
        ingreso.setCveIngresoPadre(resultSet.getLong(SQLColumnasConstants.CVE_INGRESO_PADRE));
        ingreso.setFecCreacion(resultSet.getDate(SQLColumnasConstants.FEC_CREACION));
        ingreso.setCvePaciente(resultSet.getLong(SQLColumnasConstants.CVE_PACIENTE));
        ingreso.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        ingreso.setCveEspecialidadIngreso(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD_INGRESO));
        ingreso.setCveTipoFormato(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_FORMATO));
        ingreso.setCveCapturista(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA));
        ingreso.setFecIngreso(resultSet.getDate(SQLColumnasConstants.FEC_INGRESO));
        ingreso.setCveCama(resultSet.getString(SQLColumnasConstants.CVE_CAMA));
        ingreso.setCveEspecialidadCama(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD_CAMA));
        ingreso.setRefCamaRn(resultSet.getString(SQLColumnasConstants.REF_CAMA_RN));
        ingreso.setCveTipoIngreso(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_INGRESO));
        ingreso.setCveTipoPrograma(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_PROGRAMA));
        ingreso.setCveMedico(resultSet.getString(SQLColumnasConstants.CVE_MEDICO));
        ingreso.setIndExtemporaneo(resultSet.getBoolean(SQLColumnasConstants.IND_EXTEMPORANEO));
        ingreso.setIndEgreso(resultSet.getBoolean(SQLColumnasConstants.IND_EGRESO));
        ingreso.setIndTococirugia(resultSet.getBoolean(SQLColumnasConstants.IND_TOCOCIRUGIA));
        ingreso.setIndIntervencionQx(resultSet.getBoolean(SQLColumnasConstants.IND_INTERVENCION_QX));
        ingreso.setIndMovIntrahospitalario(resultSet.getBoolean(SQLColumnasConstants.IND_MOV_INTRAHOSPITALARIO));
        ingreso.setCveEspecialidadOrigen(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD_ORIGEN));
        return ingreso;
    }

}
