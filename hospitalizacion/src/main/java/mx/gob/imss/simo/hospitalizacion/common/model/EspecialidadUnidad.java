package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class EspecialidadUnidad {

    private String cveEspecialidad;
    private String cvePresupuestal;
    private Date fecAlta;
    private Date fecBaja;
    private boolean indCirugia;
    private boolean indConsultaExterna;
    private boolean indEstatus;
    private boolean indHospital;
    private boolean indInterconsulta;
    private boolean indPediatria;
    private Integer numTipoServicio;

}
