package mx.gob.imss.simo.hospitalizacion.interconsultas.repository.impl;

import java.sql.ResultSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoDxProcInterconsultaEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.AtencionInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.EncabezadoInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.hospitalizacion.interconsultas.repository.CapturaInterconsultaRepository;

@Repository
public class CapturaInterconsultaRepositoryImpl extends HospitalizacionCommonRepositoryJDBCImpl implements CapturaInterconsultaRepository{

	@Override
	public List<Especialidad> obtenerEspecialidadesPorCvePres(String clavePresupuestal)
			throws HospitalizacionException {		
		try {
			return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_ESPECIALIDAD + " AND SE.IND_INTERCONSULTA = 1 " + SQLCatalogosConstants.ORDER_ESPECIALIDAD,
	                new Object[] { clavePresupuestal }, 
	                (ResultSet resultSet, int rowId) -> {
							Especialidad especialidad = new Especialidad();
					        especialidad.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
					        especialidad.setDesEspecialidad(resultSet.getString(SQLColumnasConstants.DES_ESPECIALIDAD));
							return especialidad;
						});
		} catch (Exception e) {
			logger.error("Erro al obtener el catalogo de especialidades para interconsulta", e);
			throw new HospitalizacionException( e.getMessage() );
		}
	}
	
	@Override
	public List<Especialidad> validarEspecialidadInterconsulta(String cveEspecialidad)
			throws HospitalizacionException {		
		try {
			return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_INTERCONSULTA_ESPECIALIDAD,
	                new Object[] { cveEspecialidad }, 
	                (ResultSet resultSet, int rowId) -> {
							Especialidad especialidad = new Especialidad();
					        especialidad.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
					        especialidad.setDesEspecialidad(resultSet.getString(SQLColumnasConstants.DES_ESPECIALIDAD));
							return especialidad;
						});
		} catch (Exception e) {
			logger.error("Erro al obtener el catalogo de especialidades para interconsulta", e);
			throw new HospitalizacionException( e.getMessage() );
		}
	}

	@Override
	public void guardarEncabezado(EncabezadoInterconsulta encabezado) throws HospitalizacionException {
		try {
			jdbcTemplate.update( SQLConstants.INSERT_INTERCONSULTA,
								 new Object[] { encabezado.getClaveEspecialidad(), encabezado.getClaveMedico(), encabezado.getClaveTurno(), encabezado.getFechaInterconsulta(),
										 		encabezado.getFechaCreacion(), encabezado.getClavePresupuestal(), encabezado.getClaveTipoFormato(),
										 		encabezado.getClaveCapturista()} );
		} catch (Exception e) {
			logger.error("Erro al registrar el encabezado de la interconsulta", e);
			throw new HospitalizacionException( e.getMessage() );
		}
	}

	@Override
	public EncabezadoInterconsulta buscarEncabezadoInterconsulta(String fechaInterconsulta, String claveEspecialidad, String claveMedico, Integer claveTurno) throws HospitalizacionException {
		try {
			return jdbcTemplate.queryForObject(SQLConstants.OBTENER_ENCABEZADO_INTERCONSULTA,
	                new Object[] { fechaInterconsulta, claveEspecialidad, claveMedico, claveTurno }, 
	                (ResultSet resultSet, int arg1) -> {
	                	EncabezadoInterconsulta encabezado = new EncabezadoInterconsulta();
	                	encabezado.setClaveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
	                	encabezado.setClaveMedico(resultSet.getString(SQLColumnasConstants.CVE_MEDICO));
	                	encabezado.setClaveTurno(resultSet.getInt("CVE_TURNO"));
	                	encabezado.setClaveInterconsulta(resultSet.getInt("CVE_INTERCONSULTA"));
	                	return encabezado;
	                });
		} catch (Exception e) {			
			if( e instanceof EmptyResultDataAccessException ){
				return null;
			}
			throw new HospitalizacionException( e.getMessage() );
		} 	
	}

	@Override
	public Long obtenerConsecutivoInterconsultas(Integer cve_interconsulta) throws HospitalizacionException {
		try {
			return jdbcTemplate.queryForObject(SQLConstants.OBTENER_CONSECUTIVO_INTERCONSULTA_ATENCION,
	                new Object[] { cve_interconsulta }, Long.class);
		} catch (Exception e) {
			if( e instanceof EmptyResultDataAccessException ){
				return null;
			}
			throw new HospitalizacionException( e.getMessage() );
		}
	}

	public void insertarDetalleInterconsulta(EncabezadoInterconsulta encabezado, AtencionInterconsulta atencionInteronsulta) throws HospitalizacionException{
		
		try {
			jdbcTemplate.update( SQLConstants.INSERT_DETALLE_INTERCONSULTA,
								 new Object[] { atencionInteronsulta.getCveInterconsulta(), 
										 		atencionInteronsulta.getNumInterconsulta(), 
										 		encabezado.getClavePresupuestal(), 
										 		atencionInteronsulta.getEspecialidadSolicita(),
										 		encabezado.getFechaCreacion(), 
										 		atencionInteronsulta.getCvePaciente(), 
										 		atencionInteronsulta.getTimDuracion(),
										 		atencionInteronsulta.getIndPVCie10Principal(),
										 		atencionInteronsulta.getIndAlta(),
										 		atencionInteronsulta.getClaveCapturista() });
			
			jdbcTemplate.update( SQLConstants.INSERT_CIE10_DETALLE_INTERCONSULTA,
					 new Object[] { atencionInteronsulta.getCveInterconsulta(), 
							 		atencionInteronsulta.getNumInterconsulta(), 
							 		TipoDxProcInterconsultaEnum.DX_PRINCIPAL.getClave(), 
							 		atencionInteronsulta.getCveCie10Principal(),
							 		atencionInteronsulta.getIndPVCie10Principal()});
			
			if(atencionInteronsulta.getCveCie10Adicional()!=null && !StringUtils.isEmpty(atencionInteronsulta.getCveCie10Adicional())) {
				jdbcTemplate.update( SQLConstants.INSERT_CIE10_DETALLE_INTERCONSULTA,
						 new Object[] { atencionInteronsulta.getCveInterconsulta(), 
								 		atencionInteronsulta.getNumInterconsulta(), 
								 		TipoDxProcInterconsultaEnum.DX_ADICIONAL.getClave(), 
								 		atencionInteronsulta.getCveCie10Adicional(),
								 		atencionInteronsulta.getIndPVCie10Adicional() });			
			}
			
			if(atencionInteronsulta.getCveCie10Complementario()!=null && !StringUtils.isEmpty(atencionInteronsulta.getCveCie10Complementario())) {
				jdbcTemplate.update( SQLConstants.INSERT_CIE10_DETALLE_INTERCONSULTA,
						 new Object[] { atencionInteronsulta.getCveInterconsulta(), 
								 		atencionInteronsulta.getNumInterconsulta(), 
								 		TipoDxProcInterconsultaEnum.DX_COMPLEMENTARIO.getClave(), 
								 		atencionInteronsulta.getCveCie10Complementario(),
								 		atencionInteronsulta.getIndPVCie10Complementario() });			
			}
			
			if(atencionInteronsulta.getCveCie9Principal()!=null && !StringUtils.isEmpty(atencionInteronsulta.getCveCie9Principal())) {
				jdbcTemplate.update( SQLConstants.INSERT_CIE9_DETALLE_INTERCONSULTA,
						 new Object[] { atencionInteronsulta.getCveInterconsulta(), 
								 		atencionInteronsulta.getNumInterconsulta(), 
								 		TipoDxProcInterconsultaEnum.PROCEDIMIENTO_PRINCIPAL.getClave(), 
								 		atencionInteronsulta.getCveCie9Principal() });			
			}
			
			if(atencionInteronsulta.getCveCie9Adicional()!=null && !StringUtils.isEmpty(atencionInteronsulta.getCveCie9Adicional())) {
				jdbcTemplate.update( SQLConstants.INSERT_CIE9_DETALLE_INTERCONSULTA,
						 new Object[] { atencionInteronsulta.getCveInterconsulta(), 
								 		atencionInteronsulta.getNumInterconsulta(), 
								 		TipoDxProcInterconsultaEnum.PROCEDIMIENTO_ADICIONAL.getClave(),
								 		atencionInteronsulta.getCveCie9Adicional() });			
			}
			
			if(atencionInteronsulta.getCveCie9Complementario()!=null && !StringUtils.isEmpty(atencionInteronsulta.getCveCie9Complementario())) {
				jdbcTemplate.update( SQLConstants.INSERT_CIE9_DETALLE_INTERCONSULTA,
						 new Object[] { atencionInteronsulta.getCveInterconsulta(), 
								 		atencionInteronsulta.getNumInterconsulta(), 
								 		TipoDxProcInterconsultaEnum.PROCEDIMIENTO_COMPLEMENTARIO.getClave(),
								 		atencionInteronsulta.getCveCie9Complementario() });		
				logger.error("YEah");
			}			
			
		} catch (Exception e) {
			logger.error("Erro al registrar el encabezado de la interconsulta", e);
			throw new HospitalizacionException( e.getMessage() );
		}
	}

}
