package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class TipoEgreso {

    private String desTipoEgreso;
    private Integer ordenUrgencias;
    private Integer ordenHospitalizacion;
    private Integer ordenUCI;
    private Integer ordenCuneros;

}
