package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.PacientesHospitalizados;

public class PacientesHospitalizadosRowMapperHelper extends BaseHelper implements RowMapper<PacientesHospitalizados> {

    @Override
    public PacientesHospitalizados mapRow(ResultSet resultSet, int rowId) throws SQLException {

        PacientesHospitalizados pacientesHospitalizados = new PacientesHospitalizados();

        pacientesHospitalizados.setAfiliacion(resultSet.getString(SQLColumnasConstants.AFILIACION));
        pacientesHospitalizados.setAgregadoMedico(resultSet.getString(SQLColumnasConstants.AGREGADO_MEDICO));
        pacientesHospitalizados.setNombre(resultSet.getString(SQLColumnasConstants.NOMBRE));
        pacientesHospitalizados.setFecIngreso(resultSet.getDate(SQLColumnasConstants.FEC_INGRESO));
        pacientesHospitalizados.setCama(resultSet.getString(SQLColumnasConstants.CAMA));
        pacientesHospitalizados.setPrograma(resultSet.getString(SQLColumnasConstants.PROGRAMA));
        pacientesHospitalizados.setFecIntervencionIqx(resultSet.getDate(SQLColumnasConstants.FEC_INTERVENCION_IQX));
        pacientesHospitalizados.setProcedimiento(resultSet.getString(SQLColumnasConstants.PROCEDIMIENTO));
        pacientesHospitalizados.setParto(resultSet.getString(SQLColumnasConstants.PARTO));
        pacientesHospitalizados.setRecienNacido(resultSet.getString(SQLColumnasConstants.RECIEN_NACIDO));
        pacientesHospitalizados.setHora(resultSet.getString(SQLColumnasConstants.HORA));
        pacientesHospitalizados.setSexo(resultSet.getString(SQLColumnasConstants.SEXO));
        pacientesHospitalizados.setUbicacion(resultSet.getString(SQLColumnasConstants.UBICACION));
        pacientesHospitalizados.setEstado(resultSet.getString(SQLColumnasConstants.ESTADO));
        pacientesHospitalizados.setAlta(resultSet.getString(SQLColumnasConstants.ALTA));
        pacientesHospitalizados.setObservacionesRN(resultSet.getString(SQLColumnasConstants.OBSERVACIONES_RN));
        pacientesHospitalizados.setObservacionesIngreso(resultSet.getString(SQLColumnasConstants.OBSERVACIONES_INGRESO));
        pacientesHospitalizados.setDesEspecialidad(resultSet.getString(SQLColumnasConstants.DES_ESPECIALIDAD));
        pacientesHospitalizados.setDesDivision(resultSet.getString(SQLColumnasConstants.DES_DIVISION));
        return pacientesHospitalizados;

    }

}
