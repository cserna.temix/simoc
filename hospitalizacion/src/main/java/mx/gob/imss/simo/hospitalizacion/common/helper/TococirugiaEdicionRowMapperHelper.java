package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.TococirugiaEdicion;

public class TococirugiaEdicionRowMapperHelper extends BaseHelper implements RowMapper<TococirugiaEdicion> {

    @Override
    public TococirugiaEdicion mapRow(ResultSet resultSet, int IdRow) throws SQLException {

        TococirugiaEdicion tococirugiaEdi = new TococirugiaEdicion();

        tococirugiaEdi.setClaveTipoNacido(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_NACIDO));
        tococirugiaEdi.setClaveTipoAtencion(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_ATENCION));
        tococirugiaEdi.setTipoAtencion(resultSet.getString(SQLColumnasConstants.DES_TIPO_ATENCION));
        tococirugiaEdi.setFechaAtencion(resultSet.getDate(SQLColumnasConstants.FEC_TOCOCIRUGIA));
        tococirugiaEdi.setTotalRecienNacidos(resultSet.getInt(SQLColumnasConstants.NUM_TOTAL_RN));
        tococirugiaEdi.setClaveTipoParto(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_PARTO));
        tococirugiaEdi.setTipoParto(resultSet.getString(SQLColumnasConstants.DES_TIPO_PARTO));
        tococirugiaEdi.setConsecutivo(resultSet.getInt(SQLColumnasConstants.NUM_RECIEN_NACIDO));
        tococirugiaEdi.setClaveSexo(resultSet.getInt(SQLColumnasConstants.CVE_SEXO));
        tococirugiaEdi.setSexo(resultSet.getString(SQLColumnasConstants.DES_SEXO));
        tococirugiaEdi.setPeso(resultSet.getInt(SQLColumnasConstants.NUM_PESO));
        tococirugiaEdi.setTalla(resultSet.getInt(SQLColumnasConstants.NUM_TALLA));
        tococirugiaEdi.setAlta(resultSet.getInt(SQLColumnasConstants.IND_EGRESO));
        tococirugiaEdi.setCvePaciente(resultSet.getLong(SQLColumnasConstants.CVE_PACIENTE));

        return tococirugiaEdi;
    }

}
