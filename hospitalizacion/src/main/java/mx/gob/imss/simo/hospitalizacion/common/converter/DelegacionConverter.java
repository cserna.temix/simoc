package mx.gob.imss.simo.hospitalizacion.common.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import mx.gob.imss.simo.hospitalizacion.common.model.DelegacionIMSS;

@FacesConverter("delegacionConverter")
public class DelegacionConverter implements Converter {

    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {

        if (value != null && value.trim().length() > 0) {
            try {
                DelegacionIMSS del = new DelegacionIMSS();
                del.setCveDelegacionImss(value);
                return del;
            } catch (Exception e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en la conversión",
                        "Delegación no válida"));
            }
        } else {
            return null;
        }
    }

    public String getAsString(FacesContext fc, UIComponent uic, Object object) {

        if (object != null) {
            return String.valueOf(((DelegacionIMSS) object).getCveDelegacionImss());
        } else {
            return null;
        }
    }
}
