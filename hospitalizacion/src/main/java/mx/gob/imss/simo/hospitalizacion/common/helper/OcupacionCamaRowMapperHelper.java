package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCama;

public class OcupacionCamaRowMapperHelper extends BaseHelper implements RowMapper<OcupacionCama> {

    @Override
    public OcupacionCama mapRow(ResultSet resultSet, int rowId) throws SQLException {

        OcupacionCama ocupacionCama = new OcupacionCama();
        ocupacionCama.setCveCama(resultSet.getString(SQLColumnasConstants.CVE_CAMA));
        ocupacionCama.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        ocupacionCama.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
        ocupacionCama.setNumPacientes(resultSet.getInt(SQLColumnasConstants.NUM_PACIENTES));
        return ocupacionCama;
    }

}
