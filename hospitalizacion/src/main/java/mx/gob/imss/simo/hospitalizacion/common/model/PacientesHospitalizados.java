package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class PacientesHospitalizados {

    private String afiliacion;
    private String agregadoMedico;
    private String nombre;
    private Date fecIngreso;
    private String cama;
    private String programa;
    private Date fecIntervencionIqx;
    private String procedimiento;
    private String parto;
    private String recienNacido;
    private String hora;
    private String sexo;
    private String ubicacion;
    private String estado;
    private String alta;
    private String observacionesRN;
    private String observacionesIngreso;
    private String desEspecialidad;
    private String desDivision;

}
