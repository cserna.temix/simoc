package mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLUpdateConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.CamaUnidadRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.CatalogoDivisionRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.EspecialidadRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.IngresoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.repository.CapturarMovimientosIntrahospitalariosRepository;

@Repository("capturarMovimientosIntrahospitalariosRepository")
public class CapturarMovimientosIntrahospitalariosRepositoryJDBCImpl extends HospitalizacionCommonRepositoryJDBCImpl
        implements CapturarMovimientosIntrahospitalariosRepository {

    public DatosCatalogo buscarCamaPorCvePresupuestalYCveCama(String cveCama, String cvePresupuestal) {

        return null;
    }

    public void guardar(MovimientoIntraHospitalario movimiento) {

        jdbcTemplate.update(SQLUpdateConstants.MOVIMIENTO_INTRAHOSPITALARIO_AGREGAR,
                new Object[] { movimiento.getClaveIngreso(), movimiento.getFechaCreacion(),
                        movimiento.getFechaMovimiento(), movimiento.getCamaOrigen(),
                        movimiento.getEspeciaidadCamaOrigen(), movimiento.getClavePresupuestal(), // del ingreso
                        movimiento.getEspecialidadOrigen(), // del T
                        movimiento.getCapturista(), movimiento.getTipoFormato(), // tipoFormato 8 movi
                        movimiento.getCamaDestino(), movimiento.getEspecialidadCamaDestino(),
                        movimiento.getEspecialidadDestino(), movimiento.getCveMedico(),
                        movimiento.getFechaMovIngreso()});
    }

    @Override
    public DatosCatalogo buscarDivisionPorCveDivision(int cveDivision) {

        List<DatosCatalogo> division = jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_DIVISION,
                new Object[] { cveDivision }, new CatalogoDivisionRowMapperHelper());
        if (null != division && !division.isEmpty()) {
            return division.get(0);
        }
        return null;

    }

    public void actualizarIngresoPorMovimientoIntraHospitalario(long cveIngreso,
            MovimientoIntraHospitalario movimiento) {

        jdbcTemplate.update(SQLUpdateConstants.ACTUALIZAR_INGRESO_POR_MOVIMIENTO,
                new Object[] { movimiento.getEspecialidadDestino(), movimiento.getEspecialidadCamaDestino(),
                        movimiento.getCamaDestino(), cveIngreso });
    }
    
    public void actualizarIngresoPorMovimientoIntraHospitalarioRN(long cveIngreso,
            MovimientoIntraHospitalario movimiento, Integer cveTipoFormato) {

        jdbcTemplate.update(SQLUpdateConstants.ACTUALIZAR_INGRESO_POR_MOVIMIENTO_RN,
                new Object[] { movimiento.getEspecialidadDestino(), movimiento.getEspecialidadCamaDestino(),
                        movimiento.getCamaDestino(), cveTipoFormato, cveIngreso });
    }

    @Override
    public void actualizarIndMovIntrahospitalario(long claveIngreso) {

        jdbcTemplate.update(SQLUpdateConstants.ACTUALIZAR_IND_MOV_INTRAHOSPITALARIO, new Object[] { claveIngreso });
    }
    
    @Override
    public List<Ingreso> buscarIngresoVigenteUCIN(long clavePaciente){
    			
    	return jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_VIGENTE_CUNERO_UCIN,
    	       new Object[] { clavePaciente, BaseConstants.ESPECIALIDAD_UCI_NEONATOS, 0 },
    	       new IngresoRowMapperHelper());
    }
    
    @Override
    public List<Ingreso> buscarIngresoAlojamiento(String nss, String cvePresupuestal){
    	return jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_ALOJAMIENTO, 
    		   new Object[] { nss, cvePresupuestal },
    		   new IngresoRowMapperHelper());
    }
    
    @Override
    public void actualizaCamaAlojamiento(String camaNueva, Ingreso ingreso){
    	
    	jdbcTemplate.update(SQLUpdateConstants.ACTUALIZAR_CAMA_ALOJAMIENTO, 
    			new Object[] { camaNueva, ingreso.getCveIngreso()});
 	
    }
    
    @Override
    public Boolean esRecienNacidoExterno(long clavePaciente,String cvePresupuestal){
    	Boolean resultado = Boolean.TRUE;
    	int tococicirugia = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_TOCOCIRUGIA_RECIEN_NACIDOS,
    			new Object[] { clavePaciente, cvePresupuestal }, Integer.class);
    			
        if (tococicirugia > 0) {
    		   resultado = Boolean.FALSE;
        }
       return resultado;
    }
    
    @Override
    public List<Ingreso> buscarIngresoMadrePorToco(long clavePaciente,String cvePresupuestal){
    	
    	return jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_MADRE_POR_TOCO,
    			new Object[] { clavePaciente, cvePresupuestal },
     	        new IngresoRowMapperHelper());
    	
    }
    
    @Override
    public List<Ingreso> buscarIngresoMadre(String clavePresupuestal, Long clavePaciente,
    		List<Integer> tiposFormato) {

    	List<Ingreso> ingresoMadre = null; 
    	for(Integer parametroTipoFormato:tiposFormato){
    		List<Ingreso> ingreso = jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_MADRE_POR_CLAVEPACIENTE, 
            		new Object[]{clavePresupuestal,clavePaciente,parametroTipoFormato}, 
            		new IngresoRowMapperHelper());
    		
    		if(ingreso.get(0).getCveCama() != null){
    			ingresoMadre = ingreso; 
    			break;
    		}
    	}
    	return ingresoMadre;
        
    }
    
    @Override
    public int obtenerNumSemanasMaxPediatria(String claveEspecialidad) {
    	
    	Especialidad especialidad = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_NUM_MAXIMO_SEMANAS_PEDIATRIA,
                new Object[] { claveEspecialidad }, new EspecialidadRowMapperHelper());
    	return especialidad.getNumEdadMaximaSemana();
    }
    
    @Override
    public List<CamaUnidad> buscarCamaUnidadMovIntra(String clavePresupuestal, String claveCama) {

            return jdbcTemplate.query(SQLConstants.BUSCAR_CAMA_POR_CLAVE_PRESUPUESTAL_CLAVE_CAMA_MOV_INTRA,
                    new Object[] { clavePresupuestal, claveCama},
                    new CamaUnidadRowMapperHelper());
    }

}
