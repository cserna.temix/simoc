package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.SalaUnidad;

public class SalaUnidadRowMapperHelper extends BaseHelper implements RowMapper<SalaUnidad> {

    @Override
    public SalaUnidad mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        SalaUnidad salaUnidad = new SalaUnidad();

        salaUnidad.setClave(resultSet.getString(SQLColumnasConstants.CVE_SALA));
        salaUnidad.setClavePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        salaUnidad.setDescripcion(resultSet.getString(SQLColumnasConstants.DES_SALA));
        salaUnidad.setIndicadorTurnoMatutitno(resultSet.getInt(SQLColumnasConstants.IND_TURNO_MATUTINO));
        salaUnidad.setIndicadorTurnoVespertino(resultSet.getInt(SQLColumnasConstants.IND_TURNO_VESPERTINO));
        salaUnidad.setIndicadorTurnoJornadaAcumulada(resultSet.getInt(SQLColumnasConstants.IND_TURNO_ACUMULADA));
        salaUnidad.setIndicadorTurnoNocturno(resultSet.getInt(SQLColumnasConstants.IND_TURNO_NOCTURNO));
        salaUnidad.setEstatus(resultSet.getBoolean(SQLColumnasConstants.IND_ESTADO));
        salaUnidad.setFechaBaja(validarFechaNula(resultSet, SQLColumnasConstants.FEC_BAJA));

        return salaUnidad;
    }

}
