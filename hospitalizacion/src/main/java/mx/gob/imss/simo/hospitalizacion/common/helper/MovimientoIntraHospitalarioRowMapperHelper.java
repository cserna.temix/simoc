package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;

public class MovimientoIntraHospitalarioRowMapperHelper extends BaseHelper
        implements RowMapper<MovimientoIntraHospitalario> {

    @Override
    public MovimientoIntraHospitalario mapRow(ResultSet resultSet, int rowId) throws SQLException {

        MovimientoIntraHospitalario movimiento = new MovimientoIntraHospitalario();
        movimiento.setClaveIngreso(resultSet.getLong(SQLColumnasConstants.CVE_INGRESO));
        movimiento.setFechaCreacion(validarFechaNula(resultSet, SQLColumnasConstants.FEC_CREACION));
        movimiento.setFechaMovimiento(validarFechaNula(resultSet, SQLColumnasConstants.FEC_MOVIMIENTO));
        movimiento.setCamaOrigen(resultSet.getString(SQLColumnasConstants.CVE_CAMA_ORIGEN));
        movimiento.setEspeciaidadCamaOrigen(resultSet.getString(SQLColumnasConstants.CVE_ESP_CAMA_ORIGEN));
        movimiento.setClavePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        movimiento.setEspecialidadOrigen(resultSet.getString(SQLColumnasConstants.CVE_ESP_ORIGEN));
        movimiento.setCapturista(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA));
        movimiento.setTipoFormato(resultSet.getLong(SQLColumnasConstants.CVE_TIPO_FORMATO));
        movimiento.setCamaDestino(resultSet.getString(SQLColumnasConstants.CVE_CAMA_DESTINO));
        movimiento.setEspecialidadCamaDestino(resultSet.getString(SQLColumnasConstants.CVE_ESP_DESTINO));
        return movimiento;
    }

}
