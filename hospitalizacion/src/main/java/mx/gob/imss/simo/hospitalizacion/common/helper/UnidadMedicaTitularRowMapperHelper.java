package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;

public class UnidadMedicaTitularRowMapperHelper extends BaseHelper implements RowMapper<UnidadMedica> {

    @Override
    public UnidadMedica mapRow(ResultSet resultSet, int rowId) throws SQLException {

        UnidadMedica unidadMedica = new UnidadMedica();
        unidadMedica.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        unidadMedica.setDesUnidadMedica(resultSet.getString(SQLColumnasConstants.DES_UNIDAD_MEDICA));
        unidadMedica.setIndAscripcion(resultSet.getBoolean(SQLColumnasConstants.IND_ASCRIPCION));
        unidadMedica.setIndSimo(resultSet.getBoolean(SQLColumnasConstants.IND_SIMO));
        unidadMedica.setCveCluesSalud(resultSet.getString(SQLColumnasConstants.CVE_CLUES_SALUD));
        unidadMedica.setCveDelegacionSiap(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION_SIAP));
        unidadMedica.setCveDelegacionImss(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
        unidadMedica.setCveEntidadFed(resultSet.getString(SQLColumnasConstants.CVE_ENTIDAD_FED));
        unidadMedica.setRefNivelUnidad(resultSet.getString(SQLColumnasConstants.REF_NIVEL_UNIDAD));
        unidadMedica.setCveTipoUnidadServicio(resultSet.getString(SQLColumnasConstants.CVE_TIPO_UNIDAD_SERVICIO));
        unidadMedica.setDesNumUnidad(resultSet.getString(SQLColumnasConstants.DES_NUM_UNIDAD));
        unidadMedica.setRefJurisdiccionSsa(resultSet.getString(SQLColumnasConstants.REF_JURISDICCION_SSA));
        unidadMedica.setCvePrei(resultSet.getString(SQLColumnasConstants.CVE_PREI));
        unidadMedica.setFecBaja(resultSet.getDate(SQLColumnasConstants.FEC_BAJA));
        unidadMedica.setIndTitular(resultSet.getInt(SQLColumnasConstants.IND_TITULAR));

        return unidadMedica;
    }

}
