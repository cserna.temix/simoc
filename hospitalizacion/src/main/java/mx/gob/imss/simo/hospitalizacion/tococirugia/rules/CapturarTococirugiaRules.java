package mx.gob.imss.simo.hospitalizacion.tococirugia.rules;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.AtencionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TococirugiaConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosTococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoAtencionToco;
import mx.gob.imss.simo.hospitalizacion.common.model.SalaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;
import mx.gob.imss.simo.hospitalizacion.tococirugia.repository.TococirugiaRepository;

@Component
public class CapturarTococirugiaRules extends HospitalizacionCommonRules {

    @Autowired
    private TococirugiaRepository tococirugiaRepository;
    @Autowired
    private HospitalizacionCommonRepository hospitalizacionCommonRepository;
    @Autowired
    private HospitalizacionCommonRules hospitalizacionCommonRules;

    public void validarHoraParto(Date fechaIngreso, String horaIngreso, Date fechaIngresoTococirugia)
            throws HospitalizacionException {

        if (null == horaIngreso || horaIngreso.isEmpty() || BaseConstants.HORA_VACIA.equals(horaIngreso)
                || horaIngreso.contains(CapturarIngresosConstants.MASCARA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(TococirugiaConstants.REQUERIDO_HORA_PARTO));
        }
        int hora = Integer.parseInt(horaIngreso.substring(0, 2));
        int minuto = Integer.parseInt(horaIngreso.substring(3));
        if (validarHoraMinuto(hora, minuto)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_031);
        }
        Date fecheHoraParto = getHospitalizacionCommonHelper().concatenarFechaHora(fechaIngresoTococirugia,
                horaIngreso);
        if (!fecheHoraParto.after(fechaIngreso)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_165);
        }
    }

    private boolean validarHoraMinuto(int hora, int minuto) {

        return validarHora(hora) || validarMinuto(minuto);
    }

    private boolean validarHora(int hora) {

        return hora < 0 || hora > TococirugiaConstants.HORA_MAXIMA;
    }

    private boolean validarMinuto(int minuto) {

        return minuto < 0 || minuto > TococirugiaConstants.MINUTO_MAXIMO;
    }

    public void validarSala(String cvePresupuestal, String cveSala) throws HospitalizacionException {

        if (null == cveSala || cveSala.isEmpty() || TococirugiaConstants.SALA_VACIA.equals(cveSala)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(TococirugiaConstants.REQUERIDO_NUMERO_SALA));
        }
        validarSalaActiva(cvePresupuestal, cveSala);
    }

    public void validarSalaActiva(String cvePresupuestal, String cveSala) throws HospitalizacionException {

        List<SalaUnidad> salaUnidad = hospitalizacionCommonRepository.buscarSalasUnidad(cvePresupuestal, cveSala);
        if (null == salaUnidad || salaUnidad.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_152));
        }

    }

    public void validarCandidatoTococirugia(String agregadoMedico) throws HospitalizacionException {

        Integer sexo = getHospitalizacionCommonHelper().sexoPorAgregadoMedico(agregadoMedico);
        Integer anio = getHospitalizacionCommonHelper().edadPorAgregadoMedico(agregadoMedico);
        Especialidad especialidad = hospitalizacionCommonRepository
                .buscarEspecialidad(TococirugiaConstants.ESPECIALIDAD_TOCOCIRUGIA);
        if (especialidad.getCveSexo() != sexo || anio < especialidad.getNumEdadMinima()
                || anio > especialidad.getNumEdadMaxima()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_067));
        }
    }

    public void validarCama(String cveCama) throws HospitalizacionException {

        if (null == cveCama || cveCama.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(TococirugiaConstants.REQUERIDO_CAMA));
        }
    }

    public void validarAtencion(Date fechaAtencion, DatosHospitalizacion datosHospitalizacion)
            throws HospitalizacionException {

        if (null == datosHospitalizacion.getDatosTococirugia().getAtencion()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(TococirugiaConstants.REQUERIDO_ATENCION));
        }
        List<Tococirugia> tococirugiaUltmaAtencion = tococirugiaRepository
                .buscarUltimaAtencionTococirugia(datosHospitalizacion);

        if (null != tococirugiaUltmaAtencion && !tococirugiaUltmaAtencion.isEmpty()
                && !validarTipoAtencionPermitida(tococirugiaUltmaAtencion.get(0).getCveTipoAtencion(),
                        datosHospitalizacion.getDatosTococirugia().getAtencion(), fechaAtencion,
                        tococirugiaUltmaAtencion.get(0).getFechaAtencion())) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_069));
        }else{
        	if(!datosHospitalizacion.getDatosTococirugia().getAtencion().
        			equals(TococirugiaConstants.TipoAtencionEnum.PARTO.getClaveTipoAtencion()) &&
        			datosHospitalizacion.getDatosTococirugia().getTotalNacidos() != null &&
        			datosHospitalizacion.getDatosTococirugia().getTipoParto() != null){
        		throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_020));	
        	}
        }
        

    }
    
    public void validarEpisiotomia(String episiotomia) throws HospitalizacionException {

        if (null == episiotomia || episiotomia.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(TococirugiaConstants.REQUERIDO_EPISIOTOMIA));
        }
    }

    /**
     * Valida el tipo de atencion permitida.
     * 
     * @param cveTipoAtencionAnterior
     * @param cveTipoAtencionIngresada
     * @param fechaAtencionActual
     * @param fechaAtencionPrevia
     * @return
     */
    private boolean validarTipoAtencionPermitida(int cveTipoAtencionAnterior, int cveTipoAtencionIngresada,
            Date fechaAtencionActual, Date fechaAtencionPrevia) {

        boolean tipoAtencionPermitida = false;

        /*
         * No se valida el periodo de fecha para el tipo de atencion legrado, ya que por regla de negocio no hay limite
         */
        if (TococirugiaConstants.TipoAtencionEnum.LEGRADO.getClaveTipoAtencion() == cveTipoAtencionIngresada) {
            return true;
        }

        long rangodias = validarRangoFecha(fechaAtencionPrevia, fechaAtencionActual);

        List<PeriodoAtencionToco> periodoAtencionToco = tococirugiaRepository
                .obtenerPeriodoAtencionToco(cveTipoAtencionAnterior, cveTipoAtencionIngresada);

        int diasValidos = periodoAtencionToco.get(0).getNumPeriodoDias();

        if (rangodias > diasValidos) {
            tipoAtencionPermitida = true;
        }

        return tipoAtencionPermitida;
    }

    @SuppressWarnings("unused")
    private boolean validarRangoAnioFecha(Date fechaAtencionActual, Date fechaAtencionPrevia) {

        return getHospitalizacionCommonHelper().validarRangoAnioFecha(fechaAtencionActual, fechaAtencionPrevia);
    }

    private long validarRangoFecha(Date fechaAtencionActual, Date fechaAtencionPrevia) {

        return getHospitalizacionCommonHelper().validarRangoFecha(fechaAtencionActual, fechaAtencionPrevia);
    }

    public void validarTipoParto(Integer cveTipoParto) throws HospitalizacionException {

        if (null == cveTipoParto) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(TococirugiaConstants.REQUERIDO_TIPO_PARTO));
        }
    }

    public void validarTotalNacidos(Integer totalNacidos, int maxRecienNacidos) throws HospitalizacionException {

        if (null == totalNacidos) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(TococirugiaConstants.REQUERIDO_TOTAL_NACIDO));
        } else if (totalNacidos < TococirugiaConstants.TOTAL_NACIDOS_MIN || totalNacidos > maxRecienNacidos) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_068));
        }
    }

    public void validarNacidoModal(String cveTipoNacido) throws HospitalizacionException {

        if (null == cveTipoNacido || cveTipoNacido.equals(TococirugiaConstants.CADENA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(TococirugiaConstants.REQUERIDO_NACIDO));
        }

    }

    public void validarSexoModal(Integer cveTipoSexo) throws HospitalizacionException {

        if (null == cveTipoSexo) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(TococirugiaConstants.REQUERIDO_SEXO));
        }
    }

    public void validarPerimetroCefalicoRecienNacido(Integer perimetroCefalico) throws HospitalizacionException {

        if (null == perimetroCefalico) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(TococirugiaConstants.REQUERIDO_PERIMETRO_CEFALICO));
        } else if (perimetroCefalico < TococirugiaConstants.PERIMETRO_CEFALICO_CENTIMETROS_RECIEN_NACIDO_MIN
                || perimetroCefalico > TococirugiaConstants.PERIMETRO_CEFALICO_CENTIMETROS_RECIEN_NACIDO_MAX) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_060));
        }
    }

    public void validarSemanasGestacionRecienNacido(Integer semanasGestacion) throws HospitalizacionException {

        if (null == semanasGestacion) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(TococirugiaConstants.REQUERIDO_SEMANAS_GESTACION));
        } else if (semanasGestacion < TococirugiaConstants.SEMANAS_GESTACION_RECIEN_NACIDO_MIN
                || semanasGestacion > TococirugiaConstants.SEMANAS_GESTACION_RECIEN_NACIDO_MAX) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_061));
        }
    }

    public void validarApgar1RecienNacido(Integer apgar1) throws HospitalizacionException {

        if (null == apgar1) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(TococirugiaConstants.REQUERIDO_APGAR_MINUTO));
        } else if (apgar1 < TococirugiaConstants.APGAR1_RECIEN_NACIDO_MIN
                || apgar1 > TococirugiaConstants.APGAR1_RECIEN_NACIDO_MAX) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_062));
        }
    }

    public void validarApgar5RecienNacido(Integer apgar5) throws HospitalizacionException {

        if (null == apgar5) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(TococirugiaConstants.REQUERIDO_APGAR_CINCO_MINUTOS));
        } else if (apgar5 < TococirugiaConstants.APGAR5_RECIEN_NACIDO_MIN
                || apgar5 > TococirugiaConstants.APGAR5_RECIEN_NACIDO_MAX) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_063));
        }
    }

    public void validarHoraDefuncion(Date fechaParto, String horaParto, String horaDefuncion)
            throws HospitalizacionException {

        if (null == horaDefuncion || horaDefuncion.isEmpty() || BaseConstants.HORA_VACIA.equals(horaDefuncion)
                || horaDefuncion.contains(CapturarIngresosConstants.MASCARA_VACIA)
                || horaDefuncion.contains(CapturarIngresosConstants.MASCARA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(TococirugiaConstants.REQUERIDO_HORA_DEFUNCION));
        }
        int hora = Integer.parseInt(horaDefuncion.substring(0, 2));
        int minuto = Integer.parseInt(horaDefuncion.substring(3));
        if (validarHoraMinuto(hora, minuto)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_031);
        }
        Date fecheHoraParto = getHospitalizacionCommonHelper().concatenarFechaHora(fechaParto, horaParto);
        Date fecheHoraDefuncion = getHospitalizacionCommonHelper().concatenarFechaHora(fechaParto, horaDefuncion);
        if (!fecheHoraDefuncion.after(fecheHoraParto)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_065);
        }
    }

    public boolean sePuedeActivarGuardar(DatosTococirugia toco, DatosPaciente paciente, DatosMedico medico) {

        if (validaCadenaNuloVacio(toco.getFecAtencion()) && validaCadenaNuloVacio(toco.getNumeroSala())
                && validaCadenaNuloVacio(paciente.getNss()) && validaCadenaNuloVacio(paciente.getCama())
                && validaNumeroNuloVacio(toco.getAtencion()) && validaCadenaNuloVacio(medico.getMatricula())) {

            if (toco.getAtencion().equals(AtencionEnum.PARTO.getClave())) {
                if (validaNumeroNuloVacio(toco.getTipoParto()) && validaCadenaNuloVacio(toco.getHoraParto())
                        && validaNumeroNuloVacio(toco.getTotalNacidos())) {
                    return true;
                } else {
                    return false;
                }
            }
            return true;

        }
        return false;
    }

    private boolean validaCadenaNuloVacio(String dato) {

        if (null != dato && !dato.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean validaNumeroNuloVacio(Integer dato) {

        if (null != dato && dato > 0) {
            return true;
        }
        return false;
    }

    public void validarFechaIngreso(Date fechaQX, Date fechaIngreso) throws HospitalizacionException {

        if (validarFecha(fechaIngreso, fechaQX)) {
            if (!getHospitalizacionCommonHelper().converDateToString(fechaIngreso)
                    .equals(getHospitalizacionCommonHelper().converDateToString(fechaQX))) {
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_057));
            }
        }
    }

    public TococirugiaRepository getTococirugiaRepository() {

        return tococirugiaRepository;
    }

    public void setTococirugiaRepository(TococirugiaRepository tococirugiaRepository) {

        this.tococirugiaRepository = tococirugiaRepository;
    }

    public HospitalizacionCommonRepository getHospitalizacionCommonRepository() {

        return hospitalizacionCommonRepository;
    }

    public void setHospitalizacionCommonRepository(HospitalizacionCommonRepository hospitalizacionCommonRepository) {

        this.hospitalizacionCommonRepository = hospitalizacionCommonRepository;
    }

    public HospitalizacionCommonRules getHospitalizacionCommonRules() {

        return hospitalizacionCommonRules;
    }

    public void setHospitalizacionCommonRules(HospitalizacionCommonRules hospitalizacionCommonRules) {

        this.hospitalizacionCommonRules = hospitalizacionCommonRules;
    }

    public void validarCamaRequerida(String cveCama) throws HospitalizacionException {

        if (null == cveCama || cveCama.equals(BaseConstants.CADENA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_CAMA));
        }
    }
    
    public void validarGesta(Integer gesta) throws HospitalizacionException{
    	if(null != gesta){
    		if(gesta <0 || gesta > 15){
    			throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_603));    			
    		}
    	}else{
    		throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),getArray(BaseConstants.REQUERIDO_GESTA));
    	}
    	
    	
    }

}
