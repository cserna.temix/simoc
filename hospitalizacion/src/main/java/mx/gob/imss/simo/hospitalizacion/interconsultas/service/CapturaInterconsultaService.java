package mx.gob.imss.simo.hospitalizacion.interconsultas.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.AtencionInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DelegacionIMSS;
import mx.gob.imss.simo.hospitalizacion.common.model.EncabezadoInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;

public interface CapturaInterconsultaService extends HospitalizacionCommonServices{

	public void validarRequeridoFecha(String fecha) throws HospitalizacionException;

	public void validarFechaInterconsulta(String fecha) throws HospitalizacionException;

	public void validarFechaEnPeriodoActualActivo(String fecha, String clavePresupuestal)
			throws HospitalizacionException;

	public String validarMatricula(String matricula, String delegacion) throws HospitalizacionException;

	public List<Especialidad> obtenerEspecialidadesPorCvePres(String clavePresupuestal) throws HospitalizacionException;

	public void validarRequeridoEspecialidad(String especialidad) throws HospitalizacionException;

	public List<DatosCatalogo> obtenerCatalogoTurno() throws HospitalizacionException;

	public void validarRequeridoTurno(String turno) throws HospitalizacionException;

	public void guardarEncabezado( EncabezadoInterconsulta encabezado ) throws HospitalizacionException;
	
	public EncabezadoInterconsulta buscarEncabezadoInterconsulta(String fechaInterconsulta, String claveEspecialidad, String claveMedico, Integer claveTurno) throws HospitalizacionException;
	
	public Long obtenerConsecutivoInterconsultas(Integer cve_interconsulta) throws HospitalizacionException;

	public void validarNss(String nss) throws HospitalizacionException;

	public void validarAgregadoMedico() throws HospitalizacionException;

	public void validarEdad(String edad) throws HospitalizacionException;

	public boolean habilitarEdad(String fechaIngreso, String anio);

	public Map<TipoCalculoEdadEnum, Integer> calcularEdadPacienteNoEncontrado(String anio, String fechaIngreso);

	public void validarRequeridoDuracionIntercon(Integer duracion) throws HospitalizacionException;

	public void validarRequeridoPrimVez(String primeraVez) throws HospitalizacionException;

	public void validarRequeridoEspecialidadSolic(String especialidad) throws HospitalizacionException;

	public void validarRequeridoAltaIntercon(String alta) throws HospitalizacionException;

	public void validarDiagnosticoPorViolencia(String cveDiagnostico) throws HospitalizacionException;

	public void validarDiagnosticoRepetido(String cveDiag1, String cveDiag2, String cveDiag3)
			throws HospitalizacionException;

	public Procedimiento buscarProcedimiento(String cveProcedimiento);

	public void validarProcedimientos(String cveProcedimiento, DatosPaciente datosPaciente, boolean validar)
			throws HospitalizacionException;
	
	public List<String> filtrarCatalogoTurnoEncabezado (String query, List<DatosCatalogo> catalogoTurnoList);
	
	public List<String> filtrarCatalogoEspecialidadEncabezado(String query, List<Especialidad> especialidadesList);
	
	public List<String> filtrarCatalogoDelegacionDetalle(String query, List<DelegacionIMSS> delegaciones);
	
	public long insertarPacienteInterconsulta(DatosPaciente paciente);
	
	public void insertarDetalleInterconsulta(EncabezadoInterconsulta encabezado, AtencionInterconsulta atencionInteronsulta) throws HospitalizacionException;
	
	public void validarDatosCapturaPaciente(String nombre, Integer campoAValidar) throws HospitalizacionException;
	
	public void validarDelegacion(String delegacion, List<DatosCatalogo> delegaciones) throws HospitalizacionException;
	
	public UnidadMedica buscarUnidadMedica(String numeroUnidad, String claveDelegacion);

	void validarFechaIngreso(String fechaIngreso, String clavePresupuestal) throws HospitalizacionException;

	boolean esPeriodoExtemporaneo(Date fechaCaptura, String clavePresupuestal) throws HospitalizacionException;

	List<DatosCatalogo> obtenerCatalogoEspecialidad(TipoUbicacionEnum tipoUbicacion, String clavePresupuestal);

	Integer validarEspecialidadInterconsulta(String claveEspecialidad,
			String clavePresupuestal) throws HospitalizacionException;

	/**
	 * @param cveEspecialidad
	 * @param edadAnios
	 * @param edadSemanas
	 * @param sexoPaciente
	 * @throws HospitalizacionException
	 */
	void validarEspecialidadPermitidaPaciente(String cveEspecialidad, Integer edadAnios, Integer edadSemanas,
			 Integer sexoPaciente) throws HospitalizacionException;

	Procedimiento buscarProcedimientoBilateral(String cveProcedimiento);

	
}
