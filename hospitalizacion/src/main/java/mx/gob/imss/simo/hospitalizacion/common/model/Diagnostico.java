package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Diagnostico {

    private String cveCie10;
    private String desCie10;
    private int cveSexo;
    private int numEdadInferiorSemanas;
    private int numEdadInferiorAnios;
    private int numEdadSuperiorSemanas;
    private int numEdadSuperiorAnios;
    private int indTriv;
    private int indErradicado;
    private int indNotifInternacional;
    private int indNotifObligatoria;
    private int indNotifInmediata;
    private int indNotifObstetrica;
    private int indNoValidaBasicaDef;
    private int indNoValidaAfeccionHosp;
    private int indValidaMuerteFetal;
    private int indEnfCronica;
    private int indTransmisible;
    private int indPrincipalConsExterna;
    private int indNivel1;
    private int indNivel2;
    private int indNivel3;
    private int indExclusion;
    private Date fecBaja;

}
