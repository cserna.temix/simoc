package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoRecienNacido;

public class EgresoRecienNacidoRowMapperHelper extends BaseHelper implements RowMapper<EgresoRecienNacido> {

    @Override
    public EgresoRecienNacido mapRow(ResultSet resultSet, int rowId) throws SQLException {

        EgresoRecienNacido egresoRN = new EgresoRecienNacido();

        egresoRN.setCvePaciente(resultSet.getLong(SQLColumnasConstants.CVE_PACIENTE));
        return egresoRN;
    }

}
