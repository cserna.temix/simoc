package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.ConfCaptura;

public class ConfCapturaRowMapperHelper extends BaseHelper implements RowMapper<ConfCaptura>{

	@Override
	  public ConfCaptura mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

		  	ConfCaptura conf = new ConfCaptura();
	        conf.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
	        conf.setCveTipoCaptura(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_CAPTURA));
	        conf.setIndConfActiva(resultSet.getInt(SQLColumnasConstants.IND_CONF_ACTIVA));
	        
	        return conf;
	    }

}
