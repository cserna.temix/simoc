package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class SubRol {

    private Integer cveSubrol;
    private String desSubrol;

}
