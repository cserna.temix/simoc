package mx.gob.imss.simo.hospitalizacion.ingresos.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngreso;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.model.DatosUsuario;

@Component
public class CapturarIngresoHelper extends HospitalizacionCommonHelper {

    public Ingreso prepararIngreso(long cvePaciente, TipoUbicacionEnum tipoUbicacion, DatosUsuario datosUsuario,
            DatosIngreso datosIngreso, CamaUnidad camaUnidad, DatosMedico datosMedico, boolean ingresoPadre) {

        int tipoFormato;
        switch (tipoUbicacion) {
            case HOSPITALIZACION:
                tipoFormato = TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave();
                break;
            case URGENCIAS:
                tipoFormato = TipoFormatoEnum.INGRESO_URGENCIAS.getClave();
                break;
            case UCI:
                tipoFormato = TipoFormatoEnum.INGRESO_UCI.getClave();
                break;
            default:
                tipoFormato = 0;
                break;
        }
        String fechaCompleta = datosIngreso.getFechaIngreso().concat(BaseConstants.ESPACIO)
                .concat(datosIngreso.getHoraIngreso());
        Date fechaIngreso;
        try {
            fechaIngreso = new SimpleDateFormat(BaseConstants.FORMATO_FECHA_HORA).parse(fechaCompleta);
        } catch (ParseException e) {
            fechaIngreso = new Date();
        }
        String especialidad = datosIngreso.getEspecialidad();
        if (especialidad.length() > BaseConstants.LONGITUD_ESPECIALIDAD) {
            especialidad = especialidad.substring(0, BaseConstants.LONGITUD_ESPECIALIDAD);
        }
        Ingreso ingreso = new Ingreso();
        ingreso.setCveIngresoPadre(datosIngreso.getIngresoPadre());
        ingreso.setFecCreacion(datosIngreso.getFechaCaptura());
        ingreso.setCvePaciente(cvePaciente);
        ingreso.setCvePresupuestal(datosUsuario.getCvePresupuestal());
        ingreso.setCveEspecialidadIngreso(especialidad);
        ingreso.setCveTipoFormato(tipoFormato);
        ingreso.setCveCapturista(datosUsuario.getUsuario());
        ingreso.setFecIngreso(fechaIngreso);
        if (!ingresoPadre) {
            ingreso.setCveCama(camaUnidad.getCveCama());
            ingreso.setCveEspecialidadCama(camaUnidad.getCveEspecialidad());
        }
        ingreso.setRefCamaRn(datosIngreso.getCamaRecienNacido());
        if (ingresoPadre) {
            ingreso.setCveTipoIngreso(CapturarIngresosConstants.TIPO_INGRESO_URGENTE_HOSPITALIZACION);
        } else {
            ingreso.setCveTipoIngreso((datosIngreso.getTipoIngreso() == null || datosIngreso.getTipoIngreso().isEmpty())
                    ? null : Integer.valueOf(datosIngreso.getTipoIngreso()));
        }
        ingreso.setCveTipoPrograma((datosIngreso.getTipoPrograma() == null || datosIngreso.getTipoPrograma().isEmpty())
                ? null : Integer.valueOf(datosIngreso.getTipoPrograma()));
        ingreso.setCveMedico(datosMedico.getMatricula());
        ingreso.setIndExtemporaneo(datosIngreso.isExtemporaneo());
        return ingreso;
    }

    public Paciente prepararPaciente(DatosPaciente datosPaciente, TipoUbicacionEnum tipoUbicacion) {

        String regimen = datosPaciente.getAgregadoMedico().substring(BaseConstants.INICIO_AGREGADO_REGIMEN);
        Paciente paciente = new Paciente();
        paciente.setCveIdPersona(datosPaciente.getIdPersona());
        paciente.setRefNss(datosPaciente.getNss());
        paciente.setRefAgregadoMedico(datosPaciente.getAgregadoMedico());
        paciente.setRefNombre(datosPaciente.getNombre());
        paciente.setRefApellidoPaterno(datosPaciente.getApellidoPaterno());
        paciente.setRefApellidoMaterno(datosPaciente.getApellidoMaterno());
        paciente.setFecConsulta(datosPaciente.getFechaConsulta());
        paciente.setFecNacimiento(datosPaciente.getFechaNacimiento());
        paciente.setNumEdadSemanas(datosPaciente.getEdadSemanas());
        paciente.setNumEdadAnios(datosPaciente.getEdadAnios());
        paciente.setCveCurp(datosPaciente.getCurp());
        paciente.setCveIdee(datosPaciente.getIdee());
        paciente.setIndAcceder(datosPaciente.isMarcaAcceder());
        paciente.setIndVigencia(datosPaciente.isVigencia());
        paciente.setCveSexo(datosPaciente.getSexo());
        paciente.setRefTipoPension(datosPaciente.getTipoPension());
        paciente.setRefRegistroPatronal(datosPaciente.getRegistroPatronal());
        paciente.setCveUnidadAdscripcion(datosPaciente.getClavePresupuestal());
        paciente.setIndRecienNacido(tipoUbicacion.equals(TipoUbicacionEnum.RECIEN_NACIDOS) ? true : false);
        paciente.setIndDerechohabiente(
                regimen.equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave()) ? false : true);
        paciente.setIndUltimoRegistro(true);
        return paciente;
    }

    public Paciente prepararPacienteRN(DatosIngresoRN datosIngresoRN, Paciente pacientePadre) {

        Paciente paciente = new Paciente();
        paciente.setRefNss(datosIngresoRN.getNss());
        paciente.setRefAgregadoMedico(datosIngresoRN.getAgregadoMedico());
        paciente.setRefNombre(datosIngresoRN.getNombre());
        paciente.setRefApellidoPaterno(datosIngresoRN.getApellidoPaterno());
        paciente.setRefApellidoMaterno(datosIngresoRN.getApellidoMaterno());
        if(pacientePadre != null) {
        	paciente.setIndVigencia(pacientePadre.isIndVigencia());
        }
        paciente.setCveUnidadAdscripcion(datosIngresoRN.getClavePresupuestal());
        paciente.setIndRecienNacido(true);
        if(pacientePadre != null) {
        	paciente.setIndDerechohabiente(pacientePadre.isIndDerechohabiente());
        }
        paciente.setIndUltimoRegistro(true);
        paciente.setNumEdadAnios(0);
        return paciente;
    }

    public Integer obtenerTipoFormatoIngresoRecienNacido(String especialidadIngreso) {

        if (especialidadIngreso.substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS).equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO.
        		substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS))) {
            return TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave();
        } else if (especialidadIngreso.substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS).equals(BaseConstants.ESPECIALIDAD_CUNERO_PATOLOGICO.
        		substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS))) {
            return TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave();
        } else {
            return TipoFormatoEnum.INGRESO_RN_UCIN.getClave();
        }
    }

    public Ingreso prepararIngresoRecienNacido(DatosIngresoRN datosIngresoRN, Integer tipoFormato,
            DatosUsuario datosUsuario) {

        String fechaCompleta = datosIngresoRN.getFechaIngreso().concat(BaseConstants.ESPACIO)
                .concat(datosIngresoRN.getHoraIngreso());
        Date fechaIngreso;
        try {
            fechaIngreso = new SimpleDateFormat(BaseConstants.FORMATO_FECHA_HORA).parse(fechaCompleta);
        } catch (ParseException e) {
            fechaIngreso = new Date();
        }
        Ingreso ingresoRN = new Ingreso();
        if (datosIngresoRN.getClaveIngresoPadre() != null) {
        	ingresoRN.setCveIngresoPadre(datosIngresoRN.getClaveIngresoPadre());
        }
        ingresoRN.setFecCreacion(new Date());
        if(datosIngresoRN.getClavePaciente() != null) {
        	ingresoRN.setCvePaciente(datosIngresoRN.getClavePaciente());
        }
        ingresoRN.setCvePresupuestal(datosUsuario.getCvePresupuestal());
        ingresoRN.setCveEspecialidadIngreso(datosIngresoRN.getEspecialidadIngreso());
        ingresoRN.setCveTipoFormato(tipoFormato);
        ingresoRN.setCveCapturista(datosUsuario.getUsuario());
        ingresoRN.setFecIngreso(fechaIngreso);
        ingresoRN.setRefCamaRn(datosIngresoRN.getCamaRN());
        ingresoRN.setCveEspecialidadCama(datosIngresoRN.getEspecialidadCamaRN());
        ingresoRN.setCveMedico(datosIngresoRN.getMatricula());
        ingresoRN.setIndExtemporaneo(datosIngresoRN.isExtemporaneo());
        if(datosIngresoRN.getNumeroRN() != null){
        	ingresoRN.setNumRecienNacido(datosIngresoRN.getNumeroRN());
        }
        return ingresoRN;
    }

    public List<DatosCatalogo> filtrarCatalogoEspecialidad(List<DatosCatalogo> especialidades,
            Integer claveTipoFormato) {

        List<DatosCatalogo> filtro = new ArrayList<DatosCatalogo>();
        if (claveTipoFormato.equals(TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave())
                || claveTipoFormato.equals(TipoFormatoEnum.INGRESO_URGENCIAS.getClave())
                || claveTipoFormato.equals(TipoFormatoEnum.INGRESO_UCI.getClave())) {
            filtro.addAll(especialidades);
        } else if (claveTipoFormato.equals(TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave())) {
            for (DatosCatalogo especialidad : especialidades) {
                if (especialidad.getClave().equals(BaseConstants.ESPECIALIDAD_CUNERO_PATOLOGICO)
                        || especialidad.getClave().equals(BaseConstants.ESPECIALIDAD_UCI_NEONATOS)) {
                    filtro.add(especialidad);
                }
            }
        } else if (claveTipoFormato.equals(TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave())) {
            for (DatosCatalogo especialidad : especialidades) {
                if (especialidad.getClave().equals(BaseConstants.ESPECIALIDAD_UCI_NEONATOS)) {
                    filtro.add(especialidad);
                }
            }
        }
        return filtro;
    }
    
    public List<DatosCatalogo> filtrarCatalogoEspecialidad(List<DatosCatalogo> especialidades) {

        List<DatosCatalogo> filtro = new ArrayList<DatosCatalogo>();
            for (DatosCatalogo especialidad : especialidades) {
                if (especialidad.getClave().substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS)
                        .equals(BaseConstants.ESPECIALIDAD_CUNERO_PATOLOGICO.substring(BaseConstants.INDEX_CERO,BaseConstants.INDEX_DOS))) {
                    filtro.add(especialidad);
                }
            }
        
        
        return filtro;
    }

}
