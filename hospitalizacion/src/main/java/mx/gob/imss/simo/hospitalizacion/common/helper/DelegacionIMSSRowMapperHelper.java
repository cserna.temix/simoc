package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.DelegacionIMSS;

public class DelegacionIMSSRowMapperHelper extends BaseHelper implements RowMapper<DelegacionIMSS> {

    @Override
    public DelegacionIMSS mapRow(ResultSet resultSet, int rowId) throws SQLException {

        DelegacionIMSS delegacionIMSS = new DelegacionIMSS();
        delegacionIMSS.setCveDelegacionImss(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
        delegacionIMSS.setDesDelegacionImss(resultSet.getString(SQLColumnasConstants.DES_DELEGACION_IMSS));
        delegacionIMSS.setRefAbreviaturaDel(resultSet.getString(SQLColumnasConstants.REF_ABREVIATURA_DEL));
        delegacionIMSS.setRefMarca(resultSet.getString(SQLColumnasConstants.REF_MARCA));
        delegacionIMSS.setFecBaja(resultSet.getDate(SQLColumnasConstants.FEC_BAJA_DELEGACION_IMSS));
        return delegacionIMSS;
    }

}
