package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class PacienteModal {

    private String refNss;
    private String refAgregadoMedico;
    private String refApellidoMaterno;
    private String refApellidoPaterno;
    private String refNombre;
    private String cama;
    private long cveIngreso;
    private long cveTipoFormato;
    private int consecutivo;
    private String unidadAdscripcion;
    private long cvePaciente;
    private Date fechaIngreso;
    private String claveSexo;
    private Date fechaNacimiento;
    private boolean indRecienNacido;
    private boolean indUltimoRegistro;
    private int numEdadAnios;
    private int numEdadSemanas;

}
