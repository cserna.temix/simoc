package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class EstructuraDatoInformacionComplementaria {

    private Integer clave;
    private String valor;
    private String concepto;
}
