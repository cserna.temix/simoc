package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

public class CatalogoRiesgoTrabajoRowMapperHelper extends BaseHelper implements RowMapper<DatosCatalogo>{
	
	@Override
    public DatosCatalogo mapRow(ResultSet resultSet, int rowId) throws SQLException {

        DatosCatalogo datosCatalogo = new DatosCatalogo();
        datosCatalogo.setClave(resultSet.getString(SQLColumnasConstants.CVE_RIESGO_TRABAJO));
        datosCatalogo.setDescripcion(resultSet.getString(SQLColumnasConstants.CVE_RIESGO_TRABAJO) + BaseConstants.ESPACIO
                + resultSet.getString(SQLColumnasConstants.DES_RIESGO_TRABAJO));
        return datosCatalogo;
    }

}
