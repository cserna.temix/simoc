package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitParteII;

public class ParteIIRowMapperHelper extends BaseHelper implements RowMapper<SitParteII> {

    @Override
    public SitParteII mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

    	SitParteII parteII = new SitParteII();
    	parteII.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL_PARTEII));
    	parteII.setCveDivision(resultSet.getInt(SQLColumnasConstants.CVE_DIVISION_PARTEII));
    	parteII.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD_PARTEII));
    	parteII.setCvePeriodoImss(resultSet.getString(SQLColumnasConstants.CVE_PERIODO_IMSS_PARTEII));
    	parteII.setNumCama(resultSet.getInt(SQLColumnasConstants.NUM_CAMA));
    	parteII.setNumDiasCama(resultSet.getDouble(SQLColumnasConstants.NUM_DIAS_CAMA));
    	parteII.setNumDiasPaciente(resultSet.getDouble(SQLColumnasConstants.NUM_DIAS_PACIENTE));
    	parteII.setNumEgrDefuncion(resultSet.getInt(SQLColumnasConstants.NUM_EGR_DEFUNCION));
    	parteII.setNumEgrExterna(resultSet.getInt(SQLColumnasConstants.NUM_EGR_EXTERNA));
    	parteII.setNumEgrHospital(resultSet.getInt(SQLColumnasConstants.NUM_EGR_HOSPITAL));
    	parteII.setNumEgrOtrohosp(resultSet.getInt(SQLColumnasConstants.NUM_EGR_OTROHOSP));
    	parteII.setNumEgrUnidad(resultSet.getInt(SQLColumnasConstants.NUM_EGR_UNIDAD));
    	parteII.setNumEjecucion(resultSet.getInt(SQLColumnasConstants.NUM_EJECUCION));
    	parteII.setNumIngHospital(resultSet.getInt(SQLColumnasConstants.NUM_ING_HOSPITAL));
    	parteII.setNumIngProgramado(resultSet.getInt(SQLColumnasConstants.NUM_ING_PROGRAMADO));
    	parteII.setNumIngUrgente(resultSet.getInt(SQLColumnasConstants.NUM_ING_URGENTE));
    	parteII.setNumIqProgramado(resultSet.getDouble(SQLColumnasConstants.NUM_IQ_PROGRAMADO));
    	parteII.setNumMedico(resultSet.getInt(SQLColumnasConstants.NUM_MEDICO));
    	parteII.setNumPostMorten(resultSet.getInt(SQLColumnasConstants.NUM_POST_MORTEN));
    	parteII.setNumTotalEgreso(resultSet.getDouble(SQLColumnasConstants.NUM_TOTAL_EGRESO));
    	parteII.setNumTotalIngreso(resultSet.getInt(SQLColumnasConstants.NUM_TOTAL_INGRESO));
    	parteII.setNumTotalIq(resultSet.getDouble(SQLColumnasConstants.NUM_TOTAL_IQ));
    	parteII.setPorcDefuncion(resultSet.getDouble(SQLColumnasConstants.PORC_DEFUNCION));
    	parteII.setPorcDiasEstancia(resultSet.getDouble(SQLColumnasConstants.PORC_DIAS_ESTANCIA));
    	parteII.setPorcIndiceRotacion(resultSet.getDouble(SQLColumnasConstants.PORC_INDICE_ROTACION));
    	parteII.setPorcInidiceSustitucion(resultSet.getDouble(SQLColumnasConstants.PORC_INDICE_SUSTITUCION));
    	parteII.setPorcOcupacion(resultSet.getDouble(SQLColumnasConstants.PORC_OCUPACION));
    	parteII.setPorcPostMorten(resultSet.getDouble(SQLColumnasConstants.PORC_POST_MORTEN));
    	parteII.setRefDivision(resultSet.getString(SQLColumnasConstants.REF_DIVISION));
    	parteII.setRefEspecialidad(resultSet.getString(SQLColumnasConstants.REF_ESPECIALIDAD));
    	parteII.setRefUnidadMedica(resultSet.getString(SQLColumnasConstants.REF_UNIDAD_MEDICA));
    	
        return parteII;
    }

}
