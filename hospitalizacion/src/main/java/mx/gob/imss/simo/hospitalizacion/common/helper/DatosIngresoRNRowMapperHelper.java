package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;

public class DatosIngresoRNRowMapperHelper extends BaseHelper implements RowMapper<DatosIngresoRN> {

    @Override
    public DatosIngresoRN mapRow(ResultSet resultSet, int rowId) throws SQLException {

        DatosIngresoRN datosIngresoRN = new DatosIngresoRN();
        datosIngresoRN.setNumeroRN(resultSet.getInt(SQLColumnasConstants.NUM_RECIEN_NACIDO));
        datosIngresoRN.setFechaParto(resultSet.getDate(SQLColumnasConstants.FEC_TOCOCIRUGIA));
        datosIngresoRN.setClaveIngresoPadre(resultSet.getLong(SQLColumnasConstants.CVE_INGRESO));
        datosIngresoRN.setClaveTipoFormato(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_FORMATO));
        //MHLA
        datosIngresoRN.setEspecialidadIngreso(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD_INGRESO));
        //
        datosIngresoRN.setCama(resultSet.getString(SQLColumnasConstants.CVE_CAMA));
        datosIngresoRN.setCamaRN(BaseConstants.PREFIJO_CAMA_RN + resultSet.getString(SQLColumnasConstants.CVE_CAMA));
        datosIngresoRN.setClavePaciente(resultSet.getLong(SQLColumnasConstants.CVE_PACIENTE));
        datosIngresoRN.setNss(resultSet.getString(SQLColumnasConstants.REF_NSS));
        datosIngresoRN.setAgregadoMedico(resultSet.getString(SQLColumnasConstants.REF_AGREGADO_MEDICO));
        datosIngresoRN
                .setNombre(BaseConstants.PREFIJO_NOMBRE_RN + resultSet.getInt(SQLColumnasConstants.NUM_RECIEN_NACIDO));
        datosIngresoRN.setApellidoPaterno(resultSet.getString(SQLColumnasConstants.REF_APELLIDO_PATERNO));
        datosIngresoRN.setApellidoMaterno(resultSet.getString(SQLColumnasConstants.REF_APELLIDO_MATERNO));
        datosIngresoRN.setNumeroUnidad(resultSet.getString(SQLColumnasConstants.DES_NUM_UNIDAD));
        datosIngresoRN.setClaveDelegacion(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
        datosIngresoRN.setDescripcionDelegacion(resultSet.getString(SQLColumnasConstants.DES_DELEGACION_IMSS));
        datosIngresoRN.setClavePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        datosIngresoRN.setDescripcionUnidad(resultSet.getString(SQLColumnasConstants.DES_UNIDAD_MEDICA));
        return datosIngresoRN;
    }

}
