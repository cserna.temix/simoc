package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Menu;

public class MenuRowMapperHelper extends BaseHelper implements RowMapper<Menu> {

    @Override
    public Menu mapRow(final ResultSet resultSet, final int rowNumber) throws SQLException {

        Menu menu = new Menu();
        menu.setClaveMenu(resultSet.getInt(SQLColumnasConstants.CLAVE_MENU));
        menu.setMenuPadre(resultSet.getInt(SQLColumnasConstants.MENU_PADRE));
        menu.setDescripcionMenu(resultSet.getString(SQLColumnasConstants.DESCRIPCION_MENU));
        menu.setRefAccion(resultSet.getString(SQLColumnasConstants.REF_ACCION));
        menu.setPosicion(resultSet.getInt(SQLColumnasConstants.POSICION));
        menu.setCuentaAd(resultSet.getString(SQLColumnasConstants.CUENTA_AD));

        return menu;
    }
}
