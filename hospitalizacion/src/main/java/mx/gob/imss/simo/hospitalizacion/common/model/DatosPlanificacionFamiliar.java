package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class DatosPlanificacionFamiliar {

    private String descPlanificacion;
    private Integer consultaExterna;
    private Integer cantidadCE;
    private Integer intervencionQuirurjica;
    private Integer cantidadIQ;
    private Integer tococirugia;
    private Integer cantidadaT;
    private Integer Egresos;
    private Integer cantidadE;
    private Integer edadIni;
    private Integer edadFin;
    private Integer vigente;
    private String dmemConRiesgo;
    private String dmemSinRiesgo;
    private Integer sexo;

}
