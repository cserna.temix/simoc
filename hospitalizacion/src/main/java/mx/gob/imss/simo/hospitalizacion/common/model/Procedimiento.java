package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Procedimiento {

    private String cveCie9;
    private String desCie9;
    private Integer numFiltro;
    private Integer cveSexo;
    private Integer numEdadInferiorAnios;
    private Integer numEdadInferiorSemanas;
    private Integer numEdadSuperiorAnios;
    private Integer numEdadSuperiorSemanas;
    private Integer numAnioVigencia;
    private String refUso;
    private String refActualizacion;
    private Integer numPrecedencia;
    private Integer indNivel1;
    private Integer indNivel2;
    private Integer indNivel3;
    private Integer numCapituloCie9;
    private Integer numGrupoCie9;
    private Integer numCodigoConsecutivo;
    private String desCie9m;
    private Integer indBilateralidad;
    private Date fecBaja;

}
