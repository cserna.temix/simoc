package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoAtencionToco;

public class CatalogoPeriodoAtencionTocoRowMapperHelper extends BaseHelper implements RowMapper<PeriodoAtencionToco> {

    @Override
    public PeriodoAtencionToco mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        PeriodoAtencionToco periodoAtencionToco = new PeriodoAtencionToco();
        periodoAtencionToco.setCveAtencionPrevia(resultSet.getInt(SQLColumnasConstants.CVE_ATENCION_ACTUAL));
        periodoAtencionToco.setCveAtencionActual(resultSet.getInt(SQLColumnasConstants.CVE_ATENCION_PREVIA));
        periodoAtencionToco.setNumPeriodoDias(resultSet.getInt(SQLColumnasConstants.NUM_PERIODO_DIAS));
        return periodoAtencionToco;
    }

}
