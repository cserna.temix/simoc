package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProcedimientoRealizado {

    private String claveIngreso;
    private String claveCie9;
    private String descripcionCie9;
    private Date fechaIntervencion;
    private Date fechaIngreso;
    private Date fechaEgreso;
    private String agregadoMedico;
    private String nss;
    private String nombreCompleto;

    public ProcedimientoRealizado() {
    }

}
