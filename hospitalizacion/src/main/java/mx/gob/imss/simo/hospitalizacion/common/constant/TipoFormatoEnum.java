package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoFormatoEnum {

    BUSCAR_EDITAR_HOSPITAL(0, "Buscar Editar Hospital Edici�n"),
    CONSULTA_EXTERNA(1, "Consulta externa"),
    INGRESO_HOSPITALIZACION(2, "Ingreso hospitalizaci�n"),
    INGRESO_URGENCIAS(3,"Ingreso Urgencias"),
    INGRESO_UCI(4, "Ingreso UCI"),
    INGRESO_RN_ALOJAMIENTO_CONJUNTO(5, "Ingreso RN alojamiento conjunto"),
    INGRESO_RN_CUNERO_PATOLOGICO(6, "Ingreso RN cunero patol�gico"),
    INGRESO_RN_UCIN(7, "Ingreso RN UCIN"),
    MOVIMIENTO_INTRAHOSPITALARIO(8, "Registro movimiento intrahospitalario"),
    INTERVECION_QUIRURGICA(9, "Registro intervenci�n quir�rgica"),
    TOCOCIRUGIA(10, "Registro Tococirug�a"),
    EGRESO_HOSPITALIZACION(11, "Egreso hospitalizaci�n"),
    EGRESO_URGENCIAS(12, "Egreso urgencias"),
    EGRESO_UCI(13, "Egreso UCI"),
    EGRESO_RN_ALOJAMIENTO_CONJUNTO(14, "Egreso RN alojamiento conjunto"),
    EGRESO_RN_CUNERO_PATOLOGICO(15, "Egreso RN cunero patol�gico"),
    EGRESO_RN_UCIN(16, "Egreso RN UCIN"),
    INTERCONSULTA(17, "Interconsultas"),
    INFORMACION_COMPLEMENTARIA(18, "Registro informaci�n complementaria"),
    SERVICIOS_SUBROGADOS(19, "Registro servicios subrogados");

    private Integer clave;
    private String descripcion;

    private TipoFormatoEnum(Integer clave, String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

    public Integer getClave() {

        return clave;
    }

    public String getDescripcion() {

        return descripcion;
    }

    public static TipoFormatoEnum parse(Integer clave) {

        TipoFormatoEnum right = null;
        for (TipoFormatoEnum item : values()) {
            if (item.getClave().equals(clave)) {
                right = item;
                break;
            }
        }
        return right;
    }

}
