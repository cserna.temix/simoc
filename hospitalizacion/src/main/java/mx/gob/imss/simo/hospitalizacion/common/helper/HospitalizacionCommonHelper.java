package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.SexoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoReporte;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.PacienteModal;
import mx.gob.imss.simo.hospitalizacion.common.model.ReporteGenerado;

@Component
public class HospitalizacionCommonHelper extends BaseHelper {

    public List<String> filtrarCatalogo(List<String> datosCatalogoString, String query, Integer longitud) {

        List<String> filtroCatalogo = new ArrayList<String>();
        if (query.length() < longitud) {
            for (String dato : datosCatalogoString) {
                if (dato.startsWith(query)) {
                    filtroCatalogo.add(dato);
                }
            }
        }
        return filtroCatalogo;
    }

    public List<DatosPaciente> armarPacientes(List<PacienteModal> listaPaciente) {

        List<DatosPaciente> listaDatosPaciente = new ArrayList<DatosPaciente>();
        if (!listaPaciente.isEmpty()) {
            for (PacienteModal paciente : listaPaciente) {
                listaDatosPaciente.add(prepararPaciente(paciente));
            }
        }
        return listaDatosPaciente;
    }

    private DatosPaciente prepararPaciente(PacienteModal paciente) {

        DatosPaciente datosPaciente = new DatosPaciente();
        datosPaciente.setCvePaciente(paciente.getCvePaciente());
        datosPaciente.setNss(paciente.getRefNss());
        datosPaciente.setAgregadoMedico(paciente.getRefAgregadoMedico());
        datosPaciente.setNombre(paciente.getRefNombre());
        datosPaciente.setApellidoPaterno(paciente.getRefApellidoPaterno());
        datosPaciente.setApellidoMaterno(paciente.getRefApellidoMaterno());
        StringBuilder completo = new StringBuilder();
        completo.append(paciente.getRefNombre()).append(BaseConstants.ESPACIO).append(paciente.getRefApellidoPaterno())
                .append(BaseConstants.ESPACIO).append(paciente.getRefApellidoMaterno());
        datosPaciente.setNombreCompleto(completo.toString());
        if (null != paciente.getCama() && !paciente.getCama().isEmpty()) {
            datosPaciente.setCama(paciente.getCama());
        }
        datosPaciente.setColorIdentificador(IdentificadorConexionEnum.VERDE.getRuta());
        datosPaciente.setClavePresupuestal(paciente.getUnidadAdscripcion());
        datosPaciente.setClaveIngreso(paciente.getCveIngreso());
        datosPaciente.setCveTipoFormato(paciente.getCveTipoFormato());
        datosPaciente.setFechaIngreso(paciente.getFechaIngreso());
        datosPaciente.setSexo(null == paciente.getClaveSexo() ? null : Integer.parseInt(paciente.getClaveSexo()));
        datosPaciente.setFechaNacimiento(paciente.getFechaNacimiento());
        datosPaciente.setIndRecienNacido(paciente.isIndRecienNacido());
        datosPaciente.setIndUltimoRegistro(paciente.isIndUltimoRegistro() ? new Long("1") : new Long("0"));
        datosPaciente.setEdadAnios(paciente.getNumEdadAnios());
        datosPaciente.setEdadSemanas(paciente.getNumEdadSemanas());
        datosPaciente.setIndPlanificacionFamiliar(requiereMostrarPlanificacionFamiliar(paciente));
        return datosPaciente;
    }

    public boolean requiereMostrarPlanificacionFamiliar(PacienteModal paciente) {

        if (null != paciente.getFechaNacimiento()) {
            Calendar fecha = Calendar.getInstance();
            fecha.setTime(paciente.getFechaNacimiento());
            Period p = Period.between(
                    LocalDate.of(fecha.get(Calendar.YEAR), fecha.get(Calendar.MONTH) + 1, fecha.get(Calendar.DATE)),
                    LocalDate.now());
            if (p.getYears() <= BaseConstants.EDAD_MINIMA_PLANIFICACION_FAMILIAR) {
                return false;
            }
        } else if (0 != paciente.getNumEdadAnios()
                && paciente.getNumEdadAnios() <= BaseConstants.EDAD_MINIMA_PLANIFICACION_FAMILIAR) {
            return false;

        } else if (0 != paciente.getNumEdadSemanas()
                && paciente.getNumEdadSemanas() <= BaseConstants.EDAD_MINIMA_SEMANAS_PLANIFICACION_FAMILIAR) {
            return false;
        }
        return true;
    }

    public DatosHospitalizacion crearPacienteUnico(List<DatosPaciente> listaDatosPaciente) {

        DatosHospitalizacion paciente = new DatosHospitalizacion();
        paciente.setDatosPaciente(listaDatosPaciente.get(0));
        paciente.getDatosIngreso().setCama(paciente.getDatosPaciente().getCama());
        return paciente;
    }

    public DatosPaciente mapearDatosPacientePorNumero(Integer numeroPaciente,
            List<DatosHospitalizacion> listDatosHospitalizacion) {

        DatosPaciente datosPacienteId = new DatosPaciente();
        for (DatosHospitalizacion datosHospitalizacion : listDatosHospitalizacion) {
            if (datosHospitalizacion.getConsecutivo().equals(numeroPaciente)) {
                datosPacienteId = datosHospitalizacion.getDatosPaciente();
                break;
            }
        }
        return datosPacienteId;
    }

    public DatosPaciente mapearDatosPacienteNumeroCama(Integer numeroCama,
            List<DatosHospitalizacion> listDatosHospitalizacion) {

        DatosPaciente paciente = new DatosPaciente();
        for (DatosHospitalizacion datosHospitalizacion : listDatosHospitalizacion) {
            if (datosHospitalizacion.getConsecutivo().equals(numeroCama)) {
                paciente = datosHospitalizacion.getDatosPaciente();
                break;
            }
        }
        return paciente;
    }
    
    public Date obtenerFecha(String fecha, String hora) {

        Date fechaDia = convertStringToDate(fecha);
        return convertStringHourToDate(fechaDia, hora);
    }
    
    public Date convertStringHourToDate(Date fecha, String horaEntrada) throws NumberFormatException {

        String[] aux = horaEntrada.split(":");
        Calendar fechaAux = Calendar.getInstance();
        fechaAux.setTime(mkDateIni(fecha));
        fechaAux.set(Calendar.HOUR_OF_DAY, Integer.parseInt(aux[0]));
        fechaAux.set(Calendar.MINUTE, Integer.parseInt(aux[1]));
        return fechaAux.getTime();
    }

    public Date convertStringToDate(String fecha) {

        SimpleDateFormat formatter = new SimpleDateFormat(BaseConstants.FORMATO_FECHA);
        Date date = null;
        try {
            date = formatter.parse(fecha);
        } catch (ParseException e) {
            logger.error("Error al generar el parseo");
        }
        return date;
    }

    public String obtenerFechaActual() {

        return new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(new Date());
    }

    public String obtenerMesString(Integer numMesPeriodo) {

        StringBuilder mes = new StringBuilder();
        if (numMesPeriodo < 10) {
            mes.append(BaseConstants.NUMERO_CERO_STRING).append(numMesPeriodo.toString());
        } else {
            mes.append(numMesPeriodo.toString());
        }
        return mes.toString();
    }

    public List<String> filtrarListasComplementarias(String cveFiltro, Integer numeroMaximo,
            List<String> listaOriginal) {

        List<String> listaFiltrada = new ArrayList<String>();
        if (cveFiltro.length() < numeroMaximo) {
            for (String segmentoLista : listaOriginal) {
                if (StringUtils.containsIgnoreCase(segmentoLista, cveFiltro)) {
                    listaFiltrada.add(segmentoLista);
                }
            }
        }
        return listaFiltrada;
    }

    public Integer edadPorAgregadoMedico(String agregadoMedico) {

        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR) - Integer
                .valueOf(agregadoMedico.substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO));
    }
    
    public Integer edadPorAgregadoMedicoFec(String agregadoMedico, Date fecCaptura) {
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecCaptura);
        return calendar.get(Calendar.YEAR) - Integer
                .valueOf(agregadoMedico.substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO));
    }

    public List<String> crearListaEspecialidad(List<DatosCatalogo> catalogoEspecialidad) {

        List<String> catalogo = new ArrayList<String>();
        for (DatosCatalogo datosCatalogo : catalogoEspecialidad) {
            catalogo.add(datosCatalogo.getClave().concat(CapturarEgresoConstants.ESPACIO)
                    .concat(datosCatalogo.getDescripcion()));
        }
        return catalogo;
    }

    public int convertStringToInt(String numAnioPeriodo) {

        return Integer.parseInt(numAnioPeriodo);
    }

    public String converIntToString(int result) {

        return String.valueOf(result);
    }

    public List<String> crearListaPlanificacionFam(List<DatosCatalogo> catalogoPlanificacion) {

        List<String> planificacion = new ArrayList<String>();
        for (DatosCatalogo datosCatalogo : catalogoPlanificacion) {
            planificacion
                    .add(datosCatalogo.getClave().concat(BaseConstants.ESPACIO).concat(datosCatalogo.getDescripcion()));
        }
        return planificacion;
    }

    private Map<String, Object> diferenciaDias(Date startDate, Date endDate) {

        Calendar calStart = Calendar.getInstance();
        Calendar calEnd = Calendar.getInstance();
        calStart.setTime(startDate);
        calEnd.setTime(endDate);
        calStart.set(Calendar.HOUR_OF_DAY, 0);
        calStart.set(Calendar.MINUTE, 0);
        calStart.set(Calendar.SECOND, 0);
        calEnd.set(Calendar.HOUR_OF_DAY, 0);
        calEnd.set(Calendar.MINUTE, 0);
        calEnd.set(Calendar.SECOND, 0);
        long diferenciaDias = 0;
        // Calcular la diferencia en milisengundos
        long diff = calEnd.getTimeInMillis() - calStart.getTimeInMillis();
        // Calcular la diferencia en dias
        diferenciaDias = diff / (24 * 60 * 60 * 1000);
        // return diferenciaDias;
        Map<String, Object> mapa = new HashMap<String, Object>();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        mapa.put("dias", diferenciaDias);
        mapa.put("bisiesto", gregorianCalendar.isLeapYear(startDate.getYear()));
        return mapa;
    }

    public boolean validarRangoAnioFecha(Date fechaInicio, Date fechaFin) {

        boolean diferenciaAnio = false;
        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa = diferenciaDias(fechaInicio, fechaFin);
        long dias = (long) mapa.get("dias");
        boolean bisiesto = (boolean) mapa.get("bisiesto");
        if (bisiesto) {
            dias = dias - 1;
        }
        if (dias <= BaseConstants.DIAS_ANIO) {
            diferenciaAnio = true;
        }
        return diferenciaAnio;
    }

    public long validarRangoFecha(Date fechaInicio, Date fechaFin) {

        Map<String, Object> mapa = diferenciaDias(fechaInicio, fechaFin);
        return (long) mapa.get("dias");

    }

    public Integer sexoPorAgregadoMedico(String agregadoMedico) {

        return SexoEnum
                .parse(agregadoMedico.substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                .getClave();
    }

    public Date concatenarFechaHora(Date date, String hora) {

        String[] aux = hora.split(":");

        Calendar fecha = Calendar.getInstance();
        fecha.setTime(date);
        fecha.set(Calendar.HOUR_OF_DAY, Integer.parseInt(aux[0]));
        fecha.set(Calendar.MINUTE, Integer.parseInt(aux[1]));
        fecha.set(Calendar.SECOND, 0);
        return fecha.getTime();

    }

    public String converDateToString(Date fechaFinal) {

        String dateMask = "dd/MM/yyyy";
        if (fechaFinal == null) {
            return "";
        }
        SimpleDateFormat sFormat = new SimpleDateFormat(dateMask);
        return sFormat.format(fechaFinal);
    }

    public String converDateHoraToString(Date fechaFinal) {

        String dateMask = "dd/MM/yyyy HH:mm";
        if (fechaFinal == null) {
            return "";
        }
        SimpleDateFormat sFormat = new SimpleDateFormat(dateMask);
        return sFormat.format(fechaFinal);
    }

    public Date agregarDiasAFecha(Date date, int numeroDias) {

        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, numeroDias);
        return cal.getTime();

    }

    /**
     * Devuelve true si la fecha Inicial es mayor a la fecha final que se pasan
     * como parametro
     */
    public boolean comparaFechas(Date fechaInicial, Date fechaFinal) {

        Calendar fechaIni = Calendar.getInstance();
        fechaIni.setTime(mkDateIni(fechaInicial));
        Calendar fechaFin = Calendar.getInstance();
        fechaFin.setTime(mkDateIni(fechaFinal));
        if (fechaIni.getTimeInMillis() > fechaFin.getTimeInMillis()) {
            return true;
        }
        return false;
    }

    public Date mkDateIni(Date date) {

        if (date == null)
            return null;
        Calendar fecha = Calendar.getInstance();
        fecha.setTime(date);
        fecha.set(Calendar.HOUR_OF_DAY, 0);
        fecha.set(Calendar.MINUTE, 0);
        fecha.set(Calendar.SECOND, 0);
        fecha.set(Calendar.MILLISECOND, 0);
        return fecha.getTime();
    }

    public String convertDateToString(Date fecha) {

        String dateMask = "dd/MM/yyyy";
        if (fecha == null) {
            return "";
        }
        SimpleDateFormat sFormat = new SimpleDateFormat(dateMask);
        return sFormat.format(fecha);

    }

    public ReporteGenerado armarReporte(String clavePresupuestal, Date fechaGeneracion) {

        ReporteGenerado reporte = new ReporteGenerado();
        reporte.setCvePresupuestal(clavePresupuestal);
        reporte.setTipoReporte(TipoReporte.CAMAS_OCUPADAS_DOS_PACIENTES.getClave());
        reporte.setFecGeneracion(fechaGeneracion);
        return reporte;
    }

    public Date formatoFechaCorta(Date fecha) {

        String fechaActual = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(fecha);

        return convertStringToDate(fechaActual);
    }

    public Date formatoFecha(Date fecha) {

        String fechaActual = new SimpleDateFormat(BaseConstants.FORMATO_FECHA_HORA).format(fecha);

        return converStringFechaCompleta(fechaActual);
    }

    private Date converStringFechaCompleta(String fecha) {

        SimpleDateFormat formatter = new SimpleDateFormat(BaseConstants.FORMATO_FECHA_HORA);
        Date date = null;
        try {
            date = formatter.parse(fecha);
        } catch (ParseException e) {
            logger.error("Error al generar el parseo");
        }
        return date;
    }

    public List<PacienteModal> filtrarRecienNacidos(List<PacienteModal> recienNacidos) {

        List<PacienteModal> alojamiento = new ArrayList<>();
        for (PacienteModal pm : recienNacidos) {
            if (pm.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()) {
                alojamiento.add(pm);
            }
        }
        List<PacienteModal> cunero = new ArrayList<>();
        for (PacienteModal pm : recienNacidos) {
            if (pm.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue()) {
                cunero.add(pm);
            }
        }
        List<PacienteModal> ucin = new ArrayList<>();
        List<Long> cveUcin = new ArrayList<>();
        for (PacienteModal pm : recienNacidos) {
            if (pm.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_UCIN.getClave().intValue()) {
                ucin.add(pm);
                cveUcin.add(pm.getCvePaciente());
            }
        }
        List<PacienteModal> rn = new ArrayList<>();
        rn.addAll(alojamiento);
        rn.addAll(ucin);
        for (PacienteModal pm : cunero) {
            if (!cveUcin.contains(pm.getCvePaciente())) {
                rn.add(pm);
            }
        }
        return rn;
    }

}
