package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class TococirugiaEdicion {

	private int claveTipoNacido;
	private int claveTipoAtencion;
    private String tipoAtencion;
    private Date fechaAtencion;
    private int totalRecienNacidos;
    private int claveTipoParto;
    private String tipoParto;
    private int consecutivo;
    private int claveSexo;
    private String sexo;
    private int peso;
    private int talla;
    private int alta;
    private Long cvePaciente;

}
