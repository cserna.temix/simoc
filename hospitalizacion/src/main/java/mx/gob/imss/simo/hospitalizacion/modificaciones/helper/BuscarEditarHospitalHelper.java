package mx.gob.imss.simo.hospitalizacion.modificaciones.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoDiagnostioEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFechaEnum;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosEgreso;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngreso;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosTococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoDiagnosticoEdicion;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientosEdicion;
import mx.gob.imss.simo.hospitalizacion.common.model.TococirugiaEdicion;

@Component
public class BuscarEditarHospitalHelper extends HospitalizacionCommonHelper {
	
	  // Estado Recien Nacidos
    private static final String DEFUNCION = "DEF";
    private static final String EGRESADO = "SI";
    private static final String INGRESADO = "NO";
    private static final String MORTINATO = "MORT";
    private static final String SIN_INGRESO = "S/ING";

    public DatosPaciente buscarPaciente(DatosPaciente datosPacienteMock) {

        boolean igual = datosPacienteMock.getNss().equals("9209881241");
        if (igual) {
            datosPacienteMock.setNombre("NOMBRE");
            datosPacienteMock.setApellidoMaterno("MATERNO");
            datosPacienteMock.setApellidoPaterno("PATERNO");
            datosPacienteMock.setNumero("123");
            datosPacienteMock.setDelegacion("01 DELEGACION");
            datosPacienteMock.setClavePresupuestal("1234567890");
            datosPacienteMock.setDescripcionUnidad("UMF 01 DESCRIPCION UNIDAD");
            datosPacienteMock.setEdadAnios(30);
            datosPacienteMock.setEdadSemanas(1560);
            datosPacienteMock.setColorIdentificador(IdentificadorConexionEnum.VERDE.getRuta());
            datosPacienteMock.setSexo(2);
            datosPacienteMock.setClavePresupuestal("35A301182151");
            datosPacienteMock.setAgregadoMedico("1M2000OR");

            return datosPacienteMock;
        }
        return datosPacienteMock;
    }

    public String definirFocoBoton(PeriodosImss periodoActual, String idBottonEditar, String idBottonSalir) {

        // Definir el campo que utilizaremos para validar si el periodo esta
        // cerrado o no
        return idBottonEditar;
    }

    /**
     * Metodo para sumar una cantidad a la fecha de hoy segun sean meses o
     * semanas
     * 
     * @param fechaFormato
     * @param referencia
     * @param valor
     * @return
     */
    public Date sumarFechaSeccion(Date fechaFormato, String referencia, int valor) {

        Date fechaSumada = null;
        Calendar calendar = Calendar.getInstance();
        if (null != fechaFormato && null != referencia && valor > 0) {
            calendar.setTime(fechaFormato);
            if (referencia.equals(TipoFechaEnum.MES.getClave())) {
                calendar.add(Calendar.MONTH, valor);
                fechaSumada = calendar.getTime();
            } else if (referencia.equals(TipoFechaEnum.SEMANA.getClave())) {
                calendar.add(Calendar.DAY_OF_YEAR, valor);
                fechaSumada = calendar.getTime();
            }
        }
        return fechaSumada;
    }

    /**
     * @param ingreso
     * @param procedimientos
     * @param tococirugiaEdicion
     * @param egreso
     * @param egresosRn
     * @param movimiento
     * @return
     */
    public DatosHospitalizacion armarDatosEdicion(final Ingreso ingreso,
            final List<ProcedimientosEdicion> procedimientos, final List<TococirugiaEdicion> tococirugiaEdicion,
            final List<EgresoDiagnosticoEdicion> egreso, DatosPaciente paciente, List<String> ingresosRN,
            List<EgresoRecienNacido> egresosRn, MovimientoIntraHospitalario movimiento) {

        DatosHospitalizacion datos = new DatosHospitalizacion();
        datos.setDatosIngreso(crearDatosIngreso(ingreso, movimiento));
        datos.getDatosIntervenciones().setProcedimientos(crearProcedimientos(procedimientos));
        datos.setDatosTococirugia(crearDatosTococirugia(tococirugiaEdicion, ingresosRN, egresosRn));
        datos.setDatosEgreso(crearDatosEgreso(egreso));
        datos.setDatosPaciente(paciente);
        return datos;
    }

    private DatosEgreso crearDatosEgreso(List<EgresoDiagnosticoEdicion> egreso) {

        DatosEgreso datosEgreso = new DatosEgreso();
        if (null != egreso && !egreso.isEmpty()) {
            datosEgreso.setTipoFormato(egreso.get(0).getTipoFormato());
            datosEgreso.setFechaEgreso(egreso.get(0).getFechaEgreso());
            datosEgreso.setFechaEgresoS(convertDateToString(egreso.get(0).getFechaEgreso()));
            datosEgreso.setEspecialidadEgreso(egreso.get(0).getClaveEspecialidad());
            datosEgreso.setMotivoEgreso(egreso.get(0).getClaveMotivoEgreso());
            datosEgreso.setMotivoAlta(egreso.get(0).getClaveMotivoAlta());
            datosEgreso.setTipoEgreso(egreso.get(0).getClaveTipoEgreso());
            datosEgreso.setCveMetodoPlaniFamiliar(Integer.toString(egreso.get(0).getClavePlanificacionFamiliar()));
            datosEgreso.setCveInicialIngresoEgreso(
                    obtenerDiagnostico(egreso, TipoDiagnostioEnum.DIAGNOSTICO_INICIAL_INGRESO.getClave())
                            .getClaveDiagnostico());
            datosEgreso.setInicialIngresoEgreso(
                    obtenerDiagnostico(egreso, TipoDiagnostioEnum.DIAGNOSTICO_INICIAL_INGRESO.getClave())
                            .getDescripcionDiagnostico());
            datosEgreso.setCvePrincipalEgreso(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave())
                            .getClaveDiagnostico()));
            datosEgreso.setPrincipalEgreso(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave())
                            .getDescripcionDiagnostico()));
            datosEgreso.setCvePrimerDiagnosticoSecundario(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave())
                            .getClaveDiagnostico()));
            datosEgreso.setPrimerDiagnosticoSecundario(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave())
                            .getDescripcionDiagnostico()));
            datosEgreso.setClaveSegundoDiagnosticoSecundario(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.SEGUNDO_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave())
                            .getClaveDiagnostico()));
            datosEgreso.setSegundoDiagnosticoSecundario(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.SEGUNDO_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave())
                            .getDescripcionDiagnostico()));
            datosEgreso.setCveComplicacionIntraPrimera(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.PRIMERA_COMPLICACION_INTRAHOSPITALARIA.getClave())
                            .getClaveDiagnostico()));
            datosEgreso.setComplicacionIntraPrimera(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.PRIMERA_COMPLICACION_INTRAHOSPITALARIA.getClave())
                            .getDescripcionDiagnostico()));
            datosEgreso.setCveComplicacionIntraSegunda(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.SEGUNDA_COMPLICACION_INTRAHOSPITALARIA.getClave())
                            .getClaveDiagnostico()));
            datosEgreso.setComplicacionIntraSegunda(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.SEGUNDA_COMPLICACION_INTRAHOSPITALARIA.getClave())
                            .getDescripcionDiagnostico()));
            datosEgreso.setCveCausaDefDirecta(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.CAUSA_DIRECTA_DEFUNCION.getClave())
                            .getClaveDiagnostico()));
            datosEgreso.setCausaDefDirecta(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.CAUSA_DIRECTA_DEFUNCION.getClave())
                            .getDescripcionDiagnostico()));
            datosEgreso.setCveCausaDefBasica(
                    (obtenerDiagnostico(egreso, TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave())
                            .getClaveDiagnostico()));
            datosEgreso
                    .setCausaDefBasica((obtenerDiagnostico(egreso, TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave())
                            .getDescripcionDiagnostico()));
        }
        return datosEgreso;
    }

    private EgresoDiagnosticoEdicion obtenerDiagnostico(List<EgresoDiagnosticoEdicion> listEgreso, String clave) {

        EgresoDiagnosticoEdicion egreso = new EgresoDiagnosticoEdicion();
        egreso.setClaveDiagnostico("");
        egreso.setDescripcionDiagnostico("");
        for (EgresoDiagnosticoEdicion e : listEgreso) {
            if (clave.equals(e.getTipoDiagnostico())) {
                egreso = e;
            }
        }
        return egreso;
    }

    /**
     * @param tococirugiaEdicion
     * @param egresosRn
     * @return
     */
    private DatosTococirugia crearDatosTococirugia(List<TococirugiaEdicion> tococirugiaEdicion, List<String> ingresosRN,
            List<EgresoRecienNacido> egresosRn) {

        DatosTococirugia datosTococirugia = new DatosTococirugia();
        if (null != tococirugiaEdicion) {
            datosTococirugia.setAtencion(tococirugiaEdicion.get(0).getClaveTipoAtencion());
            datosTococirugia.setFecAtencion(convertDateToString(tococirugiaEdicion.get(0).getFechaAtencion()));
            datosTococirugia.setTotalNacidos(tococirugiaEdicion.get(0).getTotalRecienNacidos());
            datosTococirugia.setListDatosRecienNacido(obtenerDatosRecienNacidos(tococirugiaEdicion, ingresosRN, egresosRn));
            datosTococirugia.setTipoParto(tococirugiaEdicion.get(0).getClaveTipoParto());
            datosTococirugia.setTipoPartoString(datosTococirugia.getTipoParto().toString());
        }
        return datosTococirugia;
    }

    /**
     * @param tococirugia
     * @param egresosRn
     * @return
     */

    private List<DatosRecienNacido> obtenerDatosRecienNacidos(List<TococirugiaEdicion> tococirugia,
    		List<String> ingresosRN, List<EgresoRecienNacido> egresosRn) {

        List<DatosRecienNacido> listRN = new ArrayList<DatosRecienNacido>();
        List<String> egresos = new ArrayList<String>();
        for (EgresoRecienNacido ern : egresosRn) {
            egresos.add(String.valueOf(ern.getCvePaciente()));
        }
        for (TococirugiaEdicion recien : tococirugia) {
            DatosRecienNacido datosRN = new DatosRecienNacido();
            datosRN.setCvePaciente(recien.getCvePaciente());
            datosRN.setIdNacido(recien.getConsecutivo());
            datosRN.setSexo(recien.getClaveSexo());
            datosRN.setPeso(recien.getPeso());
            datosRN.setTalla(recien.getTalla());
            datosRN.setAlta(SIN_INGRESO);
            if (recien.getClaveTipoNacido() == 2) {
                datosRN.setAlta(DEFUNCION);
            }
            if (recien.getClaveTipoNacido() == 3) {
                datosRN.setAlta(MORTINATO);
            }
            if (ingresosRN.contains(String.valueOf(recien.getCvePaciente()))) {
                datosRN.setAlta(INGRESADO);
            }
            if (egresos.contains(String.valueOf(recien.getCvePaciente()))) {
                datosRN.setAlta(EGRESADO);
            }
            listRN.add(datosRN);

        }
        return listRN;
    }

    /**
     * @param procedimientos
     * @return
     */
    private List<Procedimientos> crearProcedimientos(List<ProcedimientosEdicion> procedimientos) {

        List<Procedimientos> listProcedimientos = new ArrayList<Procedimientos>();
        if (null != procedimientos && !procedimientos.isEmpty()) {
            for (ProcedimientosEdicion procedimientosEdicion : procedimientos) {
                boolean flag = Boolean.TRUE;
                for (Procedimientos pE : listProcedimientos) {
                    String consecutivo = pE.getConsecutivo() + "";
                    if (pE.getClave().equals(procedimientosEdicion.getClaveProcedimiento())
                            && consecutivo.equals(procedimientosEdicion.getConsecutivo())
                            && pE.getFechaIntervencionHoraMinuto()
                                    .equals(converDateHoraToString(procedimientosEdicion.getFechaProcedimiento()))) {
                        flag = Boolean.FALSE;
                        break;
                    }
                }
                if (flag) {
                    Procedimientos procedimientoIt = new Procedimientos();
                    procedimientoIt.setConsecutivo(Integer.parseInt(procedimientosEdicion.getConsecutivo()));
                    procedimientoIt.setClave(procedimientosEdicion.getClaveProcedimiento());
                    procedimientoIt.setClaveOriginal(procedimientosEdicion.getClaveProcedimiento());
                    procedimientoIt.setFechaIntervencion(procedimientosEdicion.getFechaIntervencion());
                    procedimientoIt.setDescripcion(procedimientosEdicion.getDescripcionProcedimiento());
                    procedimientoIt.setFechaIntervencionHoraMinuto(
                            converDateHoraToString(procedimientosEdicion.getFechaProcedimiento()));
                    procedimientoIt.setFechaProcedimientoCadena(
                            convertDateToString(procedimientosEdicion.getFechaProcedimiento()));
                    listProcedimientos.add(procedimientoIt);
                }
            }
        }
        return listProcedimientos;
    }

    /**
     * @param ingreso
     * @param movimiento
     * @return
     */
    private DatosIngreso crearDatosIngreso(final Ingreso ingreso, MovimientoIntraHospitalario movimiento) {

        DatosIngreso datosIngreso = new DatosIngreso();
        if (null != ingreso) {
            if (null != movimiento) {
                datosIngreso.setEspecialidad(movimiento.getEspecialidadOrigen());
            } else {
                datosIngreso.setEspecialidad(ingreso.getCveEspecialidadIngreso());
            }
            datosIngreso.setFechaIngreso(converDateToString(ingreso.getFecIngreso()));
            datosIngreso.setTipoIngreso(String.valueOf(ingreso.getCveTipoIngreso()));
        }
        return datosIngreso;
    }

    public DatosRecienNacido armarRN(DatosTococirugia datosTococirugia, int numero) {

        DatosRecienNacido rn = null;
        if (null != datosTococirugia.getListDatosRecienNacido()
                && !datosTococirugia.getListDatosRecienNacido().isEmpty()) {
            for (DatosRecienNacido dRn : datosTococirugia.getListDatosRecienNacido()) {
                if (dRn.getIdNacido() == numero) {
                    rn = new DatosRecienNacido();
                    rn.setIdNacido(dRn.getIdNacido());
                    rn.setSexo(dRn.getSexo());
                    rn.setPeso(dRn.getPeso());
                    rn.setTalla(dRn.getTalla());
                    rn.setAlta(dRn.getAlta());
                    rn.setCvePaciente(dRn.getCvePaciente());
                    break;
                }
            }
        }
        return rn;
    }

    /**
     * @param procedimientos
     * @return
     */
    public int buscarSiguienteFoco(List<Procedimientos> procedimientos) {

        for (Procedimientos procedimientosAuxiliar : procedimientos) {
            if (procedimientosAuxiliar.getClave() != procedimientosAuxiliar.getClaveOriginal()) {
                return procedimientosAuxiliar.getConsecutivo();
            }
        }

        return 0;
    }

    /**
     * @param procedimientos
     * @return
     */
    public Procedimientos crearProcedimiento(Procedimientos procedimientos) {

        Procedimientos procedimientosAux = new Procedimientos();
        procedimientosAux.setConsecutivo(procedimientos.getConsecutivo());
        procedimientosAux.setFechaIntervencion(procedimientos.getFechaIntervencion());
        procedimientosAux.setClave(procedimientos.getClave());
        procedimientosAux.setDescripcion(procedimientos.getDescripcion());
        procedimientosAux.setFechaIntervencionHoraMinuto(procedimientos.getFechaIntervencionHoraMinuto());

        return procedimientosAux;
    }

    public boolean validarActivarGuardar(Boolean[] requeridos) {

        for (boolean req : requeridos) {
            if (!req) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

}
