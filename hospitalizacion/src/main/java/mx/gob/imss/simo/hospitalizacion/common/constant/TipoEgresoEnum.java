package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoEgresoEnum {

    CANCELACION("0"),
    OTRA_UNIDAD("1"),
    UNIDAD_ADSCRIPCION("2"),
    DEFUNCION("4"),
    HOSPITALIZACION("5"),
	QUIROFANO("7");

    String valor;

    private TipoEgresoEnum(String valor) {
        this.valor = valor;
    }

    public String getValor() {

        return this.valor;
    }
    
}
