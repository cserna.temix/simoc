package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class MotivoAlta {

    private String cveMotivoAlta;
    private String descMotivoAlta;
    private Boolean isDefuncion;

}
