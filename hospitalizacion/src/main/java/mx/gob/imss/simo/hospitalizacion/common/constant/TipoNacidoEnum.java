package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoNacidoEnum {
    VIVO("1", "VIVO"), DEFUNCION("2", "DEFUNCION"), MORTINATO("3", "MORTINATO");

    private String clave;
    private String descripcion;

    private TipoNacidoEnum(String clave, String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

    public String getClave() {

        return clave;
    }

    public String getDescripcion() {

        return descripcion;
    }

    public static TipoNacidoEnum find(String clave) {

        TipoNacidoEnum right = null;
        for (TipoNacidoEnum item : values()) {
            if (item.getClave().equals(clave)) {
                right = item;
                break;
            }
        }
        return right;
    }
}
