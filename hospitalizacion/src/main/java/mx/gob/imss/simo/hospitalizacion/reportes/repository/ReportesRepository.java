/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.repository;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.EspecialidadesUnidadVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.NacLubchencoVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.NacidosVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.PartosVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitMaternoInfantil;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitMovEnfermo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitParteII;

/**
 * @author francisco.rodriguez
 */
public interface ReportesRepository {

    public Date obtenerUltimaFechaReportePacientes(Integer tipoReporte, String cvePresupuestal)
            throws HospitalizacionException;

    public List<String> obtenerClavesPresupuestales() throws HospitalizacionException;

    public void borrarRegistros(String cve_Presupuestal, String periodo) throws HospitalizacionException;

    public List<PeriodoOperacion> obtenerPeriodosRepParteII() throws HospitalizacionException;

    public List<String> obtenerClavesPresupuestalesParaCierre() throws HospitalizacionException;

    public Date obtenerUltimoPeriodoParteII(String cvePresupuestal) throws HospitalizacionException;
    
    public Integer obtenerRegistroPeriodoII(String cvePresupuestal, String cvePeriodoIMSS, String cveEspecialidad, Integer cveDivision);

    public Integer obtenerRegistroMovEnfermoPeriodo(String cvePresupuestal, String cvePeriodoIMSS);
    
    public Integer obtenerRegistroMovEnfermoAcumulado(String cvePresupuestal, String cvePeriodoIMSS);
    
    public Integer obtenerRegistroMatInfantilPeriodo(String cvePresupuestal, String cvePeriodoIMSS);
    
    public Integer obtenerRegistroMatInfantilAcumulado(String cvePresupuestal, String cvePeriodoIMSS);
    
    public Integer obtenerConteoDatosParteIIPeriodo(String cve_Presupuestal, String periodo)
            throws HospitalizacionException;

    /**
     * @param anioPeriodo
     * @param mesPeriodo
     * @return
     */
    PeriodosImss obtenerPeriodoImss(Integer anioPeriodo, Integer mesPeriodo);

    /**
     * @param cvePresupuestal
     * @return
     */
    String periodoMasActualAbierto(String cvePresupuestal);

    /**
     * @param cvePresupuestal
     * @return
     */
    List<EspecialidadesUnidadVo> especialidades(String cvePresupuestal);

    /**
     * @param cvePresupuestal
     * @return
     */
    List<EspecialidadesUnidadVo> especialidadIndPed(String cvePresupuestal);

    /**
     * @param cvePresupuestal
     * @param claveEspecialidad
     * @return
     */
    Integer camasPediatria(String cvePresupuestal, String claveEspecialidad);

    /**
     * @param cvePresupuestal
     * @param claveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer productividadPediatria(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);

    /**
     * @param cvePeriodoImss
     * @return
     */
    PeriodosImss obtenerPeriodoImssXcvePeriodoImss(String cvePeriodoImss);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaMedicos(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @return
     */
    Integer calculaCamas(String cvePresupuestal, String cveEspecialidad);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Double calculaDiasCamas(String cvePresupuestal, String cveEspecialidad, String fechaInicio, String fechaFin);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Double calculaDiasRango(Date fechaInicio, Date fechaFin);

    /**
     * @param cvePresupuestal
     * @return
     */
    Date ultimoDiaCerrado(String cvePresupuestal);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaIngresosProgramados(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaIngresosProgramadosAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */

    Integer calculaIngresosUrgentes(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaIngresosUrgentesAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaIngresosProgramadosOrigen(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaIngresosProgramadosOrigenAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaIngresosUrgentesOrigen(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    
    Integer calculaIngresosUrgentesOrigenAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaIngresosHospital(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaIngresosHospitalAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosCexterna(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosAlojamientoCexterna(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosUnidad(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosUnidadAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosHospitalMov(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosHospitalMovAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosHospitalHos(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosHospitalHosAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosOhp(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosOhpAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosDefuncion(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosDefuncionAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);
    
    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosPostMortemAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaEgresosPostMortem(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaTotalInterQuirurgica(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaTotInterQuirurCirugia(String cvePresupuestal, Date fechaInicio, Date fechaFin);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    Integer calculaInterQuirurgicaProg(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);

    /**
     * @param fechaInicio
     * @param cvePresupuestal
     * @return
     */
    Integer calculaIngresosAntAdul(Date fechaInicio, String cvePresupuestal);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @return
     */
    Integer calculaIngresosPeriodoAdul(Date fechaInicio, Date fechaFin, String cvePresupuestal);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @return
     */
    Integer calculaEgresosPeriodoAdul(Date fechaInicio, Date fechaFin, String cvePresupuestal);

    /**
     * @param fechaInicio
     * @param cvePresupuestal
     * @return
     */
    Integer calculaIngresosAntCunero(Date fechaInicio, String cvePresupuestal);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @return
     */
    Integer calculaIngresosPeriodoCunero(Date fechaInicio, Date fechaFin, String cvePresupuestal);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @return
     */
    Integer calculaEgresosPeriodoCunero(Date fechaInicio, Date fechaFin, String cvePresupuestal);

    /**
     * @param sitParteII
     */
    void insertarAcomulado(SitParteII sitParteII);

    /**
     * @param sitParteII
     */
    void insertarPeriodo(SitParteII sitParteII);

    /**
     * @param sitMovEnfermo
     */
    void insertarEnfermosAcomulado(SitMovEnfermo sitMovEnfermo);

    /**
     * @param sitMovEnfermo
     */
    void insertarEnfermosPeriodo(SitMovEnfermo sitMovEnfermo);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @return
     */
    List<Tococirugia> tocoCirugias(Date fechaInicio, Date fechaFin, String cvePresupuestal);

    /**
     * @param cveTocoCirugia
     * @return
     */
    PartosVo contadorPartos(Integer cveTocoCirugia);

    /**
     * @param cveTocoCirugia
     * @return
     */
    NacLubchencoVo contadorPreTerminoPost(Integer cveTocoCirugia);

    /**
     * @param cveTocoCirugia
     * @return
     */
    NacidosVo contadorNacidos(Integer cveTocoCirugia);

    /**
     * @param sitMaternoInfantil
     */
    void insertarMaternoInfantilAcomulado(SitMaternoInfantil sitMaternoInfantil);

    /**
     * @param sitMaternoInfantil
     */
    void insertarMaternoInfantilPeriodo(SitMaternoInfantil sitMaternoInfantil);

    /**
     * @param cvePresupuestal
     * @param cvePeriodoImss
     * @param cveProceso
     * @param cveEstadoProceso
     */
    void insertarInicioBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso,
            Integer cveEstadoProceso);

    /**
     * @param cvePresupuestal
     * @param cvePeriodoImss
     * @param cveProceso
     * @param cveEstadoProceso
     * @param reproceso
     */
    void updateBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso, Integer cveEstadoProceso,
            Boolean reproceso);

    /**
     * @param cvePresupuestal
     * @param cvePeriodoImss
     * @param cveProceso
     * @param cveEstadoProceso
     */
    void finBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso, Integer cveEstadoProceso);

    /**
     * @param cvePresupuestal
     * @param cvePeriodoImss
     */
    void deleteReporteDiario(String cvePresupuestal, String cvePeriodoImss);

    /**
     * @param cvePresupuestal
     * @param cvePeriodoImss
     */
    void deleteReporteAcumulado(String cvePresupuestal, String cvePeriodoImss);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @return
     */
    List<MovimientoIntraHospitalario> listaEspeMovimiento(Date fechaInicio, Date fechaFin, String cvePresupuestal,
            String cveEspecialidad);

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @return
     */
    List<Ingreso> listaEspeIngreso(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin);

    /**
     * @param cveEspecialidad
     * @param cveIngreso
     * @return
     */
    Integer diasPacienteMov(String cveEspecialidad, long cveIngreso);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @param cveIngreso
     * @return
     */
    Integer diasPacienteIngresoMov(Date fechaInicio, Date fechaFin, String cvePresupuestal, String cveEspecialidad,
            long cveIngreso);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param cveIngreso
     * @return
     */
    Integer diasPacienteIngresoEgreso(Date fechaInicio, Date fechaFin, String cvePresupuestal, String cveEspecialidad,
            long cveIngreso);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param cveIngreso
     * @return
     */
    Integer diasPacienteIngresoEgresoSinFec(Date fechaInicio, Date fechaFin, String cvePresupuestal,
            String cveEspecialidad, long cveIngreso);

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param cveIngreso
     * @return
     */
    Integer diasPacienteMovEgreso(Date fechaInicio, Date fechaFin, String cvePresupuestal, String cveEspecialidad,
            long cveIngreso);

    /**
     * @param cvePresupuestal
     * @param cvePeriodoImss
     * @param cveProceso
     * @return
     */
    Integer buscarBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso)
            throws HospitalizacionException;

    /**
     * @param cvePresupuestal
     * @param cvePeriodoImss
     * @param cveProceso
     * @param error
     * @throws HospitalizacionException
     */
    void updateBitacoraError(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso, String error)
            throws HospitalizacionException;

    /**
     * @param cvePresupuestal
     * @return
     * @throws HospitalizacionException
     */
    public String obtenerPeriodoParaCierre(String cvePresupuestal) throws HospitalizacionException;

    /**
     * @param cvePresupuestal
     * @return
     * @throws HospitalizacionException
     */
    public String obtenerPeriodoFinalizado(String cvePresupuestal) throws HospitalizacionException;

}
