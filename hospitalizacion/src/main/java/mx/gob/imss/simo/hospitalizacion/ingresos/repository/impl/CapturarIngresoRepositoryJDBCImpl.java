package mx.gob.imss.simo.hospitalizacion.ingresos.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SecuenciasEnum;
import mx.gob.imss.simo.hospitalizacion.common.helper.DatosIngresoRNRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.EspecialidadRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.EspecialidadUnidadRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.IngresoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.PacienteRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.ParametroRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.EspecialidadUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.hospitalizacion.ingresos.repository.CapturarIngresoRepository;

@Repository("capturarIngresoRepository")
public class CapturarIngresoRepositoryJDBCImpl extends HospitalizacionCommonRepositoryJDBCImpl
        implements CapturarIngresoRepository {

    @Override
    public long guardarIngreso(Ingreso ingreso) {

        final long secuencia = obtenerSiguienteValorSecuencia(SecuenciasEnum.INGRESO);
        jdbcTemplate.update(SQLConstants.GUARDAR_INGRESO,
                new Object[] { secuencia, ingreso.getCveIngresoPadre(), ingreso.getFecCreacion(),
                        ingreso.getCvePaciente(), ingreso.getCvePresupuestal(), ingreso.getCveEspecialidadIngreso(),
                        ingreso.getCveTipoFormato(), ingreso.getCveCapturista(), ingreso.getFecIngreso(),
                        ingreso.getCveCama(), ingreso.getCveEspecialidadCama(), ingreso.getRefCamaRn(),
                        ingreso.getCveTipoIngreso(), ingreso.getCveTipoPrograma(), ingreso.getCveMedico(),
                        ingreso.isIndExtemporaneo(), ingreso.isIndEgreso(), ingreso.isIndTococirugia(),
                        ingreso.isIndIntervencionQx(), ingreso.isIndMovIntrahospitalario(), ingreso.getCveEspecialidadIngreso() });
        return secuencia;
    }

    @Override
    public List<DatosIngresoRN> buscarRecienNacidosNss(String nss, String clavePresupuestal) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_RECIEN_NACIDOS_NSS, new Object[] { nss, clavePresupuestal },
                new DatosIngresoRNRowMapperHelper());
    }

    @Override
    public List<DatosIngresoRN> buscarRecienNacidosCama(String cama, String clavePresupuestal) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_RECIEN_NACIDOS_CAMA, new Object[] { cama, clavePresupuestal },
                new DatosIngresoRNRowMapperHelper());
    }

    @Override
    public List<DatosIngresoRN> buscarRecienNacidosIngresoVigenteCuneroNoVigenteAlojamiento(String nss,
            String clavePresupuestal) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_RECIEN_NACIDOS_INGRESO_VIGENTE_CUNERO_NO_VIGENTE_ALOJAMIENTO,
                new Object[] { nss, clavePresupuestal }, new DatosIngresoRNRowMapperHelper());
    }

    @Override
    public List<DatosIngresoRN> buscarRecienNacidosCamaIngresoVigenteCuneroNoVigenteAlojamiento(String cama,
            String clavePresupuestal) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_RECIEN_NACIDOS_CAMA_INGRESO_VIGENTE_CUNERO_NO_VIGENTE_ALOJAMIENTO,
                new Object[] { cama, "B" + cama, clavePresupuestal }, new DatosIngresoRNRowMapperHelper());
    }

    @Override
    public List<Ingreso> buscarIngresoVigenteCunero(long clavePaciente) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_VIGENTE_CUNERO_UCIN,
                new Object[] { clavePaciente, BaseConstants.ESPECIALIDAD_CUNERO_PATOLOGICO, 0 },
                new IngresoRowMapperHelper());
    }
    
    @Override
    public List<Ingreso> buscarIngresoVigenteCuneroPatologico(long clavePaciente) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_VIGENTE_CUNERO_PATOLOGICO,
                new Object[] { clavePaciente, BaseConstants.CLAVE_CUNERO_PATOLOGICO, 0 },
                new IngresoRowMapperHelper());
    }

    @Override
    public List<Ingreso> buscarIngresoNoVigenteCunero(long clavePaciente) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_VIGENTE_CUNERO_UCIN,
                new Object[] { clavePaciente, BaseConstants.ESPECIALIDAD_CUNERO_PATOLOGICO, 1 },
                new IngresoRowMapperHelper());
    }

    @Override
    public List<Ingreso> buscarIngresoVigenteUcin(long clavePaciente) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_VIGENTE_CUNERO_UCIN,
                new Object[] { clavePaciente, BaseConstants.ESPECIALIDAD_UCI_NEONATOS, 0 },
                new IngresoRowMapperHelper());
    }

    @Override
    public void actualizarIndicadorRecienNacido(Long claveTococirugia, Integer numeroRecienNacido) {

        jdbcTemplate.update(SQLConstants.ACTUALIZAR_INDICADOR_RECIEN_NACIDO,
                new Object[] { claveTococirugia, numeroRecienNacido });
    }

    @Override
    public Paciente buscarPaciente(Long clavePaciente) {

        return jdbcTemplate.queryForObject(SQLConstants.BUSCAR_PACIENTE_POR_CLAVE_PACIENTE,
                new Object[] { clavePaciente }, new PacienteRowMapperHelper());
    }
    
    public Boolean validarEspecialidadIndPediaria(String claveEspecialidad, String cvePresupuestal) {
    	
        EspecialidadUnidad especialidad = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_IND_PEDIATRIA_CVE_ESPECIALIDAD_PRESUPUESTAL,
                new Object[] { claveEspecialidad, cvePresupuestal }, new EspecialidadUnidadRowMapperHelper());
        return especialidad.isIndPediatria();
    }
    
    public int obtenerNumSemanasMaxPediatria(String claveEspecialidad) {
    	
    	Especialidad especialidad = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_NUM_MAXIMO_SEMANAS_PEDIATRIA,
                new Object[] { claveEspecialidad }, new EspecialidadRowMapperHelper());
    	return especialidad.getNumEdadMaximaSemana();
    }
    
    public void actualizarCamaRecienNacido(String refCamaRN, String cveEspecialidadCama, Long cveIngreso) {
    	
    	jdbcTemplate.update(SQLConstants.ACTUALIZAR_CAMA_RECIEN_NACIDOS,
                new Object[] { refCamaRN, cveEspecialidadCama, cveIngreso });
    	
    }
    
    public void actualizarNumRecienNacido(int numRecienNacido,Long cveIngreso) {
    	
    	if(numRecienNacido > 0){
	    	jdbcTemplate.update(SQLConstants.ACTUALIZAR_NUM_RECIEN_NACIDO,
	                new Object[] { numRecienNacido,cveIngreso });
    	}
    }
    
    public void actualizarEdadPaciente(Long cvePaciente, int edadSemanas, Integer edadAnios){
    	
    	jdbcTemplate.update(SQLConstants.ACTUALIZAR_EDAD_PACIENTE,
                new Object[] { edadSemanas, edadAnios, cvePaciente });
    }
    
    public String buscarPeriodoLiberacion(String cve_Parametro) {		
	
			Parametro periodo_liberacion = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_PERIODO_LIBERACION,
	                new Object[] { cve_Parametro }, new ParametroRowMapperHelper());
			
			return periodo_liberacion.getReferenciaParametro();
	}


}
