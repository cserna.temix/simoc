package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.RecienNacido;

public class RecienNacidoRowMapperHelper extends BaseHelper implements RowMapper<RecienNacido> {

    @Override
    public RecienNacido mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        RecienNacido recienNacido = new RecienNacido();
        recienNacido.setClave(resultSet.getLong(SQLColumnasConstants.CVE_TOCOCIRUGIA));
        recienNacido.setNumeroRecienNacido(resultSet.getInt(SQLColumnasConstants.NUM_RECIEN_NACIDO));
        recienNacido.setFechaCreacion(resultSet.getDate(SQLColumnasConstants.FEC_CREACION));
        recienNacido.setFechaActualizacion(validarFechaNula(resultSet, SQLColumnasConstants.FEC_ACTUALIZACION));
        recienNacido.setPeso(resultSet.getInt(SQLColumnasConstants.NUM_PESO));
        recienNacido.setTalla(resultSet.getInt(SQLColumnasConstants.NUM_TALLA));
        recienNacido.setPerimetroCefalico(resultSet.getInt(SQLColumnasConstants.NUM_PERIMETRO_CEFALICO));
        recienNacido.setSemanasGestacion(resultSet.getInt(SQLColumnasConstants.NUM_SEM_GESTACION));
        recienNacido.setApgar1(resultSet.getInt(SQLColumnasConstants.NUM_APGAR_1MINUTO));
        recienNacido.setApgar5(resultSet.getInt(SQLColumnasConstants.NUM_APGAR_5MINUTO));
        recienNacido.setFechaDefuncion(validarFechaNula(resultSet, SQLColumnasConstants.FEC_DEFUNCION));
        recienNacido.setClaveCapturista(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA));
        recienNacido.setClaveCapturistaActualiza(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA_ACTUALIZA));
        recienNacido.setClavePaciente(resultSet.getInt(SQLColumnasConstants.CVE_PACIENTE));
        recienNacido.setIndicadorIngresoRN(resultSet.getInt(SQLColumnasConstants.IND_INGRESO_RN));
        recienNacido.setClaveTipoNacido(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_NACIDO));
        recienNacido.setClaveRangoLubchenco(resultSet.getInt(SQLColumnasConstants.CVE_RANGO_LUBCHENCO));
        return recienNacido;
    }

}
