package mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;

public interface CapturarMovimientosIntrahospitalariosRepository extends HospitalizacionCommonRepository {

    DatosCatalogo buscarCamaPorCvePresupuestalYCveCama(String cveCama, String cvePresupuestal);

    void guardar(MovimientoIntraHospitalario datosMovimientos);

    DatosCatalogo buscarDivisionPorCveDivision(int cveDivision);

    void actualizarIngresoPorMovimientoIntraHospitalario(long cveIngreso, MovimientoIntraHospitalario movimiento);
    
    void actualizarIngresoPorMovimientoIntraHospitalarioRN(long cveIngreso, MovimientoIntraHospitalario movimiento, 
    		Integer cveTipoFormato);
    
    void actualizarIndMovIntrahospitalario(long claveIngreso);
    
    List<Ingreso> buscarIngresoVigenteUCIN(long claveIngreso);
    
    List<Ingreso> buscarIngresoAlojamiento(String nss, String cvePresupuestal);
    
    void actualizaCamaAlojamiento(String camaNueva, Ingreso ingreso);
    
    Boolean esRecienNacidoExterno(long clavePaciente,String cvePresupuestal);
    
    List<Ingreso> buscarIngresoMadrePorToco(long clavePaciente,String cvePresupuestal);
    
    List<Ingreso> buscarIngresoMadre(String clavePresupuestal, Long clavePaciente,
    		List<Integer> tiposFormato);
    
    int obtenerNumSemanasMaxPediatria(String claveEspecialidad);
    
    List<CamaUnidad> buscarCamaUnidadMovIntra(String clavePresupuestal, String claveCama);

}
