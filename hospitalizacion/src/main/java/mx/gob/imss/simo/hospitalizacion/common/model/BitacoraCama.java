package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class BitacoraCama {

    private long cveBitacoraCama;
    private String cveCama;
    private String cveEspecialidad;
    private String cvePresupuestal;
    private int cveTipoFormato;
    private Date fecEvento;
    private boolean indOcupada;

}
