/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.EspecialidadesUnidadVo;

/**
 * @author francisco.rodriguez
 *
 */
public interface ProcesarParteIIService extends HospitalizacionCommonServices {

	/**
	 * @param cvePresupuestal
	 * @param tipoReporte
	 * @return
	 */
	Integer procesarPrincial(String cvePresupuestal, Integer tipoReporte) throws HospitalizacionException;

	/**
	 * @param cvePresupuestal
	 * @return
	 */
	Integer procesarCierre(String cvePresupuestal);
		
	/**
	 * @param cvePresupuestal
	 * @return
	 * @throws ParseException
	 */
	Integer procesarAcomulado(String cvePresupuestal) throws ParseException;

	/**
	 * @param cvePresupuestal
	 * @return
	 */
	Integer procesarPeriodo(String cvePresupuestal);
	
	/**
	 * @param cvePresupuestal
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	List<EspecialidadesUnidadVo> agregarEspecialidadPediatrica(String cvePresupuestal, Date fechaInicial, Date fechaFinal);
}
