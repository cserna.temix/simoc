package mx.gob.imss.simo.hospitalizacion.seguridad.service;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.PersonalOperativo;
import mx.gob.imss.simo.hospitalizacion.common.model.Rol;

public interface PersonalOperativoService {

    PersonalOperativo obtenerPersonalOperativoPorUsuarioAD(String usuario);

    List<Rol> obtenerRolesPorUsuarioAD(String usuario);

    void actualizarIntento(String usuario, Integer intento);

    void actualizarBloqueo(String usuario, Integer intento);
}
