package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.MetodoAnticonceptivo;

public class MetodoAnticonceptivoRowMapperHelper extends BaseHelper implements RowMapper<MetodoAnticonceptivo> {

    @Override
    public MetodoAnticonceptivo mapRow(ResultSet resultSet, int rowId) throws SQLException {

        MetodoAnticonceptivo metodoAnticonceptivo = new MetodoAnticonceptivo();

        metodoAnticonceptivo.setClave(resultSet.getInt(SQLColumnasConstants.CVE_METODO_ANTICONCEPTIVO));
        metodoAnticonceptivo.setDescripcion(resultSet.getString(SQLColumnasConstants.DES_METODO_ANTICONCEPTIVO));
        metodoAnticonceptivo.setNumeroEdadInicial(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_INICIAL));
        metodoAnticonceptivo.setNumeroEdadFinal(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_FINAL));
        metodoAnticonceptivo.setClaveSexo(resultSet.getInt(SQLColumnasConstants.CVE_SEXO));
        metodoAnticonceptivo.setCantidad(resultSet.getInt(SQLColumnasConstants.CAN_MAXIMA_METODO));
        metodoAnticonceptivo.setFechaBaja(resultSet.getDate(SQLColumnasConstants.FEC_BAJA));
        return metodoAnticonceptivo;
    }

}
