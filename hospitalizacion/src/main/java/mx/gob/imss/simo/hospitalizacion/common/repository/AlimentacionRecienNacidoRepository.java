package mx.gob.imss.simo.hospitalizacion.common.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.AlimentacionRecienNacido;

public interface AlimentacionRecienNacidoRepository extends BaseRepository {

    void guardar(final AlimentacionRecienNacido alimentacionRecienNacido);

    List<AlimentacionRecienNacido> buscar();
}
