package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

public class BaseHelper {

    protected final Logger logger = Logger.getLogger(getClass());

    public String[] getArray(String... strings) {

        return strings;
    }

    public Date validarFechaNula(ResultSet resultSet, String nombreColumna) throws SQLException {

        return null == resultSet.getDate(nombreColumna) ? null : resultSet.getDate(nombreColumna);
    }

}
