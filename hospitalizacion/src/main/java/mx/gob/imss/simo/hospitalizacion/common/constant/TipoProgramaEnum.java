package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoProgramaEnum {

    HOSPITAL("1", "Hospital"),
    CIRUGIA_AMBULATORIA("2", "Cirug\u00EDa ambulatoria");

    private String clave;
    private String descripcion;

    private TipoProgramaEnum(String clave, String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

    public String getClave() {

        return clave;
    }

    public String getDescripcion() {

        return descripcion;
    }

    public static TipoProgramaEnum parse(Integer clave) {

        TipoProgramaEnum right = null;
        for (TipoProgramaEnum item : values()) {
            if (item.getClave().equals(clave)) {
                right = item;
                break;
            }
        }
        return right;
    }

}
