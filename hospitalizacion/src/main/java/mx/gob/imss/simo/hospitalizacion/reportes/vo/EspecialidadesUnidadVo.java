/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.vo;

/**
 * @author francisco.rodriguez
 *
 */
public class EspecialidadesUnidadVo{

	public Integer cveDivision;
	public String desDivision;
	public String cveEspecialidad;
	public String desEspecialidad;
	public String desUnidadMedica;

	/**
	 * @return the cveDivision
	 */
	public Integer getCveDivision() {
		return cveDivision;
	}

	/**
	 * @param cveDivision
	 *            the cveDivision to set
	 */
	public void setCveDivision(Integer cveDivision) {
		this.cveDivision = cveDivision;
	}

	/**
	 * @return the desDivision
	 */
	public String getDesDivision() {
		return desDivision;
	}

	/**
	 * @param desDivision
	 *            the desDivision to set
	 */
	public void setDesDivision(String desDivision) {
		this.desDivision = desDivision;
	}

	/**
	 * @return the cveEspecialidad
	 */
	public String getCveEspecialidad() {
		return cveEspecialidad;
	}

	/**
	 * @param cveEspecialidad
	 *            the cveEspecialidad to set
	 */
	public void setCveEspecialidad(String cveEspecialidad) {
		this.cveEspecialidad = cveEspecialidad;
	}

	/**
	 * @return the desEspecialidad
	 */
	public String getDesEspecialidad() {
		return desEspecialidad;
	}

	/**
	 * @param desEspecialidad
	 *            the desEspecialidad to set
	 */
	public void setDesEspecialidad(String desEspecialidad) {
		this.desEspecialidad = desEspecialidad;
	}

	/**
	 * @return the desUnidadMedica
	 */
	public String getDesUnidadMedica() {
		return desUnidadMedica;
	}

	/**
	 * @param desUnidadMedica
	 *            the desUnidadMedica to set
	 */
	public void setDesUnidadMedica(String desUnidadMedica) {
		this.desUnidadMedica = desUnidadMedica;
	}

}
