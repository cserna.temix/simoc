package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ConsultaStringRowMapperHelper extends BaseHelper implements RowMapper<String> {

    @Override
    public String mapRow(ResultSet resultSet, int rowId) throws SQLException {

        return resultSet.getString(1);
    }

}
