/**
 * ReporteParteIIController.java
 */
package mx.gob.imss.simo.hospitalizacion.reportes.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;
import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PlantillaJasperAcumuladoParteII;
import mx.gob.imss.simo.hospitalizacion.common.constant.PlantillaJasperParteII;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoReporte;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.reportes.services.ProcesarParteIIService;
import mx.gob.imss.simo.hospitalizacion.reportes.services.ReportesServices;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * Clase que tiene como funcion de controlador de la pagina en donde se
 * realiza el cierre de mes y el informe de servicios medicos parte II. La forma en la
 * que se visualizan los reportes es por medio de cierre de mes y por periodo, es decir, por
 * fechas que calculan por medio de la fecha inicial del periodo y la ultima fecha en la que se genero el reporte 
 * de pacientes hospitalizados
 * 
 * 18-04-2017. REPORTE POR PERIODO: Se elimina el llamado al dialog que solicitaba la captura de la fecha
 * 			   inicial y la fecha final. La fecha inicial se recupera de la fecha inicial del periodo activo
 * 			   y la fecha final se recupera de la ultima fecha que se genero el reporte de pacientes 
 * 			   hospitalizados. Se muestra mensaje en caso de que le fecha inicial sea mayor que la fecha final. * 			   		
 * 
 *  		   REPORTE CIERRE DE MES: Se modifica el mensaje que se muestra al usuario para que aparezca el periodo
 *  		   en el mensaje que indica que se va a cerrar el periodo. El reporte se obtendra del boton de descargar que 
 *  		   aparece en la pagina.			   
 *  	
 * @author softtek
 * @version 2.0.0
 */
@ViewScoped
@ManagedBean(name = "reporteParteIIController")
public class ReporteParteIIController extends HospitalizacionCommonController {
	
    private static final long serialVersionUID = 1L;
    
    private String fechaInicial = "";
    private String fechaFinal  = "";

    private static final String ERROR_REPORTE = "Error al crear el reporte parte II: ";
    private static final String UNIDAD_MEDICA = "UNIDAD_MEDICA";
    private static final String NOMBRE_UNIDAD = "NOMBRE_UNIDAD";
    private static final String LOGO_IMSS = "LOGO_IMSS";
    private static final String PERIODO = "PERIODO";
    private static final String PERFIL = "PERFIL";
    private static final String PERIODO_IMSS = "PERIODO_IMSS";
    private static final String PATH_REPORTE_PARTEII_PERIODO = "/WEB-INF/reportes/reporteParteII/reportePeriodo/";
    private static final String PATH_REPORTE_PARTEII_CIERRE = "/WEB-INF/reportes/reporteParteII/reporteCierre/";
    private static final String PATH_REPORTE_PARTEII_CIERRE_ACUMULADO = "/WEB-INF/reportes/reporteParteII/reporteAcumulado/";
    private static final Integer HOSPITALIZACION = 2;
    
    private static final String ARCHIVOS_JASPER_DE_PERIODO = "periodo"; 
    private static final String ARCHIVOS_JASPER_DE_CIERRE = "cierre";
    private static final String ARCHIVOS_JASPER_DE_ACUMULADO = "acumulado";

    @ManagedProperty("#{reportesServices}")
    private transient ReportesServices reportesServices;

    @ManagedProperty("#{reporteParteDosPeriodo}")
    private transient AbstractJasperReportsSingleFormatView plantillareporteParteDosPeriodo;
    
    @ManagedProperty("#{reporteParteDosCierre}")
    private transient AbstractJasperReportsSingleFormatView plantillareporteParteDosCierre;

    private SimpleDateFormat form = new SimpleDateFormat(BaseConstants.FORMATO_FECHA);

    private transient PeriodosImss periodoImss = null;
    
    @ManagedProperty("#{procesarParteIIService}")
    private ProcesarParteIIService procesarParteIIService;
    
    @Override
    @PostConstruct
    public void init() {
        super.init();                
    }

    /**
     * Metodo que realiza el proceso del render para el reporte; asi como el proceso del paso
     * de parametros a las plantillas para la obtencion del reporte
     * 
     * @param cierre bandera que indica si el proceso que se va a realizar es el de cierre
     * 				o en su defecto por el de periodo
     * @return bandera que indica si se genero el reporte 
     * @throws Exception en caso de que exista error en el proceso de el render del reporte
     */
    public boolean fileXlsSpring(boolean cierre) throws Exception {

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        final ServletContext servletContext = (ServletContext) context.getContext();
        final Map<String, Object> model = new HashMap<>();
        boolean reporteGenerado;
                
        model.put( PERFIL, obtenerDatosUsuario().getPerfil() );
        model.put( UNIDAD_MEDICA, obtenerDatosUsuario().getCvePresupuestal() );        
        model.put( NOMBRE_UNIDAD, obtenerDatosUsuario().getDesUnidadMedicaAct().concat(" ").concat(obtenerDatosUsuario().getDesDelegacionAct()));       
        model.put( LOGO_IMSS, obtenerLogoImss() );        

        if (cierre) {
        	
        	// Se obtienen las plantillas de los reportes   
            for( PlantillaJasperParteII subreporte : PlantillaJasperParteII.values() ){
            	model.put( subreporte.getParametro(), obtenerSubreporteJasper( subreporte.getNombreArchivo(), servletContext, ARCHIVOS_JASPER_DE_CIERRE ) );
            }
            
            // Se obtienen las plantillas para el acumulado
            for( PlantillaJasperAcumuladoParteII subreporte : PlantillaJasperAcumuladoParteII.values() ){
            	model.put( subreporte.getParametro(), obtenerSubreporteJasper( subreporte.getNombreArchivo(), servletContext, ARCHIVOS_JASPER_DE_ACUMULADO ) );
            }
            String periodoFinalizado = reportesServices.obtenerPeriodoFinalizado( obtenerDatosUsuario().getCvePresupuestal() ); 
            logger.debug("CLAVE PRESUPUESTALo%%%%: " +obtenerDatosUsuario().getCvePresupuestal());
            logger.debug("periodoFinalizado%%%%: " +periodoFinalizado);
            
            model.put( PERIODO_IMSS, periodoFinalizado );  
            model.put(PERIODO, obtenerDescripcionPeriodo(periodoFinalizado) );
            model.put("PERIODICIDAD", "MENSUAL");    	
            
            plantillareporteParteDosCierre.render( model, 
									    		   (HttpServletRequest) context.getRequest(),
									               (HttpServletResponse) context.getResponse());
        } else {
        	// Se obtienen las plantillas de los reportes   
            for( PlantillaJasperParteII subreporte : PlantillaJasperParteII.values() ){
            	model.put( subreporte.getParametro(), obtenerSubreporteJasper( subreporte.getNombreArchivo(), servletContext, ARCHIVOS_JASPER_DE_PERIODO ) );
            }
            
            model.put( PERIODO_IMSS, getObjetosSs().getPeriodoActual() );            
            model.put( PERIODO, "PERIODO DEL ".concat( this.fechaInicial ).concat( " AL " ).concat( this.fechaFinal ) );
            plantillareporteParteDosPeriodo.render( model, 
								            		(HttpServletRequest) context.getRequest(),
								                    (HttpServletResponse) context.getResponse() );
        }
        
        reporteGenerado = true;
        return reporteGenerado;
    }
    
    private String obtenerDescripcionPeriodo( String fecha ){
    	String anioPeriodoActual = fecha.substring( 0, 4 );
		String mesPeriodoActual = fecha.substring( 4 );
		return descripcionMes( Integer.parseInt(mesPeriodoActual)) + " DEL " + anioPeriodoActual;
    }
    
    /**
     * Metodo que obtiene el logo del imss para el reporte
     * @return ruta del logo del imss para el encabezado
     */
    private String obtenerLogoImss(){    	
    	return ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getRealPath(File.separator + "img" + File.separator + "imss_header.png");
    }

    /**
     * Metodo que realiza la busqueda del archivo que se manda como parametro, de acuerdo al proceso 
     * que se requiere realizar
     * @param nombreSubreporte nombre del archivo que se requiere buscar
     * @param servletContext objeto de tipo <code>ServletContext</code> para obtener el path
     * @param proceso nombre del proceso que se esta realizando (cierre, proceso, periodo)
     * @return objeto de tipo <code>JasperReport</code> que contiene la version del archivo que se 
     * 			pasara como parametro a la plantilla de jasper report 
     * @throws JRException en caso de que no se pueda realizar el proceso del casteo para <code>JasperReport</code> 
     */
    private JasperReport obtenerSubreporteJasper(String nombreSubreporte, ServletContext servletContext, String proceso)
            throws JRException {
    	
    	switch( proceso ){
    		case ARCHIVOS_JASPER_DE_CIERRE:
    			return (JasperReport) JRLoader.loadObjectFromFile(servletContext.getRealPath(PATH_REPORTE_PARTEII_CIERRE + nombreSubreporte));
    		case ARCHIVOS_JASPER_DE_PERIODO:
    			return (JasperReport) JRLoader.loadObjectFromFile(servletContext.getRealPath(PATH_REPORTE_PARTEII_PERIODO + nombreSubreporte));    			
    		case ARCHIVOS_JASPER_DE_ACUMULADO:
    			return (JasperReport) JRLoader.loadObjectFromFile(servletContext.getRealPath(PATH_REPORTE_PARTEII_CIERRE_ACUMULADO + nombreSubreporte));
    	}
    	return null;
    }
    
    /**
     * Metodo que valida la fecha de cierre de acuerdo a la fecha en la que se genero el reporte
     * de pacientes hospitalizados y de acuerdo a esa fecha se obtendra el periodo y se validara 
     * si el periodo ya esta cerrado.   
     */
    public void validaCierreMensual(){
    	 boolean periodoValidoCerrado;
         Calendar fechaPacientesC = Calendar.getInstance();
         RequestContext context = RequestContext.getCurrentInstance();
         logger.debug("Clave presupuestal: " + obtenerDatosUsuario().getCvePresupuestal());
         Date fechaPacientes = null;
         // Se obtiene la fecha en la que se genero el ultimo reporte de pacientes
         try {
        	 fechaPacientes = reportesServices.obtenerUltimaFechaReportePacientes( TipoReporte.PACIENTES_HOSPITALIZADOS.getClave(), 
																						obtenerDatosUsuario().getCvePresupuestal() );        	
        	 /*
              *  Se establece la fecha ultima en la que se genero el reporte de pacientes y se obtiene el periodo
              *  de la fecha que se obtuvo
              */   
             String cvePeriodoImss = reportesServices.periodoMasActualAbierto(obtenerDatosUsuario().getCvePresupuestal());    		
     		 this.periodoImss = reportesServices.obtenerPeriodoImssXcvePeriodoImss(cvePeriodoImss);
     		 
     		//Se crean Calendar para darle el formato igual a ambas fechas
     		fechaPacientesC.setTime(fechaPacientes);
     		fechaPacientes=fechaPacientesC.getTime();
     		 
     		Calendar fechaFinalC = Calendar.getInstance();
     		fechaFinalC.setTime(periodoImss.getFechaFinal());
     		periodoImss.setFechaFinal(fechaFinalC.getTime());

     		if(fechaPacientes.equals(periodoImss.getFechaFinal())){
                                
	             periodoValidoCerrado = reportesServices.validaPeriodoCerrado( obtenerDatosUsuario().getCvePresupuestal(), 
	    																	   HOSPITALIZACION,
	    																	   this.periodoImss.getClavePeriodo());
	    		if (periodoValidoCerrado) {
	    			agregarMensajeError(getArray(MensajesErrorConstants.MG_O24), null);
	    		} else {
	                context.update("repParte2:dlgCierreMes");
	                context.execute("PF('dlgCierre').show();");
	    		}
     		} else{
     			agregarMensajeError(getArray(MensajesErrorConstants.ME_502), null);
     		}
		 } catch (HospitalizacionException e) {
	 		logger.error( "Error: " + e.getMessage() );
		 }
         
         
    }
    
    public void generarCierreDeMes() {

        logger.debug("INICIA generarCierreDeMes");
        boolean periodoValidoCerrado;        
        RequestContext context = RequestContext.getCurrentInstance();
        Integer bitacora = 0;
        PeriodoOperacion operacion = null;
        
        try {
        	        	
        	logger.info(">>>>>>>>>>>>Periodo: " + this.periodoImss.getClavePeriodo()+" CvePresupuestal:"+obtenerDatosUsuario().getCvePresupuestal()); 
        	
            operacion = reportesServices.buscarPeriodoAbierto( obtenerDatosUsuario().getCvePresupuestal(),
            												   HOSPITALIZACION, 
                    										   this.periodoImss.getClavePeriodo());
            if (operacion == null) {
                long resul = reportesServices.insertarPeriodoOperacion( obtenerDatosUsuario().getCvePresupuestal(),  
                														HOSPITALIZACION,
                														this.periodoImss.getClavePeriodo());
                if (resul == 0) {
                    agregarMensajeError(getArray(MensajesErrorConstants.ME_501), null);
                }
            }
 
            periodoValidoCerrado = reportesServices.validaPeriodoCerrado( obtenerDatosUsuario().getCvePresupuestal(), 
            															  HOSPITALIZACION,
            															  this.periodoImss.getClavePeriodo());
            
            if (periodoValidoCerrado) {
                agregarMensajeError(getArray(MensajesErrorConstants.MG_O24), null);
            } else {
            	
            	logger.info("Inicio del proceso... " + new SimpleDateFormat("dd/mm/yyyy HH:mm:ss").format(new Date()));
            	try {
					bitacora  = reportesServices.buscarBitacora( obtenerDatosUsuario().getCvePresupuestal(), getObjetosSs().getPeriodoActual(), 2);
				} catch (HospitalizacionException e) {				
					logger.error("Ocurrio un error al buscar el registro en la bitacora");
					e.printStackTrace();
				}
            	
				if(bitacora > 0){				
					reportesServices.updateBitacora(obtenerDatosUsuario().getCvePresupuestal(), getObjetosSs().getPeriodoActual(), 2, 2, Boolean.TRUE);
					reportesServices.updateBitacora(obtenerDatosUsuario().getCvePresupuestal(), getObjetosSs().getPeriodoActual(), 3, 2, Boolean.TRUE);
				}else{
					reportesServices.insertarInicioBitacora(obtenerDatosUsuario().getCvePresupuestal(), getObjetosSs().getPeriodoActual(), 2, 1 );
					reportesServices.insertarInicioBitacora(obtenerDatosUsuario().getCvePresupuestal(), getObjetosSs().getPeriodoActual(), 3, 1 );
				}
				
				reportesServices.cerrarPeriodo( obtenerDatosUsuario().getCvePresupuestal(), 
						   HOSPITALIZACION,
						   this.periodoImss.getClavePeriodo());

				reportesServices.insertarPeriodoOperacion( obtenerDatosUsuario().getCvePresupuestal(),  
									HOSPITALIZACION,
									reportesServices.siguientePeriodo( getObjetosSs().getPeriodoActual() ) );
            	           	
            	logger.info("Fin del proceso... " + new SimpleDateFormat("dd/mm/yyyy HH:mm:ss").format(new Date()));
            }

            
		} catch (HospitalizacionException e) {
			logger.error(ERROR_REPORTE + e.getMessage(), e);
			agregarMensajeError(getArray(MensajesErrorConstants.ME_501), null);		
		} catch (Exception exc) {
            logger.error(ERROR_REPORTE + exc.getMessage(), exc);
        }
        
        context.execute("PF('dlgCierre').hide();");
    }    
    
    
    /**
     * Metodo que realiza el proceso de descarga del reporte parte II con la informacion
     * que se genero en el proceso del cierre. 
     */
    public void descargar(){
    	logger.debug("Inicio proceso de descarga del reporte de cierre de mes...");
    	try {
    		fileXlsSpring(true);
		} catch (JRException e) {
            logger.error(ERROR_REPORTE + e.getMessage(), e);
        } catch (Exception exc) {
            logger.error(ERROR_REPORTE + exc.getMessage(), exc);
        }
    	
    }
    
    /**
     * Metodo que es invocado desde pantalla para que se genere el reporte a partir del periodo de fechas que se obtienen 
     * desde base de datos de acuerdo a la fecha en la que se genero el reporte de pacientes hospitalizados y la fecha
     * inicial del periodo activo.
     * 
     */
    public void generarReporteFechas() {

        logger.debug("INICIA generarReporteFechas");
        Calendar fechaPacientesC = Calendar.getInstance();
        try {
        	logger.debug("Clave presupuestal: " + obtenerDatosUsuario().getCvePresupuestal() );
        	Date fechaPacientes = reportesServices.obtenerUltimaFechaReportePacientes( TipoReporte.PACIENTES_HOSPITALIZADOS.getClave(), 
        															obtenerDatosUsuario().getCvePresupuestal() );
    		fechaPacientesC.setTime( fechaPacientes );
    		 /*
    		  *  Se establece la fecha ultima en la que se genero el reporte de pacientes y se obtiene el periodo
    		  *  de la fecha que se obtuvo
    		  */                  
    		String cvePeriodoImss = reportesServices.periodoMasActualAbierto(obtenerDatosUsuario().getCvePresupuestal());    		
    		this.periodoImss = reportesServices.obtenerPeriodoImssXcvePeriodoImss(cvePeriodoImss);
            
    		if( validarFechaIMayorFechaF( this.periodoImss.getFechaInicial(), fechaPacientes ) ){
    			abreDialogoGeneraReporte();
    			return;
    		}
                		
    		// Valida si existen registros en la tabla antes de obtener el periodo 
            Integer resultado = reportesServices.obtenerConteoDatosParteIIPeriodo( obtenerDatosUsuario().getCvePresupuestal(), 
            								getObjetosSs().getPeriodoActual()	 );
            this.fechaInicial = form.format( this.periodoImss.getFechaInicial() );
            this.fechaFinal = form.format( fechaPacientes );
            
            if (resultado > 0) {	
                fileXlsSpring(false);
            } else {               
                agregarMensajeError(getArray(MensajesErrorConstants.ME_500), null);
            }
            logger.debug("Termino de generar el reporte por periodo...");
        } catch (JRException e) {
            logger.error(ERROR_REPORTE + e.getMessage(), e);
        } catch (Exception exc) {
            logger.error(ERROR_REPORTE + exc.getMessage(), exc);
        }

    }
    
    /**
     * Metodo que valida si la fecha inicial es mayor a la feche final
     * @param fi fecha inicial
     * @param ff fecha final
     * @return true si la fecha inicial es mayor, false si la fecha
     *              inicial es mayor
     */
    private boolean validarFechaIMayorFechaF( Date fi, Date ff ){
    	Calendar fic = Calendar.getInstance();
    	Calendar ffc = Calendar.getInstance();
    	fic.setTime(fi);
    	ffc.setTime(ff);
    	if( fic.after(ffc) ){ 
    		return true;
    	}
    	return false;
    }
    
    public void abreDialogoGeneraReporte() {
        logger.info("INICIA abreDialogoGeneraReporte");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlgGenerarReportePeriodo').show();");
    }
 
    public ReportesServices getReportesServices() {

        return reportesServices;
    }

    public void setReportesServices(ReportesServices reportesServices) {

        this.reportesServices = reportesServices;
    }    

    public AbstractJasperReportsSingleFormatView getPlantillareporteParteDosPeriodo() {
		return plantillareporteParteDosPeriodo;
	}

	public void setPlantillareporteParteDosPeriodo(AbstractJasperReportsSingleFormatView plantillareporteParteDosPeriodo) {
		this.plantillareporteParteDosPeriodo = plantillareporteParteDosPeriodo;
	}

	public AbstractJasperReportsSingleFormatView getPlantillareporteParteDosCierre() {
		return plantillareporteParteDosCierre;
	}

	public void setPlantillareporteParteDosCierre(AbstractJasperReportsSingleFormatView plantillareporteParteDosCierre) {
		this.plantillareporteParteDosCierre = plantillareporteParteDosCierre;
	}

	@Override
    public String obtenerNombrePagina() {

        return PagesCommonConstants.REPORTE_PARTE_II;
    }

    @Override
    public void salir() throws IOException {
        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
    	FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }

    public PeriodosImss getPeriodoImss() {

        return periodoImss;
    }

    public void setPeriodoImss(PeriodosImss periodoImss) {

        this.periodoImss = periodoImss;
    }

	public String getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public ProcesarParteIIService getProcesarParteIIService() {
		return procesarParteIIService;
	}

	public void setProcesarParteIIService(ProcesarParteIIService procesarParteIIService) {
		this.procesarParteIIService = procesarParteIIService;
	}	

}
