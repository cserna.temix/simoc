package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class EgresoDiagnosticoEdicion {

    private Date fechaEgreso;
    private String claveEspecialidad;
    private String descripcionEspecialidad;
    private String claveMotivoEgreso;
    private String claveMotivoAlta;
    private String descripcionMotivoAlta;
    private String claveTipoEgreso;
    private String descripcionTipoEgreso;
    private String tipoDiagnostico;
    private String claveDiagnostico;
    private String descripcionDiagnostico;
    private int clavePlanificacionFamiliar;
    private String descripcionPlanificacionFamiliar;
    private int tipoFormato;

}
