package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class DatosTococirugia {

    private Date fecCreacion;
    private String fecAtencion;
    private Date fecActualizacion;
    private String numeroSala;
    private Integer atencion;
    private Integer tipoParto;
    private Boolean tienePlanificacionFamiliar;
    private String planificacionFamiliar;
    private List<DatosRecienNacido> listDatosRecienNacido;
    private DatosRecienNacido datosRecienNacidoModal;
    private Integer nacido;
    private Integer totalNacidos;
    private String horaParto;
    private String tipoPartoString;
    private Integer cantidadMetodoAnticonceptivo;
    private Integer gesta;
    private String episiotomia;

    public DatosTococirugia() {
        this.fecCreacion = null;
        this.fecAtencion = "";
        this.fecActualizacion = null;
        this.numeroSala = "";
        this.atencion = null;
        this.tipoParto = null;
        this.tienePlanificacionFamiliar = Boolean.FALSE;
        this.planificacionFamiliar = null;
        this.listDatosRecienNacido = null;
        this.nacido = null;
        this.totalNacidos = null;
        this.horaParto = "";
        this.cantidadMetodoAnticonceptivo = null;
        this.tipoPartoString = "";
        this.datosRecienNacidoModal = new DatosRecienNacido();
        this.gesta = null;
        this.episiotomia = "";
    }

}
