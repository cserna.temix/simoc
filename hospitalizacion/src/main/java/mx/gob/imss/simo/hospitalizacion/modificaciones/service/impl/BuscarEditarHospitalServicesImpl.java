package mx.gob.imss.simo.hospitalizacion.modificaciones.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.icu.text.SimpleDateFormat;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PerfilesEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoDiagnostioEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TococirugiaConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosTococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.Diagnostico;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoDiagnosticoEdicion;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientoCIE9;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientosEdicion;
import mx.gob.imss.simo.hospitalizacion.common.model.TococirugiaEdicion;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.modificaciones.helper.BuscarEditarHospitalHelper;
import mx.gob.imss.simo.hospitalizacion.modificaciones.repository.BuscarEditarHospitalRepository;
import mx.gob.imss.simo.hospitalizacion.modificaciones.rules.BuscarEditarHospitalRules;
import mx.gob.imss.simo.hospitalizacion.modificaciones.service.BuscarEditarHospitalServices;
import mx.gob.imss.simo.model.DatosUsuario;

@Service("buscarEditarHospitalServices")
public class BuscarEditarHospitalServicesImpl extends HospitalizacionCommonServicesImpl
        implements BuscarEditarHospitalServices {

    @Autowired
    private BuscarEditarHospitalRules buscarEditarHospitalRules;
    @Autowired
    private BuscarEditarHospitalHelper buscarEditarHospitalHelper;
    @Autowired
    private BuscarEditarHospitalRepository buscarEditarHospitalRepository;

    public void validarNss(String nss) throws HospitalizacionException {

        buscarEditarHospitalRules.validarNss(nss);

    }

    public void validarAgregadoMedico(DatosHospitalizacion datos) throws HospitalizacionException {

        super.validarAgregadoMedico(datos.getDatosPaciente().getAgregadoMedico());
    }

    public List<Boolean> validarEdicion(DatosHospitalizacion datosHospitalizacion, DatosUsuario usuario)
            throws HospitalizacionException {
    	
    	String claveCatalogo = "";
        Integer dias = 0;
        if (usuario.getPerfil().toUpperCase()
                .equals(PerfilesEnum.ADMINISTRADOR_DELEGACIONAL.getNombre().toUpperCase())) {
            claveCatalogo = PerfilesEnum.ADMINISTRADOR_DELEGACIONAL.getClaveCatalogo();
            Parametro parametro = buscarEditarHospitalRepository.obtenerParametroTiempoEdicion(claveCatalogo);
            dias = Integer.parseInt(parametro.getReferenciaParametro());
        } else if (usuario.getPerfil().toUpperCase().equals(PerfilesEnum.JEFE_ARIMAC.getNombre().toUpperCase())) {
            claveCatalogo = PerfilesEnum.JEFE_ARIMAC.getClaveCatalogo();
            Parametro parametro = buscarEditarHospitalRepository.obtenerParametroTiempoEdicion(claveCatalogo);
            dias = Integer.parseInt(parametro.getReferenciaParametro());
        } else if (usuario.getPerfil().toUpperCase()
                .equals(PerfilesEnum.ADMINISTRADOR_CENTRAL.getNombre().toUpperCase())) {
            dias = null;
        } else {
            dias = 0;
        }
        return buscarEditarHospitalRules.validarEdicion(datosHospitalizacion, dias);

    }

    @Override
    public void guardarEdicionHospitalizacion(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException {

        buscarEditarHospitalRepository.guardarEdicionHospitalizacion(datosHospitalizacion, datosUsuario);
        // Actualizacion para intervenciones quirurgicas
        for (Procedimientos procedimientos : datosHospitalizacion.getDatosIntervenciones().getProcedimientos()) {
            if (null != procedimientos && null != procedimientos.getClave() && !procedimientos.getClave().isEmpty()) {
                ProcedimientoCIE9 procedimientoCIE9 = new ProcedimientoCIE9();
                procedimientoCIE9.setCveCIE9(procedimientos.getClave());
                procedimientoCIE9.setCveIngreso(datosHospitalizacion.getDatosPaciente().getClaveIngreso());
                procedimientoCIE9.setNumProcedimiento(procedimientos.getConsecutivo());
                buscarEditarHospitalRepository.actualizarProcedimientosCIE9(procedimientoCIE9);
            }
        }
        buscarEditarHospitalRepository.actualizarIntervencionQuirurgica(datosUsuario, datosHospitalizacion);

        // Actualizacion estancia de recien nacidos
        for (DatosRecienNacido datosRecienNacido : datosHospitalizacion.getDatosTococirugia()
                .getListDatosRecienNacido()) {
            if (null != datosRecienNacido && null != datosRecienNacido.getSexo() && datosRecienNacido.getSexo() > 0) {
                buscarEditarHospitalRepository.actualizarRecienNacido(datosHospitalizacion, datosRecienNacido);
                buscarEditarHospitalRepository.actualizarPaciente(datosRecienNacido);
            }
        }

        buscarEditarHospitalRepository.actualizarTocoCirugia(datosHospitalizacion.getDatosTococirugia(),
                datosHospitalizacion.getDatosPaciente(), datosUsuario);

        buscarEditarHospitalRepository.actualizarEgreso(datosHospitalizacion.getDatosEgreso(),
                datosHospitalizacion.getDatosPaciente(), datosUsuario);

        if (datosHospitalizacion.getDatosEgreso().getCveInicialIngresoEgreso() != null
                && !datosHospitalizacion.getDatosEgreso().getCveInicialIngresoEgreso().isEmpty()) {
            buscarEditarHospitalRepository.actualizarDiagnosticoEgresoBusquedaActualizacion(
                    datosHospitalizacion.getDatosEgreso().getCveInicialIngresoEgreso(),
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                    TipoDiagnostioEnum.DIAGNOSTICO_INICIAL_INGRESO.getClave());
        }

        if (datosHospitalizacion.getDatosEgreso().getCvePrincipalEgreso() != null
                && !datosHospitalizacion.getDatosEgreso().getCvePrincipalEgreso().isEmpty()) {
            buscarEditarHospitalRepository.actualizarDiagnosticoEgresoBusquedaActualizacion(
                    datosHospitalizacion.getDatosEgreso().getCvePrincipalEgreso(),
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                    TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave());
        }

        if (datosHospitalizacion.getDatosEgreso().getCvePrimerDiagnosticoSecundario() != null
                && !datosHospitalizacion.getDatosEgreso().getCvePrimerDiagnosticoSecundario().isEmpty()) {
            buscarEditarHospitalRepository.actualizarDiagnosticoEgresoBusquedaActualizacion(
                    datosHospitalizacion.getDatosEgreso().getCvePrimerDiagnosticoSecundario(),
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                    TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave());
        }

        if (datosHospitalizacion.getDatosEgreso().getClaveSegundoDiagnosticoSecundario() != null
                && !datosHospitalizacion.getDatosEgreso().getClaveSegundoDiagnosticoSecundario().isEmpty()) {
            buscarEditarHospitalRepository.actualizarDiagnosticoEgresoBusquedaActualizacion(
                    datosHospitalizacion.getDatosEgreso().getClaveSegundoDiagnosticoSecundario(),
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                    TipoDiagnostioEnum.SEGUNDO_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave());
        }

        if (datosHospitalizacion.getDatosEgreso().getCveComplicacionIntraPrimera() != null
                && !datosHospitalizacion.getDatosEgreso().getCveComplicacionIntraPrimera().isEmpty()) {
            buscarEditarHospitalRepository.actualizarDiagnosticoEgresoBusquedaActualizacion(
                    datosHospitalizacion.getDatosEgreso().getCveComplicacionIntraPrimera(),
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                    TipoDiagnostioEnum.PRIMERA_COMPLICACION_INTRAHOSPITALARIA.getClave());
        }

        if (datosHospitalizacion.getDatosEgreso().getCveComplicacionIntraSegunda() != null
                && !datosHospitalizacion.getDatosEgreso().getCveComplicacionIntraSegunda().isEmpty()) {
            buscarEditarHospitalRepository.actualizarDiagnosticoEgresoBusquedaActualizacion(
                    datosHospitalizacion.getDatosEgreso().getCveComplicacionIntraSegunda(),
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                    TipoDiagnostioEnum.SEGUNDA_COMPLICACION_INTRAHOSPITALARIA.getClave());
        }

        if (datosHospitalizacion.getDatosEgreso().getCausaDefDirecta() != null
                && !datosHospitalizacion.getDatosEgreso().getCausaDefDirecta().isEmpty()) {
            buscarEditarHospitalRepository.actualizarDiagnosticoEgresoBusquedaActualizacion(
                    datosHospitalizacion.getDatosEgreso().getCveCausaDefDirecta(),
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                    TipoDiagnostioEnum.CAUSA_DIRECTA_DEFUNCION.getClave());
        }

        if (datosHospitalizacion.getDatosEgreso().getCausaDefBasica() != null
                && !datosHospitalizacion.getDatosEgreso().getCausaDefBasica().isEmpty()) {
            buscarEditarHospitalRepository.actualizarDiagnosticoEgresoBusquedaActualizacion(
                    datosHospitalizacion.getDatosEgreso().getCveCausaDefBasica(),
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                    TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave());
        }

        buscarEditarHospitalRepository.actualizarEgreso(datosHospitalizacion.getDatosEgreso(),
                datosHospitalizacion.getDatosPaciente(), datosUsuario);
    }

    public DatosHospitalizacion obtenerDatosParaEdicion(DatosPaciente datosPaciente) {

        Ingreso ingreso = buscarEditarHospitalRepository.obtenerDatosIngreso(datosPaciente.getClaveIngreso());
        List<ProcedimientosEdicion> procedimientos = buscarEditarHospitalRepository
                .buscarProcedimientosParaEdicion(datosPaciente.getClaveIngreso());
        List<TococirugiaEdicion> tococirugiaEdicion = buscarEditarHospitalRepository
                .obtenerTococirugiaPaciente(datosPaciente.getClaveIngreso());
        List<EgresoDiagnosticoEdicion> egreso = buscarEditarHospitalRepository
                .buscarEgresoDiagnosticoEdicion(datosPaciente.getClaveIngreso());
        MovimientoIntraHospitalario movimiento = buscarEditarHospitalRepository
                .buscarMovimientos(datosPaciente.getClaveIngreso());
        
        List<String> ingresosRN = new ArrayList<>();
        if (tococirugiaEdicion != null) {
            for (TococirugiaEdicion rn : tococirugiaEdicion) {
                if (buscarEditarHospitalRepository.buscarIngresoRN(rn.getCvePaciente())) {
                    ingresosRN.add(String.valueOf(rn.getCvePaciente()));
                }
            }
        }
        List<EgresoRecienNacido> egresosRn = buscarEditarHospitalRepository.buscarEgresosRN(ingreso.getCveIngreso());

        return buscarEditarHospitalHelper.armarDatosEdicion(ingreso, procedimientos, tococirugiaEdicion, egreso,
                datosPaciente, ingresosRN, egresosRn, movimiento);
    }

    public void validarPesoRecienNacido(DatosRecienNacido datosRecienNacido) throws HospitalizacionException {

        buscarEditarHospitalRules.validarNull(datosRecienNacido.getPeso());
        validarRequeridoEntero(datosRecienNacido.getPeso());
        buscarEditarHospitalRules.validarPesoRecienNacido(datosRecienNacido.getPeso());
    }

    public void validarTallaRecienNacido(DatosRecienNacido rn) throws HospitalizacionException {

        buscarEditarHospitalRules.validarNull(rn.getTalla());
        validarRequeridoEntero(rn.getTalla());
        buscarEditarHospitalRules.validarTallaRecienNacido(rn.getTalla());
    }

    public void validarSexoRn(DatosRecienNacido rn) throws HospitalizacionException {

        buscarEditarHospitalRules.validarNull(rn.getSexo());
        validarRequeridoEntero(rn.getSexo());
    }

    private void validarRequeridoEntero(int entero) throws HospitalizacionException {

        if (entero < 0) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_160);
        }
    }

    @Override
    public void validarProcedimientoIntervencionQuirurgica(Procedimientos procedimiento,
            List<Procedimientos> listaProcedimientos, String agregadoMedico) throws HospitalizacionException {

        buscarEditarHospitalRules.validarRequeridoStringCadena(procedimiento.getClave());
        Procedimiento procedimientoAux = buscarEditarHospitalRepository
                .obtenerProcedimientoPorClave(procedimiento.getClave());

        buscarEditarHospitalRules.validarClaveProcedimiento(procedimientoAux, agregadoMedico, procedimiento.getClave());

        if (!buscarEditarHospitalRules.esBilateral(procedimientoAux.getIndBilateralidad())) {
            buscarEditarHospitalRules.validarRepetidosEdicion(listaProcedimientos, procedimiento);
        }

    }

    public void validarDiagnosticoInicialEgreso(DatosHospitalizacion datos, String agregadoMedico)
            throws HospitalizacionException {

        buscarEditarHospitalRules.validarDXPorTipoDiagnostico(datos, datos.getDatosEgreso().getCveInicialIngresoEgreso(), CapturarEgresoConstants.DIAGNOSTICO_INICIAL);
        validarDiagnosticos(datos.getDatosEgreso().getCveInicialIngresoEgreso(), datos.getDatosPaciente(), Boolean.TRUE,
                CapturarEgresoConstants.DIAGNOSTICO_INICIAL);
    }

    public void validarDiagnosticoPrincipalEgreso(DatosHospitalizacion datos, String agregadoMedico)
            throws HospitalizacionException {

        buscarEditarHospitalRules.validarDXPorTipoDiagnostico(datos, datos.getDatosEgreso().getCvePrincipalEgreso(), CapturarEgresoConstants.REQUERIDO_DIAGNOSTICO_PRINCIPAL);
        validarDiagnosticos(datos.getDatosEgreso().getCvePrincipalEgreso(), datos.getDatosPaciente(), Boolean.TRUE,
                CapturarEgresoConstants.REQUERIDO_DIAGNOSTICO_PRINCIPAL);
    }

    public void validarPrimerSecundarioEgreso(DatosHospitalizacion datos, String agregadoMedico)
            throws HospitalizacionException {

        buscarEditarHospitalRules.validarDXIguales(datos, datos.getDatosEgreso().getCvePrimerDiagnosticoSecundario());
        validarDiagnosticos(datos.getDatosEgreso().getCvePrimerDiagnosticoSecundario(), datos.getDatosPaciente(),
                Boolean.TRUE, CapturarEgresoConstants.PRIMER_DIAGNOSTICO_SECUNDARIO);

    }

    public void validarSegundoSecundarioEgreso(DatosHospitalizacion datos, String agregadoMedico)
            throws HospitalizacionException {

        buscarEditarHospitalRules.validarDXIguales(datos,
                datos.getDatosEgreso().getClaveSegundoDiagnosticoSecundario());
        validarDiagnosticos(datos.getDatosEgreso().getClaveSegundoDiagnosticoSecundario(), datos.getDatosPaciente(),
                Boolean.TRUE, CapturarEgresoConstants.SEGUNDO_DIAGNOSTICO_SECUNDARIO);

    }

    public void validarPrimeraComplicacionIntraEgreso(DatosHospitalizacion datos) throws HospitalizacionException {

        buscarEditarHospitalRules.validarDXIguales(datos, datos.getDatosEgreso().getCveComplicacionIntraPrimera());
        super.validarDiagnosticos(datos.getDatosEgreso().getCveComplicacionIntraPrimera(), datos.getDatosPaciente(),
                Boolean.TRUE, CapturarEgresoConstants.PRIMERA_COMPLICACION);

    }

    public void validarSegundaComplicacionIntraEgreso(DatosHospitalizacion datos) throws HospitalizacionException {

        buscarEditarHospitalRules.validarDXIguales(datos, datos.getDatosEgreso().getCveComplicacionIntraSegunda());
        super.validarDiagnosticos(datos.getDatosEgreso().getCveComplicacionIntraSegunda(), datos.getDatosPaciente(),
                Boolean.TRUE, CapturarEgresoConstants.SEGUNDA_COMPLICACION);

    }

    @Override
    public String obtenerDescripcionProcedimiento(String clave) {

        Procedimiento procedimiento = buscarEditarHospitalRepository.obtenerProcedimientoPorClave(clave);
        if (null != procedimiento) {
            return procedimiento.getDesCie9();
        }

        return "";
    }

    @Override
    public void validarCausaDirectaDefuncion(DatosHospitalizacion datos) throws HospitalizacionException {

        buscarEditarHospitalRules.validarDXPorTipoDiagnostico(datos, datos.getDatosEgreso().getCveCausaDefDirecta(),CapturarEgresoConstants.CAUSA_DIRECTA_DEFUNCION);
        validarDiagnosticos(datos.getDatosEgreso().getCveCausaDefDirecta(), datos.getDatosPaciente(), Boolean.TRUE,
                CapturarEgresoConstants.CAUSA_DIRECTA_DEFUNCION);
        validarCausaValidaDefuncion(datos.getDatosEgreso().getCveCausaDefDirecta(),
                CapturarEgresoConstants.CAUSA_DIRECTA_DEFUNCION);

    }

    @Override
    public void validarCausaBasicaDefuncion(DatosHospitalizacion datos) throws HospitalizacionException {

        buscarEditarHospitalRules.validarDXPorTipoDiagnostico(datos, datos.getDatosEgreso().getCveCausaDefBasica(), CapturarEgresoConstants.CAUSA_BASICA_DEFUNCION);
        validarDiagnosticos(datos.getDatosEgreso().getCveCausaDefBasica(), datos.getDatosPaciente(), Boolean.TRUE,
                CapturarEgresoConstants.CAUSA_BASICA_DEFUNCION);
        validarCausaValidaDefuncion(datos.getDatosEgreso().getCveCausaDefBasica(),
                CapturarEgresoConstants.CAUSA_BASICA_DEFUNCION);

    }

    @Override
    public void validarTipoPartoTopoCirugia(DatosHospitalizacion datos) throws HospitalizacionException {

        if (null == datos.getDatosTococirugia().getTipoParto()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(TococirugiaConstants.REQUERIDO_TIPO_PARTO));
        }
    }

    @Override
    public void validarMotivoAlta(DatosHospitalizacion datos) throws HospitalizacionException {

        if (datos.getDatosEgreso().getMotivoAlta() == null || datos.getDatosEgreso().getMotivoAlta().isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(CapturarEgresoConstants.REQUERIDO_MOTIVO_ALTA));

        }
    }

    /**
     * Metodo para validar si se activa o no una seccion especifica
     * 
     * @param fechaFormato
     * @param referencia
     * @param valor
     * @return
     */
    @Override
    public boolean validarActivarSeccion(Date fechaFormato, String referencia, int valor) {

        Date fechaCalculadaSeccion = buscarEditarHospitalHelper.sumarFechaSeccion(fechaFormato, referencia, valor);
        return buscarEditarHospitalRules.validarFechaSeccion(fechaCalculadaSeccion);
    }

    @Override
    public String obtenerDescripcionDX(String cveDX) {

        Diagnostico dx = buscarEditarHospitalRepository.buscarDisagnostico(cveDX);
        return dx.getDesCie10();
    }

    public DatosRecienNacido armarRN(DatosTococirugia datosTococirugia, int numero) {

        return buscarEditarHospitalHelper.armarRN(datosTococirugia, numero);
    }

    @Override
    public int buscarSiguienteFoco(List<Procedimientos> procedimientos) {

        return buscarEditarHospitalHelper.buscarSiguienteFoco(procedimientos);
    }

    @Override
    public Procedimientos crearProcedimiento(List<Procedimientos> listaProcedimientos, int i) {

        return buscarEditarHospitalHelper.crearProcedimiento(listaProcedimientos.get(i));
    }

    public boolean validarActivarGuardar(Boolean[] requeridos) {

        return buscarEditarHospitalHelper.validarActivarGuardar(requeridos);
    }

    public boolean puedeActivarEdicion(DatosHospitalizacion datosHospitalizacion, String clavePresupuestal, String perfil) {
        
    	if(perfil.equals(PerfilesEnum.USUARIO_CAPTURA.getNombre().toUpperCase())){
    		return false;
    		
    	}else{
	        if (null != datosHospitalizacion.getDatosEgreso()
	                && null != datosHospitalizacion.getDatosEgreso().getFechaEgreso()) {
	            try {
	                validarFechaEnPeriodoActualActivo(
	                        new SimpleDateFormat(BaseConstants.FORMATO_FECHA)
	                                .format(datosHospitalizacion.getDatosEgreso().getFechaEgreso()),
	                        datosHospitalizacion.getDatosEgreso().getFechaEgreso(), clavePresupuestal,
	                        TipoCapturaEnum.HOSPITALIZACION.getClave());
	            } catch (HospitalizacionException e) {
	                return false;
	            }
	        }
	        return true; 
    	}
}
    
    
    @Override
    public Integer obtieneEdadAgregadoMedico(String agregadoMedico) {

        return buscarEditarHospitalRules.obtenerEdadAgregadoMedico(agregadoMedico);
    }

}
