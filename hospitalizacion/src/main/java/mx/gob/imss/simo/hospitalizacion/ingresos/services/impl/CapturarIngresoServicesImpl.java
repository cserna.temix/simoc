package mx.gob.imss.simo.hospitalizacion.ingresos.services.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.busquedas.rules.AccederRules;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesAdvertenciaConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.NumTipoServicioEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCama;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCamaAlojamiento;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.TotalCamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.ingresos.helper.CapturarIngresoHelper;
import mx.gob.imss.simo.hospitalizacion.ingresos.repository.CapturarIngresoRepository;
import mx.gob.imss.simo.hospitalizacion.ingresos.rules.CapturarIngresoRules;
import mx.gob.imss.simo.hospitalizacion.ingresos.services.CapturarIngresoServices;
import mx.gob.imss.simo.model.DatosUsuario;

@Service("capturarIngresoServices")
public class CapturarIngresoServicesImpl extends HospitalizacionCommonServicesImpl implements CapturarIngresoServices {

    @Autowired
    private AccederRules accederRules;

    @Autowired
    private CapturarIngresoHelper capturarIngresoHelper;

    @Autowired
    private CapturarIngresoRepository capturarIngresoRepository;

    @Autowired
    private CapturarIngresoRules capturarIngresoRules;
    
    @Autowired
    private HospitalizacionCommonHelper hospitalizacionCommonHelper;

    private static final String LOGGER_INGRESO_CREADO = "Ingreso creado - cveIngreso: ";

    @Override
    public void validarFechaIngreso(String fechaIngreso, String clavePresupuestal) throws HospitalizacionException {

        capturarIngresoRules.validarFormatoFecha(fechaIngreso);
        validarReporteRelacionPacientesGenerado(clavePresupuestal, fechaIngreso);
        capturarIngresoRules.validarFechaActual(fechaIngreso);
        validarCamasConDosPacientes(clavePresupuestal, fechaIngreso);
        validarPeriodoAnteriorCerrado(clavePresupuestal, fechaIngreso, TipoCapturaEnum.HOSPITALIZACION.getClave());
    }

    @Override
    public void validarNss(String nss) throws HospitalizacionException {

        capturarIngresoRules.validarNss(nss);
    }

    @Override
    public void validarIngresoVigente(String clavePresupuestal, TipoUbicacionEnum tipoUbicacion,
            DatosPaciente datosPaciente) throws HospitalizacionException {

        List<Integer> tipoFormato = new ArrayList<>();
        if (tipoUbicacion.equals(TipoUbicacionEnum.HOSPITALIZACION)
                || tipoUbicacion.equals(TipoUbicacionEnum.URGENCIAS)) {
            tipoFormato.add(TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave());
            tipoFormato.add(TipoFormatoEnum.INGRESO_URGENCIAS.getClave());
            tipoFormato.add(TipoFormatoEnum.INGRESO_UCI.getClave());
        } else {
            tipoFormato.add(TipoFormatoEnum.INGRESO_URGENCIAS.getClave());
            tipoFormato.add(TipoFormatoEnum.INGRESO_UCI.getClave());
        }
        List<Ingreso> ingresos = capturarIngresoRepository.buscarIngresoVigente(clavePresupuestal,
                datosPaciente.getNss(), datosPaciente.getAgregadoMedico(), tipoFormato);
        capturarIngresoRules.validarIngresoVigente(ingresos, datosPaciente.getCvePaciente(),
                datosPaciente.getCvePacientePadre(), false, tipoUbicacion,
                datosPaciente.getAgregadoMedico().substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave()));
    }
    
    @Override
    public List<Long> buscarRNExterno(DatosIngresoRN datosRN)throws HospitalizacionException {
    	
    	List<Long> clavePaciente = capturarIngresoRepository.buscarRNExterno(datosRN.getNss(),datosRN.getAgregadoMedico(),
    			datosRN.getNombre(), datosRN.getApellidoPaterno());
    	return clavePaciente;
    	
    }
    
    @Override
    public List<Ingreso> validarIngresoVigenteRecienNacido(String clavePresupuestal, DatosIngresoRN datosRN) throws HospitalizacionException {

        List<Integer> tipoFormato = new ArrayList<>();
        tipoFormato.add(TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave());
        tipoFormato.add(TipoFormatoEnum.INGRESO_RN_UCIN.getClave());
        tipoFormato.add(TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave());
        
        datosRN.setClavePaciente(null);
        List<Long> clavePaciente = buscarRNExterno(datosRN);
        List<Ingreso> ingresos = null;
        
        if(clavePaciente != null && !clavePaciente.isEmpty()){
        	datosRN.setClavePaciente(clavePaciente.get(0));
        	ingresos = capturarIngresoRepository.buscarIngresoVigenteRN(clavePresupuestal,tipoFormato, clavePaciente.get(0));
        	capturarIngresoRules.validarIngresoVigenteRN(ingresos);
        	
        }
        
        return ingresos;
    }

    @Override
    public Ingreso buscarIngresoVigenteHospitalizacion(String clavePresupuestal, DatosPaciente datosPaciente)
            throws HospitalizacionException {

        List<Integer> tipoFormato = new ArrayList<>();
        tipoFormato.add(TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave());
        List<Ingreso> ingresos = capturarIngresoRepository.buscarIngresoVigente(clavePresupuestal,
                datosPaciente.getNss(), datosPaciente.getAgregadoMedico(), tipoFormato);
        return capturarIngresoRules.validarIngresoVigente(ingresos, datosPaciente.getCvePaciente(),
                datosPaciente.getCvePacientePadre(), true, TipoUbicacionEnum.UCI,
                datosPaciente.getAgregadoMedico().substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave()));
    }

    @Override
    public boolean habilitarEdad(String fechaIngreso, String anio) {

        return capturarIngresoRules.habilitarEdad(fechaIngreso, anio);
    }

    @Override
    public Map<TipoCalculoEdadEnum, Integer> calcularEdadPacienteNoEncontrado(String anio, String fechaIngreso) {

        Date fechaAnio;
        try {
            fechaAnio = new SimpleDateFormat(CapturarIngresosConstants.FORMATO_FECHA_AGREGADO_ANIO)
                    .parse(anio.concat("/01/01"));
        } catch (ParseException e) {
            fechaAnio = new Date();
        }
        return accederRules.calcularEdadDerechohabiente(fechaAnio, fechaIngreso);
    }
    
    @Override
    public void calcularEdadPacienteEncontrado(DatosPaciente datosPaciente, String fechaIngreso) {
    	
    	Map<TipoCalculoEdadEnum, Integer> edades = new HashMap<>();
    	if(datosPaciente.getFechaNacimiento() != null){
    		edades = accederRules.calcularEdadDerechohabiente(datosPaciente.getFechaNacimiento(), fechaIngreso);	
    	}else{
    		edades = calcularEdadPacienteNoEncontrado(datosPaciente.getAgregadoMedico().substring(
                    BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO), fechaIngreso);
    	}
    	datosPaciente.setEdadAnios(edades.get(TipoCalculoEdadEnum.ANIOS));
		datosPaciente.setEdadSemanas(edades.get(TipoCalculoEdadEnum.SEMANAS));
    }

    @Override
    public void validarEdad(String edad) throws HospitalizacionException {

        capturarIngresoRules.validarEdad(edad);
    }

    @Override
    public void validarNombre(String nombre) throws HospitalizacionException {

        capturarIngresoRules.validarNombre(nombre);
    }

    @Override
    public void validarApellidoPaterno(String apellidoPaterno) throws HospitalizacionException {

        capturarIngresoRules.validarApellidoPaterno(apellidoPaterno);
    }

    @Override
    public void validarNumero(String numero) throws HospitalizacionException {

        capturarIngresoRules.validarNumero(numero);
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoEspecialidad(TipoUbicacionEnum tipoUbicacion, String clavePresupuestal) {

    	if(tipoUbicacion.equals(TipoUbicacionEnum.URGENCIAS)) {
    		// URGENCIAS
    		return super.getCatalogosHospitalizacionServices().obtenerCatalogoEspecialidadPorTipoServicio(clavePresupuestal, NumTipoServicioEnum.URGENCIAS.getValor());
    	}else if(tipoUbicacion.equals(TipoUbicacionEnum.UCI)) {
    		// UCI
    		return super.getCatalogosHospitalizacionServices().obtenerCatalogoEspecialidadPorTipoServicio(clavePresupuestal, NumTipoServicioEnum.GENERALES.getValor());
    	}else {
	        List<DatosCatalogo> catalogoEspecialidad = getCatalogosHospitalizacionServices()
	                .obtenerCatalogoEspecialidad(clavePresupuestal);
	        return capturarIngresoRules.obtenerCatalogoEspecialidad(tipoUbicacion, catalogoEspecialidad);
    	}
    }

    @Override
    public List<DatosCatalogo> filtrarCatalogoTipoIngreso(List<DatosCatalogo> tiposIngreso, boolean ingresoVigente) {

        return capturarIngresoRules.filtrarCatalogoTipoIngreso(tiposIngreso, ingresoVigente);
    }

    @Override
    public void validarTipoIngreso(String tipoIngreso, List<DatosCatalogo> catalogoTipos)
            throws HospitalizacionException {

        capturarIngresoRules.validarTipoIngreso(tipoIngreso, catalogoTipos);
    }

    @Override
    public void validarTipoPrograma(String tipoPrograma, List<DatosCatalogo> catalogoTipos)
            throws HospitalizacionException {

        capturarIngresoRules.validarTipoPrograma(tipoPrograma, catalogoTipos);
    }

    @Override
    public boolean esPeriodoExtemporaneo(Date fechaCaptura, String clavePresupuestal) throws HospitalizacionException {

        PeriodosImss periodoActivo = capturarIngresoRepository.obtenerPeriodoActivo(clavePresupuestal,
                TipoCapturaEnum.HOSPITALIZACION.getClave());
        PeriodosImss periodoAnterior=null;
        if(periodoActivo!=null){
        	periodoAnterior = capturarIngresoRepository
                    .obtenerPeriodoInmediatoAnterior(periodoActivo.getClavePeriodo());	
        }
        
        PeriodosImss periodoMonitoreo = capturarIngresoRepository.obtenerPeriodoMonitoreo(clavePresupuestal,
        		TipoCapturaEnum.HOSPITALIZACION.getClave());
        
        return capturarIngresoRules.esPeriodoExtemporaneo(periodoActivo, periodoAnterior, periodoMonitoreo, fechaCaptura);
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario,
            TipoUbicacionEnum tipoUbicacion) throws HospitalizacionException {

        try {
            logger.info("Iniciando guardado de Ingreso: " + tipoUbicacion.getDescripcion());
            logger.info(
                    "Usuario: " + datosUsuario.getUsuario() + " - Unidad Medica: " + datosUsuario.getCvePresupuestal());
            logger.info("NSS: " + datosHospitalizacion.getDatosPaciente().getNss() + " - Agregado Medico: "
                    + datosHospitalizacion.getDatosPaciente().getAgregadoMedico());
            List<CamaUnidad> camasUnidad = capturarIngresoRepository.buscarCamaUnidad(datosUsuario.getCvePresupuestal(),
                    datosHospitalizacion.getDatosIngreso().getCama(),
                    datosHospitalizacion.getDatosIngreso().getEspecialidadCama());
            CamaUnidad cama = camasUnidad.get(0);
            List<OcupacionCama> ocupacion = capturarIngresoRepository.buscarOcupacionCama(cama.getCveCama(),
                    cama.getCvePresupuestal(), cama.getCveEspecialidad());
            if (tipoUbicacion.equals(TipoUbicacionEnum.HOSPITALIZACION)
                    || tipoUbicacion.equals(TipoUbicacionEnum.URGENCIAS)) {
                Ingreso ingreso = guardarIngreso(datosHospitalizacion, tipoUbicacion, datosUsuario, cama, false);
                if (ocupacion == null || ocupacion.isEmpty()) {
                    capturarIngresoRepository.guardarOcupacionCama(cama);
                    logger.debug("Cama ocupada: " + cama.getCveCama() + " - Pacientes: " + 1);
                } else {
                    actualizarOcupacionCama(cama, BaseConstants.AGREGAR_PACIENTE);
                    logger.debug("Cama ocupada: " + cama.getCveCama() + " - Pacientes: "
                            + (ocupacion.get(0).getNumPacientes() + 1));
                }
                TipoFormatoEnum tipoFormato = tipoUbicacion.equals(TipoUbicacionEnum.HOSPITALIZACION)
                        ? TipoFormatoEnum.INGRESO_HOSPITALIZACION : TipoFormatoEnum.INGRESO_URGENCIAS;
                capturarIngresoRepository.registrarEventoBitacoraCama(cama, tipoFormato, ingreso.getFecIngreso(),
                        BaseConstants.CAMA_OCUPADA);
                logger.debug(LOGGER_INGRESO_CREADO + ingreso.getCveIngreso());
            } else {
                Ingreso ingresoVigente = null;
                if (null != datosHospitalizacion.getDatosPaciente().getCvePaciente()) {
                    ingresoVigente = buscarIngresoVigenteHospitalizacion(datosUsuario.getCvePresupuestal(),
                            datosHospitalizacion.getDatosPaciente());
                }
                if (ingresoVigente == null) {
                    logger.debug("Sin Ingreso vigente en HOSPITALIZACION");
                    String especialidadUCI = datosHospitalizacion.getDatosIngreso().getEspecialidad();
                    String especialidadHospitalizacion = datosHospitalizacion.getDatosIngreso()
                            .getEspecialidadHospitalizacion();
                    datosHospitalizacion.getDatosIngreso().setEspecialidad(especialidadHospitalizacion);
                    Ingreso ingresoPadre = guardarIngreso(datosHospitalizacion, TipoUbicacionEnum.HOSPITALIZACION,
                            datosUsuario, cama, true);
                    datosHospitalizacion.getDatosIngreso().setIngresoPadre(ingresoPadre.getCveIngreso());
                    datosHospitalizacion.getDatosIngreso().setEspecialidad(especialidadUCI);
                    Ingreso ingresoUCI = guardarIngreso(datosHospitalizacion, tipoUbicacion, datosUsuario, cama, false);
                    if (ocupacion == null || ocupacion.isEmpty()) {
                        capturarIngresoRepository.guardarOcupacionCama(cama);
                        logger.debug("Cama ocupada: " + cama.getCveCama() + " - Pacientes: " + 1);
                    } else {
                        actualizarOcupacionCama(cama, BaseConstants.AGREGAR_PACIENTE);
                        logger.debug("Cama ocupada: " + cama.getCveCama() + " - Pacientes: "
                                + (ocupacion.get(0).getNumPacientes() + 1));
                    }
                    capturarIngresoRepository.registrarEventoBitacoraCama(cama, TipoFormatoEnum.INGRESO_UCI,
                            ingresoUCI.getFecIngreso(), BaseConstants.CAMA_OCUPADA);
                    logger.debug("Ingreso Padre Creado - cveIngresoPadre: " + ingresoPadre.getCveIngreso());
                    logger.debug(LOGGER_INGRESO_CREADO + ingresoUCI.getCveIngreso());
                } else {
                    logger.debug("Ya existe un Ingreso vigente en HOSPITALIZACION - cveIngreso: "
                            + ingresoVigente.getCveIngreso());
                    datosHospitalizacion.getDatosIngreso().setIngresoPadre(ingresoVigente.getCveIngreso());
                    Ingreso ingreso = guardarIngreso(datosHospitalizacion, tipoUbicacion, datosUsuario, cama, false);
                    eliminarCamaIngresoVigente(ingresoVigente.getCveIngreso());
                    List<CamaUnidad> camaAnterior = capturarIngresoRepository.buscarCamaUnidad(
                            datosUsuario.getCvePresupuestal(), ingresoVigente.getCveCama(),
                            ingresoVigente.getCveEspecialidadCama());
                    actualizarOcupacionCama(camaAnterior.get(0), BaseConstants.QUITAR_PACIENTE);
                    logger.debug("Cama desocupada: " + camaAnterior.get(0).getCveCama());
                    if (ocupacion == null || ocupacion.isEmpty()) {
                        capturarIngresoRepository.guardarOcupacionCama(cama);
                        logger.debug("Cama ocupada: " + cama.getCveCama() + " - Pacientes: " + 1);
                    } else {
                        actualizarOcupacionCama(cama, BaseConstants.AGREGAR_PACIENTE);
                        logger.debug("Cama ocupada: " + cama.getCveCama() + " - Pacientes: "
                                + (ocupacion.get(0).getNumPacientes() + 1));
                    }
                    capturarIngresoRepository.registrarEventoBitacoraCama(camaAnterior.get(0),
                            TipoFormatoEnum.parse(ingresoVigente.getCveTipoFormato()), ingreso.getFecIngreso(),
                            BaseConstants.CAMA_DESOCUPADA);
                    capturarIngresoRepository.registrarEventoBitacoraCama(cama, TipoFormatoEnum.INGRESO_UCI,
                            ingreso.getFecIngreso(), BaseConstants.CAMA_OCUPADA);
                    logger.debug(LOGGER_INGRESO_CREADO + ingreso.getCveIngreso());
                }
            }
        } catch (Exception e) {
            logger.error("Error al guardar Ingreso: ", e);
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_020), e);
        }
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public Ingreso guardarIngreso(DatosHospitalizacion datosHospitalizacion, TipoUbicacionEnum tipoUbicacion,
            DatosUsuario datosUsuario, CamaUnidad camaUnidad, boolean ingresoPadre) {

        Long cvePaciente = datosHospitalizacion.getDatosPaciente().getCvePaciente();
        if (null == cvePaciente || cvePaciente == 0) {
            Paciente paciente = capturarIngresoHelper.prepararPaciente(datosHospitalizacion.getDatosPaciente(),
                    tipoUbicacion);
            cvePaciente = getBuscarPacienteService().guardarPaciente(paciente);
            logger.debug("Paciente creado - cvePaciente: " + cvePaciente);
            datosHospitalizacion.getDatosPaciente().setCvePaciente(cvePaciente);
        } else {
            logger.debug("Paciente encontrado - cvePaciente: " + cvePaciente);
            capturarIngresoRepository.actualizarEdadPaciente(cvePaciente, 
            		datosHospitalizacion.getDatosPaciente().getEdadSemanas(), 
            		datosHospitalizacion.getDatosPaciente().getEdadAnios());
        }
        Ingreso ingreso = capturarIngresoHelper.prepararIngreso(cvePaciente, tipoUbicacion, datosUsuario,
                datosHospitalizacion.getDatosIngreso(), camaUnidad, datosHospitalizacion.getDatosMedico(),
                ingresoPadre);
        long cveIngreso = capturarIngresoRepository.guardarIngreso(ingreso);
        ingreso.setCveIngreso(cveIngreso);
        return ingreso;
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void guardarIngresoRecienNacido(DatosIngresoRN datosIngresoRN, DatosUsuario datosUsuario)
    		throws HospitalizacionException {

    	try {
    		Integer tipoFormato = capturarIngresoHelper
    				.obtenerTipoFormatoIngresoRecienNacido(datosIngresoRN.getEspecialidadIngreso());
    		logger.info("Iniciando guardado de " + TipoFormatoEnum.parse(tipoFormato).getDescripcion());
    		logger.info(
    				"Usuario: " + datosUsuario.getUsuario() + " - Unidad Medica: " + datosUsuario.getCvePresupuestal());
    		logger.info("NSS: " + datosIngresoRN.getNss());
    		if (tipoFormato.equals(TipoFormatoEnum.INGRESO_RN_UCIN.getClave())) {
    			if (!datosIngresoRN.isCuneroVigente()) {
    				Long cvePaciente = datosIngresoRN.getClavePaciente();
        			if (null == cvePaciente || cvePaciente == 0) {
        				Paciente paciente = capturarIngresoHelper.prepararPacienteRN(datosIngresoRN,
        						null);
        				cvePaciente = getBuscarPacienteService().guardarPaciente(paciente);
        				datosIngresoRN.setClavePaciente(cvePaciente);
        				logger.debug("Paciente creado - cvePaciente: " + cvePaciente);
        			} else {
        				logger.debug("Paciente encontrado - cvePaciente: " + cvePaciente);
        			}
    				//datosIngresoRN.setEspecialidadIngreso(BaseConstants.ESPECIALIDAD_CUNERO_PATOLOGICO);
        			datosIngresoRN.setEspecialidadIngreso(datosIngresoRN.getEspecialidadHospitalizacion());
    				Ingreso ingresoCunero = capturarIngresoHelper.prepararIngresoRecienNacido(datosIngresoRN,
    						TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave(), datosUsuario);
    				ingresoCunero.setRefCamaRn(null);
    				ingresoCunero.setCveEspecialidadCama(null);
    				long claveIngresoCunero = capturarIngresoRepository.guardarIngreso(ingresoCunero);
    				capturarIngresoRepository.actualizarNumRecienNacido(ingresoCunero.getNumRecienNacido(),claveIngresoCunero);
    				logger.debug("Ingreso Padre Creado - cveIngresoPadre: " + claveIngresoCunero);
    				datosIngresoRN.setClaveIngresoPadre(claveIngresoCunero);
    				if(ingresoCunero.getCveIngresoPadre() != null) {
	    				long claveTococirugia = ingresoCunero.getCveIngresoPadre();
	    				if (!datosIngresoRN.isAlojamientoPrevio()) {
	    					capturarIngresoRepository.actualizarIndicadorRecienNacido(claveTococirugia,
	    							datosIngresoRN.getNumeroRN());
	    				}
    				}
    				datosIngresoRN.setEspecialidadIngreso(BaseConstants.ESPECIALIDAD_UCI_NEONATOS);
    				Ingreso ingresoUcin = capturarIngresoHelper.prepararIngresoRecienNacido(datosIngresoRN, tipoFormato,
    						datosUsuario);
    				long claveIngresoUcin = capturarIngresoRepository.guardarIngreso(ingresoUcin);
    				capturarIngresoRepository.actualizarNumRecienNacido(ingresoUcin.getNumRecienNacido(),claveIngresoUcin);
    				logger.debug(LOGGER_INGRESO_CREADO + claveIngresoUcin);
    			} else {
    				List<Ingreso> ingresoVigente = null;
    				if(datosIngresoRN.getClavePaciente() != null){
	    				ingresoVigente = capturarIngresoRepository
	                            .buscarIngresoVigenteCuneroPatologico(datosIngresoRN.getClavePaciente());
    				}
    				//List<CamaUnidad> camaAnterior = capturarIngresoRepository.buscarCamaUnidadRN(
                      //      datosUsuario.getCvePresupuestal(), ingresoVigente.get(0).getRefCamaRn());
    				
    				List<CamaUnidad> camaAnterior = obtenerCamaUnidadRN(datosUsuario.getCvePresupuestal(), 
    						ingresoVigente.get(0).getRefCamaRn(), ingresoVigente.get(0).getCveEspecialidadCama());
    				
    				actualizarOcupacionCama(camaAnterior.get(0), BaseConstants.QUITAR_PACIENTE);
    				capturarIngresoRepository.registrarEventoBitacoraCama(camaAnterior.get(0),
                              TipoFormatoEnum.parse(tipoFormato), hospitalizacionCommonHelper.convertStringToDate(datosIngresoRN.getFechaIngreso()),
                              BaseConstants.CAMA_DESOCUPADA);
    				capturarIngresoRepository.actualizarCamaRecienNacido(null,null,datosIngresoRN.getClaveIngresoPadre());
    				logger.debug("Ingreso Padre - cveIngresoPadre: " + datosIngresoRN.getClaveIngresoPadre());
    				Ingreso ingresoUcin = capturarIngresoHelper.prepararIngresoRecienNacido(datosIngresoRN, tipoFormato,
    						datosUsuario);
    				long claveIngresoUcin = capturarIngresoRepository.guardarIngreso(ingresoUcin);
    				capturarIngresoRepository.actualizarNumRecienNacido(ingresoUcin.getNumRecienNacido(),claveIngresoUcin);
    				logger.debug(LOGGER_INGRESO_CREADO + claveIngresoUcin);
    			}
    		} else {
    			Ingreso ingreso = capturarIngresoHelper.prepararIngresoRecienNacido(datosIngresoRN, tipoFormato,
    					datosUsuario);
    			Long cvePaciente = datosIngresoRN.getClavePaciente();
    			if (null == cvePaciente || cvePaciente == 0) {
    				Paciente paciente = capturarIngresoHelper.prepararPacienteRN(datosIngresoRN,
    						null);
    				cvePaciente = getBuscarPacienteService().guardarPaciente(paciente);
    				datosIngresoRN.setClavePaciente(cvePaciente);
    				logger.debug("Paciente creado - cvePaciente: " + cvePaciente);
    			} else {
    				logger.debug("Paciente encontrado - cvePaciente: " + cvePaciente);
    			}
    			ingreso.setCvePaciente(cvePaciente);
    			long claveIngreso = capturarIngresoRepository.guardarIngreso(ingreso);
    			capturarIngresoRepository.actualizarNumRecienNacido(ingreso.getNumRecienNacido(),claveIngreso);
    			logger.debug(LOGGER_INGRESO_CREADO + claveIngreso);
    			if (!datosIngresoRN.isAlojamientoPrevio()) {
    				capturarIngresoRepository.actualizarIndicadorRecienNacido(ingreso.getCveIngresoPadre(),
    						datosIngresoRN.getNumeroRN());
    			}
    		}
    		if(!datosIngresoRN.getEspecialidadIngreso().substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS)
    				.equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO.substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS))) {
    			List<CamaUnidad> camasUnidad = capturarIngresoRepository.buscarCamaUnidad(datosUsuario.getCvePresupuestal(),
    					datosIngresoRN.getCamaRN(),
    					datosIngresoRN.getEspecialidadCamaRN());
    			CamaUnidad cama = camasUnidad.get(0);
    			List<OcupacionCama> ocupacion = capturarIngresoRepository.buscarOcupacionCama(cama.getCveCama(),
    					cama.getCvePresupuestal(), cama.getCveEspecialidad());
    			if (ocupacion == null || ocupacion.isEmpty()) {
    				capturarIngresoRepository.guardarOcupacionCama(cama);
    				logger.debug("Cama ocupada: " + cama.getCveCama() + " - Pacientes: " + 1);
    			} else {
    				actualizarOcupacionCama(cama, BaseConstants.AGREGAR_PACIENTE);
    				logger.debug("Cama ocupada: " + cama.getCveCama() + " - Pacientes: "
    						+ (ocupacion.get(0).getNumPacientes() + 1));
    			}
                capturarIngresoRepository.registrarEventoBitacoraCama(cama, TipoFormatoEnum.parse(tipoFormato), hospitalizacionCommonHelper.convertStringToDate(datosIngresoRN.getFechaIngreso()),
                        BaseConstants.CAMA_OCUPADA);
    		}else{   			
    			actualizaOcupacionCamaAlojamiento(datosUsuario.getCvePresupuestal(), datosIngresoRN);   					
    		}
    		    		
    	} catch (Exception e) {
    		logger.error("Error al guardar Ingreso RN: ", e);
    		throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_020), e);
    	}
    }
    
    public void actualizaOcupacionCamaAlojamiento(String clavePresupestal, DatosIngresoRN datosIngresoRN){
    	
    	List<TotalCamaUnidad> camasAlojamiento = capturarIngresoRepository.buscarTotalCamaAlojamiento(clavePresupestal);
    	
    	if(camasAlojamiento != null && !camasAlojamiento.isEmpty()){
			TotalCamaUnidad camaAlojamiento = camasAlojamiento.get(0);
			List<OcupacionCamaAlojamiento> ocupacionAlojamiento = capturarIngresoRepository.buscarOcupacionAlojamiento(camaAlojamiento.getCvePresupuestal());
				if(ocupacionAlojamiento == null || ocupacionAlojamiento.isEmpty()){
					capturarIngresoRepository.guardarOcupacionCamaAlojamiento(camaAlojamiento, 
							hospitalizacionCommonHelper.convertStringToDate(datosIngresoRN.getFechaIngreso()));
					logger.debug("Se ocupo una cama de especialidad: " + camaAlojamiento.getCveEspecialidad() + " - Pacientes: " + 1);
				}else{
					if(!capturarIngresoRepository.existenIngresosRNGemelos(datosIngresoRN.getNss(),datosIngresoRN.getClavePaciente(),clavePresupestal)){
						int numPacientes = (ocupacionAlojamiento.get(0).getNumOcupacionCama()) + 1;
						capturarIngresoRepository.actualizaOcupacionCamaAlojamiento(numPacientes,
								hospitalizacionCommonHelper.convertStringToDate(datosIngresoRN.getFechaIngreso()),camaAlojamiento);
						logger.debug("Se ocupo una cama de especialidad: " + camaAlojamiento.getCveEspecialidad() + " - Pacientes: " + numPacientes);
				}
	    	}
    	}
    }
    
    public List<CamaUnidad> obtenerCamaUnidadRN(String clavePresupuestal, String claveCama, String claveEspecialidadCama){
    	List<CamaUnidad> camasUnidad = new ArrayList<CamaUnidad>();
    	if(claveEspecialidadCama != null && !claveEspecialidadCama.isEmpty()){
    		camasUnidad = capturarIngresoRepository.buscarCamaUnidad(clavePresupuestal, claveCama, 
    				claveEspecialidadCama);
    	}else{
    		camasUnidad = capturarIngresoRepository.buscarCamaUnidadRN(clavePresupuestal, claveCama);
    	}
    	
    	return camasUnidad;
    }

    
    @Override
    public List<DatosIngresoRN> validadCamaMadre(List<DatosIngresoRN> recienNacidosNuevos, String clavePresupuestal) {
    	
    	List<Integer> tipoFormato = new ArrayList<>();
    	tipoFormato.add(TipoFormatoEnum.INGRESO_UCI.getClave());
    	tipoFormato.add(TipoFormatoEnum.INGRESO_URGENCIAS.getClave());
    	tipoFormato.add(TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave());
    	
    	List<Ingreso> ingresoMadre = null;
    	for(DatosIngresoRN recienNacido:recienNacidosNuevos){
    		if(recienNacido.getCama() == null || recienNacido.getCama().isEmpty()){			
    			ingresoMadre = capturarIngresoRepository.buscarIngresoMadre(clavePresupuestal,
    					recienNacido.getNss(), recienNacido.getAgregadoMedico(), tipoFormato);
    			if(ingresoMadre != null){
    				recienNacido.setCama(ingresoMadre.get(0).getCveCama());
    				recienNacido.setCamaRN(BaseConstants.PREFIJO_CAMA_RN + ingresoMadre.get(0).getCveCama());
    			} 			
    		}
    	}
    	
    	return recienNacidosNuevos;
    }
    
    @Override
    public List<DatosIngresoRN> buscarRecienNacidosNss(String nss, String clavePresupuestal) {

        List<DatosIngresoRN> recienNacidos = new ArrayList<>();
        List<DatosIngresoRN> recienNacidosNuevos = capturarIngresoRepository.buscarRecienNacidosNss(nss,
                clavePresupuestal);
        recienNacidos.addAll(validadCamaMadre(recienNacidosNuevos,clavePresupuestal));
        List<DatosIngresoRN> recienNacidosVigentes = capturarIngresoRepository
                .buscarRecienNacidosIngresoVigenteCuneroNoVigenteAlojamiento(nss, clavePresupuestal);
        recienNacidos.addAll(filtrarRecienNacidosVigentes(recienNacidosVigentes));
        if (!recienNacidos.isEmpty()) {
            for (int i = 0; i < recienNacidos.size(); i++) {
                recienNacidos.get(i).setConsecutivo(i + 1);
              //Se valida si tiene el tipo de egreso es cunero patologico
                if(recienNacidos.get(i).getClaveTipoFormato()!= TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue() 
                		&& recienNacidos.get(i).getClaveTipoFormato()!= TipoFormatoEnum.INGRESO_RN_UCIN.getClave().intValue()
                		&& recienNacidos.get(i).getClaveTipoFormato()!= TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()){
                	recienNacidos.get(i).setEspecialidadIngreso(null);
                }
                
            }
        }
        return recienNacidos;
    }

    @Override
    public List<DatosIngresoRN> buscarRecienNacidosCama(String cama, String clavePresupuestal) {

        List<DatosIngresoRN> recienNacidos = new ArrayList<>();
        List<DatosIngresoRN> recienNacidosNuevos = capturarIngresoRepository.buscarRecienNacidosCama(cama,
                clavePresupuestal);
        recienNacidos.addAll(recienNacidosNuevos);
        List<DatosIngresoRN> recienNacidosVigentes = capturarIngresoRepository
                .buscarRecienNacidosCamaIngresoVigenteCuneroNoVigenteAlojamiento(cama, clavePresupuestal);
        recienNacidos.addAll(filtrarRecienNacidosVigentes(recienNacidosVigentes));
        if (!recienNacidos.isEmpty()) {
            for (int i = 0; i < recienNacidos.size(); i++) {
                recienNacidos.get(i).setConsecutivo(i + 1);
                //Se valida si el tipo de egreso es cunero patologico
                if(recienNacidos.get(i).getClaveTipoFormato()!= TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue() 
                		&& recienNacidos.get(i).getClaveTipoFormato()!= TipoFormatoEnum.INGRESO_RN_UCIN.getClave().intValue()
                		&& recienNacidos.get(i).getClaveTipoFormato()!= TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()){
                	recienNacidos.get(i).setEspecialidadIngreso(null);
                }
               
            }
        }
        return recienNacidos;
    }

    private List<DatosIngresoRN> filtrarRecienNacidosVigentes(List<DatosIngresoRN> recienNacidosVigentes) {

        List<DatosIngresoRN> recienNacidos = new ArrayList<>();
        for (DatosIngresoRN rnVigente : recienNacidosVigentes) {
            rnVigente.setCamaRN(null);
            if (rnVigente.getClaveTipoFormato() == TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue()) {
                List<Ingreso> ingresoVigente = capturarIngresoRepository
                        .buscarIngresoVigenteUcin(rnVigente.getClavePaciente());
                if (ingresoVigente.isEmpty()) {
                    rnVigente.setCuneroVigente(Boolean.TRUE);
                    recienNacidos.add(rnVigente);
                }
            } else {
                boolean cunero = false;
                for (DatosIngresoRN rn : recienNacidos) {
                    if (rn.getClavePaciente().equals(rnVigente.getClavePaciente())) {
                        cunero = true;
                    }
                }
                if (!cunero) {
                    List<Ingreso> ingresoVigente = capturarIngresoRepository
                            .buscarIngresoVigenteCunero(rnVigente.getClavePaciente());
                    if (ingresoVigente.isEmpty()) {
                        rnVigente.setAlojamientoPrevio(Boolean.TRUE);
                        recienNacidos.add(rnVigente);
                    }
                }
            }
        }
        if (!recienNacidos.isEmpty()) {
            List<DatosIngresoRN> recienNacidosValidos = new ArrayList<>();
            for (DatosIngresoRN rn : recienNacidos) {
                if (rn.getClaveTipoFormato() == TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue()) {
                    recienNacidosValidos.add(rn);
                } else {
                    List<Ingreso> ingresoNoVigente = capturarIngresoRepository
                            .buscarIngresoNoVigenteCunero(rn.getClavePaciente());
                    if (ingresoNoVigente.isEmpty()) {
                        recienNacidosValidos.add(rn);
                    }
                }
            }
            return recienNacidosValidos;
        }
        return recienNacidos;
    }
    
    @Override
    public void validarCamaAlojamientoConjunto(String nss, Long cvePaciente, String clavePresupuestal) throws HospitalizacionException{
    	
    	if(!capturarIngresoRepository.existenIngresosRNGemelos(nss,cvePaciente,clavePresupuestal)){
    		
    		List<TotalCamaUnidad> totalCamas = capturarIngresoRepository.buscarTotalCamaAlojamiento(clavePresupuestal);
    		List<OcupacionCamaAlojamiento> ocupacionAlojamiento = capturarIngresoRepository.buscarOcupacionAlojamiento(clavePresupuestal);
    		
    		if(totalCamas != null && !totalCamas.isEmpty() 
    				&& ocupacionAlojamiento != null && !ocupacionAlojamiento.isEmpty() ){
    		capturarIngresoRules.validaCamaAlojamientoConjunto(totalCamas.get(0).getCanCamaCensable(),
    				ocupacionAlojamiento.get(0).getNumOcupacionCama());
    		}
    	}
    }

    @Override
    public void validarCamaMadre(String cama) throws HospitalizacionException {

        capturarIngresoRules.validarCamaMadre(cama);
    }

    @Override
    public void validarFechaAnterior(String fechaIngreso, String horaIngreso, Date fechaParto,
            TipoUbicacionEnum tipoUbicacion, int claveTipoFormato) throws HospitalizacionException {

        capturarIngresoRules.validarFechaAnterior(fechaIngreso, horaIngreso, fechaParto, tipoUbicacion,
                claveTipoFormato);
    }

    @Override
    public Integer obtenerDivisionHospitalaria(String claveEspecialidad) {

        return capturarIngresoRepository.buscarEspecialidad(claveEspecialidad).getCveDivision();
    }

    @Override
    public void validarEspecialidadInicio(List<DatosCatalogo> catalogoEspecialidad) throws HospitalizacionException {

        capturarIngresoRules.validarEspecialidadInicio(catalogoEspecialidad);
    }

    @Override
    public List<DatosCatalogo> filtrarCatalogoEspecialidadRN(List<DatosCatalogo> especialidades,
            Integer claveTipoFormato) {

        return capturarIngresoHelper.filtrarCatalogoEspecialidad(especialidades, claveTipoFormato);
    }
    
    @Override
    public List<DatosCatalogo> filtrarCatalogoEspHospRN(List<DatosCatalogo> especialidades) {

        return capturarIngresoHelper.filtrarCatalogoEspecialidad(especialidades);
    }

    public AccederRules getAccederRules() {

        return accederRules;
    }

    public CapturarIngresoHelper getCapturarIngresoHelper() {

        return capturarIngresoHelper;
    }

    public CapturarIngresoRepository getCapturarIngresoRepository() {

        return capturarIngresoRepository;
    }

    public CapturarIngresoRules getCapturarIngresoRules() {

        return capturarIngresoRules;
    }
    
    public Boolean validarEspecialidadIndPediaria(String claveEspecialidad, String cvePresupuestal) {

        return capturarIngresoRepository.validarEspecialidadIndPediaria(claveEspecialidad, cvePresupuestal);
    }
    
    public int obtenerNumSemanasMaxPediatria(String claveEspecialidad) {

        return capturarIngresoRepository.obtenerNumSemanasMaxPediatria(claveEspecialidad);
    }
    
}
