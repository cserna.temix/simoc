package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.PersonalOperativo;

public class PersonalOperativoRowMapperHelper extends BaseHelper implements RowMapper<PersonalOperativo> {

    @Override
    public PersonalOperativo mapRow(final ResultSet resultSet, final int rowNumber) throws SQLException {

        PersonalOperativo personalOperativo = new PersonalOperativo();

        personalOperativo.setRefPersonalOperativo(resultSet.getString(SQLColumnasConstants.REF_PERSONAL_OPERATIVO));
        personalOperativo.setCveMatricula(resultSet.getString(SQLColumnasConstants.CVE_MATRICULA_PERSONAL_OPERATIVO));
        personalOperativo.setRefNombre(resultSet.getString(SQLColumnasConstants.REF_NOMBRE_PERSONAL_OPERATIVO));
        personalOperativo.setRefApellidoPaterno(
                resultSet.getString(SQLColumnasConstants.REF_APELLIDO_PATERNO_PERSONAL_OPERATIVO));
        personalOperativo.setRefApellidoMaterno(
                resultSet.getString(SQLColumnasConstants.REF_APELLIDO_MATERNO_PERSONAL_OPERATIVO));
        personalOperativo.setRefEmail(resultSet.getString(SQLColumnasConstants.REF_EMAIL));
        personalOperativo.setRefNumTelefono(resultSet.getString(SQLColumnasConstants.REF_NUM_TELEFONO));
        personalOperativo.setRefNumExtension(resultSet.getString(SQLColumnasConstants.REF_NUM_EXTENSION));
        personalOperativo.setIndActivo(resultSet.getInt(SQLColumnasConstants.IND_ACTIVO_PERSONAL_OPERATIVO));
        personalOperativo.setIndBloqueado(resultSet.getInt(SQLColumnasConstants.IND_BLOQUEADO));
        personalOperativo.setCanIntentos(resultSet.getInt(SQLColumnasConstants.CAN_INTENTOS));
        personalOperativo.setFecBaja(resultSet.getDate(SQLColumnasConstants.FEC_BAJA_PERSONAL_OPERATIVO));

        return personalOperativo;
    }
}
