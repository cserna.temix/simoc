/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.NacidosVo;

/**
 * @author francisco.rodriguez
 *
 */
public class NacidosRowMapperHelper extends BaseHelper implements RowMapper<NacidosVo> {

	@Override
	public NacidosVo mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		NacidosVo nacidosPojo = new NacidosVo();
		
		nacidosPojo.setDefuncion(resultSet.getInt(SQLColumnasConstants.DEFUNCION));
		nacidosPojo.setMortinato(resultSet.getInt(SQLColumnasConstants.MORTINATO));
		nacidosPojo.setAborto(resultSet.getInt(SQLColumnasConstants.ABORTO));
		nacidosPojo.setVivo(resultSet.getInt(SQLColumnasConstants.VIVO));
		
		return nacidosPojo;
	}

}
