package mx.gob.imss.simo.hospitalizacion.ingresos.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.model.DatosUsuario;

public interface CapturarIngresoServices extends HospitalizacionCommonServices {

    void validarFechaIngreso(String fechaIngreso, String clavePresupuestal) throws HospitalizacionException;

    void validarNss(String nss) throws HospitalizacionException;

    void validarIngresoVigente(String clavePresupuestal, TipoUbicacionEnum tipoUbicacion, DatosPaciente datosPaciente)
            throws HospitalizacionException;
    
    List<Ingreso> validarIngresoVigenteRecienNacido(String clavePresupuestal,DatosIngresoRN datosRN) throws HospitalizacionException;
    
    List<Long> buscarRNExterno(DatosIngresoRN datosRN)throws HospitalizacionException;

    Ingreso buscarIngresoVigenteHospitalizacion(String clavePresupuestal, DatosPaciente datosPaciente)
            throws HospitalizacionException;

    boolean habilitarEdad(String fechaIngreso, String anio);

    Map<TipoCalculoEdadEnum, Integer> calcularEdadPacienteNoEncontrado(String anio, String fechaIngreso);
    
    void calcularEdadPacienteEncontrado(DatosPaciente datosPaciente, String fechaIngreso);

    void validarEdad(String edad) throws HospitalizacionException;

    void validarNombre(String nombre) throws HospitalizacionException;

    void validarApellidoPaterno(String apellidoPaterno) throws HospitalizacionException;

    void validarNumero(String numero) throws HospitalizacionException;

    List<DatosCatalogo> obtenerCatalogoEspecialidad(TipoUbicacionEnum tipoUbicacion, String clavePresupuestal);

    List<DatosCatalogo> filtrarCatalogoTipoIngreso(List<DatosCatalogo> tiposIngreso, boolean ingresoVigente);

    void validarTipoIngreso(String tipoIngreso, List<DatosCatalogo> catalogoTipos) throws HospitalizacionException;

    void validarTipoPrograma(String tipoPrograma, List<DatosCatalogo> catalogoTipos) throws HospitalizacionException;

    boolean esPeriodoExtemporaneo(Date fechaCaptura, String clavePresupuestal) throws HospitalizacionException;

    void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario, TipoUbicacionEnum tipoUbicacion)
            throws HospitalizacionException;

    Ingreso guardarIngreso(DatosHospitalizacion datosHospitalizacion, TipoUbicacionEnum tipoUbicacion,
            DatosUsuario datosUsuario, CamaUnidad camaUnidad, boolean ingresoPadre);

    void guardarIngresoRecienNacido(DatosIngresoRN datosIngresoRN, DatosUsuario datosUsuario)
            throws HospitalizacionException;

    List<DatosIngresoRN> buscarRecienNacidosNss(String nss, String clavePresupuestal);

    List<DatosIngresoRN> buscarRecienNacidosCama(String cama, String clavePresupuestal);
    
    List<DatosIngresoRN> validadCamaMadre(List<DatosIngresoRN> recienNacidosNuevos, String clavePresupuestal);

    void validarCamaMadre(String cama) throws HospitalizacionException;

    void validarFechaAnterior(String fechaIngreso, String horaIngreso, Date fechaAnterior,
            TipoUbicacionEnum tipoUbicacion, int claveTipoFormato) throws HospitalizacionException;

    Integer obtenerDivisionHospitalaria(String claveEspecialidad);

    void validarEspecialidadInicio(List<DatosCatalogo> catalogoEspecialidad) throws HospitalizacionException;

    List<DatosCatalogo> filtrarCatalogoEspecialidadRN(List<DatosCatalogo> especialidades, Integer claveTipoFormato);
    
    List<DatosCatalogo> filtrarCatalogoEspHospRN(List<DatosCatalogo> especialidades);
    
    public Boolean validarEspecialidadIndPediaria(String claveEspecialidad, String cvePresupuestal);
    
    public int obtenerNumSemanasMaxPediatria(String claveEspecialidad);
    
    void validarCamaAlojamientoConjunto(String nss, Long cvePaciente, String clavePresupuestal) throws HospitalizacionException;

}
