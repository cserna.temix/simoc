package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class EgresoRecienNacido {

    private Long cvePaciente;

}
