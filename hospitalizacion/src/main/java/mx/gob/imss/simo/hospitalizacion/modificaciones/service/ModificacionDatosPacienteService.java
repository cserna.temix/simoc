package mx.gob.imss.simo.hospitalizacion.modificaciones.service;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;

public interface ModificacionDatosPacienteService extends HospitalizacionCommonServices {

    void validarNss(String nss) throws HospitalizacionException;

    void validarNumero(String numero) throws HospitalizacionException;

    void validarDelegacion(String numero, String delegacion, List<DatosCatalogo> delegaciones)
            throws HospitalizacionException;

    void guardarModificacionDatosPacienteService(DatosPaciente datosModificados, String nssOriginal, String agregado)
            throws HospitalizacionException;

    void validarListaVaciaDatosHopitalizacion(List<DatosPaciente> list, String nss) throws HospitalizacionException;

    void validarIngresoVigente(String clavePresupuestal, TipoUbicacionEnum tipoUbicacion, DatosPaciente datosPaciente)
            throws HospitalizacionException;

    void validarUbicacion(String ubicacion) throws HospitalizacionException;
    
    void validarFechaModificacion(String fechaModificacion, String cvePresupuestal) throws HospitalizacionException;
    
    void validarFechaModificacionMayorIngreso(DatosHospitalizacion datosHospitalizacion, String fechaModificacion)
            throws HospitalizacionException;
    
    void validarFechaModificacionMayorTococirugia(DatosHospitalizacion datosHospitalizacion, String fechaModificacion)
            throws HospitalizacionException;
    
    void validarFechaModificacionMayorIntervenciones(DatosHospitalizacion datosHospitalizacion, String fechaModificacion)
            throws HospitalizacionException;
    
    void validarFechaModificacionMayorMovIntra(DatosHospitalizacion datosHospitalizacion, String fechaModificacion)
            throws HospitalizacionException;
    

}
