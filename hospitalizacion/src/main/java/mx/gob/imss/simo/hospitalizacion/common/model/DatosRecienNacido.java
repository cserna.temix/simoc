package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class DatosRecienNacido {

    private Integer idNacido;
    private Integer apgar1;
    private Integer apgar5;
    private String horaDefuncion;
    private String nacido;
    private Integer perimetroCefalico;
    private Integer peso;
    private Integer semanasGestacion;
    private Integer sexo;
    private Integer talla;
    private String nombreMadre;
    private String apPaternoMadre;
    private String apMaternoMadre;
    private String alta;
    private Long cvePaciente;

}
