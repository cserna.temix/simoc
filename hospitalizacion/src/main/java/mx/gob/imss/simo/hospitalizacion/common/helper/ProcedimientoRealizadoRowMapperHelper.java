package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientoRealizado;

public class ProcedimientoRealizadoRowMapperHelper extends BaseHelper implements RowMapper<ProcedimientoRealizado> {

    @Override
    public ProcedimientoRealizado mapRow(ResultSet resultSet, int rowId) throws SQLException {

        ProcedimientoRealizado procedimientoRealizado = new ProcedimientoRealizado();

        procedimientoRealizado.setClaveIngreso(resultSet.getString(SQLColumnasConstants.CLAVE_INGRESO));
        procedimientoRealizado.setClaveCie9(resultSet.getString(SQLColumnasConstants.CLAVE_CIE9));
        procedimientoRealizado.setDescripcionCie9(resultSet.getString(SQLColumnasConstants.DESCRIPCION_CIE9));
        procedimientoRealizado.setFechaIntervencion(resultSet.getDate(SQLColumnasConstants.FECHA_INTERVENCION));
        procedimientoRealizado.setFechaIngreso(resultSet.getDate(SQLColumnasConstants.FECHA_INGRESO));
        procedimientoRealizado.setFechaEgreso(resultSet.getDate(SQLColumnasConstants.FECHA_EGRESO));
        procedimientoRealizado.setAgregadoMedico(resultSet.getString(SQLColumnasConstants.AGREGADO_MEDICO));
        procedimientoRealizado.setNss(resultSet.getString(SQLColumnasConstants.NSS));
        procedimientoRealizado.setNombreCompleto(resultSet.getString(SQLColumnasConstants.NOMBRE_COMPLETO));
        return procedimientoRealizado;
    }

}
