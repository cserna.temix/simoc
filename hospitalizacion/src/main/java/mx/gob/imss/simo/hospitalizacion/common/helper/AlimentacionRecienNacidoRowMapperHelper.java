package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.AlimentacionRecienNacido;

public class AlimentacionRecienNacidoRowMapperHelper extends BaseHelper implements RowMapper<AlimentacionRecienNacido> {

    @Override
    public AlimentacionRecienNacido mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        AlimentacionRecienNacido alimentacionRecienNacido = new AlimentacionRecienNacido();

        alimentacionRecienNacido.setClave(resultSet.getInt(SQLColumnasConstants.CVE_ALIMENTACION_RN));
        alimentacionRecienNacido.setDescripcion(resultSet.getString(SQLColumnasConstants.DES_ALIMENTACION_RN));
        alimentacionRecienNacido.setFechaBaja(validarFechaNula(resultSet, SQLColumnasConstants.FEC_BAJA));

        return alimentacionRecienNacido;
    }

}
