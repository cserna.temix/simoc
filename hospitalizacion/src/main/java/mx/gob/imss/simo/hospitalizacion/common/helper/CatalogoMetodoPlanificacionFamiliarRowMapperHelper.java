package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

public class CatalogoMetodoPlanificacionFamiliarRowMapperHelper extends BaseHelper implements RowMapper<DatosCatalogo> {

    @Override
    public DatosCatalogo mapRow(ResultSet resultSet, int rowId) throws SQLException {

        DatosCatalogo catalogo = new DatosCatalogo();
        catalogo.setClave(resultSet.getString(SQLColumnasConstants.CVE_METODO_ANTICONCEPTIVO));
        catalogo.setDescripcion(resultSet.getString(SQLColumnasConstants.DES_METODO_ANTICONCEPTIVO));

        return catalogo;
    }

}
