package mx.gob.imss.simo.hospitalizacion.tococirugia.helper;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.AtencionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.SexoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoNacidoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TococirugiaConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosTococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.model.RecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.model.DatosUsuario;

@Component
public class CapturarTococirugiaHelper extends HospitalizacionCommonHelper {

    public List<DatosRecienNacido> llenarTotalRecienNacido(int totalNacidos) {

        List<DatosRecienNacido> listDatosRecienNacido = new ArrayList<>();
        for (int i = 0; i < totalNacidos; i++) {
            DatosRecienNacido datosRecienNacido = new DatosRecienNacido();
            datosRecienNacido.setIdNacido(i + 1);
            datosRecienNacido.setSexo(1);
            listDatosRecienNacido.add(datosRecienNacido);
        }
        return listDatosRecienNacido;
    }

    public List<DatosRecienNacido> inicializarRecienNacidos() {

        return new ArrayList<>();
    }

    public DatosRecienNacido inicializarDatosRecienNacido(DatosHospitalizacion datosHospitalizacion,
            DatosRecienNacido datosRecienNacido) {

        if (datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido().isEmpty()) {
            datosRecienNacido.setIdNacido(1);

        } else {
            datosRecienNacido
                    .setIdNacido(datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido().size() + 1);
            datosRecienNacido.setSemanasGestacion(
                    datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido().get(0).getSemanasGestacion());
        }
        return datosRecienNacido;
    }

    public List<DatosRecienNacido> agregarRegistroRecienNacido(DatosHospitalizacion datosHospitalizacion,
            DatosRecienNacido datosRecienNacido) {

        DatosRecienNacido datosRecienNacidoAux = prepararDatosRecienNacido(datosRecienNacido.getIdNacido(),
                datosHospitalizacion.getDatosPaciente().getApellidoPaterno(),
                datosHospitalizacion.getDatosPaciente().getApellidoMaterno(), datosRecienNacido);
        datosRecienNacido.setNombreMadre(datosRecienNacidoAux.getNombreMadre());
        datosRecienNacido.setApPaternoMadre(datosRecienNacidoAux.getApPaternoMadre());
        datosRecienNacido.setApMaternoMadre(datosRecienNacidoAux.getApMaternoMadre());
        datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido().add(datosRecienNacido);
        datosHospitalizacion.getDatosTococirugia().setDatosRecienNacidoModal(new DatosRecienNacido());
        return datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido();
    }

    private DatosRecienNacido prepararDatosRecienNacido(int indice, String apPaternoMadre, String apMaternoMadre,
            DatosRecienNacido datosRecienNacido) {

        datosRecienNacido.setNombreMadre(TococirugiaConstants.RECIEN_NACIDO_NOMBRE + indice);
        datosRecienNacido.setApPaternoMadre(apPaternoMadre);
        datosRecienNacido.setApMaternoMadre(apMaternoMadre);
        return datosRecienNacido;
    }

    public List<DatosRecienNacido> prepararListaRecienNacidos(DatosRecienNacido datosRecienNacido,
            List<DatosRecienNacido> listaRecienNacido) {

        listaRecienNacido.add(datosRecienNacido);
        return listaRecienNacido;
    }

    public DatosCatalogo elementoSelecionadoPlanificacion(List<String> listaPlanificacion,
            AutoComplete planificacionComplete, List<DatosCatalogo> catalogoPlanificacion) {

        int indice = listaPlanificacion.indexOf(planificacionComplete.getValue().toString());
        return catalogoPlanificacion.get(indice);
    }

    public RecienNacido prepararRecienNacido(DatosHospitalizacion datosHospitalizacion,
            DatosRecienNacido datosRecienNacido, DatosUsuario datosUsuario, long cvePaciente, int rangoLubchenco) {

        DatosTococirugia datosTococirugia = datosHospitalizacion.getDatosTococirugia();
        RecienNacido recienNacido = new RecienNacido();
        recienNacido.setClave(datosHospitalizacion.getDatosPaciente().getClaveIngreso());
        recienNacido.setNumeroRecienNacido(datosRecienNacido.getIdNacido());
        recienNacido.setFechaCreacion(datosTococirugia.getFecCreacion());
        recienNacido.setFechaActualizacion(datosTococirugia.getFecActualizacion());
        recienNacido.setPeso(datosRecienNacido.getPeso());
        recienNacido.setTalla(datosRecienNacido.getTalla());
        recienNacido.setPerimetroCefalico(datosRecienNacido.getPerimetroCefalico());
        recienNacido.setSemanasGestacion(datosRecienNacido.getSemanasGestacion());
        recienNacido.setApgar1(datosRecienNacido.getApgar1());
        recienNacido.setApgar5(datosRecienNacido.getApgar5());
        if (datosRecienNacido.getNacido().equals(TipoNacidoEnum.DEFUNCION.getClave())) {
            recienNacido.setFechaDefuncion(
                    concatenarFechaHora(datosTococirugia.getFecCreacion(), datosRecienNacido.getHoraDefuncion()));
        } else {
            recienNacido.setFechaDefuncion(null);
        }
        recienNacido.setClaveCapturista(datosUsuario.getUsuario());
        /* capturista actualiza */ recienNacido.setClaveCapturistaActualiza(null);
        recienNacido.setIndicadorIngresoRN(0);
        recienNacido.setClavePaciente(cvePaciente);
        recienNacido.setClaveTipoNacido(Integer.parseInt(datosRecienNacido.getNacido()));
        recienNacido.setClaveRangoLubchenco(rangoLubchenco);
        return recienNacido;
    }

    public Paciente prepararPaciente(DatosPaciente madre, DatosRecienNacido recienNacido, String fecAtencion) {

        Paciente paciente = new Paciente();
        paciente.setRefNss(madre.getNss());
        paciente.setRefAgregadoMedico(formarAgregadoMedicoRN(madre,recienNacido,fecAtencion));
        paciente.setRefNombre(recienNacido.getNombreMadre());
        paciente.setRefApellidoPaterno(recienNacido.getApPaternoMadre());
        paciente.setRefApellidoMaterno(recienNacido.getApMaternoMadre());
        paciente.setNumEdadSemanas(0);
        paciente.setIndAcceder(Boolean.FALSE);
        paciente.setIndVigencia(Boolean.FALSE);
        paciente.setCveSexo(recienNacido.getSexo());
        paciente.setCveUnidadAdscripcion(madre.getClavePresupuestal());
        paciente.setIndRecienNacido(Boolean.TRUE);
        paciente.setIndDerechohabiente(Boolean.FALSE);
        paciente.setIndUltimoRegistro(Boolean.TRUE);
        return paciente;
    }
    
    public String formarAgregadoMedicoRN(DatosPaciente madre, DatosRecienNacido recienNacido, String fecAtencion){
    	
    	String agregadoMedico = "";
    	if(recienNacido.getSexo()== 1){
    		agregadoMedico = "3"+(SexoEnum.MASCULINO.getAgregado())+
    				fecAtencion.substring(6)+madre.getAgregadoMedico().substring(6);	
    	}else{
    		if(recienNacido.getSexo()== 2){
    			agregadoMedico = "3"+(SexoEnum.FEMENINO.getAgregado())+
    					fecAtencion.substring(6)+madre.getAgregadoMedico().substring(6);
    		}else{
    			agregadoMedico = madre.getAgregadoMedico();
    		}
    	}
    	
    	return agregadoMedico;
    }

    public Tococirugia prepararTococirugia(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario) {

        Tococirugia tococirugia = new Tococirugia();
        tococirugia.setClave(datosHospitalizacion.getDatosPaciente().getClaveIngreso());
        tococirugia.setFechaCreacion(datosHospitalizacion.getDatosTococirugia().getFecCreacion());
        tococirugia.setFechaActualizacion(null);
        if (AtencionEnum.PARTO.getClave() == datosHospitalizacion.getDatosTococirugia().getAtencion()) {
            tococirugia.setFechaAtencion(concatenarFechaHora(
                    convertStringToDate(datosHospitalizacion.getDatosTococirugia().getFecAtencion()),
                    datosHospitalizacion.getDatosTococirugia().getHoraParto()));
            if(datosHospitalizacion.getDatosTococirugia().getTipoParto() == TococirugiaConstants.IND_UNO){
            	tococirugia.setEpisiotomia(Integer.parseInt(datosHospitalizacion.getDatosTococirugia().getEpisiotomia()));
            }
        } else {
            tococirugia
                    .setFechaAtencion(convertStringToDate(datosHospitalizacion.getDatosTococirugia().getFecAtencion()));
        }
        tococirugia.setCveSala(datosHospitalizacion.getDatosTococirugia().getNumeroSala());
        tococirugia.setCvePresupuestal(datosUsuario.getCvePresupuestal());
        tococirugia.setCveTipoAtencion(datosHospitalizacion.getDatosTococirugia().getAtencion());
        tococirugia.setCveTipoParto(datosHospitalizacion.getDatosTococirugia().getTipoParto());
        tococirugia.setCveMetodoAnticonceptivo(
                Integer.parseInt(datosHospitalizacion.getDatosTococirugia().getPlanificacionFamiliar()));
        tococirugia.setCantidadMetodoAnticonceptivo(
                datosHospitalizacion.getDatosTococirugia().getCantidadMetodoAnticonceptivo());
        tococirugia.setTotalRecienNacidos(datosHospitalizacion.getDatosTococirugia().getTotalNacidos());
        tococirugia.setCveMedico(datosHospitalizacion.getDatosMedico().getMatricula());
        tococirugia.setCveCapturista(datosUsuario.getUsuario());
        tococirugia.setCveCapturistaActualizada(null);
        tococirugia.setCveTipoFormato(TipoFormatoEnum.TOCOCIRUGIA.getClave());
        tococirugia.setGesta(datosHospitalizacion.getDatosTococirugia().getGesta());
        return tococirugia;
    }
    
    public List<DatosCatalogo> armarCatalogoEpisiotomia() {
    	
    	List<DatosCatalogo> catalogoEpisiotomia = new ArrayList<DatosCatalogo>();
    	for(TococirugiaConstants.EpisiotomiaEnum valor: TococirugiaConstants.EpisiotomiaEnum.values()){
    		DatosCatalogo episiotomia = new DatosCatalogo();
    		episiotomia.setClave(valor.getClave());
    		episiotomia.setDescripcion(valor.getDescripcion());
    		catalogoEpisiotomia.add(episiotomia);
    	}
    	return catalogoEpisiotomia;

      
    }

}
