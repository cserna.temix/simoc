package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoDiagnosticoEdicion;

public class EgresoDiagnosticoEdicionRowMapperHelper extends BaseHelper implements RowMapper<EgresoDiagnosticoEdicion> {

    @Override
    public EgresoDiagnosticoEdicion mapRow(ResultSet resultSet, int rowId) throws SQLException {

        EgresoDiagnosticoEdicion edicion = new EgresoDiagnosticoEdicion();
        edicion.setFechaEgreso(resultSet.getDate(SQLColumnasConstants.FEC_EGRESO));
        edicion.setClaveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD_INGRESO));
        edicion.setDescripcionEspecialidad(resultSet.getString(SQLColumnasConstants.DES_ESPECIALIDAD));
        edicion.setClaveMotivoEgreso(resultSet.getString(SQLColumnasConstants.CVE_MOTIVO_EGRESO));
        edicion.setClaveMotivoAlta(resultSet.getString(SQLColumnasConstants.CVE_MOTIVO_ALTA));
        edicion.setDescripcionMotivoAlta(resultSet.getString(SQLColumnasConstants.DES_MOTIVO_ALTA));
        edicion.setClaveTipoEgreso(resultSet.getString(SQLColumnasConstants.CVE_TIPO_EGRESO));
        edicion.setDescripcionTipoEgreso(resultSet.getString(SQLColumnasConstants.DES_TIPO_EGRESO));
        edicion.setTipoDiagnostico(resultSet.getString(SQLColumnasConstants.CVE_TIPO_DIAGNOSTICO));
        edicion.setClaveDiagnostico(resultSet.getString(SQLColumnasConstants.CVE_CIE10));
        edicion.setDescripcionDiagnostico(resultSet.getString(SQLColumnasConstants.DES_CIE10));
        edicion.setClavePlanificacionFamiliar(resultSet.getInt(SQLColumnasConstants.CVE_METODO_ANTICONCEPTIVO));
        edicion.setDescripcionPlanificacionFamiliar(
                resultSet.getString(SQLColumnasConstants.DES_METODO_ANTICONCEPTIVO));
        edicion.setTipoFormato(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_FORMATO));

        return edicion;
    }

}
