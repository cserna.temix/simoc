package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;

@Data
public class DatosMedico {

    private String matricula;
    private String delegacion;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String estatus;
    private String tipoContratacion;
    private String identificadorConexion;
    private String nombreCompleto;

    public DatosMedico() {

        this.identificadorConexion = IdentificadorConexionEnum.ROJO.getRuta();
    }

}
