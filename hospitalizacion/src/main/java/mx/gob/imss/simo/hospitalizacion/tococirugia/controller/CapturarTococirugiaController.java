package mx.gob.imss.simo.hospitalizacion.tococirugia.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.confirmdialog.ConfirmDialog;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.CloseEvent;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesAdvertenciaConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoNacidoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TococirugiaConstants;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.hospitalizacion.ingresos.services.CapturarIngresoServices;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.services.CapturarIntervencionesQuirurgicasServices;
import mx.gob.imss.simo.hospitalizacion.tococirugia.services.CapturarTococirugiaServices;

@Data
@ViewScoped
@ManagedBean(name = "tococirugiaController")
public class CapturarTococirugiaController extends HospitalizacionCommonController {

    private static final long serialVersionUID = -791532937948066302L;

    @ManagedProperty("#{capturarIngresoServices}")
    private transient CapturarIngresoServices capturarIngresoServices;

    @ManagedProperty("#{capturarTococirugiaServices}")
    private transient CapturarTococirugiaServices capturarTococirugiaServices;

    @ManagedProperty("#{catalogosHospitalizacionServices}")
    private transient CatalogosHospitalizacionServices catalogosHospitalizacionServices;
    
    @ManagedProperty("#{hospitalizacionCommonHelper}")
    private transient HospitalizacionCommonHelper hospitalizacionCommonHelper;
    
    @ManagedProperty("#{capturarIntervencionesQuirurgicasServices}")
    private transient CapturarIntervencionesQuirurgicasServices capturarIntervencionesQuirurgicasService;

    private transient InputText textSala;
    private transient InputMask maskHoraParto;
    private transient SelectOneMenu selectAtencion;
    private transient SelectOneMenu selectTipoParto;
    private transient SelectOneMenu selectEpisiotomia;
    private transient AutoComplete autoCompletePlanificacionFamiliar;

    private transient List<DatosCatalogo> catalogoAtencion;
    private transient List<DatosCatalogo> catalogoTipoParto;
    private transient List<DatosCatalogo> catalogoPlanificacionFamiliar;
    private transient List<DatosCatalogo> catalogoNacido;
    private transient List<DatosCatalogo> catalogoEpisiotomia;
    private Boolean episiotomiaVisible;

    private List<String> listaAtencion;

    private transient DatosCatalogo datosCatalogoAtencion;

    private transient DataTable dataTableRecienNacidos;
    private transient InputText totalNacidos;
    private transient InputText textGesta;
    private transient List<DatosCatalogo> catalogoSexo;
    private int indice;
    private boolean deshabilitarCampo = false;
    private TipoNacidoEnum tipoNacidoEnum;

    private transient SelectOneMenu selectTablaNacido;
    private transient SelectOneMenu selectTablaSexo;
    private transient InputText textTablaPeso;
    private transient InputText textTablaTalla;
    private transient InputText textTablaPerimetroCefalico;
    private transient InputText textTablaSemanasGestacion;
    private transient InputText textTablaApgar1;
    private transient InputText textTablaApgar5;
    private transient InputMask maskTablaHoraDefuncion;

    private boolean banderaParto;
    private int contadormodal;

    private transient ConfirmDialog alertaRecienNacido;
    private transient CommandButton confirmSi;
    private transient CommandButton confirmNo;
    private String modalConfirm;
    private List<String> metodoFiltradoTocoAsociada;

    private Boolean[] requeridos = { Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE,
            Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE };

    @Override
    public String obtenerNombrePagina() {

        // LoginController.setRenderIcons(false);
        return PagesCommonConstants.TOCOCIRUGIA;
    }

    @PostConstruct
    @Override
    public void init() {

        super.init();
        setSubtitulo(obtenerMensaje(TococirugiaConstants.SUBTITULO_TOCOCIRUGIA));
        iniciar();

    }

    private void iniciar() {

        setDatosHospitalizacion(new DatosHospitalizacion());
        setTieneFoco(TococirugiaConstants.FOCO_FECHA_ATENCION);
        iniciarComponentes();
        deshabilitarComponentes();
        cargarListaComplementarias();
        setUbicacion(TipoFormatoEnum.TOCOCIRUGIA.getClave().toString());
        setTipoFormato(TipoFormatoEnum.TOCOCIRUGIA);
        setEpisiotomiaVisible(Boolean.FALSE);
        RequestContext.getCurrentInstance().execute(TococirugiaConstants.OCULTAR_EPISIOTOMIA);
        limpiaCamposRequeridos();
    }

    public void cargarListaComplementarias() {

        setCatalogoAtencion(catalogosHospitalizacionServices.obtenerCatalogoTipoAtencion());
        setCatalogoTipoParto(catalogosHospitalizacionServices.obtenerCatalogoTipoParto());

        setCatalogoNacido(catalogosHospitalizacionServices.obtenerCatalogoTipoNacido());
        setCatalogoSexo(catalogosHospitalizacionServices.obtenerCatalogoSexo());

    }

    private void cargarCatalogoPlanificacion() {

        setCatalogoPlanificacionFamiliar(catalogosHospitalizacionServices.obtenerCatalogoMetodoPlanificacion(
                TipoFormatoEnum.TOCOCIRUGIA.getClave(), getDatosHospitalizacion().getDatosPaciente(), 
                hospitalizacionCommonHelper.convertStringToDate(getDatosHospitalizacion().getDatosTococirugia().getFecAtencion())));
        setListaPlanificacionFamiliar(convertirCatalogoString(getCatalogoPlanificacionFamiliar()));
        //metodoFiltradoTocoAsociada = convertirCatalogoString(getCatalogoPlanificacionFamiliar());
       
    }

    public List<String> completarPlanificacionFamiliar(String cveFiltro) {

        return filtrarListasComplementarias(cveFiltro, TococirugiaConstants.NUMERO_MAXIMO_AUTOCOMPLETE_PLANFICACION,
                getListaPlanificacionFamiliar());
    }

    public void elementoSelecionadoPlanificacion() {

        setDatosCatalogo(capturarTococirugiaServices.elementoSelecionadoPlanificacion(getListaPlanificacionFamiliar(),
                getAutoCompletePlanificacionFamiliar(), getCatalogoPlanificacionFamiliar()));

    }

    public void validarCantidadPlanificacionTococirugia() {

        try {
            setBanderaModalCantidad(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosTococirugia().setCantidadMetodoAnticonceptivo(getNumeroCantidad());
            if (getNumeroCantidad() != null) {
                getDatosHospitalizacion().getDatosTococirugia().setCantidadMetodoAnticonceptivo(getNumeroCantidad());
                validarCantidadPlanificacion(
                        getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar());

                if (isBanderaParto()) {
                    setTieneFoco(getTextMatricula().getClientId());
                } else {
                    setTieneFoco(getTotalNacidos().getClientId());
                }
            } else {
                setTieneFoco(getTextCantidad().getClientId());
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                        getArray(BaseConstants.REQUERIDO_CANTIDAD));
            }

        } catch (HospitalizacionException e) {
        	logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            agregarMensajeError(e);
            setNumeroCantidad(null);
            setTieneFoco(getTextCantidad().getClientId());
        }

    }

    public void validarFechaAtencion() {

        try {
            capturarTococirugiaServices.validarFechaAtencion(
                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion(),
                    obtenerDatosUsuario().getCvePresupuestal());
            setTieneFoco(getTextSala().getClientId());
            getButtonCancelar().setDisabled(Boolean.FALSE);
            requeridos[0] = Boolean.TRUE;
            //habilitarGuardar();
        } catch (HospitalizacionException e1) {
        	logger.error("CapturarTococirugia- Ocurri� un error: "+e1.toString());
            agregarMensajeError(e1);
            getDatosHospitalizacion().getDatosTococirugia().setFecAtencion(TococirugiaConstants.CADENA_VACIA);
            requeridos[0] = Boolean.FALSE;
            setTieneFoco(getMaskFecha().getClientId());
            //habilitarGuardar();
        }
    }

    public void validarSala() {

        try {
            capturarTococirugiaServices.validarSala(obtenerDatosUsuario().getCvePresupuestal(),
                    getDatosHospitalizacion().getDatosTococirugia().getNumeroSala().toUpperCase());
            getDatosHospitalizacion().getDatosTococirugia()
                    .setNumeroSala(getDatosHospitalizacion().getDatosTococirugia().getNumeroSala().toUpperCase());
            setTieneFoco(getTextNss().getClientId());
            requeridos[1] = Boolean.TRUE;
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
        	logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosTococirugia().setNumeroSala(TococirugiaConstants.CADENA_VACIA);
            requeridos[1] = Boolean.FALSE;
            setTieneFoco(getTextSala().getClientId());
            //habilitarGuardar();
        }

    }

    public void validarNss() {

        try {
            capturarTococirugiaServices.validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
            setBanderaNss(Boolean.TRUE);
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            setBanderaNss(Boolean.FALSE);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosPaciente().setNss(TococirugiaConstants.CADENA_VACIA);
            setTieneFoco(getTextNss().getClientId());
        }
    }

    @Override
    public void obtenerPaciente() {

        super.obtenerPaciente();
        if (getListaDatosPaciente().size() == BaseConstants.INDEX_UNO) {
            try {
                capturarTococirugiaServices
                        .validarCandidatoTococirugia(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                requeridos[2] = Boolean.TRUE;
                requeridos[3] = Boolean.TRUE;
                capturarTococirugiaServices.validarFechaIngreso(getListaDatosPaciente().get(0).getFechaIngreso(),
                        getDatosHospitalizacion().getDatosTococirugia().getFecAtencion());
                //habilitarGuardar();
                //setTieneFoco(getSelectAtencion().getClientId());
               seleccionarMetodoAnticonceptivoIQX();
               setTieneFoco(getTextGesta().getClientId());
              
            } catch (HospitalizacionException e) {
                agregarMensajeError(e);
                logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
                resetearCamposPacienteTococirugia();
                requeridos[2] = Boolean.FALSE;
                requeridos[3] = Boolean.FALSE;               
                getDatosHospitalizacion().getDatosTococirugia().setFecAtencion(TococirugiaConstants.CADENA_VACIA);
                setTieneFoco(getMaskFecha().getClientId());
            }
        }
    }
    
   

    @Override
    public void obtenerPacientePorNumeroCama() {

        try {
            capturarTococirugiaServices.validarCamaRequerida(getDatosHospitalizacion().getDatosPaciente().getCama());
            super.obtenerPacientePorNumeroCama();
            if (getListaDatosPaciente().size() == 1) {
                capturarTococirugiaServices
                        .validarCandidatoTococirugia(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                requeridos[2] = Boolean.TRUE;
                requeridos[3] = Boolean.TRUE;
                capturarTococirugiaServices.validarFechaIngreso(getListaDatosPaciente().get(0).getFechaIngreso(),
                        getDatosHospitalizacion().getDatosTococirugia().getFecAtencion());
                //habilitarGuardar();
                //setTieneFoco(getSelectAtencion().getClientId());
                seleccionarMetodoAnticonceptivoIQX();
               setTieneFoco(getTextGesta().getClientId());
               
            } else {
                requeridos[2] = Boolean.FALSE;
                requeridos[3] = Boolean.FALSE;
               // habilitarGuardar();
            }
        } catch (HospitalizacionException e1) {
            agregarMensajeError(e1);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e1.toString());
            if (getDatosHospitalizacion().getDatosPaciente().getCama() == null
                    || getDatosHospitalizacion().getDatosPaciente().getCama().isEmpty()) {
                setTieneFoco(getTextCama().getClientId());
            } else {
                resetearCamposPacienteTococirugia();
                requeridos[2] = Boolean.FALSE;
                requeridos[3] = Boolean.FALSE;
                //habilitarGuardar();
                getDatosHospitalizacion().getDatosTococirugia().setFecAtencion(TococirugiaConstants.CADENA_VACIA);
                setTieneFoco(getMaskFecha().getClientId());
            }
        }
    }
    
    
    public void validarGesta(){
    	try{
    		getDatosHospitalizacion().getDatosTococirugia().setHoraParto(TococirugiaConstants.CADENA_VACIA);
    		capturarTococirugiaServices.validarGesta(getDatosHospitalizacion().getDatosTococirugia().getGesta());    		
    		requeridos[9] = Boolean.TRUE;
    		//setTieneFoco(getSelectAtencion().getClientId());    		
            setTieneFoco(getMaskHoraParto().getClientId());
    		//habilitarGuardar();
    		
    	}catch(HospitalizacionException e){
    		getDatosHospitalizacion().getDatosTococirugia().setGesta(null);
    		requeridos[9] = Boolean.FALSE;  
    		agregarMensajeError(e);   
    		logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
    		setTieneFoco(getTextGesta().getClientId());
    		//habilitarGuardar();  		   		
    		
    	}
    }

    public void validarCama() {

        try {
            capturarTococirugiaServices.validarCama(getDatosHospitalizacion().getDatosPaciente().getCama());
            seleccionarMetodoAnticonceptivoIQX();
            setTieneFoco(getSelectAtencion().getClientId());
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosPaciente().setCama(TococirugiaConstants.CADENA_VACIA);
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            setTieneFoco(getTextCama().getClientId());
        }
    }

    public void validarHoraParto() {

        try {
            capturarTococirugiaServices.validarHoraParto(getDatosHospitalizacion().getDatosPaciente(),
                    getDatosHospitalizacion().getDatosTococirugia().getHoraParto(),
                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion());
            requeridos[6] = Boolean.TRUE;
           // habilitarGuardar();
            //setTieneFoco(getAutoCompletePlanificacionFamiliar().getClientId());
            setTieneFoco(getSelectAtencion().getClientId());
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().setHoraParto(TococirugiaConstants.CADENA_VACIA);
            requeridos[6] = Boolean.FALSE;
            //habilitarGuardar();
            setTieneFoco(getMaskHoraParto().getClientId());
        }
    }

    public void validarAutocompletePlanificacion() {

        try {
            DatosCatalogo datosCatalogo;
            datosCatalogo = validaAutoComplete(getAutoCompletePlanificacionFamiliar(),
                    BaseConstants.LONGITUD_PLANIFICACION, getListaPlanificacionFamiliar(),
                    getCatalogoPlanificacionFamiliar());
            getDatosHospitalizacion().getDatosTococirugia().setCantidadMetodoAnticonceptivo(null);
            setNumeroCantidad(null); 
            getTextCantidad().setValue(BaseConstants.CADENA_VACIA);
            if (null != datosCatalogo) {
                getDatosHospitalizacion().getDatosTococirugia().setPlanificacionFamiliar(datosCatalogo.getClave());
                validarPlanificacionFamiliar(
                        getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar(),
                        hospitalizacionCommonHelper.convertStringToDate(getDatosHospitalizacion().getDatosTococirugia().getFecAtencion()));
                validarModalPlanificacion(getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar());
                validarMetodoIQx();
                if (isBanderaParto()) {
                    setTieneFoco(getTextMatricula().getClientId());
                } else {
                    setTieneFoco(getTotalNacidos().getClientId());
                }
            } else {
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                        getArray(BaseConstants.REQUERIDO_PLANIFICACION));
            }

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            setTieneFoco(getAutoCompletePlanificacionFamiliar().getClientId());
        }
    }
    
    public void validarMetodoIQx() throws HospitalizacionException{
    	if(getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar() != null &&
    			!getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar().isEmpty()){
    		if(getDatosHospitalizacion().getDatosTococirugia().getTienePlanificacionFamiliar()
    				&& getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar().equals(TococirugiaConstants.NUMERO_CERO_STRING) ){
    			getDatosHospitalizacion().getDatosTococirugia().setPlanificacionFamiliar(TococirugiaConstants.CADENA_VACIA);
        		setNumeroCantidad(null); 
        		getAutoCompletePlanificacionFamiliar().resetValue();
    			throw new HospitalizacionException(MensajesErrorConstants.ME_608);
    			
    		}
    		
    	}
    }

    public void validarMatricula() {

        try {
            capturarTococirugiaServices.validarMatricula(getDatosHospitalizacion().getDatosMedico().getMatricula());
            getDatosHospitalizacion().setDatosMedico(capturarTococirugiaServices.buscarPersonalOperativo(
                    getDatosHospitalizacion().getDatosMedico(), obtenerDatosUsuario().getCvePresupuestal()));
            if (getDatosHospitalizacion().getDatosMedico().getIdentificadorConexion()
                    .equals(IdentificadorConexionEnum.ROJO.getRuta())) {
                agregarMensajeAdvertencia(getArray(MensajesAdvertenciaConstants.MA_013), null);
            }
            getButtonGuardar().setDisabled(Boolean.FALSE);
            requeridos[8] = Boolean.TRUE;
            habilitarGuardar();
            setTieneFoco(getButtonGuardar().getClientId());
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosMedico().setMatricula(TococirugiaConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosMedico().setNombreCompleto(TococirugiaConstants.CADENA_VACIA);
            requeridos[8] = Boolean.FALSE;
            habilitarGuardar();
            setTieneFoco(getTextMatricula().getClientId());
        }

    }

    public void validarAtencion() {
    	limpiarCamposValidacionAtencion();
        try {
        	
            capturarTococirugiaServices.validarAtencion(
                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion(), getDatosHospitalizacion());
            requeridos[4] = Boolean.TRUE;
            if (!capturarTococirugiaServices
                    .validarAtencionTipoParto(getDatosHospitalizacion().getDatosTococirugia().getAtencion())) {
            	
            	getSelectTipoParto().setDisabled(Boolean.TRUE);
                //getMaskHoraParto().setDisabled(Boolean.TRUE);            	
                getTotalNacidos().setDisabled(Boolean.TRUE);
                requeridos[5] = Boolean.TRUE;
                requeridos[6] = Boolean.TRUE;
                requeridos[7] = Boolean.TRUE;
                requeridos[10] = Boolean.TRUE;
                //habilitarGuardar();
                if(getAutoCompletePlanificacionFamiliar().isDisabled()){
                	setTieneFoco(getTextMatricula().getClientId());
                }else{
                	setTieneFoco(getAutoCompletePlanificacionFamiliar().getClientId());
                }                
                setBanderaParto(Boolean.TRUE);
            } else {
            	getSelectTipoParto().setDisabled(Boolean.FALSE);
                //getMaskHoraParto().setDisabled(Boolean.FALSE);
                getTotalNacidos().setDisabled(Boolean.FALSE);
                requeridos[5] = Boolean.FALSE;
                requeridos[6] = Boolean.TRUE;
                requeridos[7] = Boolean.FALSE;
                requeridos[10] = Boolean.FALSE;
                //habilitarGuardar();               
                setTieneFoco(getSelectTipoParto().getClientId());
                setBanderaParto(Boolean.FALSE);      

            }
            
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().setAtencion(null);
            requeridos[4] = Boolean.FALSE;
            //habilitarGuardar();
            setTieneFoco(getSelectAtencion().getClientId());
        }

    }

    private void limpiarCamposValidacionAtencion() {

        getDatosHospitalizacion().getDatosTococirugia().setTipoParto(null);
        //getDatosHospitalizacion().getDatosTococirugia().setHoraParto(TococirugiaConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosTococirugia().setTotalNacidos(null);
        getDatosHospitalizacion().getDatosTococirugia().setListDatosRecienNacido(null);
        ocultarEpisiotomia();
       

    }

    public void validarTipoParto() {

        try {
            capturarTococirugiaServices
                    .validarTipoParto(getDatosHospitalizacion().getDatosTococirugia().getTipoParto());
            requeridos[5] = Boolean.TRUE;
            //habilitarGuardar();
            if(getDatosHospitalizacion().getDatosTococirugia().getTipoParto()== TococirugiaConstants.IND_UNO){
            	mostrarEpisiotomia();
            	setTieneFoco(getSelectEpisiotomia().getClientId());
            }else{
            	ocultarEpisiotomia();
            	if(getAutoCompletePlanificacionFamiliar().isDisabled()){
            		setTieneFoco(getTotalNacidos().getClientId());
            	}else{
            		setTieneFoco(getAutoCompletePlanificacionFamiliar().getClientId());
            	}
            	
            }
            
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            requeridos[5] = Boolean.FALSE;
            //habilitarGuardar();
            setTieneFoco(getSelectTipoParto().getClientId());
        }
    }
    
    public void mostrarEpisiotomia(){
        setEpisiotomiaVisible(Boolean.TRUE);
    	RequestContext.getCurrentInstance().execute(TococirugiaConstants.MOSTRAR_EPISIOTOMIA);
        setCatalogoEpisiotomia(capturarTococirugiaServices.obtenerCatalogoEpisiotomia());
        requeridos[10] = Boolean.FALSE;
       
    }
    
    public void ocultarEpisiotomia(){
    	setEpisiotomiaVisible(Boolean.FALSE);
    	RequestContext.getCurrentInstance().execute(TococirugiaConstants.OCULTAR_EPISIOTOMIA);
    	requeridos[10] = Boolean.TRUE;
        getDatosHospitalizacion().getDatosTococirugia().setEpisiotomia(TococirugiaConstants.CADENA_VACIA);

    }
    
    public void validarEpisiotomia(){
    	
    	try {
			capturarTococirugiaServices.validarEpisiotomia(getDatosHospitalizacion().
					getDatosTococirugia().getEpisiotomia());
			requeridos[10] = Boolean.TRUE;
			if(getAutoCompletePlanificacionFamiliar().isDisabled()){
        		setTieneFoco(getTotalNacidos().getClientId());
        	}else{
        		setTieneFoco(getAutoCompletePlanificacionFamiliar().getClientId());
        	}
			
			
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
			requeridos[10] = Boolean.FALSE;
			//habilitarGuardar();
			getDatosHospitalizacion().getDatosTococirugia().setEpisiotomia(TococirugiaConstants.CADENA_VACIA);
	        getSelectEpisiotomia().resetValue();
			setTieneFoco(getSelectEpisiotomia().getClientId());
		}
    	
    }

    public void validarTablaSexo() {

        getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().get(0).getSexo();
    }

    @Override
    public void mapearDatosPaciente() {

        super.mapearDatosPaciente();
        try {
            capturarTococirugiaServices
                    .validarCandidatoTococirugia(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
            requeridos[2] = Boolean.TRUE;
            requeridos[3] = Boolean.TRUE;
            capturarTococirugiaServices.validarFechaIngreso(
                    getDatosHospitalizacion().getDatosPaciente().getFechaIngreso(),
                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion());
            //setTieneFoco(getSelectAtencion().getClientId());
            seleccionarMetodoAnticonceptivoIQX();
            setTieneFoco(getTextGesta().getClientId());
            
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            resetearCamposPacienteTococirugia();
            requeridos[2] = Boolean.FALSE;
            requeridos[3] = Boolean.FALSE;
            getDatosHospitalizacion().getDatosTococirugia().setFecAtencion(TococirugiaConstants.CADENA_VACIA);
            setTieneFoco(getMaskFecha().getClientId());
        }
    }

    private void resetearCamposPacienteTococirugia() {

        getDatosHospitalizacion().getDatosPaciente().setNss(TococirugiaConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setNombre(TococirugiaConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setAgregadoMedico(TococirugiaConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setApellidoPaterno(TococirugiaConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setApellidoMaterno(TococirugiaConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setCama(TococirugiaConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosTococirugia().setNumeroSala(TococirugiaConstants.CADENA_VACIA);
        getTextNss().setDisabled(Boolean.FALSE);
        getTextCama().setDisabled(Boolean.TRUE);
    }

    @Override
    public void mapearDatosPacienteNumeroCama() {

        super.mapearDatosPacienteNumeroCama();
        try {
            capturarTococirugiaServices
                    .validarCandidatoTococirugia(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
            requeridos[2] = Boolean.TRUE;
            requeridos[3] = Boolean.TRUE;
            capturarTococirugiaServices.validarFechaIngreso(
                    getDatosHospitalizacion().getDatosPaciente().getFechaIngreso(),
                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion());
            //setTieneFoco(getSelectAtencion().getClientId());
            seleccionarMetodoAnticonceptivoIQX();
            setTieneFoco(getTextGesta().getClientId());
            
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            resetearCamposPacienteTococirugia();
            requeridos[2] = Boolean.FALSE;
            requeridos[3] = Boolean.FALSE;
            getDatosHospitalizacion().getDatosTococirugia().setFecAtencion(TococirugiaConstants.CADENA_VACIA);
            setTieneFoco(getMaskFecha().getClientId());
        }
    }

    public boolean validarTallaVisible(CellEditEvent event) {

        boolean visible = true;
        int index = event.getRowIndex();
        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().get(index).getPeso() == 2) {
            visible = false;
        }
        return visible;
    }

    public void validarTotalNacidos() {

        try {
            capturarTococirugiaServices
                    .validarTotalNacidos(getDatosHospitalizacion().getDatosTococirugia().getTotalNacidos());
            getDatosHospitalizacion().getDatosTococirugia()
                    .setListDatosRecienNacido(capturarTococirugiaServices.inicializarRecienNacidos());
            setContadormodal(0);
            reiniciarModalRecienNacidos();
            capturarTococirugiaServices.inicializarDatosRecienNacido(getDatosHospitalizacion(),
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal());
            setModalConfirm(TococirugiaConstants.MODAL_CONFIRM);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_RECIENNACIDOS);
            setContadormodal(getContadormodal() + 1);
            requeridos[7] = Boolean.TRUE;
            setTieneFoco(getSelectTablaNacido().getClientId());
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().setTotalNacidos(null);
            requeridos[7] = Boolean.FALSE;
            //habilitarGuardar();
            setTieneFoco(getTotalNacidos().getClientId());
        }
    }

    public void limpiaCampos() {

        getMaskTablaHoraDefuncion().setDisabled(Boolean.FALSE);
    }

    private void limpiaCamposRequeridos() {

        requeridos[0] = Boolean.FALSE;
        requeridos[1] = Boolean.FALSE;
        requeridos[2] = Boolean.FALSE;
        requeridos[3] = Boolean.FALSE;
        requeridos[4] = Boolean.FALSE;
        requeridos[5] = Boolean.FALSE;
        requeridos[6] = Boolean.FALSE;
        requeridos[7] = Boolean.FALSE;
        requeridos[8] = Boolean.FALSE;
        requeridos[9] = Boolean.FALSE;
        requeridos[10] = Boolean.FALSE;
    }

    public void validarNacidoModal() {

        try {
            capturarTococirugiaServices.validarNacidoModal(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getNacido());
            capturarTococirugiaServices.inicializarDatosRecienNacido(getDatosHospitalizacion(),
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setSexo(null);
            if (TipoNacidoEnum.VIVO.getClave()
                    .equals(getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getNacido())) {
                getMaskTablaHoraDefuncion().setDisabled(Boolean.TRUE);
                getTextTablaApgar1().setDisabled(Boolean.FALSE);
                getTextTablaApgar5().setDisabled(Boolean.FALSE);
            } else if (TipoNacidoEnum.DEFUNCION.getClave()
                    .equals(getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getNacido())) {
                getMaskTablaHoraDefuncion().setDisabled(Boolean.FALSE);
                getTextTablaApgar1().setDisabled(Boolean.FALSE);
                getTextTablaApgar5().setDisabled(Boolean.FALSE);
            } else if (TipoNacidoEnum.MORTINATO.getClave()
                    .equals(getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getNacido())) {
                getMaskTablaHoraDefuncion().setDisabled(Boolean.TRUE);
                getTextTablaApgar1().setDisabled(Boolean.TRUE);
                getTextTablaApgar5().setDisabled(Boolean.TRUE);
            }
            setTieneFoco(getSelectTablaSexo().getClientId());

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal()
                    .setNacido(TococirugiaConstants.CADENA_VACIA);
            setTieneFoco(getSelectTablaNacido().getClientId());
        }

    }

    public void validarTablaSexoModal() {
    	isValidarTablaSexoModal();
    }

    public void validarTablaPesoModal() {
    	isValidarTablaPesoModal();
    }

    public void validarTablaTallaModal() {
    	isValidarTablaTallaModal();
    }

    public void validarTablaPerimetroCefalicoModal() {
    	try {
            capturarTococirugiaServices.validarPerimetroCefalicoRecienNacido(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getPerimetroCefalico());
            if (getContadormodal() >= 2) {
                if (TipoNacidoEnum.MORTINATO.getClave().equals(
                        getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getNacido())) {
                	if(validarModalGeneral()){
	                    capturarTococirugiaServices.agregarRegistroListaRN(getDatosHospitalizacion(),
	                            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal());
	                    if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
	                            .size() < getDatosHospitalizacion().getDatosTococirugia().getTotalNacidos()) {
	                        reiniciarModalRecienNacidos();
	                    } else {
	                        RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_RECIENNACIDOS);
	                        setTieneFoco(getTextMatricula().getClientId());
	                        setContadormodal(0);
	                    }
	                    setContadormodal(getContadormodal() + 1);
                	}
                } else {
                    setTieneFoco(getTextTablaApgar1().getClientId());
                }
            } else {
                setTieneFoco(getTextTablaSemanasGestacion().getClientId());
            }
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setPerimetroCefalico(null);
            setTieneFoco(getTextTablaPerimetroCefalico().getClientId());
        }
    }

    public void validarTablaSemanasGestacionModal() {
    	try {
            capturarTococirugiaServices.validarSemanasGestacionRecienNacido(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getSemanasGestacion());
            setTieneFoco(getTextTablaApgar1().getClientId());
            if (TipoNacidoEnum.MORTINATO.getClave()
                    .equals(getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getNacido())) {
            	if(validarModalBasico() && isValidarTablaPerimetroCefalicoModal()){
	                capturarTococirugiaServices.agregarRegistroListaRN(getDatosHospitalizacion(),
	                        getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal());
	                if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
	                        .size() < getDatosHospitalizacion().getDatosTococirugia().getTotalNacidos()) {
	                    reiniciarModalRecienNacidos();
	                } else {
	                    RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_RECIENNACIDOS);
	                    setTieneFoco(getTextMatricula().getClientId());
	                    setContadormodal(0);
	                }
	                setContadormodal(getContadormodal() + 1);
            	}
            }
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setSemanasGestacion(null);
            setTieneFoco(getTextTablaSemanasGestacion().getClientId());
        }
    }

    public void validarTablaApgar1Modal() {
    	isValidarTablaApgar1Modal();
    }

    public void validarTablaApgar5Modal() {
   	
        try {
            capturarTococirugiaServices.validarApgar5RecienNacido(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getApgar5());
            if (TipoNacidoEnum.VIVO.getClave()
                    .equals(getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getNacido())) {
            	if(validarModalGeneral() && isValidarTablaPerimetroCefalicoModal() && validarModalAPG()){
	                capturarTococirugiaServices.agregarRegistroListaRN(getDatosHospitalizacion(),
	                        getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal());
	                if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
	                        .size() < getDatosHospitalizacion().getDatosTococirugia().getTotalNacidos()) {
	                    reiniciarModalRecienNacidos();
	                } else {
	                    RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_RECIENNACIDOS);
	                    setTieneFoco(getTextMatricula().getClientId());
	                    setContadormodal(0);
	                }
	                setContadormodal(getContadormodal() + 1);
            	}
            } else {
                setTieneFoco(getMaskTablaHoraDefuncion().getClientId());
            }

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setApgar5(null);
            setTieneFoco(getTextTablaApgar5().getClientId());
        }
    }

    public void validarTablaHoraDefuncionModal() {

        try {
            capturarTococirugiaServices.validarHoraDefuncion(
                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion(),
                    getDatosHospitalizacion().getDatosTococirugia().getHoraParto(),
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getHoraDefuncion());
            if (TipoNacidoEnum.DEFUNCION.getClave()
                    .equals(getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getNacido())) {
            	if(validarModalGeneral() && isValidarTablaPerimetroCefalicoModal() && validarModalAPG()){
	                capturarTococirugiaServices.agregarRegistroListaRN(getDatosHospitalizacion(),
	                        getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal());
	                if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
	                        .size() < getDatosHospitalizacion().getDatosTococirugia().getTotalNacidos()) {
	                    reiniciarModalRecienNacidos();
	                } else {
	                    RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_RECIENNACIDOS);
	                    setTieneFoco(getTextMatricula().getClientId());
	                    setContadormodal(0);
	                }
	                setContadormodal(getContadormodal() + 1);
            	}
            }
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setHoraDefuncion(null);
            setTieneFoco(getMaskTablaHoraDefuncion().getClientId());
        }
    }

    public void guardar() {
        getDatosHospitalizacion().getDatosTococirugia().setFecCreacion(new Date());
        if(!getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar().equals(BaseConstants.NUMERO_CERO_STRING)){
        	getDatosHospitalizacion().getDatosTococirugia().setCantidadMetodoAnticonceptivo(getNumeroCantidad());
        }
        if(getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar() == "23"){
        	setNumeroCantidad(1);
        	getDatosHospitalizacion().getDatosTococirugia().setCantidadMetodoAnticonceptivo(1);
        }
        try{
       	 capturarTococirugiaServices.validarFechaAtencion(
                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion(),
                    obtenerDatosUsuario().getCvePresupuestal());
       	 try{
       		 capturarTococirugiaServices.validarSala(obtenerDatosUsuario().getCvePresupuestal(),
                        getDatosHospitalizacion().getDatosTococirugia().getNumeroSala().toUpperCase());
       		 try{
       			 capturarTococirugiaServices.validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
       			 try{
       				 capturarTococirugiaServices.validarAtencion(
       		                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion(), getDatosHospitalizacion());
       				 try{
       					 capturarTococirugiaServices.validarGesta(getDatosHospitalizacion().getDatosTococirugia().getGesta());
       					 try{
       						 capturarTococirugiaServices.validarHoraParto(getDatosHospitalizacion().getDatosPaciente(),
       				                    getDatosHospitalizacion().getDatosTococirugia().getHoraParto(),
       				                    getDatosHospitalizacion().getDatosTococirugia().getFecAtencion());
       						 
       						try{
   					            validarPlanificacionFamiliar(getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar(), 
   					            		hospitalizacionCommonHelper.convertStringToDate(getDatosHospitalizacion().getDatosTococirugia().getFecAtencion()));
	       						 
   					            try{
	       							 if (!isBanderaParto()) {
	       					                capturarTococirugiaServices
	       					                        .validarTipoParto(getDatosHospitalizacion().getDatosTococirugia().getTipoParto());
	       					                capturarTococirugiaServices
	       					                        .validarTotalNacidos(getDatosHospitalizacion().getDatosTococirugia().getTotalNacidos());
	       					                if(getEpisiotomiaVisible()){
	       					                	capturarTococirugiaServices.validarEpisiotomia(getDatosHospitalizacion().getDatosTococirugia().getEpisiotomia());
	       					                }
	       					            }		    
	       					            capturarTococirugiaServices.validarMatricula(getDatosHospitalizacion().getDatosMedico().getMatricula());
	       					            actualizarMetodoAnticonceptivoIntervencionQuirurgica();
	       					            capturarTococirugiaServices.guardar(getDatosHospitalizacion(), obtenerDatosUsuario());
	       					            agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);            
	       					            cancelar();
	       						 }catch(HospitalizacionException e7){
	       							    agregarMensajeError(e7);
	       					            logger.error("CapturarTococirugia- Ocurri� un error: "+e7.toString());
	       					            if(!getEpisiotomiaVisible()){
	       					            	ocultarEpisiotomia();
	       					            }
	       						 }
       						}catch(HospitalizacionException e8){
    							    agregarMensajeError(e8);
    					            logger.error("CapturarTococirugia- Ocurri� un error: "+e8.toString());
    					            setTieneFoco(getAutoCompletePlanificacionFamiliar().getClientId());
    					            if(!getEpisiotomiaVisible()){
    					            	ocultarEpisiotomia();
    					            }
    						    }
       					 }catch(HospitalizacionException e6){
	       						if(!getEpisiotomiaVisible()){
	    			            	ocultarEpisiotomia();
	    	       				}
       						    agregarMensajeError(e6);       						    
       						    getDatosHospitalizacion().getDatosTococirugia().setHoraParto(TococirugiaConstants.CADENA_VACIA);  
       						    requeridos[6] = Boolean.FALSE;       						   
       						    setTieneFoco(getMaskHoraParto().getClientId());
       						           				            
       					 }
       				 }catch(HospitalizacionException e5){
       					if(!getEpisiotomiaVisible()){
			            	ocultarEpisiotomia();
	       				}
       					agregarMensajeError(e5);   			    		  		
   			    		getDatosHospitalizacion().getDatosTococirugia().setGesta(null);
   			    		requeridos[9] = Boolean.FALSE;   			    	
   			    		setTieneFoco(getTextGesta().getClientId());
       					
       					    
       				 }
       			 }catch(HospitalizacionException e4){
	       				if(!getEpisiotomiaVisible()){
			            	ocultarEpisiotomia();
	       				}
	       				agregarMensajeError(e4);	   		            
	   		            getDatosHospitalizacion().getDatosTococirugia().setAtencion(null);
	   		            requeridos[4] = Boolean.FALSE;	   		          
	   		            setTieneFoco(getSelectAtencion().getClientId());
	   		            
       			 }
       		 }catch(HospitalizacionException e3){
       			  	if(!getEpisiotomiaVisible()){
			            	ocultarEpisiotomia();
			            }
       			    setBanderaNss(Boolean.FALSE);       	            
       	            agregarMensajeError(e3);
       	            getDatosHospitalizacion().getDatosPaciente().setNss(TococirugiaConstants.CADENA_VACIA);
       	            setTieneFoco(getTextNss().getClientId());
       	            
       		 }
       	 }catch(HospitalizacionException e2){       		
	       		 if(!getEpisiotomiaVisible()){
			            	ocultarEpisiotomia();
			     }
	       		 agregarMensajeError(e2);	       		 
	       		 getDatosHospitalizacion().getDatosTococirugia().setNumeroSala(TococirugiaConstants.CADENA_VACIA);
	       		 requeridos[1] = Boolean.FALSE;	       		
	       		 setTieneFoco(getTextSala().getClientId());
       	 }
       }catch(HospitalizacionException e){
    		if(!getEpisiotomiaVisible()){
	           	ocultarEpisiotomia();
	       	}
    		agregarMensajeError(e);	       	
	       	getDatosHospitalizacion().getDatosTococirugia().setFecAtencion(TococirugiaConstants.CADENA_VACIA);       
	       	requeridos[0] = Boolean.FALSE;	       
	        setTieneFoco(getMaskFecha().getClientId());    
	       	   
       } 

   }

    

    public void cancelar() {

    	//Se limpia el modal de recien nacidos
    	getTextTablaSemanasGestacion().setDisabled(Boolean.FALSE);
    	setContadormodal(0);
    	getDatosHospitalizacion().getDatosTococirugia().setTotalNacidos(null);
    	getDatosHospitalizacion().getDatosTococirugia().setDatosRecienNacidoModal( new DatosRecienNacido());
        reiniciarModalRecienNacidos();
        
    	
        resetCampos();
        getTextCama().setDisabled(Boolean.TRUE);
        getTextNss().setDisabled(Boolean.FALSE);
        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);
        getSelectTipoParto().setDisabled(Boolean.FALSE);
        getMaskHoraParto().setDisabled(Boolean.FALSE);
        getTotalNacidos().setDisabled(Boolean.FALSE);
        
        iniciar();
    }

    private void resetCampos() {

        getMaskFecha().resetValue();
        getTextNss().resetValue();
        getTextAgregadoMedico().resetValue();
        getTextNombre().resetValue();
        getTextApellidoPaterno().resetValue();
        getTextApellidoMaterno().resetValue();
        getTextSala().resetValue();
        getTextCama().resetValue();
        setCatalogoAtencion(new ArrayList<DatosCatalogo>());
        setCatalogoTipoParto(new ArrayList<DatosCatalogo>());
        getMaskHoraParto().resetValue();
        getAutoCompletePlanificacionFamiliar().resetValue();
        getTotalNacidos().resetValue();
        getTextGesta().resetValue();

    }

    private void iniciarComponentes() {

        setTextAgregadoMedico(new InputText());
        setTextSala(new InputText());
        setTextCama(new InputText());
        setSelectAtencion(new SelectOneMenu());
        setSelectTipoParto(new SelectOneMenu());
        setAutoCompletePlanificacionFamiliar(new AutoComplete());

        listaAtencion = new ArrayList<>();
        setListaPlanificacionFamiliar(new ArrayList<String>());

        setTotalNacidos(new InputText());
        setTextGesta(new InputText());
        setSelectTablaNacido(new SelectOneMenu());
        setSelectTablaSexo(new SelectOneMenu());
        setTextTablaPeso(new InputText());
        setTextTablaTalla(new InputText());
        setTextTablaPerimetroCefalico(new InputText());
        setTextTablaSemanasGestacion(new InputText());
        setTextTablaApgar1(new InputText());
        setTextTablaApgar5(new InputText());
        setMaskTablaHoraDefuncion(new InputMask());

        dataTableRecienNacidos = new DataTable();

        setButtonGuardar(new CommandButton());
        setButtonCancelar(new CommandButton());
        setButtonSalir(new CommandButton());
        setContadormodal(0);
        setConfirmSi(new CommandButton());
        setConfirmNo(new CommandButton());
    }

    private void deshabilitarComponentes() {

        getTextCama().setDisabled(Boolean.TRUE);
        getTextAgregadoMedico().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getTextMedico().setDisabled(Boolean.TRUE);
        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);
    }

    private void reiniciarModalRecienNacidos() {

        reiniciarCamposModalRecienNacido();
        reiniciarDatosModalRecienNacido();
    }

    private void reiniciarDatosModalRecienNacido() {
        getDatosHospitalizacion().getDatosTococirugia().setDatosRecienNacidoModal(new DatosRecienNacido());
        if (getContadormodal() >= 1) {
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal()
                    .setSemanasGestacion(getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                            .get(0).getSemanasGestacion());
        }
    }

    private void reiniciarCamposModalRecienNacido() {

        getTextTablaApgar1().setDisabled(Boolean.FALSE);
        getTextTablaApgar5().setDisabled(Boolean.FALSE);
        getMaskTablaHoraDefuncion().setDisabled(Boolean.FALSE);
        if (getContadormodal() >= 1) {
            getTextTablaSemanasGestacion().setDisabled(Boolean.TRUE);
        } else {
            getTextTablaSemanasGestacion().setDisabled(Boolean.FALSE);
        }
        setTieneFoco(getSelectTablaNacido().getClientId());
    }

    public void confirmAcepto() {

        setModalConfirm(TococirugiaConstants.CADENA_VACIA);
        RequestContext.getCurrentInstance().execute(TococirugiaConstants.CERRAR_CONFIRM_DIAGNOSTICO);
        RequestContext.getCurrentInstance().execute(TococirugiaConstants.CERRAR_MODAL);
        setTieneFoco(getTotalNacidos().getClientId());
        getDatosHospitalizacion().getDatosTococirugia().setListDatosRecienNacido(new ArrayList<DatosRecienNacido>());
        getDatosHospitalizacion().getDatosTococirugia().setTotalNacidos(null);
    }

    public void confirmRechazo() {

        RequestContext.getCurrentInstance().execute(TococirugiaConstants.CERRAR_CONFIRM_DIAGNOSTICO);
        setTieneFoco(getSelectTablaNacido().getClientId());
        getDatosHospitalizacion().getDatosTococirugia().setDatosRecienNacidoModal(new DatosRecienNacido());
    }

    public void habilitarGuardar() {

        if (capturarTococirugiaServices.sePuedeActivarGuardar(getRequeridos())) {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            setTieneFoco(TococirugiaConstants.FOCO_GUARDAR);

        } else {
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }

    }

    public void salir() throws IOException {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }
    
	public boolean validarModalGeneral() {
		return validarModalBasico() && isValidarSemanasGestacion();
	}
	
	
	public boolean validarModalBasico() {
		if (!isValidarTablaSexoModal()) {
			return false;
		}
		if (!isValidarTablaPesoModal()) {
			return false;
		}
		if (!isValidarTablaTallaModal()) {
			return false;
		}
		return true;
	}
	
	public boolean isValidarSemanasGestacion() {
		try {
			capturarTococirugiaServices.validarSemanasGestacionRecienNacido(
					getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getSemanasGestacion());
			return true;
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
			getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setSemanasGestacion(null);
			setTieneFoco(getTextTablaSemanasGestacion().getClientId());
			return false;
		}
	}
    
	public boolean validarModalAPG() {
		if (!isValidarTablaApgar1Modal()) {
			return false;
		}
		try {
			capturarTococirugiaServices.validarApgar5RecienNacido(
					getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getApgar5());
			return true;
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
			getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setApgar5(null);
			setTieneFoco(getTextTablaApgar5().getClientId());
			return false;
		}
	}

    public boolean isValidarTablaSexoModal() {
        try {
            capturarTococirugiaServices.validarSexoModal(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getSexo());
            setTieneFoco(getTextTablaPeso().getClientId());
            return true;
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal()
                    .setSexo(TococirugiaConstants.INDEX_CERO);
            setTieneFoco(getSelectTablaSexo().getClientId());
            return false;
        }

    }

    public boolean isValidarTablaPesoModal() {
        try {
            capturarTococirugiaServices.validarPesoRecienNacido(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getPeso());
            setTieneFoco(getTextTablaTalla().getClientId());
            return true;
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setPeso(null);
            setTieneFoco(getTextTablaPeso().getClientId());
            return false;
        }
    }

    public boolean isValidarTablaTallaModal() {

        try {
            capturarTococirugiaServices.validarTallaRecienNacido(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getTalla());
            setTieneFoco(getTextTablaPerimetroCefalico().getClientId());
            return true;
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setTalla(null);
            setTieneFoco(getTextTablaTalla().getClientId());
            return false;
        }
    }

    public boolean isValidarTablaPerimetroCefalicoModal() {

        try {
            capturarTococirugiaServices.validarPerimetroCefalicoRecienNacido(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getPerimetroCefalico());
            return true;
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setPerimetroCefalico(null);
            setTieneFoco(getTextTablaPerimetroCefalico().getClientId());
            return false;
        }
    }

    public boolean isValidarTablaApgar1Modal() {
        try {
            capturarTococirugiaServices.validarApgar1RecienNacido(
                    getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().getApgar1());
            setTieneFoco(getTextTablaApgar5().getClientId());
            return true;
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("CapturarTococirugia- Ocurri� un error: "+e.toString());
            getDatosHospitalizacion().getDatosTococirugia().getDatosRecienNacidoModal().setApgar1(null);
            setTieneFoco(getTextTablaApgar1().getClientId());
            return false;
        }
    }
    
    public void closeRecienNacidos(CloseEvent event) {
    	if(getDatosHospitalizacion().getDatosTococirugia().getTotalNacidos()!=getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size()){
    		getDatosHospitalizacion().getDatosTococirugia().setTotalNacidos(null);
    		setTotalNacidos(new InputText());
    		getTotalNacidos().resetValue();
    		getDatosHospitalizacion().getDatosTococirugia().setListDatosRecienNacido(new ArrayList<>());
    	}
    }
    
        
   
    
    private void actualizarMetodoAnticonceptivoIntervencionQuirurgica(){
    	IntervencionesQuirurgicas iqx = capturarIntervencionesQuirurgicasService
    			.buscarIntervencionQXPorCveIngreso(getDatosHospitalizacion().getDatosPaciente().getClaveIngreso());    	
    	if(iqx != null && iqx.getClaveMetodoAnticonceptivo() != 23 ){ // descartando OTB
    		if(iqx.getClaveMetodoAnticonceptivo() != Integer.parseInt(getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar())){
    			capturarIntervencionesQuirurgicasService
        		.actualizarMetodoAnticonceptivoIQX(Integer.parseInt(getDatosHospitalizacion().getDatosTococirugia().getPlanificacionFamiliar()),
        				getDatosHospitalizacion().getDatosTococirugia().getCantidadMetodoAnticonceptivo(),
        				iqx.getClaveIngreso()); 
    		}
    			
    	}
    }
    
    private void mostrarMetodoAnticonceptivoIQX(int cveMetodoAnticonceptivo){
    	String descMetodo = catalogosHospitalizacionServices.obtenerDescMetodoPlanificacion(cveMetodoAnticonceptivo);
		getAutoCompletePlanificacionFamiliar().setValue(descMetodo);
    }
    
    private void seleccionarMetodoAnticonceptivoIQX(){
    	IntervencionesQuirurgicas iqx = capturarIntervencionesQuirurgicasService
    			.buscarIntervencionQXPorCveIngreso(getDatosHospitalizacion().getDatosPaciente().getClaveIngreso());
    	if(iqx != null){
    		logger.info("tiene una cirugia y mpf = "+ iqx.getClaveMetodoAnticonceptivo());
    		if(iqx.getClaveMetodoAnticonceptivo() == 23){ //descartando que sea OTB    			
    			getDatosHospitalizacion().getDatosTococirugia().setPlanificacionFamiliar("23");
    			getDatosHospitalizacion().getDatosTococirugia().setCantidadMetodoAnticonceptivo(1);
    			setNumeroCantidad(1);    	
    			mostrarMetodoAnticonceptivoIQX(iqx.getClaveMetodoAnticonceptivo());
    			getAutoCompletePlanificacionFamiliar().setDisabled(Boolean.TRUE);
    			getDatosHospitalizacion().getDatosTococirugia().setTienePlanificacionFamiliar(Boolean.TRUE);
    			
    		}else{
    			if(iqx.getClaveMetodoAnticonceptivo() == 0){
    				getDatosHospitalizacion().getDatosTococirugia().setPlanificacionFamiliar(TococirugiaConstants.CADENA_VACIA);
        			setNumeroCantidad(null);
        			getDatosHospitalizacion().getDatosTococirugia().setTienePlanificacionFamiliar(Boolean.FALSE);
    			}
    			else{	
    				mostrarMetodoAnticonceptivoIQX(iqx.getClaveMetodoAnticonceptivo());
        			getDatosHospitalizacion().getDatosTococirugia().setPlanificacionFamiliar(
    						Integer.toString(iqx.getClaveMetodoAnticonceptivo()));
    				getDatosHospitalizacion().getDatosTococirugia().setCantidadMetodoAnticonceptivo(
    						iqx.getCantidadMetodoAnticonceptivo());
    				setNumeroCantidad(iqx.getCantidadMetodoAnticonceptivo());
    				getDatosHospitalizacion().getDatosTococirugia().setTienePlanificacionFamiliar(Boolean.TRUE);
    			}
    			getAutoCompletePlanificacionFamiliar().setDisabled(Boolean.FALSE);  			
    			setCatalogoPlanificacionFamiliar(new ArrayList<DatosCatalogo>());
    		    cargarCatalogoPlanificacion();
    			
    		} 
    	}else{
    		getAutoCompletePlanificacionFamiliar().setDisabled(Boolean.FALSE); 
    		getDatosHospitalizacion().getDatosTococirugia().setPlanificacionFamiliar(TococirugiaConstants.CADENA_VACIA);
    		setNumeroCantidad(null);    		
    		setCatalogoPlanificacionFamiliar(new ArrayList<DatosCatalogo>());
    	    cargarCatalogoPlanificacion();
    	    getDatosHospitalizacion().getDatosTococirugia().setTienePlanificacionFamiliar(Boolean.FALSE);
    		
    	}
    }
}
