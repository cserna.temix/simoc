package mx.gob.imss.simo.hospitalizacion.interconsultas.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.AtencionInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.EncabezadoInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;

public interface CapturaInterconsultaRepository  extends HospitalizacionCommonRepository{
	
	public List<Especialidad> obtenerEspecialidadesPorCvePres( String clavePresupuestal ) throws HospitalizacionException;
		
	public void guardarEncabezado(EncabezadoInterconsulta encabezado) throws HospitalizacionException;
	
	public EncabezadoInterconsulta buscarEncabezadoInterconsulta(String fechaInterconsulta, String claveEspecialidad, String claveMedico, Integer claveTurno) throws HospitalizacionException;
	
	public Long obtenerConsecutivoInterconsultas(Integer cve_interconsulta) throws HospitalizacionException;
	
	public void insertarDetalleInterconsulta(EncabezadoInterconsulta encabezado, AtencionInterconsulta atencionInteronsulta) throws HospitalizacionException;

	List<Especialidad> validarEspecialidadInterconsulta(String cveEspecialidad) throws HospitalizacionException;
	
}
