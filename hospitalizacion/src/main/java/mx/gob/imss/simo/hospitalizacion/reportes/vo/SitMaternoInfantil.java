/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.vo;

/**
 * @author francisco.rodriguez
 *
 */
public class SitMaternoInfantil {

	public String cvePresupuestal;
	public String cvePeriodoImss;
	public Integer numPartoNormal;
	public Integer numPartoVaginal;
	public Integer numPartoAbdominal;
	public Integer numTotalParto;
	public Integer numPretBajo;
	public Integer numPretNormal;
	public Integer numPretAlto;
	public Integer numTerminoBajo;
	public Integer numTerminoNormal;
	public Integer numTerminoAlto;
	public Integer numPostBajo;
	public Integer numPostNormal;
	public Integer numPostAlto;
	public Integer numDefuncion;
	public Integer numMortinato;
	public Integer numAborto;
	public Integer numNacidoVivo;
	public Integer numEjecucion;

	/**
	 * @return the numEjecucion
	 */
	public Integer getNumEjecucion() {
		return numEjecucion;
	}

	/**
	 * @param numEjecucion the numEjecucion to set
	 */
	public void setNumEjecucion(Integer numEjecucion) {
		this.numEjecucion = numEjecucion;
	}

	/**
	 * @return the cvePresupuestal
	 */
	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	/**
	 * @param cvePresupuestal
	 *            the cvePresupuestal to set
	 */
	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	/**
	 * @return the cvePeriodoImss
	 */
	public String getCvePeriodoImss() {
		return cvePeriodoImss;
	}

	/**
	 * @param cvePeriodoImss
	 *            the cvePeriodoImss to set
	 */
	public void setCvePeriodoImss(String cvePeriodoImss) {
		this.cvePeriodoImss = cvePeriodoImss;
	}

	/**
	 * @return the numPartoNormal
	 */
	public Integer getNumPartoNormal() {
		return numPartoNormal;
	}

	/**
	 * @param numPartoNormal
	 *            the numPartoNormal to set
	 */
	public void setNumPartoNormal(Integer numPartoNormal) {
		this.numPartoNormal = numPartoNormal;
	}

	/**
	 * @return the numPartoVaginal
	 */
	public Integer getNumPartoVaginal() {
		return numPartoVaginal;
	}

	/**
	 * @param numPartoVaginal
	 *            the numPartoVaginal to set
	 */
	public void setNumPartoVaginal(Integer numPartoVaginal) {
		this.numPartoVaginal = numPartoVaginal;
	}

	/**
	 * @return the numPartoAbdominal
	 */
	public Integer getNumPartoAbdominal() {
		return numPartoAbdominal;
	}

	/**
	 * @param numPartoAbdominal
	 *            the numPartoAbdominal to set
	 */
	public void setNumPartoAbdominal(Integer numPartoAbdominal) {
		this.numPartoAbdominal = numPartoAbdominal;
	}

	/**
	 * @return the numTotalParto
	 */
	public Integer getNumTotalParto() {
		return numTotalParto;
	}

	/**
	 * @param numTotalParto
	 *            the numTotalParto to set
	 */
	public void setNumTotalParto(Integer numTotalParto) {
		this.numTotalParto = numTotalParto;
	}

	/**
	 * @return the numPretBajo
	 */
	public Integer getNumPretBajo() {
		return numPretBajo;
	}

	/**
	 * @param numPretBajo
	 *            the numPretBajo to set
	 */
	public void setNumPretBajo(Integer numPretBajo) {
		this.numPretBajo = numPretBajo;
	}

	/**
	 * @return the numPretNormal
	 */
	public Integer getNumPretNormal() {
		return numPretNormal;
	}

	/**
	 * @param numPretNormal
	 *            the numPretNormal to set
	 */
	public void setNumPretNormal(Integer numPretNormal) {
		this.numPretNormal = numPretNormal;
	}

	/**
	 * @return the numPretAlto
	 */
	public Integer getNumPretAlto() {
		return numPretAlto;
	}

	/**
	 * @param numPretAlto
	 *            the numPretAlto to set
	 */
	public void setNumPretAlto(Integer numPretAlto) {
		this.numPretAlto = numPretAlto;
	}

	/**
	 * @return the numTerminoBajo
	 */
	public Integer getNumTerminoBajo() {
		return numTerminoBajo;
	}

	/**
	 * @param numTerminoBajo
	 *            the numTerminoBajo to set
	 */
	public void setNumTerminoBajo(Integer numTerminoBajo) {
		this.numTerminoBajo = numTerminoBajo;
	}

	/**
	 * @return the numTerminoNormal
	 */
	public Integer getNumTerminoNormal() {
		return numTerminoNormal;
	}

	/**
	 * @param numTerminoNormal
	 *            the numTerminoNormal to set
	 */
	public void setNumTerminoNormal(Integer numTerminoNormal) {
		this.numTerminoNormal = numTerminoNormal;
	}

	/**
	 * @return the numTerminoAlto
	 */
	public Integer getNumTerminoAlto() {
		return numTerminoAlto;
	}

	/**
	 * @param numTerminoAlto
	 *            the numTerminoAlto to set
	 */
	public void setNumTerminoAlto(Integer numTerminoAlto) {
		this.numTerminoAlto = numTerminoAlto;
	}

	/**
	 * @return the numPostBajo
	 */
	public Integer getNumPostBajo() {
		return numPostBajo;
	}

	/**
	 * @param numPostBajo
	 *            the numPostBajo to set
	 */
	public void setNumPostBajo(Integer numPostBajo) {
		this.numPostBajo = numPostBajo;
	}

	/**
	 * @return the numPostNormal
	 */
	public Integer getNumPostNormal() {
		return numPostNormal;
	}

	/**
	 * @param numPostNormal
	 *            the numPostNormal to set
	 */
	public void setNumPostNormal(Integer numPostNormal) {
		this.numPostNormal = numPostNormal;
	}

	/**
	 * @return the numPostAlto
	 */
	public Integer getNumPostAlto() {
		return numPostAlto;
	}

	/**
	 * @param numPostAlto
	 *            the numPostAlto to set
	 */
	public void setNumPostAlto(Integer numPostAlto) {
		this.numPostAlto = numPostAlto;
	}

	/**
	 * @return the numDefuncion
	 */
	public Integer getNumDefuncion() {
		return numDefuncion;
	}

	/**
	 * @param numDefuncion
	 *            the numDefuncion to set
	 */
	public void setNumDefuncion(Integer numDefuncion) {
		this.numDefuncion = numDefuncion;
	}

	/**
	 * @return the numMortinato
	 */
	public Integer getNumMortinato() {
		return numMortinato;
	}

	/**
	 * @param numMortinato
	 *            the numMortinato to set
	 */
	public void setNumMortinato(Integer numMortinato) {
		this.numMortinato = numMortinato;
	}

	/**
	 * @return the numAborto
	 */
	public Integer getNumAborto() {
		return numAborto;
	}

	/**
	 * @param numAborto
	 *            the numAborto to set
	 */
	public void setNumAborto(Integer numAborto) {
		this.numAborto = numAborto;
	}

	/**
	 * @return the numNacidoVivo
	 */
	public Integer getNumNacidoVivo() {
		return numNacidoVivo;
	}

	/**
	 * @param numNacidoVivo
	 *            the numNacidoVivo to set
	 */
	public void setNumNacidoVivo(Integer numNacidoVivo) {
		this.numNacidoVivo = numNacidoVivo;
	}

}
