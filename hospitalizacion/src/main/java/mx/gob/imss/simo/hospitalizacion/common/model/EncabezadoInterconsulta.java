/**
 * EncabezadoInterconuslta.java
 * Creado el 05 de Mayo de 2017
 */
package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

/**
 * Clase que representa el modelo de datos para el encabezado de la interconsulta
 * @author luis.rodriguez
 *
 */
@Data
public class EncabezadoInterconsulta {
	
	private String claveEspecialidad;
	private String claveMedico;
	private Integer claveTurno;
	private Date fechaInterconsulta;
	private Date fechaCreacion;
	private String clavePresupuestal;
	private Integer claveTipoFormato;
	private String claveCapturista;
	private Integer claveInterconsulta;

}
