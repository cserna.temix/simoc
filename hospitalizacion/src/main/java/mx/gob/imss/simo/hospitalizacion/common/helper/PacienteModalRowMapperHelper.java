package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.PacienteModal;

public class PacienteModalRowMapperHelper extends BaseHelper implements RowMapper<PacienteModal> {

    @Override
    public PacienteModal mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        PacienteModal paciente = new PacienteModal();

        paciente.setCveIngreso(resultSet.getLong(SQLColumnasConstants.CVE_INGRESO));
        paciente.setCveTipoFormato(resultSet.getLong(SQLColumnasConstants.CVE_TIPO_FORMATO));
        paciente.setCama(resultSet.getString(SQLColumnasConstants.CVE_CAMA));
        paciente.setRefNss(resultSet.getString(SQLColumnasConstants.REF_NSS));
        paciente.setRefAgregadoMedico(resultSet.getString(SQLColumnasConstants.REF_AGREGADO_MEDICO));
        paciente.setRefNombre(resultSet.getString(SQLColumnasConstants.REF_NOMBRE));
        paciente.setRefApellidoPaterno(resultSet.getString(SQLColumnasConstants.REF_APELLIDO_PATERNO));
        paciente.setRefApellidoMaterno(resultSet.getString(SQLColumnasConstants.REF_APELLIDO_MATERNO));
        paciente.setUnidadAdscripcion(resultSet.getString(SQLColumnasConstants.CVE_UNIDAD_ADSCRIPCION));
        paciente.setCvePaciente(resultSet.getLong(SQLColumnasConstants.CVE_PACIENTE));
        paciente.setFechaIngreso(resultSet.getDate(SQLColumnasConstants.FEC_INGRESO));
        paciente.setClaveSexo(resultSet.getString(SQLColumnasConstants.CVE_SEXO));
        paciente.setFechaNacimiento(resultSet.getDate(SQLColumnasConstants.FEC_NACIMIENTO));
        paciente.setIndRecienNacido(resultSet.getBoolean(SQLColumnasConstants.IND_RECIEN_NACIDO));
        paciente.setIndUltimoRegistro(resultSet.getBoolean(SQLColumnasConstants.IND_ULTIMO_REGISTRO));
        if (!paciente.isIndRecienNacido()) {
            paciente.setNumEdadAnios(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_ANIOS));
            paciente.setNumEdadSemanas(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_SEMANAS));
        }
        return paciente;
    }

}
