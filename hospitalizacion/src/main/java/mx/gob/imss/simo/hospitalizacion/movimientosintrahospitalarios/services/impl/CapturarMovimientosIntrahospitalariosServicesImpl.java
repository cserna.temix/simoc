package mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MovimientosIntrahospitalariosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.EspecialidadUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCama;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCamaAlojamiento;
import mx.gob.imss.simo.hospitalizacion.common.model.TotalCamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.helper.CapturarMovimientosIntrahospitalariosHelper;
import mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.repository.CapturarMovimientosIntrahospitalariosRepository;
import mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.rules.CapturarMovimientosIntrahospitalariosRules;
import mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.services.CapturarMovimientosIntrahospitalariosServices;
import mx.gob.imss.simo.model.DatosUsuario;

@Service("capturarMovimientosIntrahospitalariosServices")
public class CapturarMovimientosIntrahospitalariosServicesImpl extends HospitalizacionCommonServicesImpl
        implements CapturarMovimientosIntrahospitalariosServices {

    @Autowired
    private CapturarMovimientosIntrahospitalariosRules capturarMovimientosIntrahospitalariosRules;

    @Autowired
    private CapturarMovimientosIntrahospitalariosHelper capturarMovimientosIntrahospitalariosHelper;

    @Autowired
    private CapturarMovimientosIntrahospitalariosRepository capturarMovimientosIntrahospitalariosRepository;

    public void validarFechaAtencion(String fechaAtencion, String clavePresupuestal) throws HospitalizacionException {

        capturarMovimientosIntrahospitalariosRules.validarFormatoFecha(fechaAtencion);
        super.validarReporteRelacionPacientesGenerado(clavePresupuestal, fechaAtencion);
        capturarMovimientosIntrahospitalariosRules.validarFechaActual(fechaAtencion);
        super.validarPeriodoAnteriorCerrado(clavePresupuestal, fechaAtencion,
                TipoCapturaEnum.HOSPITALIZACION.getClave());
        validarFechaEnPeriodoActualActivo(fechaAtencion,
                capturarMovimientosIntrahospitalariosHelper.convertStringToDate(fechaAtencion), clavePresupuestal,
                TipoCapturaEnum.HOSPITALIZACION.getClave());
    }

    public void validarNss(String nss) throws HospitalizacionException {

        capturarMovimientosIntrahospitalariosRules.validarNss(nss);
    }

    public MovimientoIntraHospitalario obtenerDatosAnteriores(DatosPaciente datosPaciente) {

        Ingreso ingreso = capturarMovimientosIntrahospitalariosRepository.buscarIngresoPorDatosPaciente(datosPaciente);
        Especialidad especialidad = capturarMovimientosIntrahospitalariosRepository
                .buscarEspecialidad(ingreso.getCveEspecialidadIngreso());
        DatosCatalogo division = capturarMovimientosIntrahospitalariosRepository
                .buscarDivisionPorCveDivision(especialidad.getCveDivision());
        return capturarMovimientosIntrahospitalariosHelper.prepararDatosMovimientros(ingreso, especialidad, division);
    }
    
    public List<DatosCatalogo> actualizarCatEspecialidadActualRN(List<DatosCatalogo> catalogoEspecialidades, Boolean esRecienNacidoExterno){
    	
    	return capturarMovimientosIntrahospitalariosRules
    			.filtrarCatEspecialidaActualRN(catalogoEspecialidades, esRecienNacidoExterno);
    }
    
    public Boolean validarRecienNacidoExterno(long clavePaciente,String cvePresupuestal){
    	
    	return capturarMovimientosIntrahospitalariosRepository
    			.esRecienNacidoExterno(clavePaciente,cvePresupuestal);
    	
    }

    public void validarEspecialidad(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException {

        if (datosHospitalizacion.getDatosMovimientos().getEspecialidadDestino() == null
                || datosHospitalizacion.getDatosMovimientos().getEspecialidadDestino().isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_ESPECIALIDAD));
        }
        EspecialidadUnidad especialidadUnidad = capturarMovimientosIntrahospitalariosRepository
                .buscarEspecialidadUnidad(datosHospitalizacion.getDatosMovimientos().getEspecialidadDestino(),
                        datosUsuario.getCvePresupuestal());
        Especialidad especialidad = capturarMovimientosIntrahospitalariosRepository
                .buscarEspecialidad(datosHospitalizacion.getDatosMovimientos().getEspecialidadDestino());
        capturarMovimientosIntrahospitalariosRules.validarEspecialidadAutorizada(especialidadUnidad);
        capturarMovimientosIntrahospitalariosRules
                .validarEspecialidadParaMovimientoIntra(especialidad.isIndMovIntrahospitalario());
        capturarMovimientosIntrahospitalariosRules.validarSexoEspecialidad(especialidad.getCveSexo(),
                capturarMovimientosIntrahospitalariosHelper
                        .sexoPorAgregadoMedico(datosHospitalizacion.getDatosPaciente().getAgregadoMedico()));
        if(!datosHospitalizacion.getDatosPaciente().isIndRecienNacido()){
        	Integer edad = capturarMovimientosIntrahospitalariosHelper
                    .edadPorAgregadoMedico(datosHospitalizacion.getDatosPaciente().getAgregadoMedico());
            capturarMovimientosIntrahospitalariosRules.validarEdadPermitida(especialidad, edad, (edad * 52));
            
        }else{
        	capturarMovimientosIntrahospitalariosRules.validarEdadPermitida(especialidad, datosHospitalizacion.getDatosPaciente().getEdadAnios(),
        			(datosHospitalizacion.getDatosPaciente().getEdadAnios()* 52));
        }
        

    }
    
    public void validarCamaAlojamientoConjunto(String nss, Long cvePaciente, String clavePresupuestal) throws HospitalizacionException{
    	
    	if(!capturarMovimientosIntrahospitalariosRepository.existenIngresosRNGemelos(nss,cvePaciente,clavePresupuestal)){
    		
    		List<TotalCamaUnidad> totalCamas = capturarMovimientosIntrahospitalariosRepository.buscarTotalCamaAlojamiento(clavePresupuestal);
    		List<OcupacionCamaAlojamiento> ocupacionAlojamiento = capturarMovimientosIntrahospitalariosRepository.buscarOcupacionAlojamiento(clavePresupuestal);
    		
    		if(totalCamas != null && !totalCamas.isEmpty() 
    				&& ocupacionAlojamiento != null && !ocupacionAlojamiento.isEmpty() ){
    			capturarMovimientosIntrahospitalariosRules.validaCamaAlojamientoConjunto(totalCamas.get(0).getCanCamaCensable(),
    				ocupacionAlojamiento.get(0).getNumOcupacionCama());
    		}
    	}
    }
    
    
    
    public String obtenerCamaAlojamientoConjunto(long clavePaciente,String cvePresupuestal){
    	String camaRN = "";
    	List<Ingreso> ingresoMadre = capturarMovimientosIntrahospitalariosRepository.
    			buscarIngresoMadrePorToco(clavePaciente, cvePresupuestal);
    	if(ingresoMadre != null && !ingresoMadre.isEmpty()){
    		if(ingresoMadre.get(0).getCveCama() == null){
    			List<Integer> tipoFormato = new ArrayList<>();
    	    	tipoFormato.add(TipoFormatoEnum.INGRESO_UCI.getClave());
    	    	tipoFormato.add(TipoFormatoEnum.INGRESO_URGENCIAS.getClave());
    	    	tipoFormato.add(TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave());
    	    	
    	    	List<Ingreso> ingresoLocalizado = capturarMovimientosIntrahospitalariosRepository.
    	    			buscarIngresoMadre(cvePresupuestal, ingresoMadre.get(0).getCvePaciente(),tipoFormato);
    	    	if(ingresoLocalizado != null){
    	    		camaRN = BaseConstants.PREFIJO_CAMA_RN + ingresoLocalizado.get(0).getCveCama();
    	    	}
    		}else{
    			camaRN = BaseConstants.PREFIJO_CAMA_RN + ingresoMadre.get(0).getCveCama();
    		}
    	}
    	
    	return camaRN;
    }

    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario capturista, int cveTipoFormato) {

        Ingreso ingreso = capturarMovimientosIntrahospitalariosRepository
                .buscarIngresoPorDatosPaciente(datosHospitalizacion.getDatosPaciente());
        
        CamaUnidad camaUnidad = new CamaUnidad();
        if(datosHospitalizacion.getDatosMovimientos().getEspecialidadDestino().substring(0, 2).equals(CapturarIngresosConstants.CLAVE_CUNERO_FISIOLOGICO)){
        	camaUnidad.setCveCama(datosHospitalizacion.getDatosMovimientos().getCamaDestino());
        	camaUnidad.setCveEspecialidad(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
        }else{
        		camaUnidad = capturarMovimientosIntrahospitalariosRepository.buscarCamaUnidad(
        				capturista.getCvePresupuestal(), datosHospitalizacion.getDatosMovimientos().getCamaDestino(),
        				datosHospitalizacion.getDatosMovimientos().getEspecialidadCamaDestino()).get(0);
        }
        MovimientoIntraHospitalario movimiento = capturarMovimientosIntrahospitalariosHelper.prepararMovimientosAGurdar(
                datosHospitalizacion.getDatosMovimientos(), ingreso, capturista, camaUnidad);

        capturarMovimientosIntrahospitalariosRepository.guardar(movimiento);
        
        if(ingreso.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue() 
        		|| ingreso.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue()){
        	capturarMovimientosIntrahospitalariosRepository.actualizarIngresoPorMovimientoIntraHospitalarioRN(ingreso.getCveIngreso(), 
        			movimiento, capturarMovimientosIntrahospitalariosHelper.obtenerTipoFormatoIngresoRecienNacido(
            				datosHospitalizacion.getDatosMovimientos().getEspecialidadDestino()));
        }else{
        capturarMovimientosIntrahospitalariosRepository
                .actualizarIngresoPorMovimientoIntraHospitalario(ingreso.getCveIngreso(), movimiento);
        actualizarCamaAlojamiento(datosHospitalizacion.getDatosPaciente().getNss(), capturista.getCvePresupuestal(),
        		movimiento.getCamaDestino());
        
        }
        
        if(ingreso.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()){
        	actualizaOcupacionAlojamiento(capturista.getCvePresupuestal(), datosHospitalizacion);
        }else{
	        actualizarOcupacionCama(capturista, datosHospitalizacion, ingreso, cveTipoFormato);
        }
        
        if(datosHospitalizacion.getDatosMovimientos().getEspecialidadDestino().equals(MovimientosIntrahospitalariosConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)){
        	registrarCamaAlojamiento(capturista.getCvePresupuestal(), datosHospitalizacion);
        }else{
        	registrarCamas(camaUnidad.getCveCama(), camaUnidad.getCvePresupuestal(), camaUnidad.getCveEspecialidad(),
	                TipoFormatoEnum.MOVIMIENTO_INTRAHOSPITALARIO, Boolean.TRUE, movimiento.getFechaMovimiento());
        }
        // super.registroDeCamas(movimiento.getCamaOrigen(), movimiento.getClavePresupuestal(),
        // movimiento.getEspecialidadOrigen(), TipoFormatoEnum.MOVIMIENTO_INTRAHOSPITALARIO, Boolean.FALSE,
        // movimiento.getFechaMovimiento());
    }
    
    
    public void actualizarCamaAlojamiento(String nss, String cvePresupuestal, String camaNueva){
    	
    	List<Ingreso> ingresosAlojamiento = capturarMovimientosIntrahospitalariosRepository.buscarIngresoAlojamiento(nss, cvePresupuestal);
    	String camaRN = "";
    	if(ingresosAlojamiento != null && !ingresosAlojamiento.isEmpty()){
    		for(Ingreso ingreso: ingresosAlojamiento){
    			camaRN = BaseConstants.PREFIJO_CAMA_RN + camaNueva;
    			capturarMovimientosIntrahospitalariosRepository.actualizaCamaAlojamiento(camaRN, ingreso);
    		}
    	}
    }
    
    public void actualizaOcupacionAlojamiento(String clavePresupestal, DatosHospitalizacion datosHospitalizacion){
    	
    	List<TotalCamaUnidad> camasAlojamiento = capturarMovimientosIntrahospitalariosRepository.buscarTotalCamaAlojamiento(clavePresupestal);
    	List<OcupacionCamaAlojamiento> ocupacionAlojamiento = capturarMovimientosIntrahospitalariosRepository.buscarOcupacionAlojamiento(clavePresupestal);
    	if(camasAlojamiento != null && !camasAlojamiento.isEmpty() 
				&& ocupacionAlojamiento != null && !ocupacionAlojamiento.isEmpty() && ocupacionAlojamiento.get(0).getNumOcupacionCama()!= 0){
    		TotalCamaUnidad camaAlojamiento = camasAlojamiento.get(0);
    		if(!capturarMovimientosIntrahospitalariosRepository.existenIngresosRNGemelos(datosHospitalizacion.getDatosPaciente().getNss(),
    				datosHospitalizacion.getDatosPaciente().getCvePaciente(),clavePresupestal)){
    			int numPacientes = (ocupacionAlojamiento.get(0).getNumOcupacionCama()) - 1;
    			capturarMovimientosIntrahospitalariosRepository.actualizaOcupacionCamaAlojamiento(numPacientes,
    					capturarMovimientosIntrahospitalariosHelper.convertStringToDate(datosHospitalizacion.getDatosMovimientos().getFechaAtencion()),
    					camaAlojamiento);
    			
				logger.debug("Se desocupo una cama de especialidad: " + camaAlojamiento.getCveEspecialidad() + " - Pacientes: " + numPacientes);
    		}
    	}
    }
    
    public List<CamaUnidad> obtenerCamaUnidad(String clavePresupuestal, String cama, String cveEspecialidad, Boolean esRecienNacido){
    	
    	List<CamaUnidad> camaUnidad = new ArrayList<CamaUnidad>();
    	if(esRecienNacido && cveEspecialidad == null){
        		camaUnidad =  capturarMovimientosIntrahospitalariosRepository.buscarCamaUnidadRN(clavePresupuestal,
        				cama);
        	}else{
        		camaUnidad = capturarMovimientosIntrahospitalariosRepository.buscarCamaUnidad(
        				clavePresupuestal, cama, cveEspecialidad);
        	}
    	
    	return camaUnidad;
    	
    }

    private void registrarCamas(String cveCama, String cvePresupuestal, String cveEspecialidad,
            TipoFormatoEnum tipoFormato, boolean agregarPaciente, Date fechaAtencion) {

        List<CamaUnidad> camas = capturarMovimientosIntrahospitalariosRepository.buscarCamaUnidad(cvePresupuestal,
                cveCama, cveEspecialidad);
        if (null != camas && !camas.isEmpty()) {
            CamaUnidad cama = camas.get(0);
            List<OcupacionCama> ocupacion = capturarMovimientosIntrahospitalariosRepository.buscarOcupacionCama(cveCama,
                    cvePresupuestal, cveEspecialidad);
            if (ocupacion == null || ocupacion.isEmpty()) {
                capturarMovimientosIntrahospitalariosRepository.guardarOcupacionCama(cama);
            } else {
                actualizarOcupacionCama(cama, agregarPaciente);
            }
            capturarMovimientosIntrahospitalariosRepository.registrarEventoBitacoraCama(cama, tipoFormato,
                    fechaAtencion, BaseConstants.CAMA_OCUPADA);
        }
    }
    
    public void registrarCamaAlojamiento(String clavePresupestal, DatosHospitalizacion datosHospitalizacion){
    	
    	List<TotalCamaUnidad> camasAlojamiento = capturarMovimientosIntrahospitalariosRepository.buscarTotalCamaAlojamiento(clavePresupestal);
		
		if(camasAlojamiento != null && !camasAlojamiento.isEmpty()){
			
			TotalCamaUnidad camaAlojamiento = camasAlojamiento.get(0);
			List<OcupacionCamaAlojamiento> ocupacionAlojamiento = capturarMovimientosIntrahospitalariosRepository.buscarOcupacionAlojamiento(clavePresupestal);
				if(ocupacionAlojamiento == null || ocupacionAlojamiento.isEmpty()){
					capturarMovimientosIntrahospitalariosRepository.guardarOcupacionCamaAlojamiento(camaAlojamiento, 
							capturarMovimientosIntrahospitalariosHelper.convertStringToDate(datosHospitalizacion.getDatosMovimientos().getFechaAtencion()));
					logger.debug("Se ocupo una cama de especialidad: " + camaAlojamiento.getCveEspecialidad() + " - Pacientes: " + 1);
				}else{
					if(!capturarMovimientosIntrahospitalariosRepository.existenIngresosRNGemelos(datosHospitalizacion.getDatosPaciente().getNss(),datosHospitalizacion.getDatosPaciente().getCvePaciente(),clavePresupestal)){
						int numPacientes = (ocupacionAlojamiento.get(0).getNumOcupacionCama()) + 1;
						capturarMovimientosIntrahospitalariosRepository.actualizaOcupacionCamaAlojamiento(numPacientes,
								capturarMovimientosIntrahospitalariosHelper.convertStringToDate(datosHospitalizacion.getDatosMovimientos().getFechaAtencion()),camaAlojamiento);
						logger.debug("Se ocupo una cama de especialidad: " + camaAlojamiento.getCveEspecialidad() + " - Pacientes: " + numPacientes);
				}
	    	}
		}
    }

    private void actualizarOcupacionCama(DatosUsuario datosUsuario, DatosHospitalizacion datosHospitalizacion,
            Ingreso ingreso, Integer cveTipoFormato) {
    	
    	String cveCama = "";
    	
    	if(ingreso.getCveCama() != null && !ingreso.getCveCama().isEmpty()){
    		cveCama = ingreso.getCveCama();
    	}else{
    		cveCama = ingreso.getRefCamaRn();
    	}
    	
    	List<CamaUnidad> camasUnidad = obtenerCamaUnidad(datosUsuario.getCvePresupuestal(),cveCama,ingreso.getCveEspecialidadCama(),
    			datosHospitalizacion.getDatosPaciente().isIndRecienNacido());
    	
        if (camasUnidad != null && !camasUnidad.isEmpty()) {
            CamaUnidad cama = camasUnidad.get(0);
            actualizarOcupacionCama(cama, BaseConstants.QUITAR_PACIENTE);
            // capturarMovimientosIntrahospitalariosRepository.registrarEventoBitacoraCama(cama,
            // TipoFormatoEnum.parse(cveTipoFormato), datosHospitalizacion.getDatosEgreso().getFechaEgreso(),
            // BaseConstants.CAMA_DESOCUPADA);
            logger.info("Se resto un paciente a la cama: " + cama);
        }
    }

    public CapturarMovimientosIntrahospitalariosRules getCapturarMovimientosIntrahospitalariosRules() {

        return capturarMovimientosIntrahospitalariosRules;
    }

    public void setCapturarMovimientosIntrahospitalariosRules(
            CapturarMovimientosIntrahospitalariosRules capturarMovimientosIntrahospitalariosRules) {

        this.capturarMovimientosIntrahospitalariosRules = capturarMovimientosIntrahospitalariosRules;
    }

    public CapturarMovimientosIntrahospitalariosHelper getCapturarMovimientosIntrahospitalariosHelper() {

        return capturarMovimientosIntrahospitalariosHelper;
    }

    public void setCapturarMovimientosIntrahospitalariosHelper(
            CapturarMovimientosIntrahospitalariosHelper capturarMovimientosIntrahospitalariosHelper) {

        this.capturarMovimientosIntrahospitalariosHelper = capturarMovimientosIntrahospitalariosHelper;
    }

    public CapturarMovimientosIntrahospitalariosRepository getCapturarMovimientosIntrahospitalariosRepository() {

        return capturarMovimientosIntrahospitalariosRepository;
    }

    public void setCapturarMovimientosIntrahospitalariosRepository(
            CapturarMovimientosIntrahospitalariosRepository capturarMovimientosIntrahospitalariosRepository) {

        this.capturarMovimientosIntrahospitalariosRepository = capturarMovimientosIntrahospitalariosRepository;
    }

    public DatosCatalogo elementoSelecionado(SelectEvent evento, List<String> listaEspecialidad,
            AutoComplete autoCompleteEspecialidad, List<DatosCatalogo> catalogoEspecialidad) {

        return capturarMovimientosIntrahospitalariosHelper.elementoSeleccionado(evento, listaEspecialidad,
                autoCompleteEspecialidad, catalogoEspecialidad);
    }

    public boolean validarActivarGuardar(Boolean[] requeridos) {

        return capturarMovimientosIntrahospitalariosHelper.validarActivarGuardar(requeridos);
    }
 
    public int validarCamaActual(DatosHospitalizacion  datosHospitalizacion, String clavePresupuestal)
            throws HospitalizacionException {

        int ocupacion = 0;
        capturarMovimientosIntrahospitalariosRules.validarCama(datosHospitalizacion.getDatosMovimientos().getCamaDestino());
        List<CamaUnidad> camasUnidad = capturarMovimientosIntrahospitalariosRepository.buscarCamaUnidadMovIntra(clavePresupuestal,
        		datosHospitalizacion.getDatosMovimientos().getCamaDestino());
        capturarMovimientosIntrahospitalariosRules.validarExistenciaCama(camasUnidad);
        List<CamaUnidad> camasAutorizadas = capturarMovimientosIntrahospitalariosRules.validarCamaAutorizada(camasUnidad,
        		datosHospitalizacion.getDatosPaciente().getCveTipoFormato());
    
        for(CamaUnidad cama : camasAutorizadas){
        	validarSemanasMaxPediatria(datosHospitalizacion.getDatosPaciente().getEdadSemanas(),BaseConstants.ESPECIALIDAD_PEDIATRIA,
        			cama);
        	List<OcupacionCama> ocupacionCama = capturarMovimientosIntrahospitalariosRepository
                .buscarOcupacionCama(cama.getCveCama(), cama.getCvePresupuestal(), cama.getCveEspecialidad());
        	ocupacion = capturarMovimientosIntrahospitalariosRules.validarOcupacionCama(ocupacionCama);
        	if(ocupacion < 2){
        		datosHospitalizacion.getDatosMovimientos().setEspecialidadCamaDestino(cama.getCveEspecialidad());
        		break;
        	}
        }
        return ocupacion;
    }
    
      
  public void validarSemanasMaxPediatria(Integer edadSemanas, String claveEspecialidad, CamaUnidad cama) throws HospitalizacionException{
    	if(edadSemanas != null && edadSemanas > capturarMovimientosIntrahospitalariosRepository
				.obtenerNumSemanasMaxPediatria(BaseConstants.ESPECIALIDAD_PEDIATRIA) && cama.isIndCamaPedriatica()){
    		throw new HospitalizacionException(MensajesErrorConstants.ME_803);
    	}
    	
    }
    
    public Boolean validarIngresoRNUcin(DatosHospitalizacion datosHospializacion){
    	
    	Boolean existeIngresoVigente = Boolean.FALSE;
    	List<Ingreso> ingresoVigente  = capturarMovimientosIntrahospitalariosRepository.
    			buscarIngresoVigenteUCIN(datosHospializacion.getDatosPaciente().getCvePaciente());
    	
    	if(ingresoVigente != null && !ingresoVigente.isEmpty()){
    		existeIngresoVigente = Boolean.TRUE;
    	}
    	
    	return existeIngresoVigente;
    		
    }
    
    public void validarMatricula(String matricula) throws HospitalizacionException {

    	capturarMovimientosIntrahospitalariosRules.validarMatricula(matricula);
    }
    
    @Override
    public void validarHora(String hora) throws HospitalizacionException {

        if (hora == null || hora.isEmpty() || hora.equals(BaseConstants.HORA_VACIA)
                || hora.contains(CapturarIngresosConstants.MASCARA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_HORA));
        }

        int hr = Integer.valueOf(hora.substring(0, 2));
        int min = Integer.valueOf(hora.substring(3));
        if ((hr < 0 || hr > CapturarIngresosConstants.HORA_MAXIMA)
                || (min < 0 || min > CapturarIngresosConstants.MINUTO_MAXIMO)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_031);
        }
    }
    
    public boolean validarHoraActualMayor(DatosHospitalizacion datosHospitalizacion, String fechaMovIntra){
    	
    	Date fechaAnterior = capturarMovimientosIntrahospitalariosHelper.obtenerFechaCompleta(fechaMovIntra,
                datosHospitalizacion.getDatosMovimientos().getHoraAnterior());
    	
    	return capturarMovimientosIntrahospitalariosRules.compararHoraActual(fechaAnterior,
    		     capturarMovimientosIntrahospitalariosHelper.obtenerFechaCompleta(fechaMovIntra,
    		    		 datosHospitalizacion.getDatosMovimientos().getHoraActual()));
    	
    }
    
    public Date convertirHora(String fecha, String hora) {

        return capturarMovimientosIntrahospitalariosHelper
                .convertStringHourToDate(capturarMovimientosIntrahospitalariosHelper.convertStringToDate(fecha), hora);
    }
    
    public String obtenerSiguienteDia(String fecha) {

        return capturarMovimientosIntrahospitalariosHelper.obtenerSiguienteDia(fecha);
    }

}
