package mx.gob.imss.simo.hospitalizacion.common.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;

@FacesConverter("unidadMedicaConverter")
public class UnidadMedicaConverter implements Converter {

    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {

        if (value != null && value.trim().length() > 0) {
            try {
                UnidadMedica unidad = new UnidadMedica();
                unidad.setCvePresupuestal(value);
                return unidad;
            } catch (Exception e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en la conversi�n",
                        "Unidad M�dica no v�lida"));
            }
        } else {
            return null;
        }
    }

    public String getAsString(FacesContext fc, UIComponent uic, Object object) {

        if (object != null) {
            return String.valueOf(((UnidadMedica) object).getCvePresupuestal());
        } else {
            return null;
        }
    }
}
