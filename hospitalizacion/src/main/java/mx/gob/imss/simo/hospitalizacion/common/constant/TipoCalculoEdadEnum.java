package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoCalculoEdadEnum {
	
	ANIOS, 
	SEMANAS;

}