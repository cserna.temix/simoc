package mx.gob.imss.simo.hospitalizacion.intervencionesqx.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;

@Component
public class CapturarIntervencionesQuirurgicasHelper extends HospitalizacionCommonHelper {

    public DatosCatalogo elementoSelecionado(SelectEvent evento, List<String> listaTipoEgreso,
            AutoComplete tipoEgresoComplete, List<DatosCatalogo> catalogoTipoEgreso) {

        int indice = listaTipoEgreso.indexOf(tipoEgresoComplete.getValue().toString());
        return catalogoTipoEgreso.get(indice);
    }

    public List<Procedimientos> inicializarTotalProcedimientos(int numeroProcedimientos) {

        List<Procedimientos> listDatosProcedimientos = new ArrayList<Procedimientos>();
        for (int i = 1; i <= numeroProcedimientos; i++) {
            Procedimientos datosProcedimientos = new Procedimientos();
            datosProcedimientos.setConsecutivo(i);
            listDatosProcedimientos.add(datosProcedimientos);
        }
        return listDatosProcedimientos;
    }

    public Date convertStringHourToDate(Date fecha, String horaEntrada) {

        String[] aux = horaEntrada.split(":");
        Calendar fechaAux = Calendar.getInstance();
        fechaAux.setTime(mkDateIni(fecha));
        fechaAux.set(Calendar.HOUR_OF_DAY, Integer.parseInt(aux[0]));
        fechaAux.set(Calendar.MINUTE, Integer.parseInt(aux[1]));
        return fechaAux.getTime();
    }

    public Date obtenerFechaCompleta(String fecha, String hora) {

        Date fechaDia = convertStringToDate(fecha);
        return convertStringHourToDate(fechaDia, hora);
    }

    public DatosHospitalizacion armarIntervenciones(DatosHospitalizacion datosHospitalizacion) {

        if (null == datosHospitalizacion.getDatosIntervenciones().getCantidadPlanificacion()
                || datosHospitalizacion.getDatosIntervenciones().getCantidadPlanificacion().isEmpty()) {
            datosHospitalizacion.getDatosIntervenciones().setCantidadMetodoPlanificacion(null);
        } else {
            datosHospitalizacion.getDatosIntervenciones().setCantidadMetodoPlanificacion(
                    Integer.parseInt(datosHospitalizacion.getDatosIntervenciones().getCantidadPlanificacion()));
        }
        return datosHospitalizacion;
    }

}
