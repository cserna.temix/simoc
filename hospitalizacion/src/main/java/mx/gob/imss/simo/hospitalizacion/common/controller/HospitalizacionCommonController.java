package mx.gob.imss.simo.hospitalizacion.common.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.MetodoAnticonceptivo;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.model.DatosUsuario;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.service.DatosSesionService;

@Data
@ViewScoped
@ManagedBean(name = "hospitalizacionCommonController")
public abstract class HospitalizacionCommonController extends BaseUIComponentController {

    private static final long serialVersionUID = 1L;

    private boolean activarDialogo;
    private boolean banderaNss;
    private DatosCatalogo datosCatalogo;
    private String ubicacion;
    private List<DatosCatalogo> catalogoPlanificacion;
    private TipoFormatoEnum tipoFormato;
    private List<String> listaPlanificacionFamiliar;

    @ManagedProperty("#{catalogosHospitalizacionServices}")
    private CatalogosHospitalizacionServices catalogosHospitalizacionServices;

    @ManagedProperty("#{hospitalizacionCommonServices}")
    private HospitalizacionCommonServices hospitalizacionCommonServices;

    @ManagedProperty("#{objetosSs}")
    private ObjetosEnSesionBean objetosSs;

    @ManagedProperty("#{datosSesionService}")
    private transient DatosSesionService datosSesionService;

    public abstract String obtenerNombrePagina();

    public abstract void salir() throws IOException;

    @Override
    @PostConstruct
    public void init() {

        super.init();
        datosCatalogo = new DatosCatalogo();
        listaPlanificacionFamiliar = new ArrayList<>();
    }

    public void validarAgregadoMedico() throws HospitalizacionException {

        getDatosHospitalizacion().getDatosPaciente()
                .setAgregadoMedico(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico().toUpperCase());
        hospitalizacionCommonServices
                .validarAgregadoMedico(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
    }

    public List<String> filtrarCatalogo(List<String> datosCatalogoString, String query, Integer longitud) {

        return hospitalizacionCommonServices.filtrarCatalogo(datosCatalogoString, query, longitud);
    }

    public List<String> filtrarCatalogoDelegacion(String query) {

        return filtrarCatalogo(getCatalogoDelegacionString(), query, BaseConstants.LONGITUD_DELEGACION);
    }

    public List<String> filtrarCatalogoEspecialidad(String query) {

        return filtrarCatalogo(getCatalogoEspecialidadString(), query, BaseConstants.LONGITUD_ESPECIALIDAD);
    }

    public List<String> filtrarCatalogoEspecialidadHospitalizacion(String query) {

        return filtrarCatalogo(getCatalogoEspecialidadHospitalizacionString(), query,
                BaseConstants.LONGITUD_ESPECIALIDAD);
    }

    public List<String> filtrarCatalogoPlanificacionFamiliar(String query) {

        return filtrarCatalogo(getListaPlanificacionFamiliar(), query, BaseConstants.LONGITUD_PLANIFICACION);
    }

    public DatosUsuario obtenerDatosUsuario() {

        return objetosSs.getDatosUsuario();
    }

    public void obtenerPaciente() {

        setBanderaModalCama(BaseConstants.CADENA_VACIA);
        setBanderaModalPaciente(BaseConstants.CADENA_VACIA);
        if (getBanderaNss()) {
            if (getTipoUbicacion() != null) {
                ubicacion = getTipoUbicacion().getClave();
            }
            setListaDatosPaciente(hospitalizacionCommonServices.buscarPacientesNss(
                    getDatosHospitalizacion().getDatosPaciente().getNss(), ubicacion,
                    getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico(),
                    obtenerDatosUsuario().getCvePresupuestal()));
            if (!getListaDatosPaciente().isEmpty() && getListaDatosPaciente().size() > BaseConstants.INDEX_UNO) {
                setListDatosHospitalizacion(hospitalizacionCommonServices.crearListaPacientes(getListaDatosPaciente()));
                setNumeroPaciente(null);
                setBanderaModalPaciente(BaseConstants.BANDERA_MODAL_PACIENTE);
                String nombrePagina = obtenerNombrePagina();
                if (!nombrePagina.equals(PagesCommonConstants.MOVIMIENTOS_INTRAHOSPITALARIOS)) {
                    RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PACIENTES);
                    setTieneFoco(getTextNumeroPaciente().getClientId());
                }
            } else if (!getListaDatosPaciente().isEmpty()
                    && getListaDatosPaciente().size() == BaseConstants.INDEX_UNO) {
                DatosHospitalizacion datosHospitalizacionAux = hospitalizacionCommonServices
                        .crearPaciente(getListaDatosPaciente());
                getDatosHospitalizacion().setDatosPaciente(datosHospitalizacionAux.getDatosPaciente());
                getDatosHospitalizacion().setDatosIngreso(datosHospitalizacionAux.getDatosIngreso());
            } else if (requiereBuscarPorNumeroCama()) {
                agregarMensajeAdvertencia(getArray(MensajesErrorConstants.MG_OO4), null);
                getTextCama().setDisabled(Boolean.FALSE);
                getTextNss().setDisabled(Boolean.TRUE);
                setTieneFoco(getTextCama().getClientId());
                setBanderaModalCama(BaseConstants.BANDERA_MODAL_CAMA);
            }
        }

    }
    
    public void validarPlanificacionFamiliar(String cvePlanificacionFam, Date fecCaptura) throws HospitalizacionException {

        hospitalizacionCommonServices.validarPlanificacion(cvePlanificacionFam);
        MetodoAnticonceptivo datosPlanificacionFamiliar = hospitalizacionCommonServices
                .buscarPlanificacion(cvePlanificacionFam, getTipoFormato().getClave());
        hospitalizacionCommonServices.validarPlanificacionEdadFec(datosPlanificacionFamiliar,
                getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico(), fecCaptura);
        hospitalizacionCommonServices.validarPlanificacionSexo(datosPlanificacionFamiliar,
                getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
    }
    
    
    
    

    public void validarCantidadPlanificacion(String cvePlanificacionFamiliar) throws HospitalizacionException {

        if (getNumeroCantidad() != null) {
            if (hospitalizacionCommonServices.validarPlanificacionCantidad(hospitalizacionCommonServices
                    .buscarPlanificacion(cvePlanificacionFamiliar, getTipoFormato().getClave()), getNumeroCantidad())) {
                RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CANTIDAD);
            } else {
                throw new HospitalizacionException(MensajesErrorConstants.ME_055);
            }
        } else {
            setTieneFoco(getTextCantidad().getClientId());
        }
    }

    public void obtenerPacientePorNumeroCama() {

        setBanderaModalCama(BaseConstants.CADENA_VACIA);
        if (banderaNss) {
            setListaDatosPaciente(hospitalizacionCommonServices.buscarPacienteNumeroCama(
                    getDatosHospitalizacion().getDatosPaciente().getCama(), obtenerDatosUsuario().getCvePresupuestal(),
                    ubicacion));
            setListDatosHospitalizacion(hospitalizacionCommonServices.crearListaPacientes(getListaDatosPaciente()));
            try {
                hospitalizacionCommonServices.validarPacientesNumeroCama(getListDatosHospitalizacion());
                if (getListaDatosPaciente().size() > 1) {
                    setBanderaModalCama(BaseConstants.BANDERA_MODAL_CAMA);
                    setNumeroCama(null);
                    setTieneFoco(getTextNumeroCama().getClientId());
                    RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_CAMA_PACIENTES);
                } else if (getListaDatosPaciente().size() == 1) {
                    getDatosHospitalizacion().setDatosPaciente(
                            hospitalizacionCommonServices.mapearDatosPacienteCama(getListDatosHospitalizacion()));
                }
            } catch (HospitalizacionException e) {
                agregarMensajeError(e);
                getDatosHospitalizacion().getDatosPaciente().setCama(BaseConstants.CADENA_VACIA);
                getDatosHospitalizacion().getDatosPaciente().setNss(BaseConstants.CADENA_VACIA);
                getTextCama().setDisabled(Boolean.TRUE);
                getTextNss().setDisabled(Boolean.FALSE);
                getTextNss().resetValue();
                setTieneFoco(getTextNss().getClientId());
            }
        }

    }

    public void mapearDatosPaciente() {

        if (getNumeroPaciente() != null && !getListDatosHospitalizacion().isEmpty()) {
            if (hospitalizacionCommonServices.validarNumeroIngresado(getNumeroPaciente(),
                    getListDatosHospitalizacion())) {
                getDatosHospitalizacion().setDatosPaciente(hospitalizacionCommonServices
                        .mapearDatosPacientePorNumero(getNumeroPaciente(), getListDatosHospitalizacion()));
                RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES);
                setActivarDialogo(Boolean.FALSE);
            } else {
                setNumeroPaciente(null);
                setTieneFoco(getTextNumeroPaciente().getClientId());
            }
        } else {
            setTieneFoco(getTextNumeroPaciente().getClientId());
        }
    }

    public void mapearDatosPacienteNumeroCama() {

        if (getNumeroCama() != null && !getListDatosHospitalizacion().isEmpty()) {
            if (hospitalizacionCommonServices.validarNumeroIngresado(getNumeroCama(), getListDatosHospitalizacion())) {
                getDatosHospitalizacion().setDatosPaciente(hospitalizacionCommonServices
                        .mapearDatosPacienteNumeroCama(getNumeroCama(), getListDatosHospitalizacion()));
                getDatosHospitalizacion().getDatosPaciente()
                        .setNss(getDatosHospitalizacion().getDatosPaciente().getNss());
                getDatosHospitalizacion().getDatosIngreso()
                        .setCama(getDatosHospitalizacion().getDatosPaciente().getCama());
                setBanderaModalCama(BaseConstants.CADENA_VACIA);
                RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CAMA_PACIENTES);
            } else {
                setNumeroCama(null);
                setTieneFoco(getTextNumeroCama().getClientId());
            }
        } else {
            setTieneFoco(getTextNumeroCama().getClientId());
        }
    }

    public List<String> filtrarListasComplementarias(String cveFiltro, Integer numeroMaximo,
            List<String> listaOriginal) {

        return hospitalizacionCommonServices.filtrarListasComplementarias(cveFiltro, numeroMaximo, listaOriginal);
    }

    public void validarModalPlanificacion(String cvePlanificacionFamiliar) {

        setBanderaModalCantidad(BaseConstants.CADENA_VACIA);
        if (cvePlanificacionFamiliar != null && hospitalizacionCommonServices
                .validarModalPlanificacion(cvePlanificacionFamiliar, getTipoFormato().getClave())) {
            setNumeroCantidad(null);
            setBanderaModalCantidad(BaseConstants.BANDERA_MODAL_CANTIDAD);
            RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_CANTIDAD);
            setTieneFoco(getTextCantidad().getClientId());
        } else {
            setTieneFoco(getTextMatricula().getClientId());
        }
    }

    public boolean requiereBuscarPorNumeroCama() {

        return true;
    }

    public HospitalizacionCommonServices getHospitalizacionCommonServices() {

        return hospitalizacionCommonServices;
    }

    public void setHospitalizacionCommonServices(HospitalizacionCommonServices hospitalizacionCommonServices) {

        this.hospitalizacionCommonServices = hospitalizacionCommonServices;
    }

    public boolean isActivarDialogo() {

        return activarDialogo;
    }

    public void setActivarDialogo(boolean activarDialogo) {

        this.activarDialogo = activarDialogo;
    }

    public boolean getBanderaNss() {

        return banderaNss;
    }

    public void setBanderaNss(boolean banderaNss) {

        this.banderaNss = banderaNss;
    }

    public ObjetosEnSesionBean getObjetosSs() {

        return objetosSs;
    }

    public void setObjetosSs(ObjetosEnSesionBean objetosSs) {

        this.objetosSs = objetosSs;
    }

    public DatosCatalogo getDatosCatalogo() {

        return datosCatalogo;
    }

    public void setDatosCatalogo(DatosCatalogo datosCatalogo) {

        this.datosCatalogo = datosCatalogo;
    }

    public List<DatosCatalogo> getCatalogoPlanificacion() {

        return catalogoPlanificacion;
    }

    public void setCatalogoPlanificacion(List<DatosCatalogo> catalogoPlanificacion) {

        this.catalogoPlanificacion = catalogoPlanificacion;
    }

    public TipoFormatoEnum getTipoFormato() {

        return tipoFormato;
    }

    public void setTipoFormato(TipoFormatoEnum tipoFormato) {

        this.tipoFormato = tipoFormato;
    }

    public String getUbicacion() {

        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {

        this.ubicacion = ubicacion;
    }

    public List<String> getListaPlanificacionFamiliar() {

        return listaPlanificacionFamiliar;
    }

    public void setListaPlanificacionFamiliar(List<String> listaPlanificacionFamiliar) {

        this.listaPlanificacionFamiliar = listaPlanificacionFamiliar;
    }

    public CatalogosHospitalizacionServices getCatalogosHospitalizacionServices() {

        return catalogosHospitalizacionServices;
    }

    public void setCatalogosHospitalizacionServices(CatalogosHospitalizacionServices catalogosHospitalizacionServices) {

        this.catalogosHospitalizacionServices = catalogosHospitalizacionServices;
    }

    public DatosSesionService getDatosSesionService() {

        return datosSesionService;
    }

    public void setDatosSesionService(DatosSesionService datosSesionService) {

        this.datosSesionService = datosSesionService;
    }

}
