package mx.gob.imss.simo.hospitalizacion.reportes.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.confirmdialog.ConfirmDialog;
import org.primefaces.context.RequestContext;
import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoReporte;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCama;
import mx.gob.imss.simo.hospitalizacion.common.model.PacientesHospitalizados;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.ReporteGenerado;
import mx.gob.imss.simo.hospitalizacion.reportes.services.ReportesServices;

@Data
@ViewScoped
@ManagedBean(name = "reportePacientesHospitalController")
public class ReportePacientesHospitalController extends HospitalizacionCommonController {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty("#{reportePacientesHospitalizados}")
    private transient AbstractJasperReportsSingleFormatView plantillaReportePacientesHospitalizados;

    @ManagedProperty("#{reportesServices}")
    protected transient ReportesServices reportesServices;

    @ManagedProperty("#{hospitalizacionCommonHelper}")
    private transient HospitalizacionCommonHelper hospitalizacionCommonHelper;

    private String fechaIngreso;
    protected transient ConfirmDialog alertaGenerarReporte;
    private transient CommandButton confirmSi;
    private transient CommandButton confirmNo;

    private transient List<PacientesHospitalizados> pacientesHospitalizadosList;
    private transient ReporteGenerado datosReporteGen;
    private transient Date dateFecha;
    private transient List<OcupacionCama> noOcupacion;

    @Override
    @PostConstruct
    public void init() {

        super.init();
        establecerFocoEnCampo();
        confirmSi = new CommandButton();
        confirmNo = new CommandButton();
        alertaGenerarReporte = new ConfirmDialog();
    }

    public void fileXlsSpring() {

        try {
            pacientesHospitalizadosList = null;
            reportesServices.validarFechaRequerida(this.getMaskFecha().getValue().toString());

            datosReporteGen = new ReporteGenerado();
            datosReporteGen.setCvePresupuestal(obtenerDatosUsuario().getCvePresupuestal());
            datosReporteGen.setTipoReporte(TipoReporte.PACIENTES_HOSPITALIZADOS.getClave());

            // Existe alguna cama a a que esten asignados 2 o m�s pacientes?
            noOcupacion = reportesServices
                    .buscarOcupacionCamaPorPresupuestal(obtenerDatosUsuario().getCvePresupuestal());

            logger.debug("noOcupacion:" + noOcupacion);

            /*
             * Se valida que ya exista el registro del reporte generado sin validacion de 2 pacientes asignados
             * en alguna cama
             */
            if (existeRegistroReportePacientes(fechaIngreso)) {
                establecerDatosReporteYGenerarReporte();
            }

            /*
             * Se valida que ya exista el registro del reporte generado con validacion de 2 pacientes asignados
             * en alguna cama
             */
            /*
             * else if (existeRegistroReportePacientes( fechaIngreso ) &&
             * !(noOcupacion != null && noOcupacion.size() >= 1)) {
             * establecerDatosReporteYGenerarReporte();
             * }
             */

            /*
             * Se genera el registro del reporte generado y se valida si hay mas de 1 paciente en alguna cama
             */
            else {
                dateFecha = hospitalizacionCommonHelper.convertStringToDate(fechaIngreso);
                String reportDate = hospitalizacionCommonHelper
                        .converDateToString(hospitalizacionCommonHelper.agregarDiasAFecha(dateFecha, -1));
                if (existeRegistroReportePacientes(reportDate)) {
                    datosReporteGen.setFecGeneracion(dateFecha);
                    logger.debug("LA FECHA DE IGRESO:" + fechaIngreso);
                    logger.debug("PRESUPUESTAL:" + obtenerDatosUsuario().getCvePresupuestal());
                    // Validar si hay informacion para mostrar en el reporte
                    if (reportesServices.numeroIngresosPacientesHospitalizadosFecha(
                            obtenerDatosUsuario().getCvePresupuestal(), fechaIngreso) == 0) {
                        // ME-177 Aqu� se abre una ventana para verificar si se quiere generar el reporte
                        RequestContext.getCurrentInstance().execute("PF('repPacHospConfirm').show();");
                    } else {
                        establecerDatosReporteYGenerarReporte();
                        logger.debug("pacientesHospitalizadosList:" + pacientesHospitalizadosList);
                        if (noOcupacion != null && noOcupacion.isEmpty()) {
                            reportesServices.guardarReporteGenerado(datosReporteGen);
                        }
                        // generarReporte(pacientesHospitalizadosList, dateFecha);
                    }

                } else {
                    fechaIngreso = "";
                    agregarMensajeError(getArray(MensajesErrorConstants.ME_147), null);
                }
            }
        } catch (HospitalizacionException e) {
            logger.error("Error al generar el reporte de pacientes: " + e.getMessage(), e);
            logger.error("Ocurrrio un error al validar la fecha final: " + e.getMessage(), e);
            resetFechaFinal();
            this.setTieneFoco("repPacientesHospital:idFechaRep");
            agregarMensajeError(e);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void validaFecha() {

        try {
            String cvePeriodoImss = reportesServices
                    .periodoMasActualAbierto(obtenerDatosUsuario().getCvePresupuestal());
            PeriodosImss periodo = reportesServices.obtenerPeriodoImssXcvePeriodoImss(cvePeriodoImss);

            reportesServices.validarFechaRequerida(this.getMaskFecha().getValue().toString());
            reportesServices.validarFechaPacientesHosp(getMaskFecha().getValue().toString(), periodo.getFechaFinal());

            setFechaIngreso(getMaskFecha().getValue().toString());
            this.setTieneFoco("repPacientesHospital:idBtnV");

        } catch (HospitalizacionException e) {
            logger.error("Ocurrrio un error al validar la fecha final: " + e.getMessage(), e);
            resetFechaFinal();
            this.setTieneFoco("repPacientesHospital:idFechaRep");
            agregarMensajeError(e);
        }
    }

    private boolean existeRegistroReportePacientes(String fecha) {

        return reportesServices.validarReporteGeneradoHospPorFecha(datosReporteGen.getCvePresupuestal(),
                TipoReporte.PACIENTES_HOSPITALIZADOS.getClave(), fecha);
    }

    private void establecerDatosReporteYGenerarReporte() throws Exception {

        datosReporteGen.setFecGeneracion(hospitalizacionCommonHelper.convertStringToDate(fechaIngreso));
        pacientesHospitalizadosList = new ArrayList<>();
        pacientesHospitalizadosList = reportesServices
                .obtenerPacientesHospitalizadosFecha(obtenerDatosUsuario().getCvePresupuestal(), fechaIngreso);
        generarReporte(pacientesHospitalizadosList, hospitalizacionCommonHelper.convertStringToDate(fechaIngreso));
    }

    public void generarReporte(List<PacientesHospitalizados> pacientesHospitalizadosList, Date dateFecha)
            throws Exception {

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        final Map<String, Object> model = new HashMap<>();
        final String logoImss = context.getRealPath(File.separator + "img" + File.separator + "imss_header.png");

        logger.info("fileXlsSpring obtenerDatosUsuario().getCvePresupuestal(): {"
                + obtenerDatosUsuario().getCvePresupuestal() + "}");
        model.put("logoImss", logoImss);
        model.put("fechaEncabezado", dateFecha);
        model.put("cvePresupuestal", obtenerDatosUsuario().getCvePresupuestal());
        model.put("nombreUnidad", obtenerDatosUsuario().getDesUnidadMedicaAct());
        model.put("nombreDelegacion", obtenerDatosUsuario().getDesDelegacionAct());
        model.put("perfil", obtenerDatosUsuario().getPerfil());
        model.put("data", pacientesHospitalizadosList);

        plantillaReportePacientesHospitalizados.render(model, (HttpServletRequest) context.getRequest(),
                (HttpServletResponse) context.getResponse());

    }

    public void confirmAcepto() {

        pacientesHospitalizadosList = reportesServices
                .obtenerPacientesHospitalizadosFecha(obtenerDatosUsuario().getCvePresupuestal(), fechaIngreso);
        try {
            generarReporte(pacientesHospitalizadosList, dateFecha);
            if (noOcupacion != null && noOcupacion.isEmpty()) {
                reportesServices.guardarReporteGenerado(datosReporteGen);
            }

        } catch (Exception e) {
            logger.error("Error al generar el reporte de pacientes: " + e.getMessage(), e);
        }

        resetFechaFinal();
        establecerFocoEnCampo();
        RequestContext.getCurrentInstance().execute("PF('repPacHospConfirm').hide();");

    }

    public void confirmRechazo() {

        resetFechaFinal();
        establecerFocoEnCampo();
        RequestContext.getCurrentInstance().execute("PF('repPacHospConfirm').hide();");
    }

    private void resetFechaFinal() {

        getMaskFecha().resetValue();
        fechaIngreso = null;
    }

    @Override
    public String obtenerNombrePagina() {

        return PagesCommonConstants.REPORTE_PACIENTES_HOSPITAL;
    }

    @Override
    public void salir() throws IOException {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }

    private void establecerFocoEnCampo() {

        this.setTieneFoco("repPacientesHospital:idFechaRep");
    }

}
