package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;

public class IntervencionesQXRowMapperHelper extends BaseHelper implements RowMapper<IntervencionesQuirurgicas> {

    @Override
    public IntervencionesQuirurgicas mapRow(ResultSet resultSet, int rowId) throws SQLException {

        IntervencionesQuirurgicas intervencionesQuirurgicas = new IntervencionesQuirurgicas();

        intervencionesQuirurgicas.setClaveIngreso(resultSet.getLong(SQLColumnasConstants.CVE_INGRESO));
        intervencionesQuirurgicas.setFechaCreacion(resultSet.getDate(SQLColumnasConstants.FEC_CREACION));
        intervencionesQuirurgicas.setFechaActualizacion(resultSet.getDate(SQLColumnasConstants.FEC_ACTUALIZACION));
        intervencionesQuirurgicas.setClaveCapturista(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA));
        intervencionesQuirurgicas
                .setClaveCapturistaActualiza(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA_ACTUALIZA));
        intervencionesQuirurgicas.setFechaIntervencionIqx(resultSet.getDate(SQLColumnasConstants.FEC_INTERVENCION_IQX));
        intervencionesQuirurgicas.setClaveSala(resultSet.getString(SQLColumnasConstants.CVE_SALA));
        intervencionesQuirurgicas.setClavePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        intervencionesQuirurgicas
                .setClaveEspecialidadIqx(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD_IQX));
        intervencionesQuirurgicas.setNumProcedimientos(resultSet.getInt(SQLColumnasConstants.NUM_PROCEDIMIENTOS));
        intervencionesQuirurgicas.setFechaEntradaSala(resultSet.getDate(SQLColumnasConstants.FEC_ENTRADA_SALA));
        intervencionesQuirurgicas.setFechaInicioIqx(resultSet.getDate(SQLColumnasConstants.FEC_INICIO));
        intervencionesQuirurgicas.setFechaSalidaSala(resultSet.getDate(SQLColumnasConstants.FEC_SALIDA_SALA));
        intervencionesQuirurgicas
                .setClaveTipoIntervencion(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_INTERVENCION));
        intervencionesQuirurgicas.setFechaProgramacion(resultSet.getDate(SQLColumnasConstants.FEC_PROGRAMACION));
        intervencionesQuirurgicas
                .setClaveMetodoAnticonceptivo(resultSet.getInt(SQLColumnasConstants.CVE_METODO_ANTICONCEPTIVO));
        intervencionesQuirurgicas
                .setCantidadMetodoAnticonceptivo(resultSet.getInt(SQLColumnasConstants.CAN_METODO_ANTICONCEPTIVO));
        intervencionesQuirurgicas.setClaveTipoAnestesia(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_ANESTESIA));
        intervencionesQuirurgicas.setClaveAnestesiologo(resultSet.getString(SQLColumnasConstants.CVE_ANESTESIOLOGO));
        intervencionesQuirurgicas.setClaveTipoFormato(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_FORMATO));

        return intervencionesQuirurgicas;
    }

}
