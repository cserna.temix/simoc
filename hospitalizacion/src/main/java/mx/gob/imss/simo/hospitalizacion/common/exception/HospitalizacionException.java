package mx.gob.imss.simo.hospitalizacion.common.exception;

public class HospitalizacionException extends BaseException {

    public HospitalizacionException(String... claves) {
        super(claves);
    }

    public HospitalizacionException(String[] claves, Throwable throwable) {
        super(claves, throwable);
    }

    public HospitalizacionException(String[] claves, String[] args) {
        super(claves, args);
    }

    public HospitalizacionException(String[] claves, String[] args, Throwable throwable) {
        super(claves, args, throwable);
    }

}
