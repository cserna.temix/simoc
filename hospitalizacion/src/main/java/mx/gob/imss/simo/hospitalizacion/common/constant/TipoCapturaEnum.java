package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoCapturaEnum {
    
    CONSULTA_EXTERNA(1, "Consulta externa"),
    HOSPITALIZACION(2, "Hospitalizaci\u00F3n"),
    INFORMACION_COMPLEMENTARIA(3, "Informaci\u00F3n complementaria"),
    SERVICIOS_SUBROGADOS(4, "Servicios subrogados");

    private Integer clave;
    private String descripcion;

    private TipoCapturaEnum(Integer clave, String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

    
    public Integer getClave() {
    
        return clave;
    }

    
    public String getDescripcion() {
    
        return descripcion;
    }

}
