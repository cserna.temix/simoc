package mx.gob.imss.simo.hospitalizacion.common.exception;

import net.sf.jasperreports.engine.JRException;

public class HospitalizacionRuntimeException extends RuntimeException {

	public HospitalizacionRuntimeException(JRException e) {
		super(e);
	}

}
