package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Rol {

    private String cveRol;
    private String refRol;
    private String desRol;
    private Date fecBaja;

}
