package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class RangoLubchenco {

    private int rango;
    private int semanasGestacion;
    private int pesoSuperior;
    private int pesoInferior;
    private int clave;
    private int claveClasificacionPeso;

}
