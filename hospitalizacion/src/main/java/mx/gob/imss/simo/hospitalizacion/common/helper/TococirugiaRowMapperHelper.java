package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;

public class TococirugiaRowMapperHelper extends BaseHelper implements RowMapper<Tococirugia> {

    @Override
    public Tococirugia mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        Tococirugia tococirugia = new Tococirugia();

        tococirugia.setClave(resultSet.getLong(SQLColumnasConstants.CVE_TOCOCIRUGIA));
        tococirugia.setFechaCreacion(resultSet.getDate(SQLColumnasConstants.FEC_CREACION));
        tococirugia.setFechaActualizacion(resultSet.getDate(SQLColumnasConstants.FEC_ACTUALIZACION));
        tococirugia.setFechaAtencion(resultSet.getDate(SQLColumnasConstants.FEC_TOCOCIRUGIA));
        tococirugia.setCveSala(resultSet.getString(SQLColumnasConstants.CVE_SALA));
        tococirugia.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        tococirugia.setCveTipoAtencion(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_ATENCION));
        tococirugia.setCveTipoParto(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_PARTO));
        tococirugia.setCveMetodoAnticonceptivo(resultSet.getInt(SQLColumnasConstants.CVE_METODO_ANTICONCEPTIVO));
        tococirugia.setCantidadMetodoAnticonceptivo(resultSet.getInt(SQLColumnasConstants.CAN_METODO_ANTICONCEPTIVO));
        tococirugia.setTotalRecienNacidos(resultSet.getInt(SQLColumnasConstants.NUM_TOTAL_RN));
        tococirugia.setCveMedico(resultSet.getString(SQLColumnasConstants.CVE_MEDICO));
        tococirugia.setCveCapturista(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA));
        tococirugia.setCveCapturistaActualizada(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA_ACTUALIZA));

        return tococirugia;
    }

}
