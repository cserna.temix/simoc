package mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.rules;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MovimientosIntrahospitalariosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCama;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;

@Component
public class CapturarMovimientosIntrahospitalariosRules extends HospitalizacionCommonRules {

    public void validarCama(String cama) throws HospitalizacionException {

        if (cama == null || cama.isEmpty() || cama.equals(MovimientosIntrahospitalariosConstants.CAMA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(MovimientosIntrahospitalariosConstants.REQUERIDO_CAMA));
        }
    }

    public void validarEspecialidadParaMovimientoIntra(boolean indicador) throws HospitalizacionException {

        if (!indicador) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_091);
        }
    }

    public void validarExistenciaCama(List<CamaUnidad> cama) throws HospitalizacionException {

        if (null == cama || cama.isEmpty()) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_034);
        }
    }

    public int validarOcupacionCama(List<OcupacionCama> ocupacionCama) throws HospitalizacionException {

        int ocupacion = 0;
        if (null != ocupacionCama && !ocupacionCama.isEmpty()) {
            for (OcupacionCama oC : ocupacionCama) {
                ocupacion = oC.getNumPacientes();
                break;
            }
        }
        if (ocupacion > 1) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_148);
        }
        return ocupacion;
    }
   
    
    public List<CamaUnidad> validarCamaAutorizada(List<CamaUnidad> ocupacionCama, Long tipoFormato) throws HospitalizacionException {

    	List<CamaUnidad> camasAutorizadas = new ArrayList<>();
        for (CamaUnidad cama : ocupacionCama) {
            /*if (null != cama.getFecBaja()) {
                throw new HospitalizacionException(MensajesErrorConstants.ME_036);
            }*/ 	
        	
        	 if( tipoFormato == TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave().intValue()){
                 if (!cama.getCveEspecialidad().startsWith(BaseConstants.CLAVE_URGENCIAS)
                         && !cama.getCveEspecialidad().startsWith(BaseConstants.CLAVE_UCI)
                         && !cama.getCveEspecialidad().equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)
                         && !cama.getCveEspecialidad().startsWith(BaseConstants.CLAVE_CUNERO_PATOLOGICO)
                         && !cama.getCveEspecialidad().startsWith(BaseConstants.CLAVE_UCI_NEONATOS)
                         && !cama.getCveEspecialidad().startsWith(BaseConstants.CLAVE_URGENCIAS_TOCOCIRUGICA)
                         && !cama.getCveEspecialidad().startsWith(BaseConstants.ESPECIALIDAD_CIRUGIA_AMBULATORIA)
                         && cama.getFecBaja() == null ) {
                     camasAutorizadas.add(cama);
                 }
        	 }else {
        		 if(tipoFormato == TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()
        			 || tipoFormato == TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue()){
        			 if (cama.getCveEspecialidad().startsWith(BaseConstants.CLAVE_CUNERO_PATOLOGICO)
        					 && cama.getFecBaja() == null) {
        				 camasAutorizadas.add(cama);
        			 }
        		 }
        	 }
        }
                 
        if (camasAutorizadas.isEmpty()) {
        		throw new HospitalizacionException(MensajesErrorConstants.ME_601);
        }
        return camasAutorizadas;
    }
    
    public List<DatosCatalogo> filtrarCatEspecialidaActualRN(List<DatosCatalogo> especialidadesRN, Boolean esRecienNacidoExterno){
    	
    	List<DatosCatalogo> catalogoEspecialidadRN = new ArrayList<DatosCatalogo>();
    	 for (DatosCatalogo catalogo : especialidadesRN) {
             if (catalogo.getClave().substring(0, 2).equals(CapturarIngresosConstants.CLAVE_CUNERO_PATOLOGICO)) {
            	 catalogoEspecialidadRN.add(catalogo); 
             }else if(catalogo.getClave().substring(0, 2).equals(CapturarIngresosConstants.CLAVE_CUNERO_FISIOLOGICO)
            		 && !esRecienNacidoExterno){
            	 catalogoEspecialidadRN.add(catalogo); 
             }
         }
    	 
    	 return catalogoEspecialidadRN;
    }
    
    public boolean compararHoraActual(Date fechaAnterior, Date fechaActual) {

    	return validarFecha(fechaAnterior, fechaActual);

    }
    
    public void validaCamaAlojamientoConjunto(int totalCamas, int ocupacionAlojamiento) throws HospitalizacionException{
    	if(totalCamas > BaseConstants.IND_CERO && ocupacionAlojamiento >= totalCamas){
    		throw new HospitalizacionException(MensajesErrorConstants.ME_610);
		}
    }

}
