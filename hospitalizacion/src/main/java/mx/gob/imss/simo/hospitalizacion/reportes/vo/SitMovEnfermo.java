/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.vo;

/**
 * @author francisco.rodriguez
 *
 */
public class SitMovEnfermo {

	public String cvePresupuestal;
	public String cvePeriodoImss;
	public Integer numAnteriorAdulto;
	public Integer numIngresoAdulto;
	public Integer numEgresoAdulto;
	public Integer numActualAdulto;
	public Integer numAnteriorCunero;
	public Integer numIngresoCunero;
	public Integer numEgresoCunero;
	public Integer numActualCunero;
	public Integer numEjecucion;

	/**
	 * @return the numEjecucion
	 */
	public Integer getNumEjecucion() {
		return numEjecucion;
	}

	/**
	 * @param numEjecucion the numEjecucion to set
	 */
	public void setNumEjecucion(Integer numEjecucion) {
		this.numEjecucion = numEjecucion;
	}

	/**
	 * @return the cvePresupuestal
	 */
	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	/**
	 * @param cvePresupuestal
	 *            the cvePresupuestal to set
	 */
	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	/**
	 * @return the cvePeriodoImss
	 */
	public String getCvePeriodoImss() {
		return cvePeriodoImss;
	}

	/**
	 * @param cvePeriodoImss
	 *            the cvePeriodoImss to set
	 */
	public void setCvePeriodoImss(String cvePeriodoImss) {
		this.cvePeriodoImss = cvePeriodoImss;
	}

	/**
	 * @return the numAnteriorAdulto
	 */
	public Integer getNumAnteriorAdulto() {
		return numAnteriorAdulto;
	}

	/**
	 * @param numAnteriorAdulto
	 *            the numAnteriorAdulto to set
	 */
	public void setNumAnteriorAdulto(Integer numAnteriorAdulto) {
		this.numAnteriorAdulto = numAnteriorAdulto;
	}

	/**
	 * @return the numIngresoAdulto
	 */
	public Integer getNumIngresoAdulto() {
		return numIngresoAdulto;
	}

	/**
	 * @param numIngresoAdulto
	 *            the numIngresoAdulto to set
	 */
	public void setNumIngresoAdulto(Integer numIngresoAdulto) {
		this.numIngresoAdulto = numIngresoAdulto;
	}

	/**
	 * @return the numEgresoAdulto
	 */
	public Integer getNumEgresoAdulto() {
		return numEgresoAdulto;
	}

	/**
	 * @param numEgresoAdulto
	 *            the numEgresoAdulto to set
	 */
	public void setNumEgresoAdulto(Integer numEgresoAdulto) {
		this.numEgresoAdulto = numEgresoAdulto;
	}

	/**
	 * @return the numActualAdulto
	 */
	public Integer getNumActualAdulto() {
		return numActualAdulto;
	}

	/**
	 * @param numActualAdulto
	 *            the numActualAdulto to set
	 */
	public void setNumActualAdulto(Integer numActualAdulto) {
		this.numActualAdulto = numActualAdulto;
	}

	/**
	 * @return the numAnteriorCunero
	 */
	public Integer getNumAnteriorCunero() {
		return numAnteriorCunero;
	}

	/**
	 * @param numAnteriorCunero
	 *            the numAnteriorCunero to set
	 */
	public void setNumAnteriorCunero(Integer numAnteriorCunero) {
		this.numAnteriorCunero = numAnteriorCunero;
	}

	/**
	 * @return the numIngresoCunero
	 */
	public Integer getNumIngresoCunero() {
		return numIngresoCunero;
	}

	/**
	 * @param numIngresoCunero
	 *            the numIngresoCunero to set
	 */
	public void setNumIngresoCunero(Integer numIngresoCunero) {
		this.numIngresoCunero = numIngresoCunero;
	}

	/**
	 * @return the numEgresoCunero
	 */
	public Integer getNumEgresoCunero() {
		return numEgresoCunero;
	}

	/**
	 * @param numEgresoCunero
	 *            the numEgresoCunero to set
	 */
	public void setNumEgresoCunero(Integer numEgresoCunero) {
		this.numEgresoCunero = numEgresoCunero;
	}

	/**
	 * @return the numActualCunero
	 */
	public Integer getNumActualCunero() {
		return numActualCunero;
	}

	/**
	 * @param numActualCunero
	 *            the numActualCunero to set
	 */
	public void setNumActualCunero(Integer numActualCunero) {
		this.numActualCunero = numActualCunero;
	}

}
