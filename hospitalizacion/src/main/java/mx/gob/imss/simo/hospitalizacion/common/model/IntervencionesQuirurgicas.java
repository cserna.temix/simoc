package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class IntervencionesQuirurgicas {

    private long claveIngreso;
    private Date fechaCreacion;
    private Date fechaActualizacion;
    private String claveCapturista;
    private String claveCapturistaActualiza;
    private Date fechaIntervencionIqx;
    private String claveSala;
    private String clavePresupuestal;
    private String claveEspecialidadIqx;
    private int numProcedimientos;
    private Date fechaEntradaSala;
    private Date fechaInicioIqx;
    private Date fechaTerminoIqx;
    private Date fechaSalidaSala;
    private int claveTipoIntervencion;
    private Date fechaProgramacion;
    private int claveMetodoAnticonceptivo;
    private int cantidadMetodoAnticonceptivo;
    private int claveTipoAnestesia;
    private String claveAnestesiologo;
    private int claveTipoFormato;

}
