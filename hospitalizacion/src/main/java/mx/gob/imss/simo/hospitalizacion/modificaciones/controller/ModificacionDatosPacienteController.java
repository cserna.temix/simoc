package mx.gob.imss.simo.hospitalizacion.modificaciones.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.hospitalizacion.modificaciones.service.ModificacionDatosPacienteService;

@Data
@ViewScoped
@ManagedBean(name = "modificacionDatosPacienteController")
public class ModificacionDatosPacienteController extends HospitalizacionCommonController {

    @ManagedProperty("#{modificacionDatosPacienteService}")
    private ModificacionDatosPacienteService modificacionDatosPacienteService;

    @ManagedProperty("#{catalogosHospitalizacionServices}")
    private CatalogosHospitalizacionServices catalogosHospitalizacionServices;

    private List<DatosCatalogo> catalogoDelegacion;
    private List<DatosCatalogo> ubicaciones;
    private List<String> listaUbicaciones;
    private List<DatosPaciente> pacientes;

    private DatosPaciente datosPacienteNuevos;
    private SelectOneMenu selectUbicaciones;

    private InputText textNssOriginal;
    private InputText textNombreOriginal;
    private InputText textApellidoPaternoOriginal;
    private InputText textApellidoMaternoOriginal;
    private InputText textClaveOriginal;
    private InputText textDescripcionOriginal;
    private InputText textClave;
    private InputText textDescripcion;
    private InputText textNumero;
    private InputMask maskFechaModificacion;

    private TipoUbicacionEnum tipoUbicacion;
    private String edad;
    private String delegacion;
    private Integer numeroPacienteNuevo;
    private InputText textNumeroPacienteNuevo;
    private String tipoUbicacionActual;
    private String fechaModificacion;
    private Boolean[] requeridos = { Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE, Boolean.FALSE, Boolean.FALSE};

    @Override
    public String obtenerNombrePagina() {

        // LoginController.setRenderIcons(false);
        return PagesCommonConstants.MODIFICACION_DATOS_PACIENTE;
    }

    @Override
    @PostConstruct
    public void init() {

        super.init();

        setTieneFoco(CapturarIngresosConstants.FOCO_FECHA_MODIFICACION);
        setUbicaciones(catalogosHospitalizacionServices.obtenerCatalogoUbicaciones());
        setSelectUbicaciones(new SelectOneMenu());
        datosPacienteNuevos = new DatosPaciente();
        setTextNssOriginal(new InputText());
        setButtonGuardar(new CommandButton());
        setButtonCancelar(new CommandButton());
        setTextNombreOriginal(new InputText());
        setTextApellidoPaternoOriginal(new InputText());
        setTextApellidoMaternoOriginal(new InputText());
        setTextClaveOriginal(new InputText());
        setTextDescripcionOriginal(new InputText());
        setSubtitulo(obtenerMensaje(CapturarIngresosConstants.SUBTITULO_INGRESO_HOSPITALIZACION));
        setTextNss(new InputText());
        setTextNombre(new InputText());
        setTextApellidoPaterno(new InputText());
        setTextApellidoMaterno(new InputText());
        setTextNumero(new InputText());
        setTextClave(new InputText());
        setTextDescripcion(new InputText());
        setMaskFechaModificacion(new InputMask());
        textNumeroPacienteNuevo = new InputText();
        deshabilitacampos();
        inicializarRequeridos();
    }

    private void inicializarRequeridos() {

        requeridos[0] = Boolean.FALSE; //ubicacion
        requeridos[1] = Boolean.FALSE;// nss original
        requeridos[2] = Boolean.FALSE; //nss nuevo
        requeridos[3] = Boolean.FALSE; //agregado medico
        requeridos[4] = Boolean.FALSE; //nombre nuevo
        requeridos[5] = Boolean.FALSE; //apellido paterno
        requeridos[6] = Boolean.FALSE; //numero
        requeridos[7] = Boolean.FALSE; //delegacion
        requeridos[8] = Boolean.FALSE; //fechaModificacion
    }
    
    private boolean sePuedeActivarGuardar(Boolean[] requeridos) {

        for (boolean req : requeridos) {
            if (!req) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }
    
    public void habilitarGuardar(){
    	if(sePuedeActivarGuardar(getRequeridos())){
    		 getButtonGuardar().setDisabled(Boolean.FALSE);
    	}else{
    		 getButtonGuardar().setDisabled(Boolean.TRUE);
    	}
    }
    
    public void deshabilitacampos() {

        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);
        getTextNombreOriginal().setDisabled(Boolean.TRUE);
        getTextApellidoPaternoOriginal().setDisabled(Boolean.TRUE);
        getTextApellidoMaternoOriginal().setDisabled(Boolean.TRUE);
        getTextClaveOriginal().setDisabled(Boolean.TRUE);
        getTextDescripcionOriginal().setDisabled(Boolean.TRUE);
        getTextNss().setDisabled(Boolean.TRUE);
        getTextAgregadoMedico().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getTextNumero().setDisabled(Boolean.TRUE);
        getAutoDelegacion().setDisabled(Boolean.TRUE);
        getTextClave().setDisabled(Boolean.TRUE);
        getTextDescripcion().setDisabled(Boolean.TRUE);
    }

    public List<String> completarTipoEgreso(String cveFiltro) {

        return filtrarListasComplementarias(cveFiltro, 100, getListaUbicaciones());
    }

    public void desplazarANss() {

        try {
            modificacionDatosPacienteService.validarUbicacion(getTipoUbicacionActual());
            //setTipoUbicacionActual(getUbicacion());
            //getButtonCancelar().setDisabled(Boolean.FALSE);
            requeridos[0] = Boolean.TRUE;
            habilitarGuardar();
            setTieneFoco(getTextNssOriginal().getClientId());
        } catch (HospitalizacionException e) {
        	requeridos[0] = Boolean.FALSE;
            agregarMensajeError(e);
            habilitarGuardar();
            setTieneFoco(getSelectUbicaciones().getClientId());
            
        }

    }
    
    public void validarFechaModificacion() {

        try {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
        	modificacionDatosPacienteService.validarFechaModificacion(getDatosPacienteNuevos().getFechaModificacion(),
        			obtenerDatosUsuario().getCvePresupuestal());
        	fechaModificacion = getDatosPacienteNuevos().getFechaModificacion();
            getButtonCancelar().setDisabled(Boolean.FALSE);
            setTieneFoco(getSelectUbicaciones().getClientId());
            requeridos[8] = Boolean.TRUE;
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosPacienteNuevos().setFechaModificacion(BaseConstants.CADENA_VACIA);
            requeridos[8] = Boolean.FALSE;
            habilitarGuardar();
            setTieneFoco(getMaskFechaModificacion().getClientId());
        }
    }

    public void validaNssOriginal() {

        try {
            modificacionDatosPacienteService.validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
            getAutoDelegacion().getValue();
            if (TipoUbicacionEnum.HOSPITALIZACION.getClave().equals(getTipoUbicacionActual())) {
                setUbicacion(TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave().toString());
            } else if (TipoUbicacionEnum.URGENCIAS.getClave().equals(getTipoUbicacionActual())) {
                setUbicacion(TipoFormatoEnum.INGRESO_URGENCIAS.getClave().toString());
            } else if (TipoUbicacionEnum.UCI.getClave().equals(getTipoUbicacionActual())) {
                setUbicacion(TipoFormatoEnum.INGRESO_UCI.getClave().toString());
            }
            setNumeroPaciente(null);
            getButtonCancelar().setDisabled(Boolean.FALSE);
            requeridos[1] = Boolean.TRUE;
            habilitarGuardar();
            setBanderaNss(Boolean.TRUE);
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
            setListaDatosPaciente(new ArrayList<DatosPaciente>());
            setNumeroPaciente(null);
            getTextNss().setDisabled(Boolean.TRUE);
            requeridos[1] = Boolean.FALSE;
            setBanderaNss(Boolean.FALSE);
            habilitarGuardar();
            setTieneFoco(getTextNssOriginal().getClientId());
        }
    }

    @Override
    public void obtenerPaciente() {
    	try {
    		String fechaModificacionAux = getDatosHospitalizacion().getDatosPaciente().getFechaModificacion();
	        if (getBanderaNss()) {
	            super.obtenerPaciente();
	            if (!getListaDatosPaciente().isEmpty() && getListaDatosPaciente() != null) {
	            	getDatosHospitalizacion().getDatosPaciente().setFechaModificacion(fechaModificacionAux);
					validarFechaModificacionMayor();
	                getTextNss().setDisabled(Boolean.FALSE);
	                setTieneFoco(getTextNss().getClientId());
	            } else {
	                agregarMensajeAdvertencia(getArray(MensajesErrorConstants.MG_OO4), null);
	                getDatosHospitalizacion().getDatosPaciente().setNss(BaseConstants.CADENA_VACIA);
	                selectUbicaciones.getValue();
	                getUbicacion();
	                getUbicaciones();
	            }
	        }
    	}catch (HospitalizacionException e) {
    		agregarMensajeError(e);
    		setDatosHospitalizacion(new DatosHospitalizacion());
    		setListaDatosPaciente(new ArrayList<DatosPaciente>());
    		setNumeroPaciente(null);
    		getSelectUbicaciones().resetValue();
    		cancelar();
		}
    	
    }
    
    public void validarFechaModificacionMayor() throws HospitalizacionException{
    	
		modificacionDatosPacienteService.validarFechaModificacionMayorIngreso(getDatosHospitalizacion(),getDatosPacienteNuevos().getFechaModificacion());
		modificacionDatosPacienteService.validarFechaModificacionMayorTococirugia(getDatosHospitalizacion(),getDatosPacienteNuevos().getFechaModificacion());
		modificacionDatosPacienteService.validarFechaModificacionMayorIntervenciones(getDatosHospitalizacion(),getDatosPacienteNuevos().getFechaModificacion());
		modificacionDatosPacienteService.validarFechaModificacionMayorMovIntra(getDatosHospitalizacion(),getDatosPacienteNuevos().getFechaModificacion());
   
    }

    public void avansaFocoNss() {

        if (getListaDatosPaciente().size() == BaseConstants.INDEX_UNO) {
            getTextNss().setDisabled(Boolean.FALSE);
            setTieneFoco(getTextNss().getClientId());
        }

    }

    @Override
    public boolean requiereBuscarPorNumeroCama() {

        return false;
    }

    @Override
    public void mapearDatosPaciente() {
    	try {
    		//String fechaModificacionAux = getDatosHospitalizacion().getDatosPaciente().getFechaModificacion();
	        super.mapearDatosPaciente();
	        if (getNumeroPaciente() != null) {
	        	getDatosPacienteNuevos().setFechaModificacion(fechaModificacion);
	        	//getDatosHospitalizacion().getDatosPaciente().setFechaModificacion(fechaModificacionAux);
	        	validarFechaModificacionMayor();
	            getTextNss().setDisabled(Boolean.FALSE);
	            setTieneFoco(getTextNss().getClientId());
	        }
	    }catch (HospitalizacionException e) {
			agregarMensajeError(e);
			setDatosHospitalizacion(new DatosHospitalizacion());
			setListaDatosPaciente(new ArrayList<DatosPaciente>());
			setNumeroPaciente(null);
			getSelectUbicaciones().resetValue();
			cancelar();
		}
    }

    public void validarNss() {

        try {
            modificacionDatosPacienteService.validarNss(getDatosPacienteNuevos().getNss());
            String nss = getDatosPacienteNuevos().getNss();
            setDatosPacienteNuevos(new DatosPaciente());
            getDatosPacienteNuevos().setNss(nss);
            getTextAgregadoMedico().setDisabled(Boolean.FALSE);
            requeridos[2] = Boolean.TRUE;
            habilitarGuardar();
            setTieneFoco(getTextAgregadoMedico().getClientId());
        } catch (HospitalizacionException e) {
        	requeridos[2] = Boolean.FALSE;
            getDatosPacienteNuevos().setNss("");
            agregarMensajeError(e);
            getTextAgregadoMedico().setDisabled(Boolean.TRUE);
            habilitarGuardar();
            setTieneFoco(getTextNss().getClientId());
        }
    }

    public void validarAgregadoRegimen() throws HospitalizacionException {

        try {
            getDatosPacienteNuevos().setAgregadoMedico(getDatosPacienteNuevos().getAgregadoMedico().toUpperCase());
            getHospitalizacionCommonServices().validarAgregadoMedico(getDatosPacienteNuevos().getAgregadoMedico());
            String nss = getDatosPacienteNuevos().getNss();
            String agregado = getDatosPacienteNuevos().getAgregadoMedico();
            setDatosPacienteNuevos(new DatosPaciente());
            getDatosPacienteNuevos().setAgregadoMedico(agregado);
            getDatosPacienteNuevos().setNss(nss);
            setPacientes(getHospitalizacionCommonServices().buscarPaciente(getDatosPacienteNuevos(),
                    new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(new Date())));

            if (getPacientes().size() < 2) {
                setDatosPacienteNuevos(getPacientes().get(0));

                if (getDatosPacienteNuevos().getNombre() == null || getDatosPacienteNuevos().getNombre().isEmpty()) {
                    habilitaCamposNuevos();
                    setTieneFoco(getTextNombre().getClientId());
                    requeridos[3] = Boolean.TRUE;
                } else {
                    
                    setDelegacion(getDatosHospitalizacion().getDatosPaciente().getDelegacion());
                    List<DatosCatalogo> delegaciones = catalogosHospitalizacionServices
                            .obtenerCatalogoDelegacion(getDatosHospitalizacion().getDatosPaciente().getNumero());
                    for (DatosCatalogo del : delegaciones) {
                        if (del.getClave().equals(getDelegacion())) {
                            getDatosHospitalizacion().getDatosPaciente()
                                    .setDelegacion(del.getClave() + BaseConstants.ESPACIO + del.getDescripcion());
                        }
                    }
                    deshabilitaCamposNuevos();
                    getDatosPacienteNuevos().setColorIdentificador(IdentificadorConexionEnum.VERDE.getRuta());
                    requeridos[3] = Boolean.TRUE;
                    requeridos[4] = Boolean.TRUE;
                    requeridos[5] = Boolean.TRUE;
                    requeridos[6] = Boolean.TRUE;
                    requeridos[7] = Boolean.TRUE;
                    habilitarGuardar();
                    setTieneFoco(getButtonGuardar().getClientId());
                }

            } else {
            	requeridos[3] = Boolean.TRUE;
                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PACIENTES_NUEVOS);
                habilitarGuardar();
                setTieneFoco(getTextNumeroPacienteNuevo().getClientId());
            }

        } catch (HospitalizacionException e) {
        	 requeridos[3] = Boolean.FALSE;
             requeridos[4] = Boolean.FALSE;
             requeridos[5] = Boolean.FALSE;
             requeridos[6] = Boolean.FALSE;
             requeridos[7] = Boolean.FALSE;
            agregarMensajeError(e);
            getDatosPacienteNuevos().setAgregadoMedico("");
            habilitarGuardar();
            setTieneFoco(getTextAgregadoMedico().getClientId());
            getDatosPacienteNuevos().setColorIdentificador(IdentificadorConexionEnum.ROJO.getRuta());
        }
    }

    public void desplasarFocoGuardar() {

        getButtonGuardar().setDisabled(Boolean.FALSE);
        setTieneFoco(getButtonGuardar().getClientId());
    }

    public void habilitaCamposNuevos() {

        getTextNombre().setDisabled(Boolean.FALSE);
        getTextApellidoPaterno().setDisabled(Boolean.FALSE);
        getTextApellidoMaterno().setDisabled(Boolean.FALSE);
        getTextNombre().setDisabled(Boolean.FALSE);
        getTextApellidoPaterno().setDisabled(Boolean.FALSE);
        getTextApellidoMaterno().setDisabled(Boolean.FALSE);
        getTextNumero().setDisabled(Boolean.FALSE);
        getAutoDelegacion().setDisabled(Boolean.FALSE);
    }

    public void deshabilitaCamposNuevos() {

        getDatosPacienteNuevos().setColorIdentificador(IdentificadorConexionEnum.ROJO.getRuta());
        getTextNss().setDisabled(Boolean.TRUE);
        getTextAgregadoMedico().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getTextNumero().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getTextNumero().setDisabled(Boolean.TRUE);
        getAutoDelegacion().setDisabled(Boolean.TRUE);
        getAutoEspecialidad().setDisabled(Boolean.TRUE);
    }

    public void mapearDatosPacienteNuevos() {

        if (getNumeroPacienteNuevo() == null || getNumeroPacienteNuevo() > getPacientes().size()) {
            setNumeroPacienteNuevo(null);
            setTieneFoco(getTextNumeroPacienteNuevo().getClientId());
        } else if (getNumeroPacienteNuevo() == 0) {
            habilitaCamposNuevos();
            setTieneFoco(getTextNombre().getClientId());
            RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES_NUEVOS);
        } else {
            deshabilitaCamposNuevos();
            getTextNss().setDisabled(Boolean.FALSE);
            getTextAgregadoMedico().setDisabled(Boolean.FALSE);
            setDatosPacienteNuevos(getPacientes().get(getNumeroPacienteNuevo() - 1));
            getButtonGuardar().setDisabled(Boolean.FALSE);
            setTieneFoco(getButtonGuardar().getClientId());
            RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES_NUEVOS);
        }
    }

    public void mostrarPacienteSeleccionado() {

        setDatosPacienteNuevos(getPacientes().get(getNumeroPaciente() - 1));
    }

    public void activarFocoNombre() {

        try {
            getDatosPacienteNuevos().setNombre(getDatosPacienteNuevos().getNombre().toUpperCase());
            if (getDatosPacienteNuevos().getNombre() == null || getDatosPacienteNuevos().getNombre().isEmpty()) {
            	requeridos[4] = Boolean.FALSE; 
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                        getArray(BaseConstants.REQUERIDO_NOMBRE));
            }
            requeridos[4] = Boolean.TRUE;
            habilitarGuardar();
            setTieneFoco(getTextApellidoPaterno().getClientId());
        } catch (HospitalizacionException e) {
        	requeridos[4] = Boolean.FALSE; 
        	habilitarGuardar();
        	agregarMensajeError(e);        	
            setTieneFoco(getTextNombre().getClientId());
        }
    }

    public void activarFocoApellidoPaterno() {

        try {
            getDatosPacienteNuevos().setApellidoPaterno(getDatosPacienteNuevos().getApellidoPaterno().toUpperCase());
            if (getDatosPacienteNuevos().getApellidoPaterno() == null
                    || getDatosPacienteNuevos().getApellidoPaterno().isEmpty()) {
            	requeridos[5] = Boolean.FALSE;
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                        getArray(BaseConstants.REQUERIDO_APELLIDO_PATERNO));
            }
            requeridos[5] = Boolean.TRUE;
            habilitarGuardar();
            setTieneFoco(getTextApellidoMaterno().getClientId());
        } catch (HospitalizacionException e) {
        	requeridos[5] = Boolean.FALSE;
        	habilitarGuardar();
            agregarMensajeError(e);
            setTieneFoco(getTextApellidoPaterno().getClientId());
        }
    }

    public void activarFocoApellidoMaterno() {

        getDatosPacienteNuevos().setApellidoMaterno(getDatosPacienteNuevos().getApellidoMaterno().toUpperCase());
        setTieneFoco(getTextNumero().getClientId());

    }

    public void validarNumero() {

        try {
            modificacionDatosPacienteService.validarNumero(getDatosPacienteNuevos().getNumero());
            setCatalogoDelegacion(
                    catalogosHospitalizacionServices.obtenerCatalogoDelegacion(getDatosPacienteNuevos().getNumero()));
            setCatalogoDelegacionString(convertirCatalogoString(getCatalogoDelegacion()));
            requeridos[6] = Boolean.TRUE;
            habilitarGuardar();
            setTieneFoco(getAutoDelegacion().getClientId());
        } catch (HospitalizacionException e) {
        	requeridos[6] = Boolean.FALSE;
        	habilitarGuardar();
            agregarMensajeError(e);
            getDatosPacienteNuevos().setNumero(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextNumero().getClientId());
        }

    }

    public void validarDelegacion() {

        try {
            DatosCatalogo datosCatalogo = null;
            datosCatalogo = validaAutoComplete(getAutoDelegacion(), 2, getCatalogoDelegacionString(),
                    getCatalogoDelegacion());
            if (null != datosCatalogo) {
                String cvDelegacion = getDatosPacienteNuevos().getDelegacion().substring(0, 2);
                modificacionDatosPacienteService.validarDelegacion(getDatosPacienteNuevos().getNumero(), cvDelegacion,
                        getCatalogoDelegacion());
                if (!getDatosPacienteNuevos().getAgregadoMedico().substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())) {
                    UnidadMedica unidadMedica = modificacionDatosPacienteService
                            .buscarUnidadMedica(getDatosPacienteNuevos().getNumero(), cvDelegacion);
                    getDatosPacienteNuevos().setClavePresupuestal(unidadMedica.getCvePresupuestal());
                    getDatosPacienteNuevos().setDescripcionUnidad(unidadMedica.getDesUnidadMedica());
                }
                requeridos[7] = Boolean.TRUE;
                habilitarGuardar();
                setTieneFoco(getButtonGuardar().getClientId());
            } else {
            	requeridos[7] = Boolean.FALSE;
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                        getArray(BaseConstants.REQUERIDO_DELEGACION));
            }
        } catch (HospitalizacionException e) {
            if (!datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                    .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                    .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())) {
                getDatosPacienteNuevos().setClavePresupuestal(BaseConstants.CADENA_VACIA);
                getDatosPacienteNuevos().setDescripcionUnidad(BaseConstants.CADENA_VACIA);
            }
            requeridos[7] = Boolean.FALSE;
            habilitarGuardar();
            agregarMensajeError(e);
            setTieneFoco(getAutoDelegacion().getClientId());
        }
    }

    public void guardar(ActionEvent actionEvent) {
    	/*modificaciones mejoras hospitalziacion: se requiere que se vuelvan a ejecutar todas las validaciones antes de guardar el objeto, 
    	 * si hay algun valor incorrecto no permite almacenar el objeto y pone el foco en el campo con el error*/
    	try{
    		modificacionDatosPacienteService.validarFechaModificacion(getDatosPacienteNuevos().getFechaModificacion(),
        			obtenerDatosUsuario().getCvePresupuestal());
    		validarFechaModificacionMayor();
	    	try{
	    		modificacionDatosPacienteService.validarUbicacion(getUbicacion());
	    		try{
	    			//validar el nss original
	    			modificacionDatosPacienteService.validarNss(getDatosHospitalizacion().getDatosPaciente().getNss()); 
	    			try{
	    				//validar el nss nuevo
	    				 modificacionDatosPacienteService.validarNss(getDatosPacienteNuevos().getNss());
	    				 try{
	    					 getHospitalizacionCommonServices().validarAgregadoMedico(getDatosPacienteNuevos().getAgregadoMedico());
	    					 if(validarDatosComplementariosGuardar()){
	    						 try {
	                                 if (null == getDatosPacienteNuevos().getCvePaciente()) {
	                                     getDatosPacienteNuevos().setCvePaciente(getDatosHospitalizacion().getDatosPaciente().getCvePaciente());
	                                 }
	                                 modificacionDatosPacienteService.guardarModificacionDatosPacienteService(getDatosPacienteNuevos(),
	                                         getDatosHospitalizacion().getDatosPaciente().getNss(),
	                                         getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
	                                 agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
	                                 cancelar();
	                             } catch (HospitalizacionException e4) {
	                                 agregarMensajeError(e4);
	                             }
	    					 }
	    				 }catch(HospitalizacionException e3){
	    					 requeridos[3] = Boolean.FALSE;
	    					 habilitarGuardar();
	    					 agregarMensajeError(e3);
	    			         getDatosPacienteNuevos().setAgregadoMedico("");
	    			         setTieneFoco(getTextAgregadoMedico().getClientId());
	    			         getDatosPacienteNuevos().setColorIdentificador(IdentificadorConexionEnum.ROJO.getRuta());
	    			         
	    				 }
	    			}catch(HospitalizacionException e2){
	    				requeridos[2] = Boolean.FALSE;
	    				habilitarGuardar();
	    				 getDatosPacienteNuevos().setNss("");
	    		         agregarMensajeError(e2);
	    		         getTextAgregadoMedico().setDisabled(Boolean.TRUE);
	    		         setTieneFoco(getTextNss().getClientId());
	    		         
	    			}
	    		}catch(HospitalizacionException e1){
	    				requeridos[1] = Boolean.FALSE;
	    				habilitarGuardar();
	    			  	agregarMensajeError(e1);
	    	            getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
	    	            setListaDatosPaciente(new ArrayList<DatosPaciente>());
	    	            setNumeroPaciente(null);
	    	            getTextNss().setDisabled(Boolean.TRUE);
	    	            setBanderaNss(Boolean.FALSE);
	    	            setTieneFoco(getTextNssOriginal().getClientId());
	    	            
	    		}
	    	}catch(HospitalizacionException e){
	    		requeridos[0] = Boolean.FALSE;
	    		habilitarGuardar();
	    		agregarMensajeError(e);
	            setTieneFoco(getSelectUbicaciones().getClientId());
	            
	    	}
	    }catch(HospitalizacionException e5){
	    	agregarMensajeError(e5);
            getDatosHospitalizacion().getDatosPaciente().setFechaModificacion(BaseConstants.CADENA_VACIA);
            requeridos[8] = Boolean.FALSE;
            habilitarGuardar();
            setTieneFoco(getMaskFechaModificacion().getClientId());
	        
		}
    	

    }
    private boolean validarDatosComplementariosGuardar(){
    	//validando nombre, apellido y delegacion
    	boolean validado = Boolean.TRUE;
    	try{
    		  if (getDatosPacienteNuevos().getNombre() == null || getDatosPacienteNuevos().getNombre().isEmpty()) {
                  throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                          getArray(BaseConstants.REQUERIDO_NOMBRE));
              }else{
            	  try{
            		  if (getDatosPacienteNuevos().getApellidoPaterno() == null
                              || getDatosPacienteNuevos().getApellidoPaterno().isEmpty()) {
                          throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                                  getArray(BaseConstants.REQUERIDO_APELLIDO_PATERNO));
                      }else{
                    	  try{
                    		  modificacionDatosPacienteService.validarNumero(getDatosPacienteNuevos().getNumero());
                    		  try{
                    			  if(!getAutoDelegacion().isDisabled()){
	                        		  DatosCatalogo datosCatalogo = null;
	                                  datosCatalogo = validaAutoComplete(getAutoDelegacion(), 2, getCatalogoDelegacionString(),
	                                          getCatalogoDelegacion());
	                                  if(datosCatalogo != null){
	                                	  String cvDelegacion = getDatosPacienteNuevos().getDelegacion().substring(0, 2);
	                                      modificacionDatosPacienteService.validarDelegacion(getDatosPacienteNuevos().getNumero(), cvDelegacion,
	                                              getCatalogoDelegacion());
	                                  }else{
	                                	  throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
	                                              getArray(BaseConstants.REQUERIDO_DELEGACION));
	                                  }
                    			  }
                        	  }catch(HospitalizacionException e4){
                        		  requeridos[7] = Boolean.FALSE;
                        		  habilitarGuardar();
                        		  validado = Boolean.FALSE;
                        		  if (!datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                                          .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                                          .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())) {
                                      getDatosPacienteNuevos().setClavePresupuestal(BaseConstants.CADENA_VACIA);
                                      getDatosPacienteNuevos().setDescripcionUnidad(BaseConstants.CADENA_VACIA);
                                  }
                        		  agregarMensajeError(e4);
                                  setTieneFoco(getAutoDelegacion().getClientId());
                                  
                        	  }
                    	  }catch(HospitalizacionException e3){
                    		  requeridos[6] = Boolean.FALSE;
                    		  habilitarGuardar();
                    		  validado = Boolean.FALSE;
                              agregarMensajeError(e3);
                              getDatosPacienteNuevos().setNumero(BaseConstants.CADENA_VACIA);
                              setTieneFoco(getTextNumero().getClientId());
                    	  }
                    	  
                      }
            	  }catch(HospitalizacionException e2){
            		  requeridos[5] = Boolean.FALSE;
            		  habilitarGuardar();
            		  validado = Boolean.FALSE;
            		  agregarMensajeError(e2);
                      setTieneFoco(getTextApellidoPaterno().getClientId());
                      
            	  }
              }
    	}catch(HospitalizacionException e1){
    		  requeridos[4] = Boolean.FALSE;
    		  habilitarGuardar();
    		  validado = Boolean.FALSE;
    		  agregarMensajeError(e1);
              setTieneFoco(getTextNombre().getClientId());
              
    	}
    	return validado;
    }

    public void cancelar() {

        List<DatosCatalogo> delegaciones = new ArrayList();
        setCatalogoDelegacion(delegaciones);
        setDelegacion(BaseConstants.CADENA_VACIA);
        getDatosPacienteNuevos().setDelegacion(BaseConstants.CADENA_VACIA);
        setUbicacion(BaseConstants.CADENA_VACIA);
        setTipoUbicacionActual(BaseConstants.CADENA_VACIA);
        setNumeroPaciente(null);
        setNumeroPacienteNuevo(null);
        setDatosPacienteNuevos(new DatosPaciente());
        setDatosHospitalizacion(new DatosHospitalizacion());
        getAutoDelegacion().resetValue();
        //setAutoDelegacion(new AutoComplete());
        deshabilitacampos();
        setTieneFoco(getMaskFechaModificacion().getClientId());
    }

    public void salir() throws IOException {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }

}
