package mx.gob.imss.simo.hospitalizacion.interconsultas.rules;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;

@Component
public class CapturaInterconsultasRules extends HospitalizacionCommonRules{
	
    public void validarEdad(String edad) throws HospitalizacionException {

        if (edad == null || edad.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_EDAD));
        }
        if (Integer.valueOf(edad) < 0 || Integer.valueOf(edad) > 103) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_138);
        }
    }
    
    public boolean habilitarEdad(String fechaIngreso, String anio) {

        int anioAgregado = Integer.parseInt(anio);
        int anioIngreso = Integer.parseInt(fechaIngreso.substring(6));
        if (anioAgregado == anioIngreso || (anioAgregado == (anioIngreso - 1))) {
            return true;
        }
        return false;
    }
    
    public boolean esPeriodoExtemporaneo(PeriodosImss periodoActivo, PeriodosImss periodoAnterior, Date fechaCaptura)
            throws HospitalizacionException {

        boolean extemporaneo = false;
        if (fechaCaptura.before(periodoActivo.getFechaInicial())) {
            if (fechaCaptura.before(periodoAnterior.getFechaInicial())) {
                throw new HospitalizacionException(MensajesErrorConstants.ME_009);
            } else {
                extemporaneo = true;
            }
        }
        return extemporaneo;
    }

    public List<DatosCatalogo> obtenerCatalogoEspecialidad(TipoUbicacionEnum tipoUbicacion,
            List<DatosCatalogo> especialidades) {

        List<DatosCatalogo> catalogoEspecialidad = new ArrayList<DatosCatalogo>();
        switch (tipoUbicacion) {
            case HOSPITALIZACION:
                catalogoEspecialidad.addAll(especialidades);
                break;
            case URGENCIAS:
                for (DatosCatalogo catalogo : especialidades) {
                    if (catalogo.getClave().equals(CapturarIngresosConstants.ESPECIALIDAD_URGENCIAS)) {
                        catalogoEspecialidad.add(catalogo);
                    }
                }
                break;
            case UCI:
                for (DatosCatalogo catalogo : especialidades) {
                    if (catalogo.getClave().equals(CapturarIngresosConstants.ESPECIALIDAD_UCI)) {
                        catalogoEspecialidad.add(catalogo);
                    }
                }
                break;
            case RECIEN_NACIDOS:
                for (DatosCatalogo catalogo : especialidades) {
                    if (catalogo.getClave().equals(CapturarIngresosConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)
                            || (catalogo.getClave().equals(CapturarIngresosConstants.ESPECIALIDAD_CUNERO_PATOLOGICO)
                                    || (catalogo.getClave()
                                            .equals(CapturarIngresosConstants.ESPECIALIDAD_UCI_NEONATOS)))) {
                        catalogoEspecialidad.add(catalogo);
                    }
                }
                break;
            default:
                break;
        }
        return catalogoEspecialidad;
    }
    

}
