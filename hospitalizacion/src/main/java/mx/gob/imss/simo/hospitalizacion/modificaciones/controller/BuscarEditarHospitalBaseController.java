package mx.gob.imss.simo.hospitalizacion.modificaciones.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.confirmdialog.ConfirmDialog;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.AtencionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MovimientosIntrahospitalariosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoDiagnostioEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoEgresoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.hospitalizacion.modificaciones.service.BuscarEditarHospitalServices;

@Data
@ViewScoped
@ManagedBean(name = "buscarEditarHospitalBaseController")
public class BuscarEditarHospitalBaseController extends HospitalizacionCommonController {

    private static final long serialVersionUID = -4887949625426146179L;

    @ManagedProperty("#{buscarEditarHospitalServices}")
    private BuscarEditarHospitalServices buscarEditarHospitalServices;

    @ManagedProperty("#{catalogosHospitalizacionServices}")
    private CatalogosHospitalizacionServices catalogosHospitalizacionServices;

    private InputText textFechaIngreso;
    private InputText textFechaIntQx;
    private InputText textProcedimiento;
    private InputText textProcedimientoDescrip;

    private InputText textFechaAtencionToco;
    private InputText textTotalRn;
    private InputText textFechaEgreso;
    private InputText textInicialIngresoDiag;
    private InputText textInicialIngresoDiagDesc;
    private InputText textPrincipalEgresoDiag;
    private InputText textPrincipalEgresoDiagDesc;
    private InputText textPrimerSecundarioEgresoDiag;
    private InputText textPrimerSecundarioEgresoDiagDesc;
    private InputText textSegundoSecundarioEgresoDiag;
    private InputText textSegundoSecundarioEgresoDiagDesc;
    private InputText textPrimerCompliIntraDiag;
    private InputText textPrimerCompliIntraDiagDesc;
    private InputText textSeguCompliIntraDiag;
    private InputText textSeguCompliIntraDiagDesc;
    private InputText textCausaDirectaDefuncionDiag;
    private InputText textCausaDirectaDefuncionDiagDesc;
    private InputText textCausaBasicaDefuncionDiag;
    private InputText textCausaBasicaDefuncionDiagDesc;

    private InputText textFechaIntervQx;
    private InputText textNumIntervQx;
    private InputText textNumEstanciaRn;

    protected Boolean[] requeridos = { Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE,
            Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE,
            Boolean.FALSE, Boolean.FALSE };

    /**
     * Inputs para tabla intervenciones
     */
    private InputText textoIntervencionNumero2;
    private InputText textoIntervencionNumero3;
    private InputText textoIntervencionNumero4;
    private InputText textoIntervencionNumero5;
    private InputText textoIntervencionNumero6;
    private InputText textoIntervencionNumero7;
    private InputText textoIntervencionNumero8;
    private InputText textoIntervencionNumero9;
    private InputText textoIntervencionNumero10;

    private InputText textoIntervencionFecha2;
    private InputText textoIntervencionFecha3;
    private InputText textoIntervencionFecha4;
    private InputText textoIntervencionFecha5;
    private InputText textoIntervencionFecha6;
    private InputText textoIntervencionFecha7;
    private InputText textoIntervencionFecha8;
    private InputText textoIntervencionFecha9;
    private InputText textoIntervencionFecha10;

    private InputText textoIntervencionProcedimiento2;
    private InputText textoIntervencionProcedimiento3;
    private InputText textoIntervencionProcedimiento4;
    private InputText textoIntervencionProcedimiento5;
    private InputText textoIntervencionProcedimiento6;
    private InputText textoIntervencionProcedimiento7;
    private InputText textoIntervencionProcedimiento8;
    private InputText textoIntervencionProcedimiento9;
    private InputText textoIntervencionProcedimiento10;

    private InputText textoIntervencionProcedimientoDesc2;
    private InputText textoIntervencionProcedimientoDesc3;
    private InputText textoIntervencionProcedimientoDesc4;
    private InputText textoIntervencionProcedimientoDesc5;
    private InputText textoIntervencionProcedimientoDesc6;
    private InputText textoIntervencionProcedimientoDesc7;
    private InputText textoIntervencionProcedimientoDesc8;
    private InputText textoIntervencionProcedimientoDesc9;
    private InputText textoIntervencionProcedimientoDesc10;

    private Procedimientos procedimiento1;
    private Procedimientos procedimiento2;
    private Procedimientos procedimiento3;
    private Procedimientos procedimiento4;
    private Procedimientos procedimiento5;
    private Procedimientos procedimiento6;
    private Procedimientos procedimiento7;
    private Procedimientos procedimiento8;
    private Procedimientos procedimiento9;
    private Procedimientos procedimiento10;

    private int cantidadProcedimientos;
    private String procedimientoValidar;

    private int totalIntervenciones;
    private int contadorIntervenciones;

    /**
     * fin tabla intervenciones
     */

    private SelectOneMenu selectTipoEgreso;
    private SelectOneMenu selectEspecialidadEgreso;
    private SelectOneMenu selectPlanificacionFamiliar;
    private SelectOneMenu selectMotivoAltaEgreso;
    private SelectOneMenu selectIngresoEspecialidad;
    private SelectOneMenu selectAtencionToco;
    private SelectOneMenu selectTipoPartoToco;
    private SelectOneMenu selectTipoIngreso;

    private InputMask maskNssOriginal;
    private List<DatosCatalogo> catalogoTipoIngresoHospitalizacion;
    private List<DatosCatalogo> catalogoAtencionPor;
    private List<DatosCatalogo> catalogoTipoParto;
    private List<DatosCatalogo> catalogoMotivoAltaEgreso;
    private List<DatosCatalogo> catalogoSexoHospitalizacion;
    private List<DatosCatalogo> catalogoTipoEgreso;
    private List<DatosCatalogo> catalogoMetodoPlanificacionFamiliar;

    private int totalRNTabla;
    private String valor;
    private int posicion;
    private DatosRecienNacido rn1;
    private DatosRecienNacido rn2;
    private DatosRecienNacido rn3;
    private DatosRecienNacido rn4;
    private DatosRecienNacido rn5;
    private DatosRecienNacido rn6;
    private DatosRecienNacido rn7;
    private DatosRecienNacido rn8;
    private DatosRecienNacido rn9;
    private InputText textNoRn1;
    private SelectOneMenu selectSexoRn1;
    private InputText textPesoRn1;
    private InputText textTallaRn1;
    private InputText textAltaRn1;
    private InputText textNoRn2;
    private SelectOneMenu selectSexoRn2;
    private InputText textPesoRn2;
    private InputText textTallaRn2;
    private InputText textAltaRn2;
    private InputText textNoRn3;
    private SelectOneMenu selectSexoRn3;
    private InputText textPesoRn3;
    private InputText textTallaRn3;
    private InputText textAltaRn3;
    private InputText textNoRn4;
    private SelectOneMenu selectSexoRn4;
    private InputText textPesoRn4;
    private InputText textTallaRn4;
    private InputText textAltaRn4;
    private InputText textNoRn5;
    private SelectOneMenu selectSexoRn5;
    private InputText textPesoRn5;
    private InputText textTallaRn5;
    private InputText textAltaRn5;
    private InputText textNoRn6;
    private SelectOneMenu selectSexoRn6;
    private InputText textPesoRn6;
    private InputText textTallaRn6;
    private InputText textAltaRn6;
    private InputText textNoRn7;
    private SelectOneMenu selectSexoRn7;
    private InputText textPesoRn7;
    private InputText textTallaRn7;
    private InputText textAltaRn7;
    private InputText textNoRn8;
    private SelectOneMenu selectSexoRn8;
    private InputText textPesoRn8;
    private InputText textTallaRn8;
    private InputText textAltaRn8;
    private InputText textNoRn9;
    private SelectOneMenu selectSexoRn9;
    private InputText textPesoRn9;
    private InputText textTallaRn9;
    private InputText textAltaRn9;

    private String focoTabla;

    private InputText textTallaRn;

    private InputText textPesoRn;

    private InputText textAltaRn;

    private InputText textNombreRn;

    private String tipoDiagnostico;
    private CommandButton confirmSi;
    private CommandButton confirmNo;
    private DatosCatalogo diagnosticoIni;
    private DatosCatalogo diagnosticoPri;
    private DatosCatalogo priDiagnosticoSec;
    private DatosCatalogo segDiagnosticoSec;
    private DatosCatalogo defuncionDir;
    private DatosCatalogo intrahospitalariaPri;
    private DatosCatalogo intrahospitalariaSec;
    private CommandButton confirmSiAccidente;
    private CommandButton confirmNoAccidente;
    private DatosCatalogo defuncionBas;
    protected ConfirmDialog alertaEpidemiologica;
    private DatosCatalogo complicacionIntraPrimera;
    private DatosCatalogo complicacionIntraSegunda;

    private void inicializarRequeridos() {

        requeridos[0] = Boolean.FALSE;
        requeridos[1] = Boolean.FALSE;
        requeridos[2] = Boolean.FALSE;
        requeridos[3] = Boolean.FALSE;
        requeridos[4] = Boolean.FALSE;
        requeridos[5] = Boolean.FALSE;
        requeridos[6] = Boolean.FALSE;
        requeridos[7] = Boolean.FALSE;
        requeridos[8] = Boolean.FALSE;
        requeridos[9] = Boolean.FALSE;
        requeridos[10] = Boolean.FALSE;
        requeridos[11] = Boolean.FALSE;
        requeridos[12] = Boolean.FALSE;
        requeridos[13] = Boolean.FALSE;

    }

    @Override
    public String obtenerNombrePagina() {

        // LoginController.setRenderIcons(false);
        return PagesCommonConstants.BUSQUEDA_EDICION_HOSPITAL;
    }

    public void salir() throws IOException {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }

    @Override
    @PostConstruct
    public void init() {

        super.init();
        crearElementos();
        crearElementosIntervenciones();
        crearElementosRn();
        deshabilitarControlesInicio();
        deshabilitarComponentesIntervenciones();
        deshabilitarRN();
        setTieneFoco(BaseConstants.FOCO_NSS);
        inicializarTablas();
    }

    private void crearElementos() {

        setButtonEditar(new CommandButton());
        setButtonSalir(new CommandButton());
        setButtonCancelar(new CommandButton());
        setTextAgregadoMedico(new InputText());
        setTextNombre(new InputText());
        textFechaIngreso = new InputText();
        textFechaIntQx = new InputText();
        textProcedimiento = new InputText();
        textProcedimientoDescrip = new InputText();
        textFechaIngreso = new InputText();
        textFechaAtencionToco = new InputText();
        textTotalRn = new InputText();
        textFechaEgreso = new InputText();
        textInicialIngresoDiag = new InputText();
        textInicialIngresoDiagDesc = new InputText();
        textPrincipalEgresoDiag = new InputText();
        textPrincipalEgresoDiagDesc = new InputText();
        textPrimerSecundarioEgresoDiag = new InputText();
        textPrimerSecundarioEgresoDiagDesc = new InputText();
        textSegundoSecundarioEgresoDiag = new InputText();
        textSegundoSecundarioEgresoDiagDesc = new InputText();
        textPrimerCompliIntraDiag = new InputText();
        textPrimerCompliIntraDiagDesc = new InputText();
        textSeguCompliIntraDiag = new InputText();
        textSeguCompliIntraDiagDesc = new InputText();
        textPrimerSecundarioEgresoDiag = new InputText();
        textCausaDirectaDefuncionDiag = new InputText();
        textCausaDirectaDefuncionDiagDesc = new InputText();
        textCausaBasicaDefuncionDiag = new InputText();
        textCausaBasicaDefuncionDiagDesc = new InputText();
        textFechaIntervQx = new InputText();
        textNumIntervQx = new InputText();
        textNumEstanciaRn = new InputText();
        selectTipoEgreso = new SelectOneMenu();
        selectEspecialidadEgreso = new SelectOneMenu();
        selectPlanificacionFamiliar = new SelectOneMenu();
        selectMotivoAltaEgreso = new SelectOneMenu();
        setSelectEspecialidadHospitalizacion(new SelectOneMenu());
        selectTipoIngreso = new SelectOneMenu();
        selectAtencionToco = new SelectOneMenu();
        selectTipoPartoToco = new SelectOneMenu();
        selectIngresoEspecialidad = new SelectOneMenu();
        catalogoTipoIngresoHospitalizacion = new ArrayList<>();
        catalogoAtencionPor = new ArrayList<>();
        catalogoTipoParto = new ArrayList<>();
        catalogoMotivoAltaEgreso = new ArrayList<>();
        catalogoSexoHospitalizacion = new ArrayList<>();
        maskNssOriginal = new InputMask();
        catalogoTipoEgreso = new ArrayList<DatosCatalogo>();
        catalogoMetodoPlanificacionFamiliar = new ArrayList<>();
        confirmSi = new CommandButton();
        confirmNo = new CommandButton();
        diagnosticoIni = new DatosCatalogo();
        diagnosticoPri = new DatosCatalogo();
        priDiagnosticoSec = new DatosCatalogo();
        segDiagnosticoSec = new DatosCatalogo();
        defuncionDir = new DatosCatalogo();
        defuncionBas = new DatosCatalogo();
        intrahospitalariaPri = new DatosCatalogo();
        intrahospitalariaSec = new DatosCatalogo();
        alertaEpidemiologica = new ConfirmDialog();

    }

    public void crearElementosRn() {

        setTextNoRn1(new InputText());
        setTextNoRn2(new InputText());
        setTextNoRn3(new InputText());
        setTextNoRn4(new InputText());
        setTextNoRn5(new InputText());
        setTextNoRn6(new InputText());
        setTextNoRn7(new InputText());
        setTextNoRn8(new InputText());
        setTextNoRn9(new InputText());
        setSelectSexoRn1(new SelectOneMenu());
        setSelectSexoRn2(new SelectOneMenu());
        setSelectSexoRn3(new SelectOneMenu());
        setSelectSexoRn4(new SelectOneMenu());
        setSelectSexoRn5(new SelectOneMenu());
        setSelectSexoRn6(new SelectOneMenu());
        setSelectSexoRn7(new SelectOneMenu());
        setSelectSexoRn8(new SelectOneMenu());
        setSelectSexoRn9(new SelectOneMenu());
        setTextPesoRn1(new InputText());
        setTextPesoRn2(new InputText());
        setTextPesoRn3(new InputText());
        setTextPesoRn4(new InputText());
        setTextPesoRn5(new InputText());
        setTextPesoRn6(new InputText());
        setTextPesoRn7(new InputText());
        setTextPesoRn8(new InputText());
        setTextPesoRn9(new InputText());
        setTextTallaRn1(new InputText());
        setTextTallaRn2(new InputText());
        setTextTallaRn3(new InputText());
        setTextTallaRn4(new InputText());
        setTextTallaRn5(new InputText());
        setTextTallaRn6(new InputText());
        setTextTallaRn7(new InputText());
        setTextTallaRn8(new InputText());
        setTextTallaRn9(new InputText());
        setTextAltaRn1(new InputText());
        setTextAltaRn2(new InputText());
        setTextAltaRn3(new InputText());
        setTextAltaRn4(new InputText());
        setTextAltaRn5(new InputText());
        setTextAltaRn6(new InputText());
        setTextAltaRn7(new InputText());
        setTextAltaRn8(new InputText());
        setTextAltaRn9(new InputText());

    }

    private void crearElementosIntervenciones() {

        textoIntervencionNumero2 = new InputText();
        textoIntervencionNumero3 = new InputText();
        textoIntervencionNumero4 = new InputText();
        textoIntervencionNumero5 = new InputText();
        textoIntervencionNumero6 = new InputText();
        textoIntervencionNumero7 = new InputText();
        textoIntervencionNumero8 = new InputText();
        textoIntervencionNumero9 = new InputText();
        textoIntervencionNumero10 = new InputText();

        textoIntervencionFecha2 = new InputText();
        textoIntervencionFecha3 = new InputText();
        textoIntervencionFecha4 = new InputText();
        textoIntervencionFecha5 = new InputText();
        textoIntervencionFecha6 = new InputText();
        textoIntervencionFecha7 = new InputText();
        textoIntervencionFecha8 = new InputText();
        textoIntervencionFecha9 = new InputText();
        textoIntervencionFecha10 = new InputText();

        textoIntervencionProcedimiento2 = new InputText();
        textoIntervencionProcedimiento3 = new InputText();
        textoIntervencionProcedimiento4 = new InputText();
        textoIntervencionProcedimiento5 = new InputText();
        textoIntervencionProcedimiento6 = new InputText();
        textoIntervencionProcedimiento7 = new InputText();
        textoIntervencionProcedimiento8 = new InputText();
        textoIntervencionProcedimiento9 = new InputText();
        textoIntervencionProcedimiento10 = new InputText();

        textoIntervencionProcedimientoDesc2 = new InputText();
        textoIntervencionProcedimientoDesc3 = new InputText();
        textoIntervencionProcedimientoDesc4 = new InputText();
        textoIntervencionProcedimientoDesc5 = new InputText();
        textoIntervencionProcedimientoDesc6 = new InputText();
        textoIntervencionProcedimientoDesc7 = new InputText();
        textoIntervencionProcedimientoDesc8 = new InputText();
        textoIntervencionProcedimientoDesc9 = new InputText();
        textoIntervencionProcedimientoDesc10 = new InputText();

        procedimiento1 = new Procedimientos();
        procedimiento2 = new Procedimientos();
        procedimiento3 = new Procedimientos();
        procedimiento4 = new Procedimientos();
        procedimiento5 = new Procedimientos();
        procedimiento6 = new Procedimientos();
        procedimiento7 = new Procedimientos();
        procedimiento8 = new Procedimientos();
        procedimiento9 = new Procedimientos();
        procedimiento10 = new Procedimientos();

    }

    private void deshabilitarControlesInicio() {

        getButtonEditar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);
        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonSalir().setDisabled(Boolean.FALSE);
        getSelectMotivoAltaEgreso().setDisabled(Boolean.TRUE);
        getSelectTipoIngreso().setDisabled(Boolean.TRUE);
        getSelectTipoPartoToco().setDisabled(Boolean.TRUE);
        getTextFechaIngreso().setDisabled(Boolean.TRUE);
        getTextFechaIntQx().setDisabled(Boolean.TRUE);
        getTextProcedimiento().setDisabled(Boolean.TRUE);
        getTextProcedimientoDescrip().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextTotalRn().setDisabled(Boolean.TRUE);
        getTextFechaEgreso().setDisabled(Boolean.TRUE);
        getTextInicialIngresoDiag().setDisabled(Boolean.TRUE);
        getTextInicialIngresoDiagDesc().setDisabled(Boolean.TRUE);
        getTextPrincipalEgresoDiag().setDisabled(Boolean.TRUE);
        getTextPrincipalEgresoDiagDesc().setDisabled(Boolean.TRUE);
        getTextPrimerSecundarioEgresoDiag().setDisabled(Boolean.TRUE);
        getTextPrimerSecundarioEgresoDiagDesc().setDisabled(Boolean.TRUE);
        getTextSegundoSecundarioEgresoDiag().setDisabled(Boolean.TRUE);
        getTextSegundoSecundarioEgresoDiagDesc().setDisabled(Boolean.TRUE);
        getTextPrimerCompliIntraDiag().setDisabled(Boolean.TRUE);
        getSelectEspecialidadEgreso().setDisabled(Boolean.TRUE);
        getTextPrimerCompliIntraDiagDesc().setDisabled(Boolean.TRUE);
        getTextSeguCompliIntraDiag().setDisabled(Boolean.TRUE);
        getTextSeguCompliIntraDiagDesc().setDisabled(Boolean.TRUE);
        getTextCausaDirectaDefuncionDiag().setDisabled(Boolean.TRUE);
        getTextCausaDirectaDefuncionDiagDesc().setDisabled(Boolean.TRUE);
        getTextCausaBasicaDefuncionDiag().setDisabled(Boolean.TRUE);
        getTextCausaBasicaDefuncionDiagDesc().setDisabled(Boolean.TRUE);
        getTextFechaIntervQx().setDisabled(Boolean.TRUE);
        getTextNumIntervQx().setDisabled(Boolean.TRUE);
        getTextNumEstanciaRn().setDisabled(Boolean.TRUE);
        getTextFechaAtencionToco().setDisabled(Boolean.TRUE);
        getSelectTipoEgreso().setDisabled(Boolean.TRUE);
        getSelectEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
        getSelectIngresoEspecialidad().setDisabled(Boolean.TRUE);
        getSelectAtencionToco().setDisabled(Boolean.TRUE);
        getSelectPlanificacionFamiliar().setDisabled(Boolean.TRUE);

    }

    private void deshabilitarComponentesIntervenciones() {

        getTextoIntervencionNumero2().setDisabled(Boolean.TRUE);
        getTextoIntervencionNumero3().setDisabled(Boolean.TRUE);
        getTextoIntervencionNumero4().setDisabled(Boolean.TRUE);
        getTextoIntervencionNumero5().setDisabled(Boolean.TRUE);
        getTextoIntervencionNumero6().setDisabled(Boolean.TRUE);
        getTextoIntervencionNumero7().setDisabled(Boolean.TRUE);
        getTextoIntervencionNumero8().setDisabled(Boolean.TRUE);
        getTextoIntervencionNumero9().setDisabled(Boolean.TRUE);
        getTextoIntervencionNumero10().setDisabled(Boolean.TRUE);

        getTextoIntervencionFecha2().setDisabled(Boolean.TRUE);
        getTextoIntervencionFecha3().setDisabled(Boolean.TRUE);
        getTextoIntervencionFecha4().setDisabled(Boolean.TRUE);
        getTextoIntervencionFecha5().setDisabled(Boolean.TRUE);
        getTextoIntervencionFecha6().setDisabled(Boolean.TRUE);
        getTextoIntervencionFecha7().setDisabled(Boolean.TRUE);
        getTextoIntervencionFecha8().setDisabled(Boolean.TRUE);
        getTextoIntervencionFecha9().setDisabled(Boolean.TRUE);
        getTextoIntervencionFecha10().setDisabled(Boolean.TRUE);

        getTextoIntervencionProcedimiento2().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimiento3().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimiento4().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimiento5().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimiento6().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimiento7().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimiento8().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimiento9().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimiento10().setDisabled(Boolean.TRUE);

        getTextoIntervencionProcedimientoDesc2().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimientoDesc3().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimientoDesc4().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimientoDesc5().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimientoDesc6().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimientoDesc7().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimientoDesc8().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimientoDesc9().setDisabled(Boolean.TRUE);
        getTextoIntervencionProcedimientoDesc10().setDisabled(Boolean.TRUE);

    }

    private void deshabilitarRN() {

        getTextNoRn1().setDisabled(Boolean.TRUE);
        getTextNoRn2().setDisabled(Boolean.TRUE);
        getTextNoRn3().setDisabled(Boolean.TRUE);
        getTextNoRn4().setDisabled(Boolean.TRUE);
        getTextNoRn5().setDisabled(Boolean.TRUE);
        getTextNoRn6().setDisabled(Boolean.TRUE);
        getTextNoRn7().setDisabled(Boolean.TRUE);
        getTextNoRn8().setDisabled(Boolean.TRUE);
        getTextNoRn9().setDisabled(Boolean.TRUE);
        getSelectSexoRn1().setDisabled(Boolean.TRUE);
        getSelectSexoRn2().setDisabled(Boolean.TRUE);
        getSelectSexoRn3().setDisabled(Boolean.TRUE);
        getSelectSexoRn4().setDisabled(Boolean.TRUE);
        getSelectSexoRn5().setDisabled(Boolean.TRUE);
        getSelectSexoRn6().setDisabled(Boolean.TRUE);
        getSelectSexoRn7().setDisabled(Boolean.TRUE);
        getSelectSexoRn8().setDisabled(Boolean.TRUE);
        getSelectSexoRn9().setDisabled(Boolean.TRUE);
        getTextPesoRn1().setDisabled(Boolean.TRUE);
        getTextPesoRn2().setDisabled(Boolean.TRUE);
        getTextPesoRn3().setDisabled(Boolean.TRUE);
        getTextPesoRn4().setDisabled(Boolean.TRUE);
        getTextPesoRn5().setDisabled(Boolean.TRUE);
        getTextPesoRn6().setDisabled(Boolean.TRUE);
        getTextPesoRn7().setDisabled(Boolean.TRUE);
        getTextPesoRn8().setDisabled(Boolean.TRUE);
        getTextPesoRn9().setDisabled(Boolean.TRUE);
        getTextTallaRn1().setDisabled(Boolean.TRUE);
        getTextTallaRn2().setDisabled(Boolean.TRUE);
        getTextTallaRn3().setDisabled(Boolean.TRUE);
        getTextTallaRn4().setDisabled(Boolean.TRUE);
        getTextTallaRn5().setDisabled(Boolean.TRUE);
        getTextTallaRn6().setDisabled(Boolean.TRUE);
        getTextTallaRn7().setDisabled(Boolean.TRUE);
        getTextTallaRn8().setDisabled(Boolean.TRUE);
        getTextTallaRn9().setDisabled(Boolean.TRUE);
        getTextAltaRn1().setDisabled(Boolean.TRUE);
        getTextAltaRn2().setDisabled(Boolean.TRUE);
        getTextAltaRn3().setDisabled(Boolean.TRUE);
        getTextAltaRn4().setDisabled(Boolean.TRUE);
        getTextAltaRn5().setDisabled(Boolean.TRUE);
        getTextAltaRn6().setDisabled(Boolean.TRUE);
        getTextAltaRn7().setDisabled(Boolean.TRUE);
        getTextAltaRn8().setDisabled(Boolean.TRUE);
        getTextAltaRn9().setDisabled(Boolean.TRUE);
    }

    private void inicializarTablas() {

        procedimiento1 = null;
        procedimiento2 = null;
        procedimiento3 = null;
        procedimiento4 = null;
        procedimiento5 = null;
        procedimiento6 = null;
        procedimiento7 = null;
        procedimiento8 = null;
        procedimiento9 = null;
        procedimiento10 = null;

        rn1 = null;
        rn2 = null;
        rn3 = null;
        rn4 = null;
        rn5 = null;
        rn6 = null;
        rn7 = null;
        rn8 = null;
        rn9 = null;

    }

    private void llenarCatalogos() {

        setCatalogoEspecialidadHospitalizacion(catalogosHospitalizacionServices
                .obtenerCatalogoEspecialidad(getObjetosSs().getDatosUsuario().getCvePresupuestal()));
        setCatalogoSexoHospitalizacion(catalogosHospitalizacionServices.obtenerCatalogoSexo());
        setCatalogoTipoIngresoHospitalizacion(
                catalogosHospitalizacionServices.obtenerCatalogoTipoIngreso(TipoUbicacionEnum.HOSPITALIZACION));
        setCatalogoAtencionPor(catalogosHospitalizacionServices.obtenerCatalogoTipoAtencion());
        setCatalogoTipoParto(catalogosHospitalizacionServices.obtenerCatalogoTipoParto());
        setCatalogoMotivoAltaEgreso(catalogosHospitalizacionServices
                .obtenerCatalogoMotivoAlta(getDatosHospitalizacion().getDatosEgreso().getTipoEgreso(), true));
        setTipoFormato(TipoFormatoEnum.EGRESO_HOSPITALIZACION);
        setCatalogoTipoEgreso(catalogosHospitalizacionServices.obtenerCatalogoTipoEgreso(getTipoFormato()));
        setCatalogoMetodoPlanificacionFamiliar(catalogosHospitalizacionServices.obtenerCatalogoMetodoPlanificacion(
                getTipoFormato().getClave(), getDatosHospitalizacion().getDatosPaciente()));

    }

    private void limpiarControlesInicio() {

        getTextNss().resetValue();
        getTextAgregadoMedico().resetValue();
        getTextNombre().resetValue();
        getSelectMotivoAltaEgreso().resetValue();
        getSelectTipoIngreso().resetValue();
        getSelectTipoPartoToco().resetValue();
        getTextFechaIngreso().resetValue();
        getTextFechaIntQx().resetValue();
        getTextProcedimiento().resetValue();
        getTextProcedimientoDescrip().resetValue();
        getTextNombre().resetValue();
        getTextFechaAtencionToco().resetValue();
        getTextTotalRn().resetValue();
        getTextFechaEgreso().resetValue();
        getTextInicialIngresoDiag().resetValue();
        getTextInicialIngresoDiagDesc().resetValue();
        getTextPrincipalEgresoDiag().resetValue();
        getTextPrincipalEgresoDiagDesc().resetValue();
        getTextPrimerSecundarioEgresoDiag().resetValue();
        getTextPrimerSecundarioEgresoDiagDesc().resetValue();
        getTextSegundoSecundarioEgresoDiag().resetValue();
        getTextSegundoSecundarioEgresoDiagDesc().resetValue();
        getTextPrimerCompliIntraDiag().resetValue();
        getSelectEspecialidadEgreso().resetValue();
        getTextPrimerCompliIntraDiagDesc().resetValue();
        getTextSeguCompliIntraDiag().resetValue();
        getTextSeguCompliIntraDiagDesc().resetValue();
        getTextCausaDirectaDefuncionDiag().resetValue();
        getTextCausaDirectaDefuncionDiagDesc().resetValue();
        getTextCausaBasicaDefuncionDiag().resetValue();
        getTextCausaBasicaDefuncionDiagDesc().resetValue();
        getSelectTipoEgreso().resetValue();
        getSelectEspecialidadHospitalizacion().resetValue();
        getSelectIngresoEspecialidad().resetValue();
        getSelectAtencionToco().resetValue();
        getSelectPlanificacionFamiliar().resetValue();

    }

    public void validarNss() {

        try {
            buscarEditarHospitalServices.validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
            requeridos[0] = Boolean.TRUE;
            setTieneFoco(getTextAgregadoMedico().getClientId());
            getButtonCancelar().setDisabled(Boolean.FALSE);
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[0] = Boolean.FALSE;
            setBanderaNss(Boolean.FALSE);
            getDatosHospitalizacion().getDatosPaciente().setNss(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            getTextNss().resetValue();
            limpiarControlesInicio();
            setTieneFoco(getTextNss().getClientId());
            habilitarGuardar();

        }
    }

    public void validarAgregadoMedico() {

        try {
            getDatosHospitalizacion().getDatosPaciente()
                    .setAgregadoMedico(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico().toUpperCase());
            buscarEditarHospitalServices.validarAgregadoMedico(
                    getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico().toUpperCase());
            requeridos[1] = Boolean.TRUE;
            setBanderaNss(Boolean.TRUE);
            setTieneFoco(getButtonEditar().getClientId());
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            setBanderaNss(Boolean.FALSE);
            requeridos[1] = Boolean.FALSE;
            setTieneFoco(getTextAgregadoMedico().getClientId());
            getTextAgregadoMedico().resetValue();
            getTextAgregadoMedico().setValue(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            getButtonEditar().setDisabled(Boolean.TRUE);
            agregarMensajeError(e);
            habilitarGuardar();
        }
    }

    /*public void validarEdicion() {

        try {
            List<Boolean> secciones = buscarEditarHospitalServices.validarEdicion(getDatosHospitalizacion(),
                    obtenerDatosUsuario());
            setCantidadProcedimientos(getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size());
            activarSeccionesEdicion(secciones);
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            setTieneFoco(getTextAgregadoMedico().getClientId());
            habilitarGuardar();
        }
    }*/
    
    public void validarEdicion() throws HospitalizacionException {
        setCantidadProcedimientos(getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size());
		activarSeccionesEdicion();
		habilitarGuardar();
    }

    @Override
    public void obtenerPaciente() {

        setUbicacion(TipoFormatoEnum.BUSCAR_EDITAR_HOSPITAL.getClave().toString());
        super.obtenerPaciente();
        if (getBanderaNss()) {
            try {
                buscarEditarHospitalServices.validarExistenciaPaciente(getListaDatosPaciente());
                if (!getListaDatosPaciente().isEmpty() && getListaDatosPaciente().size() == BaseConstants.INDEX_UNO) {
                    setDatosHospitalizacion(buscarEditarHospitalServices
                            .obtenerDatosParaEdicion(getDatosHospitalizacion().getDatosPaciente()));
                    setCantidadProcedimientos(
                            getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size());
                    mapearRN();
                    mapearProcedimientosIntervenciones();
                    llenarCatalogos();
                    activarBotonEditar();
                }
                requeridos[0] = Boolean.TRUE;
                requeridos[1] = Boolean.TRUE;
                requeridos[2] = Boolean.TRUE;
                requeridos[3] = Boolean.TRUE;
                requeridos[4] = Boolean.TRUE;
                requeridos[5] = Boolean.TRUE;
                requeridos[6] = Boolean.TRUE;
                requeridos[7] = Boolean.TRUE;
                requeridos[8] = Boolean.TRUE;
                requeridos[9] = Boolean.TRUE;
                requeridos[10] = Boolean.TRUE;
                requeridos[11] = Boolean.TRUE;
                requeridos[12] = Boolean.TRUE;
                requeridos[13] = Boolean.TRUE;

            } catch (HospitalizacionException e) {
                agregarMensajeError(e);
                requeridos[0] = Boolean.FALSE;
                requeridos[1] = Boolean.FALSE;
                requeridos[2] = Boolean.FALSE;
                requeridos[3] = Boolean.FALSE;
                requeridos[4] = Boolean.FALSE;
                requeridos[5] = Boolean.FALSE;
                requeridos[6] = Boolean.FALSE;
                requeridos[7] = Boolean.FALSE;
                requeridos[8] = Boolean.FALSE;
                requeridos[9] = Boolean.FALSE;
                requeridos[10] = Boolean.FALSE;
                requeridos[11] = Boolean.FALSE;
                requeridos[12] = Boolean.FALSE;
                requeridos[13] = Boolean.FALSE;
                cancelar();
                getButtonCancelar().setDisabled(Boolean.TRUE);
                setTieneFoco(getTextNss().getClientId());

            }
        }
    }

    private void activarBotonEditar() {

        if (buscarEditarHospitalServices.puedeActivarEdicion(getDatosHospitalizacion(),
                obtenerDatosUsuario().getCvePresupuestal(), obtenerDatosUsuario().getPerfil())) {
            getButtonEditar().setDisabled(Boolean.FALSE);
            setTieneFoco(getButtonEditar().getClientId());
        } else {
            getButtonEditar().setDisabled(Boolean.TRUE);
        }
    }

    @Override
    public void mapearDatosPaciente() {

        super.mapearDatosPaciente();
        setDatosHospitalizacion(
                buscarEditarHospitalServices.obtenerDatosParaEdicion(getDatosHospitalizacion().getDatosPaciente()));
        setCantidadProcedimientos(getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size());
        mapearRN();
        mapearProcedimientosIntervenciones();
        llenarCatalogos();
        activarBotonEditar();
        setBanderaNss(Boolean.TRUE);

    }

    private void mapearRN() {

        setRn1(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 1));
        setRn2(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 2));
        setRn3(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 3));
        setRn4(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 4));
        setRn5(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 5));
        setRn6(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 6));
        setRn7(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 7));
        setRn8(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 8));
        setRn9(buscarEditarHospitalServices.armarRN(getDatosHospitalizacion().getDatosTococirugia(), 9));
        if (null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()) {
            setTotalRNTabla(getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size());
        } else {
            setTotalRNTabla(BaseConstants.INDEX_CERO);
        }
    }

    private void mapearProcedimientosIntervenciones() {

        int elementos = 0;
        if (null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                && !getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().isEmpty()) {
            List<Procedimientos> listaProcedimientos = getDatosHospitalizacion().getDatosIntervenciones()
                    .getProcedimientos();
            elementos = listaProcedimientos.size();
            setCantidadProcedimientos(elementos);

            for (int i = 0; i < elementos; i++) {
                switch (i) {
                    case 0:
                        setProcedimiento1(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 1:
                        setProcedimiento2(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 2:
                        setProcedimiento3(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 3:
                        setProcedimiento4(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 4:
                        setProcedimiento5(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 5:
                        setProcedimiento6(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 6:
                        setProcedimiento7(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 7:
                        setProcedimiento8(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 8:
                        setProcedimiento9(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;
                    case 9:
                        setProcedimiento10(buscarEditarHospitalServices.crearProcedimiento(listaProcedimientos, i));
                        break;

                }
            }

        }

    }

   
    public void activarSeccionesEdicion() {
            for (int i = 0; i <= getCantidadProcedimientos(); i++) {
                switch (i) {
                	case 0:
                		break;
                    case 1:
                        getTextProcedimiento().setDisabled(Boolean.FALSE);
                        break;
                    case 2:
                        getTextoIntervencionProcedimiento2().setDisabled(Boolean.FALSE);
                        break;
                    case 3:
                        getTextoIntervencionProcedimiento3().setDisabled(Boolean.FALSE);
                        break;
                    case 4:
                        getTextoIntervencionProcedimiento4().setDisabled(Boolean.FALSE);
                        break;
                    case 5:
                        getTextoIntervencionProcedimiento5().setDisabled(Boolean.FALSE);
                        break;
                    case 6:
                        getTextoIntervencionProcedimiento6().setDisabled(Boolean.FALSE);
                        break;
                    case 7:
                        getTextoIntervencionProcedimiento7().setDisabled(Boolean.FALSE);
                        break;
                    case 8:
                        getTextoIntervencionProcedimiento8().setDisabled(Boolean.FALSE);
                        break;
                    case 9:
                        getTextoIntervencionProcedimiento9().setDisabled(Boolean.FALSE);
                        break;
                    case 10:
                        getTextoIntervencionProcedimiento10().setDisabled(Boolean.FALSE);
                    default:
                        break;
                }
            }
      
        validarSeccionTococirugia();
        activarSeccionRN();
        validarSeccionEgreso();
        activarSeccionDX();
        validarSelectPlanificacionFamiliar();
        colocarFocoSecciones();
    }
    
    public void validarSeccionEgreso() {

    	   if (getDatosHospitalizacion().getDatosEgreso().getMotivoAlta() != null) {
    	            getSelectMotivoAltaEgreso().setDisabled(Boolean.FALSE);
    	     } else {
    	    	 getSelectMotivoAltaEgreso().setDisabled(Boolean.TRUE);
    	      }
    	    }

   public void validarSeccionTococirugia() {
            if (null != getDatosHospitalizacion().getDatosTococirugia()
                    && getDatosHospitalizacion().getDatosTococirugia().getTipoParto() == null) {
                getSelectTipoPartoToco().setDisabled(Boolean.TRUE);
            } else {
                getSelectTipoPartoToco().setDisabled(Boolean.FALSE);
            }   
    }
    
    public void validarSelectPlanificacionFamiliar() {

        if (buscarEditarHospitalServices.obtieneEdadAgregadoMedico(getDatosHospitalizacion().getDatosPaciente()
                .getAgregadoMedico()) < BaseConstants.EDAD_MINIMA_PLANIFICACION_FAMILIAR) {
            getSelectPlanificacionFamiliar().setDisabled(Boolean.TRUE);
        }
        getButtonGuardar().setDisabled(Boolean.FALSE);
    }

    private void colocarFocoSecciones() {

        if (null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                && !getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().isEmpty()) {
            RequestContext.getCurrentInstance().execute(BaseConstants.ACTIVAR_CAMPOS_EDICION_INTERVENCION);
        } else if (null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                && !getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().isEmpty()) {
            setTieneFoco(getSelectSexoRn1().getClientId());
        } else if (null != getDatosHospitalizacion().getDatosTococirugia().getAtencion() && !AtencionEnum.PARTO
                .getClave().equals(getDatosHospitalizacion().getDatosTococirugia().getAtencion())) {
            setTieneFoco(getSelectAtencionToco().getClientId());
        } else if (null != getDatosHospitalizacion().getDatosEgreso().getMotivoAlta()) {
            setTieneFoco(getSelectMotivoAltaEgreso().getClientId());
        } else {
            RequestContext.getCurrentInstance().execute(BaseConstants.ACTIVAR_BOTON_CANCELAR);
        }
    }

    private void activarSeccionDX() {
        if(getDatosHospitalizacion().getDatosEgreso().getTipoEgreso() != null || 
        		getDatosHospitalizacion().getDatosEgreso().getMotivoEgreso() != null){
	        getTextPrimerCompliIntraDiag().setDisabled(Boolean.FALSE);
	        getTextSeguCompliIntraDiag().setDisabled(Boolean.FALSE);
	        if (TipoEgresoEnum.CANCELACION.getValor().equals(getDatosHospitalizacion().getDatosEgreso().getTipoEgreso()) ||
	        		TipoEgresoEnum.CANCELACION.getValor().equals(getDatosHospitalizacion().getDatosEgreso().getMotivoEgreso())) {
	            getTextInicialIngresoDiag().setDisabled(Boolean.TRUE);
	            getTextPrincipalEgresoDiag().setDisabled(Boolean.TRUE);
	            getTextPrimerSecundarioEgresoDiag().setDisabled(Boolean.TRUE);
	            getTextSegundoSecundarioEgresoDiag().setDisabled(Boolean.TRUE);
	            getTextPrimerCompliIntraDiag().setDisabled(Boolean.TRUE);
	            getTextSeguCompliIntraDiag().setDisabled(Boolean.TRUE);
	            getTextCausaDirectaDefuncionDiag().setDisabled(Boolean.TRUE);
	            getTextCausaBasicaDefuncionDiag().setDisabled(Boolean.TRUE);
	            requeridos[6] = Boolean.TRUE;
	            requeridos[7] = Boolean.TRUE;
	            requeridos[8] = Boolean.TRUE;
	            requeridos[9] = Boolean.TRUE;
	            requeridos[10] = Boolean.TRUE;
	            requeridos[11] = Boolean.TRUE;
	            requeridos[12] = Boolean.TRUE;
	            requeridos[13] = Boolean.TRUE;
	        } else if (TipoEgresoEnum.DEFUNCION.getValor()
	                .equals(getDatosHospitalizacion().getDatosEgreso().getTipoEgreso())||
	                TipoEgresoEnum.DEFUNCION.getValor()
	                .equals(getDatosHospitalizacion().getDatosEgreso().getMotivoEgreso())) {
	            getTextCausaDirectaDefuncionDiag().setDisabled(Boolean.FALSE);
	            getTextCausaBasicaDefuncionDiag().setDisabled(Boolean.FALSE);
	            getTextInicialIngresoDiag().setDisabled(Boolean.FALSE);
	            //getTextPrincipalEgresoDiag().setDisabled(Boolean.FALSE);
	            getTextPrincipalEgresoDiag().setDisabled(Boolean.TRUE);
	            getTextPrimerSecundarioEgresoDiag().setDisabled(Boolean.TRUE);
	            getTextSegundoSecundarioEgresoDiag().setDisabled(Boolean.TRUE);
	            requeridos[6] = Boolean.TRUE;
	            requeridos[7] = Boolean.TRUE;
	            requeridos[8] = Boolean.TRUE;
	            requeridos[9] = Boolean.TRUE;
	            requeridos[10] = Boolean.TRUE;
	            requeridos[11] = Boolean.TRUE;
	            requeridos[12] = Boolean.TRUE;
	        } else {
	            getTextInicialIngresoDiag().setDisabled(Boolean.FALSE);
	            getTextPrincipalEgresoDiag().setDisabled(Boolean.FALSE);
	            getTextPrimerSecundarioEgresoDiag().setDisabled(Boolean.FALSE);
	            getTextSegundoSecundarioEgresoDiag().setDisabled(Boolean.FALSE);
	            getTextPrimerCompliIntraDiag().setDisabled(Boolean.FALSE);
	            getTextCausaDirectaDefuncionDiag().setDisabled(Boolean.TRUE);
	            getTextCausaBasicaDefuncionDiag().setDisabled(Boolean.TRUE);
	            requeridos[9] = Boolean.TRUE;
	            requeridos[10] = Boolean.TRUE;
	            requeridos[11] = Boolean.TRUE;
	            requeridos[12] = Boolean.TRUE;
	            requeridos[13] = Boolean.TRUE;
	        }
	        desactivarCamposVaciosOpcionales();
        }
    }

    private void desactivarCamposVaciosOpcionales() {

        if (TipoEgresoEnum.CANCELACION.getValor().equals(getDatosHospitalizacion().getDatosEgreso().getTipoEgreso()) 
        		|| TipoEgresoEnum.CANCELACION.getValor().equals(getDatosHospitalizacion().getDatosEgreso().getMotivoEgreso())) {
            getSelectMotivoAltaEgreso().setDisabled(Boolean.TRUE);
        } else if (TipoEgresoEnum.DEFUNCION.getValor()
                .equals(getDatosHospitalizacion().getDatosEgreso().getTipoEgreso())|| 
                TipoEgresoEnum.DEFUNCION.getValor().equals(getDatosHospitalizacion().getDatosEgreso().getMotivoEgreso())) {
            if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera())) {
                getTextPrimerCompliIntraDiag().setDisabled(Boolean.TRUE);
            }
            if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraSegunda())) {
                getTextSeguCompliIntraDiag().setDisabled(Boolean.TRUE);
            }
            if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta())) {
                getTextCausaDirectaDefuncionDiag().setDisabled(Boolean.TRUE);
            }if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveInicialIngresoEgreso())) {
        		getTextInicialIngresoDiag().setDisabled(Boolean.TRUE);     
            }
        	if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCvePrincipalEgreso())) {
        		getTextPrincipalEgresoDiag().setDisabled(Boolean.TRUE);
            }
        } else {
        	if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveInicialIngresoEgreso())) {
        		getTextInicialIngresoDiag().setDisabled(Boolean.TRUE);     
            }
        	if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCvePrincipalEgreso())) {
        		getTextPrincipalEgresoDiag().setDisabled(Boolean.TRUE);
            }
        	if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCvePrimerDiagnosticoSecundario())) {
        		getTextPrimerSecundarioEgresoDiag().setDisabled(Boolean.TRUE);
            }
            if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getClaveSegundoDiagnosticoSecundario())) {
                getTextSegundoSecundarioEgresoDiag().setDisabled(Boolean.TRUE);
            }
            if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera())) {
                getTextPrimerCompliIntraDiag().setDisabled(Boolean.TRUE);
            }
            if (!validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraSegunda())) {
                getTextSeguCompliIntraDiag().setDisabled(Boolean.TRUE);
            }
        }
    }

    protected boolean validarCampo(String cadena) {

        if (null == cadena || cadena.isEmpty()) {
            return false;
        }
        return true;
    }

    private void activarSeccionRN() {

        if (null != rn1 && null != rn1.getIdNacido()) {
            getSelectSexoRn1().setDisabled(Boolean.FALSE);
            getTextPesoRn1().setDisabled(Boolean.FALSE);
            getTextTallaRn1().setDisabled(Boolean.FALSE);
        }
        if (null != rn2 && null != rn2.getIdNacido()) {
            getSelectSexoRn2().setDisabled(Boolean.FALSE);
            getTextPesoRn2().setDisabled(Boolean.FALSE);
            getTextTallaRn2().setDisabled(Boolean.FALSE);
        }
        if (null != rn3 && null != rn3.getIdNacido()) {
            getSelectSexoRn3().setDisabled(Boolean.FALSE);
            getTextPesoRn3().setDisabled(Boolean.FALSE);
            getTextTallaRn3().setDisabled(Boolean.FALSE);
        }
        if (null != rn4 && null != rn4.getIdNacido()) {
            getSelectSexoRn4().setDisabled(Boolean.FALSE);
            getTextPesoRn4().setDisabled(Boolean.FALSE);
            getTextTallaRn4().setDisabled(Boolean.FALSE);
        }
        if (null != rn5 && null != rn5.getIdNacido()) {
            getSelectSexoRn5().setDisabled(Boolean.FALSE);
            getTextPesoRn5().setDisabled(Boolean.FALSE);
            getTextTallaRn5().setDisabled(Boolean.FALSE);
        }
        if (null != rn6 && null != rn6.getIdNacido()) {
            getSelectSexoRn6().setDisabled(Boolean.FALSE);
            getTextPesoRn6().setDisabled(Boolean.FALSE);
            getTextTallaRn6().setDisabled(Boolean.FALSE);
        }
        if (null != rn7 && null != rn7.getIdNacido()) {
            getSelectSexoRn7().setDisabled(Boolean.FALSE);
            getTextPesoRn7().setDisabled(Boolean.FALSE);
            getTextTallaRn7().setDisabled(Boolean.FALSE);
        }
        if (null != rn8 && null != rn8.getIdNacido()) {
            getSelectSexoRn8().setDisabled(Boolean.FALSE);
            getTextPesoRn8().setDisabled(Boolean.FALSE);
            getTextTallaRn8().setDisabled(Boolean.FALSE);
        }
        if (null != rn9 && null != rn9.getIdNacido()) {
            getSelectSexoRn9().setDisabled(Boolean.FALSE);
            getTextPesoRn9().setDisabled(Boolean.FALSE);
            getTextTallaRn9().setDisabled(Boolean.FALSE);
        }
    }

    public void habilitarGuardar() {

        if (buscarEditarHospitalServices.validarActivarGuardar(getRequeridos())) {
            getButtonGuardar().setDisabled(Boolean.FALSE);
        } else {
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    public void guardar() {
    	try {
    		habilitarGuardar();
        	buscarEditarHospitalServices.validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
        	 try {
        		  buscarEditarHospitalServices.validarAgregadoMedico(
                          getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico().toUpperCase());
        		  	try {
        	            //armarListaProcedimiento();
        	            armarListaRN();
        	            getBuscarEditarHospitalServices().guardarEdicionHospitalizacion(getDatosHospitalizacion(),
        	                    obtenerDatosUsuario());
        	            cancelar();
        	            agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);

        	        } catch (HospitalizacionException e) {
        	            agregarMensajeError(e);
        	            getButtonGuardar().setDisabled(Boolean.TRUE);
        	            habilitarGuardar();
        	        }
             } catch (HospitalizacionException e) {
                 setBanderaNss(Boolean.FALSE);
                 requeridos[1] = Boolean.FALSE;
                 setTieneFoco(getTextAgregadoMedico().getClientId());
                 getTextAgregadoMedico().resetValue();
                 getTextAgregadoMedico().setValue(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
                 getButtonEditar().setDisabled(Boolean.TRUE);
                 agregarMensajeError(e);
                 habilitarGuardar();
             }               
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[0] = Boolean.FALSE;
            setBanderaNss(Boolean.FALSE);
            getDatosHospitalizacion().getDatosPaciente().setNss(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            getTextNss().resetValue();
            limpiarControlesInicio();
            setTieneFoco(getTextNss().getClientId());
            habilitarGuardar();

        }
       
    }

    protected List<Procedimientos> armarListaProcedimiento() {

        List<Procedimientos> pr = new ArrayList<Procedimientos>();

        pr.add(null == procedimiento1 ? null : procedimiento1);
        pr.add(null == procedimiento2 ? null : procedimiento2);
        pr.add(null == procedimiento3 ? null : procedimiento3);
        pr.add(null == procedimiento4 ? null : procedimiento4);
        pr.add(null == procedimiento5 ? null : procedimiento5);
        pr.add(null == procedimiento6 ? null : procedimiento6);
        pr.add(null == procedimiento7 ? null : procedimiento7);
        pr.add(null == procedimiento8 ? null : procedimiento8);
        pr.add(null == procedimiento9 ? null : procedimiento9);
        pr.add(null == procedimiento10 ? null : procedimiento10);
        getDatosHospitalizacion().getDatosIntervenciones().setProcedimientos(pr);

        return getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos();
    }

    private void armarListaRN() {

        List<DatosRecienNacido> rn = new ArrayList<DatosRecienNacido>();
        rn.add(null == rn1 ? null : rn1);
        rn.add(null == rn2 ? null : rn2);
        rn.add(null == rn3 ? null : rn3);
        rn.add(null == rn4 ? null : rn4);
        rn.add(null == rn5 ? null : rn5);
        rn.add(null == rn6 ? null : rn6);
        rn.add(null == rn7 ? null : rn7);
        rn.add(null == rn8 ? null : rn8);
        rn.add(null == rn9 ? null : rn9);
        getDatosHospitalizacion().getDatosTococirugia().setListDatosRecienNacido(rn);
    }

    public void cancelar() {

        setDatosHospitalizacion(new DatosHospitalizacion());
        limpiarControlesInicio();
        limpiarControlesRN();
        deshabilitarControlesInicio();
        deshabilitarComponentesIntervenciones();
        deshabilitarRN();
        setTieneFoco(getTextNss().getClientId());
        inicializarTablas();
    }

    private void limpiarControlesRN() {

        getTextNoRn1().resetValue();
        getTextNoRn2().resetValue();
        getTextNoRn3().resetValue();
        getTextNoRn4().resetValue();
        getTextNoRn5().resetValue();
        getTextNoRn6().resetValue();
        getTextNoRn7().resetValue();
        getTextNoRn8().resetValue();
        getTextNoRn9().resetValue();
        getSelectSexoRn1().resetValue();
        getSelectSexoRn2().resetValue();
        getSelectSexoRn3().resetValue();
        getSelectSexoRn4().resetValue();
        getSelectSexoRn5().resetValue();
        getSelectSexoRn6().resetValue();
        getSelectSexoRn7().resetValue();
        getSelectSexoRn8().resetValue();
        getSelectSexoRn9().resetValue();
        getTextPesoRn1().resetValue();
        getTextPesoRn2().resetValue();
        getTextPesoRn3().resetValue();
        getTextPesoRn4().resetValue();
        getTextPesoRn5().resetValue();
        getTextPesoRn6().resetValue();
        getTextPesoRn7().resetValue();
        getTextPesoRn8().resetValue();
        getTextPesoRn9().resetValue();
        getTextTallaRn1().resetValue();
        getTextTallaRn2().resetValue();
        getTextTallaRn3().resetValue();
        getTextTallaRn4().resetValue();
        getTextTallaRn5().resetValue();
        getTextTallaRn6().resetValue();
        getTextTallaRn7().resetValue();
        getTextTallaRn8().resetValue();
        getTextTallaRn9().resetValue();
        getTextAltaRn1().resetValue();
        getTextAltaRn2().resetValue();
        getTextAltaRn3().resetValue();
        getTextAltaRn4().resetValue();
        getTextAltaRn5().resetValue();
        getTextAltaRn6().resetValue();
        getTextAltaRn7().resetValue();
        getTextAltaRn8().resetValue();
        getTextAltaRn9().resetValue();

    }

    /**
     * @param procedimientos
     * @param procedimientoUno
     * @return
     */
    protected int siguienteFoco(final int procedimiento) {

        if (null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                && !getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().isEmpty()) {
            switch (procedimiento) {
                case BaseConstants.PROCEDIMIENTO_UNO:
                    validarProcedimientoUno();
                case BaseConstants.PROCEDIMIENTO_DOS:
                    validarProcedimientoDos();
                case BaseConstants.PROCEDIMIENTO_TRES:
                    validarProcedimientoTres();
                case BaseConstants.PROCEDIMIENTO_CUATRO:
                    validarProcedimientoCuatro();
                case BaseConstants.PROCEDIMIENTO_CINCO:
                    validarProcedimientoCinco();
                case BaseConstants.PROCEDIMIENTO_SEIS:
                    validarProcedimientoSeis();
                case BaseConstants.PROCEDIMIENTO_SIETE:
                    validarProcedimientoSiete();
                case BaseConstants.PROCEDIMIENTO_OCHO:
                    validarProcedimientoOcho();
                case BaseConstants.PROCEDIMIENTO_NUEVE:
                    validarProcedimientoNueve();
                case BaseConstants.PROCEDIMIENTO_DIEZ:
                    validarProcedimientoDiez();
                default:
                    break;
            }
        }
        return BaseConstants.PROCEDIMIENTO_CERO;
    }

    public int validarProcedimientoUno() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_UNO
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_UNO)) {
            return BaseConstants.PROCEDIMIENTO_DOS;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoDos() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_DOS
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_DOS)) {
            return BaseConstants.PROCEDIMIENTO_TRES;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoTres() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_TRES
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_TRES)) {
            return BaseConstants.PROCEDIMIENTO_CUATRO;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoCuatro() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_CUATRO
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_CUATRO)) {
            return BaseConstants.PROCEDIMIENTO_CINCO;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoCinco() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_CINCO
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_CINCO)) {
            return BaseConstants.PROCEDIMIENTO_SEIS;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoSeis() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_SEIS
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_SEIS)) {
            return BaseConstants.PROCEDIMIENTO_SIETE;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoSiete() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_SIETE
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_SIETE)) {
            return BaseConstants.PROCEDIMIENTO_OCHO;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoOcho() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_OCHO
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_OCHO)) {
            return BaseConstants.PROCEDIMIENTO_NUEVE;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoNueve() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() > BaseConstants.PROCEDIMIENTO_NUEVE
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(BaseConstants.PROCEDIMIENTO_NUEVE)) {
            return BaseConstants.PROCEDIMIENTO_DIEZ;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    public int validarProcedimientoDiez() {

        if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                .size() == BaseConstants.PROCEDIMIENTO_DIEZ
                && null != getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos()
                        .get(getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size()
                                - BaseConstants.PROCEDIMIENTO_UNO)) {
            return BaseConstants.PROCEDIMIENTO_DIEZ;
        } else {
            return BaseConstants.PROCEDIMIENTO_CERO;
        }
    }

    protected Boolean validarProcedimientosRequeridos() {

        for (Procedimientos p : armarListaProcedimiento()) {
            if (null != p && (null == p.getClave() || p.getClave().isEmpty())) {
                return false;
            }
        }
        return true;
    }

    protected Boolean validarRecienNacidosRequeridos() {

        armarListaRN();
        for (DatosRecienNacido datosRecienNacido : getDatosHospitalizacion().getDatosTococirugia()
                .getListDatosRecienNacido()) {
            if (null != datosRecienNacido) {
                if (null == datosRecienNacido.getSexo() || null == datosRecienNacido.getPeso()
                        || datosRecienNacido.getPeso() == 0 || null == datosRecienNacido.getTalla()
                        || datosRecienNacido.getTalla() == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void confirmAcepto() {

        if (TipoDiagnostioEnum.DIAGNOSTICO_INICIAL_INGRESO.getClave().equals(getTipoDiagnostico())) {
            // si se usa este atributo se debe de cambiar el value de la vista a
            // los catalogos
            // ej:
            // value="#{egresoHospitalizacionController.diagnosticoIni.descripcion}"
            setDiagnosticoIni(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(buscarEditarHospitalServices
                    .buscarDiagnostico(getDatosHospitalizacion().getDatosEgreso().getCveInicialIngresoEgreso())));
            // este es el modelo usado en la vista
            getDatosHospitalizacion().getDatosEgreso().setInicialIngresoEgreso(buscarEditarHospitalServices
                    .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveInicialIngresoEgreso()));
            setTieneFoco(getTextPrincipalEgresoDiag().getClientId());
        } else if (TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave().equals(getTipoDiagnostico())) {
            setDiagnosticoPri(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(buscarEditarHospitalServices
                    .buscarDiagnostico(getDatosHospitalizacion().getDatosEgreso().getCvePrincipalEgreso())));
            getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(buscarEditarHospitalServices
                    .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCvePrincipalEgreso()));
            setTieneFoco(getTextPrimerSecundarioEgresoDiag().getClientId());
        } else if (TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave().equals(getTipoDiagnostico())) {
            setPriDiagnosticoSec(catalogosHospitalizacionServices
                    .obtenerCatalogoDiagnostico(buscarEditarHospitalServices.buscarDiagnostico(
                            getDatosHospitalizacion().getDatosEgreso().getCvePrimerDiagnosticoSecundario())));
            getDatosHospitalizacion().getDatosEgreso()
                    .setPrimerDiagnosticoSecundario(buscarEditarHospitalServices.obtenerDescripcionDX(
                            getDatosHospitalizacion().getDatosEgreso().getCvePrimerDiagnosticoSecundario()));
            if (!getTextSegundoSecundarioEgresoDiag().isDisabled()) {
                setTieneFoco(getTextSegundoSecundarioEgresoDiag().getClientId());
            } else {
                setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
            }
        } else if (TipoDiagnostioEnum.SEGUNDO_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave().equals(getTipoDiagnostico())) {
            setSegDiagnosticoSec(catalogosHospitalizacionServices
                    .obtenerCatalogoDiagnostico(buscarEditarHospitalServices.buscarDiagnostico(
                            getDatosHospitalizacion().getDatosEgreso().getClaveSegundoDiagnosticoSecundario())));
            getDatosHospitalizacion().getDatosEgreso()
                    .setSegundoDiagnosticoSecundario(buscarEditarHospitalServices.obtenerDescripcionDX(
                            getDatosHospitalizacion().getDatosEgreso().getClaveSegundoDiagnosticoSecundario()));
            setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
        } else if (TipoDiagnostioEnum.PRIMERA_COMPLICACION_INTRAHOSPITALARIA.getClave().equals(getTipoDiagnostico())) {
            setIntrahospitalariaPri(catalogosHospitalizacionServices
                    .obtenerCatalogoDiagnostico(buscarEditarHospitalServices.buscarDiagnostico(
                            getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera())));
            getDatosHospitalizacion().getDatosEgreso().setComplicacionIntraPrimera(buscarEditarHospitalServices
                    .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera()));
            setTieneFoco(getTextSeguCompliIntraDiag().getClientId());
        } else if (TipoDiagnostioEnum.SEGUNDA_COMPLICACION_INTRAHOSPITALARIA.getClave().equals(getTipoDiagnostico())) {
            setIntrahospitalariaSec(catalogosHospitalizacionServices
                    .obtenerCatalogoDiagnostico(buscarEditarHospitalServices.buscarDiagnostico(
                            getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraSegunda())));
            getDatosHospitalizacion().getDatosEgreso().setComplicacionIntraSegunda(buscarEditarHospitalServices
                    .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraSegunda()));
            if (!getTextCausaDirectaDefuncionDiag().isDisabled()) {
                setTieneFoco(getTextCausaDirectaDefuncionDiag().getClientId());
            } else {
                setTieneFoco(getButtonGuardar().getClientId());
            }
        } else if (TipoDiagnostioEnum.CAUSA_DIRECTA_DEFUNCION.getClave().equals(getTipoDiagnostico())) {
            setDefuncionDir(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(buscarEditarHospitalServices
                    .buscarDiagnostico(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta())));
            getDatosHospitalizacion().getDatosEgreso().setCausaDefDirecta(buscarEditarHospitalServices
                    .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta()));
            //Mejoras 2021
            if(getDatosHospitalizacion().getDatosEgreso().getTipoFormato() != TipoFormatoEnum.EGRESO_URGENCIAS.getClave()){
            	getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(getDatosHospitalizacion()
            			.getDatosEgreso().getCveCausaDefDirecta());
            	getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(getBuscarEditarHospitalServices()
            			.obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta()));
            }
            setTieneFoco(getTextCausaBasicaDefuncionDiag().getClientId());
        } else if (TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave().equals(getTipoDiagnostico())) {
            if (buscarEditarHospitalServices
                    .validarDiagnosticoAccidente(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica())) {
                setTipoDiagnostico(TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave());
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_ACCIDENTE);
                setTieneFoco(getConfirmSiAccidente().getClientId());
            } else {
                setDefuncionBas(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(buscarEditarHospitalServices
                        .buscarDiagnostico(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica())));
                getDatosHospitalizacion().getDatosEgreso().setCausaDefBasica(buscarEditarHospitalServices
                        .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica()));
                //Mejoras 2021
                if(getDatosHospitalizacion().getDatosEgreso().getTipoFormato() != TipoFormatoEnum.EGRESO_URGENCIAS.getClave() 
                		&&(getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta() == null || 
                		getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta().isEmpty())){
                	getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(getDatosHospitalizacion()
                			.getDatosEgreso().getCveCausaDefBasica());
                	getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(getBuscarEditarHospitalServices()
                			.obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica()));
         
                }
                setTieneFoco(getButtonGuardar().getClientId());
            }
        }
        RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.CERRAR_CONFIRM_DIAGNOSTICO);
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idSi");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idNo");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idButtonSi");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idButtonNo");
    }

    public void confirmRechazo() {

        if (TipoDiagnostioEnum.DIAGNOSTICO_INICIAL_INGRESO.getClave().equals(getTipoDiagnostico())) {
            diagnosticoIni = new DatosCatalogo();
            getDatosHospitalizacion().getDatosEgreso().setDiagnosticoInicial(CapturarEgresoConstants.CADENA_VACIA);
            // este es el modelo usado en la vista
            getDatosHospitalizacion().getDatosEgreso().setInicialIngresoEgreso(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setCveInicialIngresoEgreso(CapturarEgresoConstants.CADENA_VACIA);
            setTieneFoco(getTextInicialIngresoDiag().getClientId());
        } else if (TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave().equals(getTipoDiagnostico())) {
            setDiagnosticoPri(new DatosCatalogo());
            getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(CapturarEgresoConstants.CADENA_VACIA);
            setTieneFoco(getTextPrincipalEgresoDiag().getClientId());
        } else if (TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave().equals(getTipoDiagnostico())) {
            setPriDiagnosticoSec(new DatosCatalogo());
            getDatosHospitalizacion().getDatosEgreso()
                    .setPrimerDiagnosticoSecundario(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso()
                    .setCvePrimerDiagnosticoSecundario(CapturarEgresoConstants.CADENA_VACIA);
            setTieneFoco(getTextPrimerSecundarioEgresoDiag().getClientId());
        } else if (TipoDiagnostioEnum.SEGUNDO_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave().equals(getTipoDiagnostico())) {
            setSegDiagnosticoSec(new DatosCatalogo());
            getDatosHospitalizacion().getDatosEgreso()
                    .setSegundoDiagnosticoSecundario(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso()
                    .setClaveSegundoDiagnosticoSecundario(CapturarEgresoConstants.CADENA_VACIA);
            setTieneFoco(getTextSegundoSecundarioEgresoDiag().getClientId());
        } else if (TipoDiagnostioEnum.PRIMERA_COMPLICACION_INTRAHOSPITALARIA.getClave().equals(getTipoDiagnostico())) {
            setIntrahospitalariaPri(new DatosCatalogo());
            getDatosHospitalizacion().getDatosEgreso()
                    .setComplicacionIntraPrimera(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso()
                    .setCveComplicacionIntraPrimera(CapturarEgresoConstants.CADENA_VACIA);
            setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
        } else if (TipoDiagnostioEnum.SEGUNDA_COMPLICACION_INTRAHOSPITALARIA.getClave().equals(getTipoDiagnostico())) {
            setIntrahospitalariaSec(new DatosCatalogo());
            getDatosHospitalizacion().getDatosEgreso()
                    .setComplicacionIntraSegunda(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso()
                    .setCveComplicacionIntraSegunda(CapturarEgresoConstants.CADENA_VACIA);
            setTieneFoco(getTextSeguCompliIntraDiag().getClientId());
        } else if (TipoDiagnostioEnum.CAUSA_DIRECTA_DEFUNCION.getClave().equals(getTipoDiagnostico())) {
            setDefuncionDir(new DatosCatalogo());
            getDatosHospitalizacion().getDatosEgreso().setCausaDefDirecta(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setCveCausaDefDirecta(CapturarEgresoConstants.CADENA_VACIA);
            //Mejoras 2021
            if(getDatosHospitalizacion().getDatosEgreso().getTipoFormato() != TipoFormatoEnum.EGRESO_URGENCIAS.getClave()){
            	getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(CapturarEgresoConstants.CADENA_VACIA);
            	getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(CapturarEgresoConstants.CADENA_VACIA);
            }
            setTieneFoco(getTextCausaDirectaDefuncionDiag().getClientId());
        } else if (TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave().equals(getTipoDiagnostico())) {
            defuncionBas = new DatosCatalogo();
            getDatosHospitalizacion().getDatosEgreso().setCausaDefBasica(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setCveCausaDefBasica(CapturarEgresoConstants.CADENA_VACIA);
            //Mejoras 2021
            if(getDatosHospitalizacion().getDatosEgreso().getTipoFormato() != TipoFormatoEnum.EGRESO_URGENCIAS.getClave() 
            		&&(getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta() == null || 
            		getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta().isEmpty())){
            	getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(CapturarEgresoConstants.CADENA_VACIA);
            	getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(CapturarEgresoConstants.CADENA_VACIA);
            }
            setTieneFoco(getTextCausaBasicaDefuncionDiag().getClientId());
        }

        RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.CERRAR_CONFIRM_DIAGNOSTICO);
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idSi");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idNo");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idButtonSi");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idButtonNo");
    }

    public void confirmAceptoAccidente() {

        if (TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave().equals(getTipoDiagnostico())) {
            setDefuncionBas(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(buscarEditarHospitalServices
                    .buscarDiagnostico(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica())));
            getDatosHospitalizacion().getDatosEgreso().setCausaDefBasica(buscarEditarHospitalServices
                    .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica()));
            //Mejoras 2021
            if(getDatosHospitalizacion().getDatosEgreso().getTipoFormato() != TipoFormatoEnum.EGRESO_URGENCIAS.getClave() 
            		&&(getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta() == null || 
            		getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta().isEmpty())){
            	getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(getDatosHospitalizacion()
            			.getDatosEgreso().getCveCausaDefBasica());
            	getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(getBuscarEditarHospitalServices()
            			.obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica()));
     
            }
            
            setTieneFoco(getButtonGuardar().getClientId());
        } else if (TipoDiagnostioEnum.CAUSA_DIRECTA_DEFUNCION.getClave().equals(getTipoDiagnostico())) {
            defuncionBas = new DatosCatalogo();
            getDatosHospitalizacion().getDatosEgreso().setCausaDefBasica(CapturarEgresoConstants.CADENA_VACIA);
            setTieneFoco(getTextCausaBasicaDefuncionDiag().getClientId());
        }

        RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.CERRAR_CONFIRM_ACCIDENTE);
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idSi");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idNo");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idButtonSi");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idButtonNo");
    }

    public void confirmRechazoAccidente() {

        if (TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave().equals(getTipoDiagnostico())) {
            defuncionBas = new DatosCatalogo();
            getDatosHospitalizacion().getDatosEgreso().setCausaDefBasica(CapturarEgresoConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setCveCausaDefBasica(CapturarEgresoConstants.CADENA_VACIA);
          //Mejoras 2021
            if(getDatosHospitalizacion().getDatosEgreso().getTipoFormato() != TipoFormatoEnum.EGRESO_URGENCIAS.getClave() 
            		&&(getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta() == null || 
            		getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta().isEmpty())){
            	getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(CapturarEgresoConstants.CADENA_VACIA);
            	getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(CapturarEgresoConstants.CADENA_VACIA);
            }
            
            setTieneFoco(getTextCausaBasicaDefuncionDiag().getClientId());
        }
        RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.CERRAR_CONFIRM_ACCIDENTE);
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idSi");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idNo");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idButtonSi");
        RequestContext.getCurrentInstance().reset("datosBuscarEditarHospForm:idButtonNo");
    }

    public void validarTipoPartoToco() {

        try {

            getBuscarEditarHospitalServices().validarTipoPartoTopoCirugia(getDatosHospitalizacion());
            requeridos[4] = Boolean.TRUE;
            if(!getSelectMotivoAltaEgreso().isDisabled()){
            	setTieneFoco(getSelectMotivoAltaEgreso().getClientId());
            	}else{
            		setTieneFoco(getTextInicialIngresoDiag().getClientId());
            	}
            habilitarGuardar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[4] = Boolean.FALSE;
            getSelectTipoPartoToco().resetValue();
            setTieneFoco(getSelectTipoPartoToco().getClientId());
            habilitarGuardar();
        }
    }

    public void remoteAction() {

        String datosVista[] = null;
        Procedimientos procedimientoAux = new Procedimientos();

        if (null != getProcedimientoValidar()) {

            try {
                setCantidadProcedimientos(
                        getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size());
                datosVista = getProcedimientoValidar().split(":");
                String value = getProcedimientoValidar();
                if (datosVista.length > 4) {
                    procedimientoAux.setClave(datosVista[4].toUpperCase());
                }
                procedimientoAux.setDescripcion(
                        getBuscarEditarHospitalServices().obtenerDescripcionProcedimiento(procedimientoAux.getClave()));
                int posicionProcedimiento = Integer.parseInt(datosVista[2]);
                int cont = 0;
                for (Procedimientos procedimiento : getDatosHospitalizacion().getDatosIntervenciones()
                        .getProcedimientos()) {
                    if (posicionProcedimiento == cont) {
                        procedimiento.setClave(procedimientoAux.getClave());
                        procedimiento.setDescripcion(procedimientoAux.getDescripcion());
                    }
                    cont++;
                }
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(procedimientoAux,
                        getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos(),
                        getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                String foco = null;
                if (Integer.parseInt(datosVista[2]) < (getCantidadProcedimientos() - 1)) {
                    foco = obtenerFocoTabla(datosVista[2], getProcedimientoValidar(),
                            getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos(), false);
                }
                if (null != foco && foco.length() > 0) {
                    setFocoTabla(foco);
                } else {
                    // validar que exista

                    setTieneFoco(getSelectSexoRn1().getClientId());
                    setFocoTabla(BaseConstants.CADENA_VACIA);

                }
                getButtonGuardar().setDisabled(Boolean.FALSE);
                logger.info("regresa foco " + getTieneFoco());

            } catch (HospitalizacionException e) {
                agregarMensajeError(e);
                datosVista = getProcedimientoValidar().split(":");
                int posicionProcedimiento = Integer.parseInt(datosVista[2]);
                int cont = 0;
                for (Procedimientos procedimiento : getDatosHospitalizacion().getDatosIntervenciones()
                        .getProcedimientos()) {
                    if (posicionProcedimiento == cont) {
                        procedimiento.setClave(BaseConstants.CADENA_VACIA);
                        procedimiento.setDescripcion(BaseConstants.CADENA_VACIA);
                    }
                    cont++;
                }
                setFocoTabla(obtenerFocoTabla(datosVista[2], getProcedimientoValidar(),
                        getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos(), true));
                getButtonGuardar().setDisabled(Boolean.TRUE);
                
            }
           

        }

    }

    private String obtenerFocoTabla(String numeroProcedimiento, String procedimientoValidar2,
            List<Procedimientos> procedimientos, boolean error) {

        StringBuffer cadena = new StringBuffer();
        String datos[] = procedimientoValidar2.split(":");
        int siguienteProcedimiento = Integer.parseInt(numeroProcedimiento) + 1;
        String cadenaReturn = "";
        if (!error) {
            datos[2] = String.valueOf(siguienteProcedimiento);
            if (null != procedimientos.get(siguienteProcedimiento)) {
                for (int i = 0; i < datos.length - 1; i++) {
                    cadenaReturn = cadenaReturn.concat(datos[i]).concat(":");
                }
            }
            cadena.append(cadenaReturn);
            cadenaReturn = cadena.deleteCharAt(cadenaReturn.length() - 1).toString();
            logger.info("foco tabla: " + cadenaReturn);
        } else {
            for (int i = 0; i < datos.length -1; i++) {
                cadenaReturn = cadenaReturn.concat(datos[i]).concat(":");
            }
            cadena.append(cadenaReturn);
            cadenaReturn = cadena.deleteCharAt(cadenaReturn.length() - 1).toString();
            logger.info("foco tabla: " + cadenaReturn);
        }
        return cadenaReturn;
    }

    @Override
    public boolean requiereBuscarPorNumeroCama() {

        return false;
    }

    public void actualizarFocoTablaQx() {

        logger.info(getTieneFoco());
        logger.info(getFocoTabla());
        setTieneFoco(getFocoTabla());
    }

}
