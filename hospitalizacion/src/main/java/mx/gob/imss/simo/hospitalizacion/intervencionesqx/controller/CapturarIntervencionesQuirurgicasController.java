package mx.gob.imss.simo.hospitalizacion.intervencionesqx.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.IntervencionesQuirurgicasConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MovimientosIntrahospitalariosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TococirugiaConstants;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.hospitalizacion.egresos.repository.CapturarEgresoRepository;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.helper.CapturarIntervencionesQuirurgicasHelper;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.services.CapturarIntervencionesQuirurgicasServices;
import mx.gob.imss.simo.hospitalizacion.tococirugia.services.CapturarTococirugiaServices;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;

@Data
@ViewScoped
@ManagedBean(name = "capturarIntervencionesQuirurgicasController")
public class CapturarIntervencionesQuirurgicasController extends HospitalizacionCommonController {

    private String fecha;
    private InputText textNumeroQuirofano;
    private InputText textAgregadoMedico;
    private AutoComplete autoCompleteEspecialidad;
    private SelectOneMenu selectTipoIntervencion;
    private SelectOneMenu selectMetodoPlanificacion;
    private SelectOneMenu selectTipoAnestesia;
    private InputMask textHoraEntrada;
    private InputMask textHoraInicio;
    private InputMask textHoraTermino;
    private InputMask textHoraSalidaSala;
    private InputMask textFechaIntervencion;
    private InputText textNumeroProcedimientos;
    private List<String> listaEspecialidad;
    private InputText textClaveProcedimiento;
    private InputText textDescProcedimiento;
    private InputText textClaveCirujano;
    private InputText textNombreCirujano;
    private InputText textMatricula;
    private InputText textMedico;
    private CommandButton buttonSi;
    private CommandButton buttonNo;
    private SelectOneMenu selectDivision;
    private DatosCatalogo datosCatalogo;
    private List<DatosCatalogo> catalogoTipoIntervencion;
    private List<DatosCatalogo> catalogoMetodoPlanificacion;
    private List<DatosCatalogo> catalogoTipoAnestesia;
    private DataTable dataTableDatosProcedimientos;
    private Procedimientos procedimientoModal;
    private InputText textClaveProcedimientoModal;
    private InputText textDescProcedimientoModal;
    private InputText textClaveCirujanoModal;
    private InputText textNombreCirujanoModal;
    private transient CommandButton horasSiInicio;
    private transient CommandButton horasNoInicio;
    private transient CommandButton horasSiTermino;
    private transient CommandButton horasNoTermino;
    private transient CommandButton horasSiSalida;
    private transient CommandButton horasNoSalida;
    
    private boolean banderaProcedimientos = false;
    private Boolean metodoVisible;
    private Boolean[] requeridos = { Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE,
            Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE,
            Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE };

    private Integer contadorNoRep = 0;
    private String horaAValidar;
 
     
    @ManagedProperty("#{capturarIntervencionesQuirurgicasServices}")
    private CapturarIntervencionesQuirurgicasServices capturarIntervencionesQuirurgicasServices;
    @ManagedProperty("#{catalogosHospitalizacionServices}")
    protected CatalogosHospitalizacionServices catalogosHospitalizacionServices;
    @ManagedProperty("#{capturarEgresoRepository}")
    private CapturarEgresoRepository capturarEgresoRepository;
    @ManagedProperty("#{capturarTococirugiaServices}")
    private CapturarTococirugiaServices capturarTococirugiaService;
    private List<String> metodoFiltradoTocoAsociada; 
    @Override
    public String obtenerNombrePagina() {

        // LoginController.setRenderIcons(false);
        return PagesCommonConstants.INTERVENCIONES_QUIRURGICAS;
    }

    @Override
    @PostConstruct
    public void init() {

        super.init();
        crearElementos();
        llenarCatalogos();
        desactivarElementos();
        dataTableDatosProcedimientos = new DataTable();
        setTipoFormato(TipoFormatoEnum.INTERVECION_QUIRURGICA);
        setMetodoVisible(Boolean.TRUE);
        inicializarRequeridos();
        setTieneFoco(BaseConstants.FOCO_ID_FECHA);
    }

    private void inicializarRequeridos() {

        requeridos[0] = Boolean.FALSE;
        requeridos[1] = Boolean.FALSE;
        requeridos[2] = Boolean.FALSE;
        requeridos[3] = Boolean.FALSE;
        requeridos[4] = Boolean.FALSE;
        requeridos[5] = Boolean.FALSE;
        requeridos[6] = Boolean.FALSE;
        requeridos[7] = Boolean.FALSE;
        requeridos[8] = Boolean.FALSE;
        requeridos[9] = Boolean.FALSE;
        requeridos[10] = Boolean.FALSE;
        requeridos[11] = Boolean.FALSE;
        requeridos[12] = Boolean.FALSE;
        requeridos[13] = Boolean.FALSE;
        requeridos[14] = Boolean.FALSE;
        requeridos[15] = Boolean.FALSE;
        requeridos[16] = Boolean.FALSE;
    }

    private void crearElementos() {

        setMaskFecha(new InputMask());

        setTextNumeroQuirofano(new InputText());
        setTextNss(new InputText());
        setTextCama(new InputText());
        setTextAgregadoMedico(new InputText());
        setTextNombre(new InputText());
        setTextApellidoPaterno(new InputText());
        setTextApellidoMaterno(new InputText());
        setTextNumeroProcedimientos(new InputText());

        setTextHoraEntrada(new InputMask());
        setTextHoraInicio(new InputMask());
        setTextHoraTermino(new InputMask());
        setTextHoraSalidaSala(new InputMask());

        setTextClaveProcedimiento(new InputText());
        setTextDescProcedimiento(new InputText());
        setTextClaveCirujano(new InputText());
        setTextNombreCirujano(new InputText());
        setTextClaveProcedimientoModal(new InputText());
        setTextDescProcedimientoModal(new InputText());
        setTextClaveCirujanoModal(new InputText());
        setTextNombreCirujanoModal(new InputText());
        //setTextFechaIntervencion(new InputMask());
        setTextMatricula(new InputText());
        setTextMedico(new InputText());
        setAutoCompleteEspecialidad(new AutoComplete());
        setListaEspecialidad(new ArrayList<String>());
        setDatosCatalogo(new DatosCatalogo());
        selectTipoIntervencion = new SelectOneMenu();
        catalogoTipoIntervencion = new ArrayList<DatosCatalogo>();
        selectMetodoPlanificacion = new SelectOneMenu();
        selectTipoAnestesia = new SelectOneMenu();
        catalogoTipoAnestesia = new ArrayList<DatosCatalogo>();
        selectDivision = new SelectOneMenu();
        procedimientoModal = new Procedimientos();
        setButtonGuardar(new CommandButton());
        setButtonCancelar(new CommandButton());
        setButtonSalir(new CommandButton());
        setButtonSi(new CommandButton());
        setButtonNo(new CommandButton());
        setHorasSiInicio(new CommandButton());
        setHorasNoInicio(new CommandButton());
        setHorasSiTermino(new CommandButton());
        setHorasNoTermino(new CommandButton());
        setHorasSiSalida(new CommandButton());
        setHorasNoSalida(new CommandButton());
    }

    private void llenarCatalogos() {

        setCatalogoEspecialidad(catalogosHospitalizacionServices
                .obtenerCatalogoEspecialidad(obtenerDatosUsuario().getCvePresupuestal()));
        setListaEspecialidad(
                capturarIntervencionesQuirurgicasServices.crearListaEspecialidad(getCatalogoEspecialidad()));
        setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
        setCatalogoTipoIntervencion(catalogosHospitalizacionServices.obtenerTipoIntervencion());
        setCatalogoTipoAnestesia(catalogosHospitalizacionServices.obtenerTipoAnestesia());
    }

    private void desactivarElementos() {

        getTextNss().setDisabled(Boolean.FALSE);
        getTextAgregadoMedico().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getTextCama().setDisabled(Boolean.TRUE);
        getSelectDivision().setDisabled(Boolean.TRUE);
        getTextClaveProcedimiento().setDisabled(Boolean.TRUE);
        getTextDescProcedimiento().setDisabled(Boolean.TRUE);
        getTextClaveCirujano().setDisabled(Boolean.TRUE);
        getTextNombreCirujano().setDisabled(Boolean.TRUE);
        getTextMedico().setDisabled(Boolean.TRUE);
        getTextDescProcedimientoModal().setDisabled(Boolean.TRUE);
        getTextNombreCirujanoModal().setDisabled(Boolean.TRUE);
        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);

    }

    public List<String> completarEspecialidad(String cveFiltro) {

        return filtrarListasComplementarias(cveFiltro, CapturarEgresoConstants.NUMERO_MAXIMO_TIPO_EGRESO,
                getListaEspecialidad());
    }

    public void elementoSelecionado(SelectEvent evento) {

        setDatosCatalogo(capturarIntervencionesQuirurgicasServices.elementoSelecionado(evento, getListaEspecialidad(),
                getAutoCompleteEspecialidad(), getCatalogoEspecialidad()));

    }

    public void validarFecha() {

        try {
            capturarIntervencionesQuirurgicasServices.validarFecha(getFecha(),
                    obtenerDatosUsuario().getCvePresupuestal());
            capturarIntervencionesQuirurgicasServices
                    .validarCamasConDosPacientes(obtenerDatosUsuario().getCvePresupuestal(), getFecha());
            if(getDatosHospitalizacion().getDatosPaciente().getFechaIngreso() != null ){
		            capturarIntervencionesQuirurgicasServices.validarFechaIngreso(getDatosHospitalizacion().getDatosPaciente().getFechaIngreso(), getFecha());
		            capturarIntervencionesQuirurgicasServices.validaFechaIngresoIQX(getDatosHospitalizacion().getDatosPaciente(),
		            getFecha());
		            
            }
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setFechaIQX(capturarIntervencionesQuirurgicasServices.convertirFecha(getFecha()));
            setTieneFoco(getTextNumeroQuirofano().getClientId());
            requeridos[0] = Boolean.TRUE;
            getButtonCancelar().setDisabled(Boolean.FALSE);
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            setFecha(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            requeridos[0] = Boolean.FALSE;
            setTieneFoco(getMaskFecha().getClientId());
            //habilitarGuardar();
        }
    }

    public void validarNumeroQuirofano() {

        try {
            String quirofano = getDatosHospitalizacion().getDatosIntervenciones().getNumeroQuirofano().toUpperCase();
            getDatosHospitalizacion().getDatosIntervenciones().setNumeroQuirofano(quirofano);
            capturarIntervencionesQuirurgicasServices.validarNumeroQuirofano(getDatosHospitalizacion(),
                    obtenerDatosUsuario());
            requeridos[1] = Boolean.TRUE;
            if(getTextNss().isDisabled()){
            	setTieneFoco(getTextCama().getClientId());
            }else{
            setTieneFoco(getTextNss().getClientId());
            }
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setNumeroQuirofano(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            requeridos[1] = Boolean.FALSE;
            setTieneFoco(getTextNumeroQuirofano().getClientId());
            //habilitarGuardar();
        }
    }

    public void validarNss() {

        try {
            capturarIntervencionesQuirurgicasServices.validarNSS(getDatosHospitalizacion());
            setBanderaNss(Boolean.TRUE);
            requeridos[2] = Boolean.TRUE;
            //setTieneFoco(getAutoEspecialidad().getClientId());
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosPaciente().setNss(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            setBanderaNss(Boolean.FALSE);
            requeridos[2] = Boolean.FALSE;
            setTieneFoco(getTextNss().getClientId());
            //habilitarGuardar();
        }
    }

    public void llenarEspecialidadl() {

        setCatalogoEspecialidad(catalogosHospitalizacionServices
                .obtenerCatalogoEspecialidad(getDatosHospitalizacion().getDatosPaciente().getClavePresupuestal()));

        setTieneFoco(getAutoCompleteEspecialidad().getClientId());

    }

    public void validarEspecialidad() {

        DatosCatalogo datosCatalogo = null;
        datosCatalogo = validaAutoComplete(getAutoEspecialidad(), 4, getCatalogoEspecialidadString(),
                getCatalogoEspecialidad());
        if (null != datosCatalogo) {
            getDatosHospitalizacion().getDatosIntervenciones().setEspecialidad(datosCatalogo.getClave());
        } else {
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setEspecialidad(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
        }
        try {
            String division = capturarIntervencionesQuirurgicasServices.validarEspecialidad(getDatosHospitalizacion());
            getDatosHospitalizacion().getDatosIntervenciones().setDivision(division);
            requeridos[3] = Boolean.TRUE;
            setTieneFoco(getTextNumeroProcedimientos().getClientId());
            llenarDivision();
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getAutoEspecialidad().resetValue();
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setEspecialidad(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            setCatalogoDivision(new ArrayList<DatosCatalogo>());
            requeridos[3] = Boolean.FALSE;
            setTieneFoco(getAutoEspecialidad().getClientId());
            //habilitarGuardar();
        }
    }

    public void llenarDivision() {

        setCatalogoDivision(capturarIntervencionesQuirurgicasServices
                .obtenerCatalogoDivision(getDatosHospitalizacion().getDatosIntervenciones().getDivision()));

    }

    public void validarCama() {

        String cama = getDatosHospitalizacion().getDatosPaciente().getCama().toUpperCase();
        getDatosHospitalizacion().getDatosPaciente().setCama(cama);
        super.obtenerPacientePorNumeroCama();
        if (getListaDatosPaciente().size() == BaseConstants.INDEX_UNO) {
            try {
                capturarIntervencionesQuirurgicasServices
                        .validarFechaIngreso(getListaDatosPaciente().get(0).getFechaIngreso(), getFecha());
                capturarIntervencionesQuirurgicasServices.validaFechaIngresoIQX(getListaDatosPaciente().get(0),
                        getFecha());
                complementarCatalogos();
                setTieneFoco(getAutoEspecialidad().getClientId());
                requeridos[2] = Boolean.TRUE;
            } catch (HospitalizacionException e) {
                agregarMensajeError(e);
                setTieneFoco(getMaskFecha().getClientId());
                getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
                setFecha(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                getDatosHospitalizacion().getDatosIntervenciones()
                        .setNumeroQuirofano(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                requeridos[2] = Boolean.FALSE;
            }
        }
    }

    @Override
    public void mapearDatosPacienteNumeroCama() {

    	if(getFecha() != null && !getFecha().isEmpty()){
	        super.mapearDatosPacienteNumeroCama();
	        try {
	        	
		            capturarIntervencionesQuirurgicasServices
		                    .validarFechaIngreso(getDatosHospitalizacion().getDatosPaciente().getFechaIngreso(), getFecha());
		            capturarIntervencionesQuirurgicasServices.validaFechaIngresoIQX(getDatosHospitalizacion().getDatosPaciente(),
		                    getFecha());
		            complementarCatalogos();            
		            setNumeroCama(null);
		            setTieneFoco(getAutoEspecialidad().getClientId());
	        } catch (HospitalizacionException e) {
	            setNumeroCama(null);
	            agregarMensajeError(e);
	            getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
	            setFecha(IntervencionesQuirurgicasConstants.CADENA_VACIA);
	            getDatosHospitalizacion().getDatosIntervenciones()
	                    .setNumeroQuirofano(IntervencionesQuirurgicasConstants.CADENA_VACIA);
	            setTieneFoco(getMaskFecha().getClientId());
	        }   
    	}
    }

    @Override
    public void obtenerPaciente() {

        super.obtenerPaciente();

        if (getListaDatosPaciente().size() == BaseConstants.INDEX_UNO) {
            try {
                capturarIntervencionesQuirurgicasServices
                        .validarFechaIngreso(getListaDatosPaciente().get(0).getFechaIngreso(), getFecha());
                capturarIntervencionesQuirurgicasServices.validaFechaIngresoIQX(getListaDatosPaciente().get(0),
                        getFecha());
                complementarCatalogos();
                setTieneFoco(getAutoEspecialidad().getClientId());
               
            } catch (HospitalizacionException e) {
                agregarMensajeError(e);
                getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
                setFecha(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                getDatosHospitalizacion().getDatosIntervenciones()
                        .setNumeroQuirofano(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                setTieneFoco(getMaskFecha().getClientId());
            }
        }
    }

    @Override
    public void mapearDatosPaciente() {
    	if(getFecha() != null && !getFecha().isEmpty()){
	        super.mapearDatosPaciente();
	        try {
	            capturarIntervencionesQuirurgicasServices
	                    .validarFechaIngreso(getDatosHospitalizacion().getDatosPaciente().getFechaIngreso(), getFecha());
	            capturarIntervencionesQuirurgicasServices.validaFechaIngresoIQX(getDatosHospitalizacion().getDatosPaciente(),
	                    getFecha());
	            complementarCatalogos();
	            setNumeroPaciente(null);           
	            setTieneFoco(getAutoEspecialidad().getClientId());
	            
	        } catch (HospitalizacionException e) {
	            setNumeroPaciente(null);
	            agregarMensajeError(e);
	            getMaskFecha().resetValue();
	            getTextNumeroQuirofano().resetValue();
	            getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
	            setFecha(IntervencionesQuirurgicasConstants.CADENA_VACIA);
	            getDatosHospitalizacion().getDatosIntervenciones()
	                    .setNumeroQuirofano(IntervencionesQuirurgicasConstants.CADENA_VACIA);
	            setTieneFoco(getMaskFecha().getClientId());
	        }
        }
    }

    private void complementarCatalogos() {

        if (null != getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                && !IntervencionesQuirurgicasConstants.CADENA_VACIA
                        .equals(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico())) {
            setCatalogoPlanificacion(catalogosHospitalizacionServices.obtenerCatalogoMetodoPlanificacion(
                    getTipoFormato().getClave(), getDatosHospitalizacion().getDatosPaciente(),
                    getDatosHospitalizacion().getDatosIntervenciones().getFechaIQX()));
            if (getCatalogoPlanificacion() != null && !getCatalogoPlanificacion().isEmpty()) {
                setMetodoVisible(Boolean.TRUE);
                setListaPlanificacionFamiliar(capturarIntervencionesQuirurgicasServices
                        .crearListaPlanificacionFam(getCatalogoPlanificacion()));
                metodoFiltradoTocoAsociada = capturarIntervencionesQuirurgicasServices
                        .crearListaPlanificacionFam(getCatalogoPlanificacion());
                requeridos[13] = Boolean.FALSE;
                requeridos[14] = Boolean.FALSE;
            } else {
                setMetodoVisible(Boolean.FALSE);
                requeridos[13] = Boolean.TRUE;
                requeridos[14] = Boolean.TRUE;
            }
        }

    }
    


    public void validarHoraEntradaSala() {

        try {
            capturarIntervencionesQuirurgicasServices.validarHoraEntradaSala(getDatosHospitalizacion(), getFecha());
            getDatosHospitalizacion().getDatosIntervenciones().setFechaEntrada(capturarIntervencionesQuirurgicasServices
                    .convertirHora(getFecha(), getDatosHospitalizacion().getDatosIntervenciones().getHoraEntrada()));
            requeridos[7] = Boolean.TRUE;
            getDatosHospitalizacion().getDatosIntervenciones()
            .setHoraInicio(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            requeridos[8] = Boolean.FALSE;
            setTieneFoco(getTextHoraInicio().getClientId());
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setHoraEntrada(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            requeridos[7] = Boolean.FALSE;
            setTieneFoco(getTextHoraEntrada().getClientId());
           // habilitarGuardar();
        }
    }

    public void validarHoraInicio() {

        try {
        	//validamos que el campo no est� vac�o, si lo est� enviar� una excepcion
            capturarIntervencionesQuirurgicasServices.validarHoraInicio(getDatosHospitalizacion(), getFecha());
            //validamos si la especialidad de ingreso es cirugia ambulatoria, si lo es, se valida la hora, no se pueden guardar horas menores para esta especialdiad
            Ingreso ingreso = capturarIntervencionesQuirurgicasServices.buscarIngresoPorDatosPaciente(datosHospitalizacion);
            if(ingreso.getCveEspecialidadIngreso().equals(BaseConstants.ESPECIALIDAD_CIRUGIA_AMBULATORIA)){
            	 if(capturarIntervencionesQuirurgicasServices.validarHoraInicioMenorHoraEntrada(getDatosHospitalizacion(), getFecha())){
            		 // es hora menor lanza exepcion
            		 throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_142));
            	 }else{//sigue el proceso normal
            		 String fechaInicio = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaEntrada());
                	 getDatosHospitalizacion().getDatosIntervenciones().setFechaInicio(capturarIntervencionesQuirurgicasServices
                             .convertirHora(fechaInicio, getDatosHospitalizacion().getDatosIntervenciones().getHoraInicio()));
                     requeridos[8] = Boolean.TRUE;
                     getDatosHospitalizacion().getDatosIntervenciones()
                     .setHoraTermino(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                     requeridos[9] = Boolean.FALSE;
                     setTieneFoco(getTextHoraTermino().getClientId());
                     //habilitarGuardar();
            	 }
            }else{
            	//valida si la hora inicial es menor a la hora de entrada, si no lo es, continua el proceso normal
                if(capturarIntervencionesQuirurgicasServices.validarHoraInicioMenorHoraEntrada(getDatosHospitalizacion(), getFecha())){
                	//habilita modal de confirmacion  
                	setHoraAValidar(IntervencionesQuirurgicasConstants.HORA_INICIO);
                	RequestContext context = RequestContext.getCurrentInstance();
                	context.execute("PF('dlgConfirmacionInicioHoras').show();");
                	setTieneFoco(getHorasSiInicio().getClientId());
                }else{
                	 String fechaInicio = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaEntrada());
                	 getDatosHospitalizacion().getDatosIntervenciones().setFechaInicio(capturarIntervencionesQuirurgicasServices
                             .convertirHora(fechaInicio, getDatosHospitalizacion().getDatosIntervenciones().getHoraInicio()));
                     requeridos[8] = Boolean.TRUE;
                     getDatosHospitalizacion().getDatosIntervenciones()
                     .setHoraTermino(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                     requeridos[9] = Boolean.FALSE;
                     setTieneFoco(getTextHoraTermino().getClientId());
                     //habilitarGuardar();
                }
               
            }
           
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setHoraInicio(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            requeridos[8] = Boolean.FALSE;
            setTieneFoco(getTextHoraInicio().getClientId());
            //habilitarGuardar();
        }
    }
    
    public void aprobarHoraMenores(){
    	String foco = "";
    	RequestContext context = RequestContext.getCurrentInstance();
    	switch(getHoraAValidar()){
    		case IntervencionesQuirurgicasConstants.HORA_INICIO: 
    			 context.execute("PF('dlgConfirmacionInicioHoras').hide();");
    			 String fechaInicio = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaEntrada());
    			 getDatosHospitalizacion().getDatosIntervenciones().setFechaInicio(capturarIntervencionesQuirurgicasServices
    	                 .convertirHora(obtenerSiguienteDia(fechaInicio), getDatosHospitalizacion().getDatosIntervenciones().getHoraInicio()));
    	         requeridos[8] = Boolean.TRUE;
    	         getDatosHospitalizacion().getDatosIntervenciones()
                 .setHoraTermino(IntervencionesQuirurgicasConstants.CADENA_VACIA);
    	         requeridos[9] = Boolean.FALSE;
    	         foco = getTextHoraTermino().getClientId();
    	         
    	        
    			break;
    		case IntervencionesQuirurgicasConstants.HORA_TERMINO:
    			context.execute("PF('dlgConfirmacionTerminoHoras').hide();");
    			String fechaTermino = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaInicio());
    			 getDatosHospitalizacion().getDatosIntervenciones().setFechaTermino(capturarIntervencionesQuirurgicasServices
    	                    .convertirHora(obtenerSiguienteDia(fechaTermino), getDatosHospitalizacion().getDatosIntervenciones().getHoraTermino()));
    	            requeridos[9] = Boolean.TRUE;
    	            getDatosHospitalizacion().getDatosIntervenciones()
                    .setHoraSalida(IntervencionesQuirurgicasConstants.CADENA_VACIA);
    	            requeridos[10] = Boolean.FALSE;
    	            foco = getTextHoraSalidaSala().getClientId();
    	            
    			break;
    		case IntervencionesQuirurgicasConstants.HORA_SALIDA_SALA:
    			context.execute("PF('dlgConfirmacionSalidaHoras').hide();");
    			String fechaSalida = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaTermino());
    			 getDatosHospitalizacion().getDatosIntervenciones().setFechaSalida(capturarIntervencionesQuirurgicasServices
    	                    .convertirHora(obtenerSiguienteDia(fechaSalida), getDatosHospitalizacion().getDatosIntervenciones().getHoraSalida()));
    	         requeridos[10] = Boolean.TRUE;
    	         foco = getSelectTipoIntervencion().getClientId();
    	         	        
    			break;
    			 
    	}
    	setTieneFoco(foco);
    	//habilitarGuardar();
    }
    
    private String obtenerSiguienteDia(String fechaBase){
    	Date fechaOriginal = capturarIntervencionesQuirurgicasServices.convertirFecha(fechaBase);
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(fechaOriginal);
    	calendar.add(Calendar.DATE, 1);
    	String fechaNueva = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(calendar.getTime());
    	return fechaNueva;
    }
    
    public void rechazarHoraMenores(){
    	String foco="";
    	RequestContext context = RequestContext.getCurrentInstance();
    	switch(getHoraAValidar()){
    		case IntervencionesQuirurgicasConstants.HORA_INICIO:
    			context.execute("PF('dlgConfirmacionInicioHoras').hide();");
    			getDatosHospitalizacion().getDatosIntervenciones()
    	        .setHoraInicio(IntervencionesQuirurgicasConstants.CADENA_VACIA);
    			getDatosHospitalizacion().getDatosIntervenciones()
                .setHoraTermino(IntervencionesQuirurgicasConstants.CADENA_VACIA);
    	    	requeridos[8] = Boolean.FALSE;
    	    	requeridos[9] = Boolean.FALSE;
    	        foco = getTextHoraInicio().getClientId();    	    
    			break;
    		case IntervencionesQuirurgicasConstants.HORA_TERMINO:
    			context.execute("PF('dlgConfirmacionTerminoHoras').hide();");
    			getDatosHospitalizacion().getDatosIntervenciones()
                .setHoraTermino(IntervencionesQuirurgicasConstants.CADENA_VACIA);
    			  getDatosHospitalizacion().getDatosIntervenciones()
                  .setHoraSalida(IntervencionesQuirurgicasConstants.CADENA_VACIA);
    			requeridos[9] = Boolean.FALSE;
    			requeridos[10] = Boolean.FALSE;
    			foco = getTextHoraTermino().getClientId();    			
    			break;
    		case IntervencionesQuirurgicasConstants.HORA_SALIDA_SALA:
    			context.execute("PF('dlgConfirmacionSalidaHoras').hide();");
    			 getDatosHospitalizacion().getDatosIntervenciones()
                 .setHoraSalida(IntervencionesQuirurgicasConstants.CADENA_VACIA);        
    			 requeridos[10] = Boolean.FALSE;
    			 foco = getTextHoraSalidaSala().getClientId();    			
    			break;
    		
    	}
    	setTieneFoco(foco);
    	//habilitarGuardar();
    }

    public void validarHoraTermino() {

        try {
        	//validamos que el campo no est� vac�o, si lo est� enviar� una excepcion
            capturarIntervencionesQuirurgicasServices.validarHoraTermino(getDatosHospitalizacion(), getFecha());
          //validamos si la especialidad de ingreso es cirugia ambulatoria, si lo es, se valida la hora, no se pueden guardar horas menores para esta especialdiad
            Ingreso ingreso = capturarIntervencionesQuirurgicasServices.buscarIngresoPorDatosPaciente(datosHospitalizacion);
            if(ingreso.getCveEspecialidadIngreso().equals(BaseConstants.ESPECIALIDAD_CIRUGIA_AMBULATORIA)){
            	
                if(capturarIntervencionesQuirurgicasServices.validarHoraTerminoMenorHoraInicio(getDatosHospitalizacion(), getFecha())){
                	 // es hora menor lanza exepcion
           		 	throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_602));
                }else{// la fecha es mayor, pasa al proceso normal.
                	 String fechaTermino = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaInicio());
                	 getDatosHospitalizacion().getDatosIntervenciones().setFechaTermino(capturarIntervencionesQuirurgicasServices
                             .convertirHora(fechaTermino, getDatosHospitalizacion().getDatosIntervenciones().getHoraTermino()));
                     requeridos[9] = Boolean.TRUE;
                     getDatosHospitalizacion().getDatosIntervenciones()
                     .setHoraSalida(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                     requeridos[10] = Boolean.FALSE;
                     setTieneFoco(getTextHoraSalidaSala().getClientId());
                     //habilitarGuardar();
                }
            }else{
            	//valida si la hora termino  es menor a la hora inicio, si no lo es, continua el proceso normal
                if(capturarIntervencionesQuirurgicasServices.validarHoraTerminoMenorHoraInicio(getDatosHospitalizacion(), getFecha())){
                	//habilita modal de confirmacion  
                	setHoraAValidar(IntervencionesQuirurgicasConstants.HORA_TERMINO);
                	RequestContext context = RequestContext.getCurrentInstance();
                	context.execute("PF('dlgConfirmacionTerminoHoras').show();");
                	setTieneFoco(getHorasSiTermino().getClientId());
                }else{// la fecha es mayor, pasa al proceso normal.
                	 String fechaTermino = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaInicio());
                	 getDatosHospitalizacion().getDatosIntervenciones().setFechaTermino(capturarIntervencionesQuirurgicasServices
                             .convertirHora(fechaTermino, getDatosHospitalizacion().getDatosIntervenciones().getHoraTermino()));
                     requeridos[9] = Boolean.TRUE;
                     getDatosHospitalizacion().getDatosIntervenciones()
                     .setHoraSalida(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                     requeridos[10] = Boolean.FALSE;
                     setTieneFoco(getTextHoraSalidaSala().getClientId());
                     //habilitarGuardar();
                }
            }
            
           
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setHoraTermino(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            requeridos[9] = Boolean.FALSE;
            setTieneFoco(getTextHoraTermino().getClientId());
            //habilitarGuardar();
        }
    }

    public void validarHoraSalidaSala() {

        try {
            capturarIntervencionesQuirurgicasServices.validarHoraSalaSalida(getDatosHospitalizacion(), getFecha());
            //validamos si la especialidad de ingreso es cirugia ambulatoria, si lo es, se valida la hora, no se pueden guardar horas menores para esta especialdiad
            Ingreso ingreso = capturarIntervencionesQuirurgicasServices.buscarIngresoPorDatosPaciente(datosHospitalizacion);
            if(ingreso.getCveEspecialidadIngreso().equals(BaseConstants.ESPECIALIDAD_CIRUGIA_AMBULATORIA)){
            	 if(capturarIntervencionesQuirurgicasServices.validarHoraSalaSalidaMenorHoraTermino(getDatosHospitalizacion(), getFecha())){
            		 throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_143));
                  }else{
                  	String fechaSalida = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaTermino());
                  	getDatosHospitalizacion().getDatosIntervenciones().setFechaSalida(capturarIntervencionesQuirurgicasServices
                              .convertirHora(fechaSalida, getDatosHospitalizacion().getDatosIntervenciones().getHoraSalida()));
                      requeridos[10] = Boolean.TRUE;
                      setTieneFoco(getSelectTipoIntervencion().getClientId());
                      //habilitarGuardar();
                  }
             }else{
            	 if(capturarIntervencionesQuirurgicasServices.validarHoraSalaSalidaMenorHoraTermino(getDatosHospitalizacion(), getFecha())){
                 	setHoraAValidar(IntervencionesQuirurgicasConstants.HORA_SALIDA_SALA);
                 	RequestContext context = RequestContext.getCurrentInstance();
                 	context.execute("PF('dlgConfirmacionSalidaHoras').show();");
                 	setTieneFoco(getHorasSiSalida().getClientId());
                 }else{
                 	String fechaSalida = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getDatosHospitalizacion().getDatosIntervenciones().getFechaTermino());
                 	getDatosHospitalizacion().getDatosIntervenciones().setFechaSalida(capturarIntervencionesQuirurgicasServices
                             .convertirHora(fechaSalida, getDatosHospitalizacion().getDatosIntervenciones().getHoraSalida()));
                     requeridos[10] = Boolean.TRUE;
                     setTieneFoco(getSelectTipoIntervencion().getClientId());
                     //habilitarGuardar();
                 }
            }
           
            
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setHoraSalida(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);

            requeridos[10] = Boolean.FALSE;
            setTieneFoco(getTextHoraSalidaSala().getClientId());
            //habilitarGuardar();
        }
    }

    public void validarCirujano() {

        try {
            capturarIntervencionesQuirurgicasServices.validarCirujano(getDatosHospitalizacion());
            setTieneFoco(getTextNombreCirujano().getClientId());
           // habilitarGuardar();
        } catch (HospitalizacionException e) {

            setTieneFoco(getTextClaveCirujano().getClientId());
           // habilitarGuardar();
        }
    }

    public void validarTipoIntervencion() {

        try {
            capturarIntervencionesQuirurgicasServices.validarTipoIntervencion(getDatosHospitalizacion());
            requeridos[11] = Boolean.TRUE;
           /* if (capturarIntervencionesQuirurgicasServices
                    .requiereAbrirModal(getDatosHospitalizacion().getDatosIntervenciones().getTipoIntervencion())) {
                getDatosHospitalizacion().getDatosIntervenciones()
                        .setModalFechaIntervencion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
                getTextFechaIntervencion().resetValue();
                requeridos[12] = Boolean.FALSE;
                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_INTERVENCION);
                setTieneFoco(getTextFechaIntervencion().getClientId());
            } else {*/
                //getDatosHospitalizacion().getDatosIntervenciones()
                //        .setModalFechaIntervencion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
               // getTextFechaIntervencion().resetValue();
                requeridos[12] = Boolean.TRUE;
                if (getDatosHospitalizacion().getDatosPaciente().isIndPlanificacionFamiliar() && !getAutoPlanificacion().isDisabled()
                		&& getMetodoVisible()) {
                    setTieneFoco(getAutoPlanificacion().getClientId());
                } else {
                    requeridos[13] = Boolean.TRUE;
                    setTieneFoco(getSelectTipoAnestesia().getClientId());
                }
           // }
            //habilitarGuardar();
        } catch (HospitalizacionException e) {

            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setModalFechaIntervencion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            requeridos[11] = Boolean.FALSE;
            requeridos[12] = Boolean.FALSE;
            requeridos[13] = Boolean.FALSE;
            setTieneFoco(getSelectTipoIntervencion().getClientId());
            //habilitarGuardar();
        }
    }

    /*public void validarFechaProgramacion() {

        try {
            capturarIntervencionesQuirurgicasServices.validarFechaProgramacion(getDatosHospitalizacion(), getFecha());
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setFechaProgramada(capturarIntervencionesQuirurgicasServices.convertirFecha(
                            getDatosHospitalizacion().getDatosIntervenciones().getModalFechaIntervencion()));
            requeridos[12] = Boolean.TRUE;
            if (getDatosHospitalizacion().getDatosPaciente().isIndPlanificacionFamiliar() && !getAutoPlanificacion().isDisabled()
            		&& getMetodoVisible()) {
                setTieneFoco(getAutoPlanificacion().getClientId());
            } else {
                requeridos[13] = Boolean.TRUE;
                setTieneFoco(getSelectTipoAnestesia().getClientId());
            }
           
            RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_INTERVENCION);
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setModalFechaIntervencion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            requeridos[12] = Boolean.FALSE;
            setTieneFoco(getTextFechaIntervencion().getClientId());
        }
    }*/

    public void validarMetodoPlanificacion() {

        try {

            DatosCatalogo datosCatalogo = null;
            datosCatalogo = validaAutoComplete(getAutoPlanificacion(), 4, getListaPlanificacionFamiliar(),
                    getCatalogoPlanificacion());
            if (null != datosCatalogo) {
                getDatosHospitalizacion().getDatosIntervenciones().setMetodoPlanificacion(datosCatalogo.getClave());
            } else {
                getDatosHospitalizacion().getDatosIntervenciones()
                        .setMetodoPlanificacion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            }
            existeMetodoTococirugia();
            boolean modal = capturarIntervencionesQuirurgicasServices.validarMetodoPlanificacion(
                    getDatosHospitalizacion().getDatosIntervenciones().getMetodoPlanificacion(),
                    getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico(), 
                    getDatosHospitalizacion().getDatosIntervenciones().getFechaEntrada());
            getDatosHospitalizacion().getDatosIntervenciones().setModalMetodoPlanificacion(modal);
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setCantidadPlanificacion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            requeridos[13] = Boolean.TRUE;
            if (modal) {
                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_CANTIDAD);
                setTieneFoco(getTextCantidad().getClientId());
            } else {
                requeridos[14] = Boolean.TRUE;
                setTieneFoco(getSelectTipoAnestesia().getClientId());
                habilitarGuardar();
            }

        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setMetodoPlanificacion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            requeridos[13] = Boolean.FALSE;
            getAutoPlanificacion().setValue(BaseConstants.CADENA_VACIA);
            setTieneFoco(getAutoPlanificacion().getClientId());
            //habilitarGuardar();
        }

    }

    public List<String> completarPlanificacionFam(String cveFiltro) {

        return filtrarListasComplementarias(cveFiltro, CapturarEgresoConstants.NUMERO_MAXIMO_TIPO_EGRESO,
                getListaPlanificacionFamiliar());
    }

    public void validarCantidadPlanificacionIQ() {

        try {
            capturarIntervencionesQuirurgicasServices.validarCantidadPlanificacion(
                    getDatosHospitalizacion().getDatosIntervenciones().getCantidadPlanificacion());
            setNumeroCantidad(getDatosHospitalizacion().getDatosIntervenciones().getCantidadPlanificacion() != null
                    ? Integer.parseInt(getDatosHospitalizacion().getDatosIntervenciones().getCantidadPlanificacion())
                    : null);
            super.validarCantidadPlanificacion(
                    getDatosHospitalizacion().getDatosIntervenciones().getMetodoPlanificacion());
            requeridos[14] = Boolean.TRUE;
            setTieneFoco(getSelectTipoAnestesia().getClientId());

        } catch (HospitalizacionException e) {
            setNumeroCantidad(null);
            getDatosHospitalizacion().getDatosIntervenciones()
                    .setCantidadPlanificacion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            agregarMensajeError(e);
            requeridos[14] = Boolean.FALSE;
            setTieneFoco(getTextCantidad().getClientId());
        }

    }

    public void validarTipoAnestesia() {

        try {
            capturarIntervencionesQuirurgicasServices.validarTipoAnestesia(getDatosHospitalizacion());
            requeridos[15] = Boolean.TRUE;
            setTieneFoco(getTextMatricula().getClientId());
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);

            requeridos[15] = Boolean.FALSE;
            setTieneFoco(getSelectTipoAnestesia().getClientId());
            //habilitarGuardar();
        }
    }

    public void validarAnestesiologo() {

        try {
            capturarIntervencionesQuirurgicasServices
                    .validarMatricula(getDatosHospitalizacion().getDatosMedico().getMatricula());
            getDatosHospitalizacion().setDatosMedico(capturarIntervencionesQuirurgicasServices.buscarPersonalOperativo(
                    getDatosHospitalizacion().getDatosMedico(), obtenerDatosUsuario().getCvePresupuestal()));
            getButtonGuardar().setDisabled(Boolean.FALSE);
            requeridos[16] = Boolean.TRUE;
            //setTieneFoco(IntervencionesQuirurgicasConstants.FOCO_BOTON_GUARDAR);
            //setTieneFoco(getButtonGuardar().getClientId());
            habilitarGuardar();
            
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosMedico().setMatricula(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            requeridos[16] = Boolean.FALSE;
            setTieneFoco(getTextMatricula().getClientId());
            habilitarGuardar();
        }
    }

    public void validarTotalProcedimientos() {

        try {
            capturarIntervencionesQuirurgicasServices.validarTotalProcedimientos(getDatosHospitalizacion());
            resetCamposModal();
            getDatosHospitalizacion().getDatosIntervenciones().setProcedimientos(new ArrayList<Procedimientos>());
            if (getDatosHospitalizacion().getDatosIntervenciones()
                    .getNumeroProcedimientos() > BaseConstants.INDEX_CERO) {
                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PROCEDIMIENTOS);
            }
            requeridos[4] = Boolean.TRUE;
            setTieneFoco(getTextClaveProcedimientoModal().getClientId());
            //habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosIntervenciones().setNumeroProcedimientos(0);
            requeridos[4] = Boolean.FALSE;
            setTieneFoco(getTextNumeroProcedimientos().getClientId());
            //habilitarGuardar();
        }

    }

    private void resetCamposModal() {

        setProcedimientoModal(new Procedimientos());
        getTextClaveProcedimientoModal().resetValue();
        getTextDescProcedimientoModal().resetValue();
        getTextClaveCirujanoModal().resetValue();
        getTextNombreCirujanoModal().resetValue();
    }
    
    public boolean validarListaProcedimientos(List<Procedimientos> listaProcedimientos){
    	boolean esProcedimientoEspecial = false;
    	if (null != listaProcedimientos && !listaProcedimientos.isEmpty()) {
            for (Procedimientos p : listaProcedimientos) {
                if (p.getClave().equals("6839") || p.getClave().equals("6849") 
                		|| p.getClave().equals("6551")) {
                	esProcedimientoEspecial = true;
                    break;
                }
            }
        }
    	return esProcedimientoEspecial;
    }

    public void validarProcedimientoModal() {

        try {

            String aux = procedimientoModal.getClave().toUpperCase();
            procedimientoModal.setClave(aux);
            capturarIntervencionesQuirurgicasServices.validarProcedimientoModal(getProcedimientoModal(),
                    getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos(),
                    getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
            procedimientoModal.setDescripcion(capturarIntervencionesQuirurgicasServices.obtenerDescripcion(aux));
            procedimientoModal
                    .setEtiquetaProcedimiento(armarEtiqueta(IntervencionesQuirurgicasConstants.ETIQUETA_PROCEDIMIENTO,
                            getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size()));
            requeridos[6] = Boolean.TRUE;
            if(getMetodoVisible()){
	            if(aux.trim().equals("6839") || aux.trim().equals("6849") || aux.trim().equals("6551")){
	            	datosHospitalizacion.getDatosIntervenciones().setMetodoPlanificacion("23");
		    		datosHospitalizacion.getDatosIntervenciones().setCantidadPlanificacion("1");
		    		requeridos[13] = Boolean.TRUE;
		    		requeridos[14] = Boolean.TRUE;
		    		for(String metodo : metodoFiltradoTocoAsociada ){
		    			if(metodo.contains(datosHospitalizacion.getDatosIntervenciones().getMetodoPlanificacion())){
		    				getAutoPlanificacion().setValue(metodo);    	    				
		    				setNumeroCantidad(getDatosHospitalizacion().getDatosIntervenciones().getCantidadPlanificacion() != null
	    		                    ? Integer.parseInt(getDatosHospitalizacion().getDatosIntervenciones().getCantidadPlanificacion())
	    		                    : null);	 
		    	    		getAutoPlanificacion().setDisabled(Boolean.TRUE);
		    			}
		    		}
	              
	            }else{
	            	if(!validarListaProcedimientos(getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos())){
		            	datosHospitalizacion.getDatosIntervenciones().setMetodoPlanificacion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
			    		datosHospitalizacion.getDatosIntervenciones().setCantidadPlanificacion(IntervencionesQuirurgicasConstants.CADENA_VACIA);
			    		setNumeroCantidad(null);
			    		//getAutoPlanificacion().resetValue();
			    		getAutoPlanificacion().setValue(IntervencionesQuirurgicasConstants.CADENA_VACIA);  
		            	complementarCatalogos();
		            	requeridos[13] = Boolean.FALSE;
			    		requeridos[14] = Boolean.FALSE;
		            	getAutoPlanificacion().setDisabled(Boolean.FALSE);
	            	}
	            } 
            }
            setTieneFoco(getTextClaveCirujanoModal().getClientId());
        } catch (HospitalizacionException e) {

            agregarMensajeError(e);
            getTextClaveProcedimientoModal().resetValue();
            procedimientoModal.setClave(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            requeridos[6] = Boolean.FALSE;
            setTieneFoco(getTextClaveProcedimientoModal().getClientId());
        }
    }

    private String armarEtiqueta(String constante, int numero) {

        numero = numero + 1;
        StringBuilder etiqueta = new StringBuilder();
        etiqueta.append(constante).append(numero);
        return etiqueta.toString();
    }

    public void validarCirujanoModal() {

        try {
            if (getProcedimientoModal().getClave() == null || getProcedimientoModal().getClave().length() < 4) {
                try {
                    capturarIntervencionesQuirurgicasServices.validarProcedimientoModal(getProcedimientoModal(),
                            getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos(),
                            getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                } catch (HospitalizacionException e) {
                    agregarMensajeError(e);
                    requeridos[6] = Boolean.FALSE;
                    setTieneFoco(getTextClaveProcedimientoModal().getClientId());
                    return;
                }
            } else {
                capturarIntervencionesQuirurgicasServices.validarCirujanoModal(procedimientoModal.getMatricula());

                DatosMedico medico = capturarIntervencionesQuirurgicasServices
                        .obtenerMedico(procedimientoModal.getMatricula(), obtenerDatosUsuario().getCvePresupuestal());
                if (medico != null && contadorNoRep == 0) {
                    contadorNoRep = 8888;
                    procedimientoModal.setMatricula(medico.getMatricula());
                    procedimientoModal.setNombreCompleto(medico.getNombreCompleto());
                    procedimientoModal.setIdentificadorConexion(medico.getIdentificadorConexion());
                    if (getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos() == null) {
                        getDatosHospitalizacion().getDatosIntervenciones()
                                .setProcedimientos(new ArrayList<Procedimientos>());
                    }
                    procedimientoModal.setConsecutivo(
                            getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size() + 1);
                    procedimientoModal
                            .setEtiquetaCirujano(armarEtiqueta(IntervencionesQuirurgicasConstants.ETIQUETA_CIRUJANO,
                                    getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().size()));
                    getDatosHospitalizacion().getDatosIntervenciones().getProcedimientos().add(procedimientoModal);
                    requeridos[5] = Boolean.TRUE;
                    if (getDatosHospitalizacion().getDatosIntervenciones()
                            .getNumeroProcedimientos() == getDatosHospitalizacion().getDatosIntervenciones()
                                    .getProcedimientos().size()) {
                        getDatosHospitalizacion().getDatosMedico().getIdentificadorConexion()
                                .equals(IdentificadorConexionEnum.ROJO.getRuta());
                        RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PROCEDIMIENTOS);
                        setTieneFoco(getTextHoraEntrada().getClientId());
                    } else {
                        resetCamposModal();
                        setTieneFoco(getTextClaveProcedimientoModal().getClientId());
                        RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PROCEDIMIENTOS);
                    }
                    Thread.sleep(1500);
                    contadorNoRep = 0;
                }

            }

        } catch (HospitalizacionException e) {

            agregarMensajeError(e);
            getTextClaveCirujanoModal().resetValue();
            procedimientoModal.setMatricula(IntervencionesQuirurgicasConstants.CADENA_VACIA);
            requeridos[5] = Boolean.TRUE;
            setTieneFoco(getTextClaveCirujanoModal().getClientId());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void cancelar() {

        resetCampos();
        setDatosHospitalizacion(new DatosHospitalizacion());
        
        desactivarElementos();
        llenarCatalogos();
        inicializarRequeridos();
        setTieneFoco(BaseConstants.FOCO_ID_FECHA);
    	setFecha(IntervencionesQuirurgicasConstants.CADENA_VACIA);
        dataTableDatosProcedimientos = new DataTable();
        setTipoFormato(TipoFormatoEnum.INTERVECION_QUIRURGICA);
        
        
    }

    private void resetCampos() {

        getMaskFecha().resetValue();
        getTextNss().resetValue();
        getTextNumeroQuirofano().resetValue();
        getTextCama().resetValue();
        getTextAgregadoMedico().resetValue();
        getTextNombre().resetValue();
        getTextApellidoPaterno().resetValue();
        getTextApellidoMaterno().resetValue();
        getTextNumeroProcedimientos().resetValue();
        getTextHoraEntrada().resetValue();
        getTextHoraInicio().resetValue();
        getTextHoraTermino().resetValue();
        getTextHoraSalidaSala().resetValue();
        getTextClaveProcedimiento().resetValue();
        getTextDescProcedimiento().resetValue();
        getTextClaveCirujano().resetValue();
        getTextNombreCirujano().resetValue();
        //getTextFechaIntervencion().resetValue();
        getTextCantidad().resetValue();
        getTextMatricula().resetValue();
        getTextMedico().resetValue();
        getAutoEspecialidad().resetValue();
        getAutoCompleteEspecialidad().resetValue();
        getAutoPlanificacion().resetValue();
        setCatalogoEspecialidad(new ArrayList<DatosCatalogo>());
        setCatalogoDivision(new ArrayList<DatosCatalogo>());
        setCatalogoTipoIntervencion(new ArrayList<DatosCatalogo>());
        setCatalogoMetodoPlanificacion(new ArrayList<DatosCatalogo>());
        setCatalogoPlanificacion(new ArrayList<DatosCatalogo>());
        setCatalogoTipoAnestesia(new ArrayList<DatosCatalogo>());
        setAutoCompleteEspecialidad(new AutoComplete());
        setAutoEspecialidad(new AutoComplete());
        setAutoPlanificacion(new AutoComplete());
        setFecha("");

    }

    public void guardar() {

        try {
            capturarIntervencionesQuirurgicasServices.validarFecha(getFecha(),
                    obtenerDatosUsuario().getCvePresupuestal());
            capturarIntervencionesQuirurgicasServices.validarNumeroQuirofano(getDatosHospitalizacion(),
                    obtenerDatosUsuario());
            capturarIntervencionesQuirurgicasServices.validarNSS(getDatosHospitalizacion());
            datosCatalogo = validaAutoComplete(getAutoEspecialidad(), 4, getCatalogoEspecialidadString(),
                    getCatalogoEspecialidad());
            if (null != datosCatalogo) {
                getDatosHospitalizacion().getDatosIntervenciones().setEspecialidad(datosCatalogo.getClave());
            }
            capturarIntervencionesQuirurgicasServices.validarEspecialidad(getDatosHospitalizacion());
            capturarIntervencionesQuirurgicasServices.iniciarTotalProcedimientos(
                    getDatosHospitalizacion().getDatosIntervenciones().getNumeroProcedimientos());
            capturarIntervencionesQuirurgicasServices.validarHoraEntradaSala(getDatosHospitalizacion(), getFecha());
            capturarIntervencionesQuirurgicasServices.validarHoraInicio(getDatosHospitalizacion(), getFecha());
            capturarIntervencionesQuirurgicasServices.validarHoraTermino(getDatosHospitalizacion(), getFecha());
            capturarIntervencionesQuirurgicasServices.validarHoraSalaSalida(getDatosHospitalizacion(), getFecha());
            capturarIntervencionesQuirurgicasServices.validarTipoIntervencion(getDatosHospitalizacion());
            /*if (capturarIntervencionesQuirurgicasServices
                    .requiereAbrirModal(getDatosHospitalizacion().getDatosIntervenciones().getTipoIntervencion())) {
                capturarIntervencionesQuirurgicasServices.validarFechaProgramacion(getDatosHospitalizacion(),
                        getFecha());
            }*/
            DatosCatalogo datosPlanificacion = null;
            datosPlanificacion = validaAutoComplete(getAutoPlanificacion(), 4, getListaPlanificacionFamiliar(),
                    getCatalogoPlanificacion());
            if (null != datosPlanificacion) {
                getDatosHospitalizacion().getDatosIntervenciones()
                        .setMetodoPlanificacion(datosPlanificacion.getClave());
            }

            if (getCatalogoPlanificacion() != null && !getCatalogoPlanificacion().isEmpty()) {

                if (getDatosHospitalizacion().getDatosIntervenciones().isModalMetodoPlanificacion()) {
                    capturarIntervencionesQuirurgicasServices.validarCantidadPlanificacion(
                            getDatosHospitalizacion().getDatosIntervenciones().getCantidadPlanificacion());
                }

            }

            capturarIntervencionesQuirurgicasServices.validarTipoAnestesia(getDatosHospitalizacion());
            capturarIntervencionesQuirurgicasServices
                    .validarMatricula(getDatosHospitalizacion().getDatosMedico().getMatricula());
            actualizarTococirugiaAsociada(); 
            capturarIntervencionesQuirurgicasServices.guardar(getDatosHospitalizacion(), obtenerDatosUsuario());
            agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
            //resetCampos();
            cancelar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);

            e.printStackTrace();

        }

    }

    public void habilitarGuardar() {

        if (capturarIntervencionesQuirurgicasServices.sePuedeActivarGuardar(getRequeridos())) {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            setTieneFoco(IntervencionesQuirurgicasConstants.FOCO_BOTON_GUARDAR);
        } else {
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }

    }

    public void salir() throws IOException {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }
    
    
    private void setHoraAValidar(String horaAValidar){
    	this.horaAValidar = horaAValidar;
    }
    
    private String getHoraAValidar(){
    	return horaAValidar;
    }
    
    public Tococirugia buscarTococirugiaAsociada(DatosHospitalizacion datosHospitalizacion){
    	
    	return  capturarEgresoRepository.buscarTococirugiaIngreso(datosHospitalizacion.getDatosPaciente());
    	
    }
    
    
    public void actualizarTococirugiaAsociada(){    	
    		try{
       		 Tococirugia tococirugiaAsociada = buscarTococirugiaAsociada(getDatosHospitalizacion());
                if(tococirugiaAsociada != null ){
                	tococirugiaAsociada.setCveMetodoAnticonceptivo(Integer.parseInt(datosHospitalizacion.getDatosIntervenciones().getMetodoPlanificacion()));
                	if(datosHospitalizacion.getDatosIntervenciones().getCantidadPlanificacion() != null && 
                			!datosHospitalizacion.getDatosIntervenciones().getCantidadPlanificacion().isEmpty()){
                		tococirugiaAsociada.setCantidadMetodoAnticonceptivo(Integer.parseInt(datosHospitalizacion.
                				getDatosIntervenciones().getCantidadPlanificacion()));
                	}
    	    		capturarTococirugiaService.actualizarMetodoAnticonceptivoTococirugia(tococirugiaAsociada.getCveMetodoAnticonceptivo(),
    	           		tococirugiaAsociada.getCantidadMetodoAnticonceptivo(), tococirugiaAsociada.getClave());
                }
       		
	       	}catch(HospitalizacionException e){
	       		logger.info("Ocurri� una excepcion al actualziar la tococirugia: "+e);
	       	}
    	
    	
    }
    
    public void existeMetodoTococirugia() throws HospitalizacionException{ 	
    	Tococirugia tococirugiaAsociada = buscarTococirugiaAsociada(getDatosHospitalizacion());
    	 if(tococirugiaAsociada != null && tococirugiaAsociada.getCveMetodoAnticonceptivo() != null){
    		 if(getDatosHospitalizacion().getDatosIntervenciones().getMetodoPlanificacion().equals(BaseConstants.NUMERO_CERO_STRING) && 
    				 tococirugiaAsociada.getCveMetodoAnticonceptivo() != BaseConstants.INDEX_CERO){
    			 getAutoPlanificacion().setValue(BaseConstants.CADENA_VACIA);
    			 throw new HospitalizacionException(MensajesErrorConstants.ME_609);
    		 }
    	 }    	
    }
    
    /*public void seleccionarMetodoAnticonceptivoTococirugia(){
    	 Tococirugia tococirugiaAsociada = buscarTococirugiaAsociada(getDatosHospitalizacion());
         if(tococirugiaAsociada != null ){
           		datosHospitalizacion.getDatosIntervenciones().setMetodoPlanificacion(tococirugiaAsociada.getCveMetodoAnticonceptivo().toString());
	    		datosHospitalizacion.getDatosIntervenciones().setCantidadPlanificacion(tococirugiaAsociada.getCantidadMetodoAnticonceptivo().toString());
	    		requeridos[13] = Boolean.TRUE;
	    		requeridos[14] = Boolean.TRUE;
	    		for(String metodo : metodoFiltradoTocoAsociada ){
	    			if(metodo.contains(tococirugiaAsociada.getCveMetodoAnticonceptivo().toString())){
	    				getAutoPlanificacion().setDisabled(Boolean.FALSE);
	    				getAutoPlanificacion().setValue(metodo);
	    				setNumeroCantidad(getDatosHospitalizacion().getDatosIntervenciones().getCantidadPlanificacion() != null
	    		                    ? Integer.parseInt(getDatosHospitalizacion().getDatosIntervenciones().getCantidadPlanificacion())
	    		                    : null);	    	    			    		
	    	    		getAutoPlanificacion().setDisabled(Boolean.TRUE);
	    	    		
	    			}
	    		}
	    		
         }
    }*/
    

}
