/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.vo;

import lombok.Data;

/**
 * @author francisco.rodriguez
 *
 */
@Data
public class SitParteII{

	public String cvePeriodoImss;
	public String cvePresupuestal;
	public String cveEspecialidad;
	public Integer numEjecucion;
	public String refUnidadMedica;
	public String refEspecialidad;
	public Integer cveDivision;
	public String refDivision;
	public Integer numMedico;
	public Integer numCama;
	public Integer numIngProgramado;
	public Integer numIngUrgente;
	public Integer numIngHospital;
	public Integer numTotalIngreso;
	public Integer numEgrExterna;
	public Integer numEgrUnidad;
	public Integer numEgrHospital;
	public Integer numEgrOtrohosp;
	public Integer numEgrDefuncion;
	public Double numTotalEgreso;
	public Double porcDefuncion;
	public Integer numPostMorten;
	public Double porcPostMorten;
	public Double numTotalIq;
	public Double numIqProgramado;
	public Double numDiasPaciente;
	public Double porcOcupacion;
	public Double porcDiasEstancia;
	public Double porcIndiceRotacion;
	public Double porcInidiceSustitucion;
	public Double numDiasCama;

}
