package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCamaAlojamiento;

public class OcupacionCamaAlojamientoRowMapperHelper extends BaseHelper implements RowMapper<OcupacionCamaAlojamiento> {
	
	@Override
    public OcupacionCamaAlojamiento mapRow(ResultSet resultSet, int rowId) throws SQLException {

        OcupacionCamaAlojamiento ocupacionCama = new OcupacionCamaAlojamiento();
        ocupacionCama.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
        ocupacionCama.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        ocupacionCama.setNumOcupacionCama(resultSet.getInt(SQLColumnasConstants.NUM_OCUPACION_CAMA));
        ocupacionCama.setFecCaptura(resultSet.getDate(SQLColumnasConstants.FEC_CAPTURA));
        return ocupacionCama;
    }

}
