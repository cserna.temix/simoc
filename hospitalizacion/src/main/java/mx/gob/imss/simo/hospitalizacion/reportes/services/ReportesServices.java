package mx.gob.imss.simo.hospitalizacion.reportes.services;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCama;
import mx.gob.imss.simo.hospitalizacion.common.model.PacientesHospitalizados;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientoRealizado;
import mx.gob.imss.simo.hospitalizacion.common.model.ReporteGenerado;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;

public interface ReportesServices extends HospitalizacionCommonServices {

    public PeriodosImss obtenerPeriodoImss(Integer anioPeriodo, Integer mesPeriodo);

    long guardarReporteGenerado(ReporteGenerado reporteGenerado);

    List<ProcedimientoRealizado> obtenerProcedimientosRealizados(String clavePresupuestal, String periodo,
            String procInicial, String procFinal);

    void cerrarPeriodo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    List<PacientesHospitalizados> obtenerPacientesHospitalizadosFecha(String clavePresupuestal, String fecha);

    Long numeroIngresosPacientesHospitalizadosFecha(String clavePresupuestal, String fecha);

    boolean validaPeriodoCerrado(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    Boolean validarReporteGeneradoHosp(String clavePresupuestal, Integer tipoReporte);

    Boolean validarReporteGeneradoHospPorFecha(String clavePresupuestal, Integer tipoReporte, String fecha);

    Boolean validarFormatoFecha(String fechaEgreso) throws HospitalizacionException;

    Long numeroRegistros(String clavePresupuestal, String fechaIni, String fechaFin);

    long insertarPeriodoOperacion(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss)
            throws HospitalizacionException;

    PeriodoOperacion buscarPeriodoAbierto(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    List<OcupacionCama> buscarOcupacionCamaPorPresupuestal(String clavePresupuestal);

    public Date obtenerUltimaFechaReportePacientes(Integer tipoReporte, String cvePresupuestal)
            throws HospitalizacionException;

    void ejecutarProcesosParteII() throws HospitalizacionException;

    void generarReporteParteIIPeriodo() throws HospitalizacionException;

    void generarReporteParteIICierre() throws HospitalizacionException;

    public void generarReporteParteIIAcumulado() throws HospitalizacionException;

    public String obtenerUltimoPeriodoParteII(String cvePresupuestal) throws HospitalizacionException;

    public Integer obtenerConteoDatosParteIIPeriodo(String cve_Presupuestal, String periodo)
            throws HospitalizacionException;

    public Integer buscarBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso)
            throws HospitalizacionException;

    public void updateBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso,
            Integer cveEstadoProceso, Boolean reproceso) throws HospitalizacionException;

    public void insertarInicioBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso,
            Integer cveEstadoProceso) throws HospitalizacionException;

    public String siguientePeriodo(String periodoActual) throws HospitalizacionException;

    public String obtenerPeriodoFinalizado(String cvePresupuestal) throws HospitalizacionException;

    public String periodoMasActualAbierto(String cvePresupuestal) throws HospitalizacionException;

    public PeriodosImss obtenerPeriodoImssXcvePeriodoImss(String clavePeriodoImss) throws HospitalizacionException;

    public void validarFechaRequerida(String fecha) throws HospitalizacionException;

    public void validarFechaPacientesHosp(String fecha, Date fechaFinal) throws HospitalizacionException;
}
