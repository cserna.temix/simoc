package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SexoEnum;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;

@Component
public class CatalogosHelper extends BaseHelper {

    public Integer sexoPorAgregadoMedico(String agregadoMedico) {

        return SexoEnum
                .parse(agregadoMedico.substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                .getClave();
    }

    public Integer edadPorAgregadoMedico(String agregadoMedico) {

        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR) - Integer
                .valueOf(agregadoMedico.substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO));
    }

    public List<DatosCatalogo> armarCatalogoMotivoAlta(List<DatosCatalogo> catalogoMotivoAlta, boolean transitorio) {

        List<DatosCatalogo> lista = new ArrayList<DatosCatalogo>();
        if (!transitorio) {
            for (DatosCatalogo dC : catalogoMotivoAlta) {
                if (!dC.getDescripcion().equals(CapturarEgresoConstants.TRANSITORIO)) {
                    lista.add(dC);
                }
            }
            return lista;
        }
        return catalogoMotivoAlta;
    }

    public Integer calcularEdad(DatosPaciente paciente) {

        Integer edad = 0;

        if (null != paciente.getFechaNacimiento()) {
            Calendar fecha = Calendar.getInstance();
            fecha.setTime(paciente.getFechaNacimiento());

            Calendar fechaAux = Calendar.getInstance();
            fechaAux.setTime(new Date());

            edad = fechaAux.get(Calendar.YEAR) - fecha.get(Calendar.YEAR);
        } else if (null != paciente.getEdadAnios() && paciente.getEdadAnios() > 0) {
            edad = paciente.getEdadAnios();
        } else if (null != paciente.getEdadSemanas() && paciente.getEdadSemanas() > 0) {
            edad = paciente.getEdadAnios() / BaseConstants.SEMANAS_ANIO;
        } else {
            edad = edadPorAgregadoMedico(paciente.getAgregadoMedico());
        }
        return edad;
    }
    
    public Integer calcularEdad(DatosPaciente paciente, Date fecAtencion) {

        Integer edad = 0;

        if (null != paciente.getFechaNacimiento()) {
            Calendar fecha = Calendar.getInstance();
            fecha.setTime(paciente.getFechaNacimiento());

            Calendar fechaAux = Calendar.getInstance();
            fechaAux.setTime(fecAtencion);

            edad = fechaAux.get(Calendar.YEAR) - fecha.get(Calendar.YEAR);
        } else if (null != paciente.getEdadAnios() && paciente.getEdadAnios() > 0) {
            edad = paciente.getEdadAnios();
        } else if (null != paciente.getEdadSemanas() && paciente.getEdadSemanas() > 0) {
            edad = paciente.getEdadAnios() / BaseConstants.SEMANAS_ANIO;
        } else {
            edad = edadPorAgregadoMedico(paciente.getAgregadoMedico());
        }
        return edad;
    }

    public Integer obtenerSexo(DatosPaciente paciente) {

        Integer cveSexo = 0;
        if (null != paciente.getSexo() && paciente.getSexo() > 0) {
            cveSexo = paciente.getSexo();
        } else {
            cveSexo = sexoPorAgregadoMedico(paciente.getAgregadoMedico());
        }

        return cveSexo;
    }

}
