package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Tococirugia {

    private long clave;
    private Date fechaCreacion;
    private Date fechaActualizacion;
    private Date fechaAtencion;
    private String cveSala;
    private int cveTipoAtencion;
    private Integer cveTipoParto;
    private String cvePresupuestal;
    private Integer cveMetodoAnticonceptivo;
    private Integer cantidadMetodoAnticonceptivo;
    private Integer totalRecienNacidos;
    private String cveMedico;
    private String cveCapturista;
    private String cveCapturistaActualizada;
    private int cveTipoFormato;
    private int gesta;
    private int episiotomia;

}
