package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class RecienNacido {

    private long clave;
    private int numeroRecienNacido;
    private Date fechaCreacion;
    private Date fechaActualizacion;
    private int peso;
    private int talla;
    private int perimetroCefalico;
    private int semanasGestacion;
    private Integer apgar1;
    private Integer apgar5;
    private Date fechaDefuncion;
    private String claveCapturista;
    private String claveCapturistaActualiza;
    private long clavePaciente;
    private int indicadorIngresoRN;
    private int claveTipoNacido;
    private int claveRangoLubchenco;

}
