package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class DatosIntervenciones {

    private String fechaIntervencion;

    private String numeroQuirofano;
    private String especialidad;
    private String division;
    private Integer numeroProcedimientos;
    private String horaEntrada;
    private String horaInicio;
    private String horaTermino;
    private String horaSalida;
    private List<Procedimientos> procedimientos;
    private String tipoIntervencion;
    private String metodoPlanificacion;
    private String tipoAnestesia;
    private String claveProcedimiento;
    private String descProcedimiento;
    private String claveCirujano;
    private String nombreCirujano;
    private String modalFechaIntervencion;
    private String cantidadPlanificacion;
    private Date fechaIQX;
    private Date fechaProgramada;
    private Date fechaEntrada;
    private Date fechaInicio;
    private Date fechaTermino;
    private Date fechaSalida;
    private boolean modalMetodoPlanificacion;
    private Integer cantidadMetodoPlanificacion;

}
