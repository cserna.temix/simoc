package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

public class CatalogoTurnoRowMapperHelper extends BaseHelper implements RowMapper<DatosCatalogo> {

	@Override
	public DatosCatalogo mapRow(ResultSet resultSet, int rowId) throws SQLException {
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave( String.valueOf( resultSet.getInt("CVE_TURNO")) );
		datosCatalogo.setDescripcion(resultSet.getString("DES_TURNO"));
		return datosCatalogo;
	}

}
