/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.common.constant;

import lombok.Getter;
import lombok.Setter;

public enum TipoDxProcInterconsultaEnum {
	
	DX_PRINCIPAL(1),
	DX_ADICIONAL(2),
	DX_COMPLEMENTARIO(3),
	PROCEDIMIENTO_PRINCIPAL(1),
	PROCEDIMIENTO_ADICIONAL(2),
	PROCEDIMIENTO_COMPLEMENTARIO(3);
	
	@Getter
	@Setter
    private Integer clave;

    private TipoDxProcInterconsultaEnum(Integer clave) {
        this.clave = clave;
    }	
	
}
