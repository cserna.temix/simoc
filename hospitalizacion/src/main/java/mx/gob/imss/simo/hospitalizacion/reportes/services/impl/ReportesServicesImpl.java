package mx.gob.imss.simo.hospitalizacion.reportes.services.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoReporte;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCama;
import mx.gob.imss.simo.hospitalizacion.common.model.PacientesHospitalizados;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientoRealizado;
import mx.gob.imss.simo.hospitalizacion.common.model.ReporteGenerado;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.reportes.repository.ReportesRepository;
import mx.gob.imss.simo.hospitalizacion.reportes.services.ProcesarParteIIService;
import mx.gob.imss.simo.hospitalizacion.reportes.services.ReportesServices;

@Service("reportesServices")
public class ReportesServicesImpl extends HospitalizacionCommonServicesImpl implements ReportesServices {

    @Autowired
    private ReportesRepository reportesRepository;

    @Autowired
    private ProcesarParteIIService procesarParteIIService;

    @Autowired
    private HospitalizacionCommonRules hospitalizacionCommonRules;

    private SimpleDateFormat sdf1 = new SimpleDateFormat("YYYYMM");

    @Override
    public PeriodosImss obtenerPeriodoImss(Integer anioPeriodo, Integer mesPeriodo) {

        return reportesRepository.obtenerPeriodoImss(anioPeriodo, mesPeriodo);
    }

    @Override
    public long guardarReporteGenerado(ReporteGenerado reporteGenerado) {

        return getHospitalizacionCommonRepository().guardarReporteGenerado(reporteGenerado);
    }

    @Override
    public List<ProcedimientoRealizado> obtenerProcedimientosRealizados(String clavePresupuestal, String periodo,
            String procInicial, String procFinal) {

        return getHospitalizacionCommonRepository().obtenerProcedimientosRealizados(clavePresupuestal, periodo,
                procInicial, procFinal);
    }

    @Override
    public void cerrarPeriodo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss) {

        getHospitalizacionCommonRepository().cerrarPeriodo(clavePresupuestal, tipoCaptura, clavePeriodoImss);

    }

    @Override
    public List<PacientesHospitalizados> obtenerPacientesHospitalizadosFecha(String clavePresupuestal, String fecha) {

        return getHospitalizacionCommonRepository().obtenerPacientesHospitalizadosFecha(clavePresupuestal, fecha);
    }

    @Override
    public boolean validaPeriodoCerrado(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss) {

        return getHospitalizacionCommonRepository().validaPeriodoCerrado(clavePresupuestal, tipoCaptura,
                clavePeriodoImss);
    }

    @Override
    public Boolean validarReporteGeneradoHosp(String clavePresupuestal, Integer tipoReporte) {

        return validarReporteGeneradoH(clavePresupuestal, tipoReporte);

    }

    @Override
    public Boolean validarReporteGeneradoHospPorFecha(String clavePresupuestal, Integer tipoReporte, String fecha) {

        return validarReporteGeneradoHPorFecha(clavePresupuestal, tipoReporte, fecha);
    }

    @Override
    public Boolean validarFormatoFecha(String fechaEgreso) {

        Integer dia = Integer.valueOf(fechaEgreso.substring(0, 2));
        Integer mes = Integer.valueOf(fechaEgreso.substring(3, 5));
        Integer anio = Integer.valueOf(fechaEgreso.substring(6));
        Boolean fechaValida = Boolean.TRUE;
        if (dia > 31 || mes > 12) {
            fechaValida = Boolean.FALSE;
        }
        if (this.validarMesFebrero(dia, mes, anio)) {
            fechaValida = Boolean.FALSE;
        }
        if (this.validarMes30dias(dia, mes)) {
            fechaValida = Boolean.FALSE;
        }
        return fechaValida;
    }

    public boolean validarMesFebrero(Integer dia, Integer mes, Integer anio) {

        if (mes == 2) {
            GregorianCalendar calendar = new GregorianCalendar();
            if ((calendar.isLeapYear(anio) && dia > 29) || (dia > 28)) {
                return true;
            }
        }
        return false;

    }

    public boolean validarMes30dias(Integer dia, Integer mes) {

        if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
            if (dia > 30) {
                return true;
            }
        }
        return false;

    }

    @Override
    public Long numeroRegistros(String clavePresupuestal, String fechaIni, String fechaFin) {

        return getHospitalizacionCommonRepository().numeroRegistros(clavePresupuestal, fechaIni, fechaFin);
    }

    @Override
    public long insertarPeriodoOperacion(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss)
            throws HospitalizacionException {

        return getHospitalizacionCommonRepository().insertarPeriodoOperacion(clavePresupuestal, tipoCaptura,
                clavePeriodoImss, 0, 1, 0);
    }

    @Override
    public PeriodoOperacion buscarPeriodoAbierto(String clavePresupuestal, Integer tipoCaptura,
            String clavePeriodoImss) {

        return getHospitalizacionCommonRepository().buscarPeriodoAbierto(clavePresupuestal, tipoCaptura,
                clavePeriodoImss);
    }

    @Override
    public Date obtenerUltimaFechaReportePacientes(Integer tipoReporte, String cvePresupuestal)
            throws HospitalizacionException {

        return reportesRepository.obtenerUltimaFechaReportePacientes(tipoReporte, cvePresupuestal);
    }

    @Override
    public void ejecutarProcesosParteII() throws HospitalizacionException {

        generarReporteParteIIPeriodo();
        generarReporteParteIICierre();
        generarReporteParteIIAcumulado();
    }

    /**
     * Metodo que realiza la carga de datos para el reporte parte II en la modalidad por periodo
     * Al inicio del proceso de eliminan los datos de las tablas a las que se tiene que consultar.
     * Se realiza el proceso de carga de datos para posteriormente obtener el reporte
     * 
     * @throws HospitalizacionException
     *             en caso de que ocurra un error en el proceso
     *             de carga de datos en las tablas
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void generarReporteParteIIPeriodo() throws HospitalizacionException {

        logger.debug("Entro el metodo generarReporteParteIIPeriodo...");
        System.out.println("Entro el metodo generarReporteParteIIPeriodo...");
        String clavePresupuestal = "";
        String periodoImss = "";
        try {
            List<PeriodoOperacion> periodos = reportesRepository.obtenerPeriodosRepParteII();
            logger.debug("Numero de periodos: " + periodos.size());
            for (PeriodoOperacion periodo : periodos) {
                clavePresupuestal = periodo.getClavePresupuestal();
                periodoImss = periodo.getClavePeriodoIMSS();
                logger.info("fue a Borrar Registros: Clave Presupuestal: " + periodo.getClavePresupuestal() + " Clave Periodo: " + periodo.getClavePeriodoIMSS());
                reportesRepository.borrarRegistros(periodo.getClavePresupuestal(), periodo.getClavePeriodoIMSS());
            }
            System.out.println("periodoImss..." + periodoImss);
            List<String> claves = reportesRepository.obtenerClavesPresupuestales();
            logger.debug("Numero de claves: " + claves.size());
            if (!claves.isEmpty()) {
                for (String clave : claves) {
                	logger.debug("PROCESANDO EN BATCH CLAVE: " + clave);
                    procesarParteIIService.procesarPrincial(clave, 1);
                }
            }
        } catch (NullPointerException npe) {
            logger.error("Ocurrio un error en el proceso de obtener la informacion para el reporte por periodo", npe);
            String errorMensaje = "";
            if (npe != null && npe.getMessage().length() < 250) {
                errorMensaje = npe.getMessage();
            } else if (npe != null) {
                errorMensaje = npe.getMessage().substring(0, 249);
            }
            reportesRepository.updateBitacoraError(clavePresupuestal, periodoImss, 1, errorMensaje);
            throw new HospitalizacionException(npe.getMessage());
        } catch (HospitalizacionException e) {
            logger.error("Ocurrio un error en el proceso de obtener la informacion para el reporte por periodo", e);
            String errorMensaje = "";
            if (e != null && e.getMessage().length() < 250) {
                errorMensaje = e.getMessage();
            } else if (e != null) {
                errorMensaje = e.getMessage().substring(0, 249);
            }
            reportesRepository.updateBitacoraError(clavePresupuestal, periodoImss, 1, errorMensaje);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    /**
     * Metodo que realiza la carga de datos correspondientes al cierre de mes
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void generarReporteParteIICierre() throws HospitalizacionException {

        logger.debug("Entro el metodo generarReporteParteIICierre...");

        try {
            List<String> lista = reportesRepository.obtenerClavesPresupuestalesParaCierre();
            for (String clave : lista) {
                logger.debug("Se obtiene la clave para cierre [" + clave + "]");
                logger.debug(
                        "Se inicia... " + new SimpleDateFormat(BaseConstants.FORMATO_FECHA_HORA).format(new Date()));

                procesarParteIIService.procesarPrincial(clave, 2);
                logger.debug(
                        "Finalizo... " + new SimpleDateFormat(BaseConstants.FORMATO_FECHA_HORA).format(new Date()));

                ReporteGenerado datosReporteGen = new ReporteGenerado();
                datosReporteGen.setCvePresupuestal(clave);
                datosReporteGen.setTipoReporte(TipoReporte.PARTE_II.getClave());
                datosReporteGen.setFecGeneracion(new Date());

                guardarReporteGenerado(datosReporteGen);
            }
        } catch (HospitalizacionException e) {
            logger.error("Ocurrio un error en el proceso de obtener la informacion para el reporte de cierre", e);
            throw new HospitalizacionException(e.getMessage());
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void generarReporteParteIIAcumulado() throws HospitalizacionException {

        logger.debug("Entro el metodo generarReporteParteIIAcumulado...");

        try {
            List<String> lista = reportesRepository.obtenerClavesPresupuestalesParaCierre();
            for (String clave : lista) {
                logger.info("Se obtiene la clave acumulado [" + clave + "]");
                procesarParteIIService.procesarAcomulado(clave);
            }
        } catch (HospitalizacionException e) {
            logger.error("Ocurrio un error en el proceso de obtener la informacion para el reporte de cierre", e);
            throw new HospitalizacionException(e.getMessage());
        } catch (ParseException pe) {
            logger.error("Ocurrio un error en el parseo de algun calculo", pe);
            throw new HospitalizacionException(pe.getMessage());
        }
    }

    @Override
    public List<OcupacionCama> buscarOcupacionCamaPorPresupuestal(String clavePresupuestal) {

        return getHospitalizacionCommonRepository().buscarOcupacionCamaPorPresupuestal(clavePresupuestal);
    }

    @Override
    public String obtenerUltimoPeriodoParteII(String cvePresupuestal) throws HospitalizacionException {

        Date fecha = reportesRepository.obtenerUltimoPeriodoParteII(cvePresupuestal);
        return sdf1.format(fecha);
    }

    @Override
    public Long numeroIngresosPacientesHospitalizadosFecha(String clavePresupuestal, String fecha) {

        return getHospitalizacionCommonRepository().numeroIngresosPacientesHospitalizadosFecha(clavePresupuestal,
                fecha);
    }

    @Override
    public Integer obtenerConteoDatosParteIIPeriodo(String cve_Presupuestal, String periodo)
            throws HospitalizacionException {

        return reportesRepository.obtenerConteoDatosParteIIPeriodo(cve_Presupuestal, periodo);
    }

    @Override
    public Integer buscarBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso)
            throws HospitalizacionException {

        return reportesRepository.buscarBitacora(cvePresupuestal, cvePeriodoImss, cveProceso);
    }

    @Override
    public void updateBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso,
            Integer cveEstadoProceso, Boolean reproceso) throws HospitalizacionException {

        reportesRepository.updateBitacora(cvePresupuestal, cvePeriodoImss, cveProceso, cveEstadoProceso, reproceso);
    }

    @Override
    public void insertarInicioBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso,
            Integer cveEstadoProceso) throws HospitalizacionException {

        reportesRepository.insertarInicioBitacora(cvePresupuestal, cvePeriodoImss, cveProceso, cveEstadoProceso);
    }

    @Override
    public String siguientePeriodo(String periodoActual) throws HospitalizacionException {

        String mesperiodoSiguiente = "";
        String anioPeriodoActual = periodoActual.substring(0, 4);
        String mesPeriodoActual = periodoActual.substring(4);
        logger.debug("mes [" + mesPeriodoActual + "]");
        logger.debug("anio [" + anioPeriodoActual + "]");
        if (Integer.valueOf(mesPeriodoActual) == 12) {
            mesperiodoSiguiente = "01";
            int numeroAnio = Integer.parseInt(anioPeriodoActual);
            numeroAnio++;
            return numeroAnio + mesperiodoSiguiente;
        } else {
            Integer numeroMes = Integer.parseInt(mesPeriodoActual);
            numeroMes++;
            mesperiodoSiguiente = String.format("%02d", numeroMes);
            return anioPeriodoActual + "" + mesperiodoSiguiente;
        }
    }

    @Override
    public String obtenerPeriodoFinalizado(String cvePresupuestal) throws HospitalizacionException {

        return reportesRepository.obtenerPeriodoFinalizado(cvePresupuestal);
    }

    @Override
    public String periodoMasActualAbierto(String cvePresupuestal) throws HospitalizacionException {

        return reportesRepository.periodoMasActualAbierto(cvePresupuestal);
    }

    @Override
    public PeriodosImss obtenerPeriodoImssXcvePeriodoImss(String clavePeriodoImss) throws HospitalizacionException {

        return reportesRepository.obtenerPeriodoImssXcvePeriodoImss(clavePeriodoImss);
    }

    public void validarFechaRequerida(String fecha) throws HospitalizacionException {

        if (fecha == null || fecha.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_FECHA));
        }
    }

    public void validarFechaPacientesHosp(String fecha, Date fechaFinalPeriodo) throws HospitalizacionException {

        hospitalizacionCommonRules.validarFormatoFecha(fecha);
        hospitalizacionCommonRules.validarFechaActual(fecha);
        hospitalizacionCommonRules.validarFechaReportePH(fecha, fechaFinalPeriodo);
    }

}
