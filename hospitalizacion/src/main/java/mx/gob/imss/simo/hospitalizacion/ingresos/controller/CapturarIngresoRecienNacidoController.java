package mx.gob.imss.simo.hospitalizacion.ingresos.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.BusquedasConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesAdvertenciaConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SexoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngreso;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;

@ViewScoped
@ManagedBean(name = "ingresoRecienNacidoController")
public class CapturarIngresoRecienNacidoController extends CapturarIngresoBaseController {

    private String camaMadre;

    private Boolean madreConTococirugia = Boolean.TRUE;

    private boolean madreExterna = Boolean.FALSE;

    private Boolean busquedaPorAgregado = Boolean.FALSE;

    private Boolean busquedaPorCama = Boolean.FALSE;
    
    private Boolean habilitarEspHosp = Boolean.FALSE;

    private DatosPaciente datosPaciente;

    private static final long serialVersionUID = -4269704932799094156L;

    @Override
    public String obtenerNombrePagina() {

        return PagesCommonConstants.INGRESO_RECIEN_NACIDOS;
    }

    @Override
    @PostConstruct
    public void init() {

        super.init();
        setTipoUbicacion(TipoUbicacionEnum.RECIEN_NACIDOS);
        deshabilitarCamposInicio();
        setCatalogoEspecialidad(getCapturarIngresoServices().obtenerCatalogoEspecialidad(getTipoUbicacion(),
                objetosSs.getDatosUsuario().getCvePresupuestal()));
        setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
    }

    @Override
    public void validarFechaIngreso() {

        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_1)) {
                getDatosHospitalizacion().getDatosPaciente().setNss(BaseConstants.CADENA_VACIA);
                return;
            }
            String cve_Parametro = "PERIODO_LIBERACION";
            String fechaPeriodoConsulta = null;
            fechaPeriodoConsulta = capturarIngresoRepository.buscarPeriodoLiberacion(cve_Parametro);
            logger.info("Consulta: " +fechaPeriodoConsulta);
            logger.info("fecha Ingreso: " +fechaIngreso);
            SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaComp = null;
			Date fechaPeriodo = null;
			try {
				fechaComp = sdformat.parse(fechaIngreso);
				fechaPeriodo = sdformat.parse(fechaPeriodoConsulta);
			} catch (ParseException e) {
				logger.error("error al convertir la fecha" + e);
				e.printStackTrace();
			}
            if(fechaComp.before(fechaPeriodo)){
            	throw new HospitalizacionException(MensajesErrorConstants.ME_202);
            	          	
            }
            super.validarFechaIngreso();
            //si la fecha es extemporanea, solicitar aprobacion para registro
            if(datosHospitalizacion.getDatosIngreso().isExtemporaneo()){ 
            	RequestContext context = RequestContext.getCurrentInstance();
            	context.execute("PF('dlgConfirmacionFechaExtemporanea').show();");
            	setTieneFoco(getExtemporaneoSi().getClientId());
            } else{
            	 getButtonCancelar().setDisabled(Boolean.FALSE);
                 tieneFoco = getTextNss().getClientId();
            }
        } catch (HospitalizacionException e) {
            getButtonCancelar().setDisabled(Boolean.TRUE);
            setFechaIngreso(CapturarIngresosConstants.FECHA_VACIA);
            agregarMensajeError(e);
            setTieneFoco(getMaskFecha().getClientId());
        }
    }
    
    public void aprobarFechaIngresoExtemporanea(){
    	RequestContext context = RequestContext.getCurrentInstance();
    	context.execute("PF('dlgConfirmacionFechaExtemporanea').hide();");
    	getButtonCancelar().setDisabled(Boolean.FALSE);
        tieneFoco = getTextNss().getClientId();
    }
    
    public void rechazarFechaIngresoExtemporanea(){
    	RequestContext context = RequestContext.getCurrentInstance();
    	context.execute("PF('dlgConfirmacionFechaExtemporanea').hide();");
    	 getButtonCancelar().setDisabled(Boolean.TRUE);
         fechaIngreso = CapturarIngresosConstants.FECHA_VACIA;
         tieneFoco = getMaskFecha().getClientId();
    }
    
    private void obtenerEspecialidadIngresoVig(List<Ingreso> ingresosVigentes){
    	List<DatosCatalogo> especiaidadesRN = getCapturarIngresoServices()
                .obtenerCatalogoEspecialidad(getTipoUbicacion(), objetosSs.getDatosUsuario().getCvePresupuestal());
    	setCatalogoEspecialidad(
                getCapturarIngresoServices().filtrarCatalogoEspecialidadRN(especiaidadesRN,
                		ingresosVigentes.get(0).getCveTipoFormato()));
        setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
    }

    private void obtenerEspecialidad() {

        List<DatosCatalogo> especiaidadesRN = getCapturarIngresoServices()
                .obtenerCatalogoEspecialidad(getTipoUbicacion(), objetosSs.getDatosUsuario().getCvePresupuestal());
        if (!madreConTococirugia && busquedaPorAgregado) {
            for (int i = 0; i < especiaidadesRN.size(); i++){
                if (especiaidadesRN.get(i).getClave().equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)) {
                    especiaidadesRN.remove(especiaidadesRN.get(i));
                    i--;
                }
            }
            setCatalogoEspecialidad(especiaidadesRN);
            setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
        } else {
        	setCatalogoEspecialidad(
                    getCapturarIngresoServices().filtrarCatalogoEspecialidadRN(especiaidadesRN,
                            getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato()));
            setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
            if(!getDatosHospitalizacion().getDatosIngresoRN().isAlojamientoPrevio() 
            		&& !getDatosHospitalizacion().getDatosIngresoRN().isCuneroVigente()){
                getAutoEspecialidad().setValue(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO_CADENA);
             }
//            setCatalogoEspecialidad(especiaidadesRN);
//            setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
//            getAutoEspecialidad().setValue(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO_CADENA);
        }
        setTieneFoco(getAutoEspecialidad().getClientId());
    }

    public void validarNss() {

        busquedaPorAgregado = Boolean.FALSE;
        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_2)) {
                getDatosHospitalizacion().getDatosIngresoRN().setNss(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarNssRN();
            setRecienNacidos(getCapturarIngresoServices().buscarRecienNacidosNss(
                    getDatosHospitalizacion().getDatosIngresoRN().getNss(),
                    objetosSs.getDatosUsuario().getCvePresupuestal()));
            busquedaPorCama = Boolean.FALSE;
            if (getRecienNacidos().isEmpty()) {
                madreConTococirugia = Boolean.FALSE;
                getTextCama().setDisabled(Boolean.FALSE);
                setTieneFoco(getTextCama().getClientId());
                busquedaPorCama = Boolean.TRUE;
                getButtonGuardar().setDisabled(Boolean.TRUE);
                getTextNss().setDisabled(Boolean.TRUE);
                // agregarMensajeError(new HospitalizacionException(MensajesGeneralesConstants.MG_004));
            } else if (getRecienNacidos().size() < 2) {
                getDatosHospitalizacion().setDatosIngresoRN(getRecienNacidos().get(0));
                if (madreConTococirugia) {
                    getCapturarIngresoServices().validarFechaAnterior(getFechaIngreso(),
                            getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso(),
                            getDatosHospitalizacion().getDatosIngresoRN().getFechaParto(), getTipoUbicacion(),
                            getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato());
                }
                
                if(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso() != null
                		&& !getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso().isEmpty()){
                	habilitarEspHosp = Boolean.FALSE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
                	Especialidad especialidadHospitalizacion = capturarIngresoServices
                            .buscarEspecialidad(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso());
                	getAutoEspecialidadHospitalizacion().setValue(especialidadHospitalizacion.getCveEspecialidad()
                            .concat(BaseConstants.ESPACIO).concat(especialidadHospitalizacion.getDesEspecialidad()));
          
                }else{
                	habilitarEspHosp = Boolean.TRUE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
                }
               
                getDatosHospitalizacion().getDatosIngresoRN().setEspecialidadIngreso(null);
                getDatosHospitalizacion().getDatosIngresoRN().setDivision(null);
                getAutoEspecialidad().resetValue();
                setCatalogoDivision(new ArrayList<DatosCatalogo>());
                obtenerEspecialidad();
                getButtonGuardar().setDisabled(Boolean.TRUE);
                getTextNss().setDisabled(Boolean.TRUE);
            } else {
            	getButtonGuardar().setDisabled(Boolean.TRUE);
                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PACIENTES);
                setTieneFoco(getTextNumeroPaciente().getClientId());
            }
        } catch (HospitalizacionException e) {
            if (e.getArgs() != null) {
                getDatosHospitalizacion().getDatosIngresoRN().setNss(null);
                setTieneFoco(getTextNss().getClientId());
            } else {
                setFechaIngreso(null);
                getDatosHospitalizacion().setDatosIngresoRN(new DatosIngresoRN());
                setTieneFoco(getMaskFecha().getClientId());
            }
            getTextNss().setDisabled(Boolean.FALSE);
            getTextCama().setDisabled(Boolean.TRUE);
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    public void validarAgregadoMedico() {

        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_3)) {
                getDatosHospitalizacion().getDatosIngresoRN().setAgregadoMedico(BaseConstants.CADENA_VACIA);
                return;
            }

            busquedaPorAgregado = Boolean.TRUE;
            obtenerEspecialidad();
            super.validarAgregadoMedicoRN();
            DatosPaciente datosPacienteRN = new DatosPaciente();
            datosPacienteRN.setNss(getDatosHospitalizacion().getDatosIngresoRN().getNss());
            datosPacienteRN.setAgregadoMedico(getDatosHospitalizacion().getDatosIngresoRN().getAgregadoMedico());
            pacientes = capturarIngresoServices.buscarPaciente(datosPacienteRN, fechaIngreso);

            if (pacientes.size() < 2) {
                datosPaciente = (pacientes.get(0));
                capturarIngresoServices.validarIngresoVigente(objetosSs.getDatosUsuario().getCvePresupuestal(),
                        getTipoUbicacion(), datosPaciente);
                if (datosPaciente.getAgregadoMedico().substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())
                        || datosPaciente.getNombre().isEmpty()) {
                    // Derechohabiente no encontrado / No derechohabiente
                    limpiarCatalogos();
                    //datosPaciente = new DatosPaciente();
                    DatosIngresoRN datosIngresoRN = new DatosIngresoRN();
                    datosIngresoRN.setNss(datosHospitalizacion.getDatosIngresoRN().getNss());
                    datosIngresoRN.setAgregadoMedico(datosHospitalizacion.getDatosIngresoRN().getAgregadoMedico());
                    
                    //Clave presupuestal no derechohabiente
                    if(datosPaciente.getAgregadoMedico().substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                            .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())){
                    	datosIngresoRN.setClavePresupuestal(BusquedasConstants.CLAVE_ND);
                        datosIngresoRN.setDescripcionUnidad(BusquedasConstants.DESCRIPCION_ND);
                    }  
                    datosPaciente = new DatosPaciente();
                    //
                    datosHospitalizacion.setDatosIngresoRN(datosIngresoRN);
                    habilitarCamposNoEncontradoNoDerechohabiente();
                    getDatosHospitalizacion().getDatosIngresoRN().setCama(BaseConstants.CADENA_VACIA);
                    getTextCama().setValue(null);
                    getTextCama().setDisabled(Boolean.TRUE);
                    madreExterna = Boolean.TRUE;
                    tieneFoco = getTextNombre().getClientId();
                    getTextAgregadoMedico().setDisabled(Boolean.TRUE);
                } else {
                    // Derechohabiente encontrado
                    deshabilitarCamposDerechohabiente();
                    DatosIngresoRN datosIngresoRN = new DatosIngresoRN();
                    datosIngresoRN.setNumeroUnidad(datosPaciente.getNumero());
                    datosIngresoRN.setClavePresupuestal(datosPaciente.getClavePresupuestal());
                    datosIngresoRN.setDescripcionUnidad(datosPaciente.getDescripcionUnidad());
                    datosIngresoRN.setNss(datosPaciente.getNss());
                    datosIngresoRN.setAgregadoMedico(datosPaciente.getAgregadoMedico());
                    datosIngresoRN.setClaveDelegacion(datosPaciente.getDelegacion());
                    List<DatosCatalogo> delegaciones = catalogosHospitalizacionServices
                            .obtenerCatalogoDelegacion(datosPaciente.getNumero());
                    delegacion = datosPaciente.getDelegacion();
                    obtenerDelegacionRN(delegaciones);
                    datosIngresoRN.setDescripcionDelegacion(
                            datosHospitalizacion.getDatosIngresoRN().getDescripcionDelegacion());
                    getDatosHospitalizacion().setDatosIngresoRN(datosIngresoRN);
                    getTextCama().setValue(null);
                    getTextCama().setDisabled(Boolean.TRUE);
                    tieneFoco = getTextNombre().getClientId();
                    getTextAgregadoMedico().setDisabled(Boolean.TRUE);
                }
                datosHospitalizacion.getDatosPaciente()
                        .setSexo(SexoEnum
                                .parse(datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                                        .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                                .getClave());
            } else {
                limpiarCatalogos();
                datosPaciente = new DatosPaciente();
                DatosIngresoRN datosIngresoRN = new DatosIngresoRN();
                datosIngresoRN.setNss(datosHospitalizacion.getDatosIngresoRN().getNss());
                datosIngresoRN.setAgregadoMedico(datosHospitalizacion.getDatosIngresoRN().getAgregadoMedico());
                datosHospitalizacion.setDatosIngresoRN(datosIngresoRN);
                habilitarCamposNoEncontradoNoDerechohabiente();
                madreExterna = Boolean.TRUE;
                tieneFoco = getTextNombre().getClientId();
                getTextAgregadoMedico().setDisabled(Boolean.FALSE);
                getButtonGuardar().setDisabled(Boolean.TRUE);
            }
            
        } catch (HospitalizacionException e) {
            DatosPaciente pacienteTemp = new DatosPaciente();
            pacienteTemp.setNss(datosHospitalizacion.getDatosIngresoRN().getNss());
            datosHospitalizacion.setDatosPaciente(pacienteTemp);
            datosHospitalizacion.setDatosIngreso(new DatosIngreso());
            edad = BaseConstants.CADENA_VACIA;
            DatosIngresoRN datosIngresoRN = new DatosIngresoRN();
            datosIngresoRN.setNss(datosHospitalizacion.getDatosIngresoRN().getNss());
            datosHospitalizacion.setDatosIngresoRN(datosIngresoRN);
            limpiarCatalogos();
            deshabilitarCamposInicioSinAgregado();
            if (e.getClaves()[0].equals(MensajesErrorConstants.ME_001A)) {
                getButtonCancelar().setDisabled(Boolean.FALSE);
            }
            // obtenerEspecialidad();
            agregarMensajeError(e);
            tieneFoco = getTextAgregadoMedico().getClientId();
            getButtonGuardar().setDisabled(Boolean.TRUE);
        } catch (Exception e) {
            logger.error("Ha ocurrido un error: ", e);
        }
    }

    public void validarNombre() {

        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_4) && !madreConTococirugia) {
                datosPaciente.setNombre(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarNombreRN();
            tieneFoco = getTextApellidoPaterno().getClientId();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            tieneFoco = getTextNombre().getClientId();
        }
    }

    public void validarApellidoPaterno() {

        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_5) && !madreConTococirugia) {
                datosPaciente.setApellidoPaterno(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarApellidoPaternoRN();
            
            List<Ingreso> ingresosVigentes = capturarIngresoServices.validarIngresoVigenteRecienNacido(objetosSs.getDatosUsuario().getCvePresupuestal(),
            		getDatosHospitalizacion().getDatosIngresoRN());
            if(ingresosVigentes != null && !ingresosVigentes.isEmpty()){
	            	obtenerEspecialidadIngresoVig(ingresosVigentes);
            	if(ingresosVigentes.get(0).getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave()){
            		getDatosHospitalizacion().getDatosIngresoRN().setClavePaciente(ingresosVigentes.get(0).getCvePaciente());
            		getDatosHospitalizacion().getDatosIngresoRN().setCuneroVigente(Boolean.TRUE);
            		getDatosHospitalizacion().getDatosIngresoRN().setClaveIngresoPadre(ingresosVigentes.get(0).getCveIngreso());
            	}
            	
                if(ingresosVigentes.get(0).getCveEspecialidadIngreso()!= null 
                		&& !ingresosVigentes.get(0).getCveEspecialidadIngreso().isEmpty()){
                	habilitarEspHosp = Boolean.FALSE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
                	Especialidad especialidadHospitalizacion = capturarIngresoServices
                            .buscarEspecialidad(ingresosVigentes.get(0).getCveEspecialidadIngreso());
                	getAutoEspecialidadHospitalizacion().setValue(especialidadHospitalizacion.getCveEspecialidad()
                            .concat(BaseConstants.ESPACIO).concat(especialidadHospitalizacion.getDesEspecialidad()));
                	//getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
                }
               
            }else{
            	habilitarEspHosp = Boolean.TRUE;
            	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
            }
            tieneFoco = getTextApellidoMaterno().getClientId();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            for (String clave : e.getClaves()) {
				if(clave.equals(MensajesErrorConstants.ME_047)){
					limpiarIngresoVigente();
				}else{
					tieneFoco = getTextApellidoPaterno().getClientId();
				}
            }
        }
    }

    public void validarApellidoMaterno() {

        try {
            if (!madreConTococirugia && busquedaPorAgregado && madreExterna) {
                super.validarApellidoMaternoRN();
                tieneFoco = getTextNumero().getClientId();
            } else {
                super.validarApellidoMaternoRN();
                tieneFoco = getAutoEspecialidad().getClientId();
            }
            getTextNombre().setDisabled(Boolean.TRUE);
            getTextApellidoPaterno().setDisabled(Boolean.TRUE);
            getTextApellidoMaterno().setDisabled(Boolean.TRUE);
            
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            tieneFoco = getTextApellidoMaterno().getClientId();
        }
    }

    public void validarNumero() {

        getDatosHospitalizacion().getDatosPaciente()
                .setNumero(getDatosHospitalizacion().getDatosIngresoRN().getNumeroUnidad());
        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_6)) {
                datosHospitalizacion.getDatosPaciente().setNumero(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarNumero();
            getAutoDelegacion().setDisabled(Boolean.FALSE);
            tieneFoco = getAutoDelegacion().getClientId();
        } catch (HospitalizacionException e) {
            datosHospitalizacion.getDatosPaciente().setNumero(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            tieneFoco = getTextNumero().getClientId();
        }
    }

    public void mostrarRecienNacidoSeleccionado() {

        try {
            if (getNumeroPaciente() == null || getNumeroPaciente() > getRecienNacidos().size()
                    || getNumeroPaciente() == 0) {
                setNumeroPaciente(null);
                if(getTieneFoco().equals(getTextNumeroPaciente().getClientId())){
                	setTieneFoco(getTextNumeroPaciente().getClientId());
                }
            } else {
                getDatosHospitalizacion().setDatosIngresoRN(getRecienNacidos().get(getNumeroPaciente() - 1));
                if (madreConTococirugia) {
                    getCapturarIngresoServices().validarFechaAnterior(getFechaIngreso(),
                            getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso(),
                            getDatosHospitalizacion().getDatosIngresoRN().getFechaParto(), getTipoUbicacion(),
                            getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato());
                }
                setRecienNacidos(new ArrayList<DatosIngresoRN>());
                setCatalogoEspecialidad(
                        getCapturarIngresoServices().filtrarCatalogoEspecialidadRN(getCatalogoEspecialidad(),
                                getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato()));
                setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
                if(!getDatosHospitalizacion().getDatosIngresoRN().isAlojamientoPrevio() 
                		&& !getDatosHospitalizacion().getDatosIngresoRN().isCuneroVigente()){
                    getAutoEspecialidad().setValue(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO_CADENA);
                 }
                
                if(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso() != null 
                		&& !getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso().isEmpty()){
                	habilitarEspHosp = Boolean.FALSE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
                	Especialidad especialidadHospitalizacion = capturarIngresoServices
                            .buscarEspecialidad(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso());
                	getAutoEspecialidadHospitalizacion().setValue(especialidadHospitalizacion.getCveEspecialidad()
                            .concat(BaseConstants.ESPACIO).concat(especialidadHospitalizacion.getDesEspecialidad()));
                	
                }else{
                	habilitarEspHosp = Boolean.TRUE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
                }
                
                RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES);
                setTieneFoco(getAutoEspecialidad().getClientId());
                getTextNss().setDisabled(Boolean.TRUE);
            }
            setNumeroPaciente(null);
        } catch (HospitalizacionException e) {
            setFechaIngreso(null);
            getDatosHospitalizacion().setDatosIngresoRN(new DatosIngresoRN());
            setRecienNacidos(new ArrayList<DatosIngresoRN>());
            setNumeroPaciente(null);
            RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES);
            setTieneFoco(getMaskFecha().getClientId());
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    public void validarCamaRN() {

        try {
            getCapturarIngresoServices().validarCamaMadre(getDatosHospitalizacion().getDatosIngresoRN().getCamaRN());
            if (getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso()
                    .substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS)
                    .equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO.substring(BaseConstants.INDEX_CERO,
                            BaseConstants.INDEX_DOS))) {
                validarNss();
            }
            super.validarCamaRN();
            getDatosHospitalizacion().getDatosIngresoRN().setCamaRN(getDatosHospitalizacion().getDatosIngresoRN().getCamaRN().toUpperCase());
            setTieneFoco(getMaskHoraIngreso().getClientId());
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIngresoRN().setCamaRN(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextCamaRecienNacido().getClientId());
            agregarMensajeError(e);
            //getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    public void validarCama() {

        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_8)) {
                getDatosHospitalizacion().getDatosIngresoRN().setCama(BaseConstants.CADENA_VACIA);
                return;
            }
            getCapturarIngresoServices().validarCamaMadre(getDatosHospitalizacion().getDatosIngresoRN().getCama());
            setCamaMadre(getDatosHospitalizacion().getDatosIngresoRN().getCama().toUpperCase());
            getDatosHospitalizacion().getDatosIngresoRN().setCama(getDatosHospitalizacion().getDatosIngresoRN().getCama().toUpperCase());
            setRecienNacidos(getCapturarIngresoServices().buscarRecienNacidosCama(
                    getDatosHospitalizacion().getDatosIngresoRN().getCama(),
                    objetosSs.getDatosUsuario().getCvePresupuestal()));
            if (getRecienNacidos().isEmpty()) {
                agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_004), null);
                getTextAgregadoMedico().setDisabled(Boolean.FALSE);
                getTextNss().setDisabled(Boolean.TRUE);
                setTieneFoco(getTextAgregadoMedico().getClientId());
            } else if (getRecienNacidos().size() < 2) {
                getDatosHospitalizacion().setDatosIngresoRN(getRecienNacidos().get(0));
                getDatosHospitalizacion().getDatosIngresoRN().setCama(getCamaMadre());
                if (madreConTococirugia) {
                    getCapturarIngresoServices().validarFechaAnterior(getFechaIngreso(),
                            getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso(),
                            getDatosHospitalizacion().getDatosIngresoRN().getFechaParto(), getTipoUbicacion(),
                            getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato());
                }
                setCatalogoEspecialidad(
                        getCapturarIngresoServices().filtrarCatalogoEspecialidadRN(getCatalogoEspecialidad(),
                                getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato()));
                setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
                if(!getDatosHospitalizacion().getDatosIngresoRN().isAlojamientoPrevio() 
                		&& !getDatosHospitalizacion().getDatosIngresoRN().isCuneroVigente()){
                    getAutoEspecialidad().setValue(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO_CADENA);
                 }
               
                if(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso() != null
                		&& !getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso().isEmpty()){
                	habilitarEspHosp = Boolean.FALSE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
                	Especialidad especialidadHospitalizacion = capturarIngresoServices
                            .buscarEspecialidad(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso());
                	getAutoEspecialidadHospitalizacion().setValue(especialidadHospitalizacion.getCveEspecialidad()
                            .concat(BaseConstants.ESPACIO).concat(especialidadHospitalizacion.getDesEspecialidad()));
                	
                }else{
                	habilitarEspHosp = Boolean.TRUE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
                }
                
                setTieneFoco(getAutoEspecialidad().getClientId());
                getTextNss().setDisabled(Boolean.TRUE);
            } else {
                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_CAMA_PACIENTES);
                setTieneFoco(getTextNumeroCama().getClientId());
            }
            getButtonGuardar().setDisabled(Boolean.TRUE);
        } catch (HospitalizacionException e) {
            if (e.getArgs() != null) {
                getTextNss().setDisabled(Boolean.TRUE);
                getTextCama().setDisabled(Boolean.FALSE);
                setTieneFoco(getTextCama().getClientId());
            } else {
                setFechaIngreso(null);
                getDatosHospitalizacion().setDatosIngresoRN(new DatosIngresoRN());
                setTieneFoco(getMaskFecha().getClientId());
                getTextNss().setDisabled(Boolean.FALSE);
                getTextCama().setDisabled(Boolean.TRUE);
            }
            setCamaMadre(null);
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    public void mostrarRecienNacidoSeleccionadoCama() {

        try {
            if (getNumeroCama() == null || getNumeroCama() > getRecienNacidos().size() || getNumeroCama() == 0) {
                setNumeroCama(null);
                if(getTieneFoco().equals(getTextNumeroPaciente().getClientId())){
                	setTieneFoco(getTextNumeroPaciente().getClientId());
                }
            } else {
                getDatosHospitalizacion().setDatosIngresoRN(getRecienNacidos().get(getNumeroCama() - 1));
                getDatosHospitalizacion().getDatosIngresoRN().setCama(getCamaMadre());
                if (madreConTococirugia) {
                    getCapturarIngresoServices().validarFechaAnterior(getFechaIngreso(),
                            getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso(),
                            getDatosHospitalizacion().getDatosIngresoRN().getFechaParto(), getTipoUbicacion(),
                            getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato());
                }
                setRecienNacidos(new ArrayList<DatosIngresoRN>());
                setCatalogoEspecialidad(
                        getCapturarIngresoServices().filtrarCatalogoEspecialidadRN(getCatalogoEspecialidad(),
                                getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato()));
                setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
                if(!getDatosHospitalizacion().getDatosIngresoRN().isAlojamientoPrevio() 
                		&& !getDatosHospitalizacion().getDatosIngresoRN().isCuneroVigente()){
                    getAutoEspecialidad().setValue(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO_CADENA);
                 }
               
                if(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso() != null
                		&& !getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso().isEmpty()){
                	habilitarEspHosp = Boolean.FALSE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
                	Especialidad especialidadHospitalizacion = capturarIngresoServices
                            .buscarEspecialidad(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso());
                	getAutoEspecialidadHospitalizacion().setValue(especialidadHospitalizacion.getCveEspecialidad()
                            .concat(BaseConstants.ESPACIO).concat(especialidadHospitalizacion.getDesEspecialidad()));
                	
                }else{
                	habilitarEspHosp = Boolean.TRUE;
                	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
                }
                
                RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CAMA_PACIENTES);
                getTextNss().setDisabled(Boolean.TRUE);
                setTieneFoco(getAutoEspecialidad().getClientId());
               
            }
            setNumeroCama(null);
        } catch (HospitalizacionException e) {
            setFechaIngreso(null);
            getDatosHospitalizacion().setDatosIngresoRN(new DatosIngresoRN());
            setRecienNacidos(new ArrayList<DatosIngresoRN>());
            setNumeroCama(null);
            RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CAMA_PACIENTES);
            setTieneFoco(getMaskFecha().getClientId());
            getTextNss().setDisabled(Boolean.FALSE);
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }
    
    @Override
    public void validarDelegacion() {

        try {
            super.validarDelegacionRN();
            tieneFoco = getAutoEspecialidad().getClientId();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            tieneFoco = getAutoDelegacion().getClientId();
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    @Override
    public void validarEspecialidad() {

        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_7)) {
                getDatosHospitalizacion().getDatosIngresoRN().setHoraIngreso(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarEspecialidadRecienNacido();
            if (!madreConTococirugia && busquedaPorAgregado) {
                getTextAgregadoMedico().setDisabled(Boolean.TRUE);
                getTextCamaRecienNacido().setDisabled(Boolean.FALSE);
                setTieneFoco(getTextCamaRecienNacido().getClientId());
            } else if (!getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso()
                    .substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS)
                    .equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO.substring(BaseConstants.INDEX_CERO,
                            BaseConstants.INDEX_DOS))) {
                getTextCamaRecienNacido().setValue(BaseConstants.CADENA_VACIA);
                getTextCamaRecienNacido().setDisabled(Boolean.FALSE);
                setTieneFoco(getTextCamaRecienNacido().getClientId());
            } else {
            	capturarIngresoServices.validarCamaAlojamientoConjunto(getDatosHospitalizacion().getDatosIngresoRN().getNss(), 
            			getDatosHospitalizacion().getDatosIngresoRN().getClavePaciente(),
            			objetosSs.getDatosUsuario().getCvePresupuestal());
            	String camaPrefijo = BaseConstants.PREFIJO_CAMA_RN + getDatosHospitalizacion().getDatosIngresoRN().getCama();
            	//getTextCamaRecienNacido().setValue(getDatosHospitalizacion().getDatosIngresoRN().getCamaRN());
            	getTextCamaRecienNacido().setValue(camaPrefijo);
                getTextCamaRecienNacido().setDisabled(Boolean.TRUE);
                setTieneFoco(getMaskHoraIngreso().getClientId());
            }
            
            //MHLA
            if(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso()
                    .substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS)
                    .equals(BaseConstants.ESPECIALIDAD_UCI_NEONATOS.substring(BaseConstants.INDEX_CERO,
                            BaseConstants.INDEX_DOS)) && habilitarEspHosp){
            	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
            	setCatalogoEspecialidadHospitalizacion(getCapturarIngresoServices().filtrarCatalogoEspHospRN(getCatalogoEspecialidad()));
            	setCatalogoEspecialidadHospitalizacionString(convertirCatalogoString(getCatalogoEspecialidadHospitalizacion()));
            }else{
            	getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
            	setCatalogoEspecialidadHospitalizacion(null);
            	setCatalogoEspecialidadHospitalizacionString(null);
            }
            //MHLA
            
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIngresoRN().setEspecialidadIngreso(null);
            getDatosHospitalizacion().getDatosIngresoRN().setDivision(null);
            getAutoEspecialidad().resetValue();
            setCatalogoDivision(new ArrayList<DatosCatalogo>());
            agregarMensajeError(e);
            setTieneFoco(getAutoEspecialidad().getClientId());
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }
    
    @Override
    public void validarHoraIngreso() {

        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_10)) {
                getDatosHospitalizacion().getDatosIngresoRN().setHoraIngreso(BaseConstants.CADENA_VACIA);
                return;
            }
            getDatosHospitalizacion().getDatosIngreso()
                    .setHoraIngreso(getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso());
            super.validarHoraIngreso();
            if (madreConTococirugia && !busquedaPorAgregado) {
                getCapturarIngresoServices().validarFechaAnterior(getFechaIngreso(),
                        getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso(),
                        getDatosHospitalizacion().getDatosIngresoRN().getFechaParto(), getTipoUbicacion(),
                        getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato());
            }
            if(getAutoEspecialidadHospitalizacion().isDisabled()){
            	setTieneFoco(getTextMatricula().getClientId());
            }else{
            	setTieneFoco(getAutoEspecialidadHospitalizacion().getClientId());
            }
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosIngresoRN().setHoraIngreso(null);
            agregarMensajeError(e);
            setTieneFoco(getMaskHoraIngreso().getClientId());
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }
    
    @Override
    public void validarEspecialidadHospitalizacion() {
    	try{
    		super.validarEspecialidadHospRN();
            tieneFoco = getTextMatricula().getClientId();
    	}catch(HospitalizacionException e){
    		datosHospitalizacion.getDatosIngresoRN().setEspecialidadHospitalizacion(BaseConstants.CADENA_VACIA);
            getAutoEspecialidadHospitalizacion().resetValue();
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            tieneFoco = getAutoEspecialidadHospitalizacion().getClientId();
    	}
    }

    @Override
    public void validarMatricula() {

        try {
            if (!validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_11)) {
                getDatosHospitalizacion().getDatosMedico().setMatricula(BaseConstants.CADENA_VACIA);
                return;
            }
            setDelegacion(getDatosHospitalizacion().getDatosIngresoRN().getClaveDelegacion());
            super.validarMatricula();
            if (getDatosHospitalizacion().getDatosMedico().getIdentificadorConexion()
                    .equals(IdentificadorConexionEnum.ROJO.getRuta())) {
                agregarMensajeAdvertencia(getArray(MensajesAdvertenciaConstants.MA_013), null);
            }
            getDatosHospitalizacion().getDatosIngresoRN()
                    .setMatricula(getDatosHospitalizacion().getDatosMedico().getMatricula());
            getDatosHospitalizacion().getDatosIngresoRN()
                    .setMedico(getDatosHospitalizacion().getDatosMedico().getNombreCompleto());
            getButtonGuardar().setDisabled(Boolean.FALSE);
            setTieneFoco(getButtonGuardar().getClientId());
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().setDatosMedico(new DatosMedico());
            getButtonGuardar().setDisabled(Boolean.TRUE);
            agregarMensajeError(e);
            setTieneFoco(getTextMatricula().getClientId());
        } catch (Exception e) {
            logger.error("Ha ocurrido un error: ", e);
        }
    }
    
    public void validarCamaAlojamientoGuardar() throws HospitalizacionException{
    	
    }

    @Override
    public void guardar() {
        try {
        	if (validarCamposRequeridosRecienNacido(CapturarIngresosConstants.CAMPO_11)
        			&& validarFechaIngresoGuardado()  && validarCamaGuardado() && validarHoraIngresoGuardado()
        			&& validarMatriculaGuardado() ) {
        		
        		if(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso().substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS)
        		        .equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO.substring(BaseConstants.INDEX_CERO,BaseConstants.INDEX_DOS))){
        		    	capturarIngresoServices.validarCamaAlojamientoConjunto(getDatosHospitalizacion().getDatosIngresoRN().getNss(), 
        		    			getDatosHospitalizacion().getDatosIngresoRN().getClavePaciente(),
        		        		objetosSs.getDatosUsuario().getCvePresupuestal());
        		 }
        		
	            getDatosHospitalizacion().getDatosIngresoRN().setFechaIngreso(getFechaIngreso());
	            getDatosHospitalizacion().getDatosIngresoRN().setExtemporaneo(
	            		getDatosHospitalizacion().getDatosIngreso().isExtemporaneo());
	            getCapturarIngresoServices().guardarIngresoRecienNacido(getDatosHospitalizacion().getDatosIngresoRN(),
	                    objetosSs.getDatosUsuario());
	            agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
	            limpiar();
        	}
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            setTieneFoco(getButtonGuardar().getClientId());
            getButtonGuardar().setDisabled(Boolean.TRUE);
            
        }
    }
    
    public void limpiarIngresoVigente(){
    	 //setEdad(BaseConstants.CADENA_VACIA);
         setDelegacion(BaseConstants.CADENA_VACIA);
         setDatosHospitalizacion(new DatosHospitalizacion());
         getDatosHospitalizacion().getDatosIngresoRN().getFechaIngreso();
         getAutoEspecialidad().resetValue();
         getSelectDivision().resetValue();
         getAutoEspecialidadHospitalizacion().resetValue();
         setAutoEspecialidad(new AutoComplete());
         setSelectDivision(new SelectOneMenu());
         setAutoEspecialidadHospitalizacion(new AutoComplete());
         getTextNss().setDisabled(Boolean.FALSE);
         getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
         madreConTococirugia = Boolean.TRUE;
         madreExterna = Boolean.FALSE;
         busquedaPorAgregado = Boolean.FALSE;
         busquedaPorCama = Boolean.FALSE;   
         habilitarEspHosp = Boolean.FALSE;
         
         deshabilitarCamposIngresoVig();
         limpiarCatalogos();
         setCatalogoEspecialidad(getCapturarIngresoServices().obtenerCatalogoEspecialidad(getTipoUbicacion(),
                 objetosSs.getDatosUsuario().getCvePresupuestal()));
         setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
         setTieneFoco(getTextNss().getClientId());
    	
    }
    

    public void limpiar() {

        setFechaIngreso(BaseConstants.CADENA_VACIA);
        setEdad(BaseConstants.CADENA_VACIA);
        setDelegacion(BaseConstants.CADENA_VACIA);
        setDatosHospitalizacion(new DatosHospitalizacion());
        getAutoEspecialidad().resetValue();
        getSelectDivision().resetValue();
        getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
        getAutoEspecialidadHospitalizacion().resetValue();
        setAutoEspecialidad(new AutoComplete());
        setSelectDivision(new SelectOneMenu());
        setAutoEspecialidadHospitalizacion(new AutoComplete());
        getTextNss().setDisabled(Boolean.FALSE);
        madreConTococirugia = Boolean.TRUE;
        madreExterna = Boolean.FALSE;
        busquedaPorAgregado = Boolean.FALSE;
        busquedaPorCama = Boolean.FALSE;   
        habilitarEspHosp = Boolean.FALSE;
        deshabilitarCamposInicio();
        limpiarCatalogos();
        setCatalogoEspecialidad(getCapturarIngresoServices().obtenerCatalogoEspecialidad(getTipoUbicacion(),
                objetosSs.getDatosUsuario().getCvePresupuestal()));
        setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
        setTieneFoco(getMaskFecha().getClientId());
    }

    @Override
    public void deshabilitarCamposInicio() {

        super.deshabilitarCamposInicio();
        getTextCama().setDisabled(Boolean.TRUE);
        getTextCamaRecienNacido().setDisabled(Boolean.TRUE);
        getTextAgregadoMedico().setDisabled(Boolean.TRUE);
        getTextDelegacion().setDisabled(Boolean.TRUE);
    }
    
    public void deshabilitarCamposIngresoVig() {
    	getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getTextNumero().setDisabled(Boolean.TRUE);
        getAutoDelegacion().setDisabled(Boolean.TRUE);
        getTextClave().setDisabled(Boolean.TRUE);
        getTextDescripcion().setDisabled(Boolean.TRUE);
        getSelectDivision().setDisabled(Boolean.TRUE);
        //getButtonCancelar().setDisabled(Boolean.FALSE);
        getTextCama().setDisabled(Boolean.TRUE);
        getTextCamaRecienNacido().setDisabled(Boolean.TRUE);
        getTextAgregadoMedico().setDisabled(Boolean.TRUE);
        getTextDelegacion().setDisabled(Boolean.TRUE);
        //getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
    }

    public void deshabilitarCamposInicioSinAgregado() {

        super.deshabilitarCamposInicio();
        getTextCama().setDisabled(Boolean.TRUE);
        getTextCamaRecienNacido().setDisabled(Boolean.TRUE);
        getTextDelegacion().setDisabled(Boolean.TRUE);
    }

    protected void habiltaCamposNombre() {

        getTextNombre().setDisabled(Boolean.FALSE);
        getTextApellidoPaterno().setDisabled(Boolean.FALSE);
        getTextApellidoMaterno().setDisabled(Boolean.FALSE);
    }

    public boolean validarCamposRequeridosRecienNacido(int campo) {

        if (CapturarIngresosConstants.CAMPO_1 <= campo && !validarRequeridoFechaIngreso()) {
            return false;
        }
        if (CapturarIngresosConstants.CAMPO_2 <= campo && !busquedaPorCama && !validarRequeridoNssRN()) {
            return false;
        }
        if (CapturarIngresosConstants.CAMPO_3 <= campo && !busquedaPorCama && !madreConTococirugia
                && !validarRequeridoAgregadoMedicoRN()) {
            return false;
        }
        if (CapturarIngresosConstants.CAMPO_4 <= campo && !busquedaPorCama && !validarRequeridoNombre()) {
            return false;
        }
        if (CapturarIngresosConstants.CAMPO_5 <= campo && !busquedaPorCama && !validarRequeridoApellidoPaterno()) {
            return false;
        }
        if (CapturarIngresosConstants.CAMPO_6 <= campo && !busquedaPorCama && !madreConTococirugia
                && !validarRequeridoNumero()) {
            return false;
        }
        // if (CapturarIngresosConstants.CAMPO_7 <= campo && !validarRequeridoEspecialidadRN() && !madreConTococirugia)
        // {
        // return false;
        // }
        if (CapturarIngresosConstants.CAMPO_8 <= campo && madreConTococirugia && !validarRequeridoCamaRN()) {
            return false;
        }
        if (CapturarIngresosConstants.CAMPO_9 <= campo && !madreConTococirugia && !validarRequeridoCamaRN2()) {
            return false;
        }
        if (CapturarIngresosConstants.CAMPO_10 <= campo && !validarRequeridoHoraIngresoRN()) {
            return false;
        }
        if (CapturarIngresosConstants.CAMPO_11 <= campo && !madreConTococirugia && !validarRequeridoMatricula()) {
            return false;
        }
        return true;
    }

    private boolean validarRequeridoNumero() {

        if (getDatosHospitalizacion().getDatosIngresoRN().getNumeroUnidad() == null
                || getDatosHospitalizacion().getDatosIngresoRN().getNumeroUnidad().isEmpty()) {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A), getArray(BaseConstants.REQUERIDO_NUMERO));
            setTieneFoco(getTextNumero().getClientId());
            return false;
        }
        return true;
    }

    private boolean validarRequeridoNssRN() {

        if (getDatosHospitalizacion().getDatosIngresoRN().getNss() == null
                || getDatosHospitalizacion().getDatosIngresoRN().getNss().isEmpty()) {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A), getArray(BaseConstants.REQUERIDO_NSS));
            setTieneFoco(getTextNss().getClientId());
            return false;
        }
        return true;
    }

    private boolean validarRequeridoAgregadoMedicoRN() {

        if (getDatosHospitalizacion().getDatosIngresoRN().getAgregadoMedico() == null
                || getDatosHospitalizacion().getDatosIngresoRN().getAgregadoMedico().isEmpty()) {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_AGREGADO_MEDICO));
            setTieneFoco(getTextAgregadoMedico().getClientId());
            return false;
        }
        return true;
    }

    private boolean validarRequeridoNombre() {

        if (getDatosHospitalizacion().getDatosIngresoRN().getNombre() == null
                || getDatosHospitalizacion().getDatosIngresoRN().getNombre().isEmpty()) {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A), getArray(BaseConstants.REQUERIDO_NOMBRE));
            setTieneFoco(getTextNombre().getClientId());
            return false;
        }
        return true;
    }

    private boolean validarRequeridoApellidoPaterno() {

        if (getDatosHospitalizacion().getDatosIngresoRN().getApellidoPaterno() == null
                || getDatosHospitalizacion().getDatosIngresoRN().getApellidoPaterno().isEmpty()) {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_APELLIDO_PATERNO));
            setTieneFoco(getTextApellidoPaterno().getClientId());
            return false;
        }
        return true;
    }

    private boolean validarRequeridoCamaRN() {

        if ((getDatosHospitalizacion().getDatosIngresoRN().getCama() == null
                || getDatosHospitalizacion().getDatosIngresoRN().getCama().isEmpty())
                && (getDatosHospitalizacion().getDatosIngresoRN()
                        .getClaveTipoFormato() != TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()
                        && getDatosHospitalizacion().getDatosIngresoRN()
                                .getClaveTipoFormato() != TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave()
                                        .intValue())) {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A), getArray(BaseConstants.REQUERIDO_CAMA));
            setTieneFoco(getTextCama().getClientId());
            return false;
        }
        return true;
    }

    private boolean validarRequeridoCamaRN2() {

        if (getDatosHospitalizacion().getDatosIngresoRN().getCamaRN() == null
                || getDatosHospitalizacion().getDatosIngresoRN().getCamaRN().isEmpty()) {
        	agregarMensajeError(getArray(MensajesErrorConstants.ME_001A), getArray(BaseConstants.REQUERIDO_CAMA));
            setTieneFoco(getTextCamaRecienNacido().getClientId());
            return false;
        }
        return true;
    }

    private boolean validarRequeridoHoraIngresoRN() {

        if (getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso() == null
                || getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso().isEmpty()) {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_HORA_INGRESO));
            setTieneFoco(getMaskHoraIngreso().getClientId());
            return false;
        }
        return true;
    }

    public String getCamaMadre() {

        return camaMadre;
    }

    public void setCamaMadre(String camaMadre) {

        this.camaMadre = camaMadre;
    }
    
    private boolean validarCamaGuardado(){
    	if (madreConTococirugia) {
    		if(!validarRequeridoCamaRN() || !validarRequeridoCamaRN2()){
    			getButtonGuardar().setDisabled(Boolean.TRUE);
    			return false;
    		}
    		logger.debug("validacionGuardado");
    		logger.debug(getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso());
    		if(!getDatosHospitalizacion().getDatosIngresoRN().getEspecialidadIngreso()
                    .substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS)
                    .equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO.substring(BaseConstants.INDEX_CERO,
                            BaseConstants.INDEX_DOS))
               && !validarCamaRNGuardado()){
    			getButtonGuardar().setDisabled(Boolean.TRUE);
    			return false;
    		}
        }
        if (!madreConTococirugia && (!validarRequeridoCamaRN2() || !validarCamaRNGuardado())) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            return false;
        }
        return true;
    }
    
    public boolean validarCamaRNGuardado() {
        try {
            getCapturarIngresoServices().validarCamaMadre(getDatosHospitalizacion().getDatosIngresoRN().getCamaRN());
            datosHospitalizacion.getDatosIngreso()
            .setCama(datosHospitalizacion.getDatosIngresoRN().getCamaRN().toUpperCase());
            
            getDatosHospitalizacion().getDatosIngresoRN().setCamaRN(getDatosHospitalizacion().getDatosIngresoRN().getCamaRN().toUpperCase());
            return true;
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            setTieneFoco(getTextCamaRecienNacido().getClientId());
            agregarMensajeError(e);
            return false;
        }
    }
    
    public boolean validarHoraIngresoGuardado() {
        try {
            getDatosHospitalizacion().getDatosIngreso()
                    .setHoraIngreso(getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso());
            super.validarHoraIngreso();
            if (madreConTococirugia && !busquedaPorAgregado) {
                getCapturarIngresoServices().validarFechaAnterior(getFechaIngreso(),
                        getDatosHospitalizacion().getDatosIngresoRN().getHoraIngreso(),
                        getDatosHospitalizacion().getDatosIngresoRN().getFechaParto(), getTipoUbicacion(),
                        getDatosHospitalizacion().getDatosIngresoRN().getClaveTipoFormato());
            }
            return true;
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            getDatosHospitalizacion().getDatosIngresoRN().setHoraIngreso(null);
            agregarMensajeError(e);
            setTieneFoco(getMaskHoraIngreso().getClientId());
            return false;
        }
    }
    
    
    public boolean validarFechaIngresoGuardado() {
        try {
            super.validarFechaIngreso();
            return true;
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            setFechaIngreso(CapturarIngresosConstants.FECHA_VACIA);
            agregarMensajeError(e);
            setTieneFoco(getMaskFecha().getClientId());
            return false;
        }
    }
    
    public boolean validarMatriculaGuardado() {
        try {
            super.validarMatricula();
           return true;
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            getDatosHospitalizacion().setDatosMedico(new DatosMedico());
            agregarMensajeError(e);
            setTieneFoco(getTextMatricula().getClientId());
            return false;
        } catch (Exception e) {
            logger.error("Ha ocurrido un error: ", e);
            return false;
        }
    }

}
