package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoRegimenAgregadoMedicoEnum {
	
	ORDINARIO("OR", "Ordinario"),
	TRABAJADORES("ET", "Trabajadores eventuales y temporales"),
	ESTACIONALES("EC", "Estacionales de campo"),
	ESTUDIANTE("ES", "Estudiante"),
	NO_DERECHOHABIENTE("ND", "No derechohabiente"),
	SEGURO_FAMILIA("SF", "Seguro de familia"),
	SEGURO_FACULTATIVO("SA", "Seguro facultativo"),
	PENSIONADO("PE", "Pensionado"),
	EXTRANJERO("ME","Seguro de salud para la familia de mexicano radicado en el extranjero");
	
	private String clave;
	private String descripcion;
	
	private TipoRegimenAgregadoMedicoEnum(String clave, String descripcion) {
		this.clave = clave;
		this.descripcion = descripcion;
	}

	public String getClave() {
		return clave;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public static TipoRegimenAgregadoMedicoEnum parse(String clave) {
		TipoRegimenAgregadoMedicoEnum right = null;
		for (TipoRegimenAgregadoMedicoEnum item : values()) {
			if (item.getClave().equals(clave)) {
				right = item;
				break;
			}
		}
		return right;
	}

}