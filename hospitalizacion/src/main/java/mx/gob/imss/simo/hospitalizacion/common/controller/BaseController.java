package mx.gob.imss.simo.hospitalizacion.common.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;

@Data
public class BaseController implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final Logger logger = Logger.getLogger(getClass());

    private ResourceBundle bundleHospitalizacion;
    private Locale locale;
    private String fechaEgreso;
    private Integer numeroPaciente;
    private Integer numeroCama;
    private Integer numeroCantidad;
    private String subtitulo;
    protected String tieneFoco;
    private TipoUbicacionEnum tipoUbicacion;

    protected DatosHospitalizacion datosHospitalizacion;
    private List<DatosPaciente> listaDatosPaciente;
    private List<DatosHospitalizacion> listDatosHospitalizacion;

    private List<DatosCatalogo> catalogoDelegacion;
    private List<DatosCatalogo> catalogoDivision;
    private List<DatosCatalogo> catalogoEspecialidad;
    private List<DatosCatalogo> catalogoEspecialidadHospitalizacion;
    private List<String> catalogoDelegacionString;
    private List<String> catalogoEspecialidadString;
    private List<String> catalogoEspecialidadHospitalizacionString;

    private String banderaModalPaciente;
    private String banderaModalCama;
    private String banderaModalCantidad;

    private CommandButton buttonCancelar;
    private CommandButton buttonEditar;
    private CommandButton buttonGuardar;
    private CommandButton buttonSalir;

    @PostConstruct
    public void init() {

        locale = new Locale(
                ResourceBundle.getBundle(BaseConstants.BUNDLE_LOCALE).getString(BaseConstants.IDIOMA_LOCALE),
                ResourceBundle.getBundle(BaseConstants.BUNDLE_LOCALE).getString(BaseConstants.PAIS_LOCALE));
        bundleHospitalizacion = ResourceBundle.getBundle(BaseConstants.BUNDLE_MESSAGES, getLocale());
        datosHospitalizacion = new DatosHospitalizacion();
        listaDatosPaciente = new ArrayList<>();
        catalogoDelegacion = new ArrayList<>();
        catalogoDivision = new ArrayList<>();
        catalogoEspecialidad = new ArrayList<>();
        catalogoEspecialidadHospitalizacion = new ArrayList<>();
        buttonCancelar = new CommandButton();
        buttonEditar = new CommandButton();
        buttonGuardar = new CommandButton();
        buttonSalir = new CommandButton();
        catalogoDelegacionString = new ArrayList<>();
        catalogoEspecialidadString = new ArrayList<>();
        catalogoEspecialidadHospitalizacionString = new ArrayList<String>();
    }

    public String[] getArray(String... strings) {

        return strings;
    }

    public String obtenerMensaje(String clave) {

        return getBundleHospitalizacion().getString(clave);
    }

    public void agregarMensajeError(String[] claves, String[] args) {

        if (args == null) {
            for (String clave : claves) {
                String mensaje = obtenerMensaje(clave);
                getContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, BaseConstants.CADENA_VACIA));
            }
        } else {
            String mensaje = String.format(obtenerMensaje(claves[0]), (Object[]) args);
            getContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, BaseConstants.CADENA_VACIA));
        }
    }

    public void agregarMensajeAdvertencia(String[] claves, String[] args) {

        if (args == null) {
            for (String clave : claves) {
                String mensaje = obtenerMensaje(clave);
                getContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, mensaje, BaseConstants.CADENA_VACIA));
            }
        } else {
            String mensaje = String.format(obtenerMensaje(claves[0]), (Object[]) args);
            getContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN, mensaje, BaseConstants.CADENA_VACIA));
        }
    }

    public void agregarMensajeError(HospitalizacionException e) {

        String[] claves = e.getClaves();
        String[] args = e.getArgs();
        if (claves != null) {
            if (args == null) {
                for (String clave : claves) {
                    String mensaje = obtenerMensaje(clave);
                    getContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, BaseConstants.CADENA_VACIA));
                }
            } else {
                String mensaje = String.format(obtenerMensaje(claves[0]), (Object[]) args);
                getContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, BaseConstants.CADENA_VACIA));
            }
        }
    }

    public void agregarMensajeInformativo(String[] claves, String[] args) {

        if (args == null) {
            for (String clave : claves) {
                String mensaje = obtenerMensaje(clave);
                getContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, mensaje, BaseConstants.CADENA_VACIA));
            }
        } else {
            String mensaje = String.format(obtenerMensaje(claves[0]), (Object[]) args);
            getContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, mensaje, BaseConstants.CADENA_VACIA));
        }
    }

    public String descripcionMes(Integer mes) {

        String[] mesArray = new String[] { "", "ENERO", "FEBERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO",
                "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" };
        if (mes >= 1 && mes <= 12) {
            return mesArray[mes];
        }
        return "ERROR";
    }

    public List<String> convertirCatalogoString(List<DatosCatalogo> datosCatalogo) {

        List<String> datos = new ArrayList<>();
        for (DatosCatalogo dato : datosCatalogo) {
            datos.add(dato.getClave().concat(BaseConstants.ESPACIO.concat(dato.getDescripcion())));
        }
        return datos;
    }

    public DatosCatalogo validaAutoComplete(AutoComplete componente, Integer max, List<String> listaString,
            List<DatosCatalogo> listaCatalogo) {

        DatosCatalogo objeto = null;
        String textoIngresado;
        //logger.info("en valdia autocomplete");
        if (componente.getValue() != null) {
            textoIngresado = componente.getValue().toString();            
            if(StringUtils.isNotEmpty(textoIngresado) && textoIngresado.length() >= max){
            		List<String> resultado = new ArrayList<>();
                    String cadena;
                    Iterator<String> it = listaString.iterator();
                    while (it.hasNext()) {
                        cadena = it.next();
                        if (StringUtils.containsIgnoreCase(cadena,  textoIngresado)) {
                            resultado.add(cadena);                             
                        }
                    }
                    if (!resultado.isEmpty()) {
                        componente.setValue(resultado.get(0));
                        int index;
                        if (componente.getValue() != null) {
                            index = listaString.indexOf(componente.getValue().toString());
                            objeto = listaCatalogo.get(index);                            
                        }
                    } else {
                        componente.setValue(null);
                    }
            	}/*else{
            		 List<String> resultado = new ArrayList<>();
                     String cadena;
                     Iterator<String> it = listaString.iterator();
                     while (it.hasNext()) {
                         cadena = it.next();
                         if (StringUtils.containsIgnoreCase(cadena, textoIngresado)) {
                             resultado.add(cadena); 
                             
                         }
                     }
                     if (!resultado.isEmpty()) {
                    	 componente.setValue(resultado.get(0));
                         int index;
                         if (componente.getValue() != null) {
                             index = listaString.indexOf(componente.getValue().toString());
                             objeto = listaCatalogo.get(index);                            
                         }
                     } else {
                         componente.setValue(null);
                     }
            	}*/
        }
        return objeto;
    }
    
    public DatosCatalogo validaAutoCompleteProcesosRN(AutoComplete componente, Integer max, List<String> listaString,
            List<DatosCatalogo> listaCatalogo){
    	DatosCatalogo objeto = null;
        String textoIngresado;
        if(componente.getValue()!= null){
        	textoIngresado = componente.getValue().toString();
        	if(StringUtils.isNotEmpty(textoIngresado)){
        		if(textoIngresado.length() >= max){        			
        			List<String> resultado = new ArrayList<>();
                    String cadena;
                    Iterator<String> it = listaString.iterator();
                    while (it.hasNext()) {
                        cadena = it.next();                        
                        if (StringUtils.containsAny(cadena,  textoIngresado)) {                        	
                            resultado.add(cadena);                             
                        }
                    }
                    if (!resultado.isEmpty()) {
                    	int indiceComponente= -1;
                    	//Se tomaran los digitos de mas a la izqueirda que corresponden al id del selectItem que se necesita                    	
                    	for(String res: resultado){                    		
                    		if(StringUtils.left(res, max).trim().equals(textoIngresado.trim()) || res.trim().equals(textoIngresado.trim())){ 
                    			indiceComponente = resultado.indexOf(res);                    			
                    		}
                    	}
                    	if(indiceComponente > -1){
                    		 componente.setValue(resultado.get(indiceComponente));
                    		 int index;
                             if (componente.getValue() != null) {
                                 index = listaString.indexOf(componente.getValue().toString());
                                 objeto = listaCatalogo.get(index);                                  
                             }
                    	}                       
                        
                    } else {
                        componente.setValue(null);
                    }
        		}else{        			
        			List<String> resultado = new ArrayList<>();
                    String cadena;
                    Iterator<String> it = listaString.iterator();
                    while (it.hasNext()) {
                        cadena = it.next();
                        if (StringUtils.containsAny(cadena,  textoIngresado)) {
                            resultado.add(cadena);                             
                        }
                    }
                    if (!resultado.isEmpty()) {
                    	int indiceComponente= -1;
                    	//Se tomaran los digitos de mas a la izqueirda que corresponden al id del selectItem que se necesita                    	
                    	for(String res: resultado){                    		
                    		if(StringUtils.left(res, max).trim().equals(textoIngresado.trim()) || res.trim().equals(textoIngresado.trim())){ 
                    			indiceComponente = resultado.indexOf(res);
                    		}
                    	}
                    	if(indiceComponente > -1){
                    		 componente.setValue(resultado.get(indiceComponente));
                    		 int index;
                             if (componente.getValue() != null) {
                                 index = listaString.indexOf(componente.getValue().toString());
                                 objeto = listaCatalogo.get(index);                                 
                             }
                    	}                       
                        
                    } else {
                        componente.setValue(null);                        
                    }
        		}
            }
        }
        
        
        return objeto;
    }

    public FacesContext getContext() {

        return FacesContext.getCurrentInstance();
    }

    public ResourceBundle getBundleHospitalizacion() {

        return bundleHospitalizacion;
    }

    public Locale getLocale() {

        return locale;
    }

    public String getFechaEgreso() {

        return fechaEgreso;
    }

    public void setFechaEgreso(String fechaEgreso) {

        this.fechaEgreso = fechaEgreso;
    }

    public Integer getNumeroPaciente() {

        return numeroPaciente;
    }

    public void setNumeroPaciente(Integer numeroPaciente) {

        this.numeroPaciente = numeroPaciente;
    }

    public Integer getNumeroCama() {

        return numeroCama;
    }

    public void setNumeroCama(Integer numeroCama) {

        this.numeroCama = numeroCama;
    }

    public Integer getNumeroCantidad() {

        return numeroCantidad;
    }

    public void setNumeroCantidad(Integer numeroCantidad) {

        this.numeroCantidad = numeroCantidad;
    }

    public String getSubtitulo() {

        return subtitulo;
    }

    public void setSubtitulo(String subtitulo) {

        this.subtitulo = subtitulo;
    }

    public String getTieneFoco() {

        return tieneFoco;
    }

    public void setTieneFoco(String tieneFoco) {

        this.tieneFoco = tieneFoco;
    }

    public TipoUbicacionEnum getTipoUbicacion() {

        return tipoUbicacion;
    }

    public void setTipoUbicacion(TipoUbicacionEnum tipoUbicacion) {

        this.tipoUbicacion = tipoUbicacion;
    }

    public DatosHospitalizacion getDatosHospitalizacion() {

        return datosHospitalizacion;
    }

    public void setDatosHospitalizacion(DatosHospitalizacion datosHospitalizacion) {

        this.datosHospitalizacion = datosHospitalizacion;
    }

    public List<DatosPaciente> getListaDatosPaciente() {

        return listaDatosPaciente;
    }

    public void setListaDatosPaciente(List<DatosPaciente> listaDatosPaciente) {

        this.listaDatosPaciente = listaDatosPaciente;
    }

    public List<DatosCatalogo> getCatalogoDelegacion() {

        return catalogoDelegacion;
    }

    public void setCatalogoDelegacion(List<DatosCatalogo> catalogoDelegacion) {

        this.catalogoDelegacion = catalogoDelegacion;
    }

    public List<DatosCatalogo> getCatalogoDivision() {

        return catalogoDivision;
    }

    public void setCatalogoDivision(List<DatosCatalogo> catalogoDivision) {

        this.catalogoDivision = catalogoDivision;
    }

    public List<DatosCatalogo> getCatalogoEspecialidad() {

        return catalogoEspecialidad;
    }

    public void setCatalogoEspecialidad(List<DatosCatalogo> catalogoEspecialidad) {

        this.catalogoEspecialidad = catalogoEspecialidad;
    }

    public List<DatosCatalogo> getCatalogoEspecialidadHospitalizacion() {

        return catalogoEspecialidadHospitalizacion;
    }

    public void setCatalogoEspecialidadHospitalizacion(List<DatosCatalogo> catalogoEspecialidadHospitalizacion) {

        this.catalogoEspecialidadHospitalizacion = catalogoEspecialidadHospitalizacion;
    }

    public List<DatosHospitalizacion> getListDatosHospitalizacion() {

        return listDatosHospitalizacion;
    }

    public void setListDatosHospitalizacion(List<DatosHospitalizacion> listDatosHospitalizacion) {

        this.listDatosHospitalizacion = listDatosHospitalizacion;
    }

    public String getBanderaModalCama() {

        return banderaModalCama;
    }

    public void setBanderaModalCama(String banderaModalCama) {

        this.banderaModalCama = banderaModalCama;
    }

    public String getBanderaModalPaciente() {

        return banderaModalPaciente;
    }

    public void setBanderaModalPaciente(String banderaModalPaciente) {

        this.banderaModalPaciente = banderaModalPaciente;
    }

    public String getBanderaModalCantidad() {

        return banderaModalCantidad;
    }

    public void setBanderaModalCantidad(String banderaModalCantidad) {

        this.banderaModalCantidad = banderaModalCantidad;
    }

    public CommandButton getButtonCancelar() {

        return buttonCancelar;
    }

    public void setButtonCancelar(CommandButton buttonCancelar) {

        this.buttonCancelar = buttonCancelar;
    }

    public CommandButton getButtonEditar() {

        return buttonEditar;
    }

    public void setButtonEditar(CommandButton buttonEditar) {

        this.buttonEditar = buttonEditar;
    }

    public CommandButton getButtonGuardar() {

        return buttonGuardar;
    }

    public void setButtonGuardar(CommandButton buttonGuardar) {

        this.buttonGuardar = buttonGuardar;
    }

    public CommandButton getButtonSalir() {

        return buttonSalir;
    }

    public void setButtonSalir(CommandButton buttonSalir) {

        this.buttonSalir = buttonSalir;
    }

}
