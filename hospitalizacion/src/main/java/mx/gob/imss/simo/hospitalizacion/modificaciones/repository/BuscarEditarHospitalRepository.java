package mx.gob.imss.simo.hospitalizacion.modificaciones.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosEgreso;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosTococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.Diagnostico;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoDiagnosticoEdicion;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientoCIE9;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientosEdicion;
import mx.gob.imss.simo.hospitalizacion.common.model.TococirugiaEdicion;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;
import mx.gob.imss.simo.model.DatosUsuario;

public interface BuscarEditarHospitalRepository extends HospitalizacionCommonRepository {

    List<Diagnostico> obtenerCatalogoDeDiagnosticos(String claveDiagostico);

    void guardarEdicionHospitalizacion(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario);

    Parametro obtenerParametroTiempoEdicion(String perfil);

    List<ProcedimientosEdicion> buscarProcedimientosParaEdicion(long claveIngreso);

    IntervencionesQuirurgicas obtenerIntervencionesQx(Long claveIngreso);

    List<TococirugiaEdicion> obtenerTococirugiaPaciente(Long claveIngreso);

    List<EgresoDiagnosticoEdicion> buscarEgresoDiagnosticoEdicion(Long claveIngreso);

    /**
     * Metodo que actualiza la clave del procedimiento <code>CVE_CIE9</code>
     * en la tabla <code>SIT_PROCEDIMIENTO_CIE9</code>
     * 
     * @param procedimientoCIE9
     */
    void actualizarProcedimientosCIE9(ProcedimientoCIE9 procedimientoCIE9);

    /**
     * Metodo que actualiza el nombre del usuario <code>CVE_CAPTURISTA_ACTUALIZA</code>
     * y la fecha de actualización <code>FEC_ACTUALIZACION</code>
     * en la tabla <code>SIT_INTERVENCION_QUIRURGICA</code>
     * 
     * @param datosUsuario
     * @param datosHospitalizacion
     */
    void actualizarIntervencionQuirurgica(DatosUsuario datosUsuario, DatosHospitalizacion datosHospitalizacion);

    /**
     * Metodo que actualiza la talla <code>NUM_TALLA</code>
     * el peso <code>NUM_PESO</code> para el recien nacido
     * en la tabla <code>SIT_RECIEN_NACIDO</code>
     * 
     * @param datosHospitalizacion
     * @param datosRecienNacido
     */
    void actualizarRecienNacido(DatosHospitalizacion datosHospitalizacion, DatosRecienNacido datosRecienNacido);

    /**
     * Metodo que actualiza la fecha de actualizacion <code>FEC_ACTUALIZACION</code>
     * el nombre del usuario <code>CVE_CAPTURISTA_ACTUALIZA</code>
     * de la tabla <code>SIT_TOCOCIRUGIA</code>
     * 
     * @param datosTococirugia
     * @param datosPaciente
     * @param datosUsuario
     */
    void actualizarTocoCirugia(DatosTococirugia datosTococirugia, DatosPaciente datosPaciente,
            DatosUsuario datosUsuario);

    /**
     * Metodo que actualiza el sexo <code>CVE_SEXO</code>
     * de la tabla <code>SIC_PACIENTE</code>
     * 
     * @param datosPaciente
     */
    void actualizarPaciente(DatosRecienNacido datosRecienNacido);

    /**
     * Metodo que actualiza el motivo de alta <code>CVE_MOTIVO_ALTA</code>
     * la fecha de actualizacion <code>FEC_ACTUALIZACION</code>
     * y el nombre del usuario que esta actualizando la informacion <code>CVE_CAPTURISTA_ACTUALIZA</code>
     * de la tabla <code>SIT_EGRESO</code>
     * 
     * @param datosEgreso
     * @param datosPaciente
     * @param datosUsuario
     */
    void actualizarEgreso(DatosEgreso datosEgreso, DatosPaciente datosPaciente, DatosUsuario datosUsuario);

    /**
     * Metodo que actualiza la clave del diagnostico <code>CVE_CIE10</code>
     * de la tabla <code>SIT_DIAGNOSTICO</code>
     * 
     * @param string
     * @param long1
     * @param string2
     */
    void actualizarDiagnosticoEgresoBusquedaActualizacion(String cveInicialIngresoEgreso, Long claveIngreso,
            String clave);
    
    boolean buscarIngresoRN(Long cvePaciente);

    List<EgresoRecienNacido> buscarEgresosRN(Long cveIngreso);

    MovimientoIntraHospitalario buscarMovimientos(Long claveIngreso);

}
