package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class PeriodoOperacion {

    private String clavePresupuestal;
    private int tipoCaptura;
    private String clavePeriodoIMSS;
    private boolean indicadorCerrado;
    private Date fechaCierre;
    private boolean indicadorActual;
    private boolean indicadorMonitoreo;

}
