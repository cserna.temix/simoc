package mx.gob.imss.simo.hospitalizacion.common.repository;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoReporte;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Diagnostico;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.EspecialidadUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.model.MetodoAnticonceptivo;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCama;
import mx.gob.imss.simo.hospitalizacion.common.model.OcupacionCamaAlojamiento;
import mx.gob.imss.simo.hospitalizacion.common.model.PacienteModal;
import mx.gob.imss.simo.hospitalizacion.common.model.PacientesHospitalizados;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientoRealizado;
import mx.gob.imss.simo.hospitalizacion.common.model.ReporteGenerado;
import mx.gob.imss.simo.hospitalizacion.common.model.SalaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.TotalCamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;

public interface HospitalizacionCommonRepository extends BaseRepository {

    void actualizarOcupacionCama(CamaUnidad camaUnidad, int numPacientes);

    void guardarOcupacionCama(CamaUnidad camaUnidad);
    
    void guardarOcupacionCamaAlojamiento(TotalCamaUnidad camaUnidad, Date fecCaptura);
    
    void actualizaOcupacionCamaAlojamiento(int ocupacionCama, Date fecCaptura, TotalCamaUnidad camaUnidad);

    void eliminarCamaIngresoVigente(Long ingresoVigente);

    void registrarEventoBitacoraCama(CamaUnidad camaUnidad, TipoFormatoEnum tipoFormto, Date fechaAtencion,
            boolean evento);

    List<CamaUnidad> buscarCamaUnidad(String clavePresupuestal, String claveCama, boolean cirugiaAmbulatoria);

    List<CamaUnidad> buscarCamaUnidad(String clavePresupuestal, String claveCama, String claveEspecialidad);
    
    List<CamaUnidad> buscarCamaUnidadRN(String clavePresupuestal, String claveCama);

    Especialidad buscarEspecialidad(String claveEspecialidad);

    EspecialidadUnidad buscarEspecialidadUnidad(String claveEspecialidad, String clavePresupuestal);

    List<OcupacionCama> buscarOcupacionCama(String claveCama, String clavePresupuestal, String claveEspecialidad);

    List<OcupacionCama> buscarOcupacionCamaPorPresupuestal(String clavePresupuestal);

    UnidadMedica buscarUnidadMedica(String numeroUnidad, String claveDelegacion);

    List<PacienteModal> buscarPacientesPorNumeroCama(String numeroCama, String cvePresupuestal, String ubicacion);

    List<SalaUnidad> buscarSalasUnidad(String cvePresupuestal, String cveSala);

    List<PacienteModal> buscarPacientesNss(String nss, String cveTipoUbicacion, String cveAgregadoMedico,
            String cvePresupuestal);

    PeriodosImss buscarPeriodoPorMesAnio(Integer numAnioPeriodo, String numMes);

    Diagnostico buscarDisagnostico(String cveDiagnostico);

    List<PeriodosImss> buscarPeriodoPorMesAnio(String numMes, String numAnioPeriodo);

    List<PeriodosImss> buscarPeriodoPorFechaAtencion(String fechaAtencion);
    
    List<OcupacionCamaAlojamiento> buscarOcupacionAlojamiento(String cvePresupuestal);

    PeriodosImss obtenerPeriodoActivo(String clavePresupuestal, Integer claveTipoCaptura);

    PeriodosImss obtenerPeriodoInmediatoAnterior(String clavePeriodo);
    
    PeriodosImss obtenerPeriodoMonitoreo(String clavePresupuestal, Integer claveTipoCaptura);
    
    MetodoAnticonceptivo buscarPlanificacionById(String cveplanificacion, Integer cveTipoFormato);

    Ingreso buscarIngresoPorDatosPaciente(DatosPaciente datosPaciente);
    
    Tococirugia buscarTocoPorClaveIngreso(DatosPaciente datosPaciente);
    
    IntervencionesQuirurgicas buscarIQxPorClaveIngreso(DatosPaciente datosPaciente);

    long guardarReporteGenerado(ReporteGenerado reporteGenerado);

    List<ProcedimientoRealizado> obtenerProcedimientosRealizados(String clavePresupuestal, String periodo,
            String procInicial, String procFinal);

    void cerrarPeriodo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    void abrirPeriodo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    List<PacientesHospitalizados> obtenerPacientesHospitalizadosFecha(String clavePresupuestal, String fecha);

    boolean validaPeriodoCerrado(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    List<ReporteGenerado> buscarReporteGeneradoPorDelegacion(String clavePresupuestal, TipoReporte report,
            String fechaAtencion);

    List<ReporteGenerado> buscarReporteGeneradoPorDelegacionYFecha(String clavePresupuestal, TipoReporte report,
            String fecha);

    Boolean buscarReporteGeneradoPorDelegacion(String clavePresupuestal, Integer tipoReporte);

    Boolean buscarReporteGeneradoPorDelegacionPorFecha(String clavePresupuestal, Integer tipoReporte, String fecha);

    UnidadMedica buscarUnidadPorClavePresupuestal(String clavePresupuestal);

    Procedimiento obtenerProcedimientoPorClave(String cveCie9);

    Parametro obtenerParamtro(String cveParametro);

    Long contarCamasConDosPacientesPorUnidad(String clavePresupuestal);

    Long contarCamasActivasPorUnidad(String clavePresupuestal);

    PeriodoOperacion buscarPeriodoOperacionPorClavePeriodo(String clavePeriodo, String clavePresupuestal,
            Integer tipoCaptura);

    Long numeroRegistros(String clavePresupuestal, String fechaIni, String fechaFin);

    long insertarPeriodoOperacion(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss,
            Integer indCerrado, Integer indActual, Integer indMonitoreo) throws HospitalizacionException;

    PeriodoOperacion buscarPeriodoAbierto(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    List<Ingreso> buscarIngresoVigente(String clavePresupuestal, String nss, String agregadoMedico,
            List<Integer> tipoFormato);
    
    List<Ingreso> buscarIngresoMadre(String clavePresupuestal, String nss, String agregadoMedico,
    		List<Integer> tipoFormato);
    
    List<Ingreso> buscarIngresoVigenteRN(String clavePresupuestal, List<Integer> tipoFormato, Long clavePaciente);
    
    List<Long> buscarRNExterno(String nss, String agregadoMedico,String nombre, String apellidoPaterno);

    Long numeroIngresosPacientesHospitalizadosFecha(String clavePresupuestal, String fecha);

    Ingreso obtenerDatosIngreso(Long claveIngreso);
    
    ReporteGenerado buscarUltimoReporteGenerado(String clavePresupuestal, Integer tipoReporte);

	Procedimiento obtenerProcedimientoBilateralPorClave(String cveCie9);
	
	MovimientoIntraHospitalario buscarMovIntraPorCveIngreso(DatosPaciente datosPaciente);
	
	boolean existenIngresosRNGemelos(String nss, Long cvePaciente, String clavePresupuestal);
	
	List<TotalCamaUnidad> buscarTotalCamaAlojamiento(String cvePresupuestal);

}
