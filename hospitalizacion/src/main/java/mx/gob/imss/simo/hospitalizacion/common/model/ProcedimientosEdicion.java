package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class ProcedimientosEdicion {

    private String consecutivo;
    private Date fechaIntervencion;
    private String claveProcedimiento;
    private String descripcionProcedimiento;
    private Date fechaProcedimiento;
    private String fechaIntervencionHoraMinuto;

}
