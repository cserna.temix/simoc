package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class ReporteGenerado {

    private String cvePresupuestal;
    private Integer tipoReporte;
    private Date fecGeneracion;

}
