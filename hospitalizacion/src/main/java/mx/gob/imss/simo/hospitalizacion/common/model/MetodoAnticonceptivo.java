package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class MetodoAnticonceptivo {

    private int clave;
    private String descripcion;
    private int numeroEdadInicial;
    private int numeroEdadFinal;
    private int claveSexo;
    private Date fechaBaja;
    private Integer cantidad;

}
