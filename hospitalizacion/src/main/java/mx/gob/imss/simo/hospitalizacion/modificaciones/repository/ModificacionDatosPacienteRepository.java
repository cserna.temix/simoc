package mx.gob.imss.simo.hospitalizacion.modificaciones.repository;

import java.util.Date;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;

public interface ModificacionDatosPacienteRepository {

    void guardarModificacionDatosPaciente(DatosPaciente datosPaciente, String nssOriginal, String agregado, Date fechaModificacion)
            throws HospitalizacionException;

}
