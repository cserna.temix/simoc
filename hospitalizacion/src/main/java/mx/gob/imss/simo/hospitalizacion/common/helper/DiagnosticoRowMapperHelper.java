package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Diagnostico;

public class DiagnosticoRowMapperHelper extends BaseHelper implements RowMapper<Diagnostico> {

    @Override
    public Diagnostico mapRow(ResultSet resultSet, int rowId) throws SQLException {

        Diagnostico diagnostico = new Diagnostico();

        diagnostico.setCveCie10(resultSet.getString(SQLColumnasConstants.CVE_CIE10));
        diagnostico.setCveSexo(resultSet.getInt(SQLColumnasConstants.CVE_SEXO));
        diagnostico.setDesCie10(resultSet.getString(SQLColumnasConstants.DES_CIE10));
        diagnostico.setFecBaja(resultSet.getDate(SQLColumnasConstants.FEC_BAJA));
        diagnostico.setIndEnfCronica(resultSet.getInt(SQLColumnasConstants.IND_ENF_CRONICA));
        diagnostico.setIndErradicado(resultSet.getInt(SQLColumnasConstants.IND_ERRADICADO));
        diagnostico.setIndExclusion(resultSet.getInt(SQLColumnasConstants.IND_EXCLUSION));
        diagnostico.setIndNivel1(resultSet.getInt(SQLColumnasConstants.IND_NIVEL1));
        diagnostico.setIndNivel2(resultSet.getInt(SQLColumnasConstants.IND_NIVEL2));
        diagnostico.setIndNivel3(resultSet.getInt(SQLColumnasConstants.IND_NIVEL3));
        diagnostico.setIndNotifInmediata(resultSet.getInt(SQLColumnasConstants.IND_NOTIF_INMEDIATA));
        diagnostico.setIndNotifInternacional(resultSet.getInt(SQLColumnasConstants.IND_NOTIF_INTERNACIONAL));
        diagnostico.setIndNotifObligatoria(resultSet.getInt(SQLColumnasConstants.IND_NOTIF_OBLIGATORIA));
        diagnostico.setIndNotifObstetrica(resultSet.getInt(SQLColumnasConstants.IND_NOTIF_OBSTETRICA));
        diagnostico.setIndNoValidaAfeccionHosp(resultSet.getInt(SQLColumnasConstants.IND_NOVALIDA_AFECCION_HOSP));
        diagnostico.setIndNoValidaBasicaDef(resultSet.getInt(SQLColumnasConstants.IND_NOVALIDA_BASICA_DEF));
        diagnostico.setIndPrincipalConsExterna(resultSet.getInt(SQLColumnasConstants.IND_PRINCIPAL_CONSULTA_EXTERNA));
        diagnostico.setIndTransmisible(resultSet.getInt(SQLColumnasConstants.IND_TRANSMISIBLE));
        diagnostico.setIndTriv(resultSet.getInt(SQLColumnasConstants.IND_TRIV));
        diagnostico.setIndValidaMuerteFetal(resultSet.getInt(SQLColumnasConstants.IND_VALIDA_MUERTE_FETAL));
        diagnostico.setNumEdadInferiorAnios(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_INFERIOR_ANIOS));
        diagnostico.setNumEdadInferiorSemanas(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_INFERIOR_SEMANAS));
        diagnostico.setNumEdadSuperiorAnios(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_SUPERIOR_ANIOS));
        diagnostico.setNumEdadSuperiorSemanas(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_SUPERIOR_SEMANAS));
        return diagnostico;
    }

}
