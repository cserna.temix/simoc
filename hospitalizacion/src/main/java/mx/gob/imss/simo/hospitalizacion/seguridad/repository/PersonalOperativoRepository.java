package mx.gob.imss.simo.hospitalizacion.seguridad.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.PersonalOperativo;
import mx.gob.imss.simo.hospitalizacion.common.model.Rol;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;

public interface PersonalOperativoRepository {

    PersonalOperativo obtenerPersonalOperativoPorUsuarioAD(String usuario);

    List<Rol> obtenerRolesPorUsuarioAD(String usuario);

    void actualizarIntento(String usuario, Integer intento);

    void actualizarBloqueo(String usuario, Integer intento);

    List<UnidadMedica> obtenerUnidadesMedicasPorUsuario(String usuario);
}
