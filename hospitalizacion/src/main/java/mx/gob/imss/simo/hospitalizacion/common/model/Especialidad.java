package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Especialidad {

    private Integer cveCategoriaEspecialidad;
    private int cveDivision;
    private String cveEspecialidad;
    private int cveSexo;
    private String desEspecialidad;
    private Date fecBaja;
    private boolean indMovIntrahospitalario;
    private boolean indPediatria;
    private boolean indQx;
    private int numEdadMaxima;
    private int numEdadMaximaSemana;
    private int numEdadMinima;
    private int numEdadMinSemana;
    private Integer numTipoServicio;
    private int numUbicacion;
    private String refAreaResponsable;
    private String refSimoCentral;
    private String refSimoSias;

}
