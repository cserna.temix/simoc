package mx.gob.imss.simo.hospitalizacion.menu.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.Menu;

public interface MenuRepository {

    List<Menu> getMenu(String cuentaAD);

}
