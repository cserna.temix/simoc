package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class DatosEgreso {

    private String fechaEgresoS;
    private String horaEgreso;
    private String tipoEgreso;
    private String diagnosticoPrin;
    private String cveCausaDefBasica;
    private String causaDefBasica;
    private String diagnosticoInicial;
    private String cvePrimerDiagnosticoSecundario;
    private String primerDiagnosticoSecundario;
    private String claveSegundoDiagnosticoSecundario;
    private String segundoDiagnosticoSecundario;
    private String cveCausaDefDirecta;
    private String causaDefDirecta;
    private String cveComplicacionIntraPrimera;
    private String complicacionIntraPrimera;
    private String cveComplicacionIntraSegunda;
    private String complicacionIntraSegunda;
    private String motivoAlta;
    private String planificacionFam;
    private String camaHospitalizacion;
    private String causaDefSecundaria;
    private String justificacionEgreso;
    private String cantidad;
    private String alimentacionRecNac;
    private List<String> procedimientoRecNac;
    private String tamiz;
    private String pesoEgresoRecNac;
    private String cveInicialIngresoEgreso;
    private String cvePrincipalEgreso;
    private String cveMetodoPlaniFamiliar;
    private Date fechaEgreso;
    private int tipoFormato;
    private String especialidadEgreso;
    private String inicialIngresoEgreso;
    private String principalEgreso;
    private String cveComplicacionIntraTercera;
    private String complicacionIntraTercera;
    private String motivoEgreso;
    private String envioA;
    private String tercerDiagnosticoSecundario;
    private String cuartoDiagnosticoSecundario;
    private String quintoDiagnosticoSecundario;
    private Integer numProcedimientoCama;
    private List<Procedimientos> procedimientosCama;
    private String egresoDefuncion;
    private String riesgoTrabajo;
    private String numRecetas;
    

}
