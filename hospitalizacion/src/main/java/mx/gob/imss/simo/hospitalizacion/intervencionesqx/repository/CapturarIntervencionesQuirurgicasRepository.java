package mx.gob.imss.simo.hospitalizacion.intervencionesqx.repository;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;
import mx.gob.imss.simo.model.DatosUsuario;

public interface CapturarIntervencionesQuirurgicasRepository extends HospitalizacionCommonRepository {

    Date obtenerFechaIngresoPorIdIngreso(long idIngreso);

    void guardarIntervencion(DatosHospitalizacion datos, DatosUsuario usuario, Date fecha);

    void guardarProcedimientos(Procedimientos procedimiento, Long cveIngreso, Date fecha);
    
    IntervencionesQuirurgicas buscarIntervencionPorClaveIngreso(Long cveIngreso);
    
    void actualizarMetodoAnticonceptivoIntervencion(int cveMetodo, int cantidadMetodo, Long cveIngreso );

}
