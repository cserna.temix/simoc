package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class OcupacionCamaAlojamiento {
	
    private String cveEspecialidad;
    private String cvePresupuestal;
    private int numOcupacionCama;
    private Date fecCaptura;

}
