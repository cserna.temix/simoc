package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Medico {

    private String cveMatricula;
    private String refNombre;
    private String refApellidoPaterno;
    private String refApellidoMaterno;
    private String refTipoContratacion;
    private String cveDelegacionImss;
    private Date fecConsultaSiap;
    private boolean indActivo;
    private boolean indSiap;

}
