package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class SalaUnidad {

    private String clave;
    private String clavePresupuestal;
    private String descripcion;
    private int indicadorTurnoMatutitno;
    private int indicadorTurnoVespertino;
    private int indicadorTurnoNocturno;
    private int indicadorTurnoJornadaAcumulada;
    private boolean estatus;
    private Date fechaBaja;

}
