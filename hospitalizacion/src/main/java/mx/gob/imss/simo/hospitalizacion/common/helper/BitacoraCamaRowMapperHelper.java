package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.BitacoraCama;

public class BitacoraCamaRowMapperHelper extends BaseHelper implements RowMapper<BitacoraCama> {

    @Override
    public BitacoraCama mapRow(ResultSet resultSet, int rowId) throws SQLException {

        BitacoraCama bitacoraCama = new BitacoraCama();
        bitacoraCama.setCveBitacoraCama(resultSet.getLong(SQLColumnasConstants.CVE_BITACORA_CAMA));
        bitacoraCama.setCveCama(resultSet.getString(SQLColumnasConstants.CVE_CAMA));
        bitacoraCama.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        bitacoraCama.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
        bitacoraCama.setCveTipoFormato(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_FORMATO));
        bitacoraCama.setFecEvento(resultSet.getDate(SQLColumnasConstants.FEC_EVENTO));
        bitacoraCama.setIndOcupada(resultSet.getBoolean(SQLColumnasConstants.IND_OCUPADA));
        return bitacoraCama;
    }

}
