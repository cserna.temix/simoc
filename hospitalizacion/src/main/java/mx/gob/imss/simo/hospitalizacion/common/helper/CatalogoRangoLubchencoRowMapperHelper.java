package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.RangoLubchenco;

public class CatalogoRangoLubchencoRowMapperHelper extends BaseHelper implements RowMapper<RangoLubchenco> {

    @Override
    public RangoLubchenco mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        RangoLubchenco rangoLubchenco = new RangoLubchenco();
        rangoLubchenco.setRango(resultSet.getInt(SQLColumnasConstants.CVE_RANGO_LUBCHENCO));
        rangoLubchenco.setSemanasGestacion(resultSet.getInt(SQLColumnasConstants.NUM_SEMANA_GESTACION));
        rangoLubchenco.setPesoSuperior(resultSet.getInt(SQLColumnasConstants.CAN_PESO_SUPERIOR));
        rangoLubchenco.setPesoInferior(resultSet.getInt(SQLColumnasConstants.CAN_PESO_INFERIOR));
        rangoLubchenco.setClave(resultSet.getInt(SQLColumnasConstants.CVE_LUBCHENCO));
        rangoLubchenco.setClaveClasificacionPeso(resultSet.getInt(SQLColumnasConstants.CVE_CLASIFICACION_PESO));
        return rangoLubchenco;
    }

}
