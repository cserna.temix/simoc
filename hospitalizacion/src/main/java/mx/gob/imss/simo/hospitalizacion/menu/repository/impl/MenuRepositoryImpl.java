package mx.gob.imss.simo.hospitalizacion.menu.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.MenuRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.Menu;
import mx.gob.imss.simo.hospitalizacion.common.repository.BaseJDBCRepository;
import mx.gob.imss.simo.hospitalizacion.menu.repository.MenuRepository;

@Repository
public class MenuRepositoryImpl extends BaseJDBCRepository implements MenuRepository {

    @Override
    public List<Menu> getMenu(String cuentaAD) {

        List<Menu> menu = new ArrayList<>();

        menu = jdbcTemplate.query(SQLConstants.OBTENER_MENU_POR_CUENTA_AD, new Object[] { cuentaAD },
                new MenuRowMapperHelper());
        return menu;
    }

}
