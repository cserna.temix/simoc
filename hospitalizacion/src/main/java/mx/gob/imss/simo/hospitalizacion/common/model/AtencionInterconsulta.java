/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class AtencionInterconsulta {

	Integer cveInterconsulta;
	Integer numInterconsulta;
	String especialidadSolicita;
	long cvePaciente;
	Integer timDuracion;
	Integer indAlta;
	String claveCapturista;
	
	String cveCie10Principal;
	Integer indPVCie10Principal;
	String cveCie10Adicional;
	Integer indPVCie10Adicional;
	String cveCie10Complementario;
	Integer indPVCie10Complementario;
	
	String cveCie9Principal;
	String cveCie9Adicional;
	String cveCie9Complementario;

}
