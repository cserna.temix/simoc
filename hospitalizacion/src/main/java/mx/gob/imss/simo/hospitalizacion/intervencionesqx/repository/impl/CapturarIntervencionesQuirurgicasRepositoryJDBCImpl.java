package mx.gob.imss.simo.hospitalizacion.intervencionesqx.repository.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLUpdateConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.helper.IntervencionesQXRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.repository.CapturarIntervencionesQuirurgicasRepository;
import mx.gob.imss.simo.model.DatosUsuario;

@Repository("capturarIntervencionesQuirurgicasRepository")
public class CapturarIntervencionesQuirurgicasRepositoryJDBCImpl extends HospitalizacionCommonRepositoryJDBCImpl
        implements CapturarIntervencionesQuirurgicasRepository {

    public Date obtenerFechaIngresoPorIdIngreso(long idIngreso) {

        return new Date();
    }

    public void guardarIntervencion(DatosHospitalizacion datos, DatosUsuario usuario, Date fecha) {

        jdbcTemplate.update(SQLUpdateConstants.GUARDAR_INTERVENCION_QX, new Object[] {
                datos.getDatosPaciente().getClaveIngreso(), fecha, usuario.getUsuario(),
                datos.getDatosIntervenciones().getFechaIQX(), datos.getDatosIntervenciones().getNumeroQuirofano(),
                usuario.getCvePresupuestal(), datos.getDatosIntervenciones().getEspecialidad(),
                datos.getDatosIntervenciones().getNumeroProcedimientos(),
                datos.getDatosIntervenciones().getFechaEntrada(), datos.getDatosIntervenciones().getFechaInicio(),
                datos.getDatosIntervenciones().getFechaTermino(), datos.getDatosIntervenciones().getFechaSalida(),
                Integer.parseInt(datos.getDatosIntervenciones().getTipoIntervencion()),
                datos.getDatosIntervenciones().getFechaProgramada(),
                datos.getDatosIntervenciones().getMetodoPlanificacion(),
                datos.getDatosIntervenciones().getCantidadMetodoPlanificacion(),
                Integer.parseInt(datos.getDatosIntervenciones().getTipoAnestesia()),
                datos.getDatosMedico().getMatricula(), TipoFormatoEnum.INTERVECION_QUIRURGICA.getClave() });

    }

    public void guardarProcedimientos(Procedimientos procedimiento, Long cveIngreso, Date fecha) {

        jdbcTemplate.update(SQLUpdateConstants.GUARDAR_PROCEDIMIENTO, new Object[] { cveIngreso, fecha,
                procedimiento.getConsecutivo(), procedimiento.getClave(), procedimiento.getMatricula() });

    }

	@Override
	public IntervencionesQuirurgicas buscarIntervencionPorClaveIngreso(Long cveIngreso) {
		List<IntervencionesQuirurgicas> intervenciones =  jdbcTemplate.query(SQLConstants.BUSCAR_IQX_POR_CLAVE_INGRESO,
				new Object[]{cveIngreso},new IntervencionesQXRowMapperHelper());
		
		return (intervenciones != null && !intervenciones.isEmpty()) ? intervenciones.get(0): null ;
			
	}
	
	@Override
	public void actualizarMetodoAnticonceptivoIntervencion(int cveMetodo, int cantidadMetodo, Long cveIngreso) {
		jdbcTemplate.update(SQLUpdateConstants.ACTUALIZAR_METODO_ANTICONCEP_IQX, new Object[]{cveMetodo, cantidadMetodo, cveIngreso});
		
	}

}
