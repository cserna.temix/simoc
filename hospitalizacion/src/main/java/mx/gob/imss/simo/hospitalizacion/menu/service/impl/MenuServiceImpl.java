package mx.gob.imss.simo.hospitalizacion.menu.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.hospitalizacion.common.model.Menu;
import mx.gob.imss.simo.hospitalizacion.menu.repository.MenuRepository;
import mx.gob.imss.simo.hospitalizacion.menu.service.MenuService;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

    final static Logger logger = LoggerFactory.getLogger(MenuServiceImpl.class);

    @Autowired
    MenuRepository menuRepository;

    @Override
    public List<Menu> getMenu(String cuentaAD) {

        return menuRepository.getMenu(cuentaAD);
    }

    public MenuModel createMenu(String cuentaAD) {

        MenuModel model;

        List<Menu> menu = getMenu(cuentaAD);

        logger.info("Creando menu para el usuario: " + cuentaAD);

        String nivel1 = "ui-icon-bullet";

        model = new DefaultMenuModel();

        DefaultSubMenu rootMenu = new DefaultSubMenu();
        List<MenuElement> menu0 = new ArrayList<MenuElement>();

        for (Menu men : menu) {

            if (men.getMenuPadre() == BigDecimal.ZERO.intValue()) {
                logger.info("Agregando menu: " + men.getDescripcionMenu());
                DefaultSubMenu submenu0 = new DefaultSubMenu(men.getDescripcionMenu(), nivel1);
                submenu0.setStyleClass("imagenMenuBar");
                submenu0.setStyleClass("menuContenedores");
                submenu0.setId("menu_p");

                submenu0 = validarHijos(menu, men.getClaveMenu(), submenu0);

                menu0.add(submenu0);
            }

        }

        rootMenu.setElements(menu0);
        model.addElement(rootMenu);
        return model;
    }

    private DefaultSubMenu validarHijos(List<Menu> menu, int claveMenu, DefaultSubMenu item) {

        List<Menu> menuTemp = new ArrayList<Menu>();
        for (Menu men : menu) {
            if (men.getMenuPadre() == claveMenu) {
                menuTemp.add(men);
            }
        }
        /**
         * Cuando el tamano del arreglo sea mayor a uno debe ser submenu, en otro
         * caso debera ser un SubMenuItem
         */
        if (menuTemp.size() >= 1) {
            for (Menu men : menuTemp) {
                DefaultSubMenu submenu0 = new DefaultSubMenu(men.getDescripcionMenu());
                DefaultMenuItem adminSubItem = null;
                submenu0.setStyleClass("imagenMenuBar");
                submenu0.setStyleClass("menuContenedores");
                submenu0.setId("menu_p");
                submenu0 = validarHijos(menu, men.getClaveMenu(), submenu0);
                if (submenu0.getElements().size() == 0) {
                    adminSubItem = new DefaultMenuItem(men.getDescripcionMenu());
                    adminSubItem.setCommand(men.getRefAccion());
                    item.addElement(adminSubItem);
                } else {
                    item.addElement(submenu0);
                }
            }
        }

        return item;
    }

    public void setMenuRepository(MenuRepository menuRepository) {

        this.menuRepository = menuRepository;
    }
}
