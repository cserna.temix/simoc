package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientosEdicion;

public class ProcedimientosEdicionRowMapperHelper extends BaseHelper implements RowMapper<ProcedimientosEdicion> {

    @Override
    public ProcedimientosEdicion mapRow(ResultSet resultSet, int rowId) throws SQLException {

        ProcedimientosEdicion procedimiento = new ProcedimientosEdicion();

        procedimiento.setConsecutivo(resultSet.getString(SQLColumnasConstants.NUM_PROCEDIMIENTO));
        procedimiento.setFechaIntervencion(resultSet.getDate(SQLColumnasConstants.FEC_INTERVENCION_IQX));
        procedimiento.setClaveProcedimiento(resultSet.getString(SQLColumnasConstants.CVE_CIE9));
        procedimiento.setDescripcionProcedimiento(resultSet.getString(SQLColumnasConstants.DES_CIE9));
        procedimiento.setFechaProcedimiento(resultSet.getDate(SQLColumnasConstants.FEC_INTERVENCION_IQX));
        return procedimiento;
    }

}
