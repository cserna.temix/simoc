package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class AlimentacionRecienNacido {

    private int clave;
    private String descripcion;
    private Date fechaBaja;

}
