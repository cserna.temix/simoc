package mx.gob.imss.simo.hospitalizacion.reportes.controller;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

import com.ibm.icu.util.Calendar;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientoRealizado;
import mx.gob.imss.simo.hospitalizacion.common.model.ReporteGenerado;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.hospitalizacion.reportes.services.ReportesServices;

@Data
@ViewScoped
@ManagedBean(name = "reporteProcRealizadosController")
public class ReporteProcRealizadosController extends HospitalizacionCommonController {

    /**
     * 
     */
    private static final long serialVersionUID = 8063869982146954805L;

    @ManagedProperty("#{catalogosHospitalizacionServices}")
    protected transient CatalogosHospitalizacionServices catalogosHospitalizacionServices;

    @ManagedProperty("#{reportesServices}")
    protected transient ReportesServices reportesServices;

    private String tieneFoco = "";
    private static final String VACIO = "";
    private static final String FECHA_CERO = "00";

    private Date fecInicialPeriodo;
    private Date fecFinalPeriodo;

    transient InputMask periodoIM;
    String periodo;
    transient InputText procInicialIT;
    String procInicial;
    transient InputText procFinalIT;
    String procFinal;
    transient InputText procInicialDescIT;
    String procInicialDesc;
    transient InputText procFinalDescIT;
    String procFinalDesc;
    String mesCaptura;
    String anioCaptura;
    String periodoConcatenado;
    transient Procedimiento procedimientoInicial;
    transient Procedimiento procedimientoFinal;

    @ManagedProperty("#{reporteProcedimientosRealizados}")
    private transient AbstractJasperReportsSingleFormatView plantillaReporteProcedimientosRealizados;

    @Override
    @PostConstruct
    public void init() {

        super.init();
        periodoIM = new InputMask();
        procInicialIT = new InputText();
        procFinalIT = new InputText();
        procInicialDescIT = new InputText();
        procFinalDescIT = new InputText();

        procInicialDescIT.setDisabled(true);
        procFinalDescIT.setDisabled(true);
        periodo = null;
        procInicial = null;
        procFinal = null;
        procInicialDesc = null;
        procFinalDesc = null;
        tieneFoco = "procedimientosForm:periodo";

    }

    public void validarPeriodoIM() {

        if (inputmaskLlena(periodoIM)) {
            String[] periodoArray = periodoIM.getValue().toString().split("/");
            int mesPeriodo = Integer.parseInt(periodoArray[0].trim());

            if (periodoArray[0].equals(FECHA_CERO) || mesPeriodo > 12) {
                periodoIM.resetValue();
                agregarMensajeError(getArray(MensajesErrorConstants.ME_109), null);
            } else {
                if (validaFecha(periodoArray[0], periodoArray[1])) { // valida fecha introducida mayor

                    mesCaptura = periodoArray[0];
                    anioCaptura = periodoArray[1];
                    periodoConcatenado = periodoArray[1].concat(periodoArray[0]);

                    if (!existePeriodo(periodoArray[0], periodoArray[1])) {
                        periodoIM.resetValue();
                        agregarMensajeError(getArray(MensajesErrorConstants.ME_009), null);
                    } else {
                        setTieneFoco(procInicialIT.getClientId());
                    }

                } else {

                    periodoIM.resetValue();
                    agregarMensajeError(getArray(MensajesErrorConstants.ME_007), null);
                }
            }
        } else {
            setTieneFoco(periodoIM.getClientId());
            periodoIM.resetValue();
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarEgresoConstants.REQUERIDO_MES_ANIO));
        }
        
        try{
        	if(!periodoConcatenado.equals(getObjetosSs().getPeriodoActual())){
        		throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_009));
        	}
        } catch (HospitalizacionException e) {
        	agregarMensajeError(e);
        	setTieneFoco(periodoIM.getClientId());
		}
    }

    public void validarProcInicial() {

        if (!procInicial.isEmpty()) {
            if (procInicial.length() == 4) {
                buscarProcedimientoInicial(procInicial);

            } else {
                procInicial = VACIO;
            }

        } else {
            setTieneFoco(procInicialIT.getClientId());
            procInicial = VACIO;
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarEgresoConstants.REQUERIDO_PROCEDIMIENTO_INICIAL));
        }
    }

    public void validarProcFinal() {

        if (!procFinal.isEmpty()) {
            if (procFinal.length() == 4) {
                buscarProcedimientoFinal(procFinal);
            } else {
                procFinal = VACIO;
            }
        } else {
            setTieneFoco(procFinalIT.getClientId());
            procFinal = VACIO;
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarEgresoConstants.REQUERIDO_PROCEDIMIENTO_FINAL));
        }
    }

    private boolean inputmaskLlena(InputMask inputMask) {

        return inputMask.getValue() != null && !inputMask.getValue().toString().isEmpty()
                && !inputMask.getValue().toString().contains("_");
    }

    public void generaReporte(){

        boolean reporteGenerado = false;
        List<ProcedimientoRealizado> procRealizados;

        procRealizados = obtenerProcedimientosRealizados();

        if (procRealizados == null || procRealizados.isEmpty()) {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_159), null);
            periodoIM.setValue(null);
            limpiaCie9();
            procedimientoInicial = null;
            procedimientoFinal = null;
            setTieneFoco(periodoIM.getClientId());
        } else {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            final Map<String, Object> model = new HashMap<>();
            final String logoImss = context.getRealPath(File.separator + "img" + File.separator + "imss_header.png");

            model.put("logoImss", logoImss);
            model.put("fecIngresoMes", mesCaptura);
            model.put("fecIngresoAnio", anioCaptura);
            model.put("cveCie9Ini", procInicial);
            model.put("cveCie9Fin", procFinal);
            model.put("nombreUnidad", obtenerDatosUsuario().getDesUnidadMedicaAct());
            model.put("nombreDelegacion", obtenerDatosUsuario().getDesDelegacionAct());
            model.put("perfil", obtenerDatosUsuario().getPerfil());
            model.put("cvePresupuestal", obtenerDatosUsuario().getCvePresupuestal());
            model.put("data", procRealizados);

            try {
                plantillaReporteProcedimientosRealizados.render(model, (HttpServletRequest) context.getRequest(),
                        (HttpServletResponse) context.getResponse());
                reporteGenerado = true;
            } catch (Exception e) {
                logger.error("{}", e);
            }

        }
        if (reporteGenerado) {
            ReporteGenerado generado = new ReporteGenerado();
            generado.setCvePresupuestal(obtenerDatosUsuario().getCvePresupuestal());
            generado.setFecGeneracion(new Date());
            generado.setTipoReporte(4);
            reportesServices.guardarReporteGenerado(generado);
        }

    }

    private boolean isRangoProcedimentosCorrecto() {

        return getProcedimientoInicial().getNumCodigoConsecutivo() <= getProcedimientoFinal().getNumCodigoConsecutivo()
                ? true : false;
    }

    public List<ProcedimientoRealizado> obtenerProcedimientosRealizados() {

        return reportesServices.obtenerProcedimientosRealizados(obtenerDatosUsuario().getCvePresupuestal(),
                periodoConcatenado, procInicial, procFinal);
    }

    public PeriodosImss obtenerPeriodoImss(String mes, String anio) {

        return catalogosHospitalizacionServices.obtenerPeriodo(mes, anio);
    }

    public void buscarProcedimientoInicial(String buscaProcedimientoInicial) {

        try {
            procedimientoInicial = null;
            procedimientoInicial = catalogosHospitalizacionServices.obtenerProcedimiento(buscaProcedimientoInicial);
        } catch (EmptyResultDataAccessException e) {
            logger.error("{}", e);
        }

        if (null != procedimientoInicial) {
            setProcInicialDesc(procedimientoInicial.getDesCie9());
            setTieneFoco(procFinalIT.getClientId());
        } else {
            setTieneFoco(procInicialIT.getClientId());
            procInicial = VACIO;
            procInicialDesc = VACIO;
            procedimientoInicial = null;
            agregarMensajeError(getArray(MensajesErrorConstants.ME_014), null);
        }
    }

    public void buscarProcedimientoFinal(String buscaProcedimientoFinal) {

        try {
            procedimientoFinal = null;
            procedimientoFinal = catalogosHospitalizacionServices.obtenerProcedimiento(buscaProcedimientoFinal);
        } catch (EmptyResultDataAccessException e) {
            logger.error("{}", e);
        }
        if (null != procedimientoFinal) {
            if (isRangoProcedimentosCorrecto()) {
                setProcFinalDesc(procedimientoFinal.getDesCie9());
                setTieneFoco("procedimientosForm:idGeneraReporte");
            } else {
                limpiaCie9();
                procedimientoFinal = null;
                setTieneFoco(procInicialIT.getClientId());
                agregarMensajeError(getArray(MensajesErrorConstants.ME_163), null);
            }
        } else {
            agregarMensajeError(getArray(MensajesErrorConstants.ME_014), null);
            procFinal = VACIO;
            procFinalDesc = VACIO;
            setTieneFoco(procFinalIT.getClientId());
        }
    }

    private void limpiaCie9() {

        procInicial = VACIO;
        procFinal = VACIO;
        procInicialDesc = VACIO;
        procFinalDesc = VACIO;
    }

    public boolean existePeriodo(String mes, String anio) {

        boolean existe = true;
        PeriodosImss periodosImss = null;
        try {
            periodosImss = catalogosHospitalizacionServices.obtenerPeriodo(mes, anio);
        } catch (EmptyResultDataAccessException e) {
            logger.error("{}", e);
        }
        if (periodosImss == null) {
            existe = false;
        }
        return existe;
    }

    private boolean validaFecha(String mes, String anio) {

        boolean valida = true;

        Calendar cal = Calendar.getInstance();
        int mesPeriodo = Integer.parseInt(mes);
        int anioPeriodo = Integer.parseInt(anio);
        int mesActual = cal.get(Calendar.MONTH) + 1;
        int anioActual = cal.get(Calendar.YEAR);
        if (anioPeriodo > anioActual) {
            valida = false;
        } else if (anioPeriodo == anioActual && mesPeriodo > mesActual) {
            valida = false;
        }
        return valida;
    }

    
    @Override
    public String obtenerNombrePagina() {

        // LoginController.setRenderIcons(false);
        return PagesCommonConstants.REPORTE_PROCEDIMIENTOS_REALIZADOS;
    }

    public void salir() throws IOException {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }



     
}
