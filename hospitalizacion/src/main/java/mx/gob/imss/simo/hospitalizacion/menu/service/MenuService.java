package mx.gob.imss.simo.hospitalizacion.menu.service;

import java.util.List;

import org.primefaces.model.menu.MenuModel;

import mx.gob.imss.simo.hospitalizacion.common.model.Menu;

public interface MenuService {

    List<Menu> getMenu(String cuentaAD);

    MenuModel createMenu(String cuentaAD);
}
