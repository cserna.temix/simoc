package mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MovimientosIntrahospitalariosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.model.DatosUsuario;

@Component
public class CapturarMovimientosIntrahospitalariosHelper extends HospitalizacionCommonHelper {

    public MovimientoIntraHospitalario prepararDatosMovimientros(Ingreso ingreso, Especialidad especialidad,
            DatosCatalogo division) {

        MovimientoIntraHospitalario datos = new MovimientoIntraHospitalario();
        StringBuilder esp = new StringBuilder();
        esp.append(especialidad.getCveEspecialidad()).append(" ").append(especialidad.getDesEspecialidad());
        datos.setEspecialidadOrigen(esp.toString());
        StringBuilder div = new StringBuilder();
        div.append(division.getClave()).append(" ").append(division.getDescripcion());
        datos.setDivisionOrigen(div.toString());
        if(ingreso.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()
        		||ingreso.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue())
        {
        	datos.setCamaOrigen(ingreso.getRefCamaRn());
        }else{
        	datos.setCamaOrigen(ingreso.getCveCama());
        }
        return datos;
    }

    public DatosCatalogo elementoSeleccionado(SelectEvent evento, List<String> listaEspecialidad,
            AutoComplete autoCompleteEspecialidad, List<DatosCatalogo> catalogoEspecialidad) {

        int indice = listaEspecialidad.indexOf(autoCompleteEspecialidad.getValue().toString());
        return catalogoEspecialidad.get(indice);
    }

    public MovimientoIntraHospitalario prepararMovimientosAGurdar(MovimientoIntraHospitalario movimiento,
            Ingreso ingreso, DatosUsuario capturista, CamaUnidad camaUnidad) {

        MovimientoIntraHospitalario movimientoIntra = new MovimientoIntraHospitalario();
        movimientoIntra.setClaveIngreso(ingreso.getCveIngreso());
        movimientoIntra.setFechaCreacion(new Date());
        //movimientoIntra.setFechaMovimiento(convertStringToDate(movimiento.getFechaAtencion()));
        movimientoIntra.setFechaMovimiento(movimiento.getFechaMovimiento());
        
        if(ingreso.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue() 
        		|| ingreso.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue()){
        	movimientoIntra.setCamaOrigen(ingreso.getRefCamaRn());
        	
        }else{
        	movimientoIntra.setCamaOrigen(ingreso.getCveCama());
        }
        	/*if(ingreso.getCveEspecialidadCama() == null){
        		movimientoIntra.setEspeciaidadCamaOrigen(ingreso.getCveEspecialidadIngreso());
        	}else{
        		movimientoIntra.setEspeciaidadCamaOrigen(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
        	}
        }else{
        	movimientoIntra.setCamaOrigen(ingreso.getCveCama());
        	movimientoIntra.setEspeciaidadCamaOrigen(ingreso.getCveEspecialidadCama());
        }*/
        movimientoIntra.setEspeciaidadCamaOrigen(ingreso.getCveEspecialidadCama());
        movimientoIntra.setClavePresupuestal(ingreso.getCvePresupuestal());
        movimientoIntra.setEspecialidadOrigen(ingreso.getCveEspecialidadIngreso());
        movimientoIntra.setCapturista(capturista.getUsuario());
        movimientoIntra.setTipoFormato(TipoFormatoEnum.MOVIMIENTO_INTRAHOSPITALARIO.getClave());
        movimientoIntra.setCamaDestino(camaUnidad.getCveCama());
        movimientoIntra.setEspecialidadCamaDestino(camaUnidad.getCveEspecialidad());
        movimientoIntra.setEspecialidadDestino(movimiento.getEspecialidadDestino());
        movimientoIntra.setFechaMovIngreso(movimiento.getFechaMovIngreso());
        movimientoIntra.setCveMedico(movimiento.getCveMedico());
        return movimientoIntra;
    }

    public boolean validarActivarGuardar(Boolean[] requeridos) {

        for (boolean req : requeridos) {
            if (!req) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }
    
    public Integer obtenerTipoFormatoIngresoRecienNacido(String especialidadIngreso) {
    	
    	Integer nuevoTipoFormato = 0;
        if (especialidadIngreso.substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS).equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO.
        		substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS))) {
        	nuevoTipoFormato = TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave();
        } else if (especialidadIngreso.substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS).equals(BaseConstants.ESPECIALIDAD_CUNERO_PATOLOGICO.
        		substring(BaseConstants.INDEX_CERO, BaseConstants.INDEX_DOS))) {
        	nuevoTipoFormato = TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave();
        } 
        
        return nuevoTipoFormato;
    }
    
    public Date obtenerFechaCompleta(String fecha, String hora) {

        Date fechaDia = convertStringToDate(fecha);
        return convertStringHourToDate(fechaDia, hora);
    }
    
    public String obtenerSiguienteDia(String fechaBase){
    	Date fechaOriginal = convertStringToDate(fechaBase);
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(fechaOriginal);
    	calendar.add(Calendar.DATE, 1);
    	String fechaNueva = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(calendar.getTime());
    	return fechaNueva;
    }

}
