package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class UnidadMedica {

    private String cveCluesSalud;
    private String cveDelegacionImss;
    private String cveDelegacionSiap;
    private String cveEntidadFed;
    private String cvePrei;
    private String cvePresupuestal;
    private String cveTipoUnidadServicio;
    private String desNumUnidad;
    private String desUnidadMedica;
    private Date fecBaja;
    private boolean indAscripcion;
    private boolean indSimo;
    private String refJurisdiccionSsa;
    private String refNivelUnidad;
    private Integer indTitular;

}
