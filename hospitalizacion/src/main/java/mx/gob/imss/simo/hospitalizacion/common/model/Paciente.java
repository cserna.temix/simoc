package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Paciente {

    private long cveIngreso;
    private int consecutivo;
    private String cveCurp;
    private String cveIdee;
    private Long cveIdPersona;
    private long cvePaciente;
    private Long cvePacientePadre;
    private Integer cveSexo;
    private String cveUnidadAdscripcion;
    private Date fecActualizacion;
    private Date fecConsulta;
    private Date fecNacimiento;
    private boolean indAcceder;
    private boolean indDerechohabiente;
    private boolean indRecienNacido;
    private boolean indUltimoRegistro;
    private boolean indVigencia;
    private Integer numEdadAnios;
    private int numEdadSemanas;
    private String refAgregadoMedico;
    private String refApellidoMaterno;
    private String refApellidoPaterno;
    private String refNombre;
    private String refNss;
    private String refRegistroPatronal;
    private String refTipoPension;
    private Date fechaModificacion;

}
