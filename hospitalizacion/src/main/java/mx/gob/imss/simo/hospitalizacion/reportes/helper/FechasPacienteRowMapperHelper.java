package mx.gob.imss.simo.hospitalizacion.reportes.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.FechasPacienteVo;

public class FechasPacienteRowMapperHelper extends BaseHelper implements RowMapper<FechasPacienteVo> {

	@Override
	public FechasPacienteVo mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		FechasPacienteVo fechasPaciente = new FechasPacienteVo();
		
		fechasPaciente.setFechaInicio(resultSet.getDate(SQLColumnasConstants.FECHA_INICIO));
		fechasPaciente.setFechaFin(resultSet.getDate(SQLColumnasConstants.FECHA_FIN));
		
		return fechasPaciente;
	}

	
	
}
