package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoReporte {

    PARTE_I(1, "Parte I", 1), 
    PARTE_II(2, "Parte II", 2),
    PACIENTES_HOSPITALIZADOS(3, "Relación de pacientes hospitalizados", 2),
    PROCEDIMIENTOS_REALIZADOS(4, "Procedimientos realizados", 2),
    CAMAS_OCUPADAS_DOS_PACIENTES(5, "Ocupación de camas", 2);

    private Integer clave;
    private String descripcion;
    private int tipoCaptura;

    private TipoReporte(Integer clave, String descripcion, int tipoCaptura) {
        this.clave = clave;
        this.descripcion = descripcion;
        this.tipoCaptura = tipoCaptura;
    }

    public Integer getClave() {

        return clave;
    }

    public void setClave(Integer clave) {

        this.clave = clave;
    }

    public String getDescripcion() {

        return descripcion;
    }

    public void setDescripcion(String descripcion) {

        this.descripcion = descripcion;
    }

    public int getTipoCaptura() {

        return tipoCaptura;
    }

    public void setTipoCaptura(int tipoCaptura) {

        this.tipoCaptura = tipoCaptura;
    }

    public static TipoReporte parse(Integer clave) {

        TipoReporte right = null;
        for (TipoReporte item : values()) {
            if (item.getClave().equals(clave)) {
                right = item;
                break;
            }
        }
        return right;
    }

}
