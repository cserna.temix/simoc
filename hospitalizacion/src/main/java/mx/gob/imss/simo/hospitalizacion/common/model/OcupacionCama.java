package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class OcupacionCama {

    private String cveCama;
    private String cveEspecialidad;
    private String cvePresupuestal;
    private int numPacientes;

}
