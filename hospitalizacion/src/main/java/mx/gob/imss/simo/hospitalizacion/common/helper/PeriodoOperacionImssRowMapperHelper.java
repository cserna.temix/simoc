package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;

public class PeriodoOperacionImssRowMapperHelper extends BaseHelper implements RowMapper<PeriodoOperacion> {

    @Override
    public PeriodoOperacion mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        PeriodoOperacion periodo = new PeriodoOperacion();
        periodo.setClavePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        periodo.setTipoCaptura(resultSet.getInt(SQLColumnasConstants.CVE_TIPO_CAPTURA));
        periodo.setClavePeriodoIMSS(resultSet.getString(SQLColumnasConstants.CVE_PERIODO_IMSS));
        periodo.setIndicadorCerrado(resultSet.getBoolean(SQLColumnasConstants.IND_CERRADO));
        periodo.setFechaCierre(resultSet.getDate(SQLColumnasConstants.FEC_CIERRE));
        periodo.setIndicadorActual(resultSet.getBoolean(SQLColumnasConstants.IND_ACTUAL));
        periodo.setIndicadorMonitoreo(resultSet.getBoolean(SQLColumnasConstants.IND_MONITOREO));
        return periodo;
    }

}
