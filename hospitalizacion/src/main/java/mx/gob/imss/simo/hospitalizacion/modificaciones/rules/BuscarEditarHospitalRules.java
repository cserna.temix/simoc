package mx.gob.imss.simo.hospitalizacion.modificaciones.rules;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoEgresoEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;

@Component
public class BuscarEditarHospitalRules extends HospitalizacionCommonRules {

    @Autowired
    private HospitalizacionCommonHelper hospitalizacionCommonHelper;

    public void validarDiagnosticgoIngresoEgreso(DatosHospitalizacion datos) {

    }

    public void validarTipoPartoTopoCirugia(DatosHospitalizacion datos) {

    }

    public void validarPrimerSecundarioEgreso(DatosHospitalizacion datos) {

    }

    public void validarAgregadoMedico(DatosHospitalizacion datos) {

    }

    public void validarNss(DatosHospitalizacion nss) {

    }

    public List<Boolean> validarEdicion(DatosHospitalizacion datosHospitalizacion, Integer dias)
            throws HospitalizacionException {

        boolean intervencion, tococirugia, rn, egreso, diagnosticos;
        String fechaIntervencion = datosHospitalizacion.getDatosIntervenciones().getFechaIntervencion();
        if (datosHospitalizacion.getDatosIntervenciones().getProcedimientos() != null
                && !datosHospitalizacion.getDatosIntervenciones().getProcedimientos().isEmpty()) {
            fechaIntervencion = hospitalizacionCommonHelper.convertDateToString(
                    datosHospitalizacion.getDatosIntervenciones().getProcedimientos().get(0).getFechaIntervencion());
        }
        intervencion = validarPermisoEdicion(dias, fechaIntervencion);
        tococirugia = validarPermisoEdicion(dias, datosHospitalizacion.getDatosTococirugia().getFecAtencion());
        rn = tococirugia;
        egreso = validarPermisoEdicion(dias, datosHospitalizacion.getDatosEgreso().getFechaEgresoS());
        diagnosticos = egreso;
        List<Boolean> permisos = new ArrayList<Boolean>();
        permisos.add(intervencion);
        permisos.add(tococirugia);
        permisos.add(rn);
        permisos.add(egreso);
        permisos.add(diagnosticos);
        return permisos;
    }

    private boolean validarPermisoEdicion(Integer dias, String fechaFormato) {

        if (null == dias) {
            return true;
        } else if (null == fechaFormato || fechaFormato.isEmpty()) {
            return false;
        }
        Date fechaActual = new Date();
        Date fecha = hospitalizacionCommonHelper
                .agregarDiasAFecha(hospitalizacionCommonHelper.convertStringToDate(fechaFormato), dias);
        return hospitalizacionCommonHelper.comparaFechas(fecha, fechaActual);
    }

    /**
     * Metodo para validar si una fecha es mayor o igual al dia de hoy para
     * activar o desactivar la seccion
     * 
     * @param fechaCalculadaSeccion
     * @return
     */
    public boolean validarFechaSeccion(Date fechaCalculadaSeccion) {

        Date hoy = new Date();
        boolean activarSeccion = false;
        if (fechaCalculadaSeccion != null && fechaCalculadaSeccion.compareTo(hoy) >= BaseConstants.INDEX_CERO) {
            activarSeccion = true;
        }
        return activarSeccion;
    }

    public void validarNull(Integer sexo) throws HospitalizacionException {

        if (null == sexo) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_160);
        }

    }

    public void validarRepetidosEdicion(List<Procedimientos> listaProcedimientos, Procedimientos procedimiento)
            throws HospitalizacionException {

        int contador = 0;
        if (null != listaProcedimientos && !listaProcedimientos.isEmpty()) {
            for (Procedimientos p : listaProcedimientos) {
                if ((null != p) && (null != p.getClave() && !p.getClave().isEmpty())
                        && (null != procedimiento.getClave() && !procedimiento.getClave().isEmpty())) {
                    if (p.getClave().equals(procedimiento.getClave())) {
                        contador++;
                    }

                }
            }
        }
        if (contador > BaseConstants.INDEX_UNO) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_162),
                    getArray(procedimiento.getClave()));
        }

    }

    public void validarDXIguales(DatosHospitalizacion datos, String clave) throws HospitalizacionException {

        if (null != clave && !clave.isEmpty()) {
            List<String> dx = armarLista(datos);
            int contador = 0;
            for (String str : dx) {
                if (clave.equals(str)) {
                    contador++;
                    if (contador > 1) {
                        throw new HospitalizacionException(MensajesErrorConstants.ME_158);
                    }
                }
            }
        }
    }
    
    public void validarDXPorTipoDiagnostico(DatosHospitalizacion datos, String clave, String tipoDiagnostico) throws HospitalizacionException {

        if (null != clave && !clave.isEmpty()) {
            List<String> dx = armarListaXTipoDiagnostico(datos,tipoDiagnostico);
            int contador = 0;
            for (String str : dx) {
                if (clave.equals(str)) {
                    contador++;
                    if (contador > 1) {
                        throw new HospitalizacionException(MensajesErrorConstants.ME_158);
                    }
                }
            }
        }
    }
    
    public void validarDXInicialEgreso(DatosHospitalizacion datos, String clave, String tipoDiagnostico) throws HospitalizacionException {

        if (null != clave && !clave.isEmpty()) {
            List<String> dx = armarLista(datos);
            dx.remove(1);
            int contador = 0;
            for (String str : dx) {
                if (clave.equals(str)) {
                    contador++;
                    if (contador > 1) {
                        throw new HospitalizacionException(MensajesErrorConstants.ME_158);
                    }
                }
            }
        }
    }
    
    public void validarDXSinInicialEgreso(DatosHospitalizacion datos, String clave) throws HospitalizacionException {

        if (null != clave && !clave.isEmpty()) {
            List<String> dx = armarLista(datos);
            dx.remove(0);
            int contador = 0;
            for (String str : dx) {
                if (clave.equals(str)) {
                    contador++;
                    if (contador > 1) {
                        throw new HospitalizacionException(MensajesErrorConstants.ME_158);
                    }
                }
            }
        }
    }

    private List<String> armarLista(DatosHospitalizacion datos) {

        List<String> lista = new ArrayList<String>();
        lista.add(datos.getDatosEgreso().getCveInicialIngresoEgreso());
        lista.add(datos.getDatosEgreso().getCvePrincipalEgreso());
        lista.add(datos.getDatosEgreso().getCvePrimerDiagnosticoSecundario());
        lista.add(datos.getDatosEgreso().getClaveSegundoDiagnosticoSecundario());
        lista.add(datos.getDatosEgreso().getCveComplicacionIntraPrimera());
        lista.add(datos.getDatosEgreso().getCveComplicacionIntraSegunda());
	    lista.add(datos.getDatosEgreso().getCveCausaDefDirecta());
	    lista.add(datos.getDatosEgreso().getCveCausaDefBasica());
	    
        return lista;
    }
    
    private List<String> armarListaXTipoDiagnostico(DatosHospitalizacion datos, String tipoDiagnostico) {

        List<String> lista = new ArrayList<String>();
        
        lista.add(datos.getDatosEgreso().getCvePrimerDiagnosticoSecundario());
        lista.add(datos.getDatosEgreso().getClaveSegundoDiagnosticoSecundario());
        lista.add(datos.getDatosEgreso().getCveComplicacionIntraPrimera());
        lista.add(datos.getDatosEgreso().getCveComplicacionIntraSegunda());
        if(!CapturarEgresoConstants.REQUERIDO_DIAGNOSTICO_PRINCIPAL.equals(tipoDiagnostico) && !CapturarEgresoConstants.CAUSA_BASICA_DEFUNCION.equals(tipoDiagnostico) 
        		&& !CapturarEgresoConstants.CAUSA_DIRECTA_DEFUNCION.equals(tipoDiagnostico) ){
        	lista.add(datos.getDatosEgreso().getCveInicialIngresoEgreso());
        }
        if( !CapturarEgresoConstants.DIAGNOSTICO_INICIAL.equals(tipoDiagnostico)){
	        lista.add(datos.getDatosEgreso().getCveCausaDefDirecta());
	        lista.add(datos.getDatosEgreso().getCveCausaDefBasica());
	        //lista.add(datos.getDatosEgreso().getCvePrincipalEgreso());
        }
        
        	
        return lista;
    }

    public void validarCausaDirectaDefuncion(String cveDiagnostico, boolean validar) throws HospitalizacionException {

        if ((cveDiagnostico == null || cveDiagnostico.isEmpty()) && validar) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarEgresoConstants.CAUSA_DIRECTA_DEFUNCION));
        }
    }

    public void validarCausaBasicaDefuncion(String cveDiagnostico, boolean validar) throws HospitalizacionException {

        if ((cveDiagnostico == null || cveDiagnostico.isEmpty()) && validar) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarEgresoConstants.CAUSA_BASICA_DEFUNCION));
        }
    }

}
