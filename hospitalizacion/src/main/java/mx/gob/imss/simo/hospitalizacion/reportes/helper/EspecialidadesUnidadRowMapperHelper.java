package mx.gob.imss.simo.hospitalizacion.reportes.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.EspecialidadesUnidadVo;

public class EspecialidadesUnidadRowMapperHelper extends BaseHelper implements RowMapper<EspecialidadesUnidadVo> {

	@Override
	public EspecialidadesUnidadVo mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

		EspecialidadesUnidadVo especialidadesUnidadPojo = new EspecialidadesUnidadVo();

		especialidadesUnidadPojo.setCveDivision(resultSet.getInt(SQLColumnasConstants.CVE_DIVISION));
		especialidadesUnidadPojo.setDesDivision(resultSet.getString(SQLColumnasConstants.DES_DIVISION));
		especialidadesUnidadPojo.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
		especialidadesUnidadPojo.setDesEspecialidad(resultSet.getString(SQLColumnasConstants.DES_ESPECIALIDAD));
		especialidadesUnidadPojo.setDesUnidadMedica(resultSet.getString(SQLColumnasConstants.DES_UNIDAD_MEDICA));

		return especialidadesUnidadPojo;
	}
}
