package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class ProcedimientoCIE9 {

    private Long cveIngreso;
    private Date fecCreacion;
    private long numProcedimiento;
    private String cveCIE9;
    private String cveCirujano;

}
