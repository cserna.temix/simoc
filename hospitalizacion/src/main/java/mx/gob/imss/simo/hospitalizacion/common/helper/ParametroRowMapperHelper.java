package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;

public class ParametroRowMapperHelper extends BaseHelper implements RowMapper<Parametro> {

    @Override
    public Parametro mapRow(ResultSet resultSet, int rowId) throws SQLException {

        Parametro parametro = new Parametro();

        parametro.setClaveParametro(resultSet.getString(SQLColumnasConstants.CVE_PARAMETRO));
        parametro.setDescripcionParametro(resultSet.getString(SQLColumnasConstants.DES_PARAMETRO));
        parametro.setReferenciaParametro(resultSet.getString(SQLColumnasConstants.REF_PARAMETRO));

        return parametro;
    }

}
