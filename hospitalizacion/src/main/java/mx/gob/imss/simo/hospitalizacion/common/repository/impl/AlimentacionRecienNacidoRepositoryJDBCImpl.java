package mx.gob.imss.simo.hospitalizacion.common.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLUpdateConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.AlimentacionRecienNacidoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.AlimentacionRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.repository.AlimentacionRecienNacidoRepository;
import mx.gob.imss.simo.hospitalizacion.common.repository.BaseJDBCRepository;

@Repository("alimentacionRecienNacidoRepository")
public class AlimentacionRecienNacidoRepositoryJDBCImpl extends BaseJDBCRepository
        implements AlimentacionRecienNacidoRepository {

    public void guardar(final AlimentacionRecienNacido alimentacionRecienNacido) {

        if (null != alimentacionRecienNacido) {
            jdbcTemplate
                    .update(SQLUpdateConstants.ALIMENTACION_RECIEN_NACIDO_ADD,
                            new Object[] { alimentacionRecienNacido.getClave(),
                                    alimentacionRecienNacido.getDescripcion(),
                                    alimentacionRecienNacido.getFechaBaja() });
        }
    }

    public List<AlimentacionRecienNacido> buscar() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_ALIMENTACION_RN,
                new AlimentacionRecienNacidoRowMapperHelper());
    }

}
