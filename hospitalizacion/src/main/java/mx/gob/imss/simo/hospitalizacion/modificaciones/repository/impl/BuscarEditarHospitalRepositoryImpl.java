package mx.gob.imss.simo.hospitalizacion.modificaciones.repository.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLUpdateConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.EgresoDiagnosticoEdicionRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.EgresoRecienNacidoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.IngresoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.MovimientoIntraHospitalarioRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.ParametroRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.ProcedimientosEdicionRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.TococirugiaEdicionRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosEgreso;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosTococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.Diagnostico;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoDiagnosticoEdicion;
import mx.gob.imss.simo.hospitalizacion.common.model.EgresoRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientoCIE9;
import mx.gob.imss.simo.hospitalizacion.common.model.ProcedimientosEdicion;
import mx.gob.imss.simo.hospitalizacion.common.model.TococirugiaEdicion;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.hospitalizacion.modificaciones.repository.BuscarEditarHospitalRepository;
import mx.gob.imss.simo.model.DatosUsuario;

@Repository("buscarEditarHospitalRepository")
public class BuscarEditarHospitalRepositoryImpl extends HospitalizacionCommonRepositoryJDBCImpl
        implements BuscarEditarHospitalRepository {

    @Override
    public void guardarEdicionHospitalizacion(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario) {

    }

    public Parametro obtenerParametroTiempoEdicion(String perfil) {

        List<Parametro> parametro = jdbcTemplate.query(SQLConstants.BUSCAR_PARAMETRO, new Object[] { perfil },
                new ParametroRowMapperHelper());

        if (null != parametro && !parametro.isEmpty()) {
            return parametro.get(0);
        }
        return null;
    }

    public List<ProcedimientosEdicion> buscarProcedimientosParaEdicion(long claveIngreso) {

        List<ProcedimientosEdicion> procedimientos = jdbcTemplate.query(SQLConstants.BUSCAR_PROCEDIMIENTOS_EDICION,
                new Object[] { claveIngreso }, new ProcedimientosEdicionRowMapperHelper());
        return procedimientos;

    }

    @Override
    public List<TococirugiaEdicion> obtenerTococirugiaPaciente(Long claveIngreso) {

        List<TococirugiaEdicion> tocociruguaEdicion = jdbcTemplate.query(SQLConstants.BUSCAR_TOCOCIRUGIA_EDICION,
                new Object[] { claveIngreso }, new TococirugiaEdicionRowMapperHelper());

        if (null != tocociruguaEdicion && !tocociruguaEdicion.isEmpty()) {
            return tocociruguaEdicion;
        }
        return null;
    }

    public List<EgresoDiagnosticoEdicion> buscarEgresoDiagnosticoEdicion(Long claveIngreso) {

        List<EgresoDiagnosticoEdicion> procedimientos = jdbcTemplate.query(
                SQLConstants.BUSCAR_EGRESO_DIAGNOSTIOCO_EDICION, new Object[] { claveIngreso },
                new EgresoDiagnosticoEdicionRowMapperHelper());
        return procedimientos;
    }

    @Override
    public List<Diagnostico> obtenerCatalogoDeDiagnosticos(String claveDiagostico) {

        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IntervencionesQuirurgicas obtenerIntervencionesQx(Long claveIngreso) {

        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void actualizarProcedimientosCIE9(ProcedimientoCIE9 procedimientoCIE9) {

        jdbcTemplate.update(SQLUpdateConstants.BUSCAR_EDITAR_HOSPITAL_ACTUALIZA_SIT_PROCEDIMIENTO_CIE9,
                new Object[] { procedimientoCIE9.getCveCIE9(), procedimientoCIE9.getCveIngreso(),
                        procedimientoCIE9.getNumProcedimiento() });
    }

    @Override
    public void actualizarIntervencionQuirurgica(DatosUsuario datosUsuario, DatosHospitalizacion datosHospitalizacion) {

        jdbcTemplate.update(SQLUpdateConstants.BUSCAR_EDITAR_HOSPITAL_ACTUALIZA_SIT_INTERVENCION_QUIRURGICA,
                new Object[] { datosUsuario.getUsuario(), new Date(),
                        datosHospitalizacion.getDatosPaciente().getClaveIngreso() });
    }

    @Override
    public void actualizarRecienNacido(DatosHospitalizacion datosHospitalizacion, DatosRecienNacido datosRecienNacido) {

        jdbcTemplate
                .update(SQLUpdateConstants.BUSCAR_EDITAR_HOSPITAL_ACTUALIZA_SIT_RECIEN_NACIDO,
                        new Object[] { datosRecienNacido.getTalla(), datosRecienNacido.getPeso(),
                                datosHospitalizacion.getDatosPaciente().getClaveIngreso(),
                                datosRecienNacido.getIdNacido() });
    }

    @Override
    public void actualizarTocoCirugia(DatosTococirugia datosTococirugia, DatosPaciente datosPaciente,
            DatosUsuario datosUsuario) {

        jdbcTemplate.update(SQLUpdateConstants.BUSCAR_EDITAR_HOSPITAL_ACTUALIZA_SIT_TOCOCIRUGIA,
                new Object[] { new Date(), datosTococirugia.getTipoParto(), datosUsuario.getUsuario(),
                        datosPaciente.getClaveIngreso() });
    }

    @Override
    public void actualizarPaciente(DatosRecienNacido datosRN) {

        jdbcTemplate.update(SQLUpdateConstants.BUSCAR_EDITAR_HOSPITAL_ACTUALIZA_SIC_PACIENTE,
                new Object[] { datosRN.getSexo(), datosRN.getCvePaciente() });
    }

    @Override
    public void actualizarDiagnosticoEgresoBusquedaActualizacion(String cveInicialIngresoEgreso, Long claveIngreso,
            String clave) {

        jdbcTemplate.update(SQLUpdateConstants.BUSCAR_EDITAR_HOSPITAL_ACTUALIZA_SIT_DIAGNOSTICO,
                new Object[] { cveInicialIngresoEgreso, claveIngreso, clave });
    }

    @Override
    public void actualizarEgreso(DatosEgreso datosEgreso, DatosPaciente datosPaciente, DatosUsuario datosUsuario) {

        jdbcTemplate.update(SQLUpdateConstants.BUSCAR_EDITAR_HOSPITAL_ACTUALIZA_SIT_EGRESO,
                new Object[] { new Date(), datosUsuario.getUsuario(), datosEgreso.getMotivoAlta(), datosPaciente.getClaveIngreso()});
    }
    
    @Override
    public boolean buscarIngresoRN(Long cvePaciente) {

        return !jdbcTemplate.query(SQLConstants.BUSCAR_INGRESO_VIGENTE_CLAVE_PACIENTE, new Object[] { cvePaciente },
                new IngresoRowMapperHelper()).isEmpty();
    }

    @Override
    public List<EgresoRecienNacido> buscarEgresosRN(Long cveIngreso) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_EGRESO_RN, new Object[] { cveIngreso },
                new EgresoRecienNacidoRowMapperHelper());
    }

    public MovimientoIntraHospitalario buscarMovimientos(Long claveIngreso) {

        List<MovimientoIntraHospitalario> movimientos = jdbcTemplate.query(
                SQLConstants.BUSCAR_MOVIMIENTOS_INTRA_EDICION, new Object[] { claveIngreso },
                new MovimientoIntraHospitalarioRowMapperHelper());
        if (null != movimientos && !movimientos.isEmpty()) {
            return movimientos.get(0);
        }
        return null;
    }

}
