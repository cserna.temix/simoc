package mx.gob.imss.simo.hospitalizacion.modificaciones.rules;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;
import mx.gob.imss.simo.hospitalizacion.modificaciones.helper.ModificacionDatosPacienteHelper;

@Component
public class ModificacionDatosPacienteRules extends HospitalizacionCommonRules {
	
	 @Autowired
	 private ModificacionDatosPacienteHelper modificacionDatosPacienteHelper;
    
    public Ingreso validarIngresoVigente(List<Ingreso> ingresos, Long cvePaciente, boolean uci)
            throws HospitalizacionException {

        Ingreso ingreso = null;
        if (ingresos != null && !ingresos.isEmpty()) {
            for (Ingreso in : ingresos) {
                if (in.getCvePaciente() == cvePaciente) {
                    ingreso = in;
                    break;
                }
            }
            if (ingreso != null && !uci) {
                String fechaIngreso = new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(ingreso.getFecIngreso());
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_047), getArray(fechaIngreso));
            }
        }
        return ingreso;
    }
    
    public void validarFechaModificacionMayorIngreso(Date fechaIngreso, Date fechaEgreso) throws HospitalizacionException {

        if (validarFecha(fechaIngreso, fechaEgreso)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_604);
        }
    }
    
    public boolean compararFechas(Date fechaA, Date fechaB) {

        if (null == fechaA && null == fechaB) {
            return true;
        }
        if (null != fechaA && fechaA.after(fechaB)) {
            return true;
        }
        String fechaUno = modificacionDatosPacienteHelper.converDateHoraToString(fechaA);
        String fechaDos = modificacionDatosPacienteHelper.converDateHoraToString(fechaB);

        if (fechaUno.equals(fechaDos)) {
            return true;
        }
        return false;
    }
    
    public void validarFechaModificacionMayorTococirugia(Date fechaIngreso, Date fechaEgreso)
            throws HospitalizacionException {

        if (validarFecha(fechaIngreso, fechaEgreso)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_605);
        }
    }
    
    public void validarFechaModificacionMayorIntervencionQx(Date fechaIngreso, Date fechaEgreso)
            throws HospitalizacionException {

        if (validarFecha(fechaIngreso, fechaEgreso)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_606);
        }
    }
    
    public void validarFechaModificacionMayorMovIntra(Date fechaIngreso, Date fechaEgreso)
            throws HospitalizacionException {

        if (validarFecha(fechaIngreso, fechaEgreso)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_607);
        }
    }

}
