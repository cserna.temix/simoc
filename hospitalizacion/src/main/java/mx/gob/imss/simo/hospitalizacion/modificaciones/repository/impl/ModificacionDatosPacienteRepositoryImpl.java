package mx.gob.imss.simo.hospitalizacion.modificaciones.repository.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLUpdateConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SecuenciasEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.PacienteRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.repository.BaseJDBCRepository;
import mx.gob.imss.simo.hospitalizacion.modificaciones.repository.ModificacionDatosPacienteRepository;

@Repository
public class ModificacionDatosPacienteRepositoryImpl extends BaseJDBCRepository
        implements ModificacionDatosPacienteRepository {

    @Transactional
    @Override
    public void guardarModificacionDatosPaciente(DatosPaciente datosPacienteModificados, String nssOriginal,
            String agregado, Date fechaModificacion) throws HospitalizacionException {

        Paciente paciente = buscarPacienteCVEPaciente(nssOriginal, agregado);
        actualizaUltimoPaciente(0, paciente.getCvePaciente(), fechaModificacion);
        Paciente pacienteAcceder = buscarPacienteCVEPaciente(datosPacienteModificados.getNss(), datosPacienteModificados.getAgregadoMedico());
        if(pacienteAcceder != null && pacienteAcceder.isIndAcceder()){
        	insertaNuevaModificacionDatosPacienteAcceder(paciente,pacienteAcceder, fechaModificacion);
        }else{
         insertaNuevaModificacionDatosPaciente(paciente, datosPacienteModificados, fechaModificacion);
        }
        
        actulizaIngresoPaciente(datosPacienteModificados, paciente.getCvePaciente());
    }

    private void actualizaUltimoPaciente(int ultimoRegistro, Long cvePaciente, Date fechaModificacion) {

        jdbcTemplate.update(SQLUpdateConstants.ACTUALIZA_ULTIMO_MODIFICAR_PACIENTE, ultimoRegistro, fechaModificacion, cvePaciente);

    }

    private Paciente buscarPacienteCVEPaciente(String refNss, String agregado) {

        List<Paciente> pacientes = jdbcTemplate.query(SQLCatalogosConstants.BUSCAR_PACIENTE_NSS_AGREGADO_MEDICO_NO_RN,
                new Object[] { refNss, agregado }, new PacienteRowMapperHelper());
        if(pacientes.isEmpty()){
        	return null;
        }else{
        	return pacientes.get(0);
        }
    }
        
    private Paciente buscarPacienteCVEPacientePadre(String refNss, String agregado, Long cvePaciente) {

        List<Paciente> pacientes = jdbcTemplate.query(SQLCatalogosConstants.BUSCAR_PACIENTE_POR_NSS_CVE_PACIENTE_PADRE,
                new Object[] { refNss, agregado, cvePaciente }, new PacienteRowMapperHelper());
        return pacientes.get(0);

    }

    private void insertaNuevaModificacionDatosPaciente(Paciente datosActuales, DatosPaciente datosPacienteModificados, Date fechaModificacion) {

        datosPacienteModificados.setIndUltimoRegistro(0L);
        long secuencia = obtenerSiguienteValorSecuencia(SecuenciasEnum.PACIENTE);

        jdbcTemplate.update(SQLUpdateConstants.MODIFICACION_DATOS_PACIENTE_NUEVOS,
                new Object[] { secuencia, datosActuales.getCvePaciente(), datosActuales.getCveIdPersona(),
                        datosPacienteModificados.getNss(), datosPacienteModificados.getAgregadoMedico(),
                        datosPacienteModificados.getNombre(), datosPacienteModificados.getApellidoPaterno(),
                        datosPacienteModificados.getApellidoMaterno(), datosActuales.getFecNacimiento(),
                        datosActuales.getNumEdadSemanas(), datosActuales.getCveCurp(), datosActuales.getCveIdee(),
                        datosActuales.isIndAcceder(), datosActuales.isIndVigencia(), datosActuales.getCveSexo(),
                        datosActuales.getRefTipoPension(), datosActuales.getRefRegistroPatronal(), new Date(),
                        datosActuales.getFecConsulta(), datosPacienteModificados.getClavePresupuestal(),
                        datosActuales.isIndRecienNacido(), datosActuales.isIndDerechohabiente(), 1,
                        datosActuales.getNumEdadAnios(), fechaModificacion });
    }
    
    private void insertaNuevaModificacionDatosPacienteAcceder(Paciente datosActuales, Paciente datosPacienteModificados, Date fechaModificacion) {

        jdbcTemplate.update(SQLUpdateConstants.MODIFICACION_DATOS_PACIENTE_ACCEDER,
                new Object[] { datosActuales.getCvePaciente(), fechaModificacion, datosPacienteModificados.getCvePaciente(),
                        datosPacienteModificados.getRefNss(), datosPacienteModificados.getRefAgregadoMedico()});
    }

    public void actulizaIngresoPaciente(DatosPaciente datosPacienteModificados, Long cvePacientePadre) {

        Paciente paciente = buscarPacienteCVEPacientePadre(datosPacienteModificados.getNss(),
                datosPacienteModificados.getAgregadoMedico(), cvePacientePadre);

        jdbcTemplate.update(SQLUpdateConstants.ACTUALIZA_PACIENTE_INGRESO_MODIFICADO, paciente.getCvePaciente(),
                paciente.getCvePacientePadre());

    }
}
