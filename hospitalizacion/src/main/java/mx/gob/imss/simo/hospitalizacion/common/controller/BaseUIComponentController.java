package mx.gob.imss.simo.hospitalizacion.common.controller;

import javax.annotation.PostConstruct;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import lombok.Data;

@Data
public class BaseUIComponentController extends BaseController {

    private static final long serialVersionUID = 2919809102671455638L;

    private AutoComplete autoDelegacion;
    private AutoComplete autoEspecialidad;
    private AutoComplete autoEspecialidadHospitalizacion;
    private AutoComplete autoPlanificacion;
    private InputMask maskFecha;
    private InputMask maskHoraIngreso;
    private InputText textAgregadoMedico;
    private InputText textApellidoMaterno;
    private InputText textApellidoPaterno;
    private InputText textCama;
    private InputText textCamaRecienNacido;
    private InputText textCantidad;
    private InputText textClave;
    private InputText textDelegacion;
    private InputText textDescripcion;
    private InputText textEdad;
    private InputText textMatricula;
    private InputText textMedico;
    private InputText textNombre;
    private InputText textEspecialidadEgreso;
    private InputText textNss;
    private InputText textNumero;
    private InputText textNumeroCama;
    private InputText textNumeroPaciente;
    private SelectOneMenu selectCama;
    private SelectOneMenu selectDivision;
    private SelectOneMenu selectEspecialidadHospitalizacion;
    private SelectOneMenu selectTipoIngreso;
    private SelectOneMenu selectTipoPrograma;
    
    private transient CommandButton extemporaneoSi;
    private transient CommandButton extemporaneoNo;

    @PostConstruct
    public void init() {

        super.init();
        autoDelegacion = new AutoComplete();
        autoEspecialidad = new AutoComplete();
        autoEspecialidadHospitalizacion = new AutoComplete();
        autoPlanificacion = new AutoComplete();
        maskFecha = new InputMask();
        maskHoraIngreso = new InputMask();
        textAgregadoMedico = new InputText();
        textNss = new InputText();
        textApellidoMaterno = new InputText();
        textApellidoPaterno = new InputText();
        textCama = new InputText();
        textCamaRecienNacido = new InputText();
        textCantidad = new InputText();
        textClave = new InputText();
        textDelegacion = new InputText();
        textDescripcion = new InputText();
        textEdad = new InputText();
        textMatricula = new InputText();
        textMedico = new InputText();
        textNombre = new InputText();
        //textEspecialidadEgreso.resetValue();
        textEspecialidadEgreso = new InputText();
        textNss = new InputText();
        textNumero = new InputText();
        textNumeroCama = new InputText();
        textNumeroPaciente = new InputText();
        selectCama = new SelectOneMenu();
        selectDivision = new SelectOneMenu();
        selectEspecialidadHospitalizacion = new SelectOneMenu();
        selectTipoIngreso = new SelectOneMenu();
        selectTipoPrograma = new SelectOneMenu();
        extemporaneoSi = new CommandButton();
        extemporaneoNo = new CommandButton();
    }

}
