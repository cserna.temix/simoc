package mx.gob.imss.simo.hospitalizacion.modificaciones.service;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosTococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.model.DatosUsuario;

public interface BuscarEditarHospitalServices extends HospitalizacionCommonServices {

    void validarNss(String nss) throws HospitalizacionException;

    void validarAgregadoMedico(DatosHospitalizacion datos) throws HospitalizacionException;

    DatosHospitalizacion obtenerDatosParaEdicion(DatosPaciente datosPaciente);

    void validarPesoRecienNacido(DatosRecienNacido datosRecienNacido) throws HospitalizacionException;

    void validarTallaRecienNacido(DatosRecienNacido rn) throws HospitalizacionException;

    void validarTipoPartoTopoCirugia(DatosHospitalizacion datos) throws HospitalizacionException;

    void validarDiagnosticoInicialEgreso(DatosHospitalizacion datos, String agregadoMedico)
            throws HospitalizacionException;

    void validarDiagnosticoPrincipalEgreso(DatosHospitalizacion datos, String agregadoMedico)
            throws HospitalizacionException;

    void validarPrimerSecundarioEgreso(DatosHospitalizacion datos, String agregadoMedico)
            throws HospitalizacionException;

    void validarSegundoSecundarioEgreso(DatosHospitalizacion datos, String agregadoMedico)
            throws HospitalizacionException;

    void validarPrimeraComplicacionIntraEgreso(DatosHospitalizacion datos) throws HospitalizacionException;

    void validarSegundaComplicacionIntraEgreso(DatosHospitalizacion datos) throws HospitalizacionException;

    List<Boolean> validarEdicion(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException;

    void guardarEdicionHospitalizacion(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException;

    boolean validarActivarSeccion(Date fechaFormato, String referencia, int valor) throws HospitalizacionException;

    String obtenerDescripcionProcedimiento(String clave);

    void validarCausaBasicaDefuncion(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    void validarCausaDirectaDefuncion(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    String obtenerDescripcionDX(String cveDX);

    DatosRecienNacido armarRN(DatosTococirugia datosTococirugia, int numero);

    void validarSexoRn(DatosRecienNacido rn) throws HospitalizacionException;

    void validarMotivoAlta(DatosHospitalizacion datos) throws HospitalizacionException;

    int buscarSiguienteFoco(List<Procedimientos> procedimientos);

    Procedimientos crearProcedimiento(List<Procedimientos> listaProcedimientos, int i);

    boolean puedeActivarEdicion(DatosHospitalizacion datosHospitalizacion, String clavePresupuestal, String perfil);

    boolean validarActivarGuardar(Boolean[] requeridos);

    Integer obtieneEdadAgregadoMedico(String agregadoMedico);

}
