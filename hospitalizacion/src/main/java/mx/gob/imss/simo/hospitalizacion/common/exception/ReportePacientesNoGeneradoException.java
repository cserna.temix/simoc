package mx.gob.imss.simo.hospitalizacion.common.exception;

public class ReportePacientesNoGeneradoException extends BaseException {

    public ReportePacientesNoGeneradoException(String... mensajes) {
        super(mensajes);
    }

}
