package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class ProcedimientosRecienNacido {

    private String claveProcedimientos;
    private String descProcedimientos;

}
