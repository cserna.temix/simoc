package mx.gob.imss.simo.hospitalizacion.tococirugia.services;

import java.util.Date;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.RangoLubchenco;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.model.DatosUsuario;

public interface CapturarTococirugiaServices extends HospitalizacionCommonServices {

    void validarFechaAtencion(String fechaAtencion, String clavePresupuestal) throws HospitalizacionException;

    void validarSala(String cvePresupuestal, String cveSala) throws HospitalizacionException;

    void validarNss(String nss) throws HospitalizacionException;

    void validarCama(String cveCama) throws HospitalizacionException;

    void validarCandidatoTococirugia(String agregadoMedico) throws HospitalizacionException;

    boolean validarAtencionTipoParto(Integer cveAtencion);

    void validarAtencion(String fechaAtencion, DatosHospitalizacion datosHospitalizacion)
            throws HospitalizacionException;

    void validarTipoParto(Integer cveTipoParto) throws HospitalizacionException;
    
    void validarEpisiotomia(String episiotomia)throws HospitalizacionException;

    void validarHoraParto(DatosPaciente datosPaciente, String horaParto, String fechaIngresoTococirugia)
            throws HospitalizacionException;

    void validarNacidoModal(String cveTipoNacido) throws HospitalizacionException;

    void validarSexoModal(Integer cveTipoSexo) throws HospitalizacionException;

    List<DatosRecienNacido> inicializarRecienNacidos();

    List<DatosRecienNacido> prepararListaRecienNacidos(int indice, DatosRecienNacido datosRecienNacido,
            List<DatosRecienNacido> listaRecienNacido);

    @Override
    void validarMatricula(String matricula) throws HospitalizacionException;

    @Override
    void validarPlanificacion(String cvePlanificacion) throws HospitalizacionException;

    void validarTotalNacidos(Integer totalNacidos) throws HospitalizacionException;

    DatosCatalogo elementoSelecionadoPlanificacion(List<String> listaPlanificacion, AutoComplete planificacionComplete,
            List<DatosCatalogo> catalogoPlanificacion);

    void validarPesoRecienNacido(Integer peso) throws HospitalizacionException;

    void validarTallaRecienNacido(Integer talla) throws HospitalizacionException;

    void validarPerimetroCefalicoRecienNacido(Integer perimetroCefalico) throws HospitalizacionException;

    void validarSemanasGestacionRecienNacido(Integer semanasGestacion) throws HospitalizacionException;

    void validarApgar1RecienNacido(Integer apgar1) throws HospitalizacionException;

    void validarApgar5RecienNacido(Integer apgar5) throws HospitalizacionException;

    void validarHoraDefuncion(String fechaParto, String horaParto, String horaDefuncion)
            throws HospitalizacionException;

    RangoLubchenco obtenerRangoLubchenco(int semanasGestacion, int peso);

    List<DatosRecienNacido> agregarRegistroListaRN(DatosHospitalizacion datosHospitalizacion,
            DatosRecienNacido datosRecienNacido);

    DatosRecienNacido inicializarDatosRecienNacido(DatosHospitalizacion datosHospitalizacion,
            DatosRecienNacido datosRecienNacido);

    boolean sePuedeActivarGuardar(Boolean[] requeridos);

    void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario) throws HospitalizacionException;

    void validarFechaIngreso(Date fechaIngreso, String fechaToco) throws HospitalizacionException;

    void validarCamaRequerida(String cveCama) throws HospitalizacionException;
    
    void validarGesta(Integer gesta) throws HospitalizacionException;
    
    void actualizarMetodoAnticonceptivoTococirugia(int cve_metodo, int cantidad, long cve_tococirugia) throws HospitalizacionException;
    
    List<DatosCatalogo> obtenerCatalogoEpisiotomia();

}
