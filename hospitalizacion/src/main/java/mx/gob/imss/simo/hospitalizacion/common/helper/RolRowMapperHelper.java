package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Rol;

public class RolRowMapperHelper extends BaseHelper implements RowMapper<Rol> {

    @Override
    public Rol mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        Rol rol = new Rol();
        rol.setCveRol(resultSet.getString(SQLColumnasConstants.CVE_ROL));
        rol.setDesRol(resultSet.getString(SQLColumnasConstants.DES_ROL));
        rol.setRefRol(resultSet.getString(SQLColumnasConstants.REF_ROL));

        return rol;
    }

}
