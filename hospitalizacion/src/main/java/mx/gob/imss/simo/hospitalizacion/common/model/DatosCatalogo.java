package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class DatosCatalogo {

    private String clave;
    private String descripcion;

}
