package mx.gob.imss.simo.hospitalizacion.tococirugia.services.impl;

import java.util.Date;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.busquedas.repository.BuscarPacienteRepository;
import mx.gob.imss.simo.hospitalizacion.common.constant.AtencionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TococirugiaConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosRecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.RangoLubchenco;
import mx.gob.imss.simo.hospitalizacion.common.model.RecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.rules.CapturarIntervencionesQuirurgicasRules;
import mx.gob.imss.simo.hospitalizacion.tococirugia.helper.CapturarTococirugiaHelper;
import mx.gob.imss.simo.hospitalizacion.tococirugia.repository.TococirugiaRepository;
import mx.gob.imss.simo.hospitalizacion.tococirugia.rules.CapturarTococirugiaRules;
import mx.gob.imss.simo.hospitalizacion.tococirugia.services.CapturarTococirugiaServices;
import mx.gob.imss.simo.model.DatosUsuario;

@Service("capturarTococirugiaServices")
public class CapturarTococirugiaServicesImpl extends HospitalizacionCommonServicesImpl
        implements CapturarTococirugiaServices {

    @Autowired
    private CapturarTococirugiaHelper capturarTococirugiaHelper;

    @Autowired
    private CapturarTococirugiaRules capturarTococirugiaRules;

    @Autowired
    private TococirugiaRepository tococirugiaRepository;
    @Autowired
    private BuscarPacienteRepository buscarPacienteRepository;

    @Autowired
    private CapturarIntervencionesQuirurgicasRules CapturarIntervencionesQuirurgicasRules;

    @Override
    public void validarFechaAtencion(String fechaAtencion, String clavePresupuestal) throws HospitalizacionException {

        capturarTococirugiaRules.validarFormatoFecha(fechaAtencion);
        super.validarReporteRelacionPacientesGenerado(clavePresupuestal, fechaAtencion);
        super.validarCamasConDosPacientes(clavePresupuestal, fechaAtencion);
        super.validarPeriodoAnteriorCerrado(clavePresupuestal, fechaAtencion,
                TipoCapturaEnum.HOSPITALIZACION.getClave());
        validarFechaEnPeriodoActualActivo(fechaAtencion,
                getHospitalizacionCommonHelper().convertStringToDate(fechaAtencion), clavePresupuestal,
                TipoCapturaEnum.HOSPITALIZACION.getClave());
        capturarTococirugiaRules.validarFechaActual(fechaAtencion);
    }

    @Override
    public void validarSala(String cvePresupuestal, String cveSala) throws HospitalizacionException {

        capturarTococirugiaRules.validarSala(cvePresupuestal, cveSala);
    }

    @Override
    public void validarNss(String nss) throws HospitalizacionException {

        capturarTococirugiaRules.validarNss(nss);
    }

    @Override
    public void validarCama(String cveCama) throws HospitalizacionException {

        capturarTococirugiaRules.validarCama(cveCama);
    }

    @Override
    public void validarCandidatoTococirugia(String agregadoMedico) throws HospitalizacionException {

        capturarTococirugiaRules.validarCandidatoTococirugia(agregadoMedico);

    }

    @Override
    public boolean validarAtencionTipoParto(Integer cveAtencion) {

        boolean tipoParto = false;
        if (cveAtencion == AtencionEnum.PARTO.getClave()) {
            tipoParto = true;
        }
        return tipoParto;
    }

    @Override
    public void validarAtencion(String fechaAtencion, DatosHospitalizacion datosHospitalizacion)
            throws HospitalizacionException {

        capturarTococirugiaRules.validarAtencion(capturarTococirugiaHelper.convertStringToDate(fechaAtencion),
                datosHospitalizacion);
    }

    @Override
    public void validarTipoParto(Integer cveTipoParto) throws HospitalizacionException {

        capturarTococirugiaRules.validarTipoParto(cveTipoParto);
    }
    
    @Override
    public void validarEpisiotomia(String episiotomia)throws HospitalizacionException {

        capturarTococirugiaRules.validarEpisiotomia(episiotomia);
    }

    @Override
    public void validarHoraParto(DatosPaciente datosPaciente, String horaParto, String fechaIngresoTococirugia)
            throws HospitalizacionException {

        Ingreso ingreso = tococirugiaRepository.buscarIngresoPorDatosPaciente(datosPaciente);
        capturarTococirugiaRules.validarHoraParto(ingreso.getFecIngreso(), horaParto,
                getHospitalizacionCommonHelper().convertStringToDate(fechaIngresoTococirugia));
    }

    @Override
    public List<DatosRecienNacido> inicializarRecienNacidos() {

        return capturarTococirugiaHelper.inicializarRecienNacidos();
    }

    @Override
    public List<DatosRecienNacido> prepararListaRecienNacidos(int indice, DatosRecienNacido datosRecienNacido,
            List<DatosRecienNacido> listaRecienNacido) {

        return capturarTococirugiaHelper.prepararListaRecienNacidos(datosRecienNacido, listaRecienNacido);
    }

    @Override
    public void validarNacidoModal(String tipoNacido) throws HospitalizacionException {

        capturarTococirugiaRules.validarNacidoModal(tipoNacido);

    }

    @Override
    public void validarSexoModal(Integer cveTipoSexo) throws HospitalizacionException {

        capturarTococirugiaRules.validarSexoModal(cveTipoSexo);

    }

    @Override
    public void validarMatricula(String matricula) throws HospitalizacionException {

        capturarTococirugiaRules.validarMatricula(matricula);
    }

    @Override
    public void validarPlanificacion(String cvePlanificacion) throws HospitalizacionException {

        capturarTococirugiaRules.validarPlanificacion(cvePlanificacion);
    }

    @Override
    public void validarTotalNacidos(Integer totalNacidos) throws HospitalizacionException {

        Parametro parametro = tococirugiaRepository
                .obtenerParametroMaxRecienNacido(TococirugiaConstants.NUM_MAX_RECIEN_NACIDOS);
        int maxRecienNacidos = Integer.parseInt(parametro.getReferenciaParametro());
        capturarTococirugiaRules.validarTotalNacidos(totalNacidos, maxRecienNacidos);

    }

    @Override
    public DatosCatalogo elementoSelecionadoPlanificacion(List<String> listaPlanificacion,
            AutoComplete planificacionComplete, List<DatosCatalogo> catalogoPlanificacion) {

        return capturarTococirugiaHelper.elementoSelecionadoPlanificacion(listaPlanificacion, planificacionComplete,
                catalogoPlanificacion);
    }

    @Override
    public void validarPesoRecienNacido(Integer peso) throws HospitalizacionException {

        capturarTococirugiaRules.validarPesoRecienNacido(peso);

    }

    @Override
    public void validarTallaRecienNacido(Integer talla) throws HospitalizacionException {

        capturarTococirugiaRules.validarTallaRecienNacido(talla);

    }

    @Override
    public void validarPerimetroCefalicoRecienNacido(Integer perimetroCefalico) throws HospitalizacionException {

        capturarTococirugiaRules.validarPerimetroCefalicoRecienNacido(perimetroCefalico);

    }

    @Override
    public void validarSemanasGestacionRecienNacido(Integer semanasGestacion) throws HospitalizacionException {

        capturarTococirugiaRules.validarSemanasGestacionRecienNacido(semanasGestacion);

    }

    @Override
    public void validarApgar1RecienNacido(Integer apgar1) throws HospitalizacionException {

        capturarTococirugiaRules.validarApgar1RecienNacido(apgar1);

    }

    @Override
    public void validarApgar5RecienNacido(Integer apgar5) throws HospitalizacionException {

        capturarTococirugiaRules.validarApgar5RecienNacido(apgar5);

    }

    @Override
    public RangoLubchenco obtenerRangoLubchenco(int semanasGestacion, int peso) {

        return tococirugiaRepository.obtenerRangoLubchenco(semanasGestacion, peso).get(0);
    }

    @Override
    public List<DatosRecienNacido> agregarRegistroListaRN(DatosHospitalizacion datosHospitalizacion,
            DatosRecienNacido datosRecienNacido) {

        capturarTococirugiaHelper.agregarRegistroRecienNacido(datosHospitalizacion, datosRecienNacido);
        return datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido();
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException {
    	
        try {
            tococirugiaRepository.guardarTococirugia(
                    capturarTococirugiaHelper.prepararTococirugia(datosHospitalizacion, datosUsuario));
           
           if(datosHospitalizacion.getDatosTococirugia().getAtencion().equals(TococirugiaConstants.TipoAtencionEnum.PARTO.getClaveTipoAtencion())){
            	if (null != datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido()
                        && !datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido().isEmpty()) {
                    for (DatosRecienNacido dn : datosHospitalizacion.getDatosTococirugia().getListDatosRecienNacido()) {
                        RangoLubchenco rangoLubchenco = tococirugiaRepository
                                .obtenerRangoLubchenco(dn.getSemanasGestacion(), dn.getPeso()).get(0);
                        long cvePaciente = buscarPacienteRepository.guardarPaciente(
                                capturarTococirugiaHelper.prepararPaciente(datosHospitalizacion.getDatosPaciente(), dn,
                                		datosHospitalizacion.getDatosTococirugia().getFecAtencion()));
                        RecienNacido recienNacido = capturarTococirugiaHelper.prepararRecienNacido(datosHospitalizacion, dn,
                                datosUsuario, cvePaciente, rangoLubchenco.getRango());
                        tococirugiaRepository.guardarRecienNacido(recienNacido);
                    }
                }else{
                	 throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_020));
                }
            }
            
        } catch (Exception e) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_020), e);
        }
    }

    @Override
    public boolean sePuedeActivarGuardar(Boolean[] requeridos) {

        return CapturarIntervencionesQuirurgicasRules.sePuedeActivarGuardar(requeridos);
    }

    @Override
    public DatosRecienNacido inicializarDatosRecienNacido(DatosHospitalizacion datosHospitalizacion,
            DatosRecienNacido datosRecienNacido) {

        return capturarTococirugiaHelper.inicializarDatosRecienNacido(datosHospitalizacion, datosRecienNacido);
    }

    public CapturarTococirugiaHelper getCapturarTococirugiaHelper() {

        return capturarTococirugiaHelper;
    }

    public void setCapturarTococirugiaHelper(CapturarTococirugiaHelper capturarTococirugiaHelper) {

        this.capturarTococirugiaHelper = capturarTococirugiaHelper;
    }

    public CapturarTococirugiaRules getCapturarTococirugiaRules() {

        return capturarTococirugiaRules;
    }

    public void setCapturarTococirugiaRules(CapturarTococirugiaRules capturarTococirugiaRules) {

        this.capturarTococirugiaRules = capturarTococirugiaRules;
    }

    @Override
    public void validarHoraDefuncion(String fechaParto, String horaParto, String horaDefuncion)
            throws HospitalizacionException {

        capturarTococirugiaRules.validarHoraDefuncion(getHospitalizacionCommonHelper().convertStringToDate(fechaParto),
                horaParto, horaDefuncion);
    }

    @Override
    public void validarFechaIngreso(Date fechaIngreso, String fechaToco) throws HospitalizacionException {

        capturarTococirugiaRules.validarFechaIngreso(capturarTococirugiaHelper.convertStringToDate(fechaToco),
                fechaIngreso);
    }

    @Override
    public void validarCamaRequerida(String cveCama) throws HospitalizacionException {

        capturarTococirugiaRules.validarCamaRequerida(cveCama);
    }

	@Override
	public void validarGesta(Integer gesta) throws HospitalizacionException {
		capturarTococirugiaRules.validarGesta(gesta);
	}
	
	@Override
	public List<DatosCatalogo> obtenerCatalogoEpisiotomia(){
		return capturarTococirugiaHelper.armarCatalogoEpisiotomia();
	}
	
	

	@Override
	public void actualizarMetodoAnticonceptivoTococirugia(int cve_metodo, int cantidad, long cve_tococirugia)
			throws HospitalizacionException {
		tococirugiaRepository.actualizarMetodoAnticonceptivoTococirugia(cve_metodo, cantidad, cve_tococirugia);
		
	}

}
