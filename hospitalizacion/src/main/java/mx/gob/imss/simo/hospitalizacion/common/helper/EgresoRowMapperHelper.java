package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Egreso;

public class EgresoRowMapperHelper extends BaseHelper implements RowMapper<Egreso> {

    @Override
    public Egreso mapRow(ResultSet resultSet, int rowId) throws SQLException {

        Egreso egreso = new Egreso();
        egreso.setClaveEgreso(resultSet.getLong(SQLColumnasConstants.CVE_EGRESO));
        egreso.setClaveCapturista(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA));
        egreso.setClaveCapturistaActualiza(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA_ACTUALIZA));
        egreso.setFechaCreacion(resultSet.getDate(SQLColumnasConstants.FEC_CREACION));
        egreso.setFechaActualizacion(resultSet.getDate(SQLColumnasConstants.FEC_ACTUALIZACION));
        egreso.setFechaEgreso(resultSet.getDate(SQLColumnasConstants.FEC_EGRESO));
        egreso.setClaveTipoEgreso(resultSet.getLong(SQLColumnasConstants.CVE_TIPO_EGRESO));
        egreso.setRefJustificaCancelacion(resultSet.getString(SQLColumnasConstants.REF_JUSTIFICA_CANCELACION));
        egreso.setClaveTamiz(resultSet.getLong(SQLColumnasConstants.CVE_TAMIZ));
        egreso.setClaveMetodoAnticonceptivo(resultSet.getLong(SQLColumnasConstants.CVE_METODO_ANTICONCEPTIVO));
        egreso.setCantMetodoAnticonceptivo(resultSet.getLong(SQLColumnasConstants.CAN_METODO_ANTICONCEPTIVO));
        egreso.setNumPesoRn(resultSet.getLong(SQLColumnasConstants.NUM_PESO_RN));
        egreso.setClaveAlimentacionRn(resultSet.getLong(SQLColumnasConstants.CVE_ALIMENTACION_RN));
        egreso.setClaveMotivoAlta(resultSet.getLong(SQLColumnasConstants.CVE_MOTIVO_ALTA));
        egreso.setClaveTipoFormato(resultSet.getLong(SQLColumnasConstants.CVE_TIPO_FORMATO));
        egreso.setClaveMedico(resultSet.getString(SQLColumnasConstants.CVE_MEDICO));
        return egreso;
    }

}
