package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;

public class EspecialidadRowMapperHelper extends BaseHelper implements RowMapper<Especialidad> {

    @Override
    public Especialidad mapRow(ResultSet resultSet, int rowId) throws SQLException {

        Especialidad especialidad = new Especialidad();
        especialidad.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
        especialidad.setCveDivision(resultSet.getInt(SQLColumnasConstants.CVE_DIVISION));
        especialidad.setDesEspecialidad(resultSet.getString(SQLColumnasConstants.DES_ESPECIALIDAD));
        especialidad.setNumUbicacion(resultSet.getInt(SQLColumnasConstants.NUM_UBICACION));
        especialidad.setIndQx(resultSet.getBoolean(SQLColumnasConstants.IND_QX));
        especialidad.setCveSexo(resultSet.getInt(SQLColumnasConstants.CVE_SEXO));
        especialidad.setNumEdadMinima(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_MINIMA));
        especialidad.setNumEdadMinSemana(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_MIN_SEMANA));
        especialidad.setNumEdadMaxima(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_MAXIMA));
        especialidad.setNumEdadMaximaSemana(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_MAX_SEMANA));
        especialidad.setIndPediatria(resultSet.getBoolean(SQLColumnasConstants.IND_PEDIATRIA));
        especialidad.setRefAreaResponsable(resultSet.getString(SQLColumnasConstants.REF_AREA_RESPONSABLE));
        especialidad.setRefSimoSias(resultSet.getString(SQLColumnasConstants.REF_SIMO_SIAS));
        especialidad.setRefSimoCentral(resultSet.getString(SQLColumnasConstants.REF_SIMO_CENTRAL));
        especialidad.setCveCategoriaEspecialidad(resultSet.getInt(SQLColumnasConstants.CVE_CATEGORIA_ESPECIALIDAD));
        especialidad.setIndMovIntrahospitalario(resultSet.getBoolean(SQLColumnasConstants.IND_MOV_INTRAHOSPITALARIO));
        especialidad.setNumTipoServicio(resultSet.getInt(SQLColumnasConstants.NUM_TIPO_SERVICIO));
        especialidad.setFecBaja(validarFechaNula(resultSet, SQLColumnasConstants.FEC_BAJA));
        return especialidad;
    }

}
