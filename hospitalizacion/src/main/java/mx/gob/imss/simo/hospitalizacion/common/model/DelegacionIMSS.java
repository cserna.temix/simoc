package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class DelegacionIMSS {

    private String cveDelegacionImss;
    private String desDelegacionImss;
    private String refAbreviaturaDel;
    private String refMarca;
    private Date fecBaja;

}
