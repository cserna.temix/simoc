package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class ConfCaptura {

	 private String cvePresupuestal;
	 private Integer cveTipoCaptura;
	 private Integer indConfActiva;
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfCaptura [cvePresupuestal=");
		builder.append(cvePresupuestal);
		builder.append(", cveTipoCaptura=");
		builder.append(cveTipoCaptura);
		builder.append(", indConfActiva=");
		builder.append(indConfActiva);
		builder.append("]");
		return builder.toString();
	}

	 
}
