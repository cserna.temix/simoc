package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.EspecialidadUnidad;

public class EspecialidadUnidadRowMapperHelper extends BaseHelper implements RowMapper<EspecialidadUnidad> {

    @Override
    public EspecialidadUnidad mapRow(ResultSet resultSet, int rowId) throws SQLException {

        EspecialidadUnidad especialidadUnidad = new EspecialidadUnidad();
        especialidadUnidad.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        especialidadUnidad.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
        especialidadUnidad.setIndConsultaExterna(resultSet.getBoolean(SQLColumnasConstants.IND_CONSULTA_EXTERNA));
        especialidadUnidad.setIndHospital(resultSet.getBoolean(SQLColumnasConstants.IND_HOSPITAL));
        especialidadUnidad.setIndPediatria(resultSet.getBoolean(SQLColumnasConstants.IND_PEDIATRIA));
        especialidadUnidad.setIndCirugia(resultSet.getBoolean(SQLColumnasConstants.IND_CIRUGIA));
        especialidadUnidad.setNumTipoServicio(resultSet.getInt(SQLColumnasConstants.NUM_TIPO_SERVICIO));
        especialidadUnidad.setIndEstatus(resultSet.getBoolean(SQLColumnasConstants.IND_ESTATUS));
        especialidadUnidad.setFecAlta(resultSet.getDate(SQLColumnasConstants.FEC_ALTA));
        especialidadUnidad.setFecBaja(resultSet.getDate(SQLColumnasConstants.FEC_BAJA));
        return especialidadUnidad;
    }

}
