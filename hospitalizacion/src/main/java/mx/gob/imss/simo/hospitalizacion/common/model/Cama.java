package mx.gob.imss.simo.hospitalizacion.common.model;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class Cama {

    private Boolean totales;
    private String clavePresupuestal;
    private Integer division;
    private Integer claveEspacialidad;
    private String descServicioEspecialidad;
    private String ubicacionPiso;
    private String ubicacionAla;
    private Boolean tipoCama;
    private String claveCama;
    private Boolean estatus;
    private Timestamp fechaAlta;
    private Timestamp fechaBaja;
    private Integer totalCamasCensables;
    private Integer totalCamasNoCensables;

}
