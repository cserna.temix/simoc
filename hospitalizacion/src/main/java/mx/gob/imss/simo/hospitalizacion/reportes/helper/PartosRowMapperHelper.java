package mx.gob.imss.simo.hospitalizacion.reportes.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.PartosVo;

public class PartosRowMapperHelper extends BaseHelper implements RowMapper<PartosVo> {

	@Override
	public PartosVo mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		PartosVo partosPojo = new PartosVo();
		
		partosPojo.setPartoAbdominales(resultSet.getInt(SQLColumnasConstants.ABDOMINALES));
		partosPojo.setPartoNormal(resultSet.getInt(SQLColumnasConstants.NORMAL));
		partosPojo.setPartoVaginales(resultSet.getInt(SQLColumnasConstants.VAGINALES));
		
		return partosPojo;
	}

	
	
}
