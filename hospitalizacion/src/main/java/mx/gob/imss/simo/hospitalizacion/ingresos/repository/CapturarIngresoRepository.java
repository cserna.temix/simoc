package mx.gob.imss.simo.hospitalizacion.ingresos.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;

public interface CapturarIngresoRepository extends HospitalizacionCommonRepository {

    long guardarIngreso(Ingreso ingreso);

    List<DatosIngresoRN> buscarRecienNacidosNss(String nss, String clavePresupuestal);

    List<DatosIngresoRN> buscarRecienNacidosCama(String cama, String clavePresupuestal);

    List<DatosIngresoRN> buscarRecienNacidosIngresoVigenteCuneroNoVigenteAlojamiento(String nss,
            String clavePresupuestal);

    List<DatosIngresoRN> buscarRecienNacidosCamaIngresoVigenteCuneroNoVigenteAlojamiento(String cama,
            String clavePresupuestal);

    List<Ingreso> buscarIngresoVigenteCunero(long clavePaciente);

    List<Ingreso> buscarIngresoNoVigenteCunero(long clavePaciente);

    List<Ingreso> buscarIngresoVigenteUcin(long clavePaciente);
    
    List<Ingreso> buscarIngresoVigenteCuneroPatologico(long clavePaciente);

    void actualizarIndicadorRecienNacido(Long claveTococirugia, Integer numeroRecienNacido);

    Paciente buscarPaciente(Long clavePaciente);
    
    public Boolean validarEspecialidadIndPediaria(String claveEspecialidad, String cvePresupuestal);
    
    public int obtenerNumSemanasMaxPediatria(String claveEspecialidad);
    
    public void actualizarCamaRecienNacido(String refCamaRN, String cveEspecialidadCama, Long cveIngreso);
    
    public void actualizarNumRecienNacido(int numRecienNacido,Long cveIngreso);
    
    public void actualizarEdadPaciente(Long cvePaciente, int edadSemanas, Integer edadAnios);
    
    String buscarPeriodoLiberacion(String cve_Parametro);

}
