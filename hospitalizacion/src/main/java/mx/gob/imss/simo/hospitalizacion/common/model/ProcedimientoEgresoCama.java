package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class ProcedimientoEgresoCama {

	 private Long cveEgreso;
	 private long numProcedimiento;
	 private String cveProcedimiento;
	    
}
