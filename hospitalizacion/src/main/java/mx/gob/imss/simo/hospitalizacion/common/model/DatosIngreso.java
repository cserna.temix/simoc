package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;

@Data
public class DatosIngreso {

    private String cama;
    private String camaRecienNacido;
    private String division;
    private String especialidad;
    private String especialidadCama;
    private boolean indCamaPediatrica;
    private String especialidadHospitalizacion;
    private boolean extemporaneo;
    private Date fechaCaptura;
    private String fechaIngreso;
    private String horaIngreso;
    private Long ingresoPadre;
    private String tipoIngreso;
    private String tipoPrograma;
    private TipoUbicacionEnum tipoUbicacion;

}
