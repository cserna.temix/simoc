package mx.gob.imss.simo.hospitalizacion.modificaciones.controller;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoDiagnostioEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;

@ViewScoped
@ManagedBean(name = "buscarEditarHospitalController")
public class BuscarEditarHospitalController extends BuscarEditarHospitalBaseController {

    @Override
    public String obtenerNombrePagina() {

        return super.obtenerNombrePagina();
    }

    @Override
    @PostConstruct
    public void init() {

        super.init();
    }

    public void validarProcedimientoIntervencionQx() {

        try {
            if (null != getProcedimiento1()) {
                getProcedimiento1().setClave(getProcedimiento1().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento1(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento1().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento1().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_UNO) == BaseConstants.PROCEDIMIENTO_DOS) {
                    setTieneFoco(getTextoIntervencionProcedimiento2().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getTextProcedimiento().resetValue();
            getTextoIntervencionProcedimiento2().resetValue();
            requeridos[2] = validarProcedimientosRequeridos();
            getProcedimiento1().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento1().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextProcedimiento().getClientId());
            habilitarGuardar();
        }
    }

    public void validarProcedimientoIntervencionQx2() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento2() || null != getProcedimiento1().getClave()
                    || !getProcedimiento1().getClave().isEmpty()) {
                getProcedimiento2().setClave(getProcedimiento2().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento2(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento2().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento2().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_DOS) == BaseConstants.PROCEDIMIENTO_TRES) {
                    setTieneFoco(getTextoIntervencionProcedimiento3().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            } else {
                setTieneFoco(getTextProcedimiento().getClientId());
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento2().resetValue();
            getProcedimiento2().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento2().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento2().getClientId());
            requeridos[2] = validarProcedimientosRequeridos();
            habilitarGuardar();

        }
    }

    public void validarProcedimientoIntervencionQx3() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento3() || null != getProcedimiento2().getClave()
                    || !getProcedimiento2().getClave().isEmpty()) {
                getProcedimiento3().setClave(getProcedimiento3().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento3(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento3().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento3().getClave()));
                setTieneFoco(getTextoIntervencionProcedimiento4().getClientId());
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_TRES) == BaseConstants.PROCEDIMIENTO_CUATRO) {
                    setTieneFoco(getTextoIntervencionProcedimiento4().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            } else {
                setTieneFoco(getTextoIntervencionProcedimiento2().getClientId());

            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = validarProcedimientosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento3().resetValue();
            getProcedimiento3().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento3().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento3().getClientId());
            habilitarGuardar();
        }
    }

    public void validarProcedimientoIntervencionQx4() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento4()) {
                getProcedimiento4().setClave(getProcedimiento4().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento4(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento4().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento4().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_CUATRO) == BaseConstants.PROCEDIMIENTO_CINCO) {
                    setTieneFoco(getTextoIntervencionProcedimiento5().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = validarProcedimientosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento4().resetValue();
            getProcedimiento4().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento4().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento4().getClientId());
            habilitarGuardar();
        }
    }

    public void validarProcedimientoIntervencionQx5() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento5()) {
                getProcedimiento5().setClave(getProcedimiento5().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento5(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento5().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento5().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_CINCO) == BaseConstants.PROCEDIMIENTO_SEIS) {
                    setTieneFoco(getTextoIntervencionProcedimiento6().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = validarProcedimientosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento5().resetValue();
            getProcedimiento5().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento5().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento5().getClientId());
            habilitarGuardar();
        }
    }

    public void validarProcedimientoIntervencionQx6() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento6()) {
                getProcedimiento6().setClave(getProcedimiento6().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento6(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento6().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento6().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_SEIS) == BaseConstants.PROCEDIMIENTO_SIETE) {
                    setTieneFoco(getTextoIntervencionProcedimiento7().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = validarProcedimientosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento6().resetValue();
            getProcedimiento6().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento6().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento6().getClientId());
            habilitarGuardar();
        }
    }

    public void validarProcedimientoIntervencionQx7() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento7()) {
                getProcedimiento7().setClave(getProcedimiento7().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento7(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento7().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento7().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_SIETE) == BaseConstants.PROCEDIMIENTO_OCHO) {
                    setTieneFoco(getTextoIntervencionProcedimiento8().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = validarProcedimientosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento7().resetValue();
            getProcedimiento7().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento7().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento7().getClientId());
            habilitarGuardar();
        }
    }

    public void validarProcedimientoIntervencionQx8() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento8()) {
                getProcedimiento8().setClave(getProcedimiento8().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento8(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento8().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento8().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_OCHO) == BaseConstants.PROCEDIMIENTO_NUEVE) {
                    setTieneFoco(getTextoIntervencionProcedimiento9().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = validarProcedimientosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento8().resetValue();
            getProcedimiento8().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento8().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento8().getClientId());
            habilitarGuardar();
        }
    }

    public void validarProcedimientoIntervencionQx9() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento9()) {
                getProcedimiento9().setClave(getProcedimiento9().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento9(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento9().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento9().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_NUEVE) == BaseConstants.PROCEDIMIENTO_DIEZ) {
                    setTieneFoco(getTextoIntervencionProcedimiento10().getClientId());
                } else {
                    // si existe tabla rn si no si existen egresos y si no boton
                    // cancelar
                    setTieneFoco(getSelectSexoRn1().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = validarProcedimientosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento9().resetValue();
            getProcedimiento9().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento9().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento9().getClientId());
            habilitarGuardar();
        }
    }

    public void validarProcedimientoIntervencionQx10() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            if (null != getProcedimiento10()) {
                getProcedimiento10().setClave(getProcedimiento10().getClave().toUpperCase());
                getBuscarEditarHospitalServices().validarProcedimientoIntervencionQuirurgica(getProcedimiento10(),
                        armarListaProcedimiento(), getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
                getProcedimiento10().setDescripcion(getBuscarEditarHospitalServices()
                        .obtenerDescripcionProcedimiento(getProcedimiento10().getClave()));
                requeridos[2] = validarProcedimientosRequeridos();
                if (siguienteFoco(BaseConstants.PROCEDIMIENTO_DIEZ) == BaseConstants.PROCEDIMIENTO_DIEZ) {
                    setTieneFoco(getSelectSexoRn1().getClientId());
                } /*
                   * else { // si existe tabla rn si no si existen egresos y
                   * si no boton cancelar
                   * setTieneFoco(getSelectSexoRn1().getClientId()); }
                   */
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = validarProcedimientosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getTextoIntervencionProcedimiento10().resetValue();
            getProcedimiento10().setClave(BaseConstants.CADENA_VACIA);
            getProcedimiento10().setDescripcion(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextoIntervencionProcedimiento10().getClientId());
            habilitarGuardar();
        }
    }

    private int siguienteFocoRN(final int recienNacido) {

        if (null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                && !getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().isEmpty()) {
            switch (recienNacido) {
                case BaseConstants.RN_UNO:
                    return validarProcedimientoUnoRN();
                case BaseConstants.RN_DOS:
                    return validarProcedimientoDosRN();
                case BaseConstants.RN_TRES:
                    return validarProcedimientoTresRN();
                case BaseConstants.RN_CUATRO:
                    return validarProcedimientoCuatroRN();
                case BaseConstants.RN_CINCO:
                    return validarProcedimientoCincoRN();
                case BaseConstants.RN_SEIS:
                	 return validarProcedimientoSeisRN();
                case BaseConstants.RN_SIETE:
                	 return validarProcedimientoSieteRN();
                case BaseConstants.RN_OCHO:
                	 return validarProcedimientoOchoRN();
                case BaseConstants.RN_NUEVE:
                	 return validarProcedimientoNueveRN();
                default:
                    break;
            }
        }
        	return BaseConstants.RN_CERO;
    }

    public int validarProcedimientoUnoRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_UNO
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_UNO)) {
            return BaseConstants.RN_DOS;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public int validarProcedimientoDosRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_DOS
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_DOS)) {
            return BaseConstants.RN_TRES;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public int validarProcedimientoTresRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_TRES
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_TRES)) {
            return BaseConstants.RN_CUATRO;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public int validarProcedimientoCuatroRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_CUATRO
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_CUATRO)) {
            return BaseConstants.RN_CINCO;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public int validarProcedimientoCincoRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_CINCO
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_CINCO)) {
            return BaseConstants.RN_SEIS;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public int validarProcedimientoSeisRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_SEIS
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_SEIS)) {
            return BaseConstants.RN_SIETE;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public int validarProcedimientoSieteRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_SIETE
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_SIETE)) {
            return BaseConstants.RN_OCHO;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public int validarProcedimientoOchoRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_OCHO
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_OCHO)) {
            return BaseConstants.RN_NUEVE;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public int validarProcedimientoNueveRN() {

        if (getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido().size() > BaseConstants.RN_NUEVE
                && null != getDatosHospitalizacion().getDatosTococirugia().getListDatosRecienNacido()
                        .get(BaseConstants.RN_NUEVE)) {
            return BaseConstants.RN_NUEVE;
        } else {
            return BaseConstants.RN_CERO;
        }
    }

    public void validarSexoRn1() {

        try {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            getBuscarEditarHospitalServices().validarSexoRn(getRn1());
            setTieneFoco(getTextPesoRn1().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getSelectSexoRn1().resetValue();
            getRn1().setSexo(null);
            setTieneFoco(getSelectSexoRn1().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPesoRn1() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn1());
            setTieneFoco(getTextTallaRn1().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getTextPesoRn1().resetValue();
            getRn1().setPeso(null);
            requeridos[3] = validarRecienNacidosRequeridos();
            setTieneFoco(getTextPesoRn1().getClientId());
            habilitarGuardar();
        }
    }

    public void validarTallaRn1() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn1());
            if (siguienteFocoRN(BaseConstants.RN_UNO) == BaseConstants.RN_DOS) {
                setTieneFoco(getSelectSexoRn2().getClientId());
            } else {
            	setTieneFoco(getSelectTipoPartoToco().getClientId());
            }
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn1().resetValue();
            getRn1().setTalla(null);
            setTieneFoco(getTextTallaRn1().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSexoRn2() {

        try {

            getBuscarEditarHospitalServices().validarSexoRn(getRn2());
            setTieneFoco(getTextPesoRn2().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getSelectSexoRn2().resetValue();
            getRn2().setSexo(null);
            setTieneFoco(getSelectSexoRn2().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPesoRn2() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn2());
            setTieneFoco(getTextTallaRn2().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getTextPesoRn2().resetValue();
            getRn2().setPeso(null);
            requeridos[3] = validarRecienNacidosRequeridos();
            setTieneFoco(getTextPesoRn2().getClientId());
            habilitarGuardar();
        }
    }

    public void validarTallaRn2() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn2());
            requeridos[3] = validarRecienNacidosRequeridos();
            if (siguienteFocoRN(BaseConstants.RN_DOS) == BaseConstants.RN_TRES) {
                setTieneFoco(getSelectSexoRn3().getClientId());
            } else {
            	setTieneFoco(getSelectTipoPartoToco().getClientId());
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn2().resetValue();
            getRn2().setTalla(null);
            setTieneFoco(getTextTallaRn2().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSexoRn3() {

        try {

            getBuscarEditarHospitalServices().validarSexoRn(getRn3());
            setTieneFoco(getTextPesoRn3().getClientId());

            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getSelectSexoRn3().resetValue();
            getRn3().setSexo(null);
            setTieneFoco(getSelectSexoRn3().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPesoRn3() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn3());
            setTieneFoco(getTextTallaRn3().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getTextPesoRn3().resetValue();
            getRn3().setPeso(null);
            setTieneFoco(getTextPesoRn3().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();

        }
    }

    public void validarTallaRn3() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn3());
            requeridos[3] = validarRecienNacidosRequeridos();
            if (siguienteFocoRN(BaseConstants.RN_TRES) == BaseConstants.RN_CUATRO) {
                setTieneFoco(getSelectSexoRn4().getClientId());
            } else {
            	setTieneFoco(getSelectTipoPartoToco().getClientId());
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn3().resetValue();
            getRn3().setTalla(null);
            setTieneFoco(getTextTallaRn3().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSexoRn4() {

        try {

            getBuscarEditarHospitalServices().validarSexoRn(getRn4());
            setTieneFoco(getTextPesoRn4().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getSelectSexoRn4().resetValue();
            getRn4().setSexo(null);
            setTieneFoco(getSelectSexoRn4().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        }
    }

    public void validarPesoRn4() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn4());
            setTieneFoco(getTextTallaRn4().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getTextPesoRn4().resetValue();
            getRn4().setPeso(null);
            setTieneFoco(getTextPesoRn4().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        }
    }

    public void validarTallaRn4() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn4());
            requeridos[3] = validarRecienNacidosRequeridos();
            if (siguienteFocoRN(BaseConstants.RN_CUATRO) == BaseConstants.RN_CINCO) {
                setTieneFoco(getSelectSexoRn5().getClientId());
            } else {
            	setTieneFoco(getSelectTipoPartoToco().getClientId());
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn4().resetValue();
            getRn4().setTalla(null);
            setTieneFoco(getTextTallaRn4().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSexoRn5() {

        try {

            getBuscarEditarHospitalServices().validarSexoRn(getRn5());
            setTieneFoco(getTextPesoRn5().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getSelectSexoRn5().resetValue();
            getRn5().setSexo(null);
            setTieneFoco(getSelectSexoRn5().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPesoRn5() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn5());
            setTieneFoco(getTextTallaRn5().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextPesoRn5().resetValue();
            getRn5().setPeso(null);
            setTieneFoco(getTextPesoRn5().getClientId());
            habilitarGuardar();
        }
    }

    public void validarTallaRn5() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn5());
            requeridos[3] = validarRecienNacidosRequeridos();
            if (siguienteFocoRN(BaseConstants.RN_CINCO) == BaseConstants.RN_SEIS) {
                setTieneFoco(getSelectSexoRn6().getClientId());
            } else {
            	setTieneFoco(getSelectTipoPartoToco().getClientId());
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn5().resetValue();
            getRn5().setTalla(null);
            setTieneFoco(getTextTallaRn5().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSexoRn6() {

        try {

            getBuscarEditarHospitalServices().validarSexoRn(getRn6());
            setTieneFoco(getTextPesoRn6().getClientId());

            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getSelectSexoRn6().resetValue();
            getRn6().setSexo(null);
            setTieneFoco(getSelectSexoRn6().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPesoRn6() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn6());
            setTieneFoco(getTextTallaRn6().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextPesoRn6().resetValue();
            getRn6().setPeso(null);
            setTieneFoco(getTextPesoRn6().getClientId());
            habilitarGuardar();
        }
    }

    public void validarTallaRn6() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn6());

            if (siguienteFocoRN(BaseConstants.RN_SEIS) == BaseConstants.RN_SIETE) {
                setTieneFoco(getSelectSexoRn7().getClientId());
            } else {
            	setTieneFoco(getSelectTipoPartoToco().getClientId());
            }

            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn6().resetValue();
            getRn6().setTalla(null);
            setTieneFoco(getTextTallaRn6().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSexoRn7() {

        try {

            getBuscarEditarHospitalServices().validarSexoRn(getRn7());
            setTieneFoco(getTextPesoRn7().getClientId());

            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getSelectSexoRn7().resetValue();
            getRn7().setSexo(null);
            setTieneFoco(getSelectSexoRn7().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPesoRn7() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn7());
            setTieneFoco(getTextTallaRn7().getClientId());

            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextPesoRn7().resetValue();
            getRn7().setPeso(null);
            setTieneFoco(getTextPesoRn7().getClientId());
            habilitarGuardar();
        }
    }

    public void validarTallaRn7() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn7());
            requeridos[3] = validarRecienNacidosRequeridos();
            if (siguienteFocoRN(BaseConstants.RN_SIETE) == BaseConstants.RN_OCHO) {
                setTieneFoco(getSelectSexoRn8().getClientId());
            } else {
            	setTieneFoco(getSelectTipoPartoToco().getClientId());
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn7().resetValue();
            getRn7().setTalla(null);
            setTieneFoco(getTextTallaRn7().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSexoRn8() {

        try {

            getBuscarEditarHospitalServices().validarSexoRn(getRn8());
            setTieneFoco(getTextPesoRn8().getClientId());

            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getSelectSexoRn8().resetValue();
            getRn8().setSexo(null);
            setTieneFoco(getSelectSexoRn8().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPesoRn8() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn8());
            setTieneFoco(getTextTallaRn8().getClientId());

            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextPesoRn8().resetValue();
            getRn8().setPeso(null);
            setTieneFoco(getTextPesoRn8().getClientId());
            habilitarGuardar();
        }
    }

    public void validarTallaRn8() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn8());
            requeridos[3] = validarRecienNacidosRequeridos();
            if (siguienteFocoRN(BaseConstants.RN_OCHO) == BaseConstants.RN_NUEVE) {
                setTieneFoco(getSelectSexoRn9().getClientId());
            } else {
            	setTieneFoco(getSelectTipoPartoToco().getClientId());
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn8().resetValue();
            getRn8().setTalla(null);
            setTieneFoco(getTextTallaRn8().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSexoRn9() {

        try {

            getBuscarEditarHospitalServices().validarSexoRn(getRn9());
            setTieneFoco(getTextPesoRn9().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getSelectSexoRn9().resetValue();
            getRn9().setSexo(null);
            setTieneFoco(getSelectSexoRn9().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPesoRn9() {

        try {

            getBuscarEditarHospitalServices().validarPesoRecienNacido(getRn9());
            requeridos[3] = validarRecienNacidosRequeridos();
            setTieneFoco(getTextTallaRn9().getClientId());
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextPesoRn9().resetValue();
            getRn9().setPeso(null);
            setTieneFoco(getTextPesoRn9().getClientId());
            habilitarGuardar();
        }
    }

    public void validarTallaRn9() {

        try {

            getBuscarEditarHospitalServices().validarTallaRecienNacido(getRn9());
            setTieneFoco(getSelectTipoPartoToco().getClientId());
            requeridos[3] = validarRecienNacidosRequeridos();
            habilitarGuardar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = validarRecienNacidosRequeridos();
            getTextTallaRn9().resetValue();
            getRn9().setTalla(null);
            setTieneFoco(getTextTallaRn9().getClientId());
            habilitarGuardar();
        }
    }

    public void validarMotivoAltaEgreso() {

        try {

            getBuscarEditarHospitalServices().validarMotivoAlta(getDatosHospitalizacion());
            requeridos[5] = Boolean.TRUE;
            if (validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveInicialIngresoEgreso())) {
                setTieneFoco(getTextInicialIngresoDiag().getClientId());
            } else if (validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera())) {
                setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
            } else if (validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraSegunda())) {
                setTieneFoco(getTextSeguCompliIntraDiag().getClientId());
            } else if (validarCampo(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta())) {
                setTieneFoco(getTextCausaDirectaDefuncionDiag().getClientId());
            } else {
                setTieneFoco(getTextCausaBasicaDefuncionDiag().getClientId());
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[5] = Boolean.FALSE;
            getSelectMotivoAltaEgreso().resetValue();
            setTieneFoco(getSelectMotivoAltaEgreso().getClientId());
            habilitarGuardar();
        }
    }

    public void validarInicialEgreso() {

        try {

            getDatosHospitalizacion().getDatosEgreso().setCveInicialIngresoEgreso(
                    getDatosHospitalizacion().getDatosEgreso().getCveInicialIngresoEgreso().toUpperCase());
            getBuscarEditarHospitalServices().validarDiagnosticoInicialEgreso(getDatosHospitalizacion(),
                    getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
            requeridos[6] = Boolean.TRUE;
            if (getBuscarEditarHospitalServices().validarDiagnosticoEpidemiologica(
                    getDatosHospitalizacion().getDatosEgreso().getCveInicialIngresoEgreso())) {
                setTipoDiagnostico(TipoDiagnostioEnum.DIAGNOSTICO_INICIAL_INGRESO.getClave());
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
                setTieneFoco(getConfirmSi().getClientId());
            } else {
                getDatosHospitalizacion().getDatosEgreso().setInicialIngresoEgreso(getBuscarEditarHospitalServices()
                        .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveInicialIngresoEgreso()));
                if(getTextPrincipalEgresoDiag().getClientId() != null || getTextPrincipalEgresoDiag().getClientId().isEmpty()){
                	setTieneFoco(getTextPrincipalEgresoDiag().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[6] = Boolean.FALSE;
            getTextInicialIngresoDiag().resetValue();
            getDatosHospitalizacion().getDatosEgreso().setInicialIngresoEgreso(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setCveInicialIngresoEgreso(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextInicialIngresoDiag().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPrincipalEgresoDiag() {

        try {

            getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(
                    getDatosHospitalizacion().getDatosEgreso().getCvePrincipalEgreso().toUpperCase());
            getBuscarEditarHospitalServices().validarDiagnosticoPrincipalEgreso(getDatosHospitalizacion(),
                    getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());
            requeridos[7] = Boolean.TRUE;
            if (getBuscarEditarHospitalServices().validarDiagnosticoEpidemiologica(
                    getDatosHospitalizacion().getDatosEgreso().getCvePrincipalEgreso())) {
                setTipoDiagnostico(TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave());
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
                setTieneFoco(getConfirmSi().getClientId());
            } else {
                getDatosHospitalizacion().getDatosEgreso()
                        .setPrincipalEgreso(getBuscarEditarHospitalServices().obtenerDescripcionDX(
                                getDatosHospitalizacion().getDatosEgreso().getCvePrincipalEgreso().toUpperCase()));
                setTieneFoco(getTextPrimerSecundarioEgresoDiag().getClientId());

            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[7] = Boolean.FALSE;
            getTextPrincipalEgresoDiag().resetValue();
            getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextPrincipalEgresoDiag().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPrimerSecundarioEgreso() {

        try {
            getDatosHospitalizacion().getDatosEgreso().setCvePrimerDiagnosticoSecundario(
                    getDatosHospitalizacion().getDatosEgreso().getCvePrimerDiagnosticoSecundario().toUpperCase());
            getBuscarEditarHospitalServices().validarPrimerSecundarioEgreso(getDatosHospitalizacion(),
                    getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());

            getDatosHospitalizacion().getDatosEgreso()
                    .setPrimerDiagnosticoSecundario(getBuscarEditarHospitalServices().obtenerDescripcionDX(
                            getDatosHospitalizacion().getDatosEgreso().getCvePrimerDiagnosticoSecundario()));

            if (!getTextSegundoSecundarioEgresoDiag().isDisabled()) {
                setTieneFoco(getTextSegundoSecundarioEgresoDiag().getClientId());
            } else {
                setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
            }
            requeridos[8] = Boolean.TRUE;
            if (getBuscarEditarHospitalServices().validarDiagnosticoEpidemiologica(
                    getDatosHospitalizacion().getDatosEgreso().getCvePrimerDiagnosticoSecundario())) {
                setTipoDiagnostico(TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave());
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
                setTieneFoco(getConfirmSi().getClientId());
            } else {
                getDatosHospitalizacion().getDatosEgreso()
                        .setPrimerDiagnosticoSecundario(getBuscarEditarHospitalServices().obtenerDescripcionDX(
                                getDatosHospitalizacion().getDatosEgreso().getCvePrimerDiagnosticoSecundario()));
                if (!getTextSegundoSecundarioEgresoDiag().isDisabled()) {
                    setTieneFoco(getTextSegundoSecundarioEgresoDiag().getClientId());
                } else {
                    setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[8] = Boolean.FALSE;
            getTextPrimerSecundarioEgresoDiag().resetValue();
            getTextPrimerSecundarioEgresoDiagDesc().resetValue();
            getDatosHospitalizacion().getDatosEgreso().setCvePrimerDiagnosticoSecundario(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setPrimerDiagnosticoSecundario(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextPrimerSecundarioEgresoDiag().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSegundoSecundarioEgresoDiag() {

        try {

            getDatosHospitalizacion().getDatosEgreso().setClaveSegundoDiagnosticoSecundario(
                    getDatosHospitalizacion().getDatosEgreso().getClaveSegundoDiagnosticoSecundario().toUpperCase());
            getBuscarEditarHospitalServices().validarSegundoSecundarioEgreso(getDatosHospitalizacion(),
                    getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico());

            getDatosHospitalizacion().getDatosEgreso()
                    .setSegundoDiagnosticoSecundario(getBuscarEditarHospitalServices().obtenerDescripcionDX(
                            getDatosHospitalizacion().getDatosEgreso().getClaveSegundoDiagnosticoSecundario()));
            setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
            requeridos[9] = Boolean.TRUE;
            if (getBuscarEditarHospitalServices().validarDiagnosticoEpidemiologica(
                    getDatosHospitalizacion().getDatosEgreso().getClaveSegundoDiagnosticoSecundario())) {
                setTipoDiagnostico(TipoDiagnostioEnum.SEGUNDO_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave());
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
                setTieneFoco(getConfirmSi().getClientId());
            } else {
                getDatosHospitalizacion().getDatosEgreso()
                        .setSegundoDiagnosticoSecundario(getBuscarEditarHospitalServices().obtenerDescripcionDX(
                                getDatosHospitalizacion().getDatosEgreso().getClaveSegundoDiagnosticoSecundario()));
                setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
            }
            habilitarGuardar();

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[9] = Boolean.FALSE;
            getTextSegundoSecundarioEgresoDiag().resetValue();
            getTextSegundoSecundarioEgresoDiagDesc().resetValue();
            getDatosHospitalizacion().getDatosEgreso().setClaveSegundoDiagnosticoSecundario(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setSegundoDiagnosticoSecundario(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextSegundoSecundarioEgresoDiag().getClientId());
            habilitarGuardar();
        }
    }

    public void validarPrimerComplicacionEgreso() {

        try {

            getDatosHospitalizacion().getDatosEgreso().setCveComplicacionIntraPrimera(
                    getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera().toUpperCase());
            getBuscarEditarHospitalServices().validarPrimeraComplicacionIntraEgreso(getDatosHospitalizacion());

            getDatosHospitalizacion().getDatosEgreso().setComplicacionIntraPrimera(getBuscarEditarHospitalServices()
                    .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera()));
            setTieneFoco(getTextSeguCompliIntraDiag().getClientId());
            requeridos[10] = Boolean.TRUE;
            if (getBuscarEditarHospitalServices().validarDiagnosticoEpidemiologica(
                    getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera())) {
                setTipoDiagnostico(TipoDiagnostioEnum.PRIMERA_COMPLICACION_INTRAHOSPITALARIA.getClave());
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
                setTieneFoco(getConfirmSi().getClientId());
            } else {
                getDatosHospitalizacion().getDatosEgreso()
                        .setComplicacionIntraPrimera(getBuscarEditarHospitalServices().obtenerDescripcionDX(
                                getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraPrimera()));
                if (!getTextSeguCompliIntraDiag().isDisabled()) {
                    setTieneFoco(getTextSeguCompliIntraDiag().getClientId());
                } else {
                    setTieneFoco(getButtonGuardar().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[10] = Boolean.FALSE;
            getTextPrimerCompliIntraDiag().resetValue();
            getTextPrimerCompliIntraDiagDesc().resetValue();
            getDatosHospitalizacion().getDatosEgreso().setCveComplicacionIntraPrimera(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setComplicacionIntraPrimera(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextPrimerCompliIntraDiag().getClientId());
            habilitarGuardar();
        }
    }

    public void validarSegundoComplicacionEgreso() {

        try {

            getDatosHospitalizacion().getDatosEgreso().setCveComplicacionIntraSegunda(
                    getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraSegunda().toUpperCase());
            getBuscarEditarHospitalServices().validarSegundaComplicacionIntraEgreso(getDatosHospitalizacion());
            requeridos[11] = Boolean.TRUE;
            if (getBuscarEditarHospitalServices().validarDiagnosticoEpidemiologica(
                    getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraSegunda())) {
                setTipoDiagnostico(TipoDiagnostioEnum.SEGUNDA_COMPLICACION_INTRAHOSPITALARIA.getClave());
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
                setTieneFoco(getConfirmSi().getClientId());
            } else {
                getDatosHospitalizacion().getDatosEgreso()
                        .setComplicacionIntraSegunda(getBuscarEditarHospitalServices().obtenerDescripcionDX(
                                getDatosHospitalizacion().getDatosEgreso().getCveComplicacionIntraSegunda()));
                if (!getTextCausaDirectaDefuncionDiag().isDisabled()) {
                    setTieneFoco(getTextCausaDirectaDefuncionDiag().getClientId());
                } else {
                    setTieneFoco(getButtonGuardar().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[11] = Boolean.FALSE;
            getTextSeguCompliIntraDiag().resetValue();
            getTextSeguCompliIntraDiagDesc().resetValue();
            getDatosHospitalizacion().getDatosEgreso().setCveComplicacionIntraSegunda(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setComplicacionIntraSegunda(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextSeguCompliIntraDiag().getClientId());
            habilitarGuardar();
        }
    }

    public void validarCausaDirectaDefuncion() {

        try {
            getDatosHospitalizacion().getDatosEgreso().setCveCausaDefDirecta(
                    getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta().toUpperCase());
            getBuscarEditarHospitalServices().validarCausaDirectaDefuncion(getDatosHospitalizacion());
            requeridos[12] = Boolean.TRUE;
            if (getBuscarEditarHospitalServices().validarDiagnosticoEpidemiologica(
                    getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta())) {
                setTipoDiagnostico(TipoDiagnostioEnum.CAUSA_DIRECTA_DEFUNCION.getClave());
                getDatosHospitalizacion().getDatosEgreso().setCausaDefDirecta(getBuscarEditarHospitalServices()
                        .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta()));
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
                setTieneFoco(getConfirmSi().getClientId());
            } else {
                if (getBuscarEditarHospitalServices().validarDiagnosticoAccidente(
                        getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta())) {
                    setTipoDiagnostico(TipoDiagnostioEnum.CAUSA_DIRECTA_DEFUNCION.getClave());
                    RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_ACCIDENTE);
                    setTieneFoco(getConfirmSiAccidente().getClientId());
                } else {
                    getDatosHospitalizacion().getDatosEgreso().setCausaDefDirecta(getBuscarEditarHospitalServices()
                            .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta()));
                    if(getDatosHospitalizacion().getDatosEgreso().getTipoFormato() != TipoFormatoEnum.EGRESO_URGENCIAS.getClave()){
                    	getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(getDatosHospitalizacion()
                    			.getDatosEgreso().getCveCausaDefDirecta());
                    	getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(getBuscarEditarHospitalServices()
                    			.obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefDirecta()));
                    }
                    setTieneFoco(getTextCausaBasicaDefuncionDiag().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[12] = Boolean.FALSE;
            getTextCausaDirectaDefuncionDiag().resetValue();
            getTextCausaDirectaDefuncionDiagDesc().resetValue();
            getDatosHospitalizacion().getDatosEgreso().setCveCausaDefDirecta(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setCausaDefDirecta(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextCausaDirectaDefuncionDiag().getClientId());
            habilitarGuardar();

        }
    }

    public void validarCausaBasicaDefuncion() {

        try {
            getDatosHospitalizacion().getDatosEgreso().setCveCausaDefBasica(
                    getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica().toUpperCase());
            getBuscarEditarHospitalServices().validarCausaBasicaDefuncion(getDatosHospitalizacion());
            requeridos[13] = Boolean.TRUE;
            if (getBuscarEditarHospitalServices().validarDiagnosticoEpidemiologica(
                    getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica())) {
                setTipoDiagnostico(TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave());
                RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
                setTieneFoco(getConfirmSi().getClientId());
            } else {
                if (getBuscarEditarHospitalServices().validarDiagnosticoAccidente(
                        getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica())) {
                    setTipoDiagnostico(TipoDiagnostioEnum.CAUSA_BASICA_DEFUNCION.getClave());
                    RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_ACCIDENTE);
                    setTieneFoco(getConfirmSiAccidente().getClientId());
                } else {
                    getDatosHospitalizacion().getDatosEgreso().setCausaDefBasica(getBuscarEditarHospitalServices()
                            .obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica()));
                    if(getDatosHospitalizacion().getDatosEgreso().getTipoFormato() != TipoFormatoEnum.EGRESO_URGENCIAS.getClave() 
                    		&&(getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta() == null || 
                    		getDatosHospitalizacion().getDatosEgreso().getCausaDefDirecta().isEmpty())){
                    	getDatosHospitalizacion().getDatosEgreso().setCvePrincipalEgreso(getDatosHospitalizacion()
                    			.getDatosEgreso().getCveCausaDefBasica());
                    	getDatosHospitalizacion().getDatosEgreso().setPrincipalEgreso(getBuscarEditarHospitalServices()
                    			.obtenerDescripcionDX(getDatosHospitalizacion().getDatosEgreso().getCveCausaDefBasica()));
             
                    }
                    setTieneFoco(getButtonGuardar().getClientId());
                }
            }
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[13] = Boolean.FALSE;
            getTextCausaBasicaDefuncionDiag().resetValue();
            getTextCausaBasicaDefuncionDiagDesc().resetValue();
            getDatosHospitalizacion().getDatosEgreso().setCveCausaDefBasica(BaseConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosEgreso().setCausaDefBasica(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextCausaBasicaDefuncionDiag().getClientId());
            habilitarGuardar();
        }
    }

    public void salir() throws IOException {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }
    
    public void desahabilitaGuardar(){
    	getButtonGuardar().setDisabled(Boolean.TRUE);
    }
    

}
