package mx.gob.imss.simo.hospitalizacion.intervencionesqx.services;

import java.util.Date;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.event.SelectEvent;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIntervenciones;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.model.DatosUsuario;

public interface CapturarIntervencionesQuirurgicasServices extends HospitalizacionCommonServices {

    void validarFecha(String fecha, String clavePresupuestal) throws HospitalizacionException;

    void validaFechaIngresoIQX(DatosPaciente datosPaciente, String fechaIQX) throws HospitalizacionException;

    void validarNumeroQuirofano(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException;

    void validarNSS(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    DatosCatalogo elementoSelecionado(SelectEvent evento, List<String> listaEspecialidad,
            AutoComplete autoCompleteEspecialidad, List<DatosCatalogo> catalogoEspecialidad);

    void validarTipoIntervencion(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    void validarHoraEntradaSala(DatosHospitalizacion datosHospitalizacion, String fechaIQX)
            throws HospitalizacionException;

    void validarHoraInicio(DatosHospitalizacion datosHospitalizacion, String fechaIQX) throws HospitalizacionException;

    void validarHoraTermino(DatosHospitalizacion datosHospitalizacion, String fechaIQX) throws HospitalizacionException;

    void validarHoraSalaSalida(DatosHospitalizacion datosHospitalizacion, String fechaIQX)
            throws HospitalizacionException;

    void validarTipoAnestesia(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    List<Procedimientos> iniciarTotalProcedimientos(int numeroProcedimientos) throws HospitalizacionException;

    String validarEspecialidad(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    DatosIntervenciones obtenerDatosAnteriores(DatosPaciente datosPaciente);

    void validarProcedimientoQX(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    void validarCirujano(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    void validarPlanificacionFamiliar(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    boolean validarMetodoPlanificacion(String datosPlanificacionFamiliar, String agregadoMedico, Date fecCaptura)
            throws HospitalizacionException;

    void validarCantidadPlanificacion(String datosPlanificacionFamiliar) throws HospitalizacionException;

    void validarFechaProgramacion(DatosHospitalizacion datosHospitalizacion, String fecha)
            throws HospitalizacionException;

    boolean requiereAbrirModal(String tipoIntervencion);

    void validarModalProcedimiento() throws HospitalizacionException;

    void validarCirujanoModal(String matricula) throws HospitalizacionException;

    void validarProcedimientoModal(Procedimientos procedimiento, List<Procedimientos> listaProcedimientos,
            String agregadoMedico) throws HospitalizacionException;

    String obtenerDescripcion(String clave);

    DatosMedico obtenerMedico(String matricula, String cvePresupuestal);

    void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario usuario);

    Date convertirFecha(String fecha);

    Date convertirHora(String fecha, String hora);

    void validarTotalProcedimientos(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException;

    void validarFechaIngreso(Date fechaIngreso, String fechaIntervencion) throws HospitalizacionException;

    boolean sePuedeActivarGuardar(Boolean[] requeridos);
    
    boolean validarHoraInicioMenorHoraEntrada(DatosHospitalizacion datosHospitalizacion, String fechaIQX);
    boolean validarHoraTerminoMenorHoraInicio(DatosHospitalizacion datosHospitalizacion, String fechaIQX);
    boolean validarHoraSalaSalidaMenorHoraTermino(DatosHospitalizacion datosHospitalizacion, String fechaIQX);
    Ingreso buscarIngresoPorDatosPaciente(DatosHospitalizacion datosHospitalizacion);
    IntervencionesQuirurgicas buscarIntervencionQXPorCveIngreso(Long cveIngreso);
    void actualizarMetodoAnticonceptivoIQX(int cveMetodo, int cantidadMetodo, Long cveIngreso);
            
}
