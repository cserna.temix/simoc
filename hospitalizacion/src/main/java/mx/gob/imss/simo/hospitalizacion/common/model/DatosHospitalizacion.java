package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class DatosHospitalizacion {

    private Integer consecutivo;
    private DatosEgreso datosEgreso;
    private DatosIngreso datosIngreso;
    private DatosIngresoRN datosIngresoRN;
    private DatosIntervenciones datosIntervenciones;
    private DatosMedico datosMedico;
    private MovimientoIntraHospitalario datosMovimientos;
    private DatosPaciente datosPaciente;
    private DatosTococirugia datosTococirugia;
    private DatosRecienNacido datosRecienNacidoModal;
    private DatosInformacionComplementaria datosInfoComplementaria;
    private DatosSubrogados datosSubrogados;

    public DatosHospitalizacion() {
        datosEgreso = new DatosEgreso();
        datosIngreso = new DatosIngreso();
        datosIngresoRN = new DatosIngresoRN();
        datosIntervenciones = new DatosIntervenciones();
        datosMedico = new DatosMedico();
        datosMovimientos = new MovimientoIntraHospitalario();
        datosPaciente = new DatosPaciente();
        datosTococirugia = new DatosTococirugia();
        datosRecienNacidoModal = new DatosRecienNacido();
        datosInfoComplementaria = new DatosInformacionComplementaria();
        datosSubrogados = new DatosSubrogados();
    }

}
