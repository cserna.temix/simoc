/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.services.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.reportes.repository.ReportesRepository;
import mx.gob.imss.simo.hospitalizacion.reportes.services.ProcesarParteIIService;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.EspecialidadesUnidadVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.NacLubchencoVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.NacidosVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.PartosVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitMaternoInfantil;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitMovEnfermo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitParteII;

/**
 * @author francisco.rodriguez
 */
@Service("procesarParteIIService")
public class ProcesarParteIIServiceImpl extends HospitalizacionCommonServicesImpl implements ProcesarParteIIService {

    @Autowired
    private ReportesRepository reportesRepository;

    String cvePeriodoImss = "";
    public static final String URGENCIAS = "5000";
    public static final String CIRUGIA_AMBULATORIA = "0100";

    @Override
    public Integer procesarPrincial(String cvePresupuestal, Integer tipoReporte) throws HospitalizacionException {
    	
        Integer resultado = 0;
        Integer bitacora = 0;
        cvePeriodoImss = reportesRepository.periodoMasActualAbierto(cvePresupuestal);
        // VALOR TIPO REPORTE 1 si es Periodo, 2 si es Cierre
        if(cvePeriodoImss != null){
        if (tipoReporte == 1) {
            // Se valida si existe la bitacora
            try {
                
            	bitacora = reportesRepository.buscarBitacora(cvePresupuestal, cvePeriodoImss, 1);
            } catch (HospitalizacionException e) {
                logger.error("Error: " + e.getMessage());
            }

            // 1 Parte II - Diario
            // 2 En ejecución
            
            
            if (bitacora > 0) {
            	reportesRepository.updateBitacora(cvePresupuestal, cvePeriodoImss, 1, 2, Boolean.TRUE);
            } else {
                reportesRepository.insertarInicioBitacora(cvePresupuestal, cvePeriodoImss, 1, 2);
            }
            resultado = procesarPeriodo(cvePresupuestal);

        } else {
        	
        	try {
                cvePeriodoImss = reportesRepository.obtenerPeriodoParaCierre(cvePresupuestal);
            } catch (HospitalizacionException e) {
                logger.error("Error: " + e.getMessage());
            }
            // 2 Parte II - Periodo
            // 2 En ejecución
            reportesRepository.updateBitacora(cvePresupuestal, cvePeriodoImss, 2, 2, Boolean.FALSE);
            reportesRepository.deleteReporteDiario(cvePresupuestal, cvePeriodoImss);
            resultado = procesarCierre(cvePresupuestal);
        }
        }
        logger.info("Resultado: " + resultado);
        return resultado;
    }

    @Override
    public Integer procesarCierre(String cvePresupuestal) {

        SimpleDateFormat format1 = new SimpleDateFormat(BaseConstants.FORMATO_FECHA);
        List<EspecialidadesUnidadVo> listEspecialidades = reportesRepository.especialidades(cvePresupuestal);
        PeriodosImss peridoCalculo = reportesRepository.obtenerPeriodoImssXcvePeriodoImss(cvePeriodoImss);

        List<EspecialidadesUnidadVo> listEspPediatrica = agregarEspecialidadPediatrica(cvePresupuestal,
                peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal());
        listEspecialidades.addAll(listEspPediatrica);

        for (int i = 0; i < listEspecialidades.size(); i++) {
        	int regreso = reportesRepository.obtenerRegistroPeriodoII(cvePresupuestal, cvePeriodoImss, listEspecialidades.get(i).getCveEspecialidad(), listEspecialidades.get(i).getCveDivision());
            if(regreso > 0){
            	continue;
            }
        	
            SitParteII sitParteII = new SitParteII();

            sitParteII.setCvePeriodoImss(cvePeriodoImss);
            sitParteII.setCvePresupuestal(cvePresupuestal);
            sitParteII.setRefUnidadMedica(listEspecialidades.get(i).getDesUnidadMedica());
            sitParteII.setCveEspecialidad(listEspecialidades.get(i).getCveEspecialidad());
            sitParteII.setRefEspecialidad(listEspecialidades.get(i).getDesEspecialidad());
            sitParteII.setCveDivision(listEspecialidades.get(i).getCveDivision());
            sitParteII.setRefDivision(listEspecialidades.get(i).getDesDivision());
            sitParteII.setNumEjecucion(1);

            sitParteII.setNumMedico(
                    reportesRepository.calculaMedicos(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                            peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal()));

            sitParteII.setNumCama(
                    reportesRepository.calculaCamas(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad()));

            sitParteII.setNumDiasCama(reportesRepository.calculaDiasCamas(cvePresupuestal,
                    listEspecialidades.get(i).getCveEspecialidad(), format1.format(peridoCalculo.getFechaInicial()),
                    format1.format(peridoCalculo.getFechaFinal())));
            
            if(listEspecialidades.get(i).getCveEspecialidad().equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)){
            	contarIngresosAlojamiento(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
	                    peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal(), sitParteII);
            	
            	contarEgresosAlojamiento(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                        peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal(), sitParteII);
            }else{

	            contarIngresosXEsp(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
	                    peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal(), sitParteII);
	
	            contarEgresosXEsp(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
	                    peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal(), sitParteII);
            }
            
            contarInterQuirurgica(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                    peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal(), sitParteII);

            if ((!listEspecialidades.get(i).getCveEspecialidad().equals(URGENCIAS))
                    && (!listEspecialidades.get(i).getCveEspecialidad().equals(CIRUGIA_AMBULATORIA))) {
                sitParteII.setNumDiasPaciente(
                        contarDiasPaciente(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                                peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal()));
            } else {
                sitParteII.setNumDiasPaciente(0.0);
            }

            porcentajes(sitParteII);

            reportesRepository.insertarPeriodo(sitParteII);
        }

        int enfermosPeriodo = reportesRepository.obtenerRegistroMovEnfermoPeriodo(cvePresupuestal, cvePeriodoImss);
   	 	if(enfermosPeriodo > 0){
        }else{
        	SitMovEnfermo sitMovEnfermo = contarMovEnfermos(cvePresupuestal, peridoCalculo.getFechaInicial(),
                    peridoCalculo.getFechaFinal());
        reportesRepository.insertarEnfermosPeriodo(sitMovEnfermo);
        }
        
        int mtnoInfPeriodo = reportesRepository.obtenerRegistroMatInfantilPeriodo(cvePresupuestal, cvePeriodoImss);
		 	if(mtnoInfPeriodo > 0){
	     }else{
	    	 SitMaternoInfantil sitMaternoInfantil = tocoCirugia(peridoCalculo.getFechaInicial(),
	                 peridoCalculo.getFechaFinal(), cvePresupuestal);		        
		        reportesRepository.insertarMaternoInfantilPeriodo(sitMaternoInfantil);
	     }
        // 2 Parte II - Periodo
        // 4 Finalizado
        reportesRepository.finBitacora(cvePresupuestal, cvePeriodoImss, 2, 4);
        return 1;
    }

    @Override
    public Integer procesarAcomulado(String cvePresupuestal) throws ParseException {

        Calendar calendarFecIni = GregorianCalendar.getInstance();
        Calendar calendarAxu = GregorianCalendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat(BaseConstants.FORMATO_FECHA);

        try {
            cvePeriodoImss = reportesRepository.obtenerPeriodoParaCierre(cvePresupuestal);
        } catch (HospitalizacionException e) {
            logger.error("Error: " + e.getMessage());
        }

        // 3 Parte II - Acumulado
        // 2 En ejecución
        reportesRepository.updateBitacora(cvePresupuestal, cvePeriodoImss, 3, 2, Boolean.FALSE);
        reportesRepository.deleteReporteAcumulado(cvePresupuestal, cvePeriodoImss);

        List<EspecialidadesUnidadVo> listEspecialidades = reportesRepository.especialidades(cvePresupuestal);
        PeriodosImss peridoCalculo = reportesRepository.obtenerPeriodoImssXcvePeriodoImss(cvePeriodoImss);

        int anoPeriodoInicial = Integer.parseInt(peridoCalculo.getNumeroAnioPeriodo());
        int year = anoPeriodoInicial - 1;
        calendarAxu.setTime(peridoCalculo.getFechaInicial());

        calendarFecIni.set(year, Calendar.DECEMBER, calendarAxu.get(Calendar.DATE), 0, 0, 0);
        Date dateFecIni = format1.parse(format1.format(calendarFecIni.getTime()));

        List<EspecialidadesUnidadVo> listEspPediatrica = agregarEspecialidadPediatrica(cvePresupuestal, dateFecIni,
                peridoCalculo.getFechaFinal());
        listEspecialidades.addAll(listEspPediatrica);

        for (int i = 0; i < listEspecialidades.size(); i++) {
        	 int regreso = reportesRepository.obtenerRegistroPeriodoII(cvePresupuestal, cvePeriodoImss, listEspecialidades.get(i).getCveEspecialidad(), listEspecialidades.get(i).getCveDivision());
        	 if(regreso > 0){
             	continue;
             }
        	
            SitParteII sitParteII = new SitParteII();

            sitParteII.setCvePeriodoImss(cvePeriodoImss);
            sitParteII.setCvePresupuestal(cvePresupuestal);
            sitParteII.setRefUnidadMedica(listEspecialidades.get(i).getDesUnidadMedica());
            sitParteII.setCveEspecialidad(listEspecialidades.get(i).getCveEspecialidad());
            sitParteII.setRefEspecialidad(listEspecialidades.get(i).getDesEspecialidad());
            sitParteII.setCveDivision(listEspecialidades.get(i).getCveDivision());
            sitParteII.setRefDivision(listEspecialidades.get(i).getDesDivision());
            sitParteII.setNumEjecucion(1);

            sitParteII.setNumMedico(reportesRepository.calculaMedicos(cvePresupuestal,
                    listEspecialidades.get(i).getCveEspecialidad(), dateFecIni, peridoCalculo.getFechaFinal()));

            sitParteII.setNumCama(
                    reportesRepository.calculaCamas(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad()));

            sitParteII.setNumDiasCama(
                    reportesRepository.calculaDiasCamas(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                            format1.format(calendarFecIni.getTime()), format1.format(peridoCalculo.getFechaFinal())));
            
            if(listEspecialidades.get(i).getCveEspecialidad().equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)){
            	contarIngresosAlojamiento(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
	                    peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal(), sitParteII);
            	
            	contarEgresosAlojamiento(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                        peridoCalculo.getFechaInicial(), peridoCalculo.getFechaFinal(), sitParteII);
            }else{

	            contarIngresosXEsp(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(), dateFecIni,
	                    peridoCalculo.getFechaFinal(), sitParteII);
	
	            contarEgresosXEsp(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(), dateFecIni,
	                    peridoCalculo.getFechaFinal(), sitParteII);
            
            }

            contarInterQuirurgica(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(), dateFecIni,
                    peridoCalculo.getFechaFinal(), sitParteII);

            if ((!listEspecialidades.get(i).getCveEspecialidad().equals(URGENCIAS))
                    && (!listEspecialidades.get(i).getCveEspecialidad().equals(CIRUGIA_AMBULATORIA))) {
                sitParteII.setNumDiasPaciente(contarDiasPaciente(cvePresupuestal,
                        listEspecialidades.get(i).getCveEspecialidad(), dateFecIni, peridoCalculo.getFechaFinal()));
            } else {
                sitParteII.setNumDiasPaciente(0.0);
            }

            porcentajes(sitParteII);

            reportesRepository.insertarAcomulado(sitParteII);
        }

        int enfermosAcumulado = reportesRepository.obtenerRegistroMovEnfermoAcumulado(cvePresupuestal, cvePeriodoImss);
   	 	if(enfermosAcumulado > 0){
        }else{
        SitMovEnfermo sitMovEnfermo = contarMovEnfermos(cvePresupuestal, dateFecIni, peridoCalculo.getFechaFinal());
        reportesRepository.insertarEnfermosAcomulado(sitMovEnfermo);
        }
        
   	 int mtnoInfAcumulado = reportesRepository.obtenerRegistroMatInfantilAcumulado(cvePresupuestal, cvePeriodoImss);
	 	if(mtnoInfAcumulado > 0){
     }else{
    	 SitMaternoInfantil sitMaternoInfantil = tocoCirugia(dateFecIni, peridoCalculo.getFechaFinal(), cvePresupuestal);
         reportesRepository.insertarMaternoInfantilAcomulado(sitMaternoInfantil);
     }

       

        // 3 Parte II - Acumulado
        // 4 Finalizado
        reportesRepository.finBitacora(cvePresupuestal, cvePeriodoImss, 3, 4);
        return 1;
    }

    public List<EspecialidadesUnidadVo> agregarEspecialidadPediatrica(String cvePresupuestal, Date fechaInicial,
            Date fechaFinal) {

        List<EspecialidadesUnidadVo> especialidadesFinal = new ArrayList<EspecialidadesUnidadVo>();
        List<EspecialidadesUnidadVo> listEspecialidad = reportesRepository.especialidadIndPed(cvePresupuestal);
        for (EspecialidadesUnidadVo especialidad : listEspecialidad) {
            int camasPediatria = reportesRepository.camasPediatria(cvePresupuestal, especialidad.getCveEspecialidad());
            int productividadPediatria = reportesRepository.productividadPediatria(cvePresupuestal,
                    especialidad.getCveEspecialidad(), fechaInicial, fechaFinal);
            if (camasPediatria > 0 || productividadPediatria > 0) {
                especialidadesFinal.add(especialidad);
            }
        }
        return especialidadesFinal;

    }

    @Override
    public Integer procesarPeriodo(String cvePresupuestal) {

        SimpleDateFormat format1 = new SimpleDateFormat(BaseConstants.FORMATO_FECHA);
        List<EspecialidadesUnidadVo> listEspecialidades = reportesRepository.especialidades(cvePresupuestal);
        PeriodosImss peridoCalculo = reportesRepository.obtenerPeriodoImssXcvePeriodoImss(cvePeriodoImss);
        Date ultimoDiaCerrado = reportesRepository.ultimoDiaCerrado(cvePresupuestal);
        Date ultimoDiaCerradoAux;

        Calendar calendarAxu = GregorianCalendar.getInstance();
        calendarAxu.setTime(ultimoDiaCerrado);
        calendarAxu.add(Calendar.DATE, 1);

        ultimoDiaCerradoAux = calendarAxu.getTime();

        List<EspecialidadesUnidadVo> listEspPediatrica = agregarEspecialidadPediatrica(cvePresupuestal,
                peridoCalculo.getFechaInicial(), ultimoDiaCerrado);
        listEspecialidades.addAll(listEspPediatrica);

        // TODO Sumar un dia a la fecha ultimoDiaCerrado
        // truncar la fecha inicio
        for (int i = 0; i < listEspecialidades.size(); i++) {
        	 int regreso = reportesRepository.obtenerRegistroPeriodoII(cvePresupuestal, cvePeriodoImss, listEspecialidades.get(i).getCveEspecialidad(), listEspecialidades.get(i).getCveDivision());
        	 if(regreso > 0){
             	continue;
             }
        	
            SitParteII sitParteII = new SitParteII();

            sitParteII.setCvePeriodoImss(cvePeriodoImss);
            sitParteII.setCvePresupuestal(cvePresupuestal);
            sitParteII.setRefUnidadMedica(listEspecialidades.get(i).getDesUnidadMedica());
            sitParteII.setCveEspecialidad(listEspecialidades.get(i).getCveEspecialidad());
            sitParteII.setRefEspecialidad(listEspecialidades.get(i).getDesEspecialidad());
            sitParteII.setCveDivision(listEspecialidades.get(i).getCveDivision());
            sitParteII.setRefDivision(listEspecialidades.get(i).getDesDivision());
            sitParteII.setNumEjecucion(1);

            sitParteII.setNumMedico(reportesRepository.calculaMedicos(cvePresupuestal,
                    listEspecialidades.get(i).getCveEspecialidad(), peridoCalculo.getFechaInicial(), ultimoDiaCerrado));

            sitParteII.setNumCama(
                    reportesRepository.calculaCamas(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad()));

            sitParteII.setNumDiasCama(
                    reportesRepository.calculaDiasCamas(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                            format1.format(peridoCalculo.getFechaInicial()), format1.format(ultimoDiaCerrado)));
            
            if(listEspecialidades.get(i).getCveEspecialidad().equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)){
            	contarIngresosAlojamiento(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
	                    peridoCalculo.getFechaInicial(), ultimoDiaCerrado, sitParteII);
            	
            	contarEgresosAlojamiento(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                        peridoCalculo.getFechaInicial(), ultimoDiaCerrado, sitParteII);
            }else{
	            contarIngresosXEsp(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
	                    peridoCalculo.getFechaInicial(), ultimoDiaCerrado, sitParteII);
	            
	            contarEgresosXEsp(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                        peridoCalculo.getFechaInicial(), ultimoDiaCerrado, sitParteII);
            }
            
            contarEgresosXEsp(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                    peridoCalculo.getFechaInicial(), ultimoDiaCerrado, sitParteII);

            contarInterQuirurgica(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                    peridoCalculo.getFechaInicial(), ultimoDiaCerrado, sitParteII);

            if ((!listEspecialidades.get(i).getCveEspecialidad().equals(URGENCIAS))
                    && (!listEspecialidades.get(i).getCveEspecialidad().equals(CIRUGIA_AMBULATORIA))) {
                sitParteII.setNumDiasPaciente(
                        contarDiasPaciente(cvePresupuestal, listEspecialidades.get(i).getCveEspecialidad(),
                                peridoCalculo.getFechaInicial(), ultimoDiaCerradoAux));
            } else {
                sitParteII.setNumDiasPaciente(0.0);
            }

            porcentajes(sitParteII);

            reportesRepository.insertarPeriodo(sitParteII);
        }

        

        int enfermosPeriodo = reportesRepository.obtenerRegistroMovEnfermoPeriodo(cvePresupuestal, cvePeriodoImss);
   	 	if(enfermosPeriodo > 0){
        	logger.error("Ya existen registros para la tabla SIT_MOV_ENFERMO_PERIODO con los sigs datos: Clave Presupuestal: " + cvePresupuestal + " Clave Periodo: " + cvePeriodoImss);
        }else{
        SitMovEnfermo sitMovEnfermo = contarMovEnfermos(cvePresupuestal, peridoCalculo.getFechaInicial(),
                    ultimoDiaCerrado);
       reportesRepository.insertarEnfermosPeriodo(sitMovEnfermo);
        }
   	 	
   	 	int mtnoInfPeriodo = reportesRepository.obtenerRegistroMatInfantilPeriodo(cvePresupuestal, cvePeriodoImss);
		 	if(mtnoInfPeriodo > 0){
	     }else{
		        SitMaternoInfantil sitMaternoInfantil = tocoCirugia(peridoCalculo.getFechaInicial(), ultimoDiaCerrado, cvePresupuestal);
		        reportesRepository.insertarMaternoInfantilPeriodo(sitMaternoInfantil);
	     }
        // 1 Parte II - Diario
        // 4 Finalizado
        reportesRepository.finBitacora(cvePresupuestal, cvePeriodoImss, 1, 4);

        return 1;
    }

    /**
     * Metodo que calcula los ingresos por especialidad
     * 
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @param sitParteII
     */
    private void contarIngresosXEsp(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin,
            SitParteII sitParteII) {

        Integer numIngProgramado = 0;
        Integer numIngUrgente = 0;
        Integer numIngHospital = 0;

        numIngProgramado = reportesRepository.calculaIngresosProgramados(cvePresupuestal, cveEspecialidad, fechaInicio,
                fechaFin);
        numIngUrgente = reportesRepository.calculaIngresosUrgentes(cvePresupuestal, cveEspecialidad, fechaInicio,
                fechaFin);

        numIngProgramado = numIngProgramado + reportesRepository.calculaIngresosProgramadosOrigen(cvePresupuestal,
                cveEspecialidad, fechaInicio, fechaFin);
        numIngUrgente = numIngUrgente + reportesRepository.calculaIngresosUrgentesOrigen(cvePresupuestal,
                cveEspecialidad, fechaInicio, fechaFin);

        numIngHospital = reportesRepository.calculaIngresosHospital(cvePresupuestal, cveEspecialidad, fechaInicio,
                fechaFin);

        sitParteII.setNumIngProgramado(numIngProgramado);
        sitParteII.setNumIngUrgente(numIngUrgente);
        sitParteII.setNumIngHospital(numIngHospital);

        sitParteII.setNumTotalIngreso(
                sitParteII.getNumIngProgramado() + sitParteII.getNumIngUrgente() + sitParteII.getNumIngHospital());
    }
    
    /**
     * Metodo que calcula los ingresos para alojamiento conjunto
     * 
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @param sitParteII
     */
    private void contarIngresosAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin,
            SitParteII sitParteII) {

        Integer numIngProgramado = 0;
        Integer numIngUrgente = 0;
        Integer numIngHospital = 0;

        numIngProgramado = reportesRepository.calculaIngresosProgramadosAlojamiento(cvePresupuestal, cveEspecialidad, fechaInicio,
                fechaFin);
        numIngUrgente = reportesRepository.calculaIngresosUrgentesAlojamiento(cvePresupuestal, cveEspecialidad, fechaInicio,
                fechaFin);

        numIngProgramado = numIngProgramado + reportesRepository.calculaIngresosProgramadosOrigenAlojamiento(cvePresupuestal,
                cveEspecialidad, fechaInicio, fechaFin);
        numIngUrgente = numIngUrgente + reportesRepository.calculaIngresosUrgentesOrigenAlojamiento(cvePresupuestal,
                cveEspecialidad, fechaInicio, fechaFin);

        numIngHospital = reportesRepository.calculaIngresosHospitalAlojamiento(cvePresupuestal, cveEspecialidad, fechaInicio,
                fechaFin);

        sitParteII.setNumIngProgramado(numIngProgramado);
        sitParteII.setNumIngUrgente(numIngUrgente);
        sitParteII.setNumIngHospital(numIngHospital);

        sitParteII.setNumTotalIngreso(
                sitParteII.getNumIngProgramado() + sitParteII.getNumIngUrgente() + sitParteII.getNumIngHospital());
    }

    /**
     * Metodo que calcula los Egresos por especialidad
     * 
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @param sitParteII
     */
    private void contarEgresosXEsp(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin,
            SitParteII sitParteII) {

        sitParteII.setNumEgrExterna(
                reportesRepository.calculaEgresosCexterna(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));
        sitParteII.setNumEgrUnidad(
                reportesRepository.calculaEgresosUnidad(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));
        sitParteII.setNumEgrOtrohosp(
                reportesRepository.calculaEgresosOhp(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));
        sitParteII.setNumEgrDefuncion(
                reportesRepository.calculaEgresosDefuncion(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));
        sitParteII.setNumPostMorten(
                reportesRepository.calculaEgresosPostMortem(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));

        Integer hospitalMov = reportesRepository.calculaEgresosHospitalMov(cvePresupuestal, cveEspecialidad,
                fechaInicio, fechaFin);
        Integer hospitalHop = reportesRepository.calculaEgresosHospitalHos(cvePresupuestal, cveEspecialidad,
                fechaInicio, fechaFin);

        sitParteII.setNumEgrHospital(hospitalMov + hospitalHop);
        sitParteII.setNumTotalEgreso((double) (sitParteII.getNumEgrExterna() + sitParteII.getNumEgrUnidad()
                + sitParteII.getNumEgrHospital() + sitParteII.getNumEgrOtrohosp() + sitParteII.getNumEgrDefuncion()));

    }
    
    /**
     * Metodo que calcula los Egresos de la especialidad alojamiento conjunto
     * 
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @param sitParteII
     */
    private void contarEgresosAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin,
            SitParteII sitParteII) {

        sitParteII.setNumEgrExterna(
                reportesRepository.calculaEgresosAlojamientoCexterna(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));
        sitParteII.setNumEgrUnidad(
                reportesRepository.calculaEgresosUnidadAlojamiento(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));
        sitParteII.setNumEgrOtrohosp(
                reportesRepository.calculaEgresosOhpAlojamiento(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));
        sitParteII.setNumEgrDefuncion(
                reportesRepository.calculaEgresosDefuncionAlojamiento(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));
        sitParteII.setNumPostMorten(
                reportesRepository.calculaEgresosPostMortemAlojamiento(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin));

        Integer hospitalMov = reportesRepository.calculaEgresosHospitalMovAlojamiento(cvePresupuestal, cveEspecialidad,
                fechaInicio, fechaFin);
        Integer hospitalHop = reportesRepository.calculaEgresosHospitalHosAlojamiento(cvePresupuestal, cveEspecialidad,
                fechaInicio, fechaFin);

        sitParteII.setNumEgrHospital(hospitalMov + hospitalHop);
        sitParteII.setNumTotalEgreso((double) (sitParteII.getNumEgrExterna() + sitParteII.getNumEgrUnidad()
                + sitParteII.getNumEgrHospital() + sitParteII.getNumEgrOtrohosp() + sitParteII.getNumEgrDefuncion()));

    }

    /**
     * Metodo que calcula las Intervenciones Quirurgicas
     * 
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @param sitParteII
     */
    private void contarInterQuirurgica(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin,
            SitParteII sitParteII) {

        if (cveEspecialidad.equals(CIRUGIA_AMBULATORIA)) {
            sitParteII.setNumTotalIq(
                    (double) reportesRepository.calculaTotInterQuirurCirugia(cvePresupuestal, fechaInicio, fechaFin));
        } else {
            sitParteII.setNumTotalIq((double) reportesRepository.calculaTotalInterQuirurgica(cvePresupuestal,
                    cveEspecialidad, fechaInicio, fechaFin));
        }
        sitParteII.setNumIqProgramado((double) reportesRepository.calculaInterQuirurgicaProg(cvePresupuestal,
                cveEspecialidad, fechaInicio, fechaFin));
    }

    /**
     * Metodo que calculo los dias Pacientes
     * 
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @param sitParteII
     * @return
     */
    private Double contarDiasPaciente(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin) {

        Double diasMov = 0.0;
        Double diasIng = 0.0;
        Double diasTotal = 0.0;

        diasMov = diasPacienteMov(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin);
        diasIng = diasPacienteIng(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin);

        diasTotal = diasMov + diasIng;

        return diasTotal;
    }

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    private Double diasPacienteMov(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin) {

        List<MovimientoIntraHospitalario> listMovimeitnos = reportesRepository.listaEspeMovimiento(fechaInicio,
                fechaFin, cvePresupuestal, cveEspecialidad);

        Double diasPacienteMovimiento = 0.0;
        for (MovimientoIntraHospitalario mov : listMovimeitnos) {
            Integer diasMov = reportesRepository.diasPacienteMov(cveEspecialidad, mov.getClaveIngreso());

            Integer diasIngEgreso = reportesRepository.diasPacienteIngresoEgreso(fechaInicio, fechaFin, cvePresupuestal,
                    cveEspecialidad, mov.getClaveIngreso());

            Integer diasIngresoMov = reportesRepository.diasPacienteIngresoMov(fechaInicio, fechaFin, cvePresupuestal,
                    cveEspecialidad, mov.getClaveIngreso());

            Integer diasMovEgreso = reportesRepository.diasPacienteMovEgreso(fechaInicio, fechaFin, cvePresupuestal,
                    cveEspecialidad, mov.getClaveIngreso());

            diasPacienteMovimiento += diasMov + diasIngEgreso + diasIngresoMov + diasMovEgreso;
        }

        return diasPacienteMovimiento;
    }

    /**
     * @param cvePresupuestal
     * @param cveEspecialidad
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    private Double diasPacienteIng(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin) {

        //List<Ingreso> listIngreso = reportesRepository.listaEspeIngreso(cvePresupuestal, cveEspecialidad, fechaInicio);
    	List<Ingreso> listIngreso = reportesRepository.listaEspeIngreso(cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin);

        Double diasPacienteIngreso = 0.0;
        for (Ingreso ingreso : listIngreso) {
            Integer diasMov = reportesRepository.diasPacienteMov(cveEspecialidad, ingreso.getCveIngreso());

            Integer diasIngEgreso = reportesRepository.diasPacienteIngresoEgresoSinFec(fechaInicio, fechaFin,
                    cvePresupuestal, cveEspecialidad, ingreso.getCveIngreso());

            Integer diasMovEgreso = reportesRepository.diasPacienteMovEgreso(fechaInicio, fechaFin, cvePresupuestal,
                    cveEspecialidad, ingreso.getCveIngreso());

            diasPacienteIngreso += diasMov + diasIngEgreso + diasMovEgreso;
        }
        return diasPacienteIngreso;
    }

    /**
     * Metodo que hace los calculos de porcentajes
     * 
     * @param sitParteII
     */
    private void porcentajes(SitParteII sitParteII) {

        if (sitParteII.getNumTotalEgreso() == 0) {
            sitParteII.setPorcDefuncion(0.0);
        } else {
            sitParteII.setPorcDefuncion(sitParteII.getNumEgrDefuncion() / sitParteII.getNumTotalEgreso());
        }

        if (sitParteII.getNumEgrDefuncion() == 0) {
            sitParteII.setPorcPostMorten(0.0);
        } else {
            sitParteII.setPorcPostMorten((double) (sitParteII.getNumPostMorten() / sitParteII.getNumEgrDefuncion()));
        }

        if (sitParteII.getNumDiasCama() == 0) {
            sitParteII.setPorcOcupacion(0.0);
        } else {
            sitParteII.setPorcOcupacion((sitParteII.getNumDiasPaciente() / sitParteII.getNumDiasCama()) * 100);
        }

        if (sitParteII.getNumTotalEgreso() == 0) {
            sitParteII.setPorcDiasEstancia(0.0);
        } else {
            sitParteII.setPorcDiasEstancia(sitParteII.getNumDiasPaciente() / sitParteII.getNumTotalEgreso());
        }

        if (sitParteII.getNumCama() == 0) {
            sitParteII.setPorcIndiceRotacion(0.0);
        } else {
            sitParteII.setPorcIndiceRotacion(sitParteII.getNumTotalEgreso() / sitParteII.getNumCama());
        }

        if (sitParteII.getNumTotalEgreso() == 0) {
            sitParteII.setPorcInidiceSustitucion(0.0);
        } else {
            sitParteII.setPorcInidiceSustitucion(
                    (sitParteII.getNumDiasCama() - sitParteII.getNumDiasPaciente()) / sitParteII.getNumTotalEgreso());
        }

    }

    /**
     * Metodo que calcula los Movimeintos Enferos de Adultos
     * 
     * @param cvePresupuestal
     * @param fechaInicio
     * @param fechaFin
     * @param sitMovEnfermo
     */
    private void contarMovEnfermosAdultos(String cvePresupuestal, Date fechaInicio, Date fechaFin,
            SitMovEnfermo sitMovEnfermo) {

        sitMovEnfermo.setNumAnteriorAdulto(reportesRepository.calculaIngresosAntAdul(fechaInicio, cvePresupuestal));
        sitMovEnfermo.setNumIngresoAdulto(
                reportesRepository.calculaIngresosPeriodoAdul(fechaInicio, fechaFin, cvePresupuestal));
        sitMovEnfermo.setNumEgresoAdulto(
                reportesRepository.calculaEgresosPeriodoAdul(fechaInicio, fechaFin, cvePresupuestal));

        sitMovEnfermo.setNumActualAdulto(sitMovEnfermo.getNumAnteriorAdulto() + sitMovEnfermo.getNumIngresoAdulto()
                - sitMovEnfermo.getNumEgresoAdulto());
    }

    /**
     * Metodo que calcula los Movimeintos Enferos de Cunero
     * 
     * @param cvePresupuestal
     * @param fechaInicio
     * @param fechaFin
     * @param sitMovEnfermo
     */
    private void contarMovEnfermosCuneros(String cvePresupuestal, Date fechaInicio, Date fechaFin,
            SitMovEnfermo sitMovEnfermo) {

        sitMovEnfermo.setNumAnteriorCunero(reportesRepository.calculaIngresosAntCunero(fechaInicio, cvePresupuestal));
        sitMovEnfermo.setNumIngresoCunero(
                reportesRepository.calculaIngresosPeriodoCunero(fechaInicio, fechaFin, cvePresupuestal));
        sitMovEnfermo.setNumEgresoCunero(
                reportesRepository.calculaEgresosPeriodoCunero(fechaInicio, fechaFin, cvePresupuestal));

        sitMovEnfermo.setNumActualCunero(sitMovEnfermo.getNumAnteriorCunero() + sitMovEnfermo.getNumIngresoCunero()
                - sitMovEnfermo.getNumEgresoCunero());
    }

    /**
     * @param cvePresupuestal
     * @param fechaInicio
     * @param fechaFin
     * @param sitMovEnfermo
     * @return
     */
    private SitMovEnfermo contarMovEnfermos(String cvePresupuestal, Date fechaInicio, Date fechaFin) {

        SitMovEnfermo sitMovEnfermo = new SitMovEnfermo();
        sitMovEnfermo.setCvePeriodoImss(cvePeriodoImss);
        sitMovEnfermo.setCvePresupuestal(cvePresupuestal);
        sitMovEnfermo.setNumEjecucion(1);

        contarMovEnfermosAdultos(cvePresupuestal, fechaInicio, fechaFin, sitMovEnfermo);
        contarMovEnfermosCuneros(cvePresupuestal, fechaInicio, fechaFin, sitMovEnfermo);

        return sitMovEnfermo;
    }

    /**
     * @param fechaInicio
     * @param fechaFin
     * @param cvePresupuestal
     * @return
     */
    private SitMaternoInfantil tocoCirugia(Date fechaInicio, Date fechaFin, String cvePresupuestal) {

        SitMaternoInfantil sitMaternoInfantil = new SitMaternoInfantil();
        List<Tococirugia> listToco = reportesRepository.tocoCirugias(fechaInicio, fechaFin, cvePresupuestal);

        Integer totPartoNormal = 0;
        Integer totPartoVaginales = 0;
        Integer totPartoAbdominales = 0;
        Integer totPreTerminoPesoBajo = 0;
        Integer totPreTerminoPesoNormal = 0;
        Integer totPreTerminoPesoAlto = 0;
        Integer totTerminoPesoBajo = 0;
        Integer totTerminoPesoNormal = 0;
        Integer totTerminoPesoAlto = 0;
        Integer totPostTerminoPesoBajo = 0;
        Integer totPostTerminoPesoNormal = 0;
        Integer totPostTerminoPesoAlto = 0;
        Integer totDefuncion = 0;
        Integer totMortinato = 0;
        Integer totAborto = 0;
        Integer totVivo = 0;

        for (int j = 0; j < listToco.size(); j++) {
            PartosVo partosPojo = reportesRepository.contadorPartos((int) listToco.get(j).getClave());

            totPartoNormal += partosPojo.getPartoNormal();
            totPartoVaginales += partosPojo.getPartoVaginales();
            totPartoAbdominales += partosPojo.getPartoAbdominales();

            NacLubchencoVo nacLubchencoPojo = reportesRepository
                    .contadorPreTerminoPost((int) listToco.get(j).getClave());

            totPreTerminoPesoBajo += nacLubchencoPojo.getPreTerminoPesoBajo();
            totPreTerminoPesoNormal += nacLubchencoPojo.getPreTerminoPesoNormal();
            totPreTerminoPesoAlto += nacLubchencoPojo.getPreTerminoPesoAlto();
            totTerminoPesoBajo += nacLubchencoPojo.getTerminoPesoBajo();
            totTerminoPesoNormal += nacLubchencoPojo.getTerminoPesoNormal();
            totTerminoPesoAlto += nacLubchencoPojo.getTerminoPesoAlto();
            totPostTerminoPesoBajo += nacLubchencoPojo.getPostTerminoPesoBajo();
            totPostTerminoPesoNormal += nacLubchencoPojo.getPostTerminoPesoNormal();
            totPostTerminoPesoAlto += nacLubchencoPojo.getPostTerminoPesoAlto();

            NacidosVo nacidosPojo = reportesRepository.contadorNacidos((int) listToco.get(j).getClave());

            totDefuncion += nacidosPojo.getDefuncion();
            totMortinato += nacidosPojo.getMortinato();
            totAborto += nacidosPojo.getAborto();
            totVivo += nacidosPojo.getVivo();
        }

        sitMaternoInfantil.setCvePresupuestal(cvePresupuestal);
        sitMaternoInfantil.setCvePeriodoImss(cvePeriodoImss);
        sitMaternoInfantil.setNumEjecucion(1);

        sitMaternoInfantil.setNumTotalParto(totPartoNormal + totPartoVaginales + totPartoAbdominales);
        sitMaternoInfantil.setNumPartoNormal(totPartoNormal);
        sitMaternoInfantil.setNumPartoVaginal(totPartoVaginales);
        sitMaternoInfantil.setNumPartoAbdominal(totPartoAbdominales);

        sitMaternoInfantil.setNumPretBajo(totPreTerminoPesoBajo);
        sitMaternoInfantil.setNumPretNormal(totPreTerminoPesoNormal);
        sitMaternoInfantil.setNumPretAlto(totPreTerminoPesoAlto);

        sitMaternoInfantil.setNumTerminoBajo(totTerminoPesoBajo);
        sitMaternoInfantil.setNumTerminoNormal(totTerminoPesoNormal);
        sitMaternoInfantil.setNumTerminoAlto(totTerminoPesoAlto);

        sitMaternoInfantil.setNumPostBajo(totPostTerminoPesoBajo);
        sitMaternoInfantil.setNumPostNormal(totPostTerminoPesoNormal);
        sitMaternoInfantil.setNumPostAlto(totPostTerminoPesoAlto);

        sitMaternoInfantil.setNumDefuncion(totDefuncion);
        sitMaternoInfantil.setNumMortinato(totMortinato);
        sitMaternoInfantil.setNumAborto(totAborto);
        sitMaternoInfantil.setNumNacidoVivo(totVivo);

        return sitMaternoInfantil;
    }
}
