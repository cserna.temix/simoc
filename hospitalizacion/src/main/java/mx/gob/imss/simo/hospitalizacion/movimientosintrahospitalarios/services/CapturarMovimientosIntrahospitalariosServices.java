package mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.services;

import java.util.Date;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.event.SelectEvent;

import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.model.DatosUsuario;

public interface CapturarMovimientosIntrahospitalariosServices extends HospitalizacionCommonServices {

    void validarFechaAtencion(String fechaCaptura, String clavePresupuestal) throws HospitalizacionException;

    void validarNss(String nss) throws HospitalizacionException;

    MovimientoIntraHospitalario obtenerDatosAnteriores(DatosPaciente datosPaciente);
    
    List<DatosCatalogo> actualizarCatEspecialidadActualRN(List<DatosCatalogo> catalogoEspecialidades,Boolean esRecienNacidoExterno);

    void validarEspecialidad(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException;

    DatosCatalogo elementoSelecionado(SelectEvent evento, List<String> listaEspecialidad,
            AutoComplete autoCompleteEspecialidad, List<DatosCatalogo> catalogoEspecialidad);

    void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario capturista, int cveTipoFormato);

    boolean validarActivarGuardar(Boolean[] requeridos);

    //int validarCamaActual(String camaDestino, String cvePresupuestal, String especialidadDestino)
     //       throws HospitalizacionException;
    
    //int validarCamaActual(String claveCama, String clavePresupuestal, Long tipoUbicacion, String especialidadCamaDestino)
      //      throws HospitalizacionException;
    
    int validarCamaActual(DatosHospitalizacion  datosHospitalizacion, String clavePresupuestal) throws HospitalizacionException;

    Boolean validarIngresoRNUcin(DatosHospitalizacion datosHospializacion);
    
    Boolean validarRecienNacidoExterno(long clavePaciente,String cvePresupuestal);
    
    String obtenerCamaAlojamientoConjunto(long clavePaciente,String cvePresupuestal);
    
    void validarSemanasMaxPediatria(Integer edadSemanas, String claveEspecialidad, CamaUnidad cama) throws HospitalizacionException;
    
    void validarMatricula(String matricula) throws HospitalizacionException;
    
    void validarHora(String hora) throws HospitalizacionException;
    
    boolean validarHoraActualMayor(DatosHospitalizacion datosHospitalizacion, String fechaMovIntra);
    
    Date convertirHora(String fecha, String hora);
    
    String obtenerSiguienteDia(String fecha);
    
    void validarCamaAlojamientoConjunto(String nss, Long cvePaciente, String clavePresupuestal) throws HospitalizacionException;
}
