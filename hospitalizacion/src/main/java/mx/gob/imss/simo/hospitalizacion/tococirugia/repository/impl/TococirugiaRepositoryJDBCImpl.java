package mx.gob.imss.simo.hospitalizacion.tococirugia.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLUpdateConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.CatalogoPeriodoAtencionTocoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.CatalogoRangoLubchencoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.ParametroRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.TococirugiaRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoAtencionToco;
import mx.gob.imss.simo.hospitalizacion.common.model.RangoLubchenco;
import mx.gob.imss.simo.hospitalizacion.common.model.RecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.hospitalizacion.tococirugia.repository.TococirugiaRepository;

@Repository("tococirugiaRepository")
public class TococirugiaRepositoryJDBCImpl extends HospitalizacionCommonRepositoryJDBCImpl
        implements TococirugiaRepository {

    @Override
    public List<Tococirugia> buscarUltimaAtencionTococirugia(DatosHospitalizacion datosHospitalizacion) {

        return jdbcTemplate.query(SQLConstants.BITACORA_TOCOCIRUGIA_ULTIMA_ATENCION,
                new Object[] { datosHospitalizacion.getDatosPaciente().getNss(),
                        datosHospitalizacion.getDatosPaciente().getAgregadoMedico(),
                        datosHospitalizacion.getDatosPaciente().getNombre() },
                new TococirugiaRowMapperHelper());
    }

    @Override
    public void guardarTococirugia(Tococirugia tococirugia) {

        jdbcTemplate.update(SQLUpdateConstants.TOCOCIRUGIA_AGREGAR,
                new Object[] { tococirugia.getClave(), tococirugia.getFechaCreacion(),
                        tococirugia.getFechaActualizacion(), tococirugia.getFechaAtencion(), tococirugia.getCveSala(),
                        tococirugia.getCvePresupuestal(), tococirugia.getCveTipoAtencion(),
                        tococirugia.getCveTipoParto(), tococirugia.getCveMetodoAnticonceptivo(),
                        tococirugia.getCantidadMetodoAnticonceptivo(), tococirugia.getTotalRecienNacidos(),
                        tococirugia.getCveMedico(), tococirugia.getCveCapturista(),
                        tococirugia.getCveCapturistaActualizada(), tococirugia.getCveTipoFormato(),tococirugia.getGesta(),
                        tococirugia.getEpisiotomia()}); 
       
        /*Mejoras hospitaliiacion 2020
         * Se actualiza el registro de ingreso de la madre con el numero de recien nacidos*/
        if(tococirugia.getTotalRecienNacidos() != null){
	        jdbcTemplate.update(SQLUpdateConstants.ACTUALIZAR_INGRESO_POR_NUMERO_RECIEN_NACIDOS,
	        		new Object[]{
	        				tococirugia.getTotalRecienNacidos(),
	        				tococirugia.getClave()});
        
        }
    }

    @Override
    public void guardarRecienNacido(RecienNacido recienNacido) {

        jdbcTemplate.update(SQLUpdateConstants.RECIEN_NACIDO_AGREGAR,
                new Object[] { recienNacido.getClave(), recienNacido.getNumeroRecienNacido(),
                        recienNacido.getFechaCreacion(), recienNacido.getFechaActualizacion(), recienNacido.getPeso(),
                        recienNacido.getTalla(), recienNacido.getPerimetroCefalico(),
                        recienNacido.getSemanasGestacion(), recienNacido.getApgar1(), recienNacido.getApgar5(),
                        recienNacido.getFechaDefuncion(), recienNacido.getClaveCapturista(),
                        recienNacido.getClaveCapturistaActualiza(), recienNacido.getClavePaciente(),
                        recienNacido.getIndicadorIngresoRN(), recienNacido.getClaveTipoNacido(),
                        recienNacido.getClaveRangoLubchenco() });
    }

    @Override
    public List<RangoLubchenco> obtenerRangoLubchenco(int semanasGestacion, int peso) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_CLAVE_RANGO_LUBCHENCO, new Object[] { semanasGestacion, peso },
                new CatalogoRangoLubchencoRowMapperHelper());

    }

    @Override
    public List<PeriodoAtencionToco> obtenerPeriodoAtencionToco(int cveAtencionPrevia, int cveAtencionActual) {

        return jdbcTemplate.query(SQLConstants.BUSCAR_PERIODO_ATENCION_TOCOCIRUGIA,
                new Object[] { cveAtencionPrevia, cveAtencionActual },
                new CatalogoPeriodoAtencionTocoRowMapperHelper());
    }

    @Override
    public Parametro obtenerParametroMaxRecienNacido(String cveParametroMaxRN) {

        List<Parametro> parametro = jdbcTemplate.query(SQLConstants.BUSCAR_PARAMETRO,
                new Object[] { cveParametroMaxRN }, new ParametroRowMapperHelper());

        if (null != parametro && !parametro.isEmpty()) {
            return parametro.get(0);
        }
        return null;
    }

	@Override
	public void actualizarMetodoAnticonceptivoTococirugia(int cve_metodo, int cantidad, long cve_tococirugia) {
		if(cantidad > 0){
			 jdbcTemplate.update(SQLUpdateConstants.ACTUALIZAR_METODO_ANTICONCEP_TOCOCIRUGIA,
					 new Object[]{cve_metodo,cantidad,cve_tococirugia});
		}else{
			 jdbcTemplate.update(SQLUpdateConstants.ACTUALIZAR_METODO_ANTICONCEP_TOCOCIRUGIA,
					 new Object[]{cve_metodo,null,cve_tococirugia});
		}
	}

}
