package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class Menu {

    private Integer claveMenu;
    private Integer menuPadre;
    private String descripcionMenu;
    private String refAccion;
    private Integer posicion;
    private String cuentaAd;

}
