package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class CamaUnidad {

    private String cveCama;
    private String cveEspecialidad;
    private String cvePresupuestal;
    private Date fecBaja;
    private boolean indCensable;
    private boolean indFicticio;
    private String refAla;
    private String refPiso;
    private boolean indCamaPedriatica;

}
