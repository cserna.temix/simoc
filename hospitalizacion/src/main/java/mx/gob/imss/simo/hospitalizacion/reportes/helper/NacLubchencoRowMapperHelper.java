/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.NacLubchencoVo;

/**
 * @author francisco.rodriguez
 *
 */
public class NacLubchencoRowMapperHelper extends BaseHelper implements RowMapper<NacLubchencoVo> {

	@Override
	public NacLubchencoVo mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		NacLubchencoVo nacLubchenco = new NacLubchencoVo();
		
		nacLubchenco.setPreTerminoPesoBajo(resultSet.getInt(SQLColumnasConstants.PRE_TERMINO_PESO_BAJO));
		nacLubchenco.setPreTerminoPesoNormal(resultSet.getInt(SQLColumnasConstants.PRE_TERMINO_PESO_NORMAL));
		nacLubchenco.setPreTerminoPesoAlto(resultSet.getInt(SQLColumnasConstants.PRE_TERMINO_PESO_ALTO));
		
		nacLubchenco.setTerminoPesoBajo(resultSet.getInt(SQLColumnasConstants.TERMINO_PESO_BAJO));
		nacLubchenco.setTerminoPesoNormal(resultSet.getInt(SQLColumnasConstants.TERMINO_PESO_NORMAL));
		nacLubchenco.setTerminoPesoAlto(resultSet.getInt(SQLColumnasConstants.TERMINO_PESO_ALTO));
		
		nacLubchenco.setPostTerminoPesoBajo(resultSet.getInt(SQLColumnasConstants.POST_TERMINO_PESO_BAJO));
		nacLubchenco.setPostTerminoPesoNormal(resultSet.getInt(SQLColumnasConstants.POST_TERMINO_PESO_NORMAL));
		nacLubchenco.setPostTerminoPesoAlto(resultSet.getInt(SQLColumnasConstants.POST_TERMINO_PESO_ALTO));
		
		
		return nacLubchenco;
	}

}
