package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;

public class ProcedimientoRowMapperHelper extends BaseHelper implements RowMapper<Procedimiento> {

    @Override
    public Procedimiento mapRow(ResultSet resultSet, int rowId) throws SQLException {

        Procedimiento procedimiento = new Procedimiento();
        procedimiento.setCveCie9(resultSet.getString(SQLColumnasConstants.CVE_CIE9));
        procedimiento.setDesCie9(resultSet.getString(SQLColumnasConstants.DES_CIE9));
        procedimiento.setNumFiltro(resultSet.getInt(SQLColumnasConstants.NUM_FILTRO));
        procedimiento.setCveSexo(resultSet.getInt(SQLColumnasConstants.CVE_SEXO_CIE9));
        procedimiento.setNumEdadInferiorAnios(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_INFERIOR_ANIOS));
        procedimiento.setNumEdadInferiorSemanas(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_INFERIOR_SEMANAS));
        procedimiento.setNumEdadSuperiorAnios(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_SUPERIOR_ANIOS));
        procedimiento.setNumEdadSuperiorSemanas(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_SUPERIOR_SEMANAS));
        procedimiento.setNumAnioVigencia(resultSet.getInt(SQLColumnasConstants.NUM_ANIO_VIGENCIA));
        procedimiento.setRefUso(resultSet.getString(SQLColumnasConstants.REF_USO));
        procedimiento.setRefActualizacion(resultSet.getString(SQLColumnasConstants.REF_ACTUALIZACION));
        procedimiento.setNumPrecedencia(resultSet.getInt(SQLColumnasConstants.NUM_PRECEDENCIA));
        procedimiento.setIndNivel1(resultSet.getInt(SQLColumnasConstants.IND_NIVEL1));
        procedimiento.setIndNivel2(resultSet.getInt(SQLColumnasConstants.IND_NIVEL2));
        procedimiento.setIndNivel3(resultSet.getInt(SQLColumnasConstants.IND_NIVEL3));
        procedimiento.setNumCapituloCie9(resultSet.getInt(SQLColumnasConstants.NUM_CAPITULO_CIE9));
        procedimiento.setNumGrupoCie9(resultSet.getInt(SQLColumnasConstants.NUM_GRUPO_CIE9));
        procedimiento.setNumCodigoConsecutivo(resultSet.getInt(SQLColumnasConstants.NUM_CODIGO_CONSECUTIVO));
        procedimiento.setDesCie9m(resultSet.getString(SQLColumnasConstants.DES_CIE9M));
        procedimiento.setIndBilateralidad(resultSet.getInt(SQLColumnasConstants.IND_BILATERALIDAD));
        procedimiento.setFecBaja(resultSet.getDate(SQLColumnasConstants.FEC_BAJA_CIE9));
        return procedimiento;
    }

}
