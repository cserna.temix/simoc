package mx.gob.imss.simo.hospitalizacion.modificaciones.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.hospitalizacion.busquedas.services.BuscarPacienteServices;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.egresos.repository.CapturarEgresoRepository;
import mx.gob.imss.simo.hospitalizacion.ingresos.repository.CapturarIngresoRepository;
import mx.gob.imss.simo.hospitalizacion.modificaciones.helper.ModificacionDatosPacienteHelper;
import mx.gob.imss.simo.hospitalizacion.modificaciones.repository.ModificacionDatosPacienteRepository;
import mx.gob.imss.simo.hospitalizacion.modificaciones.rules.ModificacionDatosPacienteRules;
import mx.gob.imss.simo.hospitalizacion.modificaciones.service.ModificacionDatosPacienteService;

@Service("modificacionDatosPacienteService")
public class ModificacionDatosPacienteServiceImpl extends HospitalizacionCommonServicesImpl
        implements ModificacionDatosPacienteService {

    @Autowired
    private BuscarPacienteServices buscarPacienteServices;

    @Autowired
    private ModificacionDatosPacienteRules modificacionDatosPacienteRule;

    @Autowired
    private ModificacionDatosPacienteHelper modificacionDatosPacienteHelper;

    @Autowired
    private ModificacionDatosPacienteRepository modificacionDatosPacienteRepository;

    @Autowired
    private CapturarIngresoRepository capturarIngresoRepository;
    
    @Autowired
    private CapturarEgresoRepository capturarEgresoRepository;
    
    @Autowired
    private HospitalizacionCommonRepository hospitalizacionCommonRepository;

    @Override
    public void validarNss(String nss) throws HospitalizacionException {

        modificacionDatosPacienteRule.validarNss(nss);
    }
    
    public void validarFechaModificacion(String fechaModificacion, String cvePresupuestal) throws HospitalizacionException {

        modificacionDatosPacienteRule.validarFormatoFecha(fechaModificacion);
        modificacionDatosPacienteRule.validarFechaActual(fechaModificacion);
        super.validarFechaEnPeriodoActualActivo(fechaModificacion, modificacionDatosPacienteHelper.convertStringToDate(fechaModificacion), 
    			cvePresupuestal, TipoCapturaEnum.HOSPITALIZACION.getClave());
    	super.validarReporteRelacionPacientesGenerado(cvePresupuestal, fechaModificacion);
    	super.validarPeriodoAnteriorCerrado(cvePresupuestal,fechaModificacion, TipoCapturaEnum.HOSPITALIZACION.getClave());
    }
    
    public void validarFechaModificacionMayorIngreso(DatosHospitalizacion datosHospitalizacion, String fechaModificacion)
            throws HospitalizacionException {
    	
    	Ingreso ingreso = hospitalizacionCommonRepository
                .buscarIngresoPorDatosPaciente(datosHospitalizacion.getDatosPaciente());
    	modificacionDatosPacienteRule.validarFechaModificacionMayorIngreso(ingreso.getFecIngreso(),
    			modificacionDatosPacienteHelper.obtenerFecha(fechaModificacion,
                        CapturarEgresoConstants.HORA_MAXIMA_COMPLETA));
    	
    }
    
    public void validarFechaModificacionMayorTococirugia(DatosHospitalizacion datosHospitalizacion, String fechaModificacion)
            throws HospitalizacionException {

        Tococirugia tococirugia = capturarEgresoRepository
                .buscarTococirugiaIngreso(datosHospitalizacion.getDatosPaciente());
        if (tococirugia != null) {
        	modificacionDatosPacienteRule.validarFechaModificacionMayorTococirugia(tococirugia.getFechaAtencion(),
        			modificacionDatosPacienteHelper.obtenerFecha(fechaModificacion,
                            CapturarEgresoConstants.HORA_MAXIMA_COMPLETA));
        }
    }
    
    public void validarFechaModificacionMayorIntervenciones(DatosHospitalizacion datosHospitalizacion, String fechaModificacion)
            throws HospitalizacionException {

        IntervencionesQuirurgicas intervencionesQx = capturarEgresoRepository
                .buscarIntervencionesIngreso(datosHospitalizacion.getDatosPaciente());
        if (intervencionesQx != null) {
        	modificacionDatosPacienteRule.validarFechaModificacionMayorIntervencionQx(intervencionesQx.getFechaSalidaSala(),
            		modificacionDatosPacienteHelper.obtenerFecha(fechaModificacion,
                            CapturarEgresoConstants.HORA_MAXIMA_COMPLETA));
        }
    }
    
    public void validarFechaModificacionMayorMovIntra(DatosHospitalizacion datosHospitalizacion, String fechaModificacion)
            throws HospitalizacionException {

    	MovimientoIntraHospitalario movIntrahospitalario = capturarEgresoRepository
                .buscarMovIntraPorCveIngreso(datosHospitalizacion.getDatosPaciente());
    	if (movIntrahospitalario != null) {
        	modificacionDatosPacienteRule.validarFechaModificacionMayorMovIntra(movIntrahospitalario.getFechaMovimiento(),
            		modificacionDatosPacienteHelper.obtenerFecha(fechaModificacion,
                            CapturarEgresoConstants.HORA_MAXIMA_COMPLETA));
    	}
    }
    
    public DatosPaciente buscarPacienteAcceder(DatosPaciente datosPaciente) throws ParseException {

        return buscarPacienteServices
                .buscarPaciente(datosPaciente, new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(new Date()))
                .get(0);
    }

    @Override
    public void validarNumero(String numero) throws HospitalizacionException {

        modificacionDatosPacienteRule.validarNumero(numero);
    }

    @Override
    public void validarDelegacion(String numero, String delegacion, List<DatosCatalogo> delegaciones)
            throws HospitalizacionException {

        modificacionDatosPacienteRule.validarDelegacion(delegacion, delegaciones);
    }

    @Override
    public void guardarModificacionDatosPacienteService(DatosPaciente datosModificados, String nssOriginal,
            String agregado) throws HospitalizacionException {

        modificacionDatosPacienteRepository.guardarModificacionDatosPaciente(datosModificados, nssOriginal, agregado, 
        		modificacionDatosPacienteHelper.convertStringToDate(datosModificados.getFechaModificacion()));

    }

    public void validarListaVaciaDatosHopitalizacion(List<DatosPaciente> listPaciente, String nss)
            throws HospitalizacionException {

        if (listPaciente == null || listPaciente.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_NSS));
        }
    }

    @Override
    public void validarIngresoVigente(String clavePresupuestal, TipoUbicacionEnum tipoUbicacion,
            DatosPaciente datosPaciente) throws HospitalizacionException {

        List<Integer> tipoFormato = new ArrayList<Integer>();
        if (tipoUbicacion.equals(TipoUbicacionEnum.HOSPITALIZACION)
                || tipoUbicacion.equals(TipoUbicacionEnum.URGENCIAS)) {
            tipoFormato.add(TipoFormatoEnum.INGRESO_HOSPITALIZACION.getClave());
            tipoFormato.add(TipoFormatoEnum.INGRESO_URGENCIAS.getClave());
            tipoFormato.add(TipoFormatoEnum.INGRESO_UCI.getClave());
        } else {
            tipoFormato.add(TipoFormatoEnum.INGRESO_UCI.getClave());
        }
        List<Ingreso> ingresos = capturarIngresoRepository.buscarIngresoVigente(clavePresupuestal,
                datosPaciente.getNss(), datosPaciente.getAgregadoMedico(), tipoFormato);
        modificacionDatosPacienteRule.validarIngresoVigente(ingresos, datosPaciente.getCvePaciente(), false);
    }

    public void validarUbicacion(String ubicacion) throws HospitalizacionException {

        if (null == ubicacion) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_UBICACION));
        }
    }

    public void setModificacionDatosPacienteRule(ModificacionDatosPacienteRules modificacionDatosPacienteRule) {

        this.modificacionDatosPacienteRule = modificacionDatosPacienteRule;
    }

    public ModificacionDatosPacienteHelper getModificacionDatosPacienteHelper() {

        return modificacionDatosPacienteHelper;
    }

    public void setModificacionDatosPacienteHelper(ModificacionDatosPacienteHelper modificacionDatosPacienteHelper) {

        this.modificacionDatosPacienteHelper = modificacionDatosPacienteHelper;
    }

    public BuscarPacienteServices getBuscarPacienteServices() {

        return buscarPacienteServices;
    }

    public void setBuscarPacienteServices(BuscarPacienteServices buscarPacienteServices) {

        this.buscarPacienteServices = buscarPacienteServices;
    }

    public ModificacionDatosPacienteRepository getModificacionDatosPacienteRepository() {

        return modificacionDatosPacienteRepository;
    }

    public void setModificacionDatosPacienteRepository(
            ModificacionDatosPacienteRepository modificacionDatosPacienteRepository) {

        this.modificacionDatosPacienteRepository = modificacionDatosPacienteRepository;
    }

}
