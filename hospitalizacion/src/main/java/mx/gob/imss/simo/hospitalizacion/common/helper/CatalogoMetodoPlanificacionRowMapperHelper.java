package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

public class CatalogoMetodoPlanificacionRowMapperHelper extends BaseHelper implements RowMapper<DatosCatalogo> {

    @Override
    public DatosCatalogo mapRow(ResultSet resultSet, int rowId) throws SQLException {

        DatosCatalogo datosCatalogo = new DatosCatalogo();
        datosCatalogo.setClave(resultSet.getString(SQLColumnasConstants.CVE_METODO_ANTICONCEPTIVO));
        datosCatalogo.setDescripcion(resultSet.getString(SQLColumnasConstants.DES_METODO_ANTICONCEPTIVO));
        return datosCatalogo;
    }

}
