package mx.gob.imss.simo.hospitalizacion.common.model;

import lombok.Data;

@Data
public class PeriodoAtencionToco {

    private int cveAtencionPrevia;
    private int cveAtencionActual;
    private int numPeriodoDias;

}
