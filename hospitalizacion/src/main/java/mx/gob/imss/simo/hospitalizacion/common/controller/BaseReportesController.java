package mx.gob.imss.simo.hospitalizacion.common.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionRuntimeException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

public abstract class BaseReportesController extends HospitalizacionCommonController {

    private static final long serialVersionUID = 1L;

    //public static final String CONTENT_TYPE_XLS = "application/vnd.ms-excel";
    public static final String CONTENT_TYPE_XLS = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml";
    public static final String XLS_EXTENSION = ".xlsx";
    protected String jasperReporteName;
    protected String endFilename = null;
    protected static final String REPORT_PATH_JASPER = "/WEB-INF/reportes";
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    public abstract Map<String, Object> getParametersReports();

    /**
     * metodo que obtiene el archivo xls
     * 
     * @return
     */
    public StreamedContent getFileXls() {

        StreamedContent streamedContent = null;
        logger.info(":: Generar reporte de Excel, {" + jasperReporteName + "}");

        try {
            streamedContent = this.getReport(getParametersReports(), jasperReporteName);
        } catch (final Exception genericExc) {
            logger.error(genericExc.getMessage(), genericExc);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, StringUtils.EMPTY,
                    "Hubo un error al generar el reporte, contacte a su administrador.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return streamedContent;
    }

    /**
     * Metodo que obtiene el streamed del reporte
     * 
     * @param params
     * @param jasperReportName
     * @return
     */
    public StreamedContent getReport(Map<String, Object> params, String jasperReportName) {

        final InputStream stream = getInternalReport(params, jasperReportName);
        return new DefaultStreamedContent(stream, CONTENT_TYPE_XLS, jasperReportName.concat(XLS_EXTENSION));
    }

    /**
     * metodo que regresa el InputStream para dibujar el xls
     * 
     * @return
     */
    public InputStream getInternalReport(Map<String, Object> params, String jasperReportName) {

        InputStream stream = null;
        try {

            final JasperPrint print = getJasperReport(params, jasperReportName);
            final ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(print));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(arrayOutputStream));
            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setDetectCellType(true);// Set configuration as you like it!!
            configuration.setCollapseRowSpan(false);
            exporter.setConfiguration(configuration);
            exporter.exportReport();

            stream = new ByteArrayInputStream(arrayOutputStream.toByteArray());

            logger.info(":: Termina Exportar ");
        } catch (JRException e) {
            throw new HospitalizacionRuntimeException(e);
        }
        return stream;
    }

    /**
     * metodo que regresa un JasperPrint a partir de los parametros y el nombre del reporte
     * 
     * @return
     */
    private JasperPrint getJasperReport(Map<String, Object> params, String jasperReportName) {

        Connection connection = null;
        JasperPrint print = null;
        try {
            logger.info(":: Inicia generacion JasperReport, params: {" + params + "}");

            final FacesContext context = FacesContext.getCurrentInstance();
            final ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
            final String jasperPathReport = servletContext
                    .getRealPath(REPORT_PATH_JASPER + FILE_SEPARATOR + jasperReportName + ".jasper");
            print = JasperFillManager.fillReport(jasperPathReport, params, connection);
            logger.info(":: Finaliza generacion JasperReport ");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return print;
    }

    public String getJasperReporteName() {

        return jasperReporteName;
    }

    public void setJasperReporteName(String jasperReporteName) {

        this.jasperReporteName = jasperReporteName;
    }

    public String getEndFilename() {

        return endFilename;
    }

    public void setEndFilename(String endFilename) {

        this.endFilename = endFilename;
    }

}
