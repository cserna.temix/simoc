package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;

public class PacienteRowMapperHelper extends BaseHelper implements RowMapper<Paciente> {

    @Override
    public Paciente mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        Paciente paciente = new Paciente();
        paciente.setCvePaciente(resultSet.getLong(SQLColumnasConstants.CVE_PACIENTE));
        paciente.setCvePacientePadre(resultSet.getLong(SQLColumnasConstants.CVE_PACIENTE_PADRE));
        paciente.setCveIdPersona(resultSet.getLong(SQLColumnasConstants.CVE_ID_PERSONA));
        paciente.setRefNss(resultSet.getString(SQLColumnasConstants.REF_NSS));
        paciente.setRefAgregadoMedico(resultSet.getString(SQLColumnasConstants.REF_AGREGADO_MEDICO));
        paciente.setRefNombre(resultSet.getString(SQLColumnasConstants.REF_NOMBRE));
        paciente.setRefApellidoPaterno(resultSet.getString(SQLColumnasConstants.REF_APELLIDO_PATERNO));
        paciente.setRefApellidoMaterno(resultSet.getString(SQLColumnasConstants.REF_APELLIDO_MATERNO));
        paciente.setFecNacimiento(resultSet.getDate(SQLColumnasConstants.FEC_NACIMIENTO));
        paciente.setNumEdadSemanas(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_SEMANAS));
        paciente.setCveCurp(resultSet.getString(SQLColumnasConstants.CVE_CURP));
        paciente.setCveIdee(resultSet.getString(SQLColumnasConstants.CVE_IDEE));
        paciente.setIndAcceder(resultSet.getBoolean(SQLColumnasConstants.IND_ACCEDER));
        paciente.setIndVigencia(resultSet.getBoolean(SQLColumnasConstants.IND_VIGENCIA));
        paciente.setCveSexo(resultSet.getInt(SQLColumnasConstants.CVE_SEXO));
        paciente.setRefTipoPension(resultSet.getString(SQLColumnasConstants.REF_TIPO_PENSION));
        paciente.setRefRegistroPatronal(resultSet.getString(SQLColumnasConstants.REF_REGISTRO_PATRONAL));
        paciente.setFecActualizacion(resultSet.getDate(SQLColumnasConstants.FEC_ACTUALIZACION));
        paciente.setFecConsulta(resultSet.getDate(SQLColumnasConstants.FEC_CONSULTA));
        paciente.setCveUnidadAdscripcion(resultSet.getString(SQLColumnasConstants.CVE_UNIDAD_ADSCRIPCION));
        paciente.setIndRecienNacido(resultSet.getBoolean(SQLColumnasConstants.IND_RECIEN_NACIDO));
        paciente.setIndDerechohabiente(resultSet.getBoolean(SQLColumnasConstants.IND_DERECHOHABIENTE));
        paciente.setIndUltimoRegistro(resultSet.getBoolean(SQLColumnasConstants.IND_ULTIMO_REGISTRO));
        paciente.setNumEdadAnios(resultSet.getInt(SQLColumnasConstants.NUM_EDAD_ANIOS));
        return paciente;
    }

}
