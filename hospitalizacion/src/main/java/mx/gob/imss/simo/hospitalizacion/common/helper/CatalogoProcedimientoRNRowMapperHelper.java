package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

public class CatalogoProcedimientoRNRowMapperHelper extends BaseHelper implements RowMapper<DatosCatalogo> {

    @Override
    public DatosCatalogo mapRow(ResultSet resultSet, int rowId) throws SQLException {

        DatosCatalogo datosCatalogo = new DatosCatalogo();
        datosCatalogo.setClave(resultSet.getString(SQLColumnasConstants.CVE_PROCEDIMIENTO_RN));
        datosCatalogo.setDescripcion(resultSet.getString(SQLColumnasConstants.CVE_PROCEDIMIENTO_RN)
                + BaseConstants.ESPACIO + resultSet.getString(SQLColumnasConstants.DES_PROCEDIMIENTO_RN));
        return datosCatalogo;
    }

}
