/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.repository.impl;

import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLParteII;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.IngresoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.MovimientoIntraHospitalarioRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.ParteIIRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.PeriodosImssRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.TococirugiaRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.common.repository.BaseJDBCRepository;
import mx.gob.imss.simo.hospitalizacion.reportes.helper.EspecialidadesUnidadRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.helper.FechasPacienteRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.helper.NacLubchencoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.helper.NacidosRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.helper.PartosRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.reportes.repository.ReportesRepository;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.EspecialidadesUnidadVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.FechasPacienteVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.NacLubchencoVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.NacidosVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.PartosVo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitMaternoInfantil;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitMovEnfermo;
import mx.gob.imss.simo.hospitalizacion.reportes.vo.SitParteII;

/**
 * @author softtek
 */
@Repository
public class ReportesRepositoryImpl extends BaseJDBCRepository implements ReportesRepository {

    @Override
    public PeriodosImss obtenerPeriodoImss(Integer anioPeriodo, Integer mesPeriodo) {

        return jdbcTemplate.queryForObject(SQLCatalogosConstants.OBTENER_PERIODO_CIERRE,
                new Object[] { anioPeriodo, mesPeriodo }, new PeriodosImssRowMapperHelper());
    }
    
    @Override
    public Integer obtenerRegistroPeriodoII(String cvePresupuestal, String cvePeriodoIMSS, String cveEspecialidad, Integer cveDivision) {

        return jdbcTemplate.queryForObject(SQLParteII.OBTENER_REGISTROS_PERIODOII,
                new Object[] { cvePresupuestal, cvePeriodoIMSS, cveEspecialidad, cveDivision }, Integer.class);
    }
    
    @Override
    public Integer obtenerRegistroMovEnfermoPeriodo(String cvePresupuestal, String cvePeriodoIMSS) {

        return jdbcTemplate.queryForObject(SQLParteII.OBTENER_REGISTROS_SIT_MOV_ENFERMO_PERIODO,
                new Object[] { cvePresupuestal, cvePeriodoIMSS }, Integer.class);
    }
    
    @Override
    public Integer obtenerRegistroMovEnfermoAcumulado(String cvePresupuestal, String cvePeriodoIMSS) {

        return jdbcTemplate.queryForObject(SQLParteII.OBTENER_REGISTROS_SIT_MOV_ENFERMO_ACUMULADO,
                new Object[] { cvePresupuestal, cvePeriodoIMSS }, Integer.class);
    }
    
    @Override
    public Integer obtenerRegistroMatInfantilPeriodo(String cvePresupuestal, String cvePeriodoIMSS) {

        return jdbcTemplate.queryForObject(SQLParteII.OBTENER_REGISTROS_SIT_MAT_INFANTIL_PERIODO,
                new Object[] { cvePresupuestal, cvePeriodoIMSS }, Integer.class);
    }
    
    @Override
    public Integer obtenerRegistroMatInfantilAcumulado(String cvePresupuestal, String cvePeriodoIMSS) {

        return jdbcTemplate.queryForObject(SQLParteII.OBTENER_REGISTROS_SIT_MAT_INFANTIL_ACUMULADO,
                new Object[] { cvePresupuestal, cvePeriodoIMSS }, Integer.class);
    }

    @Override
    public String periodoMasActualAbierto(String cvePresupuestal) {

        return jdbcTemplate.queryForObject(SQLParteII.PERIODO_MAS_ACTUAL_ABIERTO, new Object[] { cvePresupuestal },
                String.class);
    }

    @Override
    public List<EspecialidadesUnidadVo> especialidades(String cvePresupuestal) {

        return jdbcTemplate.query(SQLParteII.ESPECIALIDADES, new Object[] { cvePresupuestal },
                new EspecialidadesUnidadRowMapperHelper());
    }

    @Override
    public List<EspecialidadesUnidadVo> especialidadIndPed(String cvePresupuestal) {

        return jdbcTemplate.query(SQLParteII.ESPECIALIDADES_PEDIATRICA, new Object[] { cvePresupuestal },
                new EspecialidadesUnidadRowMapperHelper());
    }

    @Override
    public Integer camasPediatria(String cvePresupuestal, String claveEspecialidad) {

        return jdbcTemplate.queryForObject(SQLParteII.OBTENER_CAMAS_PEDIATRICA_ESPECIALIDAD,
                new Object[] { cvePresupuestal, claveEspecialidad }, Integer.class);
    }

    @Override
    public Integer productividadPediatria(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.OBTENER_PROD_ESPECIALIDAD_PEDIATRICA,
                new Object[] { cvePresupuestal, cveEspecialidad, cveEspecialidad, fechaInicio, fechaFin },
                Integer.class);
    }

    @Override
    public PeriodosImss obtenerPeriodoImssXcvePeriodoImss(String cvePeriodoImss) {

        return jdbcTemplate.queryForObject(SQLParteII.PERIODOS_IMSS, new Object[] { cvePeriodoImss },
                new PeriodosImssRowMapperHelper());
    }

    @Override
    public Integer calculaMedicos(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.F_MEDICOS,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaCamas(String cvePresupuestal, String cveEspecialidad) {

        return jdbcTemplate.queryForObject(SQLParteII.F_CAMAS, new Object[] { cvePresupuestal, cveEspecialidad },
                Integer.class);

    }

    @Override
    public Double calculaDiasCamas(String cvePresupuestal, String cveEspecialidad, String fechaInicio,
            String fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.F_DIAS_CAMAS,
                new Object[] { fechaFin, fechaInicio, cvePresupuestal, cveEspecialidad }, Double.class);

    }

    @Override
    public Double calculaDiasRango(Date fechaInicio, Date fechaFin) {

        return jdbcTemplate.queryForObject(
                SQLParteII.F_DIAS_RANGO, new Object[] { fechaFin, fechaInicio, fechaFin, fechaInicio, fechaFin,
                        fechaInicio, fechaFin, fechaInicio, fechaFin, fechaInicio, fechaFin, fechaInicio, },
                Double.class);
    }

    @Override
    public Date ultimoDiaCerrado(String cvePresupuestal) {

        return jdbcTemplate.queryForObject(SQLParteII.ULTIMO_DIA_CERRADO, new Object[] { cvePresupuestal }, Date.class);
    }

    @Override
    public Integer calculaIngresosProgramados(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_PROGRAMADOS,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaIngresosProgramadosAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_PROGRAMADOS_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaIngresosUrgentes(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_URGERNTES,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaIngresosUrgentesAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_URGERNTES_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaIngresosProgramadosOrigen(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_PROGRAMADOS_ORIGEN,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaIngresosProgramadosOrigenAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_PROGRAMADOS_ORIGEN_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaIngresosUrgentesOrigen(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_URGERNTES_ORIGEN,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaIngresosUrgentesOrigenAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_URGENTES_ORIGEN_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaIngresosHospital(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_HOSPITAL,
                new Object[] { fechaInicio, fechaFin, cvePresupuestal, cveEspecialidad }, Integer.class);
    }
    
    @Override
    public Integer calculaIngresosHospitalAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_HOSPITAL_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }


    @Override
    public Integer calculaEgresosCexterna(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_CEXTERNA,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaEgresosAlojamientoCexterna(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_ALOJAMIENTO_CEXTERNA,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaEgresosUnidad(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_UNIDAD,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaEgresosUnidadAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_UNIDAD_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaEgresosHospitalMov(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_HOSPITAL_MOV,
                new Object[] { fechaInicio, fechaFin, cvePresupuestal, cveEspecialidad }, Integer.class);
    }
    
    @Override
    public Integer calculaEgresosHospitalMovAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_HOSPITAL_MOV_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaEgresosHospitalHos(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_HOSPITAL_HOS,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaEgresosHospitalHosAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_HOSPITAL_HOS_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaEgresosOhp(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_OHP,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaEgresosOhpAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio, Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_OHP_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaEgresosDefuncion(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_DEFUNCION,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaEgresosDefuncionAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_DEFUNCION_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaEgresosPostMortem(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_POST_MORTEM,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }
    
    @Override
    public Integer calculaEgresosPostMortemAlojamiento(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_POST_MORTEM_ALOJAMIENTO,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaTotalInterQuirurgica(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.TOTAL_INT_QUIRURGICA,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaTotInterQuirurCirugia(String cvePresupuestal, Date fechaInicio, Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.TOTAL_INT_QUIRURGICA_CIRU_AMB,
                new Object[] { cvePresupuestal, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaInterQuirurgicaProg(String cvePresupuestal, String cveEspecialidad, Date fechaInicio,
            Date fechaFin) {

        return jdbcTemplate.queryForObject(SQLParteII.INT_QUIRURGICA_PROG,
                new Object[] { cvePresupuestal, cveEspecialidad, fechaInicio, fechaFin }, Integer.class);
    }

    @Override
    public Integer calculaIngresosAntAdul(Date fechaInicio, String cvePresupuestal) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_EXIS_FEC_INI_ADUL,
                new Object[] { fechaInicio, cvePresupuestal }, Integer.class);
    }

    @Override
    public Integer calculaIngresosPeriodoAdul(Date fechaInicio, Date fechaFin, String cvePresupuestal) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_PERIODO_ADUL,
                new Object[] { fechaInicio, fechaFin, cvePresupuestal }, Integer.class);
    }

    @Override
    public Integer calculaEgresosPeriodoAdul(Date fechaInicio, Date fechaFin, String cvePresupuestal) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_PERIODO_ADUL,
                new Object[] { fechaInicio, fechaFin, cvePresupuestal }, Integer.class);
    }

    @Override
    public Integer calculaIngresosAntCunero(Date fechaInicio, String cvePresupuestal) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_EXIS_FEC_INI_CUNERO,
                new Object[] { fechaInicio, cvePresupuestal }, Integer.class);
    }

    @Override
    public Integer calculaIngresosPeriodoCunero(Date fechaInicio, Date fechaFin, String cvePresupuestal) {

        return jdbcTemplate.queryForObject(SQLParteII.INGRESOS_PERIODO_CUNERO,
                new Object[] { fechaInicio, fechaFin, cvePresupuestal }, Integer.class);
    }

    @Override
    public Integer calculaEgresosPeriodoCunero(Date fechaInicio, Date fechaFin, String cvePresupuestal) {

        return jdbcTemplate.queryForObject(SQLParteII.EGRESOS_PERIODO_CUNERO,
                new Object[] { fechaInicio, fechaFin, cvePresupuestal }, Integer.class);
    }

    @Override
    public void insertarAcomulado(SitParteII sitParteII) {

        jdbcTemplate.update(SQLParteII.INSERT_PARTE_II_ACOMULADO,
                new Object[] { sitParteII.getCvePeriodoImss(), sitParteII.getCvePresupuestal(),
                        sitParteII.getCveEspecialidad(), sitParteII.getRefUnidadMedica(),
                        sitParteII.getRefEspecialidad(), sitParteII.getCveDivision(), sitParteII.getRefDivision(),
                        sitParteII.getNumMedico(), sitParteII.getNumCama(), sitParteII.getNumIngProgramado(),
                        sitParteII.getNumIngUrgente(), sitParteII.getNumIngHospital(), sitParteII.getNumTotalIngreso(),
                        sitParteII.getNumEgrExterna(), sitParteII.getNumEgrUnidad(), sitParteII.getNumEgrHospital(),
                        sitParteII.getNumEgrOtrohosp(), sitParteII.getNumEgrDefuncion(), sitParteII.getNumTotalEgreso(),
                        sitParteII.getPorcDefuncion(), sitParteII.getNumPostMorten(), sitParteII.getPorcPostMorten(),
                        sitParteII.getNumTotalIq(), sitParteII.getNumIqProgramado(), sitParteII.getNumDiasPaciente(),
                        sitParteII.getPorcOcupacion(), sitParteII.getPorcDiasEstancia(),
                        sitParteII.getPorcIndiceRotacion(), sitParteII.getPorcInidiceSustitucion(),
                        sitParteII.getNumDiasCama(), sitParteII.getNumEjecucion() });

    }

    @Override
    public void insertarPeriodo(SitParteII sitParteII) {

        jdbcTemplate.update(SQLParteII.INSERT_PARTE_II_PERIODO,
                new Object[] { sitParteII.getCvePeriodoImss(), sitParteII.getCvePresupuestal(),
                        sitParteII.getCveEspecialidad(), sitParteII.getRefUnidadMedica(),
                        sitParteII.getRefEspecialidad(), sitParteII.getCveDivision(), sitParteII.getRefDivision(),
                        sitParteII.getNumMedico(), sitParteII.getNumCama(), sitParteII.getNumIngProgramado(),
                        sitParteII.getNumIngUrgente(), sitParteII.getNumIngHospital(), sitParteII.getNumTotalIngreso(),
                        sitParteII.getNumEgrExterna(), sitParteII.getNumEgrUnidad(), sitParteII.getNumEgrHospital(),
                        sitParteII.getNumEgrOtrohosp(), sitParteII.getNumEgrDefuncion(), sitParteII.getNumTotalEgreso(),
                        sitParteII.getPorcDefuncion(), sitParteII.getNumPostMorten(), sitParteII.getPorcPostMorten(),
                        sitParteII.getNumTotalIq(), sitParteII.getNumIqProgramado(), sitParteII.getNumDiasPaciente(),
                        sitParteII.getPorcOcupacion(), sitParteII.getPorcDiasEstancia(),
                        sitParteII.getPorcIndiceRotacion(), sitParteII.getPorcInidiceSustitucion(),
                        sitParteII.getNumDiasCama(), sitParteII.getNumEjecucion() });
        
        logger.info("inserto");

    }

    @Override
    public void insertarEnfermosAcomulado(SitMovEnfermo sitMovEnfermo) {

        jdbcTemplate.update(SQLParteII.INSERT_SIT_MOV_ENFERMO_ACOMULADO,
                new Object[] { sitMovEnfermo.getCvePresupuestal(), sitMovEnfermo.getCvePeriodoImss(),
                        sitMovEnfermo.getNumAnteriorAdulto(), sitMovEnfermo.getNumIngresoAdulto(),
                        sitMovEnfermo.getNumEgresoAdulto(), sitMovEnfermo.getNumActualAdulto(),
                        sitMovEnfermo.getNumAnteriorCunero(), sitMovEnfermo.getNumIngresoCunero(),
                        sitMovEnfermo.getNumEgresoCunero(), sitMovEnfermo.getNumActualCunero(),
                        sitMovEnfermo.getNumEjecucion() });
    }

    @Override
    public void insertarEnfermosPeriodo(SitMovEnfermo sitMovEnfermo) {

        jdbcTemplate.update(SQLParteII.INSERT_SIT_MOV_ENFERMO_PERIODO,
                new Object[] { sitMovEnfermo.getCvePresupuestal(), sitMovEnfermo.getCvePeriodoImss(),
                        sitMovEnfermo.getNumAnteriorAdulto(), sitMovEnfermo.getNumIngresoAdulto(),
                        sitMovEnfermo.getNumEgresoAdulto(), sitMovEnfermo.getNumActualAdulto(),
                        sitMovEnfermo.getNumAnteriorCunero(), sitMovEnfermo.getNumIngresoCunero(),
                        sitMovEnfermo.getNumEgresoCunero(), sitMovEnfermo.getNumActualCunero(),
                        sitMovEnfermo.getNumEjecucion() });
    }

    @Override
    public List<Tococirugia> tocoCirugias(Date fechaInicio, Date fechaFin, String cvePresupuestal) {

        return jdbcTemplate.query(SQLParteII.LISTA_TOCO_CIRUGIA,
                new Object[] { fechaInicio, fechaFin, cvePresupuestal }, new TococirugiaRowMapperHelper());
    }

    @Override
    public PartosVo contadorPartos(Integer cveTocoCirugia) {

        return jdbcTemplate.queryForObject(SQLParteII.CONTAR_PARTOS,
                new Object[] { cveTocoCirugia, cveTocoCirugia, cveTocoCirugia }, new PartosRowMapperHelper());
    }

    @Override
    public NacLubchencoVo contadorPreTerminoPost(Integer cveTocoCirugia) {

        return jdbcTemplate.queryForObject(SQLParteII.PRE_TERMINO_POST,
                new Object[] { cveTocoCirugia, cveTocoCirugia, cveTocoCirugia, cveTocoCirugia, cveTocoCirugia,
                        cveTocoCirugia, cveTocoCirugia, cveTocoCirugia, cveTocoCirugia },
                new NacLubchencoRowMapperHelper());
    }

    @Override
    public NacidosVo contadorNacidos(Integer cveTocoCirugia) {

        return jdbcTemplate.queryForObject(SQLParteII.CONTAR_NACIDOS,
                new Object[] { cveTocoCirugia, cveTocoCirugia, cveTocoCirugia, cveTocoCirugia },
                new NacidosRowMapperHelper());
    }

    @Override
    public void insertarMaternoInfantilAcomulado(SitMaternoInfantil sitMaternoInfantil) {

        jdbcTemplate.update(SQLParteII.INSERT_SIT_MATERNO_INFANTIL_ACOMULADO,
                new Object[] { sitMaternoInfantil.getCvePresupuestal(), sitMaternoInfantil.getCvePeriodoImss(),
                        sitMaternoInfantil.getNumPartoNormal(), sitMaternoInfantil.getNumPartoVaginal(),
                        sitMaternoInfantil.getNumPartoAbdominal(), sitMaternoInfantil.getNumTotalParto(),
                        sitMaternoInfantil.getNumPretBajo(), sitMaternoInfantil.getNumPretNormal(),
                        sitMaternoInfantil.getNumPretAlto(), sitMaternoInfantil.getNumTerminoBajo(),
                        sitMaternoInfantil.getNumTerminoNormal(), sitMaternoInfantil.getNumTerminoAlto(),
                        sitMaternoInfantil.getNumPostBajo(), sitMaternoInfantil.getNumPostNormal(),
                        sitMaternoInfantil.getNumPostAlto(), sitMaternoInfantil.getNumDefuncion(),
                        sitMaternoInfantil.getNumMortinato(), sitMaternoInfantil.getNumAborto(),
                        sitMaternoInfantil.getNumNacidoVivo(), sitMaternoInfantil.getNumEjecucion() });

    }

    @Override
    public void insertarMaternoInfantilPeriodo(SitMaternoInfantil sitMaternoInfantil) {

        jdbcTemplate.update(SQLParteII.INSERT_SIT_MATERNO_INFANTIL_PERIODO,
                new Object[] { sitMaternoInfantil.getCvePresupuestal(), sitMaternoInfantil.getCvePeriodoImss(),
                        sitMaternoInfantil.getNumPartoNormal(), sitMaternoInfantil.getNumPartoVaginal(),
                        sitMaternoInfantil.getNumPartoAbdominal(), sitMaternoInfantil.getNumTotalParto(),
                        sitMaternoInfantil.getNumPretBajo(), sitMaternoInfantil.getNumPretNormal(),
                        sitMaternoInfantil.getNumPretAlto(), sitMaternoInfantil.getNumTerminoBajo(),
                        sitMaternoInfantil.getNumTerminoNormal(), sitMaternoInfantil.getNumTerminoAlto(),
                        sitMaternoInfantil.getNumPostBajo(), sitMaternoInfantil.getNumPostNormal(),
                        sitMaternoInfantil.getNumPostAlto(), sitMaternoInfantil.getNumDefuncion(),
                        sitMaternoInfantil.getNumMortinato(), sitMaternoInfantil.getNumAborto(),
                        sitMaternoInfantil.getNumNacidoVivo(), sitMaternoInfantil.getNumEjecucion() });

    }

    @Override
    public void insertarInicioBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso,
            Integer cveEstadoProceso) {

        jdbcTemplate.update(SQLParteII.INSERT_SIT_BITACORA_PROCESO,
                new Object[] { cvePresupuestal, cvePeriodoImss, cveProceso, cveEstadoProceso });

    }

    @Override
    public void updateBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso,
            Integer cveEstadoProceso, Boolean reproceso) {
        String cadena = "";
        if (reproceso) {
            cadena = " , FEC_INICIO = SYSDATE ";
        }
        String query = SQLParteII.UPDATE_SIT_BITACORA_PROCESO.concat(cadena)
                .concat(SQLParteII.UPDATE_SIT_BITACORA_PROCESO_FIN);

        jdbcTemplate.update(query, new Object[] { cveEstadoProceso, cvePresupuestal, cvePeriodoImss, cveProceso });

    }

    @Override
    public void finBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso,
            Integer cveEstadoProceso) {

        jdbcTemplate.update(SQLParteII.UPDATE_FIN_SIT_BITACORA_PROCESO,
                new Object[] { cveEstadoProceso, cvePresupuestal, cvePeriodoImss, cveProceso });

    }

    @Override
    public void deleteReporteDiario(String cvePresupuestal, String cvePeriodoImss) {

        jdbcTemplate.update(SQLParteII.DELETE_SIT_MAT_INFANTIL_PERIODO,
                new Object[] { cvePresupuestal, cvePeriodoImss });

        jdbcTemplate.update(SQLParteII.DELETE_SIT_MOV_ENFERMO_PERIODO,
                new Object[] { cvePresupuestal, cvePeriodoImss });

        jdbcTemplate.update(SQLParteII.DELETE_SIT_PARTEII_PERIODO, new Object[] { cvePresupuestal, cvePeriodoImss });
    }

    @Override
    public void deleteReporteAcumulado(String cvePresupuestal, String cvePeriodoImss) {

        jdbcTemplate.update(SQLParteII.DELETE_SIT_MAT_INFANTIL_ACUMULADO,
                new Object[] { cvePresupuestal, cvePeriodoImss });

        jdbcTemplate.update(SQLParteII.DELETE_SIT_MOV_ENFERMO_ACUMULADO,
                new Object[] { cvePresupuestal, cvePeriodoImss });

        jdbcTemplate.update(SQLParteII.DELETE_SIT_PARTEII_ACUMULADO, new Object[] { cvePresupuestal, cvePeriodoImss });
    }

    @Override
    public List<MovimientoIntraHospitalario> listaEspeMovimiento(Date fechaInicio, Date fechaFin,
            String cvePresupuestal, String cveEspecialidad) {

        return jdbcTemplate.query(SQLParteII.lIST_DIAS_PACIENTE_MOVIMIENTOS,
                new Object[] { fechaInicio, fechaFin, cvePresupuestal, cveEspecialidad },
                new MovimientoIntraHospitalarioRowMapperHelper());
    }

    @Override
    public List<Ingreso> listaEspeIngreso(String cvePresupuestal, String cveEspecialidad, Date fecha_inicio, Date fecha_fin) {

        return jdbcTemplate.query(SQLParteII.LIST_DIAS_PACIENTE_INGRESO_FECHA,
                new Object[] { cvePresupuestal, cveEspecialidad, fecha_fin, fecha_inicio }, new IngresoRowMapperHelper());
    }

    @Override
    public Integer diasPacienteMov(String cveEspecialidad, long cveIngreso) {

        List<FechasPacienteVo> listFechasPacienteVo = jdbcTemplate.query(SQLParteII.DIAS_MOV,
                new Object[] { cveEspecialidad, cveIngreso }, new FechasPacienteRowMapperHelper());

        if (!listFechasPacienteVo.isEmpty()) {
            FechasPacienteVo fechasPacienteVo = listFechasPacienteVo.get(0);
            return jdbcTemplate.queryForObject(SQLParteII.F_DIAS_RANGO,
                    new Object[] { fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio() },
                    Integer.class);

        } else {
            return 0;
        }
    }

    @Override
    public Integer diasPacienteIngresoMov(Date fechaInicio, Date fechaFin, String cvePresupuestal,
            String cveEspecialidad, long cveIngreso) {

        List<FechasPacienteVo> listFechasPacienteVo = jdbcTemplate.query(
                SQLParteII.DIAS_ING_MOV, new Object[] { fechaInicio, fechaInicio, fechaInicio, fechaFin,
                        cvePresupuestal, fechaInicio, fechaFin, cvePresupuestal, cveIngreso, cveEspecialidad },
                new FechasPacienteRowMapperHelper());

        if (!listFechasPacienteVo.isEmpty()) {
            FechasPacienteVo fechasPacienteVo = listFechasPacienteVo.get(0);
            return jdbcTemplate.queryForObject(SQLParteII.F_DIAS_RANGO,
                    new Object[] { fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio() },
                    Integer.class);

        } else {
            return 0;
        }
    }

    @Override
    public Integer diasPacienteIngresoEgreso(Date fechaInicio, Date fechaFin, String cvePresupuestal,
            String cveEspecialidad, long cveIngreso) {

        List<FechasPacienteVo> listFechasPacienteVo = jdbcTemplate.query(SQLParteII.DIAS_ING_EGRESO, new Object[] {
                fechaFin, fechaFin, fechaFin, cveEspecialidad, fechaInicio, fechaFin, cvePresupuestal, cveIngreso },
                new FechasPacienteRowMapperHelper());

        if (!listFechasPacienteVo.isEmpty()) {
            FechasPacienteVo fechasPacienteVo = listFechasPacienteVo.get(0);
            return jdbcTemplate.queryForObject(SQLParteII.F_DIAS_RANGO,
                    new Object[] { fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio() },
                    Integer.class);

        } else {
            return 0;
        }
    }

    @Override
    public Integer diasPacienteIngresoEgresoSinFec(Date fechaInicio, Date fechaFin, String cvePresupuestal,
            String cveEspecialidad, long cveIngreso) {

        List<FechasPacienteVo> listFechasPacienteVo = jdbcTemplate.query(SQLParteII.DIAS_ING_EGRESO_SIN_FEC,
                new Object[] { fechaInicio, fechaInicio, fechaFin, fechaFin, fechaFin, cveEspecialidad, cvePresupuestal,
                        cveIngreso },
                new FechasPacienteRowMapperHelper());

        if (!listFechasPacienteVo.isEmpty()) {
            FechasPacienteVo fechasPacienteVo = listFechasPacienteVo.get(0);
            return jdbcTemplate.queryForObject(SQLParteII.F_DIAS_RANGO,
                    new Object[] { fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio() },
                    Integer.class);

        } else {
            return 0;
        }
    }

    @Override
    public Integer diasPacienteMovEgreso(Date fechaInicio, Date fechaFin, String cvePresupuestal,
            String cveEspecialidad, long cveIngreso) {

        List<FechasPacienteVo> listFechasPacienteVo = jdbcTemplate.query(
                SQLParteII.DIAS_MOV_EGRESO, new Object[] { fechaFin, fechaFin, fechaFin, cvePresupuestal, fechaInicio,
                        fechaFin, cveEspecialidad, fechaInicio, fechaFin, cvePresupuestal, cveIngreso },
                new FechasPacienteRowMapperHelper());

        if (!listFechasPacienteVo.isEmpty()) {
            FechasPacienteVo fechasPacienteVo = listFechasPacienteVo.get(0);
            return jdbcTemplate.queryForObject(SQLParteII.F_DIAS_RANGO,
                    new Object[] { fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio(),
                            fechasPacienteVo.getFechaFin(), fechasPacienteVo.getFechaInicio() },
                    Integer.class);

        } else {
            return 0;
        }
    }

    @Override
    public Date obtenerUltimaFechaReportePacientes(Integer tipoReporte, String cvePresupuestal)
            throws HospitalizacionException {

        try {
            return jdbcTemplate.queryForObject(SQLConstants.BUSCAR_ULTIMA_FECHA_REPORTE_PACIENTES,
                    new Object[] { tipoReporte, cvePresupuestal }, Date.class);
        } catch (Exception e) {
            logger.error("Error al consultar la ultima fecha del reporte de pacientes generado", e);
            return null;
        }
    }

    @Override
    public List<String> obtenerClavesPresupuestales() throws HospitalizacionException {

        try {
            return jdbcTemplate.queryForList(SQLConstants.BUSCAR_CVE_PRESUPUESTAL_REP_PARTEII_PERIODO, String.class);
        } catch (Exception e) {
            logger.error("Error al obtener las claves presupuestales", e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    @Override
    public void borrarRegistros(String cvePresupuestal, String periodo) throws HospitalizacionException {
        logger.info(
                "Se borran registros de SIT_MAT_INFANTIL_PERIODO, SIT_MOV_ENFERMO_PERIODO, SIT_PARTEII_PERIODO....");
        try {
            jdbcTemplate.update(SQLConstants.BORRAR_MAT_INFANTIL_PERIODO, new Object[] { cvePresupuestal, periodo });
            jdbcTemplate.update(SQLConstants.BORRAR_MOV_ENFERMO_PERIODO, new Object[] { cvePresupuestal, periodo });
            jdbcTemplate.update(SQLConstants.BORRAR_PARTEII_PERIODO, new Object[] { cvePresupuestal, periodo });
            logger.info("borr�");
        } catch (Exception e) {
            logger.error(
                    "Error al borrar datos de las tablas SIT_MAT_INFANTIL_PERIODO, SIT_MOV_ENFERMO_PERIODO, SIT_PARTEII_PERIODO",
                    e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    @Override
    public List<PeriodoOperacion> obtenerPeriodosRepParteII() throws HospitalizacionException {

        try {
            return jdbcTemplate.query(SQLConstants.BUSCAR_PERIODOS_PROCESO_REPORTE_PARTEII,

                    (ResultSet resultSet, int arg1) -> {
                        PeriodoOperacion periodo = new PeriodoOperacion();
                        periodo.setClavePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
                        periodo.setClavePeriodoIMSS(resultSet.getString(SQLColumnasConstants.CVE_PERIODO_IMSS));
                        return periodo;
                    });
        } catch (Exception e) {
            logger.error("Error al obtener las periodos para el proceso de reporte parte II", e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    @Override
    public List<String> obtenerClavesPresupuestalesParaCierre() throws HospitalizacionException {

        try {
            return jdbcTemplate.queryForList(SQLConstants.BUSCAR_PROCESO_PENDIENTE_CIERRE_MES, String.class);
        } catch (Exception e) {
            logger.error("Error al obtener las claves presupuestales de bitacora", e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    @Override
    public Date obtenerUltimoPeriodoParteII(String cvePresupuestal) throws HospitalizacionException {

        try {
            return jdbcTemplate.queryForObject(SQLConstants.BUSCAR_ULTIMO_PERIODO_PARTEII,
                    new Object[] { cvePresupuestal }, Date.class);
        } catch (Exception e) {
            logger.error("Error al obtener el periodo", e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    @Override
    public Integer obtenerConteoDatosParteIIPeriodo(String cvePresupuestal, String periodo)
            throws HospitalizacionException {

        try {
            return jdbcTemplate.queryForObject(SQLConstants.BUSCAR_REGISTROS_PERIODOS_PARTEII,
                    new Object[] { cvePresupuestal, periodo }, Integer.class);
        } catch (Exception e) {
            logger.error("Error al obtener el conteo de registros en periodo", e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    @Override
    public Integer buscarBitacora(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso)
            throws HospitalizacionException {

        try {
            return jdbcTemplate.queryForObject(SQLParteII.SELECT_SIT_BITACORA_PROCESO,
                    new Object[] { cvePresupuestal, cvePeriodoImss, cveProceso }, Integer.class);
        } catch (Exception e) {
            logger.error("Error al obtener el SIT_BITACORA_PROCESO ", e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    @Override
    public void updateBitacoraError(String cvePresupuestal, String cvePeriodoImss, Integer cveProceso, String error)
            throws HospitalizacionException {

        try {
            jdbcTemplate.update(SQLParteII.UPDATE_ERROR_SIT_BITACORA_PROCESO,
                    new Object[] { 3, error, cvePresupuestal, cvePeriodoImss, cveProceso });
        } catch (Exception e) {
            logger.error("Error al obtener el SIT_BITACORA_PROCESO ", e);
            throw new HospitalizacionException(e.getMessage());
        }

    }

    @Override
    public String obtenerPeriodoParaCierre(String cvePresupuestal) throws HospitalizacionException {

        try {
            return jdbcTemplate.queryForObject(SQLParteII.PERIODOS_IMSS_CERRADO, new Object[] { cvePresupuestal },
                    String.class);
        } catch (Exception e) {
            logger.error("Error al obtener el PERIODOS_IMSS_CERRADO ", e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

    @Override
    public String obtenerPeriodoFinalizado(String cvePresupuestal) throws HospitalizacionException {

        try {
            return jdbcTemplate.queryForObject(SQLParteII.PERIODOS_IMSS_FINALIZADO, new Object[] { cvePresupuestal },
                    String.class);
        } catch (Exception e) {
            logger.error("Error al obtener el PERIODOS_IMSS_FINALIZADO ", e);
            throw new HospitalizacionException(e.getMessage());
        }
    }

}
