package mx.gob.imss.simo.hospitalizacion.intervencionesqx.rules;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IntervencionesQuirurgicasConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIntervenciones;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.SalaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;

@Component
public class CapturarIntervencionesQuirurgicasRules extends HospitalizacionCommonRules {

    public void validarNumeroQuirofano(List<SalaUnidad> sala, DatosHospitalizacion datosHospitalizacion)
            throws HospitalizacionException {

        if (null == datosHospitalizacion.getDatosIntervenciones().getNumeroQuirofano()
                || datosHospitalizacion.getDatosIntervenciones().getNumeroQuirofano().isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(IntervencionesQuirurgicasConstants.NUMERO_QUIROFANO));
        }
        if (null == sala || sala.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_050));
        }

    }

    public void validarTipoIntervencion(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        if (null == datosHospitalizacion.getDatosIntervenciones().getTipoIntervencion()
                || datosHospitalizacion.getDatosIntervenciones().getTipoIntervencion().isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_TIPO_INTERVENCION));
        }

    }

    public void validarHoraEntradaSala(Date fechaIngreso, Date fechaEntradaSala) throws HospitalizacionException {

        if (validarFecha(fechaIngreso, fechaEntradaSala)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_126));
        }

    }

    public boolean validarHoraInicio(Date fechaEntradaSala, Date fechaIncio){
    	
    	return validarFecha(fechaEntradaSala, fechaIncio);
        

    }

    public boolean validarHoraTermino(Date fechaIncio, Date fechaTermino){
    	return validarFecha(fechaIncio, fechaTermino);
    }

    public boolean validarHoraSalaSalida(Date fechaTermino, Date fechaSalida) {

    	return validarFecha(fechaTermino, fechaSalida);

    }

    public void validarTipoAnestesia(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        if (null == datosHospitalizacion.getDatosIntervenciones().getTipoAnestesia()
                || datosHospitalizacion.getDatosIntervenciones().getTipoAnestesia().isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_TIPO_ANESTESIA));
        }

    }

    public void validarProcedimientoQX(String claveProcedimiento) {

    }

    public void validarMetodoPlanificacionFamiliar(String metodoPlanificacion) {

    }

    public void validarCantidadPlanificacion(String cantidadPlanificacion) {

    }

    public void validarFechaProgramacion(Date fechaProgramada, Date fechaIntervencion) throws HospitalizacionException {

        if (!fechaProgramada.before(fechaIntervencion)
                || getHospitalizacionCommonHelper().converDateToString(fechaProgramada)
                        .equals(getHospitalizacionCommonHelper().converDateToString(fechaIntervencion))) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_054));
        }
    }

    public boolean requiereAbrirModal(String tipoIntervencion) {

        if (IntervencionesQuirurgicasConstants.TIPO_INTERVENCION_PROGRAMADA.equals(tipoIntervencion)) {
            return true;
        }
        return false;
    }

    public void validarMaximoRegistrosPermitidos(Parametro parametro, DatosIntervenciones datosIntervenciones)
            throws HospitalizacionException {

        if (null == datosIntervenciones.getNumeroProcedimientos()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_PROCEDIMIENTO));
        }

        if (null != parametro) {
            if (Integer.parseInt(parametro.getReferenciaParametro()) < datosIntervenciones.getNumeroProcedimientos()) {
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_051));
            }
        }
    }

    public void validarEspecialidadParaIQX(Especialidad especialidad) throws HospitalizacionException {

        if (null == especialidad || !especialidad.isIndQx()) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_150);
        }
    }

    public void validarHoraIQ(String hora, String texto) throws HospitalizacionException {

        if (hora == null || hora.isEmpty() || hora.equals(BaseConstants.HORA_VACIA)
                || hora.contains(CapturarIngresosConstants.MASCARA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A), getArray(texto));
        }

        int hr = Integer.parseInt(hora.substring(0, 2));
        int min = Integer.parseInt(hora.substring(3));
        if ((hr < 0 || hr > CapturarIngresosConstants.HORA_MAXIMA)
                || (min < 0 || min > CapturarIngresosConstants.MINUTO_MAXIMO)) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_031);
        }

    }

    public boolean sePuedeActivarGuardar(Boolean[] requeridos) {

        for (boolean req : requeridos) {
            if (!req) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public void validarProcedimientoModal(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        if (null == datosHospitalizacion.getDatosIntervenciones().getClaveProcedimiento()
                || datosHospitalizacion.getDatosIntervenciones().getClaveProcedimiento().isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(IntervencionesQuirurgicasConstants.CLAVE_PROCEDIMIENTO));
        }

    }

    public void validarRequeridoFechaProgramacion(String fecha) throws HospitalizacionException {

        if (fecha == null || fecha.isEmpty() || fecha.equals(BaseConstants.FECHA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(IntervencionesQuirurgicasConstants.REQUERIDO_FECHA_IQX));
        }

    }

    public void validarRequeridoComboEspecialidad(String cadena) throws HospitalizacionException {

        if (cadena == null || cadena.isEmpty() || cadena.equals(BaseConstants.CADENA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_ESPECIALIDAD));
        }
    }

    public void validarRequeridoComboMetodo(String cadena) throws HospitalizacionException {

        if (cadena == null || cadena.isEmpty() || cadena.equals(BaseConstants.CADENA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_METODO_PLANIFICACION));
        }
    }

    public void validarFechaIngreso(Date fechaIntervencion, Date fechaIngreso) throws HospitalizacionException {

        if (getHospitalizacionCommonHelper().converDateToString(fechaIntervencion)
                .equals(getHospitalizacionCommonHelper().converDateToString(fechaIngreso))) {
            return;
        }
        if (fechaIntervencion.before(fechaIngreso)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_049));
        }
    }

    public void validarFechaIngresoIQX(Date fechaIntervencion, String fechaIngreso) throws HospitalizacionException {

        if (!getHospitalizacionCommonHelper().converDateToString(fechaIntervencion).equals(fechaIngreso)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_802));
        }

    }

}
