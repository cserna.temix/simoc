package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ConsultaCountRowMapperHelper extends BaseHelper implements RowMapper<Long> {

    @Override
    public Long mapRow(ResultSet resultSet, int rowId) throws SQLException {

        return resultSet.getLong(1);
    }

}
