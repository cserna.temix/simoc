/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.vo;

import java.util.Date;

/**
 * @author francisco.rodriguez
 *
 */
public class FechasPacienteVo {

	public Date fechaInicio;
	public Date fechaFin;
	
	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}
	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	/**
	 * @return the fechaFin
	 */
	public Date getFechaFin() {
		return fechaFin;
	}
	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
}
