package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;

public class PeriodosImssRowMapperHelper extends BaseHelper implements RowMapper<PeriodosImss> {

    @Override
    public PeriodosImss mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        PeriodosImss periodo = new PeriodosImss();
        periodo.setClavePeriodo(resultSet.getString(SQLColumnasConstants.CVE_PERIODO_IMSS));
        periodo.setNumeroAnioPeriodo(resultSet.getString(SQLColumnasConstants.NUM_ANIO_PERIODO));
        periodo.setDescripcionMesPeriodo(resultSet.getString(SQLColumnasConstants.DES_MES_PERIODO));
        periodo.setFechaInicial(resultSet.getDate(SQLColumnasConstants.FEC_INICIAL));
        periodo.setFechaFinal(resultSet.getDate(SQLColumnasConstants.FEC_FINAL));
        periodo.setIndicadorVigente(resultSet.getBoolean(SQLColumnasConstants.IND_VIGENTE));
        periodo.setFechaBaja(validarFechaNula(resultSet, SQLColumnasConstants.FEC_BAJA));
        return periodo;
    }

}
