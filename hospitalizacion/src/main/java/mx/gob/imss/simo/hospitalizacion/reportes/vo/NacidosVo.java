/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.vo;

/**
 * @author francisco.rodriguez
 *
 */
public class NacidosVo{

	public Integer defuncion;
	public Integer mortinato;
	public Integer aborto;
	public Integer vivo;

	/**
	 * @return the defuncion
	 */
	public Integer getDefuncion() {
		return defuncion;
	}

	/**
	 * @param defuncion
	 *            the defuncion to set
	 */
	public void setDefuncion(Integer defuncion) {
		this.defuncion = defuncion;
	}

	/**
	 * @return the mortinato
	 */
	public Integer getMortinato() {
		return mortinato;
	}

	/**
	 * @param mortinato
	 *            the mortinato to set
	 */
	public void setMortinato(Integer mortinato) {
		this.mortinato = mortinato;
	}

	/**
	 * @return the aborto
	 */
	public Integer getAborto() {
		return aborto;
	}

	/**
	 * @param aborto
	 *            the aborto to set
	 */
	public void setAborto(Integer aborto) {
		this.aborto = aborto;
	}

	/**
	 * @return the vivo
	 */
	public Integer getVivo() {
		return vivo;
	}

	/**
	 * @param vivo
	 *            the vivo to set
	 */
	public void setVivo(Integer vivo) {
		this.vivo = vivo;
	}

}
