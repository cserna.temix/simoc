package mx.gob.imss.simo.hospitalizacion.intervencionesqx.services.impl;

import java.util.Date;
import java.util.List;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IntervencionesQuirurgicasConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIntervenciones;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.IntervencionesQuirurgicas;
import mx.gob.imss.simo.hospitalizacion.common.model.MetodoAnticonceptivo;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimientos;
import mx.gob.imss.simo.hospitalizacion.common.model.SalaUnidad;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.helper.CapturarIntervencionesQuirurgicasHelper;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.repository.CapturarIntervencionesQuirurgicasRepository;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.rules.CapturarIntervencionesQuirurgicasRules;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.services.CapturarIntervencionesQuirurgicasServices;
import mx.gob.imss.simo.model.DatosUsuario;

@Service("capturarIntervencionesQuirurgicasServices")
public class CapturarIntervencionesQuirurgicasServicesImpl extends HospitalizacionCommonServicesImpl
        implements CapturarIntervencionesQuirurgicasServices {

    @Autowired
    private CapturarIntervencionesQuirurgicasHelper capturarIntervencionesQuirurgicasHelper;
    @Autowired
    private CapturarIntervencionesQuirurgicasRules capturarIntervencionesQuirurgicasRules;
    @Autowired
    private CapturarIntervencionesQuirurgicasRepository capturarIntervencionesQuirurgicasRepository;
    @Autowired
    private HospitalizacionCommonRules hospitalizacionCommonRules;

    public void validarFecha(String fecha, String clavePresupuestal) throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarFormatoFecha(fecha);
        super.validarReporteRelacionPacientesGenerado(clavePresupuestal, fecha);
        capturarIntervencionesQuirurgicasRules.validarFechaActual(fecha);
        super.validarFechaEnPeriodoActualActivo(fecha,
                capturarIntervencionesQuirurgicasHelper.convertStringToDate(fecha), clavePresupuestal,
                TipoCapturaEnum.HOSPITALIZACION.getClave());
        super.validarPeriodoAnteriorCerrado(clavePresupuestal, fecha, TipoCapturaEnum.HOSPITALIZACION.getClave());

    }

    public void validaFechaIngresoIQX(DatosPaciente ingresoPaciente, String fechaIQX) throws HospitalizacionException {

        Ingreso ingreso = capturarIntervencionesQuirurgicasRepository.buscarIngresoPorDatosPaciente(ingresoPaciente);
        if (ingreso.getCveEspecialidadIngreso().equals(BaseConstants.ESPECIALIDAD_CIRUGIA_AMBULATORIA)) {
            capturarIntervencionesQuirurgicasRules.validarFechaIngresoIQX(ingreso.getFecIngreso(), fechaIQX);
        }
    }

    public void validarTotalProcedimientos(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        Parametro parametro = capturarIntervencionesQuirurgicasRepository
                .obtenerParamtro(IntervencionesQuirurgicasConstants.PARAMETRO_MAXIMO_REGISTROS_PROCEDIMIENTOS);
        capturarIntervencionesQuirurgicasRules.validarMaximoRegistrosPermitidos(parametro,
                datosHospitalizacion.getDatosIntervenciones());

    }

    public void validarNumeroQuirofano(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException {

        List<SalaUnidad> sala = capturarIntervencionesQuirurgicasRepository.buscarSalasUnidad(
                datosUsuario.getCvePresupuestal(), datosHospitalizacion.getDatosIntervenciones().getNumeroQuirofano());
        capturarIntervencionesQuirurgicasRules.validarNumeroQuirofano(sala, datosHospitalizacion);

    }

    public void validarNSS(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarNss(datosHospitalizacion.getDatosPaciente().getNss());

    }

    public DatosCatalogo elementoSelecionado(SelectEvent evento, List<String> listaTipoEgreso,
            AutoComplete tipoEgresoComplete, List<DatosCatalogo> catalogoTipoEgreso) {

        return capturarIntervencionesQuirurgicasHelper.elementoSelecionado(evento, listaTipoEgreso, tipoEgresoComplete,
                catalogoTipoEgreso);
    }

    public void validarTipoIntervencion(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarTipoIntervencion(datosHospitalizacion);

    }

    public void validarHoraEntradaSala(DatosHospitalizacion datosHospitalizacion, String fechaIQX)
            throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarHoraIQ(
                datosHospitalizacion.getDatosIntervenciones().getHoraEntrada(),
                IntervencionesQuirurgicasConstants.HORA_ENTRADA_SALA);
        Ingreso ingreso = capturarIntervencionesQuirurgicasRepository
                .buscarIngresoPorDatosPaciente(datosHospitalizacion.getDatosPaciente());

        capturarIntervencionesQuirurgicasRules.validarHoraEntradaSala(ingreso.getFecIngreso(),
                capturarIntervencionesQuirurgicasHelper.obtenerFechaCompleta(fechaIQX,
                        datosHospitalizacion.getDatosIntervenciones().getHoraEntrada()));

    }

    public void validarHoraInicio(DatosHospitalizacion datosHospitalizacion, String fechaIQX)
            throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarHoraIQ(
                datosHospitalizacion.getDatosIntervenciones().getHoraInicio(),
                IntervencionesQuirurgicasConstants.HORA_INICIO);

     }

    public void validarHoraTermino(DatosHospitalizacion datosHospitalizacion, String fechaIQX)
            throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarHoraIQ(
                datosHospitalizacion.getDatosIntervenciones().getHoraTermino(),
                IntervencionesQuirurgicasConstants.HORA_TERMINO);      

    }

    public void validarHoraSalaSalida(DatosHospitalizacion datosHospitalizacion, String fechaIQX)
            throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarHoraIQ(
                datosHospitalizacion.getDatosIntervenciones().getHoraSalida(),
                IntervencionesQuirurgicasConstants.HORA_SALIDA_SALA);

    }

    public void validarTipoAnestesia(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarTipoAnestesia(datosHospitalizacion);

    }

    public List<Procedimientos> iniciarTotalProcedimientos(int numeroProcedimientos) throws HospitalizacionException {

        return capturarIntervencionesQuirurgicasHelper.inicializarTotalProcedimientos(numeroProcedimientos);
    }

    public String validarEspecialidad(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules
                .validarRequeridoComboEspecialidad(datosHospitalizacion.getDatosIntervenciones().getEspecialidad());

        Especialidad especialidad = getHospitalizacionCommonRepository()
                .buscarEspecialidad(datosHospitalizacion.getDatosIntervenciones().getEspecialidad());
        capturarIntervencionesQuirurgicasRules.validarEspecialidadParaIQX(especialidad);
        getHospitalizacionCommonRules().validarSexoEspecialidad(especialidad.getCveSexo(),
                datosHospitalizacion.getDatosPaciente().getSexo());
        getHospitalizacionCommonRules().validarEdadPermitida(especialidad,
                datosHospitalizacion.getDatosPaciente().getEdadAnios(),
                datosHospitalizacion.getDatosPaciente().getEdadSemanas());
        return String.valueOf(especialidad.getCveDivision());

    }

    public DatosIntervenciones obtenerDatosAnteriores(DatosPaciente datosPaciente) {

        return null;
    }

    public void validarProcedimientoQX(DatosHospitalizacion datosHospitalizacion) {

        capturarIntervencionesQuirurgicasRules
                .validarProcedimientoQX(datosHospitalizacion.getDatosIntervenciones().getClaveProcedimiento());
    }

    public void validarCirujano(DatosHospitalizacion datosHospitalizacion) throws HospitalizacionException {

        super.validarMatricula(datosHospitalizacion.getDatosIntervenciones().getClaveCirujano());
    }

    public void validarPlanificacionFamiliar(DatosHospitalizacion datosHospitalizacion)
            throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarMetodoPlanificacionFamiliar(
                datosHospitalizacion.getDatosIntervenciones().getMetodoPlanificacion());

    }

    @Override
    public boolean validarMetodoPlanificacion(String datosPlanificacionFamiliar, String agregadoMedico, Date fecCaptura)
            throws HospitalizacionException {

        boolean requiereCantidad = Boolean.FALSE;

        capturarIntervencionesQuirurgicasRules.validarRequeridoComboMetodo(datosPlanificacionFamiliar);
        MetodoAnticonceptivo metodo = capturarIntervencionesQuirurgicasRepository
                .buscarPlanificacionById(datosPlanificacionFamiliar, TipoFormatoEnum.INTERVECION_QUIRURGICA.getClave());
        super.validarPlanificacionSexo(metodo, agregadoMedico);
        super.validarPlanificacionEdadFec(metodo, agregadoMedico, fecCaptura);
        if (null != metodo.getCantidad() && metodo.getCantidad() > 0) {
            requiereCantidad = Boolean.TRUE;
        }
        return requiereCantidad;
    }
    
    

    @Override
    public void validarCantidadPlanificacion(String datosPlanificacionFamiliar) throws HospitalizacionException {

        if (datosPlanificacionFamiliar == null || datosPlanificacionFamiliar.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_CANTIDAD));
        }
    }

    @Override
    public void validarFechaProgramacion(DatosHospitalizacion datosHospitalizacion, String fecha)
            throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarRequeridoFechaProgramacion(
                datosHospitalizacion.getDatosIntervenciones().getModalFechaIntervencion());
        capturarIntervencionesQuirurgicasRules
                .validarFormatoFecha(datosHospitalizacion.getDatosIntervenciones().getModalFechaIntervencion());
        capturarIntervencionesQuirurgicasRules
                .validarFechaActual(datosHospitalizacion.getDatosIntervenciones().getModalFechaIntervencion());
        Date fechaProgramada = capturarIntervencionesQuirurgicasHelper
                .convertStringToDate(datosHospitalizacion.getDatosIntervenciones().getModalFechaIntervencion());
        Date fechaIntervencion = capturarIntervencionesQuirurgicasHelper.convertStringToDate(fecha);
        capturarIntervencionesQuirurgicasRules.validarFechaProgramacion(fechaProgramada, fechaIntervencion);
    }

    @Override
    public boolean requiereAbrirModal(String tipoIntervencion) {

        return capturarIntervencionesQuirurgicasRules.requiereAbrirModal(tipoIntervencion);
    }

    @Override
    public void validarModalProcedimiento() throws HospitalizacionException {

    }

    public void validarCirujanoModal(String matricula) throws HospitalizacionException {

        super.validarMatricula(matricula);

    }

    @Override
    public void validarProcedimientoModal(Procedimientos procedimiento, List<Procedimientos> listaProcedimientos,
            String agregadoMedico) throws HospitalizacionException {

        super.validarProcedimientoIntervencionQuirurgica(procedimiento, listaProcedimientos, agregadoMedico);

    }

    @Override
    public String obtenerDescripcion(String clave) {

        Procedimiento procedimiento = capturarIntervencionesQuirurgicasRepository.obtenerProcedimientoPorClave(clave);
        return procedimiento.getDesCie9();
    }

    @Override
    public DatosMedico obtenerMedico(String matricula, String cvePresupuestal) {

        DatosMedico medico = new DatosMedico();
        medico.setMatricula(matricula);
        medico = super.buscarPersonalOperativo(medico, cvePresupuestal);
        return medico;
    }

    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario usuario) {

        Date fecha = new Date();

        capturarIntervencionesQuirurgicasRepository.guardarIntervencion(
                capturarIntervencionesQuirurgicasHelper.armarIntervenciones(datosHospitalizacion), usuario, fecha);
        for (Procedimientos p : datosHospitalizacion.getDatosIntervenciones().getProcedimientos()) {
            capturarIntervencionesQuirurgicasRepository.guardarProcedimientos(p,
                    datosHospitalizacion.getDatosPaciente().getClaveIngreso(), fecha);
        }
    }

    public Date convertirFecha(String fecha) {

        return capturarIntervencionesQuirurgicasHelper.convertStringToDate(fecha);

    }

    public Date convertirHora(String fecha, String hora) {

        return capturarIntervencionesQuirurgicasHelper
                .convertStringHourToDate(capturarIntervencionesQuirurgicasHelper.convertStringToDate(fecha), hora);
    }

    @Override
    public void validarFechaIngreso(Date fechaIngreso, String fechaIntervencion) throws HospitalizacionException {

        capturarIntervencionesQuirurgicasRules.validarFechaIngreso(
                capturarIntervencionesQuirurgicasHelper.convertStringToDate(fechaIntervencion), fechaIngreso);

    }

    @Override
    public boolean sePuedeActivarGuardar(Boolean[] requeridos) {

        return capturarIntervencionesQuirurgicasRules.sePuedeActivarGuardar(requeridos);
    }

    public HospitalizacionCommonRules getHospitalizacionCommonRules() {

        return hospitalizacionCommonRules;
    }

    public void setHospitalizacionCommonRules(HospitalizacionCommonRules hospitalizacionCommonRules) {

        this.hospitalizacionCommonRules = hospitalizacionCommonRules;
    }

	@Override
	public boolean validarHoraInicioMenorHoraEntrada(DatosHospitalizacion datosHospitalizacion, String fechaIQX) {
		
	       Date fechaEntradaSala = capturarIntervencionesQuirurgicasHelper.obtenerFechaCompleta(fechaIQX,
	                datosHospitalizacion.getDatosIntervenciones().getHoraEntrada());
	       
	      return capturarIntervencionesQuirurgicasRules.validarHoraInicio(fechaEntradaSala,
	                capturarIntervencionesQuirurgicasHelper.obtenerFechaCompleta(fechaIQX,
	                        datosHospitalizacion.getDatosIntervenciones().getHoraInicio())); 
	
	}

	@Override
	public boolean validarHoraTerminoMenorHoraInicio(DatosHospitalizacion datosHospitalizacion, String fechaIQX) {
		  Date fechaInicio = capturarIntervencionesQuirurgicasHelper.obtenerFechaCompleta(fechaIQX,
	                datosHospitalizacion.getDatosIntervenciones().getHoraInicio());
		  
	       return  capturarIntervencionesQuirurgicasRules.validarHoraTermino(fechaInicio, capturarIntervencionesQuirurgicasHelper
	                .obtenerFechaCompleta(fechaIQX, datosHospitalizacion.getDatosIntervenciones().getHoraTermino()));
	}

	@Override
	public boolean validarHoraSalaSalidaMenorHoraTermino(DatosHospitalizacion datosHospitalizacion, String fechaIQX) {
		 Date fechaTermino = capturarIntervencionesQuirurgicasHelper.obtenerFechaCompleta(fechaIQX,
	                datosHospitalizacion.getDatosIntervenciones().getHoraTermino());

	     return capturarIntervencionesQuirurgicasRules.validarHoraSalaSalida(fechaTermino,
	                capturarIntervencionesQuirurgicasHelper.obtenerFechaCompleta(fechaIQX,
	                        datosHospitalizacion.getDatosIntervenciones().getHoraSalida()));
	}

	@Override
	public Ingreso buscarIngresoPorDatosPaciente(DatosHospitalizacion datosHospitalizacion) {
		 
		return capturarIntervencionesQuirurgicasRepository.buscarIngresoPorDatosPaciente(datosHospitalizacion.getDatosPaciente());
	}

	@Override
	public IntervencionesQuirurgicas buscarIntervencionQXPorCveIngreso(Long cveIngreso) {
		return capturarIntervencionesQuirurgicasRepository.buscarIntervencionPorClaveIngreso(cveIngreso);
	}

	@Override
	public void actualizarMetodoAnticonceptivoIQX(int cveMetodo, int cantidadMetodo, Long cveIngreso) {
		capturarIntervencionesQuirurgicasRepository.actualizarMetodoAnticonceptivoIntervencion(cveMetodo, cantidadMetodo, cveIngreso);
		
	}

}
