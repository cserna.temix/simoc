package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoUbicacionEnum {

    HOSPITALIZACION("1", "HOSPITALIZACION"),
    UCI("2", "UCI"),
    URGENCIAS("3", "URGENCIAS"),
    RECIEN_NACIDOS("4", "RECIEN NACIDOS"),
    TOCOCIRUGIA("5", "TOCOCIRUGIA");

    private String clave;
    private String descripcion;

    private TipoUbicacionEnum(String clave, String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

    public String getClave() {

        return clave;
    }

    public String getDescripcion() {

        return descripcion;
    }

    public static TipoUbicacionEnum parse(String clave) {

        TipoUbicacionEnum right = null;
        for (TipoUbicacionEnum item : values()) {
            if (item.getClave().equals(clave)) {
                right = item;
                break;
            }
        }
        return right;
    }

}
