package mx.gob.imss.simo.hospitalizacion.common.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DelegacionIMSS;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;

public interface CatalogosHospitalizacionRepository extends BaseRepository {

    List<DatosCatalogo> obtenerCatalogoDelegacion(String numeroUnidad);

    List<DatosCatalogo> obtenerCatalogoDivision(int claveDivision);

    List<DatosCatalogo> obtenerCatalogoEspecialidad(String clavePresupuestal);

    List<DatosCatalogo> obtenerCatalogoTipoIngreso(String tipoFormato);

    List<DatosCatalogo> obtenerCatalogoTipoAtencion();

    List<DatosCatalogo> obtenerCatalogoTipoParto();

    List<DatosCatalogo> obtenerCatalogoSexo();

    List<DatosCatalogo> obtenerCatalogoTipoNacido();

    List<DatosCatalogo> obtenerCatalogoTipoPrograma(Integer claveTipoPrograma);

    List<DatosCatalogo> obtenerCatalogoMetodoPlanificacion(Integer cveTipoFormato, Integer sexo, Integer edad);
    
    DatosCatalogo obtenerMetodoPlanificacion(int cveMetodoPlanificacion);

    List<DatosCatalogo> obtenerCatalogoTipoUbicacion();

    List<DatosCatalogo> obtenerCatalogoTipoEgreso(Integer cveTipoFormato);

    List<DatosCatalogo> obtenerCatalogoMotivosAlta(String cveTipoEgreso);
    
    List<DatosCatalogo> obtenerCatalogoMotivoEgreso();
    
    List<DatosCatalogo> obtenerCatalogoEnvioA();
    
    List<DatosCatalogo> obtenerCatalogoEgresoDefuncion();
    
    List<DatosCatalogo> obtenerCatalogoRiesgoTrabajo();

    List<DatosCatalogo> obtenerCatalogoTipoIntervencion();

    List<DatosCatalogo> obtenerCatalogoTipoAnestesia();

    List<DatosCatalogo> obtenerDivisionPorEspecialidad(String cveEspecialidad);

    List<DatosCatalogo> obtenerAlimentacionRecienNacido();

    List<DatosCatalogo> obtenerProcedimientoRecienNacido();

    List<DatosCatalogo> obtenerProcedimientoRecienNacidoDos();
    
    List<DatosCatalogo> obtenerProcedimientoRecienNacidoSinCero();

    List<DatosCatalogo> obtenerTamiz();

    UnidadMedica buscarUnidadMedicaPorUnidadAdscripcion(String clavePresupuestal);

    Procedimiento obtenerProcedimiento(String cveProcedimiento);

    PeriodosImss obtenerPeriodo(String mes, String anio);

    Integer obtenerUltimoPeriodoAbierto(String cvePresupuestal, String cveTipoCaptura);

    List<UnidadMedica> obtenerUnidadesMedicasPorDelegacion(String cveDelegacion);

    List<DelegacionIMSS> obtenerDelegacionesIMSS();

    List<DatosCatalogo> obtenerCatalogoTurno();
    
    public List<DatosCatalogo> obtenerCatalogoEspecialidadPorTipoServicio(String cvePresupuestal, Integer numTipoServicio);

}
