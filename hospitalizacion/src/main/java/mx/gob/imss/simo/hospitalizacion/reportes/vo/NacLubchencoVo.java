/**
 * 
 */
package mx.gob.imss.simo.hospitalizacion.reportes.vo;

/**
 * @author francisco.rodriguez
 *
 */
public class NacLubchencoVo{

	public Integer preTerminoPesoBajo;
	public Integer preTerminoPesoNormal;
	public Integer preTerminoPesoAlto;
	public Integer terminoPesoBajo;
	public Integer terminoPesoNormal;
	public Integer terminoPesoAlto;
	public Integer postTerminoPesoBajo;
	public Integer postTerminoPesoNormal;
	public Integer postTerminoPesoAlto;

	/**
	 * @return the preTerminoPesoBajo
	 */
	public Integer getPreTerminoPesoBajo() {
		return preTerminoPesoBajo;
	}

	/**
	 * @param preTerminoPesoBajo
	 *            the preTerminoPesoBajo to set
	 */
	public void setPreTerminoPesoBajo(Integer preTerminoPesoBajo) {
		this.preTerminoPesoBajo = preTerminoPesoBajo;
	}

	/**
	 * @return the preTerminoPesoNormal
	 */
	public Integer getPreTerminoPesoNormal() {
		return preTerminoPesoNormal;
	}

	/**
	 * @param preTerminoPesoNormal
	 *            the preTerminoPesoNormal to set
	 */
	public void setPreTerminoPesoNormal(Integer preTerminoPesoNormal) {
		this.preTerminoPesoNormal = preTerminoPesoNormal;
	}

	/**
	 * @return the preTerminoPesoAlto
	 */
	public Integer getPreTerminoPesoAlto() {
		return preTerminoPesoAlto;
	}

	/**
	 * @param preTerminoPesoAlto
	 *            the preTerminoPesoAlto to set
	 */
	public void setPreTerminoPesoAlto(Integer preTerminoPesoAlto) {
		this.preTerminoPesoAlto = preTerminoPesoAlto;
	}

	/**
	 * @return the terminoPesoBajo
	 */
	public Integer getTerminoPesoBajo() {
		return terminoPesoBajo;
	}

	/**
	 * @param terminoPesoBajo
	 *            the terminoPesoBajo to set
	 */
	public void setTerminoPesoBajo(Integer terminoPesoBajo) {
		this.terminoPesoBajo = terminoPesoBajo;
	}

	/**
	 * @return the terminoPesoNormal
	 */
	public Integer getTerminoPesoNormal() {
		return terminoPesoNormal;
	}

	/**
	 * @param terminoPesoNormal
	 *            the terminoPesoNormal to set
	 */
	public void setTerminoPesoNormal(Integer terminoPesoNormal) {
		this.terminoPesoNormal = terminoPesoNormal;
	}

	/**
	 * @return the terminoPesoAlto
	 */
	public Integer getTerminoPesoAlto() {
		return terminoPesoAlto;
	}

	/**
	 * @param terminoPesoAlto
	 *            the terminoPesoAlto to set
	 */
	public void setTerminoPesoAlto(Integer terminoPesoAlto) {
		this.terminoPesoAlto = terminoPesoAlto;
	}

	/**
	 * @return the postTerminoPesoBajo
	 */
	public Integer getPostTerminoPesoBajo() {
		return postTerminoPesoBajo;
	}

	/**
	 * @param postTerminoPesoBajo
	 *            the postTerminoPesoBajo to set
	 */
	public void setPostTerminoPesoBajo(Integer postTerminoPesoBajo) {
		this.postTerminoPesoBajo = postTerminoPesoBajo;
	}

	/**
	 * @return the postTerminoPesoNormal
	 */
	public Integer getPostTerminoPesoNormal() {
		return postTerminoPesoNormal;
	}

	/**
	 * @param postTerminoPesoNormal
	 *            the postTerminoPesoNormal to set
	 */
	public void setPostTerminoPesoNormal(Integer postTerminoPesoNormal) {
		this.postTerminoPesoNormal = postTerminoPesoNormal;
	}

	/**
	 * @return the postTerminoPesoAlto
	 */
	public Integer getPostTerminoPesoAlto() {
		return postTerminoPesoAlto;
	}

	/**
	 * @param postTerminoPesoAlto
	 *            the postTerminoPesoAlto to set
	 */
	public void setPostTerminoPesoAlto(Integer postTerminoPesoAlto) {
		this.postTerminoPesoAlto = postTerminoPesoAlto;
	}

}
