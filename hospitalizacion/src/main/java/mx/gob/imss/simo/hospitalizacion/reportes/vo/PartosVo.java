package mx.gob.imss.simo.hospitalizacion.reportes.vo;

public class PartosVo{

	public Integer partoNormal;
	public Integer partoVaginales;
	public Integer partoAbdominales;

	/**
	 * @return the partoNormal
	 */
	public Integer getPartoNormal() {
		return partoNormal;
	}

	/**
	 * @param partoNormal
	 *            the partoNormal to set
	 */
	public void setPartoNormal(Integer partoNormal) {
		this.partoNormal = partoNormal;
	}

	/**
	 * @return the partoVaginales
	 */
	public Integer getPartoVaginales() {
		return partoVaginales;
	}

	/**
	 * @param partoVaginales
	 *            the partoVaginales to set
	 */
	public void setPartoVaginales(Integer partoVaginales) {
		this.partoVaginales = partoVaginales;
	}

	/**
	 * @return the partoAbdominales
	 */
	public Integer getPartoAbdominales() {
		return partoAbdominales;
	}

	/**
	 * @param partoAbdominales
	 *            the partoAbdominales to set
	 */
	public void setPartoAbdominales(Integer partoAbdominales) {
		this.partoAbdominales = partoAbdominales;
	}

}
