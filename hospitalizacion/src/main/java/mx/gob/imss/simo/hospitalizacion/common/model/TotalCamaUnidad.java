package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class TotalCamaUnidad {
	
	private String cvePresupuestal;
    private String cveEspecialidad;
    private int canCamaCensable;
    private int canCamaNoCensable;


}
