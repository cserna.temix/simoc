package mx.gob.imss.simo.hospitalizacion.tococirugia.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoAtencionToco;
import mx.gob.imss.simo.hospitalizacion.common.model.RangoLubchenco;
import mx.gob.imss.simo.hospitalizacion.common.model.RecienNacido;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;

public interface TococirugiaRepository extends HospitalizacionCommonRepository {

    List<Tococirugia> buscarUltimaAtencionTococirugia(DatosHospitalizacion datosHospitalizacion);

    void guardarTococirugia(Tococirugia tococirugia);

    List<RangoLubchenco> obtenerRangoLubchenco(int semanasGestacion, int peso);

    Parametro obtenerParametroMaxRecienNacido(String cveParametroMaxRN);

    List<PeriodoAtencionToco> obtenerPeriodoAtencionToco(int cveAtencionPrevia, int cveAtencionActual);

    void guardarRecienNacido(RecienNacido recienNacido);
    
    void actualizarMetodoAnticonceptivoTococirugia(int cve_metodo, int cantidad, long cve_tococirugia);

}
