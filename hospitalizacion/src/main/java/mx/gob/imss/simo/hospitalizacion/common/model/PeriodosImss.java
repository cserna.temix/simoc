package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class PeriodosImss {

    private String clavePeriodo;
    private String numeroAnioPeriodo;
    private String descripcionMesPeriodo;
    private Date fechaInicial;
    private Date fechaFinal;
    private boolean indicadorVigente;
    private Date fechaBaja;

}
