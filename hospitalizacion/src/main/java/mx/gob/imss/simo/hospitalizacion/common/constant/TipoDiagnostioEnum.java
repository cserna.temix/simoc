package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoDiagnostioEnum {

    DIAGNOSTICO_INICIAL_INGRESO("1", "Diagnostico inicial (ingreso)"), DIAGNOSTICO_PRINCIPAL_EGRESO("2",
            "Diagn\u00F3stico principal de egreso"), PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO("3",
                    "Primer diagn\u00F3stico secundario de egreso"), SEGUNDO_DIAGNOSTICO_SECUNDARIO_EGRESO("4",
                            "Segundo diagn\u00F3stico secundario de egreso."), CAUSA_DIRECTA_DEFUNCION("5",
                                    "Causa directa de la defunci\u00F3n"), CAUSA_BASICA_DEFUNCION("6",
                                            "Causa b\u00E1sica de la defunci\u00F3n"), CAUSA_SECUNDARIA_DEFUNCION("7",
                                                    "Causa secundaria de la defuncion"), PRIMERA_COMPLICACION_INTRAHOSPITALARIA(
                                                            "8",
                                                            "Primera complicaci\u00F3n intrahospitalaria."), SEGUNDA_COMPLICACION_INTRAHOSPITALARIA(
                                                                    "9", "Segunda complicaci\u00F3n intrahospitalaria"),                                                                 
    																	TERCER_DIAGNOSTICO_SECUNDARIO_EGRESO("10",
    																			"Tercer diagn\u00F3stico secundario de egreso"),
    																		CUARTO_DIAGNOSTICO_SECUNDARIO_EGRESO("11",
    																				"Cuarto diagn\u00F3stico secundario de egreso"),
    																			QUINTO_DIAGNOSTICO_SECUNDARIO_EGRESO("12",
    																					"Cuarto diagn\u00F3stico secundario de egreso"),
    																			TERCERA_COMPLICACION_INTRAHOSPITALARIA(
    				                                                                            "13", "Tercera complicaci\u00F3n intrahospitalaria");
	

    private TipoDiagnostioEnum(String clave, String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

    private String descripcion;
    private String clave;

    public String getDescripcion() {

        return descripcion;
    }

    private void setDescripcion(String descripcion) {

        this.descripcion = descripcion;
    }

    public String getClave() {

        return clave;
    }

    private void setClave(String clave) {

        this.clave = clave;
    }

}
