package mx.gob.imss.simo.hospitalizacion.seguridad.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLUpdateConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.PersonalOperativoRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.RolRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.UnidadMedicaTitularRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.PersonalOperativo;
import mx.gob.imss.simo.hospitalizacion.common.model.Rol;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;
import mx.gob.imss.simo.hospitalizacion.common.repository.BaseJDBCRepository;
import mx.gob.imss.simo.hospitalizacion.seguridad.repository.PersonalOperativoRepository;

@Repository
public class PersonalOperativoRepositoryImpl extends BaseJDBCRepository implements PersonalOperativoRepository {

    @Override
    public PersonalOperativo obtenerPersonalOperativoPorUsuarioAD(String usuario) {

        return jdbcTemplate.queryForObject(SQLConstants.BUSCAR_PERSONAL_OPERATIVO_POR_CUENTA_AD,
                new Object[] { usuario }, new PersonalOperativoRowMapperHelper());
    }

    @Override
    public List<Rol> obtenerRolesPorUsuarioAD(String usuario) {

        return jdbcTemplate.query(SQLConstants.OBTENER_ROLES_POR_CUENTA_AD, new Object[] { usuario },
                new RolRowMapperHelper());
    }

    @Override
    public void actualizarIntento(String usuario, Integer intento) {

        jdbcTemplate.update(SQLUpdateConstants.ACTUALIZA_INTENTO, intento, usuario);

    }

    @Override
    public void actualizarBloqueo(String usuario, Integer intento) {

        jdbcTemplate.update(SQLUpdateConstants.ACTUALIZA_BLOQUEO, intento, usuario);
    }

    @Override
    public List<UnidadMedica> obtenerUnidadesMedicasPorUsuario(String usuario) {

        return jdbcTemplate.query(SQLConstants.OBTENER_UNIDADES_MEDICAS_POR_CUENTA_AD, new Object[] { usuario },
                new UnidadMedicaTitularRowMapperHelper());
    }

}
