package mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesAdvertenciaConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MovimientosIntrahospitalariosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TococirugiaConstants;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.MovimientoIntraHospitalario;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.hospitalizacion.movimientosintrahospitalarios.services.CapturarMovimientosIntrahospitalariosServices;

@ViewScoped
@ManagedBean(name = "movimientosIntraController")
public class CapturarMovimientosIntrahospitalariosController extends HospitalizacionCommonController {

    private static final long serialVersionUID = -3215005722511710446L;

    @ManagedProperty("#{capturarMovimientosIntrahospitalariosServices}")
    private CapturarMovimientosIntrahospitalariosServices capturarMovimientosIntrahospitalariosServices;

    @ManagedProperty("#{catalogosHospitalizacionServices}")
    private CatalogosHospitalizacionServices catalogosHospitalizacionServices;

    private SelectOneMenu selectEspecialidadAnterior;
    private SelectOneMenu selectDivisionAnterior;
    private InputText textCamaAnterior;
    private SelectOneMenu selectDivisionActual;
    private InputText textCamaActual;
    private AutoComplete autoCompleteEspecialidad;
    private InputMask maskHoraAnterior;
    private InputMask maskHoraActual;
    private CommandButton horasSiActual;
    private CommandButton horasNoActual;

    private List<DatosCatalogo> catalogoEspecialidadAnterior;
    private List<DatosCatalogo> catalogoDivisionAnterior;
    private List<DatosCatalogo> catalogoEspecialidadActual;
    private List<DatosCatalogo> catalogoDivisionActual;
    private Boolean[] requeridos = { Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE };

    private List<String> listaEspecialidad;
    private boolean banderaGuardar;

    @Override
    public String obtenerNombrePagina() {

        // LoginController.setRenderIcons(false);
        return PagesCommonConstants.MOVIMIENTOS_INTRAHOSPITALARIOS;
    }

    @PostConstruct
    public void init() {

        super.init();
        setSubtitulo(obtenerMensaje(BaseConstants.SUBTITULO_MOVIMIENTOS_INTRAHOSPITALARIOS));
        iniciarComponentes();
        desHabilitarComponentes();
        llenarCatalogos();
        inicializarRequeridos();
        setTieneFoco(MovimientosIntrahospitalariosConstants.FOCO_FECHA_ATENCION);
    }

    private void inicializarRequeridos() {

        requeridos[0] = Boolean.FALSE;
        requeridos[1] = Boolean.FALSE;
        requeridos[2] = Boolean.FALSE;
        requeridos[3] = Boolean.FALSE;
        requeridos[4] = Boolean.FALSE; //matricula
        requeridos[5] = Boolean.FALSE; //Hora anterior
        requeridos[6] = Boolean.FALSE; //Hora actual
    }

    private void iniciarComponentes() {

        setSelectEspecialidadAnterior(new SelectOneMenu());
        setSelectDivisionAnterior(new SelectOneMenu());
        setTextCamaAnterior(new InputText());

        setAutoEspecialidad(new AutoComplete());
        setSelectDivisionActual(new SelectOneMenu());
        setTextCamaActual(new InputText());

        setButtonGuardar(new CommandButton());
        setButtonCancelar(new CommandButton());
        setButtonSalir(new CommandButton());
        
        setMaskHoraAnterior(new InputMask());
        setMaskHoraActual(new InputMask());
        setHorasSiActual(new CommandButton());
        setHorasNoActual(new CommandButton());
    }

    private void desHabilitarComponentes() {

        getTextAgregadoMedico().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getSelectEspecialidadAnterior().setDisabled(Boolean.TRUE);
        getSelectDivisionAnterior().setDisabled(Boolean.TRUE);
        getTextCamaAnterior().setDisabled(Boolean.TRUE);
        getSelectDivisionActual().setDisabled(Boolean.TRUE);
        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);
        getTextMedico().setDisabled(Boolean.TRUE);
        getMaskHoraActual().setDisabled(Boolean.FALSE);
        getMaskHoraAnterior().setDisabled(Boolean.FALSE);
    }

    private void llenarCatalogos() {

        setCatalogoEspecialidadActual(catalogosHospitalizacionServices
                .obtenerCatalogoEspecialidad(obtenerDatosUsuario().getCvePresupuestal()));
        setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidadActual()));
    }

    public void validarFechaAtencion() {

        try {
            capturarMovimientosIntrahospitalariosServices.validarFechaAtencion(
                    getDatosHospitalizacion().getDatosMovimientos().getFechaAtencion(),
                    obtenerDatosUsuario().getCvePresupuestal());
            setTieneFoco(getTextNss().getClientId());
            requeridos[0] = Boolean.TRUE;
            getButtonCancelar().setDisabled(Boolean.FALSE);
            habilitarGuardar();
        } catch (HospitalizacionException e1) {
            agregarMensajeError(e1);
            requeridos[0] = Boolean.FALSE;
            getDatosHospitalizacion().getDatosMovimientos()
                    .setFechaAtencion(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            setTieneFoco(getMaskFecha().getClientId());
            habilitarGuardar();
        }
    }
    
    public void validarHoraAnterior(){
    	try {
			capturarMovimientosIntrahospitalariosServices.validarHora(getDatosHospitalizacion()
					.getDatosMovimientos().getHoraAnterior());
			getDatosHospitalizacion().getDatosMovimientos().setHoraActual(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
    		requeridos[6] = Boolean.FALSE;
			getDatosHospitalizacion().getDatosMovimientos().setFechaMovimiento(capturarMovimientosIntrahospitalariosServices
	                 .convertirHora(getDatosHospitalizacion().getDatosMovimientos().getFechaAtencion(), getDatosHospitalizacion()
	                		 .getDatosMovimientos().getHoraAnterior()));
			requeridos[5] = Boolean.TRUE; 
			//setTieneFoco(getAutoEspecialidad().getClientId());
			setTieneFoco(getMaskHoraActual().getClientId());
            habilitarGuardar();
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			getDatosHospitalizacion().getDatosMovimientos().
				setHoraAnterior(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
			setTieneFoco(getMaskHoraAnterior().getClientId());
			requeridos[5] = Boolean.FALSE;
		}
    }
    
    public void validarHoraActual(){
    	try {
			capturarMovimientosIntrahospitalariosServices.validarHora(getDatosHospitalizacion()
					.getDatosMovimientos().getHoraActual());
			if(capturarMovimientosIntrahospitalariosServices.validarHoraActualMayor(getDatosHospitalizacion(),
					getDatosHospitalizacion().getDatosMovimientos().getFechaAtencion())){
				RequestContext context = RequestContext.getCurrentInstance();
             	context.execute("PF('dlgConfirmacionHoras').show();");
             	setTieneFoco(getHorasSiActual().getClientId());
			}else{
				getDatosHospitalizacion().getDatosMovimientos().setFechaMovIngreso(capturarMovimientosIntrahospitalariosServices
		                 .convertirHora(getDatosHospitalizacion().getDatosMovimientos().getFechaAtencion(), getDatosHospitalizacion()
		                		 .getDatosMovimientos().getHoraActual()));
		    	
				requeridos[6] = Boolean.TRUE; 
				//setTieneFoco(getTextMatricula().getClientId());
				setTieneFoco(getAutoEspecialidad().getClientId());
	            habilitarGuardar();
			}
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			getDatosHospitalizacion().getDatosMovimientos().
				setHoraActual(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
			setTieneFoco(getMaskHoraActual().getClientId());
			requeridos[6] = Boolean.FALSE;
		}
    }
    
    public void aprobarHoraActual(){
    	
    	RequestContext context = RequestContext.getCurrentInstance();
    	context.execute("PF('dlgConfirmacionHoras').hide();");
    	String fechaMovIngreso = getDatosHospitalizacion().getDatosMovimientos().getFechaAtencion();
    	getDatosHospitalizacion().getDatosMovimientos().setFechaMovIngreso(capturarMovimientosIntrahospitalariosServices
                 .convertirHora(capturarMovimientosIntrahospitalariosServices.obtenerSiguienteDia(fechaMovIngreso), getDatosHospitalizacion()
                		 .getDatosMovimientos().getHoraActual()));
    	requeridos[6] = Boolean.TRUE; 
    	habilitarGuardar();
    	//setTieneFoco(getTextMatricula().getClientId());
    	setTieneFoco(getAutoEspecialidad().getClientId());
        
    	
    }
    
public void rechazarHoraActual(){
    	
    	RequestContext context = RequestContext.getCurrentInstance();
    	context.execute("PF('dlgConfirmacionHoras').hide();");
    	getDatosHospitalizacion().getDatosMovimientos().setFechaMovIngreso(new Date());
    	getDatosHospitalizacion().getDatosMovimientos().setHoraActual(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
    	requeridos[6] = Boolean.FALSE;
    	habilitarGuardar();
    	setTieneFoco(getMaskHoraActual().getClientId());
    	
    }

    public void validarNss() {

        try {
            capturarMovimientosIntrahospitalariosServices
                    .validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
            getDatosHospitalizacion().getDatosPaciente().setNss(getDatosHospitalizacion().getDatosPaciente().getNss());
            setBanderaNss(Boolean.TRUE);
            requeridos[1] = Boolean.TRUE;
            habilitarGuardar();
            setTieneFoco(getMaskHoraAnterior().getClientId());
        } catch (HospitalizacionException e) {
            setBanderaNss(Boolean.FALSE);
            requeridos[1] = Boolean.FALSE;
            getDatosHospitalizacion().getDatosPaciente().setNss(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            agregarMensajeError(e);
            habilitarGuardar();
            setTieneFoco(getTextNss().getClientId());
        }
    }

    private void llenarDatosAnteriores() {

        MovimientoIntraHospitalario datosAnteriores = capturarMovimientosIntrahospitalariosServices
                .obtenerDatosAnteriores(getDatosHospitalizacion().getDatosPaciente());
        List<DatosCatalogo> especialidad = new ArrayList<DatosCatalogo>();
        DatosCatalogo esp = new DatosCatalogo();
        esp.setClave(datosAnteriores.getEspeciaidadCamaOrigen());
        esp.setDescripcion(datosAnteriores.getEspecialidadOrigen());
        especialidad.add(esp);
        setCatalogoEspecialidadAnterior(especialidad);
        List<DatosCatalogo> division = new ArrayList<DatosCatalogo>();
        DatosCatalogo div = new DatosCatalogo();
        div.setClave(datosAnteriores.getDivisionOrigen());
        div.setDescripcion(datosAnteriores.getDivisionOrigen());
        division.add(div);
        setCatalogoDivisionAnterior(division);
        getDatosHospitalizacion().getDatosMovimientos().setCamaOrigen(datosAnteriores.getCamaOrigen());
        getDatosHospitalizacion().getDatosMovimientos()
                .setEspecialidadOrigen(datosAnteriores.getEspecialidadOrigen().substring(0, 4));
        
        if(getDatosHospitalizacion().getDatosPaciente().getCveTipoFormato()== TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()){
        	actualizarCatEspecialidadActualRN(Boolean.FALSE);
        }else {
        	if(getDatosHospitalizacion().getDatosPaciente().getCveTipoFormato()== TipoFormatoEnum.INGRESO_RN_CUNERO_PATOLOGICO.getClave().intValue()){
        		actualizarCatEspecialidadActualRN(capturarMovimientosIntrahospitalariosServices.
	        			validarRecienNacidoExterno(getDatosHospitalizacion().getDatosPaciente().getCvePaciente(), 
	        					obtenerDatosUsuario().getCvePresupuestal()));
	        }else{
	        	llenarCatalogos();
	        }
        }
        
        setTieneFoco(getMaskHoraAnterior().getClientId());
    }
    
    private void actualizarCatEspecialidadActualRN(Boolean esRecienNacidoExterno){
    	
    	List<DatosCatalogo> especialidadesRN = capturarMovimientosIntrahospitalariosServices
    			.actualizarCatEspecialidadActualRN(getCatalogoEspecialidadActual(),esRecienNacidoExterno);
    	
    	setCatalogoEspecialidadActual(especialidadesRN);
        setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidadActual()));
    	
    	
    }

    public void validarEspecialidadActual() {

        try {
            DatosCatalogo especialidad = null;
            especialidad = validaAutoComplete(getAutoEspecialidad(), BaseConstants.LONGITUD_ESPECIALIDAD,
                    getCatalogoEspecialidadString(), getCatalogoEspecialidadActual());
            if (null != especialidad) {
                getDatosHospitalizacion().getDatosMovimientos().setEspecialidadDestino(especialidad.getClave());
                actualizaCamaActualRN(especialidad.getClave());
            } else {
                getDatosHospitalizacion().getDatosMovimientos()
                        .setEspecialidadDestino(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
                setCatalogoDivisionActual(new ArrayList<DatosCatalogo>());
            }
            capturarMovimientosIntrahospitalariosServices.validarEspecialidad(getDatosHospitalizacion(),
                    obtenerDatosUsuario());
            if(getDatosHospitalizacion().getDatosMovimientos().getEspecialidadDestino()
            		.equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)){
            	capturarMovimientosIntrahospitalariosServices.validarCamaAlojamientoConjunto(getDatosHospitalizacion().getDatosPaciente().getNss(),
            			getDatosHospitalizacion().getDatosPaciente().getCvePaciente(),obtenerDatosUsuario().getCvePresupuestal());
            }
            llenarDivision();
            requeridos[2] = Boolean.TRUE;
            //setTieneFoco(getTextCamaActual().getClientId());
            habilitarGuardar();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[2] = Boolean.FALSE;
            getAutoEspecialidad().resetValue();
            getDatosHospitalizacion().getDatosMovimientos()
                    .setEspecialidadDestino(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            getDatosHospitalizacion().getDatosMovimientos()
            		.setCamaDestino(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            setCatalogoDivisionActual(new ArrayList<DatosCatalogo>());
            setTieneFoco(getAutoEspecialidad().getClientId());
            habilitarGuardar();
        }
    }
    
    public void actualizaCamaActualRN(String especialidadDestino){
    	if(especialidadDestino.substring(0, 2).equals(CapturarIngresosConstants.CLAVE_CUNERO_FISIOLOGICO)){
    		getDatosHospitalizacion().getDatosMovimientos().setCamaDestino(capturarMovimientosIntrahospitalariosServices.
    				obtenerCamaAlojamientoConjunto(getDatosHospitalizacion().getDatosPaciente().getCvePaciente(),
    				obtenerDatosUsuario().getCvePresupuestal()));
        	getTextCamaActual().setDisabled(Boolean.TRUE);
        	setTieneFoco(getTextMatricula().getClientId());
        	requeridos[3] = Boolean.TRUE;
        }else{
        	getDatosHospitalizacion().getDatosMovimientos().setCamaDestino(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
        	getTextCamaActual().setDisabled(Boolean.FALSE);
        	setTieneFoco(getTextCamaActual().getClientId());
        	requeridos[3] = Boolean.FALSE;
        }
    }
    

    public void llenarDivision() {

        if (null != getDatosHospitalizacion().getDatosMovimientos().getEspecialidadDestino()
                && !getDatosHospitalizacion().getDatosMovimientos().getEspecialidadDestino().isEmpty()) {
            setCatalogoDivisionActual(catalogosHospitalizacionServices.obtenerDivisionPorEspecialidad(
                    getDatosHospitalizacion().getDatosMovimientos().getEspecialidadDestino()));
            if (!getCatalogoDivisionActual().isEmpty()) {
                getDatosHospitalizacion().getDatosMovimientos()
                        .setDivisionDestino(getCatalogoDivisionActual().get(0).getClave());
            }
        }

    }

    public void validarCamaActual() {

        try {
        	
        	int indicador = capturarMovimientosIntrahospitalariosServices.validarCamaActual(getDatosHospitalizacion(),
        			obtenerDatosUsuario().getCvePresupuestal());
        	
            requeridos[3] = Boolean.TRUE;
            habilitarGuardar();
            if (indicador == 1) {
                agregarMensajeAdvertencia(getArray(MensajesAdvertenciaConstants.MA_012), null);
            }
            //setTieneFoco(getMaskHoraActual().getClientId());
            setTieneFoco(getTextMatricula().getClientId());
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            requeridos[3] = Boolean.FALSE;
            getDatosHospitalizacion().getDatosMovimientos()
                    .setCamaDestino(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            setTieneFoco(getTextCamaActual().getClientId());
            habilitarGuardar();
        }
    }

    public void habilitarGuardar() {

        if (capturarMovimientosIntrahospitalariosServices.validarActivarGuardar(getRequeridos())) {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            setTieneFoco(MovimientosIntrahospitalariosConstants.FOCO_BOTON_GUARDAR);
        } else {
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }

    }

    @Override
    public void obtenerPaciente() {

        //getDatosHospitalizacion().getDatosPaciente().setAgregadoMedico("");
    	getBanderaNss();
    if(getBanderaNss()){
        super.obtenerPaciente();
        if (getListaDatosPaciente().isEmpty()) {
            setBanderaNss(Boolean.FALSE);
            requeridos[1] = Boolean.FALSE;
            getDatosHospitalizacion().getDatosPaciente().setNss(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
            agregarMensajeError(new HospitalizacionException(MensajesGeneralesConstants.MG_004));
            habilitarGuardar();
            setTieneFoco(getTextNss().getClientId());
            return;
        }
        if (!getListaDatosPaciente().isEmpty() && getListaDatosPaciente().size() > BaseConstants.INDEX_UNO) {
        	getBanderaNss();
            List<DatosHospitalizacion> datosHospitalizacion = getHospitalizacionCommonServices()
                    .crearListaPacientes(getListaDatosPaciente());
            List<DatosHospitalizacion> filtro = new ArrayList<DatosHospitalizacion>();
            for (DatosHospitalizacion dato : datosHospitalizacion) {
                if (dato.getDatosPaciente().getCveTipoFormato() == 2L || dato.getDatosPaciente().getCveTipoFormato() == 5L ) {
                    filtro.add(dato);
                }else  if(dato.getDatosPaciente().getCveTipoFormato() == 6L && 
                		!capturarMovimientosIntrahospitalariosServices.validarIngresoRNUcin(dato)){
                	filtro.add(dato);
                	}
            }
            setListDatosHospitalizacion(filtro);
            if (getListDatosHospitalizacion().isEmpty()) {
                setBanderaNss(Boolean.FALSE);
                requeridos[1] = Boolean.FALSE;
                getDatosHospitalizacion().getDatosPaciente()
                        .setNss(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
                agregarMensajeError(new HospitalizacionException(MensajesGeneralesConstants.MG_004));
                habilitarGuardar();
                setTieneFoco(getTextNss().getClientId());
                return;
            } else {
            	if(getListDatosHospitalizacion().size()>1){
	                for (int i = 0; i < getListDatosHospitalizacion().size(); i++) {
	                    getListDatosHospitalizacion().get(i).setConsecutivo(i + 1);
	                }
	                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PACIENTES);
	                setTieneFoco(getTextNumeroPaciente().getClientId());
	                
            	}else{     		 
            		 getDatosHospitalizacion().setDatosPaciente(getListDatosHospitalizacion().get(0).getDatosPaciente());
                     getDatosHospitalizacion().setDatosIngreso(getListDatosHospitalizacion().get(0).getDatosIngreso());
                     llenarDatosAnteriores();
                     setTieneFoco(getMaskHoraAnterior().getClientId());
            	}
            }
        } else if (!getListaDatosPaciente().isEmpty() && getListaDatosPaciente().size() == BaseConstants.INDEX_UNO) {
            DatosHospitalizacion datosHospitalizacion = getHospitalizacionCommonServices()
                    .crearPaciente(getListaDatosPaciente());
            if (datosHospitalizacion.getDatosPaciente().getCveTipoFormato() == 2L || 
            		datosHospitalizacion.getDatosPaciente().getCveTipoFormato() == 5L) {
                getDatosHospitalizacion().setDatosPaciente(datosHospitalizacion.getDatosPaciente());
                getDatosHospitalizacion().setDatosIngreso(datosHospitalizacion.getDatosIngreso());
                setTieneFoco(getMaskHoraAnterior().getClientId());
                
            }else if(datosHospitalizacion.getDatosPaciente().getCveTipoFormato() == 6L && 
            		!capturarMovimientosIntrahospitalariosServices.validarIngresoRNUcin(datosHospitalizacion)){
            	getDatosHospitalizacion().setDatosPaciente(datosHospitalizacion.getDatosPaciente());
                getDatosHospitalizacion().setDatosIngreso(datosHospitalizacion.getDatosIngreso());
                setTieneFoco(getMaskHoraAnterior().getClientId());
                
            }else {
                getDatosHospitalizacion().setDatosMovimientos(new MovimientoIntraHospitalario());
                getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
                setCatalogoEspecialidadAnterior(new ArrayList<DatosCatalogo>());
                setCatalogoDivisionAnterior(new ArrayList<DatosCatalogo>());
                getDatosHospitalizacion().getDatosPaciente()
                        .setNss(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
                agregarMensajeError(new HospitalizacionException(MensajesErrorConstants.MG_OO4));
                setTieneFoco(getTextNss().getClientId());
                return;
            }
        }
        if (getBanderaNss()) {
            try {
                capturarMovimientosIntrahospitalariosServices.validarExistenciaPaciente(getListaDatosPaciente());
                if (getListaDatosPaciente().size() == BaseConstants.INDEX_UNO) {
                    llenarDatosAnteriores();
                    setTieneFoco(getMaskHoraAnterior().getClientId());
                }
            } catch (HospitalizacionException e) {
                getDatosHospitalizacion().setDatosMovimientos(new MovimientoIntraHospitalario());
                getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
                setCatalogoEspecialidadAnterior(new ArrayList<DatosCatalogo>());
                setCatalogoDivisionAnterior(new ArrayList<DatosCatalogo>());
                getDatosHospitalizacion().getDatosPaciente()
                        .setNss(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
                agregarMensajeError(e);
                setTieneFoco(getTextNss().getClientId());
            }
          }
    	}
        setBanderaNss(Boolean.FALSE);
    }

    @Override
    public void mapearDatosPaciente() {

        super.mapearDatosPaciente();
        if (getNumeroPaciente() == null) {
            return;
        }
        llenarDatosAnteriores();
        setTieneFoco(getMaskHoraAnterior().getClientId());

    }

    public void guardar() {
    	
    	/*modificaciones mejoras hospitalziacion: se requiere que se vuelvan a ejecutar todas las validaciones antes de guardar el objeto, 
    	 * si hay algun valor incorrecto no permite almacenar el objeto y pone el foco en el campo con el error*/
    	try{
    		capturarMovimientosIntrahospitalariosServices.validarFechaAtencion(
                    getDatosHospitalizacion().getDatosMovimientos().getFechaAtencion(),
                    obtenerDatosUsuario().getCvePresupuestal());
    		try{
       		 	capturarMovimientosIntrahospitalariosServices
                .validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
       		 	
	       		    try{
	       		       capturarMovimientosIntrahospitalariosServices.validarHora(getDatosHospitalizacion()
	       					  .getDatosMovimientos().getHoraAnterior());
	       		 	
		       		 	try{
		       		 		capturarMovimientosIntrahospitalariosServices.validarEspecialidad(getDatosHospitalizacion(),
		                         obtenerDatosUsuario());
		       		 		
			       		 	if(getDatosHospitalizacion().getDatosMovimientos().getEspecialidadDestino()
			                		.equals(BaseConstants.ESPECIALIDAD_ALOJAMIENTO_CONJUNTO)){
			                	capturarMovimientosIntrahospitalariosServices.validarCamaAlojamientoConjunto(getDatosHospitalizacion().getDatosPaciente().getNss(),
			                			getDatosHospitalizacion().getDatosPaciente().getCvePaciente(),obtenerDatosUsuario().getCvePresupuestal());
			                }
		       		 		
		       		 		try{
		       		 			capturarMovimientosIntrahospitalariosServices.validarHora(getDatosHospitalizacion()
		       		 						.getDatosMovimientos().getHoraActual());
				       		 		try{
				       		 			capturarMovimientosIntrahospitalariosServices.validarMatricula(getDatosHospitalizacion()
				       		 					.getDatosMedico().getMatricula());
							       		 	try {
							       	           
							       	            if(!getTextCamaActual().isDisabled()){
													capturarMovimientosIntrahospitalariosServices.validarCamaActual(getDatosHospitalizacion(),
													obtenerDatosUsuario().getCvePresupuestal());
												}
							       	            
								       	        capturarMovimientosIntrahospitalariosServices.guardar(getDatosHospitalizacion(), obtenerDatosUsuario(), 0);
								       	        agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
								       	        cancelar();
							       	           
							       	        } catch (HospitalizacionException e4) {
							       	        	agregarMensajeError(e4);
							       	            requeridos[3] = Boolean.FALSE;
							       	            getDatosHospitalizacion().getDatosMovimientos()
							       	                    .setCamaDestino(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
							       	            habilitarGuardar();
							       	        }
				       		 		}catch (HospitalizacionException e5) {
							       	     agregarMensajeError(e5);
							       	     getDatosHospitalizacion().getDatosMedico().setMatricula(TococirugiaConstants.CADENA_VACIA);
							             getDatosHospitalizacion().getDatosMedico().setNombreCompleto(TococirugiaConstants.CADENA_VACIA);
							             getDatosHospitalizacion().getDatosMovimientos().setCveMedico(TococirugiaConstants.CADENA_VACIA);
							             requeridos[4] = Boolean.FALSE;
							             habilitarGuardar();
							             setTieneFoco(getTextMatricula().getClientId());
							       	}
			       		 	}catch (HospitalizacionException e6) {
					       	     agregarMensajeError(e6);
					       	     getDatosHospitalizacion().getDatosMovimientos().
					       	     setHoraActual(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
					       	     setTieneFoco(getMaskHoraActual().getClientId());
					       	     requeridos[6] = Boolean.FALSE;
					       }			       	        
			         	}catch(HospitalizacionException e3){
			         		   agregarMensajeError(e3);
			                   requeridos[2] = Boolean.FALSE;
			                   getAutoEspecialidad().resetValue();
			                   getDatosHospitalizacion().getDatosMovimientos()
			                           .setEspecialidadDestino(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
			                   setCatalogoDivisionActual(new ArrayList<DatosCatalogo>());
			                   setTieneFoco(getAutoEspecialidad().getClientId());
			                   habilitarGuardar();
			         	}
			       	}catch(HospitalizacionException e7){
			       		agregarMensajeError(e7);
			       		getDatosHospitalizacion().getDatosMovimientos().
						setHoraAnterior(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
			       		setTieneFoco(getMaskHoraAnterior().getClientId());
			       		requeridos[5] = Boolean.FALSE;
			       	}  		
		    	}catch(HospitalizacionException e2){
		      		 setBanderaNss(Boolean.FALSE);
		               requeridos[1] = Boolean.FALSE;
		               getDatosHospitalizacion().getDatosPaciente().setNss(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
		               agregarMensajeError(e2);
		               setTieneFoco(getTextNss().getClientId());
		               habilitarGuardar();
		      	}      	
	    	}catch(HospitalizacionException e1){
	    		 agregarMensajeError(e1);
	    		 requeridos[0] = Boolean.FALSE;
	             getDatosHospitalizacion().getDatosMovimientos()
	                     .setFechaAtencion(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
	             setTieneFoco(getMaskFecha().getClientId());
	             habilitarGuardar();
	    	}
    	
    	
    }
   
    public void validarMatricula() {
    	try {
    		
	    	capturarMovimientosIntrahospitalariosServices.validarMatricula(getDatosHospitalizacion().getDatosMedico().getMatricula());
	    	getDatosHospitalizacion().setDatosMedico(capturarMovimientosIntrahospitalariosServices.buscarPersonalOperativo(
	                getDatosHospitalizacion().getDatosMedico(), obtenerDatosUsuario().getCvePresupuestal()));
	    	getDatosHospitalizacion().getDatosMovimientos().setCveMedico(getDatosHospitalizacion().getDatosMedico().getMatricula());
	    	if (getDatosHospitalizacion().getDatosMedico().getIdentificadorConexion()
	                .equals(IdentificadorConexionEnum.ROJO.getRuta())) {
	            agregarMensajeAdvertencia(getArray(MensajesAdvertenciaConstants.MA_013), null);
	        }
	    	requeridos[4] = Boolean.TRUE;
	    	habilitarGuardar();
	    	//setTieneFoco(getButtonGuardar().getClientId());
	        
    	} catch (HospitalizacionException e) {
    		 agregarMensajeError(e);
    		 getDatosHospitalizacion().getDatosMedico().setMatricula(TococirugiaConstants.CADENA_VACIA);
             getDatosHospitalizacion().getDatosMedico().setNombreCompleto(TococirugiaConstants.CADENA_VACIA);
             getDatosHospitalizacion().getDatosMovimientos().setCveMedico(TococirugiaConstants.CADENA_VACIA);
             requeridos[4] = Boolean.FALSE;
             habilitarGuardar();
             setTieneFoco(getTextMatricula().getClientId());
    	}
    	
    }

    public void cancelar() {

//        getSelectEspecialidadAnterior().resetValue();
//        getSelectDivisionAnterior().resetValue();
//        getAutoEspecialidad().resetValue();
//        getSelectDivisionActual().resetValue();
//
//        setSelectEspecialidadAnterior(new SelectOneMenu());
//        setSelectDivisionAnterior(new SelectOneMenu());
//        setTextCamaAnterior(new InputText());
//
//        setAutoEspecialidad(new AutoComplete());
//        setSelectDivisionActual(new SelectOneMenu());
//
//        desHabilitarComponentes();
//        setDatosHospitalizacion(new DatosHospitalizacion());
//        llenarCatalogos();
//
        setCatalogoEspecialidadAnterior(new ArrayList<DatosCatalogo>());
        setCatalogoDivisionAnterior(new ArrayList<DatosCatalogo>());
        setCatalogoDivisionActual(new ArrayList<DatosCatalogo>());
        getAutoEspecialidad().setValue(MovimientosIntrahospitalariosConstants.CADENA_VACIA);;
        setAutoEspecialidad(new AutoComplete());
        setSelectDivisionActual(new SelectOneMenu());
//        inicializarRequeridos();
    	init();
        setBanderaNss(Boolean.FALSE);
        setTieneFoco(MovimientosIntrahospitalariosConstants.FOCO_FECHA_ATENCION);
    }
    
    public void limpiarEspecialidadCama(){
    	getDatosHospitalizacion().getDatosMovimientos().setEspecialidadDestino
    			(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
    	getDatosHospitalizacion().getDatosMovimientos().setCamaDestino
    			(MovimientosIntrahospitalariosConstants.CADENA_VACIA);
    }

    public SelectOneMenu getSelectEspecialidadAnterior() {

        return selectEspecialidadAnterior;
    }

    public void setSelectEspecialidadAnterior(SelectOneMenu selectEspecialidadAnterior) {

        this.selectEspecialidadAnterior = selectEspecialidadAnterior;
    }

    public SelectOneMenu getSelectDivisionAnterior() {

        return selectDivisionAnterior;
    }

    public void setSelectDivisionAnterior(SelectOneMenu selectDivisionAnterior) {

        this.selectDivisionAnterior = selectDivisionAnterior;
    }

    public List<DatosCatalogo> getCatalogoEspecialidadAnterior() {

        return catalogoEspecialidadAnterior;
    }

    public void setCatalogoEspecialidadAnterior(List<DatosCatalogo> catalogoEspecialidadAnterior) {

        this.catalogoEspecialidadAnterior = catalogoEspecialidadAnterior;
    }

    public List<DatosCatalogo> getCatalogoDivisionAnterior() {

        return catalogoDivisionAnterior;
    }

    public void setCatalogoDivisionAnterior(List<DatosCatalogo> catalogoDivisionAnterior) {

        this.catalogoDivisionAnterior = catalogoDivisionAnterior;
    }

    public InputText getTextCamaAnterior() {

        return textCamaAnterior;
    }

    public void setTextCamaAnterior(InputText textCamaAnterior) {

        this.textCamaAnterior = textCamaAnterior;
    }

    public SelectOneMenu getSelectDivisionActual() {

        return selectDivisionActual;
    }

    public void setSelectDivisionActual(SelectOneMenu selectDivisionActual) {

        this.selectDivisionActual = selectDivisionActual;
    }

    public InputText getTextCamaActual() {

        return textCamaActual;
    }

    public void setTextCamaActual(InputText textCamaActual) {

        this.textCamaActual = textCamaActual;
    }
  
    public InputMask getMaskHoraAnterior(){
    	return maskHoraAnterior;
    }
    
    public void setMaskHoraAnterior(InputMask maskHoraAnterior) {

        this.maskHoraAnterior = maskHoraAnterior;
    }
    
    public InputMask getMaskHoraActual(){
    	return maskHoraActual;
    }
    
    public void setMaskHoraActual(InputMask maskHoraActual) {

        this.maskHoraActual = maskHoraActual;
    }
    
    public CommandButton getHorasSiActual(){
    	return horasSiActual;
    }
    
    public void setHorasSiActual(CommandButton horasSiActual) {

        this.horasSiActual = horasSiActual;
    }
    
    public CommandButton getHorasNoActual(){
    	return horasNoActual;
    }
    
    public void setHorasNoActual(CommandButton horasNoActual) {

        this.horasNoActual = horasNoActual;
    }

    public List<DatosCatalogo> getCatalogoEspecialidadActual() {

        return catalogoEspecialidadActual;
    }

    public void setCatalogoEspecialidadActual(List<DatosCatalogo> catalogoEspecialidadActual) {

        this.catalogoEspecialidadActual = catalogoEspecialidadActual;
    }

    public List<DatosCatalogo> getCatalogoDivisionActual() {

        return catalogoDivisionActual;
    }

    public void setCatalogoDivisionActual(List<DatosCatalogo> catalogoDivisionActual) {

        this.catalogoDivisionActual = catalogoDivisionActual;
    }

    public CapturarMovimientosIntrahospitalariosServices getCapturarMovimientosIntrahospitalariosServices() {

        return capturarMovimientosIntrahospitalariosServices;
    }

    public void setCapturarMovimientosIntrahospitalariosServices(
            CapturarMovimientosIntrahospitalariosServices capturarMovimientosIntrahospitalariosServices) {

        this.capturarMovimientosIntrahospitalariosServices = capturarMovimientosIntrahospitalariosServices;
    }

    @Override
    public boolean requiereBuscarPorNumeroCama() {

        return false;
    }

    public List<String> getListaEspecialidad() {

        return listaEspecialidad;
    }

    public void setListaEspecialidad(List<String> listaEspecialidad) {

        this.listaEspecialidad = listaEspecialidad;
    }

    public AutoComplete getAutoCompleteEspecialidad() {

        return autoCompleteEspecialidad;
    }

    public void setAutoCompleteEspecialidad(AutoComplete autoCompleteEspecialidad) {

        this.autoCompleteEspecialidad = autoCompleteEspecialidad;
    }

    public boolean isBanderaGuardar() {

        return banderaGuardar;
    }

    public void setBanderaGuardar(boolean banderaGuardar) {

        this.banderaGuardar = banderaGuardar;
    }

    public Boolean[] getRequeridos() {

        return requeridos;
    }

    public void setRequeridos(Boolean[] requeridos) {

        this.requeridos = requeridos;
    }

    public void salir() throws IOException {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }

    public CatalogosHospitalizacionServices getCatalogosHospitalizacionServices() {

        return catalogosHospitalizacionServices;
    }

    public void setCatalogosHospitalizacionServices(CatalogosHospitalizacionServices catalogosHospitalizacionServices) {

        this.catalogosHospitalizacionServices = catalogosHospitalizacionServices;
    }

}
