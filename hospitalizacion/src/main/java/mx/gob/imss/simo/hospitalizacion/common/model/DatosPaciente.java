package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;

@Data
@EqualsAndHashCode(callSuper = false)
public class DatosPaciente implements Cloneable{

    private String agregadoMedico;
    private String apellidoMaterno;
    private String apellidoPaterno;
    private String clavePresupuestal;
    private long cveTipoFormato;
    private String cama;
    private String colorIdentificador;
    private String curp;
    private Long cvePaciente;
    private String delegacion;
    private String descripcionUnidad;
    private Integer edadAnios;
    private Integer edadSemanas;
    private Date fechaConsulta;
    private Date fechaNacimiento;
    private String idee;
    private long idPersona;
    private boolean marcaAcceder;
    private String nombre;
    private String numero;
    private String nss;
    private String registroPatronal;
    private Integer sexo;
    private String tipoPension;
    private boolean vigencia;
    private Long claveIngreso;
    private Long cvePacientePadre;
    private Long cveRecineNacido;
    private Date fechaIngreso;
    private boolean indPlanificacionFamiliar;
    private String nombreCompleto;
    private boolean indRecienNacido;
    private Long indUltimoRegistro;
    private Integer consecutivo;
    private String fechaModificacion;

    public DatosPaciente() {
        this.agregadoMedico = "";
        this.apellidoMaterno = "";
        this.apellidoPaterno = "";
        this.cama = "";
        this.clavePresupuestal = "";
        this.colorIdentificador = IdentificadorConexionEnum.ROJO.getRuta();
        this.curp = "";
        this.delegacion = "";
        this.descripcionUnidad = "";
        this.idee = "";
        this.nombre = "";
        this.numero = "";
        this.nss = "";
        this.registroPatronal = "";
        this.tipoPension = "";
        this.fechaModificacion = "";
       
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
