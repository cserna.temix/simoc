package mx.gob.imss.simo.hospitalizacion.common.constant;

public class TococirugiaConstants extends BaseConstants {

    /*
     * Caracteres Unicode #
     * # � - \u00C1 � - \u00E1
     * # � - \u00C9 � - \u00E9
     * # � - \u00CD � - \u00ED
     * # � - \u00D3 � - \u00F3
     * # � - \u00DA � - \u00FA
     * # � - \u00D1 � - \u00F1
     */

    public static final String FOCO_PLANIFICACION_FAMILIAR = "idPlanificacionFamiliar";
    public static final String FOCO_HORA_PARTO = "idHoraParto";
    public static final String REQUERIDO_HORA_PARTO = "Hora de parto";
    public static final String REQUERIDO_ATENCION = "Atencion por";
    public static final String REQUERIDO_TIPO_PARTO = "Tipo de parto";
    public static final String REQUERIDO_EPISIOTOMIA = "Episiotom\u00EDa";
    public static final String REQUERIDO_TOTAL_NACIDO = "Total de reci\u00E9n nacidos";
    public static final String FOCO_FECHA_ATENCION = "idFechaAtencion";
    public static final int LONGITUD_MIN_SALA = 2;
    public static final int TOTAL_NACIDOS_MIN = 1;
    public static final int TOTAL_NACIDOS_MAX = 9;
    public static final int NUMERO_MAXIMO_AUTOCOMPLETE_PLANFICACION = 2;
    public static final int PESO_GRAMOS_RECIEN_NACIDO_MIN = 200;
    public static final int PESO_GRAMOS_RECIEN_NACIDO_MAX = 6000;
    public static final int TALLA_CENTIMETROS_RECIEN_NACIDO_MIN = 30;
    public static final int TALLA_CENTIMETROS_RECIEN_NACIDO_MAX = 62;
    public static final int PERIMETRO_CEFALICO_CENTIMETROS_RECIEN_NACIDO_MIN = 0;
    public static final int PERIMETRO_CEFALICO_CENTIMETROS_RECIEN_NACIDO_MAX = 40;
    public static final int SEMANAS_GESTACION_RECIEN_NACIDO_MIN = 21;
    public static final int SEMANAS_GESTACION_RECIEN_NACIDO_MAX = 44;
    public static final int APGAR1_RECIEN_NACIDO_MIN = 1;
    public static final int APGAR1_RECIEN_NACIDO_MAX = 9;
    public static final int APGAR5_RECIEN_NACIDO_MIN = 1;
    public static final int APGAR5_RECIEN_NACIDO_MAX = 9;
    public static final String SALAS_TOCOCIRUGIA = "Salas de Tococirug\u00EDa";
    public static final String ESPECIALIDAD_TOCOCIRUGIA = "A600";

    public static final int EDAD_MINIMA_TOCOCIRUGIA = 10;
    public static final int EDAD_MAXIMA_TOCOCIRUGIA = 49;

    public static final String REQUERIDO_NACIDO = "Nacido";
    public static final String REQUERIDO_PESO = "Peso";
    public static final String REQUERIDO_SEXO = "Sexo";
    public static final String REQUERIDO_TALLA = "Talla";
    public static final String REQUERIDO_PERIMETRO_CEFALICO = "Per\u00EDmetro cef\u00E1lico";
    public static final String REQUERIDO_SEMANAS_GESTACION = "Semanas de gestaci\u00F3n";
    public static final String REQUERIDO_APGAR_MINUTO = "APGAR al minuto";
    public static final String REQUERIDO_APGAR_CINCO_MINUTOS = "APGAR a los cinco minutos";
    public static final String REQUERIDO_HORA_DEFUNCION = "Hora Defunci\u00F3n";

    public static final String RECIEN_NACIDO_NOMBRE = "RN";
    public static final String NUM_MAX_RECIEN_NACIDOS = "NUM_MAX_RECIEN_NACIDOS";

    public static final String CERRAR_CONFIRM_DIAGNOSTICO = "PF('recienNacidoConfirm').hide();";
    public static final String CERRAR_MODAL = "PF('idDlgreciennacido').hide();";
    public static final String MODAL_CONFIRM = "CONFIRMACION";
    
    public static final String OCULTAR_EPISIOTOMIA = "ocultarEpisiotomia();";
    public static final String MOSTRAR_EPISIOTOMIA = "mostrarEpisiotomia();";

    public enum TipoAtencionEnum {
    	PARTO( 1 ),ABORTO( 2 ), LEGRADO( 3 );
    	
    	TipoAtencionEnum( int claveTipoAtencion ){
    		this.claveTipoAtencion = claveTipoAtencion;
    	}
    	
    	private final int claveTipoAtencion;

		public int getClaveTipoAtencion() {
			return claveTipoAtencion;
		}    	
    	
    }
    
    public enum EpisiotomiaEnum {
    	CON( "1","CON"),SIN( "2","SIN" );
    	
    	private final String clave;
    	private final String descripcion;
    	
    	EpisiotomiaEnum( String clave, String descripcion ){
    		this.clave = clave;
            this.descripcion = descripcion;
    	}
    	
    	public String getClave() {

            return clave;
        }

        public String getDescripcion() {

            return descripcion;
        }   	
    	
    }
}
