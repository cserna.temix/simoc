package mx.gob.imss.simo.hospitalizacion.interconsultas.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.confirmdialog.ConfirmDialog;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CampoInterconsultaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.IntervencionesQuirurgicasConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesAdvertenciaConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MovimientosIntrahospitalariosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SexoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoDiagnostioEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoProgramaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.AtencionInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.EncabezadoInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.hospitalizacion.ingresos.helper.CapturarIngresoHelper;
import mx.gob.imss.simo.hospitalizacion.interconsultas.service.CapturaInterconsultaService;

@Data
@ViewScoped
@ManagedBean(name = "capturaInterconsultaController")
public class CapturaInterconsultaController extends HospitalizacionCommonController {

@ManagedProperty("#{catalogosHospitalizacionServices}")
protected CatalogosHospitalizacionServices catalogosHospitalizacionServices;
	
	private static final long serialVersionUID = 1L;

	private Integer duracionInter;
	
	private Date fechaActual;
    
	private String fecha;
	private String matricula;	
	private String nss;
	private String agregadoMedico;
	private String fechaActualString;
	private String delegacion;
	private String especialidadSel;
	//private String especialidadDescripcion;
	private String especialidadSolic;
	//private String especialidadSolicDescripcion;
	private String turnoDescripcion;
	private SelectOneMenu selectTurno;
	private String tieneFoco = "";
	private String edad;
	private String primVezSel;
	private String diagAdicPrimVez;
	private String diagComplPrimVez;

	private String altaIntercon;
	private String diagnosticoPrincipal;
	private String diagnosticoAdicional;
	private String diagnosticoComplementario;
	private String procedimientoPrincipal;
	private String procedimientoAdicional;
	private String procedimientoComplementario;
	private String procedimientoPrincipalDesc;
	private String procedimientoAdicionalDesc;
	private String procedimientoComplementarioDesc;
	private String tipoDiagnostico;
	private Integer claveInterconsulta;
	protected long cvePaciente;
	
	private InputText textEdad;
	private InputMask textFechaNacimiento;
	private InputText textNumero;
	private InputText textClave;
	private InputText textDescripcion;
	private InputText textDuracionIntercon;
	private InputText textDiagnosticoPrincipal;
	private InputText textDiagnosticoPrincipalDesc;
	private InputText textDiagnosticoAdicional;
	private InputText textDiagnosticoAdicionalDesc;
	private InputText textDiagnosticoComplementario;
	private InputText textDiagnosticoComplementarioDesc;
	private InputText textProcedimientoPrincipal;
	private InputText textProcedimientoAdicional;
	private InputText textProcedimientoComplementario;
	private InputText textProcedimientoPrincipalDesc;
	private InputText textProcedimientoAdicionalDesc;
	private InputText textProcedimientoComplementarioDesc;
	private CommandButton botonModalSi;
	
	private boolean encabezadoGuardado;
	   
    protected ConfirmDialog alertaEpidemiologica;
    
    private DatosCatalogo diagPrincipalDesc;
	private DatosCatalogo diagnosticoAdicionalDesc;
	private DatosCatalogo diagnosticoComplementarioDesc;
	
	private Procedimiento procedPrincipalDesc;
	private Procedimiento procedAdicionalDesc;
	private Procedimiento procedComplemtDesc;

	private SelectOneMenu selectPrimVez;
	private SelectOneMenu selectAltaIntercon;
	private SelectOneMenu selectPVezDiagAdicional;
	private SelectOneMenu selectPVezDiagComplement;
	
	
	private AutoComplete autoEspecialidadSolic;
	
	private CommandButton buttonGuardarDetalle;
	private CommandButton buttonCancelarDetalle;
	private CommandButton buttonSalirDetalle;
	private CommandButton confirmSi;
    private CommandButton confirmNo;
		
	private List<DatosPaciente> pacientes;
	//private List<Especialidad> especialidadesList;
	private List<DatosCatalogo> catalogoTurnoList;
	
	private Map<String,Map<String,String>> data = new HashMap<String, Map<String,String>>();
	private Map<String,String> alta;
	private Map<String,String> primeraVez;
	private Map<String,String> primVezDiagAdic;
	private Map<String,String> primVezDiagComp;
	
	private Long contadorInterconsulta;
	
	private static final Integer HOSPITALIZACION = 2;

    @ManagedProperty("#{capturaInterconsultaService}")
    private CapturaInterconsultaService capturaInterconsultaService;
    
    @Autowired
    private CapturarIngresoHelper capturarIngresoHelper;

    @Override
	@PostConstruct
    public void init() {
    	
		super.init();
		try {

			setFechaActual(new Date());
			setFechaActualString(new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getFechaActual()));
			diagPrincipalDesc = new DatosCatalogo();
			diagnosticoAdicionalDesc = new DatosCatalogo();
			diagnosticoComplementarioDesc = new DatosCatalogo();			
			procedPrincipalDesc = new Procedimiento();
			procedAdicionalDesc = new Procedimiento();
			procedComplemtDesc = new Procedimiento();
			alertaEpidemiologica = new ConfirmDialog();
			tieneFoco = "interconsultaForm:idFechaIntercon";
			selectTurno = new SelectOneMenu();
			//especialidadesList = capturaInterconsultaService.obtenerEspecialidadesPorCvePres(getObjetosSs().getDatosUsuario().getCvePresupuestal());
			setCatalogoEspecialidad(capturaInterconsultaService.obtenerCatalogoEspecialidad(TipoUbicacionEnum.HOSPITALIZACION,
	                getObjetosSs().getDatosUsuario().getCvePresupuestal()));
	        setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
	        setTipoUbicacion(TipoUbicacionEnum.HOSPITALIZACION);
			//logger.info("Numero de especialidades: " + especialidadesList.size());
			catalogoTurnoList = capturaInterconsultaService.obtenerCatalogoTurno();
			textEdad = new InputText();
			textFechaNacimiento = new InputMask();
			textNumero = new InputText();
			textClave = new InputText();
			textDescripcion = new InputText();
			textDiagnosticoPrincipal = new InputText(); 
			textDiagnosticoPrincipalDesc = new InputText();
			textDiagnosticoAdicional = new InputText();
			textDiagnosticoAdicionalDesc = new InputText();
			selectPVezDiagAdicional = new SelectOneMenu();
			textDiagnosticoComplementario = new InputText();
			textDiagnosticoComplementarioDesc = new InputText();
			selectPVezDiagComplement = new SelectOneMenu();
			textProcedimientoPrincipal = new InputText();
			textProcedimientoAdicional = new InputText();
			textProcedimientoComplementario = new InputText();
			textProcedimientoPrincipalDesc = new InputText();
			textProcedimientoAdicionalDesc = new InputText();
			textProcedimientoComplementarioDesc = new InputText();
			textDuracionIntercon = new InputText();
			buttonCancelarDetalle = new CommandButton();
			buttonGuardarDetalle = new CommandButton();
			buttonSalirDetalle = new CommandButton();
			getButtonGuardar().setDisabled(Boolean.TRUE);
			getButtonCancelar().setDisabled(Boolean.TRUE);
			getTextMedico().setDisabled(Boolean.TRUE);
			buttonCancelarDetalle.setDisabled(Boolean.TRUE);
			buttonGuardarDetalle.setDisabled(Boolean.TRUE);
			deshabilitarCamposDerechohabiente();
			deshabilitaCamposDiagYProced();
			camposHashMap();
			this.encabezadoGuardado = false;
			tieneFoco="interconsultaForm:idFechaIntercon";
			catalogoTurnoList=capturaInterconsultaService.obtenerCatalogoTurno();
			
		} catch (HospitalizacionException e) {
			logger.error("Error al consultar el catalogo de especialidades");			
		}
	}

    @Override
	public String obtenerNombrePagina() {		
    	
		return PagesCommonConstants.INTERCONSULTAS;
	}
    
	public void validarNombre() {
		
		try {
			capturaInterconsultaService.validarDatosCapturaPaciente(datosHospitalizacion.getDatosPaciente().getNombre(), CampoInterconsultaEnum.NOMBRE_PACIENTE.getClave());
			datosHospitalizacion.getDatosPaciente().setNombre(datosHospitalizacion.getDatosPaciente().getNombre().toUpperCase());
			tieneFoco = getTextApellidoPaterno().getClientId();
		} catch (HospitalizacionException e) {
			tieneFoco = getTextNombre().getClientId();
			agregarMensajeError(e);
		}
	}

	public void validarApellidoPaterno() {
		
		try {
			capturaInterconsultaService.validarDatosCapturaPaciente(datosHospitalizacion.getDatosPaciente().getApellidoPaterno(), CampoInterconsultaEnum.AP_PAT_PACIENTE.getClave());
			datosHospitalizacion.getDatosPaciente()
			.setApellidoPaterno(datosHospitalizacion.getDatosPaciente().getApellidoPaterno().toUpperCase());
			tieneFoco = getTextApellidoMaterno().getClientId();
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			tieneFoco = getTextApellidoPaterno().getClientId();
		}
	}

	public void validarApellidoMaterno() {
		
		try {
			capturaInterconsultaService.validarDatosCapturaPaciente(datosHospitalizacion.getDatosPaciente().getApellidoMaterno(), CampoInterconsultaEnum.AP_MAT_PACIENTE.getClave());
			datosHospitalizacion.getDatosPaciente()
			.setApellidoMaterno(datosHospitalizacion.getDatosPaciente().getApellidoMaterno().toUpperCase());
			tieneFoco = getTextNumero().getClientId();
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			tieneFoco = getTextApellidoMaterno().getClientId();
		}
	}
    
	public void validarFechaDeNacimiento() {
		
//		try {
//			capturaInterconsultaService.validarFechaInterconsulta(textFechaNacimiento.getValue().toString());
////			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
////			try {
////				datosHospitalizacion.getDatosPaciente()
////					.setFechaNacimiento(sdf.parse(textFechaNacimiento.getValue().toString()));
////				datosHospitalizacion.getDatosPaciente()
////				.setFechaNacimiento(stringToDate(textFechaNacimiento.getValue().toString()));				
////			} catch (ParseException e) {
////				e.printStackTrace();
////			}			
////			tieneFoco = getTextDuracionIntercon().getClientId();
////			tieneFoco = getTextClave().getClientId();
//			tieneFoco = getTextNumero().getClientId();
//		} catch (HospitalizacionException e) {
//			tieneFoco = getTextFechaNacimiento().getClientId();
//			agregarMensajeError(e);
//		}	
	}
	
	public void validarUnidadAdscripcion() {
		
		tieneFoco = getTextDuracionIntercon().getClientId();
	}
	
    public void validarNumero() throws HospitalizacionException {

        datosHospitalizacion.getDatosPaciente()
                .setNumero(datosHospitalizacion.getDatosPaciente().getNumero().toUpperCase());
        if (datosHospitalizacion.getDatosPaciente().getNumero() == null || datosHospitalizacion.getDatosPaciente().getNumero().isEmpty()) {
        	tieneFoco = getTextNumero().getClientId();
        	throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_NUMERO));
        }
        setCatalogoDelegacion(catalogosHospitalizacionServices
                .obtenerCatalogoDelegacion(datosHospitalizacion.getDatosPaciente().getNumero()));
        setCatalogoDelegacionString(convertirCatalogoString(getCatalogoDelegacion()));
        tieneFoco = getAutoDelegacion().getClientId();
    }
	
    public void validarDelegacion() throws HospitalizacionException {

        DatosCatalogo datosCatalogo;
        datosCatalogo = validaAutoComplete(getAutoDelegacion(), BaseConstants.LONGITUD_DELEGACION,
                getCatalogoDelegacionString(), getCatalogoDelegacion());
        if (null != datosCatalogo) {
            datosHospitalizacion.getDatosPaciente().setDelegacion(datosCatalogo.getClave());
            capturaInterconsultaService.validarDelegacion(datosHospitalizacion.getDatosPaciente().getDelegacion(),
                    getCatalogoDelegacion());
            if (!datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                    .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                    .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())) {
                UnidadMedica unidadMedica = capturaInterconsultaService.buscarUnidadMedica(
                        datosHospitalizacion.getDatosPaciente().getNumero(),
                        datosHospitalizacion.getDatosPaciente().getDelegacion());
                datosHospitalizacion.getDatosPaciente().setClavePresupuestal(unidadMedica.getCvePresupuestal());
                datosHospitalizacion.getDatosPaciente().setDescripcionUnidad(unidadMedica.getDesUnidadMedica());
                tieneFoco = getTextDuracionIntercon().getClientId();
            }
        } else {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_DELEGACION));
        }
    }
    
    public void habilitarCamposNoEncontradoNoDerechohabiente() {

        getTextNombre().setDisabled(Boolean.FALSE);
        getTextApellidoPaterno().setDisabled(Boolean.FALSE);
        getTextApellidoMaterno().setDisabled(Boolean.FALSE);
        textNumero.setDisabled(Boolean.FALSE);
        getAutoDelegacion().setDisabled(Boolean.FALSE);
        textFechaNacimiento.setDisabled(Boolean.TRUE);
        textEdad.setDisabled( Boolean.FALSE );
//        textClave.setDisabled(Boolean.FALSE);
//        textDescripcion.setDisabled(Boolean.FALSE);
        
    }
    
    public void deshabilitarCamposDerechohabiente() {

    	  getTextNombre().setDisabled(Boolean.TRUE);
          getTextApellidoPaterno().setDisabled(Boolean.TRUE);
          getTextApellidoMaterno().setDisabled(Boolean.TRUE);
          textNumero.setDisabled(Boolean.TRUE);
          getAutoDelegacion().setDisabled(Boolean.TRUE);
          textFechaNacimiento.setDisabled(Boolean.TRUE);
          textEdad.setDisabled( Boolean.TRUE);
          textClave.setDisabled(Boolean.TRUE);
          textDescripcion.setDisabled(Boolean.TRUE);
    }
    
    public void deshabilitaCamposDiagYProced(){
    	
    	textDiagnosticoPrincipalDesc.setDisabled(Boolean.TRUE);
		textDiagnosticoAdicional.setDisabled(Boolean.TRUE);
		textDiagnosticoAdicionalDesc.setDisabled(Boolean.TRUE);
		selectPVezDiagAdicional.setDisabled(Boolean.TRUE);
		textDiagnosticoComplementario.setDisabled(Boolean.TRUE);
		textDiagnosticoComplementarioDesc.setDisabled(Boolean.TRUE);
		selectPVezDiagComplement.setDisabled(Boolean.TRUE);
		textProcedimientoPrincipal.setDisabled(Boolean.TRUE);
		textProcedimientoAdicional.setDisabled(Boolean.TRUE);
		textProcedimientoComplementario.setDisabled(Boolean.TRUE);
		textProcedimientoPrincipalDesc.setDisabled(Boolean.TRUE);
		textProcedimientoAdicionalDesc.setDisabled(Boolean.TRUE);
		textProcedimientoComplementarioDesc.setDisabled(Boolean.TRUE);
    }
    
    public void habilitarCamposNoDerechohabiente() {

    	if (getCapturaInterconsultaService().habilitarEdad(getFechaActualString(),
                getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
            setEdad(null);
            getTextEdad().setDisabled(Boolean.FALSE);
            setTieneFoco(getTextEdad().getClientId());
        } else {
            Map<TipoCalculoEdadEnum, Integer> edades = getCapturaInterconsultaService()
                    .calcularEdadPacienteNoEncontrado(
                            getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                    .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO),
                            getFechaActualString());
            getDatosHospitalizacion().getDatosPaciente().setEdadAnios(edades.get(TipoCalculoEdadEnum.ANIOS));
            getDatosHospitalizacion().getDatosPaciente().setEdadSemanas(edades.get(TipoCalculoEdadEnum.SEMANAS));
            setEdad(getDatosHospitalizacion().getDatosPaciente().getEdadAnios().toString());
            getTextEdad().setDisabled(Boolean.TRUE);
            setTieneFoco(getTextNombre().getClientId());
        }
    }
    
    public void limpiarCatalogos() {

        setCatalogoDelegacion(new ArrayList<DatosCatalogo>());
    }
    
    public void habilitarEdad() {

        if (getCapturaInterconsultaService().habilitarEdad(getFechaActualString(),
                getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
            setEdad(getDatosHospitalizacion().getDatosPaciente().getEdadSemanas().toString());
        } else {
            setEdad(getDatosHospitalizacion().getDatosPaciente().getEdadAnios().toString());
        }
    }
    
    public void habilitarPacienteFueraDeNucleoFam() {

        getDatosHospitalizacion().getDatosPaciente().setCvePaciente(null);
        getDatosHospitalizacion().getDatosPaciente().setNombre(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setApellidoPaterno(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setApellidoMaterno(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setNumero(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setDelegacion(BaseConstants.CADENA_VACIA);
        getAutoDelegacion().resetValue();
        getDatosHospitalizacion().getDatosPaciente().setClavePresupuestal(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setDescripcionUnidad(BaseConstants.CADENA_VACIA);
        if (capturaInterconsultaService.habilitarEdad(getFechaActualString(),
                getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
            setEdad(BaseConstants.CADENA_VACIA);
            getTextEdad().setDisabled(Boolean.FALSE);
            setTieneFoco(getTextEdad().getClientId());
        } else {
            Map<TipoCalculoEdadEnum, Integer> edades = capturaInterconsultaService
                    .calcularEdadPacienteNoEncontrado(
                            getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                    .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO),
                            getFechaActualString());
            getDatosHospitalizacion().getDatosPaciente().setEdadAnios(edades.get(TipoCalculoEdadEnum.ANIOS));
            getDatosHospitalizacion().getDatosPaciente().setEdadSemanas(edades.get(TipoCalculoEdadEnum.SEMANAS));
            setEdad(getDatosHospitalizacion().getDatosPaciente().getEdadAnios().toString());
            getTextEdad().setDisabled(Boolean.TRUE);
            setTieneFoco(getTextNombre().getClientId());
        }
        getDatosHospitalizacion().getDatosPaciente()
                .setSexo(SexoEnum
                        .parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                        .getClave());
        limpiarCatalogos();
        habilitarCamposNoEncontradoNoDerechohabiente();
        RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES);
    }
    
    public void camposHashMap(){
    	
    	primeraVez  = new HashMap<String, String>();
		primeraVez.put("NO", "0");
		primeraVez.put("SI", "1");
		
		alta  = new HashMap<String, String>();
		alta.put("NO", "0");
		alta.put("SI", "1");
		
		primVezDiagAdic  = new HashMap<String, String>();
		primVezDiagAdic.put("NO", "0");
		primVezDiagAdic.put("SI", "1");
		
		primVezDiagComp  = new HashMap<String, String>();
		primVezDiagComp.put("NO", "0");
		primVezDiagComp.put("SI", "1");
    	
    }

	//E N C A B E Z A D O
	
	/**
	 * Metodo que realiza la validacion de la fecha
	 * 1 .- Valida que la fecha no este vacia
	 * 2 .- Valida el formato de la fecha
	 * 3 .- Valida que la fecha sea menor o igual a la fecha actual
	 * 4 .- Valida que la fecha este dentro del periodo activo
	 * 5 .- Valida que se haya generado el reporte de pacientes hospitalizados del dia anterior
	 */
	public void validarFecha() {
		logger.debug("Entro a validar fecha...");
		try {
			capturaInterconsultaService.validarFechaInterconsulta( this.getMaskFecha().getValue().toString() );	
			capturaInterconsultaService.validarFechaEnPeriodoActualActivo( this.getMaskFecha().getValue().toString(), getObjetosSs().getDatosUsuario().getCvePresupuestal());
			if( capturaInterconsultaService instanceof HospitalizacionCommonServices ){
				((HospitalizacionCommonServices)capturaInterconsultaService).validarReporteRelacionPacientesGenerado(getObjetosSs().getDatosUsuario().getCvePresupuestal(), this.getMaskFecha().getValue().toString());
			}						
			getButtonCancelar().setDisabled(Boolean.FALSE);
            tieneFoco = getAutoEspecialidad().getClientId();
		} catch (HospitalizacionException e) {
			tieneFoco = getMaskFecha().getClientId();
			limpiarFecha();
			setTieneFoco(getMaskFecha().getClientId());
			getButtonCancelar().setDisabled(Boolean.TRUE);            
            agregarMensajeError(e);
            
		}
		
		
		
//		try {
//            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_1)) {
//                fechaIngreso = CapturarIngresosConstants.FECHA_VACIA;
//                return;
//            }
//            validarFechaIngreso();
//            getButtonCancelar().setDisabled(Boolean.FALSE);
//            tieneFoco = getTextNss().getClientId();
//        } catch (HospitalizacionException e) {
//            getButtonCancelar().setDisabled(Boolean.TRUE);
//            fechaIngreso = CapturarIngresosConstants.FECHA_VACIA;
//            agregarMensajeError(e);
//            tieneFoco = getMaskFecha().getClientId();
//        }
				
	}
	
	 public void validarFechaIngreso() throws HospitalizacionException {
		 try {
		 capturaInterconsultaService.validarFechaIngreso( this.getMaskFecha().getValue().toString() , getObjetosSs().getDatosUsuario().getCvePresupuestal());
	        SimpleDateFormat formatter = new SimpleDateFormat(BaseConstants.FORMATO_FECHA);
	        Date date = null;
	       
	            date = formatter.parse( this.getMaskFecha().getValue().toString() );	            
	            getButtonCancelar().setDisabled(Boolean.FALSE);
	            tieneFoco = getAutoEspecialidad().getClientId();
	        } catch (ParseException e) {
	        	tieneFoco = getMaskFecha().getClientId();
				limpiarFecha();
				setTieneFoco(getMaskFecha().getClientId());
				getButtonCancelar().setDisabled(Boolean.TRUE);            
	        } catch(HospitalizacionException ex) {
	        	tieneFoco = getMaskFecha().getClientId();
				limpiarFecha();
				setTieneFoco(getMaskFecha().getClientId());
				getButtonCancelar().setDisabled(Boolean.TRUE);            
	            agregarMensajeError(ex);
	        }
	       
	       
	    }
	
	protected void limpiarFecha(){
		getMaskFecha().setValue("");
	}
	
//	public void validarEspecialidadEncabezado(){
//			
//		try{
//			capturaInterconsultaService.validarRequeridoEspecialidad(getEspecialidadSel());	
//			//especialidadDescripcion = especialidadSel.replaceAll("[0-9]","");
//			DatosCatalogo datosCatalogo = null;
//	        datosCatalogo = validaAutoComplete(getAutoEspecialidad(), 4, getCatalogoEspecialidadString(),
//	                getCatalogoEspecialidad());
//			setTieneFoco(getTextMatricula().getClientId());
//		}catch (HospitalizacionException e) {
//			agregarMensajeError(e);
//			setTieneFoco(getSelectEspecialidadHospitalizacion().getClientId());
//		}
//		
//	}
	
	public void validarMatricula() { 
		
		try {
			capturaInterconsultaService.validarMatricula( this.getTextMatricula().getValue().toString(), obtenerDatosUsuario().getCveDelegacionUmae());
	        if (getDatosHospitalizacion().getDatosMedico().getIdentificadorConexion()
	                .equals(IdentificadorConexionEnum.ROJO.getRuta())) {
	            agregarMensajeAdvertencia(getArray(MensajesAdvertenciaConstants.MA_013), null);
	        }			
	        setTieneFoco( selectTurno.getClientId() );
	        //if( getTextMedico().getValue() != null && getTextMedico().getValue().toString().isEmpty() ){
	        //	setTieneFoco(this.getTextMatricula().getClientId());
	        //}
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			limpiarMatricula();
			setTieneFoco(this.getTextMatricula().getClientId());
		}
		
	}
	
	protected void limpiarMatricula() {
		matricula = "";
		getTextMatricula().setValue("");
	}
	
	public void validarTurno()  throws HospitalizacionException{
		
		try{
			if (selectTurno.getValue() != null) {
			capturaInterconsultaService.validarRequeridoTurno(selectTurno.getValue().toString());			
			turnoDescripcion=selectTurno.getValue().toString();
			habilitarAgregar();
			setTieneFoco(getButtonGuardar().getClientId());
			}else {
				throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
						getArray(BaseConstants.REQUERIDO_TURNO));
			}
		}catch (HospitalizacionException e) {
			agregarMensajeError(e);
			setTieneFoco(selectTurno.getClientId());
		}
	}
	
	 
	
	
	public void habilitarAgregar(){
		String fecha = this.getMaskFecha().getValue().toString();		
		String matricula = this.getTextMatricula().getValue().toString();
		
		if( ( fecha == null || fecha.isEmpty() ) ||
			( matricula == null || matricula.isEmpty() ) ||
			( especialidadSel == null || especialidadSel.isEmpty() ) ||
			( selectTurno.getValue()== null || selectTurno.getValue().toString().isEmpty() ) ){
			getButtonGuardar().setDisabled(Boolean.TRUE);
		} else  {
			getButtonGuardar().setDisabled(Boolean.FALSE);
		}
	}
	
	public void limpiar(){
		
		this.getMaskFecha().setValue("");
		getDatosHospitalizacion().getDatosMedico().setMatricula("");
        getDatosHospitalizacion().getDatosMedico().setNombreCompleto("");
        getDatosHospitalizacion().getDatosMedico().setIdentificadorConexion(IdentificadorConexionEnum.ROJO.getRuta());
        selectTurno.resetValue();
        getAutoEspecialidad().setValue("");
        this.getSelectEspecialidadHospitalizacion().setValue("");
        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);
		setTieneFoco(getMaskFecha().getClientId());
	}
	
    /**
     * Metodo que realiza el proceso de guardar el encabezado de la interconsulta
     * - Valida que los datos ingresados (fecha, clave de la especialidad, matricula del medico y la clave 
     *   del turno) en la pantalla existan en la base de datos. Si existe se mostrara un modal en pantalla.
     *   Si no existe se guardara el registro en la base.
     */
    public void guardarEncabezado() {

        logger.info("Voy a guardar el encabezado de la interconsulta...");
        RequestContext context = RequestContext.getCurrentInstance();
        try {
        	
        	EncabezadoInterconsulta encabezado = consultarEncabezado();        	
        	if( encabezado != null ){        		
        		this.encabezadoGuardado = false;
        		claveInterconsulta = encabezado.getClaveInterconsulta();
        		setTieneFoco(getBotonModalSi().getClientId());
        		context.execute("PF('dlgEncabezadoInterconsultaExiste').show();");
        	} else {        		
        		capturaInterconsultaService.guardarEncabezado( obtenerDatosEncabezado() );
        		this.encabezadoGuardado = true;
        		encabezado = consultarEncabezado();  
        		claveInterconsulta = encabezado.getClaveInterconsulta();
        		setTieneFoco(this.getTextNss().getClientId());
        		contadorInterconsulta = 1L;
        	}        	
            
        } catch (HospitalizacionException e) {
            logger.error("Erro al buscarr la cabecera para la interconsulta: " + e.getMessage());
            agregarMensajeError(e);
        } catch (NumberFormatException e) {
        	 logger.error("Erro en el formateo del numero: " + e.getMessage());        	 
		} catch (ParseException e) {
			 logger.error("Erro en el parseo de la fecha: " + e.getMessage());			 
		}

    }
    
    protected EncabezadoInterconsulta consultarEncabezado() {
    	EncabezadoInterconsulta encabezado = new EncabezadoInterconsulta();
    	try {
			encabezado = capturaInterconsultaService.buscarEncabezadoInterconsulta( this.getMaskFecha().getValue().toString(), 
					especialidadSel.replaceAll("[^\\d.]", ""),
				    this.getTextMatricula().getValue().toString(), 
				    new Integer( selectTurno.getValue().toString().replaceAll("[^\\d.]", "") ) );
			return encabezado;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (HospitalizacionException e) {
			e.printStackTrace();
		}
		return encabezado;
    }
    
	public void guardarDetalle(){
		
        try {
        	
			EncabezadoInterconsulta encabezado = obtenerDatosEncabezado();
			
			if(getDatosHospitalizacion().getDatosPaciente().getCvePaciente() == null || 
					getDatosHospitalizacion().getDatosPaciente().getClavePresupuestal().isEmpty()) {
				cvePaciente = capturaInterconsultaService.insertarPacienteInterconsulta(datosHospitalizacion.getDatosPaciente());
			}
			
			AtencionInterconsulta atencionInteronsulta = obtenerDetalleInterGuardar();				
			capturaInterconsultaService.insertarDetalleInterconsulta(encabezado, atencionInteronsulta);	
			 logger.error("LA INSERCION FUE EXITOSA: ");
			 agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
			 
					Long consecutivoInterconsultas = capturaInterconsultaService.obtenerConsecutivoInterconsultas(claveInterconsulta);
					if( consecutivoInterconsultas == null ){
						contadorInterconsulta = 1L;
					} else {
						contadorInterconsulta = consecutivoInterconsultas + 1L;
					}
					setTieneFoco(getTextNss().getClientId());
					
				
        	limpiarDetalle();
        } catch (HospitalizacionException e) {
        	logger.error("Error al gurdar la interconsulta: " + e.getMessage());
            agregarMensajeError(e);       	 
        } catch (NumberFormatException e) {
        	 logger.error("Erro en el formateo del numero: " + e.getMessage());        	 
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	protected AtencionInterconsulta obtenerDetalleInterGuardar() throws HospitalizacionException{
		
		AtencionInterconsulta atencionInterconsulta = new AtencionInterconsulta();

		atencionInterconsulta.setCveCie10Adicional(diagnosticoAdicional != null ? diagnosticoAdicional.toUpperCase():StringUtils.EMPTY);
		atencionInterconsulta.setCveCie10Complementario(diagnosticoComplementario != null ? diagnosticoComplementario.toUpperCase():StringUtils.EMPTY);
		atencionInterconsulta.setCveCie10Principal(diagnosticoPrincipal.toUpperCase());
		atencionInterconsulta.setCvePaciente( cvePaciente != 0L ? cvePaciente : getDatosHospitalizacion().getDatosPaciente().getCvePaciente().intValue());
		atencionInterconsulta.setEspecialidadSolicita(especialidadSolic.replaceAll("[^\\d.]", ""));
		atencionInterconsulta.setIndAlta(Integer.parseInt(altaIntercon));
		atencionInterconsulta.setIndPVCie10Adicional(diagAdicPrimVez!=null&&!StringUtils.isEmpty(diagAdicPrimVez)?Integer.parseInt(diagAdicPrimVez):null);
		atencionInterconsulta.setIndPVCie10Complementario(diagComplPrimVez!=null&&!StringUtils.isEmpty(diagComplPrimVez)?Integer.parseInt(diagComplPrimVez):null);
		atencionInterconsulta.setIndPVCie10Principal(Integer.parseInt(getPrimVezSel()));
		atencionInterconsulta.setNumInterconsulta(contadorInterconsulta.intValue());
		atencionInterconsulta.setTimDuracion(duracionInter);
		atencionInterconsulta.setCveInterconsulta(claveInterconsulta);
		atencionInterconsulta.setClaveCapturista( getObjetosSs().getPersonalOperativo().getRefCuentaActiveD());
		atencionInterconsulta.setCveCie9Principal(procedimientoPrincipal!=null?procedimientoPrincipal.toUpperCase():null);
		atencionInterconsulta.setCveCie9Adicional(procedimientoAdicional!=null?procedimientoAdicional.toUpperCase():null);
		atencionInterconsulta.setCveCie9Complementario(procedimientoComplementario!=null?procedimientoComplementario.toUpperCase():null);
		return atencionInterconsulta;
	}
    
    /**
     * Boton SI de la ventana modal en el que se 
     */
    public void botonSIAgregarEncabezado(){
    	this.encabezadoGuardado = true;
    	try {
			Long consecutivoInterconsultas = capturaInterconsultaService.obtenerConsecutivoInterconsultas(claveInterconsulta);
			if( consecutivoInterconsultas == null ){
				contadorInterconsulta = 1L;
			} else {
				contadorInterconsulta = consecutivoInterconsultas + 1L;
			}
			setTieneFoco(getTextNss().getClientId());
			
		} catch (HospitalizacionException e) {
            logger.error("Erro al obtener el conteo de las interconsultas: " + e.getMessage());
            agregarMensajeError(e);
        } 
    }
    
    private Date stringToDate( String fecha ) throws ParseException{
    	
    	SimpleDateFormat formatter = new SimpleDateFormat(BaseConstants.FORMATO_FECHA);
        Date dates = null;
        return dates = formatter.parse(fecha);
       
    }
    
    private EncabezadoInterconsulta obtenerDatosEncabezado() throws ParseException{
    	EncabezadoInterconsulta encabezado = new EncabezadoInterconsulta();
		encabezado.setClaveEspecialidad( this.especialidadSel.replaceAll("[^\\d.]", ""));
		encabezado.setClaveMedico( this.getTextMatricula().getValue().toString());
		encabezado.setClaveTurno( new Integer( selectTurno.getValue().toString().replaceAll("[^\\d.]", "") ));
		encabezado.setFechaInterconsulta( stringToDate(this.getMaskFecha().getValue().toString()) );
		encabezado.setFechaCreacion( new Date());
		encabezado.setClavePresupuestal( obtenerDatosUsuario().getCvePresupuestal());
		encabezado.setClaveTipoFormato( HOSPITALIZACION );
		encabezado.setClaveCapturista( getObjetosSs().getPersonalOperativo().getRefCuentaActiveD());
		return encabezado;
    }
	
    public void salir() throws IOException {
    	getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
    	Properties properties = new Properties();
    	properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
    	String url = properties.getProperty(BaseConstants.CVE_URL_CE);
    	FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }
	
	//Fin Encabezado
	
	
	
	//I N I C I A    D E T A L L E  
	
	
	public void validarNSS() {
		try {
			capturaInterconsultaService.validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
			datosHospitalizacion.getDatosPaciente()
				.setNss(getDatosHospitalizacion().getDatosPaciente().getNss());
			setTieneFoco( getTextAgregadoMedico().getClientId());
			buttonCancelarDetalle.setDisabled(Boolean.FALSE);
		} catch (HospitalizacionException e) {
			agregarMensajeError(e);
			getDatosHospitalizacion().getDatosPaciente().setNss(BaseConstants.CADENA_VACIA);			
			setTieneFoco(getTextNss().getClientId());
			limpiarDetalleSinNSS();
		}
		
	}
		
	public void validarAgregadoMedico(){
		
		try{			
			Integer sexoP=0;
			super.validarAgregadoMedico();
			setPacientes(getHospitalizacionCommonServices().buscarPaciente(getDatosHospitalizacion().getDatosPaciente(),
                    new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getFechaActual())));
			EncabezadoInterconsulta encabezado;
			encabezado = obtenerDatosEncabezado();

			  if (getPacientes().size() < 2) {
	                getDatosHospitalizacion().setDatosPaciente(getPacientes().get(0));
	               
	                if (getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
	                        .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
	                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())
	                        || getDatosHospitalizacion().getDatosPaciente().getNombre().isEmpty()) {
	                	
	                	
	                    // Derechohabiente no encontrado / No derechohabiente
	                   habilitarCamposNoDerechohabiente();
	                   sexoP=SexoEnum.parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                        .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                                .getClave();
	                   capturaInterconsultaService.validarEspecialidadPermitidaPaciente(encabezado.getClaveEspecialidad(),
	                		   getDatosHospitalizacion().getDatosPaciente().getEdadAnios(),getDatosHospitalizacion().getDatosPaciente().getEdadSemanas(),sexoP);
	                   limpiarCatalogos();
	                   habilitarCamposNoEncontradoNoDerechohabiente();
	                } else {
	                	sexoP=SexoEnum.parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                        .getClave();
	                	
	                	 capturaInterconsultaService.validarEspecialidadPermitidaPaciente(encabezado.getClaveEspecialidad(),
	                			 getPacientes().get(0).getEdadAnios(),getPacientes().get(0).getEdadSemanas(),sexoP);
	                    // Derechohabiente encontrado
	                    deshabilitarCamposDerechohabiente();
	                    datosHospitalizacion.getDatosPaciente().setColorIdentificador(IdentificadorConexionEnum.VERDE.getRuta());
	                    habilitarEdad();
	                    setDelegacion(getDatosHospitalizacion().getDatosPaciente().getDelegacion());
	                    List<DatosCatalogo> delegaciones = getCatalogosHospitalizacionServices()
	                            .obtenerCatalogoDelegacion(getDatosHospitalizacion().getDatosPaciente().getNumero());
	                    obtenerDelegacion(delegaciones);
	                    setTieneFoco(getTextDuracionIntercon().getClientId());
	                    
	                }
	                getDatosHospitalizacion().getDatosPaciente()
	                        .setSexo(SexoEnum.parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
	                                        .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
	                                .getClave());
	            } 
	                else {
	                	sexoP=SexoEnum.parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                        .getClave();
	                	capturaInterconsultaService.validarEspecialidadPermitidaPaciente(encabezado.getClaveEspecialidad(),
		                		   getDatosHospitalizacion().getDatosPaciente().getEdadAnios(),getDatosHospitalizacion().getDatosPaciente().getEdadSemanas(),sexoP);
	                setNumeroPaciente(null);
	                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PACIENTES);
	                setTieneFoco(getTextNumeroPaciente().getClientId());
	            }
	      
			
		}catch (HospitalizacionException e){
			logger.error("Erro al validar el agregado medico: " + e.getMessage());
			limpiarDetalleSinNSS();
			agregarMensajeError(e);
			setTieneFoco( getTextAgregadoMedico().getClientId() );
		}catch (ParseException ex) {
			logger.error("Erro al validar el agregado medico: " + ex.getMessage());
			limpiarDetalleSinNSS();		
			setTieneFoco( getTextAgregadoMedico().getClientId() );
		}
	}
	
	
	public void mostrarPacienteSeleccionado() {

        if (getNumeroPaciente() == null || getNumeroPaciente() > getPacientes().size()) {
            setNumeroPaciente(null);
            setTieneFoco(getTextNumeroPaciente().getClientId());
        } else if (getNumeroPaciente() == 0) {
        	habilitarPacienteFueraDeNucleoFam();
        } else {
            try {
                getDatosHospitalizacion().setDatosPaciente(getPacientes().get(getNumeroPaciente() - 1));
                deshabilitarCamposDerechohabiente();
                habilitarEdad();
                setDelegacion(getDatosHospitalizacion().getDatosPaciente().getDelegacion());
                List<DatosCatalogo> delegaciones = catalogosHospitalizacionServices
                        .obtenerCatalogoDelegacion(getDatosHospitalizacion().getDatosPaciente().getNumero());
                obtenerDelegacion(delegaciones);
                getDatosHospitalizacion().getDatosPaciente()
                        .setSexo(SexoEnum
                                .parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                        .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                                .getClave());
                
//                setPacientes(new ArrayList<DatosPaciente>());
//                setNumeroPaciente(null);
                RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES);
                setTieneFoco(getTextDuracionIntercon().getClientId());
            } catch (HospitalizacionException e) {
                DatosPaciente pacienteTemp = new DatosPaciente();
                pacienteTemp.setNss(getDatosHospitalizacion().getDatosPaciente().getNss());
                getDatosHospitalizacion().setDatosPaciente(pacienteTemp);
                setNumeroPaciente(null);
                agregarMensajeError(e);
                setTieneFoco(getTextNumeroPaciente().getClientId());
            }
        }
    }
	
	public void obtenerDelegacion(List<DatosCatalogo> delegaciones) {

        for (DatosCatalogo del : delegaciones) {
            if (del.getClave().equals(getDelegacion())) {
                getDatosHospitalizacion().getDatosPaciente()
                        .setDelegacion(del.getClave() + BaseConstants.ESPACIO + del.getDescripcion());
            }
        }
    }
	
	public void validarEdad(){
	
	    try {
            
            capturaInterconsultaService.validarEdad(getEdad());
            getDatosHospitalizacion().getDatosPaciente().setEdadSemanas(Integer.valueOf(getEdad()));
            getDatosHospitalizacion().getDatosPaciente()
                    .setEdadAnios(Integer.valueOf(getEdad()) / BaseConstants.SEMANAS_ANIO);
            setTieneFoco(getTextNombre().getClientId());
        } catch (HospitalizacionException e) {
            setEdad(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            setTieneFoco(getTextEdad().getClientId());
        }
	}
	
	public void validarDuracionIntercon(){
		try{
			capturaInterconsultaService.validarRequeridoDuracionIntercon(getDuracionInter());
			setTieneFoco(getSelectPrimVez().getClientId());
		}catch (HospitalizacionException e) {
			agregarMensajeError(e);
			setTieneFoco(getTextDuracionIntercon().getClientId());
		}
		
	}
	
	public void validarPrimeraVez(){
		
		try{	
			capturaInterconsultaService.validarRequeridoPrimVez(getPrimVezSel());
			setTieneFoco(autoEspecialidadSolic.getClientId());
		}catch (HospitalizacionException e){
			agregarMensajeError(e);
			setTieneFoco(getSelectPrimVez().getClientId());
		}
			
	}
		
	public void validarEspecialidadSolicitada(){
		
		try{
			capturaInterconsultaService.validarRequeridoEspecialidadSolic(getEspecialidadSolic());
			setTieneFoco(getSelectAltaIntercon().getClientId());
		}catch (HospitalizacionException e){
			agregarMensajeError(e);
			setTieneFoco(autoEspecialidadSolic.getClientId());
		}
	}
		
	public void validarAltaInterconsulta(){
		
		try{
			capturaInterconsultaService.validarRequeridoAltaIntercon(getAltaIntercon());
			setTieneFoco(getTextDiagnosticoPrincipal().getClientId());
		}catch (HospitalizacionException e){
			agregarMensajeError(e);
			setTieneFoco(getSelectAltaIntercon().getClientId());
		}
	}
	
	
	 //DIAGNOSTICOS Y PROCEDIMIENTOS
	 
	public void validarDiagnosticoPrincipal() {

		try {
			getHospitalizacionCommonServices().validarDiagnosticos(getDiagnosticoPrincipal(),
					getDatosHospitalizacion().getDatosPaciente(), Boolean.TRUE,
					BaseConstants.REQUERIDO_DIAGNOSTICO_PRINCIPAL_INTERCON);
			
			capturaInterconsultaService.validarDiagnosticoPorViolencia(getDiagnosticoPrincipal());

			if (getHospitalizacionCommonServices().validarDiagnosticoEpidemiologica(getDiagnosticoPrincipal())) {
				setTipoDiagnostico(TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave());
				getTextDiagnosticoAdicional().setDisabled(Boolean.FALSE);
				RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
				setTieneFoco(getConfirmSi().getClientId());
			} else {
				
				setDiagPrincipalDesc(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(
						getHospitalizacionCommonServices().buscarDiagnostico(getDiagnosticoPrincipal())));
				getTextDiagnosticoAdicional().setDisabled(Boolean.FALSE);
				setTieneFoco(getTextDiagnosticoAdicional().getClientId());
				habilitarGuardarDetalle();
				
			} 

		} catch (HospitalizacionException e) {
			setDiagnosticoPrincipal(BaseConstants.CADENA_VACIA);
			agregarMensajeError(e);
			getDiagPrincipalDesc().setDescripcion(StringUtils.EMPTY);
			setTieneFoco(getTextDiagnosticoPrincipal().getClientId());

		}
	}
	
	public void validarDiagnosticoAdicional(){
		
		try{
			
			if( diagnosticoAdicional !=null && !StringUtils.isEmpty(diagnosticoAdicional)) {
				getHospitalizacionCommonServices().validarDiagnosticos(getDiagnosticoAdicional(),
						getDatosHospitalizacion().getDatosPaciente(), Boolean.TRUE,
						BaseConstants.REQUERIDO_DIAGNOSTICO_ADICIONAL_INTERCON);
				
				capturaInterconsultaService.validarDiagnosticoRepetido(getDiagnosticoPrincipal(), getDiagnosticoAdicional(), getDiagnosticoComplementario());
				
				if (getHospitalizacionCommonServices().validarDiagnosticoEpidemiologica(getDiagnosticoAdicional())) {
		            setTipoDiagnostico(TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave());
		            RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
		            setTieneFoco(getConfirmSi().getClientId());
		        } else {
		            setDiagnosticoAdicionalDesc(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(getHospitalizacionCommonServices()
		                    .buscarDiagnostico(getDiagnosticoAdicional())));
		            getSelectPVezDiagAdicional().setDisabled(Boolean.FALSE);
		            getTextDiagnosticoComplementario().setDisabled(Boolean.FALSE);
		            setTieneFoco(getSelectPVezDiagAdicional().getClientId());
		        }
			}else {
				setDiagnosticoAdicionalDesc(null);
				getSelectPVezDiagAdicional().setValue(StringUtils.EMPTY);
				getSelectPVezDiagAdicional().setDisabled(Boolean.TRUE);
				getTextDiagnosticoComplementario().setDisabled(Boolean.TRUE);
				getTextProcedimientoPrincipal().setDisabled(Boolean.FALSE);
				setTieneFoco(getTextProcedimientoPrincipal().getClientId());
			}
						
		}catch (HospitalizacionException e){
			agregarMensajeError(e);
			getDiagnosticoAdicionalDesc().setDescripcion(StringUtils.EMPTY);
			getSelectPVezDiagAdicional().setValue(StringUtils.EMPTY);
			setDiagnosticoAdicional(BaseConstants.CADENA_VACIA);
			setTieneFoco(getTextDiagnosticoAdicional().getClientId());
		}
		
	}
	
	public void validarPrimVezDiagAdicional(){
		
		try{			
			capturaInterconsultaService.validarRequeridoPrimVez(getDiagAdicPrimVez());
			getTextDiagnosticoComplementario().setDisabled(Boolean.FALSE);
			setTieneFoco(getTextDiagnosticoComplementario().getClientId());
			
		}catch (HospitalizacionException e){
			agregarMensajeError(e);
			setTieneFoco(getSelectPVezDiagAdicional().getClientId());
		}
		
	}
	
	
	public void validarDiagnosticoComplementario(){
		
		try{
			
			if(diagnosticoComplementario != null && !StringUtils.isEmpty(diagnosticoComplementario)) {
				getHospitalizacionCommonServices().validarDiagnosticos(getDiagnosticoComplementario(),
						getDatosHospitalizacion().getDatosPaciente(), Boolean.TRUE,
						BaseConstants.REQUERIDO_DIAGNOSTICO_COMPLEMENTARIO_INTERCON);
				
				capturaInterconsultaService.validarDiagnosticoRepetido(getDiagnosticoPrincipal(), getDiagnosticoAdicional(), getDiagnosticoComplementario());
				
				if (getHospitalizacionCommonServices().validarDiagnosticoEpidemiologica(getDiagnosticoComplementario())) {
		            setTipoDiagnostico(TipoDiagnostioEnum.SEGUNDO_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave());
		            RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.ABRIR_CONFIRM_DIAGNOSTICO);
		            setTieneFoco(getConfirmSi().getClientId());
		        } else {
		            setDiagnosticoComplementarioDesc(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(getHospitalizacionCommonServices()
		                    .buscarDiagnostico(getDiagnosticoComplementario())));
		            getSelectPVezDiagComplement().setDisabled(Boolean.FALSE);
		            setTieneFoco(getSelectPVezDiagComplement().getClientId());
		        }
			}else {
				selectPVezDiagComplement.setValue(null);
				selectPVezDiagComplement.setDisabled(Boolean.TRUE);
				getTextProcedimientoPrincipal().setDisabled(Boolean.FALSE);
				setTieneFoco(getTextProcedimientoPrincipal().getClientId());
			}
			
		}catch (HospitalizacionException e){
			agregarMensajeError(e);
			setDiagnosticoComplementario(BaseConstants.CADENA_VACIA);
			diagnosticoComplementario = BaseConstants.CADENA_VACIA;
			setTieneFoco(getTextDiagnosticoComplementario().getClientId());
			
		}
		
	}
	
	public void validarPrimVezDiagComplementario(){
		
		try{			
			capturaInterconsultaService.validarRequeridoPrimVez(getDiagComplPrimVez());
			getTextProcedimientoPrincipal().setDisabled(Boolean.FALSE);
			setTieneFoco(getTextProcedimientoPrincipal().getClientId());
			
		}catch (HospitalizacionException e){
			agregarMensajeError(e);
			setTieneFoco(getSelectPVezDiagComplement().getClientId());
		}
		
	}
		
	protected void validarFocoConfirm() {
		
		if(getTextDiagnosticoComplementario().getValue() != null && !StringUtils.isEmpty(getTextDiagnosticoComplementario().getValue().toString())) {
			getSelectPVezDiagComplement().setDisabled(Boolean.FALSE);
			setTieneFoco(getSelectPVezDiagComplement().getClientId());
			return;
		}			
		
		if(getTextDiagnosticoAdicional().getValue()!=null && !StringUtils.isEmpty(getTextDiagnosticoAdicional().getValue().toString())) {
			getSelectPVezDiagAdicional().setDisabled(Boolean.FALSE);
			setTieneFoco(getSelectPVezDiagAdicional().getClientId());
			return;
		} 
		
		if(getTextDiagnosticoPrincipal().getValue() != null && !StringUtils.isEmpty(getTextDiagnosticoPrincipal().getValue().toString())) {
			getTextDiagnosticoAdicional().setDisabled(Boolean.FALSE);
			setTieneFoco(getTextDiagnosticoAdicional().getClientId());
		}
	}
	
	public void confirmAcepto(){

		if (TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave().equals(getTipoDiagnostico())) {
			setDiagPrincipalDesc(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(getHospitalizacionCommonServices()
					.buscarDiagnostico(getDiagnosticoPrincipal())));
		}else if(TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave().equals(getTipoDiagnostico())) {
			setDiagnosticoAdicionalDesc(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(getHospitalizacionCommonServices()
					.buscarDiagnostico(getDiagnosticoAdicional())));			
		}else {
			setDiagnosticoComplementarioDesc(catalogosHospitalizacionServices.obtenerCatalogoDiagnostico(getHospitalizacionCommonServices()
					.buscarDiagnostico(getDiagnosticoComplementario())));	
		}
		validarFocoConfirm();
		RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.CERRAR_CONFIRM_DIAGNOSTICO);
		RequestContext.getCurrentInstance().reset("interconsultaForm:idSi");
		RequestContext.getCurrentInstance().reset("interconsultaForm:idNo");
		habilitarGuardarDetalle();

	}	
		 
	public void confirmRechazo(){
		
		if (TipoDiagnostioEnum.DIAGNOSTICO_PRINCIPAL_EGRESO.getClave().equals(getTipoDiagnostico())) {
			diagPrincipalDesc = new DatosCatalogo();
            setDiagnosticoPrincipal(BaseConstants.CADENA_VACIA);
            setTieneFoco(getTextDiagnosticoPrincipal().getClientId());
	}else if(TipoDiagnostioEnum.PRIMER_DIAGNOSTICO_SECUNDARIO_EGRESO.getClave().equals(getTipoDiagnostico())) {
		  setTieneFoco(getTextDiagnosticoAdicional().getClientId());
		  setDiagnosticoAdicional(BaseConstants.CADENA_VACIA);
	}else {
		setTieneFoco(getTextDiagnosticoComplementario().getClientId());
		setDiagnosticoComplementario(BaseConstants.CADENA_VACIA);
	}
		 RequestContext.getCurrentInstance().execute(CapturarEgresoConstants.CERRAR_CONFIRM_DIAGNOSTICO);
	        RequestContext.getCurrentInstance().reset("interconsultaForm:idSi");
	        RequestContext.getCurrentInstance().reset("interconsultaForm:idNo");
	       
	}
	
	public void validarProcedimientoPrincipal(){
		
		try{
			if(getProcedimientoPrincipal()!=null && !StringUtils.isEmpty(getProcedimientoPrincipal())) {
				getCapturaInterconsultaService().validarProcedimientos(getProcedimientoPrincipal(),
						getDatosHospitalizacion().getDatosPaciente(), Boolean.TRUE);
				
				capturaInterconsultaService.validarDiagnosticoRepetido(procedimientoPrincipal, procedimientoAdicional, procedimientoComplementario);
		            
				setProcedPrincipalDesc(capturaInterconsultaService.buscarProcedimiento(getProcedimientoPrincipal()));
				getTextProcedimientoAdicional().setDisabled(Boolean.FALSE);
				setTieneFoco(getTextProcedimientoAdicional().getClientId());
			}else {
				getProcedPrincipalDesc().setDesCie9(StringUtils.EMPTY);
				setTieneFoco(getButtonGuardarDetalle().getClientId());
			}
			
		}catch (HospitalizacionException e){
			agregarMensajeError(e);
			setProcedimientoPrincipal(BaseConstants.CADENA_VACIA);
			setTieneFoco(getTextProcedimientoPrincipal().getClientId());
			
		}		
	}
	
	public void validarProcedimientoAdicional() {

		try {
			if(getProcedimientoAdicional()!=null && !StringUtils.isEmpty(getProcedimientoAdicional())) {
				getCapturaInterconsultaService().validarProcedimientos(getProcedimientoAdicional(),
						getDatosHospitalizacion().getDatosPaciente(), Boolean.TRUE);
				
				capturaInterconsultaService.validarDiagnosticoRepetido(procedimientoPrincipal, procedimientoAdicional, procedimientoComplementario);
				
				setProcedAdicionalDesc(capturaInterconsultaService.buscarProcedimiento(getProcedimientoAdicional()));
				getTextProcedimientoComplementario().setDisabled(Boolean.FALSE);
				setTieneFoco(getTextProcedimientoComplementario().getClientId());
			}else {
				getProcedAdicionalDesc().setDesCie9(StringUtils.EMPTY);
				setTieneFoco(getButtonGuardarDetalle().getClientId());
			}
		} catch (HospitalizacionException e) {
			setProcedimientoAdicional(BaseConstants.CADENA_VACIA);
			agregarMensajeError(e);
			setTieneFoco(getTextProcedimientoAdicional().getClientId());

		}
	}
	
	public void validarProcedimientoComplementario() {

		try {
			if(getProcedimientoComplementario()!=null && !StringUtils.isEmpty(getProcedimientoComplementario())) {
				getCapturaInterconsultaService().validarProcedimientos(getProcedimientoComplementario(),
						getDatosHospitalizacion().getDatosPaciente(), Boolean.TRUE);
	
				capturaInterconsultaService.validarDiagnosticoRepetido(procedimientoPrincipal, procedimientoAdicional, procedimientoComplementario);
				
				setProcedComplemtDesc(capturaInterconsultaService.buscarProcedimiento(getProcedimientoComplementario()));
				setTieneFoco(getButtonGuardarDetalle().getClientId());
			}else {
				getProcedComplemtDesc().setDesCie9(StringUtils.EMPTY);
				setTieneFoco(getButtonGuardarDetalle().getClientId());
			}
		} catch (HospitalizacionException e) {
			setProcedimientoComplementario(BaseConstants.CADENA_VACIA);
			agregarMensajeError(e);
			setTieneFoco(getTextProcedimientoComplementario().getClientId());

		}
	}
	
	protected void limpiarDetalleSinNSS() {

		getDatosHospitalizacion().getDatosPaciente().setColorIdentificador(IdentificadorConexionEnum.ROJO.getRuta());
		getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
		edad = BaseConstants.CADENA_VACIA;
		textDuracionIntercon.setValue(null);
		selectPrimVez.setValue("");
		autoEspecialidadSolic.setValue("");
		selectAltaIntercon.setValue("");
		diagnosticoPrincipal = BaseConstants.CADENA_VACIA;
		diagnosticoAdicional = BaseConstants.CADENA_VACIA;
		diagnosticoComplementario = BaseConstants.CADENA_VACIA;
		diagPrincipalDesc = new DatosCatalogo(); 
		diagnosticoAdicionalDesc = new DatosCatalogo();
		diagnosticoComplementarioDesc = new DatosCatalogo();
		procedimientoPrincipal = BaseConstants.CADENA_VACIA;
		procedimientoAdicional = BaseConstants.CADENA_VACIA;
		procedimientoComplementario = BaseConstants.CADENA_VACIA;
		procedPrincipalDesc = new Procedimiento();
		procedAdicionalDesc = new Procedimiento();
		procedComplemtDesc = new Procedimiento();
		selectPVezDiagAdicional.setValue("");
		selectPVezDiagComplement.setValue("");
		deshabilitarCamposDerechohabiente();
		deshabilitaCamposDiagYProced();
		buttonCancelarDetalle.setDisabled(Boolean.TRUE);
		buttonGuardarDetalle.setDisabled(Boolean.TRUE);
		setTieneFoco(getTextNss().getClientId());
	}
	
	public void limpiarDetalle(){
		getDatosHospitalizacion().getDatosPaciente().setNss("");
		getDatosHospitalizacion().getDatosPaciente().setColorIdentificador(IdentificadorConexionEnum.ROJO.getRuta());
        getDatosHospitalizacion().setDatosPaciente(new DatosPaciente());
        pacientes.clear();
        setNumeroPaciente(null);
        edad = BaseConstants.CADENA_VACIA;
        textDuracionIntercon.setValue(null);
        selectPrimVez.setValue("");
        autoEspecialidadSolic.setValue("");
        selectAltaIntercon.setValue("");
        diagnosticoPrincipal = BaseConstants.CADENA_VACIA;
        diagnosticoAdicional = BaseConstants.CADENA_VACIA;
        diagnosticoComplementario = BaseConstants.CADENA_VACIA;
        diagPrincipalDesc = new DatosCatalogo(); 
        diagnosticoAdicionalDesc = new DatosCatalogo();
        diagnosticoComplementarioDesc = new DatosCatalogo();
        procedimientoPrincipal = BaseConstants.CADENA_VACIA;
        procedimientoAdicional = BaseConstants.CADENA_VACIA;
        procedimientoComplementario = BaseConstants.CADENA_VACIA;
        procedPrincipalDesc = new Procedimiento();
        procedAdicionalDesc = new Procedimiento();
        procedComplemtDesc = new Procedimiento();
        selectPVezDiagAdicional.setValue("");
        selectPVezDiagComplement.setValue("");
        deshabilitarCamposDerechohabiente();
        deshabilitaCamposDiagYProced();
        buttonCancelarDetalle.setDisabled(Boolean.TRUE);
        buttonGuardarDetalle.setDisabled(Boolean.TRUE);
		setTieneFoco(getTextNss().getClientId());
	}
	
	
	public List<String> filtrarCatalogoEspecialidadEncabezado(String query) {
		
		//return capturaInterconsultaService.filtrarCatalogoEspecialidadEncabezado(query, especialidadesList);
		//return capturaInterconsultaService.filtrarCatalogoEspecialidadEncabezado(query, getCatalogoEspecialidad());
		return capturaInterconsultaService.filtrarCatalogo(getCatalogoEspecialidadString(), query,
                BaseConstants.LONGITUD_ESPECIALIDAD);
	}
	
	
	
	public List<String> filtrarCatalogoTurnoEncabezado(String query) {
		
		return capturaInterconsultaService.filtrarCatalogoTurnoEncabezado(query, catalogoTurnoList);
	}	
	
	public void habilitarGuardarDetalle(){
		
		String nss = this.getTextNombre().getValue().toString();
		String diagnosticoPrincipal = this.getTextDiagnosticoPrincipal().getValue().toString();
	
		if( ( nss == null || nss.isEmpty() ) ||
			( getDatosHospitalizacion().getDatosPaciente() == null) ||
			( duracionInter == null ) ||
			( primVezSel == null || primVezSel.isEmpty() ) ||
			( especialidadSolic == null || especialidadSolic.isEmpty() ) ||
			( altaIntercon == null || altaIntercon.isEmpty() ) ||
			( diagnosticoPrincipal == null || diagnosticoPrincipal.isEmpty() )){
			
			buttonGuardarDetalle.setDisabled(Boolean.TRUE);
		} else  {
			buttonGuardarDetalle.setDisabled(Boolean.FALSE);
		}
	}
	
	 public void validarEspecialidad() throws HospitalizacionException {
		 try {
	        DatosCatalogo datosCatalogo;
	        datosCatalogo = validaAutoComplete(getAutoEspecialidad(), BaseConstants.LONGITUD_ESPECIALIDAD,
	                getCatalogoEspecialidadString(), getCatalogoEspecialidad());
	        if (null != datosCatalogo) {
	        	
	        		datosHospitalizacion.getDatosIngreso().setEspecialidad(datosCatalogo.getClave());
		            datosHospitalizacion.getDatosIngreso().setDivision(capturaInterconsultaService
		                    .validarEspecialidadInterconsulta( datosHospitalizacion.getDatosIngreso().getEspecialidad(),
		                             getObjetosSs().getDatosUsuario().getCvePresupuestal())
		                    .toString());
		            if (getTipoUbicacion().equals(TipoUbicacionEnum.HOSPITALIZACION)) {
		                setCatalogoDivision(capturaInterconsultaService
		                        .obtenerCatalogoDivision(datosHospitalizacion.getDatosIngreso().getDivision()));
		                datosHospitalizacion.getDatosIngreso()
		                        .setTipoPrograma(datosHospitalizacion.getDatosIngreso().getEspecialidad()
		                                .equals(CapturarIngresosConstants.ESPECIALIDAD_CIRUGIA_AMBULATORIA)
		                                        ? TipoProgramaEnum.CIRUGIA_AMBULATORIA.getClave()
		                                        : TipoProgramaEnum.HOSPITAL.getClave());
//		                catalogoTipoPrograma = catalogosHospitalizacionServices.obtenerCatalogoTipoPrograma(
//		                        Integer.parseInt(datosHospitalizacion.getDatosIngreso().getTipoPrograma()));
		            }
		            setTieneFoco(getTextMatricula().getClientId());	     
	        } else {
	        	setTieneFoco(getAutoEspecialidad().getClientId());
	            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
	                    getArray(BaseConstants.REQUERIDO_ESPECIALIDAD));
	        }
		 } catch (HospitalizacionException e) {
				tieneFoco = getAutoEspecialidad().getClientId();
				getAutoEspecialidad().resetValue();		
				especialidadSel="";
	            agregarMensajeError(e);		            
			}
	    }
	 
	 
	 public void validarAutoEspecialidadSolicitada() throws HospitalizacionException {

	        DatosCatalogo datosCatalogo;
	        datosCatalogo = validaAutoComplete(getAutoEspecialidadSolic(), BaseConstants.LONGITUD_ESPECIALIDAD,
	                getCatalogoEspecialidadString(), getCatalogoEspecialidad());
	        try {
	        	if (null != datosCatalogo) {
		            datosHospitalizacion.getDatosIngreso().setEspecialidad(datosCatalogo.getClave());
		            datosHospitalizacion.getDatosIngreso().setDivision(capturaInterconsultaService
		                    .validarEspecialidad(getTipoUbicacion(), datosHospitalizacion.getDatosIngreso().getEspecialidad(),
		                            datosHospitalizacion.getDatosPaciente(), getObjetosSs().getDatosUsuario().getCvePresupuestal())
		                    .toString());
		            if (getTipoUbicacion().equals(TipoUbicacionEnum.HOSPITALIZACION)) {
		                setCatalogoDivision(capturaInterconsultaService
		                        .obtenerCatalogoDivision(datosHospitalizacion.getDatosIngreso().getDivision()));
		                datosHospitalizacion.getDatosIngreso()
		                        .setTipoPrograma(datosHospitalizacion.getDatosIngreso().getEspecialidad()
		                                .equals(CapturarIngresosConstants.ESPECIALIDAD_CIRUGIA_AMBULATORIA)
		                                        ? TipoProgramaEnum.CIRUGIA_AMBULATORIA.getClave()
		                                        : TipoProgramaEnum.HOSPITAL.getClave());
//		                catalogoTipoPrograma = catalogosHospitalizacionServices.obtenerCatalogoTipoPrograma(
//		                        Integer.parseInt(datosHospitalizacion.getDatosIngreso().getTipoPrograma()));
		            }
		            setTieneFoco(getSelectAltaIntercon().getClientId());
		           
		        } else {
		        	setTieneFoco(getAutoEspecialidadSolic().getClientId());
		            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
		                    getArray(BaseConstants.REQUERIDO_ESPECIALIDAD));
		        }
	        	
	        } catch (HospitalizacionException e) {
				tieneFoco = getAutoEspecialidadSolic().getClientId();
				getAutoEspecialidadSolic().resetValue();
				especialidadSolic="";
	            agregarMensajeError(e);		            
			}
	        
	    }

	 
 
	    public void validarEspecialidadHospitalizacion() throws HospitalizacionException {

	        DatosCatalogo datosCatalogo;
	        datosCatalogo = validaAutoComplete(getAutoEspecialidad(), BaseConstants.LONGITUD_ESPECIALIDAD,
	                getCatalogoEspecialidadHospitalizacionString(), getCatalogoEspecialidadHospitalizacion());
	        if (null != datosCatalogo) {
	            datosHospitalizacion.getDatosIngreso().setEspecialidadHospitalizacion(datosCatalogo.getClave());
	            capturaInterconsultaService.validarEspecialidad(TipoUbicacionEnum.HOSPITALIZACION,
	                    datosHospitalizacion.getDatosIngreso().getEspecialidadHospitalizacion(),
	                    datosHospitalizacion.getDatosPaciente(), getObjetosSs().getDatosUsuario().getCvePresupuestal());
	        } else {
	            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
	                    getArray(BaseConstants.REQUERIDO_ESPECIALIDAD_HOSPITALIZACION));
	        }
	    }
	    
	    
	    
	   

	 
 
	   
}
