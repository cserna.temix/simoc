package mx.gob.imss.simo.hospitalizacion.common.constant;

public enum TipoFechaEnum {

    MES("MES", 1), SEMANA("SEMANA", 2);

    private String clave;
    private int valor;

    private TipoFechaEnum(String clave, int valor) {
        this.clave = clave;
        this.valor = valor;
    }

    public String getClave() {

        return clave;
    }

    public void setClave(String clave) {

        this.clave = clave;
    }

    public int getValor() {

        return valor;
    }

    public void setValor(int valor) {

        this.valor = valor;
    }

    public static TipoFechaEnum parse(String clave) {

        TipoFechaEnum right = null;
        for (TipoFechaEnum item : values()) {
            if (item.getClave().equals(clave)) {
                right = item;
                break;
            }
        }
        return right;
    }

}
