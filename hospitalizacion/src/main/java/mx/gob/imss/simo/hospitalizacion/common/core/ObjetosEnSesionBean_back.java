package mx.gob.imss.simo.hospitalizacion.common.core;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.model.DatosUsuario;

@Scope("session")
@Component(value = "objetosSs_back")
public class ObjetosEnSesionBean_back implements Serializable {

    private static final long serialVersionUID = -298082286199949718L;

    private String colorHeader;
    private DatosUsuario datosUsuario;
    private boolean mostrarDialogoDelegaciones;
    private String periodoActualCex;
    private String periodoActualHospi;
    private String periodoActualInfoComplementaria;
    private String periodoActualSubrogados;

    public ObjetosEnSesionBean_back() {
        datosUsuario = new DatosUsuario();
    }

    public String getColorHeader() {

        return colorHeader;
    }

    public void setColorHeader(String colorHeader) {

        this.colorHeader = colorHeader;
    }

    public DatosUsuario getDatosUsuario() {

        return datosUsuario;
    }

    public void setDatosUsuario(DatosUsuario datosUsuario) {

        this.datosUsuario = datosUsuario;
    }

    public boolean getMostrarDialogoDelegaciones() {

        return mostrarDialogoDelegaciones;
    }

    public void setMostrarDialogoDelegaciones(boolean mostrarDialogoDelegaciones) {

        this.mostrarDialogoDelegaciones = mostrarDialogoDelegaciones;
    }

    public String getPeriodoActualCex() {

        return periodoActualCex;
    }

    public void setPeriodoActualCex(String periodoActualCex) {

        this.periodoActualCex = periodoActualCex;
    }

    public String getPeriodoActualHospi() {

        return periodoActualHospi;
    }

    public void setPeriodoActualHospi(String periodoActualHospi) {

        this.periodoActualHospi = periodoActualHospi;
    }

    public String getPeriodoActualInfoComplementaria() {

        return periodoActualInfoComplementaria;
    }

    public void setPeriodoActualInfoComplementaria(String periodoActualInfoComplementaria) {

        this.periodoActualInfoComplementaria = periodoActualInfoComplementaria;
    }

    public String getPeriodoActualSubrogados() {

        return periodoActualSubrogados;
    }

    public void setPeriodoActualSubrogados(String periodoActualSubrogados) {

        this.periodoActualSubrogados = periodoActualSubrogados;
    }

}
