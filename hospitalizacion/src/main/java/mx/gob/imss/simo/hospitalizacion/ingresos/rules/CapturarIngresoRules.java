package mx.gob.imss.simo.hospitalizacion.ingresos.rules;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesAdvertenciaConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoFormatoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngresoRN;
import mx.gob.imss.simo.hospitalizacion.common.model.Ingreso;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;

@Component
public class CapturarIngresoRules extends HospitalizacionCommonRules {

    public Ingreso validarIngresoVigente(List<Ingreso> ingresos, Long cvePaciente, Long cvePacientePadre, boolean uci,
            TipoUbicacionEnum tipoUbicacion, boolean noDerechohabiente) throws HospitalizacionException {

        Long pacientePadre = null == cvePacientePadre ? 0 : cvePacientePadre;
        Ingreso ingreso = null;
        if (ingresos != null && !ingresos.isEmpty()) {
            for (Ingreso in : ingresos) {
                if ((cvePaciente != null && in.getCvePaciente() == cvePaciente)
                        || (pacientePadre != null && in.getCvePaciente() == pacientePadre)
                        || (noDerechohabiente && ingresos.size() == 1)) {
                    ingreso = in;
                    break;
                }
            }
            if (ingreso != null && !uci) {
                if (ingreso.getCveTipoFormato() == TipoFormatoEnum.INGRESO_URGENCIAS.getClave()
                        && tipoUbicacion.equals(TipoUbicacionEnum.UCI)) {
                    throw new HospitalizacionException(MensajesErrorConstants.ME_161);
                } else {
                    String fechaIngreso = new SimpleDateFormat(BaseConstants.FORMATO_FECHA)
                            .format(ingreso.getFecIngreso());
                    throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_047), getArray(fechaIngreso));
                }
            }
        }
        return ingreso;
    }
    
    public Ingreso validarIngresoVigenteRN(List<Ingreso> ingresos) throws HospitalizacionException {

    	Ingreso ingreso = null;
        if (ingresos != null && !ingresos.isEmpty()) {
            for (Ingreso in : ingresos) {
            	  if(in.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave()
                		    || in.getCveTipoFormato() == TipoFormatoEnum.INGRESO_RN_UCIN.getClave()){
                    ingreso = in;
                    break;
                }
            }
            if (ingreso != null) {
                    String fechaIngreso = new SimpleDateFormat(BaseConstants.FORMATO_FECHA)
                            .format(ingreso.getFecIngreso());
                    throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_047), getArray(fechaIngreso));
            }
        }
        return ingreso;
    }

    public boolean habilitarEdad(String fechaIngreso, String anio) {

        int anioAgregado = Integer.parseInt(anio);
        int anioIngreso = Integer.parseInt(fechaIngreso.substring(6));
        if (anioAgregado == anioIngreso || (anioAgregado == (anioIngreso - 1))) {
            return true;
        }
        return false;
    }

    public void validarEdad(String edad) throws HospitalizacionException {

        if (edad == null || edad.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_EDAD));
        }
        if (Integer.valueOf(edad) < 0 || Integer.valueOf(edad) > 103) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_138);
        }
    }

    public List<DatosCatalogo> filtrarCatalogoTipoIngreso(List<DatosCatalogo> tiposIngreso, boolean ingresoVigente) {

        List<DatosCatalogo> tipos = new ArrayList<DatosCatalogo>();
        for (DatosCatalogo tipo : tiposIngreso) {
            if (ingresoVigente) {
                if (tipo.getClave().equals(CapturarIngresosConstants.TIPO_INGRESO_HOSPITAL)) {
                    tipos.add(tipo);
                }
            } else {
                if (!tipo.getClave().equals(CapturarIngresosConstants.TIPO_INGRESO_HOSPITAL)) {
                    tipos.add(tipo);
                }
            }
        }
        return tipos;
    }

    public void validarTipoIngreso(String tipoIngreso, List<DatosCatalogo> catalogoTipos)
            throws HospitalizacionException {

        if (tipoIngreso == null || tipoIngreso.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_TIPO_INGRESO));
        }
        List<String> tipos = new ArrayList<>();
        for (DatosCatalogo tipo : catalogoTipos) {
            tipos.add(tipo.getClave());
        }
        if (!tipos.contains(tipoIngreso)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_TIPO_INGRESO));
        }
    }

    public void validarTipoPrograma(String tipoPrograma, List<DatosCatalogo> catalogoTipos)
            throws HospitalizacionException {

        if (tipoPrograma == null || tipoPrograma.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_TIPO_PROGRAMA));
        }
        List<String> tipos = new ArrayList<>();
        for (DatosCatalogo tipo : catalogoTipos) {
            tipos.add(tipo.getClave());
        }
        if (!tipos.contains(tipoPrograma)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_TIPO_PROGRAMA));
        }
    }

    public boolean esPeriodoExtemporaneo(PeriodosImss periodoActivo, PeriodosImss periodoAnterior, PeriodosImss periodoMonitoreo, Date fechaCaptura)
            throws HospitalizacionException {

        boolean extemporaneo = false;
        if (fechaCaptura.before(periodoActivo.getFechaInicial())) {
            if (fechaCaptura.before(periodoAnterior.getFechaInicial())) {
            	//
            	if (periodoMonitoreo == null || !validarFecCapturaMonitoreo (periodoMonitoreo, fechaCaptura)){
	                // TODO Validar mensaje de error a mostrar
	                throw new HospitalizacionException(MensajesErrorConstants.ME_009);
            	}
            	//
            } else { 
            	if(periodoMonitoreo == null || !periodoAnterior.getClavePeriodo().equals(periodoMonitoreo.getClavePeriodo())){
            		extemporaneo = true;
            	}
                
            }
        }
        return extemporaneo;
    }
    
    private Date convertirFechaIngreso(String fechaIngreso, String horaIngreso) {

        try {
            if (horaIngreso == null || horaIngreso.isEmpty()) {
                return new SimpleDateFormat(CapturarIngresosConstants.FORMATO_FECHA).parse(fechaIngreso);
            } else {
                String fechaHoraIngreso = fechaIngreso.concat(BaseConstants.ESPACIO).concat(horaIngreso);
                return new SimpleDateFormat(CapturarIngresosConstants.FORMATO_FECHA_HORA).parse(fechaHoraIngreso);
            }
        } catch (ParseException e) {
            return new Date();
        }
    }

    public void validarFechaAnterior(String fechaIngreso, String horaIngreso, Date fechaAnterior,
            TipoUbicacionEnum tipoUbicacion, int claveTipoFormato) throws HospitalizacionException {

        Date ingreso = convertirFechaIngreso(fechaIngreso, horaIngreso);
        Calendar nuevo = Calendar.getInstance();
        nuevo.setTime(ingreso);
        Calendar anterior = Calendar.getInstance();
        anterior.setTime(fechaAnterior);
        if (nuevo.get(Calendar.DAY_OF_YEAR) == anterior.get(Calendar.DAY_OF_YEAR)
                && nuevo.get(Calendar.MONTH) == anterior.get(Calendar.MONTH)
                && nuevo.get(Calendar.YEAR) == anterior.get(Calendar.YEAR)
                && (horaIngreso == null || horaIngreso.isEmpty())) {
            return;
        } else {
            if (ingreso.before(fechaAnterior)) {
                if (claveTipoFormato == TipoFormatoEnum.INGRESO_RN_ALOJAMIENTO_CONJUNTO.getClave().intValue()) {
                    throw new HospitalizacionException(MensajesErrorConstants.ME_178);
                }
                if (tipoUbicacion.equals(TipoUbicacionEnum.UCI)) {
                    throw new HospitalizacionException(MensajesErrorConstants.ME_200);
                } else {
                    if (horaIngreso == null || horaIngreso.isEmpty()) {
                        throw new HospitalizacionException(MensajesErrorConstants.ME_040);
                    } else {
                        throw new HospitalizacionException(MensajesErrorConstants.ME_075);
                    }
                }
            }
        }
    }

    public void validarEspecialidadInicio(List<DatosCatalogo> catalogoEspecialidad) throws HospitalizacionException {

        if (catalogoEspecialidad == null || catalogoEspecialidad.isEmpty()) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_030);
        }
    }

    
    public void validarBusquedaRecienNacidos(List<DatosIngresoRN> recienNacidos) throws HospitalizacionException {
        if (recienNacidos == null || recienNacidos.isEmpty()) {
            throw new HospitalizacionException(MensajesGeneralesConstants.MG_004);
        }
    }

    public void validarCamaMadre(String cama) throws HospitalizacionException {

        if (cama == null || cama.equals(BaseConstants.CADENA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_CAMA));
        }
    }
    
    public void validaCamaAlojamientoConjunto(int totalCamas, int ocupacionAlojamiento) throws HospitalizacionException{
    	if(totalCamas > BaseConstants.IND_CERO && ocupacionAlojamiento >= totalCamas){
    		throw new HospitalizacionException(MensajesErrorConstants.ME_610);
		}
    }

    public List<DatosCatalogo> obtenerCatalogoEspecialidad(TipoUbicacionEnum tipoUbicacion,
            List<DatosCatalogo> especialidades) {

        List<DatosCatalogo> catalogoEspecialidad = new ArrayList<DatosCatalogo>();
        switch (tipoUbicacion) {
            case HOSPITALIZACION:
                catalogoEspecialidad.addAll(especialidades);
                break;
            case RECIEN_NACIDOS:
                for (DatosCatalogo catalogo : especialidades) {
                    if (catalogo.getClave().substring(0, 2).equals(CapturarIngresosConstants.CLAVE_CUNERO_FISIOLOGICO)
                            || (catalogo.getClave().substring(0, 2).equals(CapturarIngresosConstants.CLAVE_CUNERO_PATOLOGICO)
                                    || (catalogo.getClave().substring(0, 2)
                                            .equals(CapturarIngresosConstants.CLAVE_UCI_NEONATOS)))) {
                        catalogoEspecialidad.add(catalogo);                	
                	
                    }
                }
                break;
            default:
                break;
        }
        return catalogoEspecialidad;
    }

}
