package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class PersonalOperativo {

    private String refPersonalOperativo;
    private String cveMatricula;
    private String refNombre;
    private String refApellidoPaterno;
    private String refApellidoMaterno;
    private String refEmail;
    private String refNumTelefono;
    private String refNumExtension;
    private Integer indActivo;
    private Integer indBloqueado;
    private Integer canIntentos;
    private Date fecBaja;
    private List<Rol> roles;
    private List<UnidadMedica> unidadesMedicas;

}
