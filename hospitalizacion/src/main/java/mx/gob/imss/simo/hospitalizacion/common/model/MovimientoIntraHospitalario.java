package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class MovimientoIntraHospitalario {

    private String fechaAtencion;
    private String divisionOrigen;
    private String divisionDestino;
    private long claveIngreso;
    private Date fechaCreacion;
    private Date fechaMovimiento;
    private String camaOrigen;
    private String especiaidadCamaOrigen;
    private String clavePresupuestal;
    private String especialidadOrigen;
    private String capturista;
    private long tipoFormato;
    private String camaDestino;
    private String especialidadCamaDestino;
    private String especialidadDestino;
    private String horaAnterior;
    private String horaActual;
    private Date fechaMovIngreso;
    private String cveMedico;
    

}
