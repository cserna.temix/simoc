package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Procedimientos {

    private Date fechaIntervencion;
    private int consecutivo;
    private String clave;
    private String descripcion;
    private DatosMedico medico;
    private String matricula;
    private String nombreCompleto;
    private String identificadorConexion;
    private String etiquetaProcedimiento;
    private String etiquetaCirujano;
    private String claveOriginal;
    private String fechaIntervencionHoraMinuto;
    private String fechaProcedimientoCadena;

}
