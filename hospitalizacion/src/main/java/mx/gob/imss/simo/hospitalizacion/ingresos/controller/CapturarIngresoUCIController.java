package mx.gob.imss.simo.hospitalizacion.ingresos.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesAdvertenciaConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SexoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosIngreso;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;

@ViewScoped
@ManagedBean(name = "ingresoUciController")
public class CapturarIngresoUCIController extends CapturarIngresoBaseController {

    private static final long serialVersionUID = -8875239821252311982L;

    @Override
    public String obtenerNombrePagina() {

        // LoginController.setRenderIcons(false);
        return PagesCommonConstants.INGRESO_UCI;
    }

    @Override
    @PostConstruct
    public void init() {

        super.init();
        setTipoUbicacion(TipoUbicacionEnum.UCI);
        deshabilitarCamposInicio();
        setCatalogoEspecialidad(capturarIngresoServices.obtenerCatalogoEspecialidad(getTipoUbicacion(),
                objetosSs.getDatosUsuario().getCvePresupuestal()));
        setCatalogoEspecialidadString(convertirCatalogoString(getCatalogoEspecialidad()));
        catalogoTipoIngreso = catalogosHospitalizacionServices.obtenerCatalogoTipoIngreso(getTipoUbicacion());
        validarEspecialidadInicio();
        setCatalogoEspecialidadHospitalizacion(capturarIngresoServices.obtenerCatalogoEspecialidad(
                TipoUbicacionEnum.HOSPITALIZACION, objetosSs.getDatosUsuario().getCvePresupuestal()));
        setCatalogoEspecialidadHospitalizacionString(convertirCatalogoString(getCatalogoEspecialidadHospitalizacion()));
    }

    @Override
    public void validarFechaIngreso() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_1)) {
                fechaIngreso = CapturarIngresosConstants.FECHA_VACIA;
                return;
            }
            String cve_Parametro = "PERIODO_LIBERACION";
            String fechaPeriodoConsulta = null;
            fechaPeriodoConsulta = capturarIngresoRepository.buscarPeriodoLiberacion(cve_Parametro);
            logger.info("Consulta: " +fechaPeriodoConsulta);
            logger.info("fecha Ingreso: " +fechaIngreso);
            SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaComp = null;
			Date fechaPeriodo = null;
			try {
				fechaComp = sdformat.parse(fechaIngreso);
				fechaPeriodo = sdformat.parse(fechaPeriodoConsulta);
			} catch (ParseException e) {
				logger.error("error al convertir la fecha" + e);
				e.printStackTrace();
			}
            if(fechaComp.before(fechaPeriodo)){
            	throw new HospitalizacionException(MensajesErrorConstants.ME_202);
            	          	
            }
            super.validarFechaIngreso();
            //si la fecha es extemporanea, solicitar aprobacion para registro
            if(datosHospitalizacion.getDatosIngreso().isExtemporaneo()){ 
            	RequestContext context = RequestContext.getCurrentInstance();
            	context.execute("PF('dlgConfirmacionFechaExtemporanea').show();");
            	tieneFoco = getExtemporaneoSi().getClientId();
            } else{
            	 getButtonCancelar().setDisabled(Boolean.FALSE);
                 tieneFoco = getTextNss().getClientId();
            }
        } catch (HospitalizacionException e) {
            getButtonCancelar().setDisabled(Boolean.TRUE);
            fechaIngreso = CapturarIngresosConstants.FECHA_VACIA;
            agregarMensajeError(e);
            tieneFoco = getMaskFecha().getClientId();
        }
    }

    public void aprobarFechaIngresoExtemporanea(){
    	RequestContext context = RequestContext.getCurrentInstance();
    	context.execute("PF('dlgConfirmacionFechaExtemporanea').hide();");
    	getButtonCancelar().setDisabled(Boolean.FALSE);
        tieneFoco = getTextNss().getClientId();
    }
    
    public void rechazarFechaIngresoExtemporanea(){
    	RequestContext context = RequestContext.getCurrentInstance();
    	context.execute("PF('dlgConfirmacionFechaExtemporanea').hide();");
    	 getButtonCancelar().setDisabled(Boolean.TRUE);
         fechaIngreso = CapturarIngresosConstants.FECHA_VACIA;
         tieneFoco = getMaskFecha().getClientId();
    }
    
    
    @Override
    public void validarNss() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_2)) {
                datosHospitalizacion.getDatosPaciente().setNss(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarNss();
            tieneFoco = getTextAgregadoMedico().getClientId();
        } catch (HospitalizacionException e) {
            datosHospitalizacion.getDatosPaciente().setNss(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            tieneFoco = getTextNss().getClientId();
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    @Override
    public void validarAgregadoMedico() {
    	
        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_3)) {
                datosHospitalizacion.getDatosPaciente().setAgregadoMedico(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarAgregadoMedico();
            pacientes = capturarIngresoServices.buscarPaciente(datosHospitalizacion.getDatosPaciente(), fechaIngreso);
            if (pacientes.size() < 2) {
                datosHospitalizacion.setDatosPaciente(pacientes.get(0));
                capturarIngresoServices.validarIngresoVigente(objetosSs.getDatosUsuario().getCvePresupuestal(),
                        getTipoUbicacion(), datosHospitalizacion.getDatosPaciente());
                if (datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())
                        || datosHospitalizacion.getDatosPaciente().getNombre().isEmpty()) {
                    // Derechohabiente no encontrado / No derechohabiente
                    if (capturarIngresoServices.habilitarEdad(fechaIngreso,
                            datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                                    .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
                        edad = BaseConstants.CADENA_VACIA;
                        getTextEdad().setDisabled(Boolean.FALSE);
                        getButtonGuardar().setDisabled(Boolean.TRUE);
                        tieneFoco = getTextEdad().getClientId();
                    } else {
                        Map<TipoCalculoEdadEnum, Integer> edades = capturarIngresoServices
                                .calcularEdadPacienteNoEncontrado(
                                        datosHospitalizacion.getDatosPaciente().getAgregadoMedico().substring(
                                                BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO),
                                        fechaIngreso);
                        datosHospitalizacion.getDatosPaciente().setEdadAnios(edades.get(TipoCalculoEdadEnum.ANIOS));
                        datosHospitalizacion.getDatosPaciente().setEdadSemanas(edades.get(TipoCalculoEdadEnum.SEMANAS));
                        edad = datosHospitalizacion.getDatosPaciente().getEdadAnios().toString();
                        getTextEdad().setDisabled(Boolean.TRUE);
                        getButtonGuardar().setDisabled(Boolean.TRUE);
                        tieneFoco = getTextNombre().getClientId();
                    }
                    limpiarCatalogos();
                    validarTipoIngresoSeleccionado();
                    habilitarCamposNoEncontradoNoDerechohabiente();
                } else {
                    // Derechohabiente encontrado
                    deshabilitarCamposDerechohabiente();
                    datosHospitalizacion.getDatosPaciente().setColorIdentificador(IdentificadorConexionEnum.VERDE.getRuta());
                    capturarIngresoServices.calcularEdadPacienteEncontrado(
                    		datosHospitalizacion.getDatosPaciente(),fechaIngreso);
                    if (capturarIngresoServices.habilitarEdad(fechaIngreso,
                            datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                                    .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))&&
                    		(datosHospitalizacion.getDatosPaciente().getEdadAnios() == null || 
                   		     datosHospitalizacion.getDatosPaciente().getEdadAnios() < 1)){
                        edad = datosHospitalizacion.getDatosPaciente().getEdadSemanas().toString();
                    } else {
                        edad = datosHospitalizacion.getDatosPaciente().getEdadAnios().toString();
                    }
                    delegacion = datosHospitalizacion.getDatosPaciente().getDelegacion();
                    List<DatosCatalogo> delegaciones = catalogosHospitalizacionServices
                            .obtenerCatalogoDelegacion(datosHospitalizacion.getDatosPaciente().getNumero());
                    obtenerDelegacion(delegaciones);
                    ingresoVigente = capturarIngresoServices.buscarIngresoVigenteHospitalizacion(
                            objetosSs.getDatosUsuario().getCvePresupuestal(), datosHospitalizacion.getDatosPaciente());
                    validarTipoIngresoSeleccionado();
                    obtenerIngresoPadre();
                    getButtonGuardar().setDisabled(Boolean.TRUE);
                    tieneFoco = getAutoEspecialidad().getClientId();
                }
                datosHospitalizacion.getDatosPaciente()
                        .setSexo(SexoEnum
                                .parse(datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                                        .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                                .getClave());
            } else {
                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PACIENTES);
                getButtonGuardar().setDisabled(Boolean.TRUE);
                tieneFoco = getTextNumeroPaciente().getClientId();
            }
            if(getCatalogoEspecialidad() != null && !getCatalogoEspecialidad().isEmpty()) {
    	        getAutoEspecialidad().setValue(getCatalogoEspecialidad().get(BaseConstants.IND_CERO).getClave()
    	        		.concat(BaseConstants.ESPACIO.concat(getCatalogoEspecialidad().get(BaseConstants.IND_CERO).getDescripcion())));
            }
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            if (e.getClaves()[0].equals(MensajesErrorConstants.ME_200)) {
                agregarMensajeError(e);
                limpiar();
            } else {
                DatosPaciente pacienteTemp = new DatosPaciente();
                pacienteTemp.setNss(datosHospitalizacion.getDatosPaciente().getNss());
                datosHospitalizacion.setDatosPaciente(pacienteTemp);
                datosHospitalizacion.setDatosIngreso(new DatosIngreso());
                getAutoEspecialidad().resetValue();
                getAutoEspecialidadHospitalizacion().resetValue();
                getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
                edad = BaseConstants.CADENA_VACIA;
                limpiarCatalogos();
                deshabilitarCamposInicio();
                if (e.getClaves()[0].equals(MensajesErrorConstants.ME_001A)) {
                    getButtonCancelar().setDisabled(Boolean.FALSE);
                }
                agregarMensajeError(e);
                tieneFoco = getTextAgregadoMedico().getClientId();
            }
        } catch (Exception e) {
            logger.error("Ha ocurrido un error: ", e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    @Override
    public void validarEdad() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_4)) {
                edad = BaseConstants.CADENA_VACIA;
                return;
            }
            super.validarEdad();
            datosHospitalizacion.getDatosPaciente().setEdadSemanas(Integer.valueOf(edad));
            datosHospitalizacion.getDatosPaciente().setEdadAnios(Integer.valueOf(edad) / BaseConstants.SEMANAS_ANIO);
            tieneFoco = getTextNombre().getClientId();
        } catch (HospitalizacionException e) {
            edad = BaseConstants.CADENA_VACIA;
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            tieneFoco = getTextEdad().getClientId();
        }
    }

    @Override
    public void validarNombre() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_5)) {
                datosHospitalizacion.getDatosPaciente().setNombre(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarNombre();
            tieneFoco = getTextApellidoPaterno().getClientId();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            tieneFoco = getTextNombre().getClientId();
        }
    }

    @Override
    public void validarApellidoPaterno() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_6)) {
                datosHospitalizacion.getDatosPaciente().setApellidoPaterno(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarApellidoPaterno();
            tieneFoco = getTextApellidoMaterno().getClientId();
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            tieneFoco = getTextApellidoPaterno().getClientId();
        }
    }

    @Override
    public void validarApellidoMaterno() {

        if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_6)) {
            datosHospitalizacion.getDatosPaciente().setApellidoMaterno(BaseConstants.CADENA_VACIA);
            return;
        }
        super.validarApellidoMaterno();
    }

    @Override
    public void validarNumero() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_7)) {
                datosHospitalizacion.getDatosPaciente().setNumero(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarNumero();
            tieneFoco = getAutoDelegacion().getClientId();
        } catch (HospitalizacionException e) {
            datosHospitalizacion.getDatosPaciente().setNumero(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            tieneFoco = getTextNumero().getClientId();
        }
    }

    @Override
    public void validarDelegacion() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_8)) {
                datosHospitalizacion.getDatosPaciente().setDelegacion(BaseConstants.CADENA_VACIA);
                getAutoDelegacion().resetValue();
                return;
            }
            super.validarDelegacion();
            tieneFoco = getAutoEspecialidad().getClientId();
        } catch (HospitalizacionException e) {
            if (!datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                    .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                    .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())) {
                datosHospitalizacion.getDatosPaciente().setClavePresupuestal(BaseConstants.CADENA_VACIA);
                datosHospitalizacion.getDatosPaciente().setDescripcionUnidad(BaseConstants.CADENA_VACIA);
            }
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            tieneFoco = getAutoDelegacion().getClientId();
        }
    }

    @Override
    public void validarHoraIngreso() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_9)) {
                datosHospitalizacion.getDatosIngreso().setHoraIngreso(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarHoraIngreso();
            if (null != datosHospitalizacion.getDatosPaciente().getCvePaciente()) {
                ingresoVigente = capturarIngresoServices.buscarIngresoVigenteHospitalizacion(
                        objetosSs.getDatosUsuario().getCvePresupuestal(), datosHospitalizacion.getDatosPaciente());
            }
            if (ingresoVigente != null) {
                capturarIngresoServices.validarFechaAnterior(fechaIngreso,
                        datosHospitalizacion.getDatosIngreso().getHoraIngreso(), ingresoVigente.getFecIngreso(),
                        getTipoUbicacion(), 0);
            }
            tieneFoco = getTextCama().getClientId();
        } catch (HospitalizacionException e) {
            datosHospitalizacion.getDatosIngreso().setHoraIngreso(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            tieneFoco = getMaskHoraIngreso().getClientId();
        }
    }

    @Override
    public void validarCama() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_10)) {
                datosHospitalizacion.getDatosIngreso().setCama(BaseConstants.CADENA_VACIA);
                return;
            }
            super.validarCama();
            tieneFoco = getSelectTipoIngreso().getClientId();
        } catch (HospitalizacionException e) {
            datosHospitalizacion.getDatosIngreso().setCama(BaseConstants.CADENA_VACIA);
            datosHospitalizacion.getDatosIngreso().setEspecialidadCama(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            tieneFoco = getTextCama().getClientId();
        }
    }

    @Override
    public void validarTipoIngreso() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_11)) {
                datosHospitalizacion.getDatosIngreso().setTipoIngreso(BaseConstants.CADENA_VACIA);
                setSelectTipoIngreso(new SelectOneMenu());
                catalogoTipoIngreso = catalogosHospitalizacionServices.obtenerCatalogoTipoIngreso(getTipoUbicacion());
                return;
            }
            super.validarTipoIngreso();
            if (datosHospitalizacion.getDatosIngreso().getEspecialidadHospitalizacion() == null
                    || datosHospitalizacion.getDatosIngreso().getEspecialidadHospitalizacion().isEmpty()) {
                tieneFoco = getAutoEspecialidadHospitalizacion().getClientId();
            } else {
                tieneFoco = getTextMatricula().getClientId();
            }
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            tieneFoco = getSelectTipoIngreso().getClientId();
        }
    }

    @Override
    public void validarEspecialidadHospitalizacion() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_12)) {
                datosHospitalizacion.getDatosIngreso().setEspecialidadHospitalizacion(BaseConstants.CADENA_VACIA);
                getAutoEspecialidadHospitalizacion().resetValue();
                return;
            }
            super.validarEspecialidadHospitalizacion();
            tieneFoco = getTextMatricula().getClientId();
        } catch (HospitalizacionException e) {
            datosHospitalizacion.getDatosIngreso().setEspecialidadHospitalizacion(BaseConstants.CADENA_VACIA);
            getAutoEspecialidadHospitalizacion().resetValue();
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            tieneFoco = getAutoEspecialidadHospitalizacion().getClientId();
        }
    }

    @Override
    public void validarMatricula() {

        try {
            if (!validarCamposRequeridos(CapturarIngresosConstants.CAMPO_13)) {
                datosHospitalizacion.getDatosMedico().setMatricula(BaseConstants.CADENA_VACIA);
                return;
            }
            getButtonGuardar().setDisabled(Boolean.FALSE);
            super.validarMatricula();
            if (datosHospitalizacion.getDatosMedico().getIdentificadorConexion()
                    .equals(IdentificadorConexionEnum.ROJO.getRuta())) {
                agregarMensajeAdvertencia(getArray(MensajesAdvertenciaConstants.MA_013), null);
            }
            tieneFoco = getButtonGuardar().getClientId();
        } catch (HospitalizacionException e) {
            datosHospitalizacion.getDatosMedico().setMatricula(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            tieneFoco = getTextMatricula().getClientId();
        } catch (Exception e) {
            logger.error("Ha ocurrido un error: ", e);
        }
    }

    @Override
    public void guardar() {

        try {
        	if (validarCamposRequeridos(CapturarIngresosConstants.CAMPO_14) &&
        			 validarGeneralGuardado() && validarEspecialidadGuardado() && validarEspecialidadHospGuadado()
        			&& validarCamaGuardado()) {
        		
	            super.guardar();
	            limpiar();
        	}
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            tieneFoco = getButtonGuardar().getClientId();
            
        }
    }

    @Override
    public void deshabilitarCamposInicio() {

        super.deshabilitarCamposInicio();
        getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
    }

    public void limpiar() {

        fechaIngreso = BaseConstants.CADENA_VACIA;
        edad = BaseConstants.CADENA_VACIA;
        delegacion = BaseConstants.CADENA_VACIA;
        datosHospitalizacion = new DatosHospitalizacion();
        getAutoEspecialidad().resetValue();
        getAutoEspecialidadHospitalizacion().resetValue();
        getSelectTipoIngreso().resetValue();
        setAutoEspecialidad(new AutoComplete());
        setAutoEspecialidadHospitalizacion(new AutoComplete());
        setSelectTipoIngreso(new SelectOneMenu());
        deshabilitarCamposInicio();
        tieneFoco = getMaskFecha().getClientId();
    }

    public void validarEspecialidadInicio() {

        try {
            capturarIngresoServices.validarEspecialidadInicio(getCatalogoEspecialidad());
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            agregarMensajeError(e);
        }
    }

    public void obtenerIngresoPadre() throws HospitalizacionException {

        if (ingresoVigente != null) {
            capturarIngresoServices.validarFechaAnterior(fechaIngreso,
                    datosHospitalizacion.getDatosIngreso().getHoraIngreso(), ingresoVigente.getFecIngreso(),
                    getTipoUbicacion(), 0);
            datosHospitalizacion.getDatosIngreso()
                    .setEspecialidadHospitalizacion(ingresoVigente.getCveEspecialidadIngreso());
            Especialidad especialidadHospitalizacion = capturarIngresoServices
                    .buscarEspecialidad(ingresoVigente.getCveEspecialidadIngreso());
            getAutoEspecialidadHospitalizacion().setValue(especialidadHospitalizacion.getCveEspecialidad()
                    .concat(BaseConstants.ESPACIO).concat(especialidadHospitalizacion.getDesEspecialidad()));
            getAutoEspecialidadHospitalizacion().setDisabled(Boolean.TRUE);
        } else {
            datosHospitalizacion.getDatosIngreso().setEspecialidadHospitalizacion(BaseConstants.CADENA_VACIA);
            getAutoEspecialidadHospitalizacion().resetValue();
            getAutoEspecialidadHospitalizacion().setDisabled(Boolean.FALSE);
        }
    }
    
    public void validarEspecialidad() {

        try {
            super.validarEspecialidad();
            tieneFoco = getMaskHoraIngreso().getClientId();
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            datosHospitalizacion.getDatosIngreso().setEspecialidad(BaseConstants.CADENA_VACIA);
            datosHospitalizacion.getDatosIngreso().setDivision(BaseConstants.CADENA_VACIA);
            datosHospitalizacion.getDatosIngreso().setTipoPrograma(BaseConstants.CADENA_VACIA);
            getAutoEspecialidad().resetValue();
            setCatalogoDivision(new ArrayList<DatosCatalogo>());
            catalogoTipoPrograma = new ArrayList<DatosCatalogo>();
            agregarMensajeError(e);
            tieneFoco = getAutoEspecialidad().getClientId();
        }
    }
    

    public boolean validarEspecialidadGuardado() {

        try {
            super.validarEspecialidad();
            return true;
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
            datosHospitalizacion.getDatosIngreso().setEspecialidad(BaseConstants.CADENA_VACIA);
            datosHospitalizacion.getDatosIngreso().setDivision(BaseConstants.CADENA_VACIA);
            datosHospitalizacion.getDatosIngreso().setTipoPrograma(BaseConstants.CADENA_VACIA);
            getAutoEspecialidad().resetValue();
            setCatalogoDivision(new ArrayList<DatosCatalogo>());
            catalogoTipoPrograma = new ArrayList<DatosCatalogo>();
            agregarMensajeError(e);
            tieneFoco = getAutoEspecialidad().getClientId();
            return false;
        }
    } 
    
    private boolean validarEspecialidadHospGuadado(){
	    try {
	        super.validarEspecialidadHospitalizacion();
	        return true;
	    } catch (HospitalizacionException e) {
	    	getButtonGuardar().setDisabled(Boolean.TRUE);
	    	datosHospitalizacion.getDatosIngreso().setEspecialidadHospitalizacion(BaseConstants.CADENA_VACIA);
	        agregarMensajeError(e);
	        tieneFoco = getAutoEspecialidadHospitalizacion().getClientId();
	        return false;
	    }
    }
    
    private boolean validarCamaGuardado() {
        try {
            super.validarCama();
            return true;
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
        	datosHospitalizacion.getDatosIngreso().setCama(BaseConstants.CADENA_VACIA);
            datosHospitalizacion.getDatosIngreso().setEspecialidadCama(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            tieneFoco = getTextCama().getClientId();
            return false;
        }
    }
    
    private boolean validarGeneralGuardado() {
    	String errorFoco = null;
        try {
        	errorFoco = getTextAgregadoMedico().getClientId();
            super.validarAgregadoMedico();
            errorFoco = getTextEdad().getClientId();
            super.validarEdad();
            //validarAgregadoMedicoGuardado();
            return true;
        } catch (HospitalizacionException e) {
        	getButtonGuardar().setDisabled(Boolean.TRUE);
        	datosHospitalizacion.getDatosPaciente().setAgregadoMedico(BaseConstants.CADENA_VACIA);
            agregarMensajeError(e);
            tieneFoco = errorFoco;
            return false;
        }
    }
    
    
    	public void validarAgregadoMedicoGuardado() throws HospitalizacionException {

                capturarIngresoServices.validarIngresoVigente(objetosSs.getDatosUsuario().getCvePresupuestal(),
                        getTipoUbicacion(), datosHospitalizacion.getDatosPaciente());
                if (datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())
                        || datosHospitalizacion.getDatosPaciente().getNombre().isEmpty()) {
                    // Derechohabiente no encontrado / No derechohabiente
                    if (capturarIngresoServices.habilitarEdad(fechaIngreso,
                            datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                                    .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
                        edad = BaseConstants.CADENA_VACIA;
                        super.validarEdad();
        				datosHospitalizacion.getDatosPaciente().setEdadSemanas(Integer.valueOf(edad));
        				datosHospitalizacion.getDatosPaciente()
        						.setEdadAnios(Integer.valueOf(edad) / BaseConstants.SEMANAS_ANIO);
                        getTextEdad().setDisabled(Boolean.FALSE);
                    } else {
                        Map<TipoCalculoEdadEnum, Integer> edades = capturarIngresoServices
                                .calcularEdadPacienteNoEncontrado(
                                        datosHospitalizacion.getDatosPaciente().getAgregadoMedico().substring(
                                                BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO),
                                        fechaIngreso);
                        datosHospitalizacion.getDatosPaciente().setEdadAnios(edades.get(TipoCalculoEdadEnum.ANIOS));
                        datosHospitalizacion.getDatosPaciente().setEdadSemanas(edades.get(TipoCalculoEdadEnum.SEMANAS));
                        edad = datosHospitalizacion.getDatosPaciente().getEdadAnios().toString();
                        getTextEdad().setDisabled(Boolean.TRUE);
                    }
                } else {
                    if (capturarIngresoServices.habilitarEdad(fechaIngreso,
                            datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                                    .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
                        edad = datosHospitalizacion.getDatosPaciente().getEdadSemanas().toString();
                        super.validarEdad();
        				datosHospitalizacion.getDatosPaciente().setEdadSemanas(Integer.valueOf(edad));
        				datosHospitalizacion.getDatosPaciente()
        						.setEdadAnios(Integer.valueOf(edad) / BaseConstants.SEMANAS_ANIO);
                    } else {
                    	 Map<TipoCalculoEdadEnum, Integer> edades = capturarIngresoServices
                                 .calcularEdadPacienteNoEncontrado(
                                         datosHospitalizacion.getDatosPaciente().getAgregadoMedico().substring(
                                                 BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO),
                                         fechaIngreso);
                         datosHospitalizacion.getDatosPaciente().setEdadAnios(edades.get(TipoCalculoEdadEnum.ANIOS));
                         datosHospitalizacion.getDatosPaciente().setEdadSemanas(edades.get(TipoCalculoEdadEnum.SEMANAS));
                         edad = datosHospitalizacion.getDatosPaciente().getEdadAnios().toString();
                         getTextEdad().setDisabled(Boolean.TRUE);
                    }

                }
                datosHospitalizacion.getDatosPaciente()
                        .setSexo(SexoEnum
                                .parse(datosHospitalizacion.getDatosPaciente().getAgregadoMedico()
                                        .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                                .getClave());
    }
}
