package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Ingreso {

    private String cveCama;
    private String cveCapturista;
    private String cveEspecialidadCama;
    private String cveEspecialidadIngreso;
    private String cveEspecialidadOrigen;
    private long cveIngreso;
    private Long cveIngresoPadre;
    private String cveMedico;
    private long cvePaciente;
    private String cvePresupuestal;
    private int cveTipoFormato;
    private int cveTipoIngreso;
    private Integer cveTipoPrograma;
    private Date fecCreacion;
    private Date fecIngreso;
    private boolean indEgreso;
    private boolean indExtemporaneo;
    private boolean indIntervencionQx;
    private boolean indMovIntrahospitalario;
    private boolean indTococirugia;
    private String refCamaRn;
    private int numRecienNacido; 

}
