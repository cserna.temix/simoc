package mx.gob.imss.simo.hospitalizacion.interconsultas.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.busquedas.repository.BuscarPacienteRepository;
import mx.gob.imss.simo.hospitalizacion.busquedas.rules.AccederRules;
import mx.gob.imss.simo.hospitalizacion.busquedas.services.BuscarPersonalOperativoService;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CampoInterconsultaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoUbicacionEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.AtencionInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DelegacionIMSS;
import mx.gob.imss.simo.hospitalizacion.common.model.EncabezadoInterconsulta;
import mx.gob.imss.simo.hospitalizacion.common.model.Especialidad;
import mx.gob.imss.simo.hospitalizacion.common.model.EspecialidadUnidad;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.model.Procedimiento;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;
import mx.gob.imss.simo.hospitalizacion.common.repository.CatalogosHospitalizacionRepository;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.hospitalizacion.interconsultas.repository.CapturaInterconsultaRepository;
import mx.gob.imss.simo.hospitalizacion.interconsultas.rules.CapturaInterconsultasRules;
import mx.gob.imss.simo.hospitalizacion.interconsultas.service.CapturaInterconsultaService;

@Service("capturaInterconsultaService")
public class CapturaInterconsultaServiceImpl extends HospitalizacionCommonServicesImpl
		implements CapturaInterconsultaService {

	@Autowired
	private CapturaInterconsultasRules capturaInterconsultaRules;

	@Autowired
	private HospitalizacionCommonRules hospitalizacionCommonRules;

	@Autowired
	private HospitalizacionCommonHelper hospitalizacionCommonHelper;

	@Autowired
	private CapturaInterconsultaRepository capturaInterconsultaRepository;

	@Autowired
	private BuscarPersonalOperativoService buscarPersonalOperativoService;

	@Autowired
	private CatalogosHospitalizacionRepository catalogosHospitalizacionRepository;

	@Autowired
	private HospitalizacionCommonRepository hospitalizacionCommonRepository;
	
	@Autowired
	private BuscarPacienteRepository buscarPacienteRepository;
	
	@Autowired
	private AccederRules accederRules;
	
	public long insertarPacienteInterconsulta(DatosPaciente datosPaciente){

		String regimen = datosPaciente.getAgregadoMedico().substring(BaseConstants.INICIO_AGREGADO_REGIMEN);
		Paciente paciente = new Paciente();
		paciente.setRefNss(datosPaciente.getNss());
		paciente.setRefAgregadoMedico(datosPaciente.getAgregadoMedico());
		paciente.setRefNombre(datosPaciente.getNombre());
		paciente.setRefApellidoPaterno(datosPaciente.getApellidoPaterno());
		paciente.setRefApellidoMaterno(datosPaciente.getApellidoMaterno());
		paciente.setFecNacimiento(datosPaciente.getFechaNacimiento());
		paciente.setNumEdadSemanas(datosPaciente.getEdadSemanas());
		paciente.setNumEdadAnios(datosPaciente.getEdadAnios());
		paciente.setIndAcceder(datosPaciente.isMarcaAcceder());
		paciente.setIndVigencia(datosPaciente.isVigencia());
		paciente.setIndRecienNacido(false);
		paciente.setIndDerechohabiente(
				regimen.equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave()) ? false : true);
		paciente.setIndUltimoRegistro(true);
		paciente.setCveUnidadAdscripcion(datosPaciente.getClavePresupuestal());
		return buscarPacienteRepository.guardarPaciente(paciente);
	}
	
    public void validarDelegacion(String delegacion, List<DatosCatalogo> delegaciones) throws HospitalizacionException {

        if (delegacion == null || delegacion.isEmpty() || delegacion.length() != BaseConstants.LONGITUD_DELEGACION) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_DELEGACION));
        }
        List<String> delegacionesValidas = new ArrayList<String>();
        for (DatosCatalogo del : delegaciones) {
            delegacionesValidas.add(del.getClave());
        }
        if (!delegacionesValidas.contains(delegacion)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_DELEGACION));
        }
    }	
	
    public UnidadMedica buscarUnidadMedica(String numeroUnidad, String claveDelegacion) {

        return hospitalizacionCommonRepository.buscarUnidadMedica(numeroUnidad, claveDelegacion);
    }    
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void insertarDetalleInterconsulta(EncabezadoInterconsulta encabezado, AtencionInterconsulta atencionInteronsulta) throws HospitalizacionException  {

		try {
			capturaInterconsultaRepository.insertarDetalleInterconsulta(encabezado, atencionInteronsulta);
		} catch (HospitalizacionException e) {
			e.printStackTrace();
		}
	}
	
	public void validarDatosCapturaPaciente(String nombre, Integer validarDatosCapturaPaciente) throws HospitalizacionException {
		if (validarDatosCapturaPaciente == CampoInterconsultaEnum.NOMBRE_PACIENTE.getClave()) {
			if (nombre == null || nombre.isEmpty()) {
				throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
						getArray(BaseConstants.REQUERIDO_NOMBRE));
			}
		}
		if (validarDatosCapturaPaciente == CampoInterconsultaEnum.AP_PAT_PACIENTE.getClave()) {
			if (nombre == null || nombre.isEmpty()) {
	            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
	                    getArray(BaseConstants.REQUERIDO_APELLIDO_PATERNO));
			}
		}
		if (validarDatosCapturaPaciente == CampoInterconsultaEnum.AP_MAT_PACIENTE.getClave()) {
			if (nombre == null || nombre.isEmpty()) {
				throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
						getArray(BaseConstants.REQUERIDO_APELLIDO_MATERNO));
			}
		}
	}

	@Override
	public void validarRequeridoFecha(String fecha) throws HospitalizacionException {

		if (fecha == null || fecha.isEmpty()) {
			throw new HospitalizacionException(MensajesErrorConstants.ME_160);
		}
	}

	@Override
	public void validarFechaInterconsulta(String fecha) throws HospitalizacionException {

			capturaInterconsultaRules.validarFormatoFecha(fecha);		
			capturaInterconsultaRules.validarFechaActual(fecha);

	}

	@Override
	public void validarFechaEnPeriodoActualActivo(String fecha, String clavePresupuestal)
			throws HospitalizacionException {

		try {
			this.validarFechaEnPeriodoActualActivo(fecha, hospitalizacionCommonHelper.convertStringToDate(fecha), clavePresupuestal,
					TipoCapturaEnum.HOSPITALIZACION.getClave());
		} catch (HospitalizacionException e) {
			throw new HospitalizacionException(MensajesErrorConstants.ME_009);
		}
	}
	
	@Override
    public void validarFechaIngreso(String fechaIngreso, String clavePresupuestal) throws HospitalizacionException {

	 	capturaInterconsultaRules.validarFormatoFecha(fechaIngreso);
	 	capturaInterconsultaRules.validarFechaActual(fechaIngreso);
	 	validarFechaEnPeriodoActualActivo( fechaIngreso, clavePresupuestal);
        validarReporteRelacionPacientesGenerado(clavePresupuestal, fechaIngreso);        
       // validarCamasConDosPacientes(clavePresupuestal, fechaIngreso);
        validarPeriodoAnteriorCerrado(clavePresupuestal, fechaIngreso, TipoCapturaEnum.HOSPITALIZACION.getClave());
    }

	public void validarRequeridoEspecialidad(String especialidad) throws HospitalizacionException {

		if (especialidad == null) {
			throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
					getArray(BaseConstants.REQUERIDO_ESPECIALIDAD));
		}

	}

	@Override
	public List<Especialidad> obtenerEspecialidadesPorCvePres(String clavePresupuestal)
			throws HospitalizacionException {

		return capturaInterconsultaRepository.obtenerEspecialidadesPorCvePres(clavePresupuestal);
	}

	@Override
	public String validarMatricula(String matricula, String delegacion) throws HospitalizacionException {

		capturaInterconsultaRules.validarMatricula(matricula);
		DatosMedico datos = buscarPersonalOperativoService.buscarPersonalOperativo(matricula, delegacion);
		return datos.getNombreCompleto();
	}

	@Override
	public List<DatosCatalogo> obtenerCatalogoTurno() throws HospitalizacionException {

		return catalogosHospitalizacionRepository.obtenerCatalogoTurno();
	}

	@Override
	public void validarRequeridoTurno(String turno) throws HospitalizacionException {

		if (turno == null || turno.equals(BaseConstants.CADENA_VACIA)) {
			throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
					getArray(BaseConstants.REQUERIDO_TURNO));
		}
	}

	@Override
    public void guardarEncabezado( EncabezadoInterconsulta encabezado ) throws HospitalizacionException {
    	capturaInterconsultaRepository.guardarEncabezado(encabezado);
    }
	
	public EncabezadoInterconsulta buscarEncabezadoInterconsulta(String fechaInterconsulta, String claveEspecialidad,
			String claveMedico, Integer claveTurno) throws HospitalizacionException {
		return capturaInterconsultaRepository.buscarEncabezadoInterconsulta(fechaInterconsulta, claveEspecialidad, claveMedico, claveTurno);
	}

	@Override
	public Long obtenerConsecutivoInterconsultas(Integer cve_interconsulta) throws HospitalizacionException {
		return capturaInterconsultaRepository.obtenerConsecutivoInterconsultas(cve_interconsulta);
	}

	@Override
	public void validarNss(String nss) throws HospitalizacionException {

		capturaInterconsultaRules.validarNss(nss);
	}

	@Override
	public void validarAgregadoMedico() throws HospitalizacionException {

	}

	@Override
	public void validarRequeridoDuracionIntercon(Integer duracion) throws HospitalizacionException {

		if (duracion == null) {
			throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
					getArray(BaseConstants.REQUERIDO_DURACION_INTERCON));
		}

	}

	@Override
	public void validarRequeridoPrimVez(String primeraVez) throws HospitalizacionException {

		if (primeraVez == null) {
			throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
					getArray(BaseConstants.REQUERIDO_PRIMERA_VEZ));
		}

	}

	@Override
	public void validarRequeridoEspecialidadSolic(String especialidad) throws HospitalizacionException {

		if (especialidad == null) {
			throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
					getArray(BaseConstants.REQUERIDO_ESPECIALIDAD));
		}
	}

	@Override
	public void validarRequeridoAltaIntercon(String alta) throws HospitalizacionException {

		if (alta == null) {
			throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
					getArray(BaseConstants.REQUERIDO_ALTA_INTERCONSULTA));
		}

	}

	@Override
	public void validarEdad(String edad) throws HospitalizacionException {

		capturaInterconsultaRules.validarEdad(edad);
	}

	@Override
	public boolean habilitarEdad(String fechaIngreso, String anio) {

		return capturaInterconsultaRules.habilitarEdad(fechaIngreso, anio);

	}

	@Override
	public Map<TipoCalculoEdadEnum, Integer> calcularEdadPacienteNoEncontrado(String anio, String fechaIngreso) {

		Date fechaAnio;
		try {
			fechaAnio = new SimpleDateFormat(CapturarIngresosConstants.FORMATO_FECHA_AGREGADO_ANIO)
					.parse(anio.concat("/01/01"));
		} catch (ParseException e) {
			fechaAnio = new Date();
		}
		return accederRules.calcularEdadDerechohabiente(fechaAnio, fechaIngreso);
	}
	
	@Override
	public void validarDiagnosticoPorViolencia(String cveDiagnostico) throws HospitalizacionException {
		
		
		if(validarDiagnosticoAccidente(cveDiagnostico.toUpperCase())){
			throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_019),getArray(BaseConstants.CIE10));
		}
	}
	
	@Override
    public void validarDiagnosticoRepetido(String cveDiag1, String cveDiag2, String cveDiag3)
            throws HospitalizacionException {
		if(cveDiag2 != null && !cveDiag2.isEmpty()) {
	        if (cveDiag1.equals(cveDiag2) || cveDiag1.equals(cveDiag3) ||
	        		cveDiag2.equals(cveDiag3)) {
	        	if(buscarProcedimientoBilateral(cveDiag1)==null && buscarProcedimientoBilateral(cveDiag2)==null && buscarProcedimientoBilateral(cveDiag3)==null) {
	        		throw new HospitalizacionException(MensajesErrorConstants.ME_158);
	        	}
	        }
		}
    }

	@Override
	public Procedimiento buscarProcedimiento(String cveProcedimiento) {

		return hospitalizacionCommonRepository.obtenerProcedimientoPorClave(cveProcedimiento.toUpperCase());
	}
	
	
	@Override
	public Procedimiento buscarProcedimientoBilateral(String cveProcedimiento) {
		Procedimiento procedimiento;
		if(cveProcedimiento!=null && !cveProcedimiento.equals(BaseConstants.CADENA_VACIA)) {
			procedimiento=hospitalizacionCommonRepository.obtenerProcedimientoBilateralPorClave(cveProcedimiento.toUpperCase());
		}else {
			procedimiento=null;
		}
		return procedimiento;
	}
	
	@Override
	public void validarProcedimientos(String cveProcedimiento, DatosPaciente datosPaciente, boolean validar) throws HospitalizacionException {

			
		if (null != cveProcedimiento && !cveProcedimiento.isEmpty()) {
			Procedimiento procedimiento = buscarProcedimiento(cveProcedimiento);
			if (procedimiento == null && validar) {
				throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_014),
						getArray(BaseConstants.CIE9));
			}
			hospitalizacionCommonRules.validarProcedimientoEdad(procedimiento, datosPaciente.getEdadAnios(), Boolean.TRUE,
					datosPaciente.getEdadSemanas());
			hospitalizacionCommonRules.validarProcedimientoSexo(procedimiento, datosPaciente.getSexo(), Boolean.TRUE);
			
		}

	}

	public List<String> filtrarCatalogoTurnoEncabezado (String query, List<DatosCatalogo> catalogoTurnoList) {
	
		List<String> filtroCatalogo = new ArrayList<String>();
		if (query.length() < BaseConstants.LONGITUD_ESPECIALIDAD) {
			for(DatosCatalogo turno : catalogoTurnoList) {
				if (turno.getClave().startsWith(query)) {
					filtroCatalogo.add(turno.getClave()
			                .concat(BaseConstants.ESPACIO).concat(turno.getDescripcion()));
				}
			}
		}
		return filtroCatalogo;
	}
	
	public List<String> filtrarCatalogoEspecialidadEncabezado (String query, List<Especialidad> especialidadesList) {
		
		List<String> filtroCatalogo = new ArrayList<String>();
		if (query.length() < BaseConstants.LONGITUD_ESPECIALIDAD) {
			for(Especialidad especialidad : especialidadesList) {
				if (especialidad.getCveEspecialidad().startsWith(query)) {
					filtroCatalogo.add(especialidad.getCveEspecialidad()
			                .concat(BaseConstants.ESPACIO).concat(especialidad.getDesEspecialidad()));
				}
			}
		}
		return filtroCatalogo;
	}

	public List<String> filtrarCatalogoDelegacionDetalle(String query, List<DelegacionIMSS> delegaciones) {
		List<String> filtroCatalogo = new ArrayList<String>();
		if (query.length() < BaseConstants.LONGITUD_DELEGACION) {
			for(DelegacionIMSS delegacion : delegaciones) {
				if (delegacion.getCveDelegacionImss().startsWith(query)) {
					filtroCatalogo.add(delegacion.getCveDelegacionImss()
							.concat(BaseConstants.ESPACIO).concat(delegacion.getDesDelegacionImss()));
				}
			}
		}
		return filtroCatalogo;
	}	
	
	 @Override
    public boolean esPeriodoExtemporaneo(Date fechaCaptura, String clavePresupuestal) throws HospitalizacionException {

        PeriodosImss periodoActivo = capturaInterconsultaRepository.obtenerPeriodoActivo(clavePresupuestal,
                TipoCapturaEnum.HOSPITALIZACION.getClave());
        PeriodosImss periodoAnterior=null;
        if(periodoActivo!=null){
        	periodoAnterior = capturaInterconsultaRepository
                    .obtenerPeriodoInmediatoAnterior(periodoActivo.getClavePeriodo());	
        }       
        return capturaInterconsultaRules.esPeriodoExtemporaneo(periodoActivo, periodoAnterior, fechaCaptura);
    }
	 
    @Override
    public List<DatosCatalogo> obtenerCatalogoEspecialidad(TipoUbicacionEnum tipoUbicacion, String clavePresupuestal) {

        List<DatosCatalogo> catalogoEspecialidad = getCatalogosHospitalizacionServices()
                .obtenerCatalogoEspecialidad(clavePresupuestal);
        return capturaInterconsultaRules.obtenerCatalogoEspecialidad(tipoUbicacion, catalogoEspecialidad);
    }

    
    @Override
    @Transactional
    public Integer validarEspecialidadInterconsulta( String claveEspecialidad,
            String clavePresupuestal) throws HospitalizacionException {

        EspecialidadUnidad especialidadUnidad = hospitalizacionCommonRepository
                .buscarEspecialidadUnidad(claveEspecialidad, clavePresupuestal);
        hospitalizacionCommonRules.validarEspecialidadAutorizada(especialidadUnidad);
        String clave = claveEspecialidad.substring(0, 2);
        List<Especialidad> especialidadesInterconsulta=capturaInterconsultaRepository.validarEspecialidadInterconsulta(claveEspecialidad);
        if(especialidadesInterconsulta.isEmpty()){
        	 throw new HospitalizacionException(MensajesErrorConstants.ME_164);
        }
        
        List<String> claves = new ArrayList<>();

                claves.add(CapturarIngresosConstants.CLAVE_CUNERO_FISIOLOGICO);
                claves.add(CapturarIngresosConstants.CLAVE_CUNERO_PATOLOGICO);
                claves.add(CapturarIngresosConstants.CLAVE_UCI);
                claves.add(CapturarIngresosConstants.CLAVE_UCI_NEONATOS);
                claves.add(CapturarIngresosConstants.CLAVE_URGENCIAS);
                if (claves.contains(clave)) {
                    throw new HospitalizacionException(MensajesErrorConstants.ME_070);
                }
          
        Especialidad especialidad = hospitalizacionCommonRepository.buscarEspecialidad(claveEspecialidad);
       
        return especialidad.getCveDivision();
    }
    
    @Override
	public void validarEspecialidadPermitidaPaciente(String cveEspecialidad, Integer edadAnios, Integer edadSemanas,
			Integer sexoPaciente) throws HospitalizacionException {

		Especialidad especialidad = hospitalizacionCommonRepository.buscarEspecialidad(cveEspecialidad);
		hospitalizacionCommonRules.validarEdadPermitida(especialidad, edadAnios, edadSemanas);
		hospitalizacionCommonRules.validarSexoEspecialidad(especialidad.getCveSexo(),sexoPaciente);
	}
}
