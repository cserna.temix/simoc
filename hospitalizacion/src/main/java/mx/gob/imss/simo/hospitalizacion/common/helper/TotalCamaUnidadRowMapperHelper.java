package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.TotalCamaUnidad;

public class TotalCamaUnidadRowMapperHelper extends BaseHelper implements RowMapper<TotalCamaUnidad> {
	
	@Override
	public TotalCamaUnidad mapRow(ResultSet resultSet, int rowId) throws SQLException {
		
		TotalCamaUnidad totalCamaUnidad = new TotalCamaUnidad();
		totalCamaUnidad.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
		totalCamaUnidad.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
		totalCamaUnidad.setCanCamaCensable(resultSet.getInt(SQLColumnasConstants.CAN_CAMA_CENSABLE));
		totalCamaUnidad.setCanCamaNoCensable(resultSet.getInt(SQLColumnasConstants.CAN_CAMA_NO_CENSABLE));
		return totalCamaUnidad;
		
	}

}
