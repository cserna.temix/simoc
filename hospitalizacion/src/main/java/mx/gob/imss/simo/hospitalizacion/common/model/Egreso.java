package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Egreso {

    private long claveEgreso;
    private String claveCapturista;
    private String claveCapturistaActualiza;
    private Date fechaCreacion;
    private Date fechaActualizacion;
    private Date fechaEgreso;
    private long claveTipoEgreso;
    private String refJustificaCancelacion;
    private long claveTamiz;
    private long claveMetodoAnticonceptivo;
    private long cantMetodoAnticonceptivo;
    private long numPesoRn;
    private long claveAlimentacionRn;
    private long claveMotivoAlta;
    private long claveTipoFormato;
    private String claveMedico;

}
