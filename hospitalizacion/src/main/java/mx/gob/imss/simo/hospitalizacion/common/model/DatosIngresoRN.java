package mx.gob.imss.simo.hospitalizacion.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class DatosIngresoRN {

    private String agregadoMedico;
    private boolean alojamientoPrevio;
    private String apellidoMaterno;
    private String apellidoPaterno;
    private String cama;
    private String camaRN;
    private String especialidadCamaRN;
    private String claveDelegacion;
    private Long claveIngresoPadre;
    private Long clavePaciente;
    private Long clavePacientePadre;
    private String clavePresupuestal;
    private int claveTipoFormato;
    private Integer consecutivo;
    private boolean cuneroVigente;
    private String descripcionDelegacion;
    private String descripcionUnidad;
    private String division;
    private String especialidadIngreso;
    private String especialidadHospitalizacion;
    private boolean extemporaneo;
    private String fechaIngreso;
    private Date fechaParto;
    private String horaIngreso;
    private String matricula;
    private String medico;
    private String nombre;
    private String nss;
    private String numeroUnidad;
    private Integer numeroRN;

}
