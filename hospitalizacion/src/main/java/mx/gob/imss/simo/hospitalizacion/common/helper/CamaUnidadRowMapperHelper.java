package mx.gob.imss.simo.hospitalizacion.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.model.CamaUnidad;

public class CamaUnidadRowMapperHelper extends BaseHelper implements RowMapper<CamaUnidad> {

    @Override
    public CamaUnidad mapRow(ResultSet resultSet, int rowId) throws SQLException {

        CamaUnidad camaUnidad = new CamaUnidad();
        camaUnidad.setCveCama(resultSet.getString(SQLColumnasConstants.CVE_CAMA));
        camaUnidad.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        camaUnidad.setCveEspecialidad(resultSet.getString(SQLColumnasConstants.CVE_ESPECIALIDAD));
        camaUnidad.setRefPiso(resultSet.getString(SQLColumnasConstants.REF_PISO));
        camaUnidad.setRefAla(resultSet.getString(SQLColumnasConstants.REF_ALA));
        camaUnidad.setIndCensable(resultSet.getBoolean(SQLColumnasConstants.IND_CENSABLE));
        camaUnidad.setIndFicticio(resultSet.getBoolean(SQLColumnasConstants.IND_FICTICIO));
        camaUnidad.setFecBaja(resultSet.getDate(SQLColumnasConstants.FEC_BAJA));
        camaUnidad.setIndCamaPedriatica(resultSet.getBoolean(SQLColumnasConstants.IND_CAMA_PEDIATRIA));
        return camaUnidad;
    }

}
