package mx.gob.imss.simo.hospitalizacion.tococirugia.services.implTest;

import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Matchers.any;


import mx.gob.imss.simo.hospitalizacion.common.constant.TococirugiaConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosTococirugia;
import mx.gob.imss.simo.hospitalizacion.common.model.Tococirugia;
import mx.gob.imss.simo.hospitalizacion.tococirugia.helper.CapturarTococirugiaHelper;
import mx.gob.imss.simo.hospitalizacion.tococirugia.repository.TococirugiaRepository;
import mx.gob.imss.simo.hospitalizacion.tococirugia.rules.CapturarTococirugiaRules;
import mx.gob.imss.simo.hospitalizacion.tococirugia.services.impl.CapturarTococirugiaServicesImpl;
import mx.gob.imss.simo.model.DatosUsuario;

public class CapturarTococirugiaServicesImplTest {
	
	@InjectMocks
	CapturarTococirugiaServicesImpl capturarTococirugiaServicesImpl;
	
	@Mock
	TococirugiaRepository tococirugiaRepository;
	
	@Mock
	CapturarTococirugiaHelper capturarTococirugiaHelper;
	
	@Mock
	CapturarTococirugiaRules capturarTococirugiaRules;
	
	final static Logger logger = LoggerFactory.getLogger(CapturarTococirugiaServicesImplTest.class);
	
	
	@BeforeClass
	public static void ini() {
		BasicConfigurator.configure();
	}

	@Before
	public void setup() {
		capturarTococirugiaServicesImpl = PowerMockito.spy(new CapturarTococirugiaServicesImpl());
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void guardarTest(){
		try {
			DatosTococirugia datosTococirugia = new DatosTococirugia();
			datosTococirugia.setAtencion(TococirugiaConstants.TipoAtencionEnum.ABORTO.getClaveTipoAtencion());
			
			DatosHospitalizacion datosHospitalizacion = new DatosHospitalizacion();
			datosHospitalizacion.setDatosTococirugia(datosTococirugia);
			
			DatosUsuario datosUsuario = new DatosUsuario();
			datosUsuario.setNombre("Susana");
			datosUsuario.setAppMaterno("Gonzalez");
			datosUsuario.setAppPaterno("Per�z");
			datosUsuario.setCvePresupuestal("010101012151");
			
			Tococirugia tococirugia = new Tococirugia();
			tococirugia.setClave(945854);
			tococirugia.setCvePresupuestal("010101012151");
			tococirugia.setCveTipoAtencion(2);
			tococirugia.setCveSala("Q01");
			tococirugia.setTotalRecienNacidos(1);
			
			doNothing().when(tococirugiaRepository).guardarTococirugia(any(Tococirugia.class));
			Mockito.when(capturarTococirugiaHelper.prepararTococirugia(any(DatosHospitalizacion.class)
					,any(DatosUsuario.class))).thenReturn(tococirugia);

			capturarTococirugiaServicesImpl.guardar(datosHospitalizacion,datosUsuario);
			
			logger.info("Test guardarTest");
			logger.info("Se guardo correctamente:");
			logger.info("Clave Tococirugia:{}", tococirugia.getClave());
			logger.info("Tipo Atenci�n:{}", datosTococirugia.getAtencion());
			logger.info("Clave Presupuestal:{}", tococirugia.getCvePresupuestal());
			
			
		} catch (HospitalizacionException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void obtenerCatalogoEpisiotomiaTest(){
		logger.info("Test obtenerCatalogoEpisiotomiaTest");
		List<DatosCatalogo> catalogoEpisiotomia = new ArrayList<>();
		for(TococirugiaConstants.EpisiotomiaEnum valor: TococirugiaConstants.EpisiotomiaEnum.values()){
    		DatosCatalogo episiotomia = new DatosCatalogo();
    		episiotomia.setClave(valor.getClave());
    		episiotomia.setDescripcion(valor.getDescripcion());
    		catalogoEpisiotomia.add(episiotomia);
    	}
		Mockito.when(capturarTococirugiaHelper.armarCatalogoEpisiotomia()).thenReturn(catalogoEpisiotomia);
		List<DatosCatalogo> catalogo = capturarTococirugiaServicesImpl.obtenerCatalogoEpisiotomia();
		Assert.assertEquals(catalogoEpisiotomia, catalogo);
		logger.info("Datos catalogo episiotomia: {}", catalogo.get(0));
		logger.info("Datos catalogo episiotomia: {}", catalogo.get(1));
	}
	
	@Test
	public void validarEpisiotomiaTest() throws Exception{
		logger.info("Test validarEpisiotomiaTest: validando episiotomia no vacio");
		capturarTococirugiaServicesImpl.validarEpisiotomia("CON");
		logger.info("episiotomia valido");		
	}
	
	

	
}