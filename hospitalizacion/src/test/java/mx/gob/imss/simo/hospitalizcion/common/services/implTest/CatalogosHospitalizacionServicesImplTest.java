package mx.gob.imss.simo.hospitalizcion.common.services.implTest;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.repository.CatalogosHospitalizacionRepository;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.CatalogosHospitalizacionServicesImpl;
import mx.gob.imss.simo.hospitalizacion.intervencionesqx.rulesTest.CapturarIntervencionesQuirurgicasRulesTest;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CatalogosHospitalizacionServicesImpl.class)
public class CatalogosHospitalizacionServicesImplTest {

    @InjectMocks
    CatalogosHospitalizacionServicesImpl catalogosHospitalizacionServicesImpl;

    @Mock
    CatalogosHospitalizacionRepository catalogosHospitalizacionRepository;

    final static Logger logger = LoggerFactory.getLogger(CapturarIntervencionesQuirurgicasRulesTest.class);

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        catalogosHospitalizacionServicesImpl = new CatalogosHospitalizacionServicesImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void obtenerCatalogoEspecialidadPorTipoServicioTest() {

        String cvePresupuestal = "030103022151";
        int numTipoServicio = 2;
        List<DatosCatalogo> listCatalogos = new ArrayList<DatosCatalogo>();
        DatosCatalogo primerEspecialidad = new DatosCatalogo();
        primerEspecialidad.setClave("5001");
        primerEspecialidad.setDescripcion("Primer Contacto");

        DatosCatalogo segundaEspecialidad = new DatosCatalogo();
        segundaEspecialidad.setClave("5000");
        segundaEspecialidad.setDescripcion("Medicina de Urgencias");

        listCatalogos.add(primerEspecialidad);
        listCatalogos.add(segundaEspecialidad);

        try {

            when(catalogosHospitalizacionRepository.obtenerCatalogoEspecialidadPorTipoServicio(anyString(), anyInt()))
                    .thenReturn(listCatalogos);
            List<DatosCatalogo> resultTest = catalogosHospitalizacionServicesImpl
                    .obtenerCatalogoEspecialidadPorTipoServicio(cvePresupuestal, numTipoServicio);
            Assert.assertEquals(resultTest, listCatalogos);
            logger.info("obtenerCatalogoEspecialidadPorTipoServicioTest");
            logger.info("Lista de especialidades por tipo servicio: {}", resultTest.get(0));
            logger.info("Lista de especialidades por tipo servicio: {}", resultTest.get(1));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void obtenerProcedimientoRecienNacidoSinCeroTest(){
    	
    	List<DatosCatalogo> listCatalogos = new ArrayList<DatosCatalogo>();
    	DatosCatalogo procedimientoRN = new DatosCatalogo();
    	procedimientoRN.setClave("1");
    	procedimientoRN.setDescripcion("Pinzamiento tard�o del cord�n");
        listCatalogos.add(procedimientoRN);
    	
    	when(catalogosHospitalizacionRepository.obtenerProcedimientoRecienNacidoSinCero())
        .thenReturn(listCatalogos);
    	List<DatosCatalogo> resultTest = catalogosHospitalizacionServicesImpl.
    			obtenerProcedimientoRecienNacidoSinCero();
    	
    	 Assert.assertEquals(resultTest, listCatalogos);
    	 logger.info("obtenerProcedimientoRecienNacidoSinCeroTest");
         logger.info("Lista de procedimientos RN: {}", resultTest.get(0));
         
    }

}
