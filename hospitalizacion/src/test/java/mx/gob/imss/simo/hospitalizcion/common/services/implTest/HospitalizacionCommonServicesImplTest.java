package mx.gob.imss.simo.hospitalizcion.common.services.implTest;

import static org.mockito.Matchers.anyString;

//import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.mockito.Matchers.any;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.MetodoAnticonceptivo;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(HospitalizacionCommonServicesImpl.class)
public class HospitalizacionCommonServicesImplTest {

    @InjectMocks
    HospitalizacionCommonServicesImpl hospitalizacionCommonServicesImpl;
    
    @Mock
    HospitalizacionCommonHelper hospitalizacionCommonHelper;

    final static Logger logger = LoggerFactory.getLogger(HospitalizacionCommonServicesImplTest.class);
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        hospitalizacionCommonServicesImpl = new HospitalizacionCommonServicesImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void validarHoraTest() {

        String horaIngreso = "11:40";

        try {
            hospitalizacionCommonServicesImpl.validarHora(horaIngreso);
            logger.info("validarHoraTest");
            logger.info("Validaci�n de hora de ingreso exitosa: {}", horaIngreso);

        } catch (HospitalizacionException e) {
            e.printStackTrace();
            logger.info("validarHoraTest");
            logger.info("Ocurrio un error al validar hora de ingreso: {}", e);

        }

    }
    
    @Test
    public void validarPlanificacionEdadFecTest() throws HospitalizacionException, ParseException{
    	
    	Integer anios = new Integer(59);
    	MetodoAnticonceptivo metodoAnticonceptivo = new MetodoAnticonceptivo();
    	metodoAnticonceptivo.setClave(0);
    	metodoAnticonceptivo.setDescripcion("Ning�no");
    	metodoAnticonceptivo.setNumeroEdadFinal(59);
    	metodoAnticonceptivo.setNumeroEdadInicial(0);    	
    	String agregadoMedico = "2F1960OR";
    	
        	
    	try {
    		Date fechaCaptura = formatoFecha.parse("02/12/2019");
    		Mockito.when(hospitalizacionCommonHelper.edadPorAgregadoMedicoFec(anyString(),any(Date.class))).thenReturn(anios);
    		hospitalizacionCommonServicesImpl.validarPlanificacionEdadFec(metodoAnticonceptivo,agregadoMedico,fechaCaptura);
    		logger.info("validarPlanificacionEdadFec");
    		logger.info("Edad valida para el m�todo de planificaci�n familiar: {}", metodoAnticonceptivo.getDescripcion());
    		
    		
    	}catch (HospitalizacionException e) {
			e.printStackTrace();
			logger.info("validarPlanificacionEdadFec");
			logger.info("Edad no valida para el m�todo de planificaci�n familiar: {}", metodoAnticonceptivo.getDescripcion());

		}
    	
    }
}
