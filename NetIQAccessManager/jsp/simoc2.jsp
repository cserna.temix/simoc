<%@ page language="java" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.novell.nidp.common.provider.*" %>
<%@ page import="java.util.*" %> <%@ page import="com.novell.nidp.ui.*" %>
<%@ page import="com.novell.nidp.*" %>
<%@ page import="com.novell.nidp.servlets.*" %>
<%@ page import="com.novell.nidp.resource.*" %>
<%@ page import="com.novell.nidp.resource.jsp.*" %>
<%@ page import="com.novell.nidp.common.xml.w3c.*" %>
<% ContentHandler handler = new ContentHandler(request,response); %>

<html
        xmlns="http://www.w3.org/1999/xhtml"
        xmlns:th="http://www.thymeleaf.org"
        xmlns:tiles="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../simocscr/theme.css">
<link type="text/css" rel="stylesheet" href="../simocscr/simocentral.css">
<link type="text/css" rel="stylesheet" href="../simocscr/primefaces.css">
<script type="text/javascript" src="../simocscr/jquery.js"></script>
<script type="text/javascript" src="../simocscr/jquery-plugins.js"></script>
<title>Sistema de Informaci&oacute;n M&eacute;dico Operativo Central</title>
</head>
<body class="background_gris_oscuro">
        <script type="text/javascript">

                $(document).ready(function() {

                        var formIDPLogin       = $("form#IDPLogin");
                        var inputEcomUserID    = $("input#EcomUserID");
                        var inputEcom_Password = $("input#Ecom_Password");
                        var btnEntrar          = $("button#btnEntrar");
                        var growlContainer     = $("div#growl_container");
                        var growlUsuario       = $("div#growl_usuario");
                        var growlPassword      = $("div#growl_password");
                        var mensajeErrorNam    = "<%=(request.getAttribute(NIDPConstants.ATTR_LOGIN_ERROR) == null ? "" : (String)request.getAttribute(NIDPConstants.ATTR_LOGIN_ERROR))%>";
                        var GROWL_USUARIO      = "growl_usuario";
                        var GROWL_PASSWORD     = "growl_password";

                        inputEcomUserID.focus();

                        growl(GROWL_USUARIO, mensajeErrorNam, false);

                        inputEcomUserID.keydown(function(event) {

                                inputEcomUserID.removeClass("ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all")
                                .addClass("inputfield ui-state-active, ui-inputtext ui-state-active");

                                if ((event.keyCode == 13)) {
                                        inputEcom_Password.focus();
                                        event.preventDefault();
                                        return false;
                                }
                        });

                        inputEcom_Password.keydown(function(event) {

                                inputEcom_Password.removeClass("ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all")
                                .addClass("inputfield ui-state-active, ui-inputtext ui-state-active");

                                /*if ((event.keyCode == 13)) {
                                        btnEntrar.focus();
                                        event.preventDefault();
                                        return false;
                                }*/
                                if ((event.keyCode == 13) && (validateForm() == false)) {
                                        event.preventDefault();
                                        return false;
                                }else if((event.keyCode == 13) && (validateForm() == true)){
                                        formIDPLogin.submit();
                                }
                        });

                        btnEntrar.keydown(function(event) {
                                if ((event.keyCode == 13) && (validateForm() == false)) {
                                        event.preventDefault();
                                        return false;
                                }else{
                                        formIDPLogin.submit();
                                }
                        });


                        btnEntrar.click(function(event) {
                                if ((validateForm() == false)) {
                                        event.preventDefault();
                                }
                        });

                        growlUsuario.click(function(event) {
                                growlUsuario.html("");
                                event.preventDefault();

                        });

                        growlPassword.click(function(event) {
                                growlPassword.html("");
                                event.preventDefault();

                        });

                        function validateForm() {

                                var validarInputEcomUserID = true;
                                var validarInputEcom_Password = true;
                                var mensajeError = "";

                                if (inputEcomUserID.val() == ""){
                                        growl(GROWL_USUARIO, "Debe especificar usuario.", true);
                                        inputEcomUserID.focus();
                                        validarInputEcomUserID = false;
                                }

                                if(inputEcom_Password.val() == ""){
                                        growl(GROWL_PASSWORD, "Debe especificar contrase&ntilde;a.", true);
                                        inputEcomUserID.focus();
                                        validarInputEcom_Password = false;
                                }

                                return (validarInputEcomUserID && validarInputEcom_Password);
                        }

                        $(document).on('keydown',function(e){
                                key = e.keyCode;
                                if((key == 13) && (validateForm() == false)){
                                        event.preventDefault();
                                        return false;
                                }else if((key == 13) && (validateForm() == true)){
                                        formIDPLogin.submit();
                                }
                        });

                        function growl(id, mensaje, isError) {

                                if (mensaje != null && mensaje != "") {

                                        var imagencita = (isError ? "ui-growl-image-error" : "ui-growl-image-warn");

                                        var div = "";
                                        div += "<div  id=\"mensaje\" class=\"ui-growl-item-container ui-state-highlight ui-corner-all ui-helper-hidden ui-shadow\" aria-live=\"polite\" style=\"display: block;\">";
                                        div += "        <div class=\"ui-growl-item\">";
                                        div += "             <div class=\"ui-growl-icon-close ui-icon ui-icon-closethick\" style=\"display: none\"></div>";
                                        div += "                <span class=\"ui-growl-image " + imagencita + "\"></span>";
                                        div += "                <div class=\"ui-growl-message\">";
                                        div += "                        <span class=\"ui-growl-title\">" + mensaje + "</span>";
                                        div += "                        <p></p>";
                                        div += "                </div>";
                                        div += "                <div style=\"clear: both;\"></div>";
                                        div += "        </div>";
                                        div += "</div>";

                                        if(id == GROWL_USUARIO){
                                                growlUsuario.html(div);
                                        } else if(id == GROWL_PASSWORD){
                                                growlPassword.html(div);
                                        }else {
                                                growlContainer.html(growlContainer.html() + div);
                                        }
                                }
                        }

                        function limpiarGrowl(isLimpiarGrowl){

                                if(isLimpiarGrowl != null || isLimpiarGrowl){
                                        growlContainer.html("");
                                }

                        }
                });
        </script>

        <form id="IDPLogin" enctype="application/x-www-form-urlencoded" method="POST"  action="<%= (String) request.getAttribute("url") %>" autocomplete="off">

                <table class="noBorder ancho_100 background_gris_oscuro" >
                        <tbody>
                                <tr>
                                        <td class=ancho_10 background_gris_oscuro noBorder" style="height: 95px;">
                                        </td>
                                        <td class="ancho_40 align_l background_gris_oscuro noBorder">
                                                <img src="../simocscr/gobierno.png" class="resize_img">
                                        </td>
                                        <td class="ancho_40 align_r background_gris_oscuro noBorder">
                                                <img src="../simocscr/imss_header_blanco.png" class="resize_img">
                                        </td>
                                        <td class="ancho_10 background_gris_oscuro noBorder"></td>
                                </tr>
                        </tbody>
                </table>


                <hr class="lineaSeparacion">
                <p style="padding-top: 30px;"></p>
                <div class="align_c">
                        <label class="tituloHeaderPre ">ACCESO AL SISTEMA</label>
                </div>


                <div class="background_img_portada">
                        <div class="espacio_vertical espacio_horizontal"></div>
                        <table class="ui-panelgrid ui-widget noBorder ancho_100 background_img_none">
                                <tbody>
                                        <tr class="ui-widget-content ui-panelgrid-even noBorder background_img_none">
                                                <td></td>
                                                <td class="ui-panelgrid-cell ancho_15 noBorder background_img_none align_r">
                                                                Usuario: *
                                                </td>
                                                <td class="ui-panelgrid-cell ancho_15 noBorder background_img_none align_l">
                                                        <input
                                                                id="EcomUserID"
                                                                name="Ecom_User_ID"
                                                                type="text"
                                                                class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all">
                                                </td>
                                                <td class="ui-panelgrid-cell ancho_30 noBorder background_img_none align_l"></td>
                                        </tr>
                                        <tr class="ui-widget-content ui-panelgrid-odd noBorder background_img_none">
                                                <td></td>
                                                <td class="ui-panelgrid-cell noBorder background_img_none align_r">
                                                                Contrase&ntilde;a: *
                                                </td>
                                                <td class="ui-panelgrid-cell noBorder background_img_none">
                                                        <input
                                                                id="Ecom_Password"
                                                                name="Ecom_Password"
                                                                type="password"
                                                                class="ui-inputfield ui-password ui-widget ui-state-default ui-corner-all">
                                                </td>
                                                <td class="ui-panelgrid-cell noBorder background_img_none">
                                                        <button
                                                                id="btnEntrar"
                                                                name="btnEntrar"
                                                                class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                                                style="margin-right:20px;">
                                                                <span class="ui-button-text ui-c">Aceptar</span>
                                                        </button>
                                                </td>
                                        </tr>
                                </tbody>
                        </table>
                </div>



                <hr class="lineaSeparacion">
                <div class="align_c">
                        <label id="footer" class="ui-outputlabel ui-widget tituloFooterPre">
                                SIMOC v3.5 JULIO 2021 INSTITUTO MEXICANO DEL SEGURO SOCIAL 
                        </label>
                </div>
        </form>

        <div id="growl_container" class="ui-growl ui-widget" style="z-index: 1001;">
                <div id="growl_usuario"></div>
                <div id="growl_password"></div>
        </div>

</body>
</html>