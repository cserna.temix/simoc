package mx.gob.imss.simo.administracionusuario.common.service.impl;


import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import org.apache.log4j.BasicConfigurator;
//import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.administracionusuario.administracion.repository.impl.AdministracionRepositoryJDBCImpl;
import mx.gob.imss.simo.administracionusuario.common.helper.UsuarioHelper;
import mx.gob.imss.simo.administracionusuario.common.model.DatosSiap;
import mx.gob.imss.simo.administracionusuario.common.model.Parametro;
import mx.gob.imss.simo.administracionusuario.common.service.impl.SiapServiceImpl;

public class SiapServiceImplTests {
	
	
	final static Logger logger = LoggerFactory.getLogger(SiapServiceImplTests.class);
	
	@InjectMocks
	private SiapServiceImpl siapServiceImpl;
	
	@Mock
	private AdministracionRepositoryJDBCImpl adminisracionRepositoryImpl;
	
	@Mock
	private UsuarioHelper usuarioHelper;
	
	@BeforeClass
	public static void ini() {
		BasicConfigurator.configure();
	}

	@Before
	public void setup() {
		siapServiceImpl = PowerMockito.spy(new SiapServiceImpl());
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void consultarSiap() {
		try {
			logger.info("Inicia test consultarSiap");
			Parametro parametro = new Parametro();
			parametro.setClaveParametro("SIMO_OSB");
			parametro.setDescripcionParametro("Servicio WEB para consumir los servicios de SIAP y vigencia");
			parametro.setReferenciaParametro("http://172.16.23.247/SIMO-ESB/Core/PSSimoESB?wsdl");
			
			DatosSiap datosSiap = new DatosSiap();
			datosSiap.setApellidoMaterno("BECERRA");
			datosSiap.setApellidoPaterno("VELASCO");
			datosSiap.setNombre(" ROCIO DEL PILAR");
			datosSiap.setTipoContratacion("");
			datosSiap.setDelegacion("01");
			datosSiap.setMatricula("99352147");
			datosSiap.setConecta(Boolean.TRUE);
			
			when(adminisracionRepositoryImpl.obtenerParametro("SIMO_OSB")).thenReturn(parametro);
			when(usuarioHelper.crearDatosSiap(anyObject())).thenReturn(datosSiap);
			DatosSiap datosSiapRespuesta = siapServiceImpl.consultarSiap("99352147", "99");
			logger.info("Nombre "+ datosSiapRespuesta.getNombre());
			logger.info("Apellido Paterno: "+ datosSiapRespuesta.getApellidoPaterno());
			logger.info("Apellido Materno: "+ datosSiapRespuesta.getApellidoMaterno());
			
			logger.info("Finalizo exitosamente ");
		} catch (Exception e) {
			logger.error("Error "+ e.getMessage());
			e.printStackTrace();
		}
	}

	
}
