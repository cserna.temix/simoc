package mx.gob.imss.simo.administracionusuario.common.helper;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;
import mx.gob.imss.simo.administracionusuario.common.model.DatosSiap;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SicRol;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitPersonalOperativo;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitUnidadMedica;
import mx.gob.imss.simo.siap.jaxb.ConsultaSimoResponse;
import mx.gob.imss.simo.siap.jaxb2.ConsultaSimoResult;
import mx.gob.imss.simo.siap.jaxb2.NewDataSet;
import mx.gob.imss.simo.siap.jaxb2.Qry;




public class UsuarioHelperTest {

	@InjectMocks
	UsuarioHelper usuarioHelper;
	
	final static Logger logger = LoggerFactory.getLogger(UsuarioHelperTest.class);
	
	@BeforeClass
	public static void ini() {
		BasicConfigurator.configure();
	}

	@Before
	public void setup() {
		usuarioHelper = PowerMockito.spy(new UsuarioHelper());
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void crearDatosMedico(){
		
		DatosSiap datosSiap = factoryDatosSiap("34348839","Rosa","Rodriguez","Sanchez", true, null,"1","1","Base");
		Usuario datosUusuario = factoryDatosMedico("34348839","Rosa","Rodriguez","Sanchez");
		Usuario resultTest = usuarioHelper.crearDatosMedico(datosSiap);
		Assert.assertEquals(resultTest,datosUusuario);
		
		logger.info("Test crearDatosMedico");
        logger.info("Datos del M�dico: {}", resultTest);
		
		
	}
	
	@Test
	public void crearDatosMedicoMatricula(){
	
		Usuario datosUusuario = factoryDatosMedico("67348839",null,null,null);
		Usuario resultTest = usuarioHelper.crearDatosMedico("67348839");
		Assert.assertEquals(resultTest,datosUusuario);
		
		logger.info("Test crearDatosMedicoMatricula");
        logger.info("Datos del M�dico: {}", resultTest);
		
	}
	
	@Test
	public void crearDatosSiap(){
		
     ConsultaSimoResponse csr = factorySIMOResponse("Garcia","Lopez", "Lorena",(byte) 1, 98393, "Base");
     DatosSiap datosSiap = factoryDatosSiap("98393","Lorena","Lopez","Garcia", true, null,null,"1","Base");
     DatosSiap resultTest = usuarioHelper.crearDatosSiap(csr);
     Assert.assertEquals(resultTest,datosSiap);
     
     logger.info("Test crearDatosSiap");
     logger.info("Datos del M�dico obtenidos de SIAP: {}", resultTest);
     
	}
	
	@Test
	public void convertPersonalOperativo(){
		Usuario usuario = new Usuario();
		usuario.setFecBaja(null);
		usuario.setRefPersonalOperativo("brenda.martinezr");
		usuario.setRefEmail("brenda.martinezr@imss.gob.mx");
		usuario.setCveMatricula("67493498");
		usuario.setRefNombre("Brenda Ivonne");
		usuario.setRefApellidoPaterno("Mart�nez");
		usuario.setRefApellidoMaterno("Rodr�guez");
		usuario.setIndActivo(new Integer(1));
		usuario.setRefNumTelefono(null);
		
		SitPersonalOperativo resultTest = usuarioHelper.convertPersonalOperativo(usuario);
		Assert.assertNotNull(resultTest);
		
		logger.info("Test convertPersonalOperativo");
	    logger.info("Datos Personal Operativo: {}", resultTest);
				
	}
	
	@Test
	public void convertPersonalOperativoCompleto(){
		
		Rol rolUsuario = new Rol();
		rolUsuario.setCveRol("3");
		rolUsuario.setDesRol("JEFE DE ARIMAC");
		rolUsuario.setRefRol("ROLE_ARIMAC_JEFE");
		
		UnidadMedica unidadMedica = factoryUnidadMedica("365311012151","HGZ 27 Tlatelolco", new Date(), new Date(),
				"34", "Zacatecas");
		
		AsignacionUnidad asigncionUnidad = new AsignacionUnidad();
		asigncionUnidad.setDescTipoAsignacion("Titular");
		asigncionUnidad.setDesEstatus("Activo");
		asigncionUnidad.setIndActivoUnidad(true);
		asigncionUnidad.setIndTitular(true);
		asigncionUnidad.setRol(rolUsuario);
		asigncionUnidad.setUnidadMedica(unidadMedica);
		
		List<AsignacionUnidad> listUnidad = new ArrayList<AsignacionUnidad>();
		listUnidad.add(asigncionUnidad);
		
		Usuario usuario = new Usuario();
		usuario.setFecBaja(null);
		usuario.setRefPersonalOperativo("brenda.martinezr");
		usuario.setRefEmail("brenda.martinezr@imss.gob.mx");
		usuario.setCveMatricula("67493498");
		usuario.setRefNombre("Brenda Ivonne");
		usuario.setRefApellidoPaterno("Mart�nez");
		usuario.setRefApellidoMaterno("Rodr�guez");
		usuario.setIndActivo(new Integer(1));
		usuario.setRefNumTelefono(null);
		usuario.setAsignacionUnidad(listUnidad);
		
		SitPersonalOperativo resultTest = usuarioHelper.convertPersonalOperativoCompleto(usuario);
		Assert.assertNotNull(resultTest);
		
		logger.info("Test convertPersonalOperativoCompleto");
	    logger.info("Datos Personal Operativo: {}", resultTest);
			
	}
	
	@Test
	public void convertPersonalOperativoUsuario(){
		
		SitPersonalOperativo personalOperativo = factorySitPersonalOperativo(1, "8754874", null, 1, 0,
			"S�nchez", "G�mez", "Perla", "perla.sanchez", "perla.sanchez@imss.gob.mx", null,
			null, null);
		
		Usuario resultTest = usuarioHelper.convertPersonalOperativo(personalOperativo);
		Assert.assertNotNull(resultTest);
		
		logger.info("Test convertPersonalOperativoUsuario");
	    logger.info("Datos Objeto Usuario: {}", resultTest);
	}
	
	@Test
	public void convertUnidadesMedicas(){
		
		SitUnidadMedica unidadMedica = new SitUnidadMedica();
		unidadMedica.setIndActivoUnidad(1);
		unidadMedica.setCveDelegacion("34");
		unidadMedica.setCvePresupuestal("365311012151");
		unidadMedica.setDesDelegacion("Zacatecas");
		unidadMedica.setDesUnidadMedica("HGZ 27 Tlatelolco");
		unidadMedica.setFecAltaUnidad(null);
		unidadMedica.setFecBajaUnidad(null);
		List<SitUnidadMedica> unidadesMedicas = new ArrayList<SitUnidadMedica>();
		unidadesMedicas.add(unidadMedica);
		
		UnidadMedica unidadMedicaOracle = factoryUnidadMedica("365311012151","HGZ 27 Tlatelolco", null, null,
				"34", "Zacatecas");
		
		List<UnidadMedica> unidadesMedicasOracle = new ArrayList<UnidadMedica>();
		unidadesMedicasOracle.add(unidadMedicaOracle);
		
		List<UnidadMedica> resultTest = usuarioHelper.convertUnidadesMedicas(unidadesMedicas);
		
		Assert.assertEquals(resultTest,unidadesMedicasOracle);
		logger.info("Test convertUnidadesMedicas");
	    logger.info("Datos Lista de Unidades Medicas: {}", resultTest);
			
	}
	
	@Test
	public void convertUnidadMedica(){
		SitUnidadMedica unidadMedica = new SitUnidadMedica();
		unidadMedica.setIndActivoUnidad(1);
		unidadMedica.setCveDelegacion("34");
		unidadMedica.setCvePresupuestal("365311012151");
		unidadMedica.setDesDelegacion("Zacatecas");
		unidadMedica.setDesUnidadMedica("HGZ 27 Tlatelolco");
		
		UnidadMedica unidadMedicaOracle = factoryUnidadMedica("365311012151","HGZ 27 Tlatelolco", null, null,
				"34", "Zacatecas");
		
		UnidadMedica resultTest = usuarioHelper.convertUnidadMedica(unidadMedica);
		Assert.assertEquals(resultTest,unidadMedicaOracle);
		
		logger.info("Test convertUnidadMedica");
	    logger.info("Datos Unidad Medica: {}", resultTest);
		
	}
	
	@Test
	public void convertAsignacion(){
		SitUnidadMedica unidadMedica = new SitUnidadMedica();
		unidadMedica.setIndActivoUnidad(1);
		unidadMedica.setCveDelegacion("34");
		unidadMedica.setCvePresupuestal("365311012151");
		unidadMedica.setDesDelegacion("Zacatecas");
		unidadMedica.setDesUnidadMedica("HGZ 27 Tlatelolco");
		unidadMedica.setFecAltaUnidad(new Date());
		unidadMedica.setFecBajaUnidad(new Date());
		
		SicRol rol = new SicRol();
		rol.setCveRol("3");
		rol.setDesRol("JEFE DE ARIMAC");
		rol.setFecBaja(new Date());
		
		AsignacionUnidad resultTest = usuarioHelper.convertAsignacion(unidadMedica,rol);
		Assert.assertNotNull(resultTest);
		
		logger.info("Test convertAsignacion");
	    logger.info("Datos Asignaci�n de Usuario: {}", resultTest);
		
	}
	
	@Test
	public void convertSitUnidadMedicaMongo(){
		
		Rol rolUsuario = new Rol();
		rolUsuario.setCveRol("3");
		rolUsuario.setDesRol("JEFE DE ARIMAC");
		rolUsuario.setRefRol("ROLE_ARIMAC_JEFE");
		
		UnidadMedica unidadMedica = factoryUnidadMedica("365311012151","HGZ 27 Tlatelolco", new Date(), new Date(),
				"34", "Zacatecas");
		
		AsignacionUnidad asigncionUnidad = new AsignacionUnidad();
		asigncionUnidad.setDescTipoAsignacion("Titular");
		asigncionUnidad.setDesEstatus("Activo");
		asigncionUnidad.setIndActivoUnidad(true);
		asigncionUnidad.setIndTitular(true);
		asigncionUnidad.setRol(rolUsuario);
		asigncionUnidad.setUnidadMedica(unidadMedica);
		
		SitUnidadMedica resultTest = usuarioHelper.convertSitUnidadMedicaMongo(asigncionUnidad);
		Assert.assertNotNull(resultTest);
		
		logger.info("Test convertSitUnidadMedicaMongo");
	    logger.info("Datos Unidad Medica Mongo: {}", resultTest);
		
	}
	
	@Test
	public void convertRolMongo() throws ParseException{
		
		String stringFecha = "17/04/2020";
		DateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaBaja = fecha.parse(stringFecha);
		
		Rol rolUsuario = new Rol();
		rolUsuario.setCveRol("3");
		rolUsuario.setDesRol("JEFE DE ARIMAC");
		rolUsuario.setRefRol("ROLE_ARIMAC_JEFE");
		rolUsuario.setFecBaja(fechaBaja);
		
		SicRol rol = new SicRol();
		rol.setCveRol("ROLE_ARIMAC_JEFE");
		rol.setDesRol("JEFE DE ARIMAC");
		rol.setFecBaja(fechaBaja);
		
		SicRol resultTest = usuarioHelper.convertRolMongo(rolUsuario);
		Assert.assertEquals(resultTest.getCveRol(),rol.getCveRol());
		Assert.assertEquals(resultTest.getDesRol(),rol.getDesRol());
		Assert.assertEquals(resultTest.getFecBaja(),rol.getFecBaja());
		
		logger.info("Test convertRolMongo");
	    logger.info("Datos Rol Mongo: {}", resultTest);
	    
	}
	
	@Test
	public void convertRol(){
		
		SicRol rol = new SicRol();
		rol.setCveRol("ROLE_ARIMAC_CAPTURA");
		rol.setDesRol("USUARIO DE CAPTURA");
	
		Rol rolUsuario = new Rol();
		rolUsuario.setCveRol("4");
		rolUsuario.setDesRol("USUARIO DE CAPTURA");
		rolUsuario.setRefRol("ROLE_ARIMAC_CAPTURA");
		
		Rol resultTest = usuarioHelper.convertRol(rol);
		Assert.assertEquals(resultTest.getCveRol(),rol.getCveRol());
		Assert.assertEquals(resultTest.getDesRol(),rol.getDesRol());
		
		logger.info("Test convertRol");
	    logger.info("Datos Rol Usuario: {}", resultTest);
		
	}
	
	@Test
	public void obtenerListAsignaciones(){
		
		SitUnidadMedica unidadMedica = new SitUnidadMedica();
		unidadMedica.setIndActivoUnidad(1);
		unidadMedica.setCveDelegacion("34");
		unidadMedica.setCvePresupuestal("365311012151");
		unidadMedica.setDesDelegacion("Zacatecas");
		unidadMedica.setDesUnidadMedica("HGZ 27 Tlatelolco");
		unidadMedica.setFecAltaUnidad(null);
		unidadMedica.setFecBajaUnidad(null);
		List<SitUnidadMedica> unidadesMedicas = new ArrayList<SitUnidadMedica>();
		unidadesMedicas.add(unidadMedica);
		
		SicRol rol = new SicRol();
		rol.setCveRol("ROLE_ARIMAC_CAPTURA");
		rol.setDesRol("USUARIO DE CAPTURA");
		
		List<AsignacionUnidad> resultTest =  usuarioHelper.obtenerListAsignaciones(unidadesMedicas,rol);
		Assert.assertNotNull(resultTest);
		
		logger.info("Test obtenerListAsignaciones");
	    logger.info("Lista de Asignaciones: {}", resultTest);
		
		
	}
	
	@Test
	public void convertListPersonalOperativo(){
		
		SicRol rol = new SicRol();
		rol.setCveRol("ROLE_ARIMAC_JEFE");
		rol.setDesRol("JEFE DE ARIMAC");
		
		List<SicRol> listaRol = new ArrayList<SicRol>();
		listaRol.add(rol);
		
		SitUnidadMedica unidadMedica = new SitUnidadMedica();
		unidadMedica.setIndActivoUnidad(1);
		unidadMedica.setCveDelegacion("34");
		unidadMedica.setCvePresupuestal("365311012151");
		unidadMedica.setDesDelegacion("Zacatecas");
		unidadMedica.setDesUnidadMedica("HGZ 27 Tlatelolco");
		unidadMedica.setFecAltaUnidad(null);
		unidadMedica.setFecBajaUnidad(null);
		List<SitUnidadMedica> unidadesMedicas = new ArrayList<SitUnidadMedica>();
		unidadesMedicas.add(unidadMedica);
		
		
		SitPersonalOperativo personalOperativo = factorySitPersonalOperativo(1, "8754874", null, 1, 0,
				"S�nchez", "G�mez", "Perla", "perla.sanchez", "perla.sanchez@imss.gob.mx", null,
				listaRol, unidadesMedicas);
		
		List<SitPersonalOperativo> listPersonalOperativo = new ArrayList<SitPersonalOperativo>();
		listPersonalOperativo.add(personalOperativo);
		
		List<Usuario> resultTest = usuarioHelper.convertListPersonalOperativo(listPersonalOperativo);
		Assert.assertNotNull(resultTest);
		
		logger.info("Test convertListPersonalOperativo");
	    logger.info("Lista de Personal Operativo: {}", resultTest);
		
	}
	
	private UnidadMedica factoryUnidadMedica(String cvePresupuestal,String desUnidadMedica, Date fecAltaUnidad, Date fecBajaUnidad,
			String cveDelegacionImss, String desDelegacionImss){
		UnidadMedica unidaOracle= new UnidadMedica();
		unidaOracle.setCvePresupuestal(cvePresupuestal);
		unidaOracle.setDesPresupuestal(desUnidadMedica);
		unidaOracle.setFecAlta(fecAltaUnidad);
		unidaOracle.setFecBaja(fecBajaUnidad);
		unidaOracle.setCveDelegacionImss(cveDelegacionImss);
		unidaOracle.setDesDelegacionImss(desDelegacionImss);
		
		return unidaOracle;
	}
	
	private SitPersonalOperativo factorySitPersonalOperativo(Object caIntentos, String cveMatricula, Object fecBaja,
			int indActivo, int indBloqueado, String refApellidoMaterno, String refApellidoPaterno, String refNombre,
			String refCuentaActiveD, String refEmail, Object refNumTelefono, List<SicRol> sdRol, List<SitUnidadMedica> sdUnidadMedica) {
		SitPersonalOperativo personalOperativo = new SitPersonalOperativo();
		personalOperativo.setCaIntentos((Integer) caIntentos);
		personalOperativo.setCveMatricula(cveMatricula);
		personalOperativo.setFecBaja((Date)fecBaja);
		personalOperativo.setIndActivo(indActivo);
		personalOperativo.setIndBloqueado(indBloqueado);
		personalOperativo.setRefApellidoMaterno(refApellidoMaterno);
		personalOperativo.setRefApellidoPaterno(refApellidoPaterno);
		personalOperativo.setRefNombre(refNombre);
		personalOperativo.setRefCuentaActiveD(refCuentaActiveD);
		personalOperativo.setRefEmail(refEmail);
		personalOperativo.setRefNumTelefono((String)refNumTelefono);
		personalOperativo.setSdRol(sdRol);
		personalOperativo.setSdUnidadMedica(sdUnidadMedica);
		
		return personalOperativo;
	}

	private DatosSiap factoryDatosSiap(String matricula, String nombre,String apellidoPaterno, String apellidoMaterno,
			Boolean conecta, String descripcionError, String estatus, String delegacion, String tipoContratacion ){
		DatosSiap datosSiap = new DatosSiap();
		datosSiap.setMatricula(matricula);
		datosSiap.setNombre(nombre);
		datosSiap.setApellidoPaterno(apellidoPaterno);
		datosSiap.setApellidoMaterno(apellidoMaterno);
		datosSiap.setConecta(conecta);
		datosSiap.setDescripcionError(descripcionError);
		datosSiap.setEstatus(estatus);
		datosSiap.setDelegacion(delegacion);
		datosSiap.setTipoContratacion(tipoContratacion);
		return datosSiap;
		
	}
	
	private Usuario factoryDatosMedico(String cveMatricula, String refNombre, String refApellidoPaterno, String refApellidoMaterno){
		Usuario datosUsuario = new Usuario();
		datosUsuario.setCveMatricula(cveMatricula);
		datosUsuario.setRefApellidoMaterno(refApellidoMaterno);
		datosUsuario.setRefApellidoPaterno(refApellidoPaterno);
		datosUsuario.setRefNombre(refNombre);
		
		return datosUsuario;
		
	}
	
	/*private SitPersonalOperativo factorySitPersonalOperativo(Integer caIntentos, String cveMatricula, Date fecBaja, Boolean indActivo, int indBloqueado,
			String refApellidoMaterno, String refApellidoPaterno, String refNombre, String refCuentaActiveD, String refEmail, String refNumTelefono,
			List<SicRol> sdRol, List<SitUnidadMedica> sdUnidadMedica){
		SitPersonalOperativo personalOperativo = new SitPersonalOperativo();
		personalOperativo.setCaIntentos(caIntentos);
		personalOperativo.setCveMatricula(cveMatricula);
		personalOperativo.setFecBaja(fecBaja);
		personalOperativo.setIndActivo(indActivo);
		personalOperativo.setIndBloqueado(indBloqueado);
		personalOperativo.setRefApellidoMaterno(refApellidoMaterno);
		personalOperativo.setRefApellidoPaterno(refApellidoPaterno);
		personalOperativo.setRefNombre(refNombre);
		personalOperativo.setRefCuentaActiveD(refCuentaActiveD);
		personalOperativo.setRefEmail(refEmail);
		personalOperativo.setRefNumTelefono(refNumTelefono);
		personalOperativo.setSdRol(sdRol);
		personalOperativo.setSdUnidadMedica(sdUnidadMedica);
		
		return personalOperativo;
		
	}*/
	
	private ConsultaSimoResponse factorySIMOResponse(String aMaterno, String aPaterno, String nombre,
			byte delegacion, int matricula, String tipoContratacion){
		
	
		Qry qry = new Qry();
		qry.setAMATERNO(aMaterno);
		qry.setAPATERNO(aPaterno);
		qry.setNOMBRES(nombre);
		qry.setDELEGACION(delegacion);
		qry.setMATRICULA(matricula);
		qry.setTIPOCONTRATACION(tipoContratacion);
		
	
		
		NewDataSet newDataSet = new NewDataSet();
		newDataSet.setQry(qry);
		
		ConsultaSimoResult simoResult = new ConsultaSimoResult();
		simoResult.setNewDataSet(newDataSet);
	
		ConsultaSimoResponse csr = new ConsultaSimoResponse();
		csr.setConsultaSimoResult(simoResult);
		
		return csr;
		
		
	}
	
}
