package mx.gob.imss.simo.administracionusuario.common.service.impl;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;
import mx.gob.imss.simo.administracionusuario.common.repository.CatalogoRepository;


public class CatalogoServiceImplTest {

	@InjectMocks
	CatalogoServiceImpl catalogoServiceImpl;
	
	@Mock
	CatalogoRepository capturarIngresoRepository;
	
	final static Logger logger = LoggerFactory.getLogger(CatalogoServiceImplTest.class);
	
	@BeforeClass
	public static void ini() {
		BasicConfigurator.configure();
	}

	@Before
	public void setup() {
		catalogoServiceImpl = PowerMockito.spy(new CatalogoServiceImpl());
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void obtenerCatalogoRol(){
	   
		CatalogoObject catalogoRo11 = factoryCatalogoGenerico("1", "ADMINSITRADOR CENTRAL");
		CatalogoObject catalogoRo12 = factoryCatalogoGenerico("2", "ADMINISTRADOR DELEGACIONAL");
		CatalogoObject catalogoRo13 = factoryCatalogoGenerico("3", "JEFE DE ARIMAC");
		CatalogoObject catalogoRo14 = factoryCatalogoGenerico("4", "USUARIO DE CAPTURA");
		
		List<CatalogoObject> roles = new ArrayList<CatalogoObject>();
		roles.add(catalogoRo11);
		roles.add(catalogoRo12);
		roles.add(catalogoRo13);
		roles.add(catalogoRo14);
			
		when(capturarIngresoRepository.obtenerCatalogoRol()).thenReturn(roles);
        List<CatalogoObject> resultTest = catalogoServiceImpl.obtenerCatalogoRol();
        
        Assert.assertEquals(resultTest, roles);
        
        logger.info("Test obtenerCatalogoRol");
        logger.info("Catalogo obtenido: {}", resultTest);
	}
	
	@Test
	public void obtenerCatalogoTipoAsignacion(){
		
		CatalogoObject catalogoRo11 = factoryCatalogoGenerico("1", "ADMINSITRADOR CENTRAL");
		CatalogoObject catalogoRo12 = factoryCatalogoGenerico("2", "ADMINISTRADOR DELEGACIONAL");
		CatalogoObject catalogoRo13 = factoryCatalogoGenerico("3", "JEFE DE ARIMAC");
		CatalogoObject catalogoRo14 = factoryCatalogoGenerico("4", "USUARIO DE CAPTURA");
		
		List<CatalogoObject> roles = new ArrayList<CatalogoObject>();
		roles.add(catalogoRo11);
		roles.add(catalogoRo12);
		roles.add(catalogoRo13);
		roles.add(catalogoRo14);
			
		when(capturarIngresoRepository.obtenerCatalogoRol()).thenReturn(roles);
        List<CatalogoObject> resultTest = catalogoServiceImpl.obtenerCatalogoEstatus();
        
        Assert.assertEquals(resultTest, roles);
        
        logger.info("Test obtenerCatalogoTipoAsignacion");
        logger.info("Catalogo obtenido: {}", resultTest);
		
	}
	
	@Test
	public void obtenerCatalogoEstatus(){
		
		CatalogoObject catalogoRo11 = factoryCatalogoGenerico("1", "ADMINSITRADOR CENTRAL");
		CatalogoObject catalogoRo12 = factoryCatalogoGenerico("2", "ADMINISTRADOR DELEGACIONAL");
		CatalogoObject catalogoRo13 = factoryCatalogoGenerico("3", "JEFE DE ARIMAC");
		CatalogoObject catalogoRo14 = factoryCatalogoGenerico("4", "USUARIO DE CAPTURA");
		
		List<CatalogoObject> roles = new ArrayList<CatalogoObject>();
		roles.add(catalogoRo11);
		roles.add(catalogoRo12);
		roles.add(catalogoRo13);
		roles.add(catalogoRo14);
			
		when(capturarIngresoRepository.obtenerCatalogoRol()).thenReturn(roles);
        List<CatalogoObject> resultTest = catalogoServiceImpl.obtenerCatalogoEstatus();
        
        Assert.assertEquals(resultTest, roles);
        
        logger.info("Test obtenerCatalogoEstatus");
        logger.info("Catalogo obtenido: {}", resultTest);
		
	}
	
	@Test
	public void obtenerCatalogoDelegaciones(){
		
		CatalogoObject catalogoDel1 = factoryCatalogoGenerico("01", "Aguascalientes");
		CatalogoObject catalogoDel2 = factoryCatalogoGenerico("02", "Baja California");
		CatalogoObject catalogoDel3 = factoryCatalogoGenerico("03", "Baja California Sur");
		CatalogoObject catalogoDel4 = factoryCatalogoGenerico("04", "Campeche");
		CatalogoObject catalogoDel5 = factoryCatalogoGenerico("05", "Coahuila");
		
		List<CatalogoObject> delegaciones = new ArrayList<CatalogoObject>();
		delegaciones.add(catalogoDel1);
		delegaciones.add(catalogoDel2);
		delegaciones.add(catalogoDel3);
		delegaciones.add(catalogoDel4);
		delegaciones.add(catalogoDel5);
		
		when(capturarIngresoRepository.obtenerCatalogoGeneric(anyString(),anyString(),anyString())).thenReturn(delegaciones);
        List<CatalogoObject> resultTest = catalogoServiceImpl.obtenerCatalogoDelegaciones();
        
        Assert.assertEquals(resultTest, delegaciones);
        
        logger.info("Test obtenerCatalogoDelegaciones");
        logger.info("Catalogo obtenido: {}", resultTest);
				
	}
	
	@Test
	public void obtenerCatalogoUnidadesMedicas(){
		
		CatalogoObject catalogoUnidades1 = factoryCatalogoGenerico("090909090909", "División de Información en Salud");
		CatalogoObject catalogoUnidades2 = factoryCatalogoGenerico("000000000000", "No derechohabiente");
		
		List<CatalogoObject> unidadesMedicas = new ArrayList<CatalogoObject>();
		unidadesMedicas.add(catalogoUnidades1);
		unidadesMedicas.add(catalogoUnidades2);
		
		when(capturarIngresoRepository.obtenerCatalogoUnidadesMedicas(anyString(),anyString(),anyString())).thenReturn(unidadesMedicas);
		List<CatalogoObject> resultTest = catalogoServiceImpl.obtenerCatalogoUnidadesMedicas("09");
		Assert.assertEquals(resultTest,unidadesMedicas);
		
		logger.info("Test obtenerCatalogoUnidadesMedicas");
        logger.info("Catalogo obtenido: {}", resultTest);
		
	}
	
	private CatalogoObject factoryCatalogoGenerico(String clave, String descripcion){
		CatalogoObject catalogoRol = new CatalogoObject();
		catalogoRol.setClave(clave);
		catalogoRol.setDescripcion(descripcion);
		
		return catalogoRol;
		
	}
}
