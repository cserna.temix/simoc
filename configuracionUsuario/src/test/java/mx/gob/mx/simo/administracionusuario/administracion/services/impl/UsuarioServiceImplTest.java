package mx.gob.mx.simo.administracionusuario.administracion.services.impl;

import org.junit.Assert;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import mx.gob.imss.simo.administracionusuario.administracion.repository.UsuarioRepository;
import mx.gob.imss.simo.administracionusuario.administracion.repository.impl.UsuarioRepositoryJDBCImpl;
import mx.gob.imss.simo.administracionusuario.administracion.service.UsuarioService;
import mx.gob.imss.simo.administracionusuario.administracion.service.impl.UsuarioServiceImpl;
import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.helper.UsuarioHelper;
import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.mongodb.dao.AdministracionMongoDao;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SicRol;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitPersonalOperativo;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitUnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.utileria.UtileriaUsuario;

public class UsuarioServiceImplTest {
	
	@Mock
	private UsuarioHelper usuarioHelper;

	@Mock
	private UsuarioRepositoryJDBCImpl usuarioRepository;

	@Mock
	private AdministracionMongoDao administracionMongoDao;
	
	@InjectMocks
	private UsuarioServiceImpl usuarioService;

	final static Logger logger = LoggerFactory.getLogger(UsuarioServiceImplTest.class);
	
	@BeforeClass
	public static void ini() {
		BasicConfigurator.configure();
	}

	@Before
	public void setup() {
		usuarioService = PowerMockito.spy(new UsuarioServiceImpl());
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void listaUsuario() throws AdministracionUsuarioException{
		List<SicRol> sdRol= new ArrayList<SicRol>();
		SicRol sicRol = new SicRol();
		sicRol.setCveRol("ROLE_ARIMAC_CAPTURA");
		sicRol.setDesRol("USUARIO DE CAPTURA");
		sicRol.setFecBaja(null);
		sdRol.add(sicRol);
		
		List<SitUnidadMedica> sdUnidadMedica= new ArrayList<SitUnidadMedica>();
		SitUnidadMedica sitUnidadMedicaUno=factorySitUnidadMedica("39", "35A301182151", 
				"DF Norte", "HGP 3A Magdalena Salinas", null, null, 1, 1);
		
		SitUnidadMedica sitUnidadMedicaDos=factorySitUnidadMedica("12", "120205062151", 
				"Guerrero", "HGR 1 Vicente  Guerrero", null, null, 1, 1);
		sdUnidadMedica.add(sitUnidadMedicaUno);
		sdUnidadMedica.add(sitUnidadMedicaDos);
		SitPersonalOperativo sitPersonalOperativo = factorySitPersonalOperativo("Hector Miguel", "Jaso", "Garcia", "miguel.jaso",
				"miguel.jaso@imss.gob.mx", null, null, 0, "99091370", 
				0, null, 1,sdRol, sdUnidadMedica);
		
		
		List<SitPersonalOperativo> listaSitPersonalOperativo = new ArrayList<SitPersonalOperativo>();
		listaSitPersonalOperativo.add(sitPersonalOperativo);
		when(administracionMongoDao.buscarCuentaActiveD(sitPersonalOperativo.getRefCuentaActiveD())).thenReturn(listaSitPersonalOperativo);
		
		Rol rol3 = factoryRol("3", "ROLE_ARIMAC_JEFE", null);
		Rol rol1 = factoryRol("1", "ROLE_ADMIN_CENTRAL", null);
		
		UnidadMedica unidadMedicaOne= factoryUnidadMedica("02", "020501142151", null, null,"2");
		UnidadMedica unidadMedicaDos= factoryUnidadMedica("39", "35A301182151", null, null,"9");
		List<AsignacionUnidad> asignacionUnidad = new ArrayList<AsignacionUnidad>();
		AsignacionUnidad asignacionOne = factoryAsignacionUnidad(null, Boolean.TRUE, null,Boolean.FALSE, rol3, unidadMedicaOne);
		AsignacionUnidad asignacionDos = factoryAsignacionUnidad(null, Boolean.TRUE, null, Boolean.TRUE, rol1, unidadMedicaDos);
		asignacionUnidad.add(asignacionOne);
		asignacionUnidad.add(asignacionDos);
		Usuario usuario=factoryUsuario("02", "", "99091370", "020501142151", null, null, 1, 
				"miguel.jaso", null, null, "", "miguel.jaso@imss.gob.mx", 0, 0, asignacionUnidad);
		List<Usuario> listUsuario = new ArrayList<Usuario>();
		listUsuario.add(usuario);
		
		when(usuarioHelper.convertListPersonalOperativo(listaSitPersonalOperativo)).thenReturn(listUsuario);
		when(usuarioRepository.buscarUsuarioSimoc(sitPersonalOperativo.getRefCuentaActiveD())).thenReturn(listUsuario);
		List<Usuario> resultado = usuarioService.listaUsuario("miguel.jaso");
		Assert.assertEquals(resultado, listUsuario);
		
		logger.info("Test listaUsuario");
        logger.info("Lista de usuarios: {}", resultado);
				
	};
	
	@Test
	public void obtenerAsignaciones() throws AdministracionUsuarioException{
		List<SicRol> sdRol= new ArrayList<SicRol>();
		SicRol sicRol = new SicRol();
		sicRol.setCveRol("ROLE_ARIMAC_CAPTURA");
		sicRol.setDesRol("USUARIO DE CAPTURA");
		sicRol.setFecBaja(null);
		sdRol.add(sicRol);
		
		List<SitUnidadMedica> sdUnidadMedica= new ArrayList<SitUnidadMedica>();
		SitUnidadMedica sitUnidadMedicaUno=factorySitUnidadMedica("39", "35A301182151", 
				"DF Norte", "HGP 3A Magdalena Salinas", null, null, 1, 1);
		
		SitUnidadMedica sitUnidadMedicaDos=factorySitUnidadMedica("12", "120205062151", 
				"Guerrero", "HGR 1 Vicente  Guerrero", null, null, 1, 1);
		sdUnidadMedica.add(sitUnidadMedicaUno);
		sdUnidadMedica.add(sitUnidadMedicaDos);
		SitPersonalOperativo sitPersonalOperativo = factorySitPersonalOperativo("Hector Miguel", "Jaso", "Garcia", "miguel.jaso",
				"miguel.jaso@imss.gob.mx", null, null, 0, "99091370", 
				0, null, 1,sdRol, sdUnidadMedica);
		
		List<AsignacionUnidad> listAsignacion = new ArrayList<AsignacionUnidad>();
		Rol rol3 = factoryRol("3", "ROLE_ARIMAC_JEFE", null);
		Rol rol1 = factoryRol("1", "ROLE_ADMIN_CENTRAL", null);
		UnidadMedica unidadMedicaOne= factoryUnidadMedica("02", "020501142151", null, null,"2");
		UnidadMedica unidadMedicaDos= factoryUnidadMedica("39", "35A301182151", null, null,"9");
		AsignacionUnidad asignacionOne = factoryAsignacionUnidad(null, Boolean.TRUE, null,Boolean.FALSE, rol3, unidadMedicaOne);
		AsignacionUnidad asignacionDos = factoryAsignacionUnidad(null, Boolean.TRUE, null, Boolean.TRUE, rol1, unidadMedicaDos);
		listAsignacion.add(asignacionOne);
		listAsignacion.add(asignacionDos);
		when(administracionMongoDao.buscarUnidadesMedicas(sitPersonalOperativo.getRefCuentaActiveD())).thenReturn(sdUnidadMedica);
		when(administracionMongoDao.buscarRol(sitPersonalOperativo.getRefCuentaActiveD())).thenReturn(sicRol);
		when(usuarioHelper.obtenerListAsignaciones(sdUnidadMedica,sicRol)).thenReturn(listAsignacion);
		List<AsignacionUnidad> resultado = usuarioService.obtenerAsignaciones("miguel.jaso");
		Assert.assertEquals(resultado, listAsignacion);
		
		logger.info("Test obtenerAsignaciones");
        logger.info("Asiganciones: {}", resultado);
	}
	
	
	@Test
	public void insertarUsuario(){
		
		List<SicRol> sdRol= new ArrayList<SicRol>();
		SicRol sicRol = new SicRol();
		sicRol.setCveRol("ROLE_ARIMAC_JEFE");
		sicRol.setDesRol("JEFE DE ARIMAC");
		sicRol.setFecBaja(null);
		sdRol.add(sicRol);
		
		List<SitUnidadMedica> sdUnidadMedica= new ArrayList<SitUnidadMedica>();
		SitUnidadMedica sitUnidadMedicaUno=factorySitUnidadMedica("39", "35A301182151", 
				"DF Norte", "HGP 3A Magdalena Salinas", null, null, 1, 1);
		
		SitUnidadMedica sitUnidadMedicaDos=factorySitUnidadMedica("12", "120205062151", 
				"Guerrero", "HGR 1 Vicente  Guerrero", null, null, 1, 1);
		sdUnidadMedica.add(sitUnidadMedicaUno);
		sdUnidadMedica.add(sitUnidadMedicaDos);
		SitPersonalOperativo sitPersonalOperativo = factorySitPersonalOperativo("Hector Miguel", "Jaso", "Garcia", "miguel.jaso",
				"miguel.jaso@imss.gob.mx", null, null, 0, "99091370", 
				0, null, 1,sdRol, sdUnidadMedica);
		
		try{
			Rol rol3 = factoryRol("3", "ROLE_ARIMAC_JEFE", null);
			Rol rol1 = factoryRol("1", "ROLE_ADMIN_CENTRAL", null);
			
			UnidadMedica unidadMedicaOne= factoryUnidadMedica("02", "020501142151", null, null,"2");
			UnidadMedica unidadMedicaDos= factoryUnidadMedica("39", "35A301182151", null, null,"9");
			List<AsignacionUnidad> asignacionUnidad = new ArrayList<AsignacionUnidad>();
			AsignacionUnidad asignacionOne = factoryAsignacionUnidad(null, Boolean.TRUE, null,Boolean.FALSE, rol3, unidadMedicaOne);
			AsignacionUnidad asignacionDos = factoryAsignacionUnidad(null, Boolean.TRUE, null, Boolean.TRUE, rol1, unidadMedicaDos);
			asignacionUnidad.add(asignacionOne);
			asignacionUnidad.add(asignacionDos);
			Usuario usuario=factoryUsuario("02", "", "99091370", "020501142151", null, null, 1, 
					"miguel.jaso", null, null, "", "miguel.jaso@imss.gob.mx", 0, 0, asignacionUnidad);
			//usuarioRepository.altaUsuarioOperativo(usuario);
			
			when(usuarioRepository.obtenerRolById(usuario.getAsignacionUnidad().get(0).getRol().getCveRol())).thenReturn(rol3);
			doNothing().when(administracionMongoDao).guardarSitPersonalOperativo(sitPersonalOperativo);
			doNothing().when(usuarioRepository).altaUsuarioOperativo(usuario);
			doNothing().when(usuarioRepository).agregarAsignacion(anyString(),anyInt(), anyInt(), anyInt(),anyString(),any(Date.class),any(Date.class));
			usuarioService.insertarUsuario(usuario);
			
			logger.info("Test insertarUsuario");
	        logger.info("Usuario insertado en base de datos: {}", usuario);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void insertarRol(){
		Rol rol1 = factoryRefRol("1", "USUARIO DE CAPTURA","ROLE_ARIMAC_CAPTURA", null);
		SicRol sicRol = new SicRol();
		sicRol.setCveRol("ROLE_ARIMAC_CAPTURA");
		sicRol.setDesRol("USUARIO DE CAPTURA");
		sicRol.setFecBaja(null);
		
		Usuario usuario=factoryUsuario("02", "", "99091370", "020501142151", null, null, 1, 
				"miguel.jaso", null, null, "", "miguel.jaso@imss.gob.mx", 0, 0, null);
		try{
		when(usuarioRepository.obtenerRolById(anyString())).thenReturn(rol1);
		when(usuarioHelper.convertRolMongo(rol1)).thenReturn(sicRol);	
		when(administracionMongoDao.actualizarSicRol("miguel.jaso", sicRol)).thenReturn(Boolean.TRUE);
		doNothing().when(usuarioRepository).asignacionRol("miguel.jaso", UtileriaUsuario.convertSringToint(rol1.getCveRol()));
		usuarioService.insertarRol(rol1,usuario);
		
		logger.info("Test insertarRol");
        logger.info("Rol insertado en base de datos: {}", rol1);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Test
	public void insertarAsignacion(){
		
		Rol rol = factoryRol("1", "ROLE_ADMIN_CENTRAL", null);
		List<Rol> listaRol = new ArrayList<Rol>();
		listaRol.add(rol);
		
		UnidadMedica unidadMedicaOne= factoryUnidadMedica("02", "020501142151", null, null,"2");
		AsignacionUnidad asignacionOne = factoryAsignacionUnidad(null, Boolean.TRUE, null,Boolean.FALSE, rol, unidadMedicaOne);
		
		SitUnidadMedica sitUnidadMedicaUno=factorySitUnidadMedica("39", "35A301182151", 
				"DF Norte", "HGP 3A Magdalena Salinas", null, null, 1, 1);
		List<AsignacionUnidad> asignacionUnidad = new ArrayList<AsignacionUnidad>();
		asignacionUnidad.add(asignacionOne);
		Usuario usuario=factoryUsuario("02", "", "99091370", "020501142151", null, null, 1, 
				"miguel.jaso", null, null, "", "miguel.jaso@imss.gob.mx", 0, 0, asignacionUnidad);
		
		try{
			when(administracionMongoDao.guardarUnidadMedica("miguel.jaso",sitUnidadMedicaUno)).thenReturn(sitUnidadMedicaUno);
			when(usuarioHelper.convertSitUnidadMedicaMongo(asignacionOne)).thenReturn(sitUnidadMedicaUno);
			when(usuarioRepository.obtenerRol(anyString())).thenReturn(listaRol);
			doNothing().when(usuarioRepository).agregarAsignacion("35A301182151",0,1,UtileriaUsuario.convertSringToint(rol.getCveRol()),"miguel.jaso",null,null);
			usuarioService.insertarAsignacion(asignacionOne,usuario);
			
			logger.info("Test insertarAsignacion");
	        logger.info("Unidad m�dica asignada: {}", asignacionUnidad);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Test
	public void updateAsignacion(){
		Rol rol = factoryRol("1", "ROLE_ADMIN_CENTRAL", null);
		List<Rol> listaRol = new ArrayList<Rol>();
		listaRol.add(rol);
		
		UnidadMedica unidadMedicaOne= factoryUnidadMedica("02", "020501142151", null, null,"2");
		AsignacionUnidad asignacionOne = factoryAsignacionUnidad(null, Boolean.TRUE, null,Boolean.FALSE, rol, unidadMedicaOne);
		List<AsignacionUnidad> asignacionUnidad = new ArrayList<AsignacionUnidad>();
		asignacionUnidad.add(asignacionOne);
		SitUnidadMedica sitUnidadMedicaUno=factorySitUnidadMedica("39", "35A301182151", 
				"DF Norte", "HGP 3A Magdalena Salinas", null, null, 1, 1);
		
		Usuario usuario=factoryUsuario("02", "", "99091370", "020501142151", null, null, 1, 
				"miguel.jaso", null, null, "", "miguel.jaso@imss.gob.mx", 0, 0, asignacionUnidad);

		try {
			
			when(administracionMongoDao.actualizarUnidadMedica(anyString(),any(SitUnidadMedica.class), anyString())).thenReturn(sitUnidadMedicaUno);
			when(usuarioHelper.convertSitUnidadMedicaMongo(asignacionOne)).thenReturn(null);
			when(usuarioRepository.actualizarAsignacion("35A301182151",0,1,null, null, "miguel.jaso","35A301182151")).thenReturn(true);
			when(usuarioRepository.obtenerRol(anyString())).thenReturn(listaRol);
			doNothing().when(usuarioRepository).agregarAsignacion(anyString(),anyInt(),anyInt(),anyInt(),anyString(),any(Date.class),any(Date.class));
			usuarioService.updateAsignacion(asignacionOne,usuario,"020501142151");
			
			logger.info("Test updateAsignacion");
	        logger.info("Unidad m�dica actualizada: {}", asignacionUnidad);
			
		} catch (AdministracionUsuarioException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void obtenerAsignacionTitular(){
		List<SitUnidadMedica> listSitUnidadMedica = new ArrayList<SitUnidadMedica>();
		SitUnidadMedica sitUnidadMedica=factorySitUnidadMedica("39", "35A301182151", 
				"DF Norte", "HGP 3A Magdalena Salinas", null, null, 1, 1);
		listSitUnidadMedica.add(sitUnidadMedica);
		
		when(administracionMongoDao.buscarUnidadesMedicas(anyString())).thenReturn(listSitUnidadMedica);
		SitUnidadMedica resultado = usuarioService.obtenerAsignacionTitular("miguel.jaso");
		Assert.assertEquals(resultado, sitUnidadMedica);
		
		logger.info("Test obtenerAsignacionTitular");
        logger.info("Asignacion obtenida: {}", resultado);
	}
	
	@Test
	public void updateEstatusAsignacion(){
		when(administracionMongoDao.actualizarEstatusUnidadMedica("aldair.vidal",
				1, "35A301182151")).thenReturn(Boolean.TRUE);
		when(usuarioRepository.actualizarEstatusAsignacion("aldair.vidal", "35A301182151", 1)).thenReturn(Boolean.TRUE);
		Boolean resultado = usuarioService.updateEstatusAsignacion("aldair.vidal", "35A301182151", Boolean.TRUE);
		Assert.assertEquals(resultado, Boolean.TRUE);
		
		logger.info("Test updateEstatusAsignacion");
        logger.info("Estatus asignaci�n: {}", resultado);
	}
	
	@Test
	public void obtenerRol(){
		SicRol sicRol = factorySicRol("ROLE_ARIMAC_CAPTURA", "USUARIO DE CAPTURA", null);
		Rol rol = factoryRol("1", "USUARIO DE CAPTURA", null);
		when(administracionMongoDao.buscarRol(anyString())).thenReturn(sicRol);
				when(usuarioHelper.convertRol(any(SicRol.class))).thenReturn(rol);
				
		Rol resultado = usuarioService.obtenerRol("miguel.jaso");
		Assert.assertEquals(resultado, rol);
		logger.info("Test obtenerRol");
        logger.info("Rol: {}", resultado);
	}
	
	@Test
	public void updateEstatusAllAsig(){
		
		when(administracionMongoDao.actualizarEstatusAsignacionesUsuario("aldair.vidal", 1)).thenReturn(Boolean.TRUE);
		when(usuarioRepository.actualizarEstatusTodasAsigUsuario("aldair.vidal", 1)).thenReturn(Boolean.TRUE);
		Boolean resultado = usuarioService.updateEstatusAllAsig("aldair.vidal",Boolean.TRUE);
		
		Assert.assertEquals(resultado, Boolean.TRUE);
		
		logger.info("Test updateEstatusAllAsig");
        logger.info("Estatus asignaci�n: {}", resultado);
	}

	private SitPersonalOperativo factorySitPersonalOperativo(String refNombre, String refApellidoPaterno, String refApellidoMaterno, 
			String refCuentaActiveD, String refEmail, String refNumExtension,String refNumTelefono,Integer caIntentos
			,String cveMatricula,int indBloqueado,Date fecBaja,int indActivo,List<SicRol> sdRol,List<SitUnidadMedica> sdUnidadMedica){
		SitPersonalOperativo sitPersonalOperativo = new SitPersonalOperativo();
		sitPersonalOperativo.setRefNombre(refNombre);
		sitPersonalOperativo.setRefApellidoPaterno(refApellidoPaterno);
		sitPersonalOperativo.setRefApellidoMaterno(refApellidoMaterno);
		sitPersonalOperativo.setRefCuentaActiveD(refCuentaActiveD);
		sitPersonalOperativo.setRefEmail(refEmail);
		sitPersonalOperativo.setRefNumExtension(refNumExtension);
		sitPersonalOperativo.setRefNumTelefono(refNumTelefono);
		sitPersonalOperativo.setCaIntentos(caIntentos);
		sitPersonalOperativo.setCveMatricula(cveMatricula);
		sitPersonalOperativo.setFecBaja(fecBaja);
		sitPersonalOperativo.setIndBloqueado(indBloqueado);
		sitPersonalOperativo.setIndActivo(indActivo);
		sitPersonalOperativo.setSdRol(sdRol);
		sitPersonalOperativo.setSdUnidadMedica(sdUnidadMedica);
		return sitPersonalOperativo;
	}
	
	
	private SitUnidadMedica factorySitUnidadMedica(String cveDelegacion,
			String cvePresupuestal,String desDelegacion, String desUnidadMedica,Date fecAltaUnidad,
			Date fecBajaUnidad, Integer indActivoUnidad,int indTitular){
		SitUnidadMedica sitUnidadMedica = new SitUnidadMedica();
		sitUnidadMedica.setCveDelegacion(cveDelegacion);
		sitUnidadMedica.setDesDelegacion(desDelegacion);
		sitUnidadMedica.setCvePresupuestal(cvePresupuestal);
		sitUnidadMedica.setDesUnidadMedica(desUnidadMedica);
		sitUnidadMedica.setFecAltaUnidad(fecAltaUnidad);
		sitUnidadMedica.setFecBajaUnidad(fecBajaUnidad);
		sitUnidadMedica.setIndActivoUnidad(indActivoUnidad);
		sitUnidadMedica.setIndTitular(indTitular);
		return sitUnidadMedica;
	}
	
	private SicRol factorySicRol(String cveRol,String desRol, Date fecBaja){
		SicRol sicRol = new SicRol();
		sicRol.setCveRol(cveRol);
		sicRol.setDesRol(desRol);
		sicRol.setFecBaja(fecBaja);
		return sicRol;
	}
	
	private AsignacionUnidad factoryAsignacionUnidad(Date fecAltaUnidad, Boolean indActivoUnidad,Date fecBajaUnidad,Boolean indTitular,Rol rol, UnidadMedica unidadMedica){
		AsignacionUnidad asignacion = new AsignacionUnidad();
//		asignacion.setDescTipoAsignacion(descTipoAsignacion);
//		asignacion.setDesEstatus(desEstatus);
		asignacion.setFecAltaUnidad(fecAltaUnidad);
		asignacion.setIndActivoUnidad(indActivoUnidad);
		asignacion.setFecBajaUnidad(fecBajaUnidad);
		asignacion.setIndTitular(indTitular);
		asignacion.setRol(rol);
		asignacion.setUnidadMedica(unidadMedica);
		return asignacion;
	}
	
	private Rol factoryRol(String cveRol,String desRol, Date fecBaja){
		Rol rol = new Rol();
		rol.setCveRol(cveRol);
		rol.setDesRol(desRol);
		rol.setFecBaja(fecBaja);
		return rol;
	}
	
	private Rol factoryRefRol(String cveRol,String desRol, String refRol, Date fecBaja){
		Rol rol = new Rol();
		rol.setCveRol(cveRol);
		rol.setDesRol(desRol);
		rol.setRefRol(refRol);
		rol.setFecBaja(fecBaja);
		return rol;
	}
	
	private UnidadMedica factoryUnidadMedica(String cveDelegacionImss,String cvePresupuestal,Date fecAlta,Date fecBaja,String cveEntidadFed){
		UnidadMedica unidadMedica = new UnidadMedica();
		unidadMedica.setCveDelegacionImss(cveDelegacionImss);
//		unidadMedica.setDesDelegacionImss(desDelegacionImss);
		unidadMedica.setCvePresupuestal(cvePresupuestal);
		unidadMedica.setFecAlta(fecAlta);
		unidadMedica.setFecBaja(fecBaja);
//		unidadMedica.setRefPersonalOperativo(refPersonalOperativo);
		unidadMedica.setCveEntidadFed(cveEntidadFed);
		return unidadMedica;
	}
	
	private Usuario factoryUsuario(String cveDelegacion,String descDelegacion, String cveMatricula, 
			String cvePresupuestal, Date fecAlta,Date fecBaja, int indActivo,
			String refPersonalOperativo,String refNumTelefono,String refNumExtension,String refNombreCompleto,
			String refEmail,Integer consecutivo,Integer caIntentos,List<AsignacionUnidad> asignacionUnidad){
		Usuario usuario= new Usuario();
		usuario.setCveDelegacion(cveDelegacion);
		usuario.setDescDelegacion(descDelegacion);
		usuario.setCveMatricula(cveMatricula);
		usuario.setCvePresupuestal(cvePresupuestal);
		usuario.setFecAlta(fecAlta);
		usuario.setFecBaja(fecBaja);
		usuario.setIndActivo(indActivo);
		usuario.setRefPersonalOperativo(refPersonalOperativo);
		usuario.setRefNumTelefono(refNumTelefono);
		usuario.setRefNumExtension(refNumExtension);
		usuario.setRefNombreCompleto(refNombreCompleto);
		usuario.setRefEmail(refEmail);
		usuario.setConsecutivo(consecutivo);
		usuario.setCaIntentos(caIntentos);
		usuario.setAsignacionUnidad(asignacionUnidad);
		return usuario;
	}
}
