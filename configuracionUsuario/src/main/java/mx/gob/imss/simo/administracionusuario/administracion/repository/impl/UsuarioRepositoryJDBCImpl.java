package mx.gob.imss.simo.administracionusuario.administracion.repository.impl;

import java.util.Date;
import java.util.List;

import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.administracionusuario.administracion.repository.UsuarioRepository;
import mx.gob.imss.simo.administracionusuario.common.constant.SQLConstants;
import mx.gob.imss.simo.administracionusuario.common.helper.AsignacionRowMapperHelper;
import mx.gob.imss.simo.administracionusuario.common.helper.ParametroRowMapperHelper;
import mx.gob.imss.simo.administracionusuario.common.helper.RolRowMapperHelper;
import mx.gob.imss.simo.administracionusuario.common.helper.UsuarioHelper;
import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.Parametro;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.repository.BaseJDBCRepository;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;


@Repository
public class UsuarioRepositoryJDBCImpl extends BaseJDBCRepository
        implements UsuarioRepository {

	@Override
	public List<AsignacionUnidad> obtenerAsignaciones(String usuario) {
		return jdbcTemplate.query(SQLConstants.OBTENER_UNIDADES_MEDICAS_POR_CUENTA_AD, new Object[] {usuario},
              new AsignacionRowMapperHelper());
	}

	@Override
	public List<Usuario> buscarUsuarioSimoc(String cveDominio) {
		return jdbcTemplate.query(SQLConstants.BUSCAR_USUARIO_SIMOC, new Object[] {cveDominio},
	              new UsuarioHelper());
	}
    

	@Override
	public void altaUsuarioOperativo (Usuario personalOperativo) {
		jdbcTemplate.update(SQLConstants.ALTA_USUARIO_OPERATIVO, new Object[] {personalOperativo.getRefPersonalOperativo(),
				personalOperativo.getCveMatricula(),personalOperativo.getRefNombre(), personalOperativo.getRefApellidoPaterno(), 
				personalOperativo.getRefApellidoMaterno(), personalOperativo.getRefEmail(), null, null,personalOperativo.getIndActivo(),
				personalOperativo.getIndBloqueado(), personalOperativo.getCaIntentos(), personalOperativo.getFecBaja()});
	};

	
	@Override
	public void asignacionRol (String cveCuentaDominio,int cveRol) {
		jdbcTemplate.update(SQLConstants.ASIGNACION_ROL, new Object [] {cveRol, cveCuentaDominio});
	};
	
	@Override
	public boolean actualizarAsignacion (String cvePresupuestal,int indTitular, int indEstatus,Date fecBaja, Date fecAlta, String cveCuentaDominio,String cvePresupuestalEditar){
		int rowUpdated = jdbcTemplate.update(SQLConstants.ACTUALIZAR_ASIGNACION, new Object [] {cvePresupuestal,indTitular, indEstatus,fecBaja,fecAlta,cveCuentaDominio,cvePresupuestalEditar});
		return rowUpdated==1;
	}

	@Override
	public void agregarAsignacion(String cvePresupuestal,int indTitular, int indEstatus,int cveRol,
			String cveCuentaDominio,Date fecAlta,Date fecBaja) {
		jdbcTemplate.update(SQLConstants.AGREGAR_ASIGNACION, new Object [] {cvePresupuestal, cveCuentaDominio,indTitular,cveRol,indEstatus,fecAlta, fecBaja});
	}

	@Override
	public List<Rol> obtenerRol(String cveCuentaDominio) {
		return jdbcTemplate.query(SQLConstants.BUSCAR_ROL, new Object[] {cveCuentaDominio},
	              new RolRowMapperHelper());
	}

	@Override
	public Parametro obtenerParamtro(String cveParametro) {
		Parametro parametro = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_PARAMETRO, new Object[] { cveParametro },
                new ParametroRowMapperHelper());
        return parametro;
	}

	@Override
	public Rol obtenerRolById(String idRol) {
		Rol rol = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_ROL_ID, new Object[] {idRol},
                new RolRowMapperHelper());
		return rol;
	}

	@Override
	public AsignacionUnidad buscarAsignacionUnidad(String cveDominio, String cvePresupuestal) {
		try{
		AsignacionUnidad asignacionUnidad = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_ASIGNACION_ID, new Object[] {cveDominio, cvePresupuestal},
                new AsignacionRowMapperHelper());
			return asignacionUnidad;
		}catch(IncorrectResultSizeDataAccessException e){
			logger.error("IncorrectResultSizeDataAccessException no se encontro ningun resultado");
			return null;
		}
	}

	@Override
	public boolean actualizarUsuarioOperativo(Usuario personalOperativo) {
		// TODO Auto-generated method stub
		int rowUpdate = jdbcTemplate.update(SQLConstants.ACTUALIZAR_USUARIO_OPERATIVO,
				new Object[] { personalOperativo.getCveMatricula(), personalOperativo.getRefNombre(),
						personalOperativo.getRefApellidoPaterno(), personalOperativo.getRefApellidoMaterno(),
						personalOperativo.getRefEmail(), personalOperativo.getRefNumTelefono(), personalOperativo.getRefNumExtension(),
						personalOperativo.getIndActivo(), personalOperativo.getIndBloqueado(),
						personalOperativo.getCaIntentos(), personalOperativo.getFecBaja(), personalOperativo.getRefPersonalOperativo()});
		return rowUpdate == 1;
	}

	@Override
	public boolean actualizarEstatusAsignacion(String cveCuentaDominio, String cvePresupuestal, int estatus) {
		int rowUpdate = jdbcTemplate.update(SQLConstants.ACTUALIZAR_STATUS_ASIGNACION,new Object[] {estatus,cveCuentaDominio,cvePresupuestal});
		return rowUpdate == 1;
	}

	@Override
	public boolean actualizarEstatusTodasAsigUsuario(String cveCuentaDominio, int estatus) {
		int rowUpdate = jdbcTemplate.update(SQLConstants.ACTUALIZAR_TODAS_ASIGNACION_ESTATUS,new Object[] {estatus,cveCuentaDominio});
		return rowUpdate >= 1;
	}

	@Override
	public boolean eliminarAsignacion(String cveCuentaDominio, String cvePresupuestal) {
		int rowUpdate = jdbcTemplate.update(SQLConstants.ELIMINAR_ASIGNACION,new Object[] {cveCuentaDominio,cvePresupuestal});
		return rowUpdate == 1;
	}

	@Override
	public boolean actualizarEstatusUsuarioOperativo(String personalOperativo, int estatus, Date fechaBaja) {
		int rowUpdate = jdbcTemplate.update(SQLConstants.ACTUALIZAR_ESTATUS_PERSONAL_OPERATIVO,new Object[] {estatus,fechaBaja,personalOperativo});
		return rowUpdate == 1;
	}

}
