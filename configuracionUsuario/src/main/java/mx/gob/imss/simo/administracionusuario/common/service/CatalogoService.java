package mx.gob.imss.simo.administracionusuario.common.service;

import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;

public interface CatalogoService {
	
	public List<CatalogoObject> obtenerCatalogoRol();
	
	public List<CatalogoObject> obtenerCatalogoTipoAsignacion();
	
	public List<CatalogoObject> obtenerCatalogoEstatus();
	
public List<CatalogoObject> obtenerCatalogoDelegaciones();
	
	public List<CatalogoObject> obtenerCatalogoUnidadesMedicas(String claveDelegacionImss);
}
