package mx.gob.imss.simo.administracionusuario.common.model;

import lombok.Data;

@Data
public class DatosSiap {

    private String matricula;
    private String delegacion;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String estatus;
    private String tipoContratacion;
    private String numeroError;
    private String descripcionError;
    private Boolean conecta;

}
