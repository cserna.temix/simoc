package mx.gob.imss.simo.administracionusuario.administracion.service;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.DatosSiap;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitUnidadMedica;

public interface UsuarioService {
	
	List<Usuario> listaUsuario(String cuentaDominio) throws AdministracionUsuarioException;
	
	List<AsignacionUnidad> obtenerAsignaciones(String cuentaDominio)  throws AdministracionUsuarioException;
	
	void insertarUsuario(Usuario usuario) throws AdministracionUsuarioException;
	
	void insertarRol(Rol rol,Usuario usuario) throws AdministracionUsuarioException;
	
	void insertarAsignacion(AsignacionUnidad asignacion,Usuario usuario) throws AdministracionUsuarioException;
	
	void updateAsignacion(AsignacionUnidad asignacion,Usuario usuario, String cvePresupuestal) throws AdministracionUsuarioException;
	
	SitUnidadMedica obtenerAsignacionTitular(String cuentaDominio);
	
	boolean updateEstatusAsignacion(String cuentaDominio, String cvePresupuestal, boolean estatus);
	
	Rol obtenerRol(String cuentaDominio);
	
	boolean updateEstatusAllAsig(String cuentaDominio, boolean estatus);
	
	boolean updateEstatusPersonalOperativo(String cuentaDominio, boolean estatus);
	
	Date calcularFechaBaja(int indTitular);
}
