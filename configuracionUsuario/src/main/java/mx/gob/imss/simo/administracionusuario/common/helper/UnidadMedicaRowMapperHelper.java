package mx.gob.imss.simo.administracionusuario.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.administracionusuario.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;



public class UnidadMedicaRowMapperHelper extends BaseHelper implements RowMapper<UnidadMedica>{

	@Override
	public UnidadMedica mapRow(ResultSet rs, int rowNum) throws SQLException {
		UnidadMedica unidadMedica = new UnidadMedica();
		unidadMedica.setCveDelegacionSiap(rs.getString(SQLColumnasConstants.CVE_DELEGACION_SIAP));
		unidadMedica.setCvePresupuestal(rs.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
		unidadMedica.setCveDelegacionImss(rs.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
		unidadMedica.setDesPresupuestal(rs.getString(SQLColumnasConstants.DES_UNIDAD_MEDICA));
		return unidadMedica;
	}

}
