package mx.gob.imss.simo.administracionusuario.common.mongodb.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.mongodb.core.MongoManager;
import mx.gob.imss.simo.administracionusuario.common.mongodb.dao.AdministracionMongoDao;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SicRol;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitPersonalOperativo;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitUnidadMedica;
//import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;


@Repository("administracionMongoDao")
public class AdministracionMongoDaoImpl implements AdministracionMongoDao {
	
	
	Logger logger = Logger.getLogger(AdministracionMongoDaoImpl.class);


	@Override
	public List<SitPersonalOperativo> buscarCuentaActiveD(String actDirectoryAccount) {
		return buscarUsuario("refCuentaActiveD",actDirectoryAccount);
	}

	@Override
	public List<SitPersonalOperativo> buscarMatricula(String matricula) {		
		return buscarUsuario("cveMatricula",matricula);
	}

	@Override
	public void guardarSitPersonalOperativo(SitPersonalOperativo sitPersonalOperativo) {
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
			ds.save(sitPersonalOperativo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public SitUnidadMedica guardarUnidadMedica(String actDirectoryAccount, SitUnidadMedica unidadMedica) {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
			System.out.println("guardarUnidadMedica");
			Query<SitPersonalOperativo> updateQuery=ds.createQuery(SitPersonalOperativo.class)
					.filter("refCuentaActiveD", actDirectoryAccount);
			System.out.println("guardarUnidadMedica");
			SitPersonalOperativo operativo = updateQuery.get();
			operativo.getSdUnidadMedica().add(unidadMedica);

			UpdateOperations<SitPersonalOperativo> ops = ds
					.createUpdateOperations(SitPersonalOperativo.class).set("sdUnidadMedica", operativo.getSdUnidadMedica());
			System.out.println("update");
			UpdateResults guardaUnidadResult = ds.update(updateQuery, ops);
			 if (guardaUnidadResult.getUpdatedExisting() || guardaUnidadResult.getInsertedCount() > 0) {
				 System.out.println("update exitoso");
	        	 return unidadMedica;
	         }else{
	        	 System.out.println("no update");
	        	 return null;
	         }
	}





	@Override
	public boolean actualizarSicRol(String actDirectoryAccount, SicRol sicRol) {
         Datastore ds = MongoManager.INSTANCE.getDatastore();
         Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class)
                 .filter("refCuentaActiveD", actDirectoryAccount);
         SitPersonalOperativo sitPersonalOperativo = updateQuery.get();
         sitPersonalOperativo.setSdRol(new ArrayList<>());
         SicRol rol = new SicRol();
         rol.setCveRol(sicRol.getCveRol());
         rol.setDesRol(sicRol.getDesRol());
         rol.setFecBaja(null);
         sitPersonalOperativo.getSdRol().add(rol);
         UpdateOperations<SitPersonalOperativo> ops = ds.createUpdateOperations(SitPersonalOperativo.class)
                 .set("sdRol",sitPersonalOperativo.getSdRol());
         UpdateResults actualizaUrnidadResult = ds.update(updateQuery, ops);
         return actualizaUrnidadResult.getUpdatedExisting() || actualizaUrnidadResult.getInsertedCount() > 0;
		
	}

	
	private List<SitPersonalOperativo> buscarUsuario(String campo,String username) {
		logger.info("campo "+ campo + "  username "+ username);
		List<SitPersonalOperativo> usuarios = new ArrayList<>();
		try {
		Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
		
		SitPersonalOperativo usuario = dsEsp.createQuery(SitPersonalOperativo.class)
				.filter("refCuentaActiveD",username).get();
		
		logger.info(usuario.toString());
		usuarios.add(usuario);
		logger.info(usuarios.size());
		return usuarios;
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return usuarios;
	}

	@Override
	public SitUnidadMedica buscarUnidadMedica(String actDirectoryAccount,String cvePresupuestal) {
		logger.info("buscarUnidadMedica: "+ actDirectoryAccount+" ,"+cvePresupuestal);
		Datastore ds = MongoManager.INSTANCE.getDatastore();
		 Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class);
		 updateQuery.field("refCuentaActiveD").equal(actDirectoryAccount);
		List<SitUnidadMedica> listUnidades = updateQuery.get().getSdUnidadMedica();
		SitUnidadMedica sitUnidadRetorno=null;
		logger.info("listUnidades : "+listUnidades.size());
		if(listUnidades!=null && !listUnidades.isEmpty()){
			for(SitUnidadMedica sitUnidadMedica:listUnidades){
				logger.info("elemento de la lista : "+ sitUnidadMedica.getCvePresupuestal() +" vs "+ cvePresupuestal);
				if(cvePresupuestal.trim().equals(sitUnidadMedica.getCvePresupuestal().trim())){
					sitUnidadRetorno=sitUnidadMedica;
					break;
				}
			}			
		}
		return sitUnidadRetorno;
	}

	@Override
	public SicRol buscarRol(String actDirectoryAccount) {
		Datastore ds = MongoManager.INSTANCE.getDatastore();
		 Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class).filter("refCuentaActiveD", actDirectoryAccount);
		return updateQuery.get().getSdRol().get(0);
	}

	@Override
	public SitUnidadMedica actualizarUnidadMedica(String actDirectoryAccount, SitUnidadMedica unidadMedica, String cvePresupuestal) {
		Datastore ds = MongoManager.INSTANCE.getDatastore();
		 
		 Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class);
		updateQuery.filter("refCuentaActiveD", actDirectoryAccount);
		 SitPersonalOperativo sitPersonalOperativo = updateQuery.get();
		 
		 for(int i = 0; i<sitPersonalOperativo.getSdUnidadMedica().size(); i++){
			 SitUnidadMedica sitUnidadMedica = sitPersonalOperativo.getSdUnidadMedica().get(i);
			 if(sitUnidadMedica.getCvePresupuestal().equals(cvePresupuestal)){
				 sitPersonalOperativo.getSdUnidadMedica().set(i, unidadMedica);
			 }
		 }
		 

         UpdateOperations<SitPersonalOperativo> ops = ds.createUpdateOperations(SitPersonalOperativo.class)
                 .set("sdUnidadMedica", sitPersonalOperativo.getSdUnidadMedica());
         
         UpdateResults actualizaUrnidadResult = ds.update(updateQuery, ops);
         if (actualizaUrnidadResult.getUpdatedExisting() || actualizaUrnidadResult.getInsertedCount() > 0) {
        	 return unidadMedica;
         }else{
        	 return null;
         }
	}

	@Override
	public boolean actualizarEstatusPersonalOperativo(String actDirectoryAccount, Integer estatus, Date fechaBaja) {
		Datastore ds = MongoManager.INSTANCE.getDatastore();
//		Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class)
//                .filter("refCuentaActiveD", actDirectoryAccount);
//        SitPersonalOperativo sitPersonalOperativo = updateQuery.get();
//        Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class).
		 Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class)
                 .filter("refCuentaActiveD", actDirectoryAccount);
			 UpdateOperations<SitPersonalOperativo> ops = ds.createUpdateOperations(SitPersonalOperativo.class).set("indActivo",estatus);
			 if(fechaBaja==null){
//				 ops.set("fecBaja", "'null'");
			 }else{
				 ops.set("fecBaja", fechaBaja);
			 }
			 logger.error("El intEstatus : " + estatus);
			logger.error("La fechaBaja : " + fechaBaja);
		 UpdateResults update= ds.update(updateQuery, ops);
		return update.getUpdatedExisting() || update.getInsertedCount() > 0;
	}

	@Override
	public List<SitUnidadMedica> buscarUnidadesMedicas(String actDirectoryAccount) {
		Datastore ds = MongoManager.INSTANCE.getDatastore();
		 Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class);
		 updateQuery.field("refCuentaActiveD")
        .equal(actDirectoryAccount);
		return updateQuery.get().getSdUnidadMedica();
	}

	@Override
	public boolean actualizarEstatusUnidadMedica(String actDirectoryAccount, int indEstatus,
			String cvePresupuestal) {
		
		Datastore ds = MongoManager.INSTANCE.getDatastore();
		Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class)
                .filter("refCuentaActiveD", actDirectoryAccount);
		
		SitPersonalOperativo personalOperativo = updateQuery.get();
		for(SitUnidadMedica unidad : personalOperativo.getSdUnidadMedica()){
			if(unidad.getCvePresupuestal().equals(cvePresupuestal)){
				unidad.setIndActivoUnidad(indEstatus);
				break;
			}
		}
		
		 UpdateOperations<SitPersonalOperativo> ops = ds.createUpdateOperations(SitPersonalOperativo.class)
                .set("sdUnidadMedica",personalOperativo.getSdUnidadMedica());
		UpdateResults update=ds.update(updateQuery, ops);
		return update.getUpdatedExisting() || update.getInsertedCount() > 0;
	}

	@Override
	public boolean actualizarEstatusAsignacionesUsuario(String actDirectoryAccount, int indEstatus) {
		Datastore ds = MongoManager.INSTANCE.getDatastore();
		Query<SitPersonalOperativo> updateQuery = ds.createQuery(SitPersonalOperativo.class)
                .filter("refCuentaActiveD", actDirectoryAccount);
		
		SitPersonalOperativo personalOperativo = updateQuery.get();
		Iterator<SitUnidadMedica> iterator = personalOperativo.getSdUnidadMedica().iterator();
		List<SitUnidadMedica> listSitUnidad = new ArrayList<>();
		while(iterator.hasNext()) 
			{
			SitUnidadMedica sitUnidadMedica = iterator.next();
			sitUnidadMedica.setIndActivoUnidad(indEstatus);
			listSitUnidad.add(sitUnidadMedica);
		}
		
		 UpdateOperations<SitPersonalOperativo> ops = ds.createUpdateOperations(SitPersonalOperativo.class)
                .set("sdUnidadMedica",listSitUnidad);
		UpdateResults update=ds.update(updateQuery, ops);
		return update.getUpdatedExisting() || update.getInsertedCount() > 0;
	}

}
