package mx.gob.imss.simo.administracionusuario.common.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.administracionusuario.common.constant.BaseConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.SQLConstants;
import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;
import mx.gob.imss.simo.administracionusuario.common.repository.CatalogoRepository;
import mx.gob.imss.simo.administracionusuario.common.service.CatalogoService;

@Service("catalogosAdministracionServices")
public class CatalogoServiceImpl implements CatalogoService{
	
	@Autowired
    private CatalogoRepository capturarIngresoRepository;

	@Override
	public List<CatalogoObject> obtenerCatalogoRol() {
		List<CatalogoObject> roles = new ArrayList<CatalogoObject>();
		roles = capturarIngresoRepository.obtenerCatalogoRol();
	    return roles;
	}

	@Override
	public List<CatalogoObject> obtenerCatalogoTipoAsignacion() {
		List<CatalogoObject> roles = new ArrayList<CatalogoObject>();
		roles = capturarIngresoRepository.obtenerCatalogoRol();
	    return roles;
	}

	@Override
	public List<CatalogoObject> obtenerCatalogoEstatus() {
		List<CatalogoObject> roles = new ArrayList<CatalogoObject>();
		roles = capturarIngresoRepository.obtenerCatalogoRol();
	    return roles;
	}

	@Override
	public List<CatalogoObject> obtenerCatalogoDelegaciones() {
		List<CatalogoObject> roles = new ArrayList<CatalogoObject>();
		roles = capturarIngresoRepository.obtenerCatalogoGeneric(SQLCatalogosConstants.OBTENER_CATALOGOS_DELEGACIONES,
				SQLColumnasConstants.CVE_DELEGACION_IMSS, SQLColumnasConstants.DES_DELEGACION_IMSS);
		return roles;
	}

	@Override
	public List<CatalogoObject> obtenerCatalogoUnidadesMedicas(String claveDelegacionImss) {
		List<CatalogoObject> roles = new ArrayList<CatalogoObject>();
		roles = capturarIngresoRepository.obtenerCatalogoUnidadesMedicas(claveDelegacionImss,
				SQLColumnasConstants.CVE_PRESUPUESTAL, SQLColumnasConstants.DES_UNIDAD_MEDICA);
	    return roles;
	}

	
}
