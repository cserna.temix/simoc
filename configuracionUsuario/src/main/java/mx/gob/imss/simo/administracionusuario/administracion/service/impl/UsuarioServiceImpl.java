package mx.gob.imss.simo.administracionusuario.administracion.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.administracionusuario.administracion.repository.UsuarioRepository;
import mx.gob.imss.simo.administracionusuario.administracion.service.UsuarioService;
import mx.gob.imss.simo.administracionusuario.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.helper.UsuarioHelper;
import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.mongodb.dao.AdministracionMongoDao;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitUnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.utileria.UtileriaUsuario;

@Service("usuarioService")
public class UsuarioServiceImpl implements UsuarioService {
	

	@Autowired
	private UsuarioHelper usuarioHelper;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private AdministracionMongoDao administracionMongoDao;

	protected final Logger logger = Logger.getLogger(getClass());

	public UsuarioHelper getUsuarioHelper() {
		return usuarioHelper;
	}

	public UsuarioRepository getUsuarioRepository() {
		return usuarioRepository;
	}

	public AdministracionMongoDao getAdministracionMongoDao() {
		return administracionMongoDao;
	}
	
	private static int CONST_ENTERO_POSITIVO = 1;
	private static int CONST_ENTERO_NEGATIVO = 0;
	
	private static String  CONST_STRING_POSITIVO = "1";
	private static String CONST_STRING_FALSE = "0";

	@Override
	public List<Usuario> listaUsuario(String cuentaDominio) throws AdministracionUsuarioException {
		List<Usuario> usuarios = obtenerUsuarioSimoc(cuentaDominio);
		if (usuarios != null && usuarios.size() == 1) {
			if (usuarios.get(0).getAsignacionUnidad() == null || usuarios.get(0).getAsignacionUnidad().isEmpty()) {
				List<AsignacionUnidad> asignaciones = buscarAsignaciones(cuentaDominio);
				if (asignaciones.isEmpty()) {
					logger.error("Error obtenerAsignaciones :Sin asignaciones cuentaDominio: " + cuentaDominio);
					throw new AdministracionUsuarioException(MensajesErrorConstants.ME_021);
				}
				usuarios.get(0).setAsignacionUnidad(asignaciones);
			}
		}
		return usuarios;
	}

	@Override
	public List<AsignacionUnidad> obtenerAsignaciones(String cuentaDominio) throws AdministracionUsuarioException {
		List<AsignacionUnidad> asignaciones = buscarAsignaciones(cuentaDominio);
		if (asignaciones.isEmpty()) {
			logger.error("Error obtenerAsignaciones :Sin asignaciones cuentaDominio: " + cuentaDominio);
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_021);
		}
		return asignaciones;
	}

	private List<Usuario> obtenerUsuarioSimoc(String cveDominio) throws AdministracionUsuarioException {
		try {
			//Mongo
			List<Usuario> usuariosMongo = new ArrayList<Usuario>();
			usuariosMongo = getUsuarioHelper()
					.convertListPersonalOperativo(getAdministracionMongoDao().buscarCuentaActiveD(cveDominio));
			//Oracle
			List<Usuario> usuariosSimoc = new ArrayList<Usuario>();
			usuariosSimoc = getUsuarioRepository().buscarUsuarioSimoc(cveDominio);
			if(usuariosMongo.equals(usuariosSimoc)){
				logger.error("Los valores son correctos  ");
			}
			return usuariosMongo;
		} catch (Exception e) {
			logger.error("Error obtenerUsuarioSimoc : cuentaDominio " + cveDominio + " " + e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_001B);
		}
	}

	private List<AsignacionUnidad> buscarAsignaciones(String cuentaDominio) throws AdministracionUsuarioException {
		try {
			List<AsignacionUnidad> asignacionesMongo = new ArrayList<AsignacionUnidad>();
			asignacionesMongo = getUsuarioHelper().obtenerListAsignaciones(
					getAdministracionMongoDao().buscarUnidadesMedicas(cuentaDominio),
					getAdministracionMongoDao().buscarRol(cuentaDominio));
			logger.info("estatus del 1 : "+asignacionesMongo.get(0).getIndActivoUnidad());
			return asignacionesMongo;
		} catch (Exception e) {
			logger.error("Error buscarAsignaciones : cuentaDominio " + cuentaDominio + " " + e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
		}
	}

	@Override
	@Transactional(rollbackFor = { AdministracionUsuarioException.class })
	public void insertarUsuario(Usuario usuario) throws AdministracionUsuarioException {

			usuario.getAsignacionUnidad().get(0).setFecAltaUnidad(new Date());
			usuario.getAsignacionUnidad().get(0).setFecBajaUnidad(calcularFechaBaja(usuario.getAsignacionUnidad().get(0).getIndTitular() ? 1 : 0));
		//Mongo
		Rol rol = getUsuarioRepository().obtenerRolById(usuario.getAsignacionUnidad().get(0).getRol().getCveRol());
		usuario.getAsignacionUnidad().get(0).setRol(rol);
		getAdministracionMongoDao().guardarSitPersonalOperativo(getUsuarioHelper().convertPersonalOperativoCompleto(usuario));
		
		//Oracle
		logger.info("Inicia guardado de usuario oracle");
		String refPersonalOperativo = usuario.getRefPersonalOperativo();
		AsignacionUnidad altaAsignacion =  usuario.getAsignacionUnidad().get(0);
		List<Usuario> usuarioList = getUsuarioRepository().buscarUsuarioSimoc(refPersonalOperativo);
		boolean seActualizoAsignacion= false;
		if(usuarioList == null || usuarioList.isEmpty()){
			logger.info("Se crea el usuario oracle");
			getUsuarioRepository().altaUsuarioOperativo(usuario);
		}else{
			logger.info("Si existe se actualiza el usuario oracle");
			getUsuarioRepository().actualizarUsuarioOperativo(usuario);
			Usuario usuarioOracle = usuarioList.get(0);
			usuarioOracle.setAsignacionUnidad(getUsuarioRepository().obtenerAsignaciones(usuarioOracle.getRefPersonalOperativo()));
			if(usuarioOracle.getAsignacionUnidad()!=null && !usuarioOracle.getAsignacionUnidad().isEmpty()){
				for(AsignacionUnidad asignacionUnidad : usuarioOracle.getAsignacionUnidad()){
					if(asignacionUnidad.getUnidadMedica().getCvePresupuestal().equals(altaAsignacion.getUnidadMedica().getCvePresupuestal())){
						getUsuarioRepository().actualizarAsignacion(altaAsignacion.getUnidadMedica().getCvePresupuestal(), 
								altaAsignacion.getIndTitular() ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO, altaAsignacion.getIndActivoUnidad() ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO,altaAsignacion.getUnidadMedica().getFecBaja(),altaAsignacion.getUnidadMedica().getFecAlta(), usuario.getRefPersonalOperativo(), usuario.getCvePresupuestal());
						seActualizoAsignacion=true;
					}else{
						logger.info("validacion de cvepresupuesta");
						getUsuarioRepository().eliminarAsignacion(refPersonalOperativo, asignacionUnidad.getUnidadMedica().getCvePresupuestal());
					}
				}
				getUsuarioRepository().asignacionRol(refPersonalOperativo, UtileriaUsuario.convertSringToint(altaAsignacion.getRol().getCveRol()));
			}
		}
		if(!seActualizoAsignacion){
			logger.info("Se inserta la asignacion si no existe");
				getUsuarioRepository().agregarAsignacion(altaAsignacion.getUnidadMedica().getCvePresupuestal(), altaAsignacion.getIndTitular()? CONST_ENTERO_POSITIVO :CONST_ENTERO_NEGATIVO,
						altaAsignacion.getIndActivoUnidad() ? CONST_ENTERO_POSITIVO :CONST_ENTERO_NEGATIVO,UtileriaUsuario.convertSringToint(altaAsignacion.getRol().getCveRol()), refPersonalOperativo, altaAsignacion.getFecAltaUnidad(),altaAsignacion.getFecBajaUnidad());
		}
	}

	@Override
	@Transactional(rollbackFor = { AdministracionUsuarioException.class })
	public void insertarRol(Rol rol, Usuario usuario) throws AdministracionUsuarioException {
		try {
			// Mongo
			String refPersonalOperativo = usuario.getRefPersonalOperativo();
			rol = getUsuarioRepository().obtenerRolById(rol.getCveRol());
			boolean seInsertoInMongo=false;
			if (rol != null && rol.getRefRol() != null && !rol.getRefRol().isEmpty()) {
				seInsertoInMongo = getAdministracionMongoDao()
						.actualizarSicRol(refPersonalOperativo, getUsuarioHelper().convertRolMongo(rol));
			} else {
				logger.error("Error insertarRol: usuario " + usuario);
				throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
			}
			if (seInsertoInMongo) {
				// oracle
				getUsuarioRepository().asignacionRol(refPersonalOperativo, UtileriaUsuario.convertSringToint(rol.getCveRol()));
			}else{
				logger.error("Error insertarRol: usuario " + usuario);
				throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
			}
		} catch (Exception e) {
			logger.error("Error insertarRol: usuario " + usuario + " " + e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
		}
	}

	@Override
	@Transactional(rollbackFor = { AdministracionUsuarioException.class })
	public void insertarAsignacion(AsignacionUnidad asignacion, Usuario usuario) throws AdministracionUsuarioException {
		SitUnidadMedica unidadMongo = null;
		try {
		// mongo
		asignacion.setFecAltaUnidad(new Date());
		asignacion.setFecBajaUnidad(calcularFechaBaja(asignacion.getIndTitular() ? 1 : 0));
		String refPersonalOperativo = usuario.getRefPersonalOperativo();
			unidadMongo = getAdministracionMongoDao().buscarUnidadMedica(refPersonalOperativo,
					asignacion.getUnidadMedica().getCvePresupuestal());
			if (unidadMongo == null) {
				logger.info("Se da alta");
				getAdministracionMongoDao().guardarUnidadMedica(refPersonalOperativo,
						getUsuarioHelper().convertSitUnidadMedicaMongo(asignacion));
				// oracle
				// Se busca si existe en oracle
				AsignacionUnidad unidadOracle = getUsuarioRepository().buscarAsignacionUnidad(
						usuario.getRefPersonalOperativo(), asignacion.getUnidadMedica().getCvePresupuestal());
				if (unidadOracle != null && unidadOracle.getUnidadMedica() != null) {
					// actualizar oracle
					logger.info("actualizar oracle el mismo que en mongo");
					getUsuarioRepository().actualizarAsignacion(asignacion.getUnidadMedica().getCvePresupuestal(),
							asignacion.getIndTitular() ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO,
							asignacion.getIndActivoUnidad() ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO,
							asignacion.getFecBajaUnidad(), asignacion.getFecAltaUnidad(), refPersonalOperativo,
							asignacion.getUnidadMedica().getCvePresupuestal());

				} else {
					logger.info("Se agrega asignacion que no existia en oracle");
					Rol rol = getUsuarioRepository().obtenerRol(refPersonalOperativo).get(0);
					getUsuarioRepository().agregarAsignacion(asignacion.getUnidadMedica().getCvePresupuestal(),
							asignacion.getIndTitular() ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO,
							asignacion.getIndActivoUnidad() ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO,
							UtileriaUsuario.convertSringToint(rol.getCveRol()), refPersonalOperativo,
							asignacion.getFecAltaUnidad(), asignacion.getFecBajaUnidad());
				}
			}
		} catch (Exception e) {
			logger.error("Error insertarAsignacion: " + e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
		}
		if(unidadMongo != null){
			logger.info("Ya se encuentra dada de alta la unidad en mongodb");
			 throw new AdministracionUsuarioException(MensajesErrorConstants.ME_110);
		}
	}

	@Override
	@Transactional(rollbackFor = { AdministracionUsuarioException.class })
	public void updateAsignacion(AsignacionUnidad asignacion, Usuario usuario, String cvePresupuestal) throws AdministracionUsuarioException {
		SitUnidadMedica unidadMongo = null;
		try {
			SitUnidadMedica unidadMedicaMongo = null;
			String refPersonalOperativo = usuario.getRefPersonalOperativo();
			unidadMongo = getAdministracionMongoDao().buscarUnidadMedica(refPersonalOperativo, asignacion.getUnidadMedica().getCvePresupuestal());
			
			if(asignacion.getUnidadMedica().getCvePresupuestal().equals(cvePresupuestal) || (!asignacion.getUnidadMedica().getCvePresupuestal().equals(cvePresupuestal) && unidadMongo == null)){
				if(!asignacion.getUnidadMedica().getCvePresupuestal().equals(cvePresupuestal) || 
						(asignacion.getFecAltaUnidad()==null || asignacion.getFecBajaUnidad()==null)){
					//Se actualiza unidadmedica
					logger.info("Se actualiza unidadmedica: "+asignacion.getUnidadMedica().getCvePresupuestal());
					asignacion.setFecAltaUnidad(new Date());
					asignacion.setFecBajaUnidad(calcularFechaBaja(asignacion.getIndTitular() ? 1 : 0));
				}
				
				// mongo
				unidadMedicaMongo = getAdministracionMongoDao().actualizarUnidadMedica(refPersonalOperativo,
						getUsuarioHelper().convertSitUnidadMedicaMongo(asignacion), cvePresupuestal);
				
			}
			// oracle
			if(unidadMedicaMongo!= null){
				AsignacionUnidad asignacionOracle = getUsuarioRepository().buscarAsignacionUnidad(refPersonalOperativo, cvePresupuestal);
				if(asignacionOracle!=null){
					getUsuarioRepository().actualizarAsignacion(asignacion.getUnidadMedica().getCvePresupuestal(), asignacion.getIndTitular() ? CONST_ENTERO_POSITIVO: CONST_ENTERO_NEGATIVO,
							asignacion.getIndActivoUnidad() ? CONST_ENTERO_POSITIVO: CONST_ENTERO_NEGATIVO, asignacion.getFecBajaUnidad(), asignacion.getFecAltaUnidad(),refPersonalOperativo,cvePresupuestal);
				}else{
					Rol rol = getUsuarioRepository().obtenerRol(refPersonalOperativo).get(0);
					getUsuarioRepository().agregarAsignacion(asignacion.getUnidadMedica().getCvePresupuestal(), asignacion.getIndTitular() ? CONST_ENTERO_POSITIVO: CONST_ENTERO_NEGATIVO, asignacion.getIndActivoUnidad() ? CONST_ENTERO_POSITIVO :CONST_ENTERO_NEGATIVO, UtileriaUsuario.convertSringToint(rol.getCveRol()), refPersonalOperativo,asignacion.getFecAltaUnidad(), asignacion.getFecBajaUnidad());
				}
			}
		} catch (Exception e) {
			logger.error("Error updateAsignacion : " + e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
		}
		if(!asignacion.getUnidadMedica().getCvePresupuestal().equals(cvePresupuestal) && unidadMongo!=null){
			logger.info("Ya se encuentra dada de alta en mongo");
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_110);
		}
	}


	@Override
	public SitUnidadMedica obtenerAsignacionTitular(String cuentaDominio) {
		List<SitUnidadMedica> asignaciones = getAdministracionMongoDao().buscarUnidadesMedicas(cuentaDominio);
		SitUnidadMedica asignacionUnidadTitular = null;
		for(SitUnidadMedica admini: asignaciones){
			if(admini.getIndTitular()==1){
				asignacionUnidadTitular = admini;
				break;
			}
		}
		return asignacionUnidadTitular;
	}

	@Override
	public Rol obtenerRol(String cuentaDominio) {
		return getUsuarioHelper().convertRol(getAdministracionMongoDao().buscarRol(cuentaDominio));
	}

	@Override
	public boolean updateEstatusAsignacion(String cuentaDominio, String cvePresupuestal, boolean estatus){
		boolean seActualizoMongo = false;
		boolean seActualizoOracle = false;
		seActualizoMongo = getAdministracionMongoDao().actualizarEstatusUnidadMedica(cuentaDominio,
				estatus ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO, cvePresupuestal);
		seActualizoOracle = getUsuarioRepository().actualizarEstatusAsignacion(cuentaDominio, cvePresupuestal, estatus ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO);
		return seActualizoMongo && seActualizoOracle;
	}

	@Override
	public boolean updateEstatusAllAsig(String cuentaDominio, boolean estatus) {
		boolean seActualizoMongo = false;
		boolean seActualizoOracle = false;
		seActualizoMongo = getAdministracionMongoDao().actualizarEstatusAsignacionesUsuario(cuentaDominio, estatus ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO);
		seActualizoOracle = getUsuarioRepository().actualizarEstatusTodasAsigUsuario(cuentaDominio, estatus ? CONST_ENTERO_POSITIVO : CONST_ENTERO_NEGATIVO);
		return seActualizoMongo && seActualizoOracle;
	}
	
	@Override
	public Date calcularFechaBaja(int indTitular){
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(new Date());
		if(indTitular == 1){
			calendario.add(Calendar.YEAR, 1);
		}else{
			calendario.add(Calendar.MONTH, 1);
		}
		return calendario.getTime();
	}

	@Override
	public boolean updateEstatusPersonalOperativo(String cuentaDominio, boolean estatus) {
		int intEstatus = 0;
		Date fechaBaja=null;
		if(estatus){
			intEstatus=1;
//			fechaBaja = new Date(0);
		}else{
			fechaBaja = new Date();
		}
		boolean seActualizoMongo = false;
		boolean seActualizoOracle = false;
		logger.error("El intEstatus : " + intEstatus);
		logger.error("La fechaBaja : " + fechaBaja);
		seActualizoOracle= getUsuarioRepository().actualizarEstatusUsuarioOperativo(cuentaDominio, intEstatus, fechaBaja);
		seActualizoMongo = getAdministracionMongoDao().actualizarEstatusPersonalOperativo(cuentaDominio, intEstatus, fechaBaja);
		return seActualizoMongo && seActualizoOracle;
	}
	
}
