package mx.gob.imss.simo.administracionusuario.common.enumeracion;

import java.util.ArrayList;
import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;

public enum EstatusEnum {
	
	ACTIVO("1", "Activo"), DESACTIVADOR("0","Inactivo");

	private String clave;
    private String descripcion;
    
    private EstatusEnum(String clave,String descripcion) {
        this.clave = clave;
        this.descripcion =descripcion;
    }
    
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public static EstatusEnum obtenerPorClave(String clave){
		if(clave !=null){
		for (EstatusEnum estatusEnum : EstatusEnum.values()) {
			if(estatusEnum.getClave().equals(clave)){
				return estatusEnum;
			}
		}
		}
		return null;
	}
	
	public static EstatusEnum obtenerPorDescripcion(String descripcion){
		if(descripcion !=null){
		for (EstatusEnum estatusEnum : EstatusEnum.values()) {
			if(estatusEnum.getDescripcion().equals(descripcion)){
				return estatusEnum;
			}
		}
		}
		return null;
	}
	
	
	public static List<CatalogoObject> obtenerCatalogo(){
		List<CatalogoObject> catalogos = new ArrayList<CatalogoObject>();
		for(EstatusEnum tipoValor : EstatusEnum.values()){
			catalogos.add(new CatalogoObject(tipoValor.getClave(),tipoValor.getDescripcion()));
		}
		return catalogos;
	}

}
