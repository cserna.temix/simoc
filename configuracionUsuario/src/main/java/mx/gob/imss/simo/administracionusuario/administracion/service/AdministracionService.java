package mx.gob.imss.simo.administracionusuario.administracion.service;


import mx.gob.imss.simo.administracionusuario.common.model.Parametro;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;

public interface AdministracionService {
	
	 Parametro obtenerParametro(String cveParametro);
	 
	 UnidadMedica obtenerDelegacionSiap(String cveDelegacionImss, String cvePresupuestal);
}
