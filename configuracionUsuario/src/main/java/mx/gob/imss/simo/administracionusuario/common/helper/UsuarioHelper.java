package mx.gob.imss.simo.administracionusuario.common.helper;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.administracionusuario.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.EstatusEnum;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.TipoAsignacionEnum;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.TipoAsignacionMongoEnum;
import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.DatosSiap;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SicRol;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitPersonalOperativo;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitUnidadMedica;
import mx.gob.imss.simo.siap.jaxb.ConsultaSimoResponse;

@Component
public class UsuarioHelper extends BaseHelper implements RowMapper<Usuario> {

    public Usuario crearDatosMedico(DatosSiap datosSiap) {

    	Usuario datosMedico = new Usuario();
        if (datosSiap != null) {
            datosMedico.setCveMatricula(datosSiap.getMatricula());
            datosMedico.setRefNombre(datosSiap.getNombre());
            datosMedico.setRefApellidoPaterno(datosSiap.getApellidoPaterno());
            datosMedico.setRefApellidoMaterno(datosSiap.getApellidoMaterno());

        }
        return datosMedico;
    }

    public Usuario crearDatosMedico(String matricula) {
    	Usuario usuario = new Usuario();
    	usuario.setCveMatricula(matricula);
        return usuario;
    }

    public DatosSiap crearDatosSiap(ConsultaSimoResponse csr) {

        DatosSiap datosSiap = new DatosSiap();
        datosSiap.setApellidoMaterno(csr.getConsultaSimoResult().getNewDataSet().getQry().getAMATERNO());
        datosSiap.setApellidoPaterno(csr.getConsultaSimoResult().getNewDataSet().getQry().getAPATERNO());
        datosSiap.setNombre(csr.getConsultaSimoResult().getNewDataSet().getQry().getNOMBRES());
        datosSiap.setTipoContratacion(csr.getConsultaSimoResult().getNewDataSet().getQry().getTIPOCONTRATACION());
        datosSiap.setDelegacion(String.valueOf(csr.getConsultaSimoResult().getNewDataSet().getQry().getDELEGACION()));
        datosSiap.setMatricula(String.valueOf(csr.getConsultaSimoResult().getNewDataSet().getQry().getMATRICULA()));
        datosSiap.setConecta(Boolean.TRUE);
        return datosSiap;
    }

	@Override
	public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
		Usuario usuario = new Usuario();
		usuario.setRefPersonalOperativo(rs.getNString(SQLColumnasConstants.REF_PERSONAL_OPERATIVO));
		usuario.setCveMatricula(rs.getNString(SQLColumnasConstants.CVE_MATRICULA));
		usuario.setRefNombre(rs.getNString(SQLColumnasConstants.REF_NOMBRE));
		usuario.setRefApellidoPaterno(rs.getNString(SQLColumnasConstants.REF_APELLIDO_PATERNO));
		usuario.setRefApellidoMaterno(rs.getNString(SQLColumnasConstants.REF_APELLIDO_MATERNO));
		usuario.setRefEmail(rs.getNString(SQLColumnasConstants.REF_EMAIL));
		usuario.setRefNumTelefono(rs.getString(SQLColumnasConstants.REF_NUM_TELEFONO));
		usuario.setRefNumExtension(rs.getString(SQLColumnasConstants.REF_NUM_EXTENSION));
		usuario.setCaIntentos(rs.getInt(SQLColumnasConstants.CAN_INTENTOS));
		usuario.setIndActivo(rs.getInt(SQLColumnasConstants.IND_ACTIVO));
		usuario.setIndBloqueado(rs.getInt(SQLColumnasConstants.IND_BLOQUEADO));
		usuario.setFecBaja(rs.getDate(SQLColumnasConstants.FEC_BAJA));
		return usuario;
	}

	
	public SitPersonalOperativo convertPersonalOperativo(Usuario usuario) {
		SitPersonalOperativo personalOperativo = new SitPersonalOperativo();
		personalOperativo.setFecBaja(usuario.getFecBaja());
		personalOperativo.setRefCuentaActiveD(usuario.getRefPersonalOperativo());
		personalOperativo.setRefEmail(usuario.getRefEmail());
		
		personalOperativo.setCveMatricula(usuario.getCveMatricula());
		personalOperativo.setRefNombre(usuario.getRefNombre());
		personalOperativo.setRefApellidoPaterno(usuario.getRefApellidoPaterno());
		personalOperativo.setRefApellidoMaterno(usuario.getRefApellidoMaterno());
		personalOperativo.setIndActivo(usuario.getIndActivo());
		personalOperativo.setRefNumTelefono(usuario.getRefNumTelefono());
		return personalOperativo;
	}
	
	public SitPersonalOperativo convertPersonalOperativoCompleto(Usuario usuario) {
		SitPersonalOperativo personalOperativo = new SitPersonalOperativo();
		personalOperativo.setFecBaja(usuario.getFecBaja());
		personalOperativo.setRefCuentaActiveD(usuario.getRefPersonalOperativo());
		personalOperativo.setRefEmail(usuario.getRefEmail());
		personalOperativo.setCveMatricula(usuario.getCveMatricula());
		personalOperativo.setRefNombre(usuario.getRefNombre());
		personalOperativo.setRefApellidoPaterno(usuario.getRefApellidoPaterno());
		personalOperativo.setRefApellidoMaterno(usuario.getRefApellidoMaterno());
		personalOperativo.setIndActivo(usuario.getIndActivo());
		personalOperativo.setRefNumTelefono(usuario.getRefNumTelefono());
		personalOperativo.setSdUnidadMedica(new ArrayList<SitUnidadMedica>());
		personalOperativo.getSdUnidadMedica().add(convertSitUnidadMedicaMongo(usuario.getAsignacionUnidad().get(0)));
		personalOperativo.setSdRol(new ArrayList<SicRol>());
		personalOperativo.getSdRol().add(convertRolMongo(usuario.getAsignacionUnidad().get(0).getRol()));
		personalOperativo.setCaIntentos(usuario.getCaIntentos());
		return personalOperativo;
	}
	
	public Usuario convertPersonalOperativo(SitPersonalOperativo sitPersonalOperativo) {
		Usuario usuario = new Usuario();
		usuario.setFecBaja(sitPersonalOperativo.getFecBaja());
		usuario.setRefPersonalOperativo(sitPersonalOperativo.getRefCuentaActiveD());
		usuario.setRefEmail(sitPersonalOperativo.getRefEmail());
		usuario.setCveMatricula(sitPersonalOperativo.getCveMatricula());
		usuario.setRefNombre(sitPersonalOperativo.getRefNombre());
		usuario.setRefApellidoPaterno(sitPersonalOperativo.getRefApellidoPaterno());
		usuario.setRefApellidoMaterno(sitPersonalOperativo.getRefApellidoMaterno());
		usuario.setIndActivo(sitPersonalOperativo.getIndActivo());
		usuario.setRefNumTelefono(sitPersonalOperativo.getRefNumTelefono());
		return usuario;
	}
	
	public List<UnidadMedica> convertUnidadesMedicas(List<SitUnidadMedica> unidadMedica){
		int i =0;
		List<UnidadMedica> unidades = new ArrayList<>();
		while (unidadMedica.size()>i) {
			unidades.add(convertUnidadMedica(unidadMedica.get(i)));
			i++;
		}
		return unidades;
	}
	
	public UnidadMedica convertUnidadMedica(SitUnidadMedica unidadMedica){
		UnidadMedica unidaOracle= new UnidadMedica();
		unidaOracle.setCvePresupuestal(unidadMedica.getCvePresupuestal());
		unidaOracle.setDesPresupuestal(unidadMedica.getDesUnidadMedica());
		unidaOracle.setFecAlta(unidadMedica.getFecAltaUnidad());
		unidaOracle.setFecBaja(unidadMedica.getFecBajaUnidad());
		unidaOracle.setCveDelegacionImss(unidadMedica.getCveDelegacion());
		unidaOracle.setDesDelegacionImss(unidadMedica.getDesDelegacion());
		return unidaOracle;
	}
	
	public AsignacionUnidad convertAsignacion(SitUnidadMedica sitUnidadMedica, SicRol sicRol){
		AsignacionUnidad asignacionUnidad = new AsignacionUnidad();
		UnidadMedica unidadMedica = convertUnidadMedica(sitUnidadMedica);
		asignacionUnidad.setRol(convertRol(sicRol));
		asignacionUnidad.setUnidadMedica(unidadMedica);
		asignacionUnidad.setIndActivoUnidad(sitUnidadMedica.getIndActivoUnidad() == 1);
		if(asignacionUnidad.getIndActivoUnidad()){
			asignacionUnidad.setDesEstatus(EstatusEnum.ACTIVO.getDescripcion());
		}else{
			asignacionUnidad.setDesEstatus(EstatusEnum.DESACTIVADOR.getDescripcion());
		}
		if(sitUnidadMedica.getIndTitular()==TipoAsignacionMongoEnum.TITULARMONGO.getClaveInt()){
			asignacionUnidad.setIndTitular(Boolean.TRUE);
			asignacionUnidad.setDescTipoAsignacion(TipoAsignacionEnum.TITULAR.getDescripcion());	
		}else if(sitUnidadMedica.getIndTitular()==TipoAsignacionMongoEnum.HUESPEDMONGO.getClaveInt()){
			asignacionUnidad.setIndTitular(Boolean.FALSE);
			asignacionUnidad.setDescTipoAsignacion(TipoAsignacionEnum.HUESPED.getDescripcion());
		}
		asignacionUnidad.setFecAltaUnidad(sitUnidadMedica.getFecAltaUnidad());
		asignacionUnidad.setFecBajaUnidad(sitUnidadMedica.getFecBajaUnidad());
		return asignacionUnidad;
	}
	
	public SitUnidadMedica convertSitUnidadMedicaMongo(AsignacionUnidad asignacionUnidad){
		SitUnidadMedica unidadMongo= new SitUnidadMedica();
		unidadMongo.setCvePresupuestal(asignacionUnidad.getUnidadMedica().getCvePresupuestal());
		unidadMongo.setDesUnidadMedica(asignacionUnidad.getUnidadMedica().getDesPresupuestal());
		unidadMongo.setFecAltaUnidad(asignacionUnidad.getFecAltaUnidad());
		unidadMongo.setFecBajaUnidad(asignacionUnidad.getFecBajaUnidad());
		unidadMongo.setCveDelegacion(asignacionUnidad.getUnidadMedica().getCveDelegacionImss());
		unidadMongo.setDesDelegacion(asignacionUnidad.getUnidadMedica().getDesDelegacionImss());
		if(asignacionUnidad.getIndTitular()){
			unidadMongo.setIndTitular(TipoAsignacionMongoEnum.TITULARMONGO.getClaveInt());
		}else{
			unidadMongo.setIndTitular(TipoAsignacionMongoEnum.HUESPEDMONGO.getClaveInt());
		}
		unidadMongo.setIndActivoUnidad(asignacionUnidad.getIndActivoUnidad()? 1:0);
		return unidadMongo;
	}
	
	public SicRol convertRolMongo(Rol rol){
		SicRol sicRol = new SicRol();
		sicRol.setCveRol(rol.getRefRol());
		sicRol.setDesRol(rol.getDesRol());
		sicRol.setFecBaja(rol.getFecBaja());
		return sicRol;
	}
	
	public Rol convertRol(SicRol sicRol){
		Rol rol = new Rol();
		rol.setCveRol(sicRol.getCveRol());
		rol.setDesRol(sicRol.getDesRol());
		return rol;
	}
	
	public List<AsignacionUnidad> obtenerListAsignaciones(List<SitUnidadMedica> listSitUnidades, SicRol sicRol){
		List<AsignacionUnidad> listAsignaciones = new ArrayList<>();
		if(listSitUnidades !=null && !listSitUnidades.isEmpty()){
			for(SitUnidadMedica sitUnidadMedica :listSitUnidades){
				System.out.println(sitUnidadMedica.getIndActivoUnidad());
				System.out.println("titulahelper"+sitUnidadMedica.getIndTitular());
				listAsignaciones.add(convertAsignacion(sitUnidadMedica, sicRol));
			}
		}
		return listAsignaciones; 
	}
	
	
	public List<Usuario> convertListPersonalOperativo(List<SitPersonalOperativo> listSitPersonalOperativo) {
		List<Usuario> listUsuario = new ArrayList<Usuario>();
		if(listSitPersonalOperativo!=null && !listSitPersonalOperativo.isEmpty()){
			for(SitPersonalOperativo sitpersonal : listSitPersonalOperativo){
				Usuario usuario = convertPersonalOperativo(sitpersonal);
				usuario.setAsignacionUnidad(new ArrayList<>());
				
				for(SitUnidadMedica sitUnidadMedica : sitpersonal.getSdUnidadMedica()){
					usuario.getAsignacionUnidad().add(convertAsignacion(sitUnidadMedica, sitpersonal.getSdRol().get(0)));
				}
				listUsuario.add(usuario);
			}
		}
		return listUsuario;
	}

}
