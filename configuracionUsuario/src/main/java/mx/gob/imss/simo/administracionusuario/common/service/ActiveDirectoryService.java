package mx.gob.imss.simo.administracionusuario.common.service;

import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;

public interface ActiveDirectoryService {
	
	public List<Usuario> searchDirectorioActivo(boolean mock, String dominio,String cveDominio) throws AdministracionUsuarioException;
	
//	public void modificarGrupoSeguridad(String cveDominio,String usuarioDn, List<String> memberList) throws AdministracionUsuarioException;

	public void agregarGrupoSeguridad(String cveDominio) throws AdministracionUsuarioException;
	
	public void removerGrupoSeguridad(String cveDominio) throws AdministracionUsuarioException;
}
