package mx.gob.imss.simo.administracionusuario.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class AsignacionUnidad {
	
	private Rol rol;
    private UnidadMedica unidadMedica;
    private Boolean indActivoUnidad;
    private String desEstatus;
    private Boolean indTitular;
    private String descTipoAsignacion;
    private Date fecBajaUnidad;
    private Date fecAltaUnidad;
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsignacionUnidad other = (AsignacionUnidad) obj;
		if (desEstatus == null) {
			if (other.desEstatus != null)
				return false;
		} else if (!desEstatus.equals(other.desEstatus))
			return false;
		if (descTipoAsignacion == null) {
			if (other.descTipoAsignacion != null)
				return false;
		} else if (!descTipoAsignacion.equals(other.descTipoAsignacion))
			return false;
		if (fecAltaUnidad == null) {
			if (other.fecAltaUnidad != null)
				return false;
		} else if (!fecAltaUnidad.equals(other.fecAltaUnidad))
			return false;
		if (fecBajaUnidad == null) {
			if (other.fecBajaUnidad != null)
				return false;
		} else if (!fecBajaUnidad.equals(other.fecBajaUnidad))
			return false;
		if (indActivoUnidad == null) {
			if (other.indActivoUnidad != null)
				return false;
		} else if (!indActivoUnidad.equals(other.indActivoUnidad))
			return false;
		if (indTitular == null) {
			if (other.indTitular != null)
				return false;
		} else if (!indTitular.equals(other.indTitular))
			return false;
		if (rol == null) {
			if (other.rol != null)
				return false;
		} else if (!rol.equals(other.rol))
			return false;
		if (unidadMedica == null) {
			if (other.unidadMedica != null)
				return false;
		} else if (!unidadMedica.equals(other.unidadMedica))
			return false;
		return true;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desEstatus == null) ? 0 : desEstatus.hashCode());
		result = prime * result + ((descTipoAsignacion == null) ? 0 : descTipoAsignacion.hashCode());
		result = prime * result + ((fecAltaUnidad == null) ? 0 : fecAltaUnidad.hashCode());
		result = prime * result + ((fecBajaUnidad == null) ? 0 : fecBajaUnidad.hashCode());
		result = prime * result + ((indActivoUnidad == null) ? 0 : indActivoUnidad.hashCode());
		result = prime * result + ((indTitular == null) ? 0 : indTitular.hashCode());
		result = prime * result + ((rol == null) ? 0 : rol.hashCode());
		result = prime * result + ((unidadMedica == null) ? 0 : unidadMedica.hashCode());
		return result;
	}
    
    
    
}
