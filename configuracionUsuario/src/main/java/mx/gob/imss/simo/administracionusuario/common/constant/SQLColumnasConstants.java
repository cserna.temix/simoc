package mx.gob.imss.simo.administracionusuario.common.constant;

public class SQLColumnasConstants {

    private SQLColumnasConstants() {
        super();
    }

    /**
     * SIC_ALIMENTACION_RN
     */
    public static final String CVE_ALIMENTACION_RN = "CVE_ALIMENTACION_RN";
    public static final String DES_ALIMENTACION_RN = "DES_ALIMENTACION_RN";

    /**
     * SIC_CAMA_UNIDAD
     */
    public static final String CVE_CAMA = "CVE_CAMA";
    public static final String REF_PISO = "REF_PISO";
    public static final String REF_ALA = "REF_ALA";
    public static final String IND_CENSABLE = "IND_CENSABLE";
    public static final String IND_FICTICIO = "IND_FICTICIO";

    /**
     * SIC_DELEGACION_IMSS
     */
    public static final String CVE_DELEGACION_IMSS = "CVE_DELEGACION_IMSS";
    public static final String DES_DELEGACION_IMSS = "DES_DELEGACION_IMSS";
    public static final String REF_ABREVIATURA_DEL = "REF_ABREVIATURA_DEL";
    public static final String REF_MARCA = "REF_MARCA";
    public static final String FEC_BAJA_DELEGACION_IMSS = "FEC_BAJA";

    public static final String COLOR = "COLOR";

    /**
     * SIC_DIVISION
     */
    public static final String CVE_DIVISION = "CVE_DIVISION";
    public static final String DES_DIVISION = "DES_DIVISION";

    /**
     * SIC_ESPECIALIDAD
     */
    public static final String CVE_ESPECIALIDAD = "CVE_ESPECIALIDAD";
    public static final String DES_ESPECIALIDAD = "DES_ESPECIALIDAD";
    public static final String NUM_UBICACION = "NUM_UBICACION";
    public static final String IND_QX = "IND_QX";
    public static final String NUM_EDAD_MINIMA = "NUM_EDAD_MINIMA";
    public static final String NUM_EDAD_MIN_SEMANA = "NUM_EDAD_MIN_SEMANA";
    public static final String NUM_EDAD_MAXIMA = "NUM_EDAD_MAXIMA";
    public static final String NUM_EDAD_MAX_SEMANA = "NUM_EDAD_MAX_SEMANA";
    public static final String IND_PEDIATRIA = "IND_PEDIATRIA";
    public static final String REF_AREA_RESPONSABLE = "REF_AREA_RESPONSABLE";
    public static final String REF_SIMO_SIAS = "REF_SIMO_SIAS";
    public static final String REF_SIMO_CENTRAL = "REF_SIMO_CENTRAL";
    public static final String CVE_CATEGORIA_ESPECIALIDAD = "CVE_CATEGORIA_ESPECIALIDAD";
    public static final String IND_MOV_INTRAHOSPITALARIO = "IND_MOV_INTRAHOSPITALARIO";
    public static final String NUM_TIPO_SERVICIO = "NUM_TIPO_SERVICIO";

    /**
     * SIC_ESPECIALIDAD_UNIDAD
     */
    public static final String IND_CONSULTA_EXTERNA = "IND_CONSULTA_EXTERNA";
    public static final String IND_HOSPITAL = "IND_HOSPITAL";
    public static final String IND_CIRUGIA = "IND_CIRUGIA";
    public static final String IND_INTERCONSULTA = "IND_INTERCONSULTA";
    public static final String IND_ESTATUS = "IND_ESTATUS";
    public static final String FEC_ALTA = "FEC_ALTA";

    /**
     * SIC_PACIENTE
     */
    public static final String CVE_PACIENTE = "CVE_PACIENTE";
    public static final String CVE_PACIENTE_PADRE = "CVE_PACIENTE_PADRE";
    public static final String CVE_ID_PERSONA = "CVE_ID_PERSONA";
    public static final String REF_NSS = "REF_NSS";
    public static final String REF_AGREGADO_MEDICO = "REF_AGREGADO_MEDICO";
    public static final String REF_NOMBRE = "REF_NOMBRE";
    public static final String REF_APELLIDO_PATERNO = "REF_APELLIDO_PATERNO";
    public static final String REF_APELLIDO_MATERNO = "REF_APELLIDO_MATERNO";
    public static final String FEC_NACIMIENTO = "FEC_NACIMIENTO";
    public static final String NUM_EDAD_SEMANAS = "NUM_EDAD_SEMANAS";
    public static final String CVE_CURP = "CVE_CURP";
    public static final String CVE_IDEE = "CVE_IDEE";
    public static final String IND_ACCEDER = "IND_ACCEDER";
    public static final String IND_VIGENCIA = "IND_VIGENCIA";
    public static final String CVE_SEXO = "CVE_SEXO";
    public static final String REF_TIPO_PENSION = "REF_TIPO_PENSION";
    public static final String REF_REGISTRO_PATRONAL = "REF_REGISTRO_PATRONAL";
    public static final String FEC_ACTUALIZACION = "FEC_ACTUALIZACION";
    public static final String FEC_CONSULTA = "FEC_CONSULTA";
    public static final String CVE_UNIDAD_ADSCRIPCION = "CVE_UNIDAD_ADSCRIPCION";
    public static final String IND_RECIEN_NACIDO = "IND_RECIEN_NACIDO";
    public static final String IND_DERECHOHABIENTE = "IND_DERECHOHABIENTE";
    public static final String IND_ULTIMO_REGISTRO = "IND_ULTIMO_REGISTRO";
    public static final String NUM_EDAD_ANIOS = "NUM_EDAD_ANIOS";

    /**
     * SIC_PERIODO_IMSS
     */
    public static final String CVE_PERIODO_IMSS = "CVE_PERIODO_IMSS";
    public static final String NUM_ANIO_PERIODO = "NUM_ANIO_PERIODO";
    public static final String DES_MES_PERIODO = "DES_MES_PERIODO";
    public static final String FEC_INICIAL = "FEC_INICIAL";
    public static final String FEC_FINAL = "FEC_FINAL";
    public static final String IND_VIGENTE = "IND_VIGENTE";
    public static final String FEC_BAJA = "FEC_BAJA";
    public static final String IND_ACTUAL = "IND_ACTUAL";
    public static final String IND_MONITOREO = "IND_MONITOREO";
    public static final String IND_CAMA_PEDIATRIA = "IND_CAMA_PEDIATRIA";

    /**
     * SIC_TIPO_FORMATO
     */
    public static final String CVE_TIPO_FORMATO = "CVE_TIPO_FORMATO";
    public static final String DES_TIPO_FORMATO = "DES_TIPO_FORMATO";
    public static final String REF_FORMATO_SIMOC = "REF_FORMATO_SIMOC";

    /**
     * SIC_TIPO_INGRESO
     */
    public static final String CVE_TIPO_INGRESO = "CVE_TIPO_INGRESO";
    public static final String DES_TIPO_INGRESO = "DES_TIPO_INGRESO";

    /**
     * SIC_TIPO_PROGRAMA
     */
    public static final String CVE_TIPO_PROGRAMA = "CVE_TIPO_PROGRAMA";
    public static final String DES_TIPO_PROGRAMA = "DES_TIPO_PROGRAMA";

    /**
     * SIC_UNIDAD_MEDICA
     */
    public static final String CVE_PRESUPUESTAL = "CVE_PRESUPUESTAL";
    public static final String DES_UNIDAD_MEDICA = "DES_UNIDAD_MEDICA";
    public static final String IND_ASCRIPCION = "IND_ASCRIPCION";
    public static final String IND_SIMO = "IND_SIMO";
    public static final String CVE_CLUES_SALUD = "CVE_CLUES_SALUD";
    public static final String CVE_DELEGACION_SIAP = "CVE_DELEGACION_SIAP";
    public static final String CVE_ENTIDAD_FED = "CVE_ENTIDAD_FED";
    public static final String REF_NIVEL_UNIDAD = "REF_NIVEL_UNIDAD";
    public static final String CVE_TIPO_UNIDAD_SERVICIO = "CVE_TIPO_UNIDAD_SERVICIO";
    public static final String DES_NUM_UNIDAD = "DES_NUM_UNIDAD";
    public static final String REF_JURISDICCION_SSA = "REF_JURISDICCION_SSA";
    public static final String CVE_PREI = "CVE_PREI";
    public static final String IND_TITULAR = "IND_TITULAR";

    /**
     * SIC_TIPO_ATENCION
     */

    public static final String DES_TIPO_ATENCION = "DES_TIPO_ATENCION";

    /**
     * SIC_TIPO_PARTO
     */

    public static final String DES_TIPO_PARTO = "DES_TIPO_PARTO";

    /**
     * SIC_SEXO
     */

    public static final String DES_SEXO = "DES_SEXO";

    /**
     * SIC_TIPO_NACIDO
     */

    public static final String CVE_TIPO_NACIDO = "CVE_TIPO_NACIDO";
    public static final String DES_TIPO_NACIDO = "DES_TIPO_NACIDO";

    /**
     * SIC_METODO_ANTICONCEPTIVO
     */

    public static final String DES_METODO_ANTICONCEPTIVO = "DES_METODO_ANTICONCEPTIVO";
    public static final String NUM_EDAD_INICIAL = "NUM_EDAD_INICIAL";
    public static final String NUM_EDAD_FINAL = "NUM_EDAD_FINAL";
    public static final String REF_DMEM_CONR = "REF_DMEM_CONR";
    public static final String REF_DMEM_SINR = "REF_DMEM_SINR";
    public static final String CAN_MAXIMA_METODO = "CAN_MAXIMA_METODO";

    /**
     * SIC_SALA_UNIDAD
     */

    public static final String DES_SALA = "DES_SALA";
    public static final String IND_TURNO_MATUTINO = "IND_TURNO_MATUTINO";
    public static final String IND_TURNO_VESPERTINO = "IND_TURNO_VESPERTINO";
    public static final String IND_TURNO_ACUMULADA = "IND_TURNO_ACUMULADA";
    public static final String IND_TURNO_NOCTURNO = "IND_TURNO_NOCTURNO";
    public static final String IND_ESTADO = "IND_ESTADO";

    /**
     * SIT_BITACORA_CAMA
     */
    public static final String CVE_BITACORA_CAMA = "CVE_BITACORA_CAMA";
    public static final String FEC_EVENTO = "FEC_EVENTO";
    public static final String IND_OCUPADA = "IND_OCUPADA";

    /**
     * SIT_INGRESO
     */
    public static final String CVE_INGRESO = "CVE_INGRESO";
    public static final String CVE_INGRESO_PADRE = "CVE_INGRESO_PADRE";
    public static final String FEC_CREACION = "FEC_CREACION";
    public static final String CVE_ESPECIALIDAD_INGRESO = "CVE_ESPECIALIDAD_INGRESO";
    public static final String CVE_CAPTURISTA = "CVE_CAPTURISTA";
    public static final String FEC_INGRESO = "FEC_INGRESO";
    public static final String CVE_ESPECIALIDAD_CAMA = "CVE_ESPECIALIDAD_CAMA";
    public static final String REF_CAMA_RN = "REF_CAMA_RN";
    public static final String CVE_MEDICO = "CVE_MEDICO";
    public static final String IND_EXTEMPORANEO = "IND_EXTEMPORANEO";
    public static final String IND_EGRESO = "IND_EGRESO";
    public static final String IND_TOCOCIRUGIA = "IND_TOCOCIRUGIA";
    public static final String IND_INTERVENCION_QX = "IND_INTERVENCION_QX";
    public static final String CVE_ESPECIALIDAD_ORIGEN = "CVE_ESPECIALIDAD_ORIGEN";

    /**
     * SIT_OCUPACION_CAMA
     */
    public static final String NUM_PACIENTES = "NUM_PACIENTES";

    /**
     * SIT_TOCOCIRUGIA
     */

    public static final String CVE_TOCOCIRUGIA = "CVE_TOCOCIRUGIA";
    public static final String FEC_TOCOCIRUGIA = "FEC_TOCOCIRUGIA";
    public static final String CVE_SALA = "CVE_SALA";
    public static final String CVE_TIPO_ATENCION = "CVE_TIPO_ATENCION";
    public static final String CVE_TIPO_PARTO = "CVE_TIPO_PARTO";
    public static final String CVE_METODO_ANTICONCEPTIVO = "CVE_METODO_ANTICONCEPTIVO";
    public static final String CAN_METODO_ANTICONCEPTIVO = "CAN_METODO_ANTICONCEPTIVO";
    public static final String NUM_TOTAL_RN = "NUM_TOTAL_RN";
    public static final String CVE_CAPTURISTA_ACTUALIZA = "CVE_CAPTURISTA_ACTUALIZA";

    /**
     * SIT_RECIEN_NACIDO
     */
    public static final String NUM_RECIEN_NACIDO = "NUM_RECIEN_NACIDO";
    public static final String NUM_PESO = "NUM_PESO";
    public static final String NUM_TALLA = "NUM_TALLA";
    public static final String NUM_PERIMETRO_CEFALICO = "NUM_PERIMETRO_CEFALICO";
    public static final String NUM_SEM_GESTACION = "NUM_SEM_GESTACION";
    public static final String NUM_APGAR_1MINUTO = "NUM_APGAR_1MINUTO";
    public static final String NUM_APGAR_5MINUTO = "NUM_APGAR_5MINUTO";
    public static final String FEC_DEFUNCION = "FEC_DEFUNCION";
    public static final String IND_INGRESO_RN = "IND_INGRESO_RN";
    public static final String CVE_RANGO_LUBCHENCO = "CVE_RANGO_LUBCHENCO";

    /**
     * SIC_RANGO_LUBCHENCO
     */

    public static final String NUM_SEMANA_GESTACION = "NUM_SEMANA_GESTACION";
    public static final String CAN_PESO_SUPERIOR = "CAN_PESO_SUPERIOR";
    public static final String CAN_PESO_INFERIOR = "CAN_PESO_INFERIOR";
    public static final String CVE_LUBCHENCO = "CVE_LUBCHENCO";
    public static final String CVE_CLASIFICACION_PESO = "CVE_CLASIFICACION_PESO";

    /**
     * SIC_PERIODO_ATENCION_TOCO
     */

    public static final String CVE_ATENCION_PREVIA = "CVE_ATENCION_PREVIA";
    public static final String CVE_ATENCION_ACTUAL = "CVE_ATENCION_ACTUAL";
    public static final String NUM_PERIODO_DIAS = "NUM_PERIODO_DIAS";

    /**
     * SIC_TIPO_INTERVENCIONES
     */
    public static final String CVE_TIPO_INTERVENCION = "CVE_TIPO_INTERVENCION";
    public static final String DES_TIPO_INTERVENCION = "DES_TIPO_INTERVENCION";

    /**
     * SIC_TIPO_ANESTESIA
     */
    public static final String CVE_TIPO_ANESTESIA = "CVE_TIPO_ANESTESIA";
    public static final String DES_TIPO_ANESTESIA = "DES_TIPO_ANESTESIA";

    /**
     * SIC_TIPO_EGRESO
     */

    public static final String CVE_TIPO_EGRESO = "CVE_TIPO_EGRESO";
    public static final String DES_TIPO_EGRESO = "DES_TIPO_EGRESO";

    /**
     * SIC_MOTIVO_ALTA
     */

    public static final String CVE_MOTIVO_ALTA = "CVE_MOTIVO_ALTA";
    public static final String DES_MOTIVO_ALTA = "DES_MOTIVO_ALTA";

    /**
     * SIC_TIPO_UBICACION
     */
    public static final String CVE_TIPO_UBICACION = "CVE_TIPO_UBICACION";
    public static final String DES_TIPO_UBICACION = "DES_TIPO_UBICACION";

    /**
     * SIC_MEDICO
     */
    public static final String CVE_MATRICULA = "CVE_MATRICULA";
    public static final String REF_TIPO_CONTRATACION = "REF_TIPO_CONTRATACION";
    public static final String FEC_CONSULTA_SIAP = "FEC_CONSULTA_SIAP";
    public static final String IND_ACTIVO = "IND_ACTIVO";
    public static final String IND_SIAP = "IND_SIAP";

    /**
     * SIC_CIE10 DIAGNOSTICO
     */
    public static final String CVE_CIE10 = "CVE_CIE10";
    public static final String DES_CIE10 = "DES_CIE10";
    public static final String IND_TRIV = "IND_TRIV";
    public static final String IND_ERRADICADO = "IND_ERRADICADO";
    public static final String IND_NOTIF_INTERNACIONAL = "IND_NOTIF_INTERNACIONAL";
    public static final String IND_NOTIF_OBLIGATORIA = "IND_NOTIF_OBLIGATORIA";
    public static final String IND_NOTIF_INMEDIATA = "IND_NOTIF_INMEDIATA";
    public static final String IND_NOTIF_OBSTETRICA = "IND_NOTIF_OBSTETRICA";
    public static final String IND_NOVALIDA_BASICA_DEF = "IND_NOVALIDA_BASICA_DEF";
    public static final String IND_NOVALIDA_AFECCION_HOSP = "IND_NOVALIDA_AFECCION_HOSP";
    public static final String IND_VALIDA_MUERTE_FETAL = "IND_VALIDA_MUERTE_FETAL";
    public static final String IND_ENF_CRONICA = "IND_ENF_CRONICA";
    public static final String IND_TRANSMISIBLE = "IND_TRANSMISIBLE";
    public static final String IND_PRINCIPAL_CONSULTA_EXTERNA = "IND_PRINCIPAL_CONSULTA_EXTERNA";
    public static final String IND_EXCLUSION = "IND_EXCLUSION";

    /**
     * SIC_CIE9
     */
    public static final String CVE_CIE9 = "CVE_CIE9";
    public static final String DES_CIE9 = "DES_CIE9";
    public static final String NUM_FILTRO = "NUM_FILTRO";
    public static final String CVE_SEXO_CIE9 = "CVE_SEXO";
    public static final String NUM_EDAD_INFERIOR_ANIOS = "NUM_EDAD_INFERIOR_ANIOS";
    public static final String NUM_EDAD_INFERIOR_SEMANAS = "NUM_EDAD_INFERIOR_SEMANAS";
    public static final String NUM_EDAD_SUPERIOR_ANIOS = "NUM_EDAD_SUPERIOR_ANIOS";
    public static final String NUM_EDAD_SUPERIOR_SEMANAS = "NUM_EDAD_SUPERIOR_SEMANAS";
    public static final String NUM_ANIO_VIGENCIA = "NUM_ANIO_VIGENCIA";
    public static final String REF_USO = "REF_USO";
    public static final String REF_ACTUALIZACION = "REF_ACTUALIZACION";
    public static final String NUM_PRECEDENCIA = "NUM_PRECEDENCIA";
    public static final String IND_NIVEL1 = "IND_NIVEL1";
    public static final String IND_NIVEL2 = "IND_NIVEL2";
    public static final String IND_NIVEL3 = "IND_NIVEL3";
    public static final String NUM_CAPITULO_CIE9 = "NUM_CAPITULO_CIE9";
    public static final String NUM_GRUPO_CIE9 = "NUM_GRUPO_CIE9";
    public static final String NUM_CODIGO_CONSECUTIVO = "NUM_CODIGO_CONSECUTIVO";
    public static final String DES_CIE9M = "DES_CIE9M";
    public static final String IND_BILATERALIDAD = "IND_BILATERALIDAD";
    public static final String FEC_BAJA_CIE9 = "FEC_BAJA";

    /**
     * Procedimientos Realizados(reporte)
     */
    public static final String CLAVE_INGRESO = "CLAVE_INGRESO";
    public static final String CLAVE_CIE9 = "CLAVE_CIE9";
    public static final String DESCRIPCION_CIE9 = "DESCRIPCION_CIE9";
    public static final String FECHA_INTERVENCION = "FECHA_INTERVENCION";
    public static final String FECHA_INGRESO = "FECHA_INGRESO";
    public static final String FECHA_EGRESO = "FECHA_EGRESO";
    public static final String FEC_GENERACION = "FEC_GENERACION";
    public static final String AGREGADO_MEDICO = "AGREGADO_MEDICO";
    public static final String NSS = "NSS";
    public static final String NOMBRE_COMPLETO = "NOMBRE_COMPLETO";

    /**
     * Pacientes Hospitalizados
     */
    // public static final String DES_ESPECIALIDAD="DES_ESPECIALIDAD";
    public static final String AFILIACION = "AFILIACION";
    // public static final String AGREGADO_MEDICO="AGREGADO_MEDICO";
    public static final String NOMBRE = "NOMBRE";
    // public static final String FEC_INGRESO="FEC_INGRESO";
    public static final String CAMA = "CAMA";
    public static final String PROGRAMA = "PROGRAMA";
    public static final String FEC_INTERVENCION_IQX = "FEC_INTERVENCION_IQX";
    public static final String PROCEDIMIENTO = "PROCEDIMIENTO";
    public static final String PARTO = "PARTO";
    public static final String RECIEN_NACIDO = "RECIEN_NACIDO";
    public static final String HORA = "HORA";
    public static final String SEXO = "SEXO";
    public static final String UBICACION = "UBICACION";
    public static final String ESTADO = "ESTADO";
    public static final String ALTA = "ALTA";
    public static final String OBSERVACIONES_RN = "OBSERVACIONES_RN";
    public static final String OBSERVACIONES_INGRESO = "OBSERVACIONES_INGRESO";
    public static final String CVE_TIPO_REPORTE = "CVE_TIPO_REPORTE";

    /**
     * SIC_PROCEDIMIENTO_RN
     */
    public static final String CVE_PROCEDIMIENTO_RN = "CVE_PROCEDIMIENTO_RN";
    public static final String DES_PROCEDIMIENTO_RN = "DES_PROCEDIMIENTO_RN";

    /**
     * SIC_TAMIZ
     */
    public static final String CVE_TAMIZ = "CVE_TAMIZ";
    public static final String DES_TAMIZ = "DES_TAMIZ";

    /**
     * SIT_INTERVENCION_QUIRURGICA
     */

    public static final String NUM_PROCEDIMIENTOS = "NUM_PROCEDIMIENTOS";
    public static final String FEC_ENTRADA_SALA = "FEC_ENTRADA_SALA";
    public static final String FEC_INICIO = "FEC_INICIO_IQX";
    public static final String FEC_TERMINO = "FEC_TERMINO_IQX";
    public static final String FEC_SALIDA_SALA = "FEC_SALIDA_SALA";
    public static final String FEC_PROGRAMACION = "FEC_PROGRAMACION";
    public static final String CVE_ANESTESIOLOGO = "CVE_ANESTESIOLOGO";
    public static final String NUM_PROCEDIMIENTO = "NUM_PROCEDIMIENTO";
    public static final String CVE_ESPECIALIDAD_IQX = "CVE_ESPECIALIDAD_IQX";

    /**
     * SIC_PARAMETRO
     */
    public static final String CVE_PARAMETRO = "CVE_PARAMETRO";
    public static final String REF_PARAMETRO = "REF_PARAMETRO";
    public static final String DES_PARAMETRO = "DES_PARAMETRO";

    /**
     * SIT_EGRESO
     */
    public static final String CVE_EGRESO = "CVE_EGRESO";
    public static final String FEC_EGRESO = "FEC_EGRESO";
    public static final String CLAVE_TIPO_EGRESO = "CVE_TIPO_EGRESO";
    public static final String REF_JUSTIFICA_CANCELACION = "REF_JUSTIFICA_CANCELACION";
    public static final String CVE_TAMIZ_EGRESO = "CVE_TAMIZ";
    public static final String NUM_PESO_RN = "NUM_PESO_RN";

    /**
     * SIC_PERSONAL_OPERATIVO
     */
    public static final String REF_PERSONAL_OPERATIVO = "REF_PERSONAL_OPERATIVO";
    public static final String CVE_MATRICULA_PERSONAL_OPERATIVO = "CVE_MATRICULA";
    public static final String REF_NOMBRE_PERSONAL_OPERATIVO = "REF_NOMBRE";
    public static final String REF_APELLIDO_PATERNO_PERSONAL_OPERATIVO = "REF_APELLIDO_PATERNO";
    public static final String REF_APELLIDO_MATERNO_PERSONAL_OPERATIVO = "REF_APELLIDO_MATERNO";
    public static final String REF_EMAIL = "REF_EMAIL";
    public static final String REF_NUM_TELEFONO = "REF_NUM_TELEFONO";
    public static final String REF_NUM_EXTENSION = "REF_NUM_EXTENSION";
    public static final String IND_ACTIVO_PERSONAL_OPERATIVO = "IND_ACTIVO";
    public static final String IND_BLOQUEADO = "IND_BLOQUEADO";
    public static final String CAN_INTENTOS = "CAN_INTENTOS";
    public static final String FEC_BAJA_PERSONAL_UNIDAD = "FEC_BAJA_UNIDAD";
    public static final String FEC_ALTA_PERSONAL_UNIDAD = "FEC_ALTA_UNIDAD";
    public static final String ESTATUS_PERSONAL_UNIDAD = "IND_ACTIVO_UNIDAD";

    /**
     * MENU
     */
    public static final String CLAVE_MENU = "CLAVE_MENU";
    public static final String MENU_PADRE = "MENU_PADRE";
    public static final String DESCRIPCION_MENU = "DESCRIPCION_MENU";
    public static final String REF_ACCION = "REF_ACCION";
    public static final String POSICION = "POSICION";
    public static final String CUENTA_AD = "CUENTA_AD";
    public static final String CVE_TIPO_DIAGNOSTICO = "CVE_TIPO_DIAGNOSTICO";

    /**
     * SIC_ROL
     */
    public static final String CVE_ROL = "CVE_ROL";
    public static final String REF_ROL = "REF_ROL";
    public static final String DES_ROL = "DES_ROL";

    public static final String CVE_TIPO_CAPTURA = "CVE_TIPO_CAPTURA";
    public static final String IND_CERRADO = "IND_CERRADO";
    public static final String FEC_CIERRE = "FEC_CIERRE";
    public static final String FEC_MOVIMIENTO = "FEC_MOVIMIENTO";
    public static final String CVE_CAMA_ORIGEN = "CVE_CAMA_ORIGEN";
    public static final String CVE_ESP_CAMA_ORIGEN = "CVE_ESP_CAMA_ORIGEN";
    public static final String CVE_ESP_ORIGEN = "CVE_ESPECIALIDAD_ORIGEN";
    public static final String CVE_CAMA_DESTINO = "CVE_CAMA_DESTINO";
    public static final String CVE_ESP_DESTINO = "CVE_ESPECIALIDAD_DESTINO";

    /**
     * Monitor Cierre
     */
    public static final String CVE_DELEGACION = "CVE_DELEGACION";
    public static final String UNIDAD_MEDICA = "UNIDAD_MEDICA";
    public static final String CONSULTA_EXTERNA = "CONSULTA_EXTERNA";
    public static final String HOSPITALIZACION = "HOSPITALIZACION";
    public static final String INFORMA_COMPLEMEN = "INFORMA_COMPLEMEN";
    public static final String SERVICIO_SUB = "SERVICIO_SUB";

    /**
     * Monitor Dias Sin Capturar
     */
    public static final String FEC_MONITOR = "FEC_MONITOR";
    public static final String ESPECIALIDADES = "ESPECIALIDADES";
    public static final String URGENCIAS_CONSULTA_EXTERNA = "URGENCIAS_CE";
    public static final String HOSPITALARIOS = "HOSPITALARIOS";
    public static final String QUIRURGICAS = "QUIRURGICAS";
    public static final String URGENCIAS_EGRESOS = "URGENCIAS_E";

    /**
     * SIC_TIPO_CONTRATO
     */
    public static final String CVE_TIPO_CONTRATO = "CVE_TIPO_CONTRATO";
    public static final String DES_TIPO_CONTRATO = "DES_TIPO_CONTRATO";

    /**
     * SIC_TIPO_SERVICIO
     */
    public static final String CVE_TIPO_SERVICIO = "CVE_TIPO_SERVICIO";
    public static final String DES_TIPO_SERVICIO = "DES_TIPO_SERVICIO";

    /**
     * SIC_MOTIVO_SUBROGACION
     */
    public static final String CVE_MOTIVO_SUBROGACION = "CVE_MOTIVO_SUBROGACION";
    public static final String REF_INICIAL_MOTIVO = "REF_INICIAL_MOTIVO";

    /**
     * SIC_SERVICIO_SUBROGAR
     */
    public static final String CVE_SERVICIO_SUBROGAR = "CVE_SERVICIO_SUBROGAR";
    public static final String DES_SERVICIO_SUBROGAR = "DES_SERVICIO_SUBROGAR";
    public static final String CVE_GRUPO_SUBROGAR = "CVE_GRUPO_SUBROGAR";

    /**
     * SIT_SERVICIO_SUBROGADO
     */
    public static final String CVE_SERVICIO_SUBROGADO = "CVE_SERVCIO_SUBROGADO";
    public static final String REF_FOLIO_CONTRATO = "REF_FOLIO_CONTRATO";
    public static final String NUM_COSTO_UNITARIO = "NUM_COSTO_UNITARIO";
    public static final String NUM_CANTIDAD_EVENTO = "NUM_CANTIDAD_EVENTO";
    public static final String FEC_SERVICIO = "FEC_SERVICIO";

    /**
     * SIC_GRUPO_SUBROGAR
     */
    public static final String DES_GRUPO_SUBROGAR = "DES_GRUPO_SUBROGAR";

    /**
     * Partos Reporte Parte II
     */

    public static final String ABDOMINALES = "ABDOMINALES";
    public static final String NORMAL = "NORMAL";
    public static final String VAGINALES = "VAGINALES";

    public static final String PRE_TERMINO_PESO_BAJO = "PRE_TERMINO_PESO_BAJO";
    public static final String PRE_TERMINO_PESO_NORMAL = "PRE_TERMINO_PESO_NORMAL";
    public static final String PRE_TERMINO_PESO_ALTO = "PRE_TERMINO_PESO_ALTO";

    public static final String TERMINO_PESO_BAJO = "TERMINO_PESO_BAJO";
    public static final String TERMINO_PESO_NORMAL = "TERMINO_PESO_NORMAL";
    public static final String TERMINO_PESO_ALTO = "TERMINO_PESO_ALTO";

    public static final String POST_TERMINO_PESO_BAJO = "POST_TERMINO_PESO_BAJO";
    public static final String POST_TERMINO_PESO_NORMAL = "POST_TERMINO_PESO_NORMAL";
    public static final String POST_TERMINO_PESO_ALTO = "POST_TERMINO_PESO_ALTO";

    public static final String DEFUNCION = "DEFUNCION";
    public static final String MORTINATO = "MORTINATO";
    public static final String ABORTO = "ABORTO";
    public static final String VIVO = "VIVO";

    public static final String FECHA_INICIO = "FECHA_INICIO";
    public static final String FECHA_FIN = "FECHA_FIN";
    public static final String IND_CONF_ACTIVA = "IND_CONF_ACTIVA";
    
}
