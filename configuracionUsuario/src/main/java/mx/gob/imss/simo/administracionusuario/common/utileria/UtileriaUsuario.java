package mx.gob.imss.simo.administracionusuario.common.utileria;

public class UtileriaUsuario {

	public static int convertSringToint(String cadena){
		int result =0;
		if(cadena !=null && !cadena.isEmpty()){
			result = Integer.parseInt(cadena);			
		}
		return result;
	}
}
