package mx.gob.imss.simo.administracionusuario.administracion.repository;


import mx.gob.imss.simo.administracionusuario.common.model.Parametro;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;

public interface AdministracionRepository {
		
	 Parametro obtenerParametro(String cveParametro);
	 
	 UnidadMedica obtenerDelegacionSIAP(String cveDelegacionImss, String cvePresupuestal);

}
