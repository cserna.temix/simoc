package mx.gob.imss.simo.administracionusuario.administracion.repository.impl;


import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.administracionusuario.administracion.repository.AdministracionRepository;
import mx.gob.imss.simo.administracionusuario.common.constant.SQLConstants;
import mx.gob.imss.simo.administracionusuario.common.helper.ParametroRowMapperHelper;
import mx.gob.imss.simo.administracionusuario.common.helper.UnidadMedicaRowMapperHelper;
import mx.gob.imss.simo.administracionusuario.common.model.Parametro;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.repository.BaseJDBCRepository;

@Repository("administracionRepository")
public class AdministracionRepositoryJDBCImpl extends BaseJDBCRepository implements AdministracionRepository {

	@Override
	public Parametro obtenerParametro(String cveParametro) {
		Parametro parametro = jdbcTemplate.queryForObject(SQLConstants.BUSCAR_PARAMETRO, new Object[] { cveParametro },
                new ParametroRowMapperHelper());
        return parametro;
	}

	@Override
	public UnidadMedica obtenerDelegacionSIAP(String cveDelegacionImss, String cvePresupuestal) {
		UnidadMedica unidadMedico = jdbcTemplate.queryForObject(SQLConstants.BUSQUEDA_DELEGACIO_SIAP, new Object[] {cveDelegacionImss, cvePresupuestal},
                new UnidadMedicaRowMapperHelper());
        return unidadMedico;
	}

}
