package mx.gob.imss.simo.administracionusuario.common.enumeracion;

import java.util.ArrayList;
import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;

public enum TipoAsignacionEnum {
	
	TITULAR("1",1, "Titular"), HUESPED("0",0,"Hu�sped/Apoyo");

	private String clave;
	private Integer claveInt;
    private String descripcion;
    
    private TipoAsignacionEnum(String clave,Integer claveInt,String descripcion) {
        this.clave = clave;
        this.claveInt = claveInt;
        this.descripcion =descripcion;
    }
    
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public Integer getClaveInt() {
		return claveInt;
	}

	public void setClaveInt(Integer claveInt) {
		this.claveInt = claveInt;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
    
	public static TipoAsignacionEnum obtenerPorClave(String clave){
		if(clave !=null){
		for (TipoAsignacionEnum tipoAsigEnum : TipoAsignacionEnum.values()) {
			if(tipoAsigEnum.getClave().equals(clave)){
				return tipoAsigEnum;
			}
		}
		}
		return null;
	}
	
	public static TipoAsignacionEnum obtenerPorDescripcion(String descripcion){
		if(descripcion !=null){
		for (TipoAsignacionEnum tipoAsigEnum : TipoAsignacionEnum.values()) {
			if(tipoAsigEnum.getDescripcion().equals(descripcion)){
				return tipoAsigEnum;
			}
		}
		}
		return null;
	}
	
	
	public static List<CatalogoObject> obtenerCatalogo(){
		List<CatalogoObject> catalogos = new ArrayList<CatalogoObject>();
		for(TipoAsignacionEnum tipoValor : TipoAsignacionEnum.values()){
			catalogos.add(new CatalogoObject(tipoValor.getClave(),tipoValor.getDescripcion()));
		}
		return catalogos;
	}
}
