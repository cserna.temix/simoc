package mx.gob.imss.simo.administracionusuario.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.administracionusuario.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;

public class RolRowMapperHelper  extends BaseHelper implements RowMapper<Rol> {

	@Override
	public Rol mapRow(ResultSet rs, int rowNum) throws SQLException {
		Rol rol = new Rol();
		rol.setCveRol(rs.getString(SQLColumnasConstants.CVE_ROL));
		rol.setRefRol(rs.getString(SQLColumnasConstants.REF_ROL));
		rol.setDesRol(rs.getString(SQLColumnasConstants.DES_ROL));
		rol.setFecBaja(rs.getDate(SQLColumnasConstants.FEC_BAJA));
		return rol;
	}

}
