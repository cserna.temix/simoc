package mx.gob.imss.simo.administracionusuario.common.constant;

public enum SecuenciaEnum {
	
	 BITACORA_CAMA("SIS_CVE_BITACORA_CAMA");

	    private String nombreSecuencia;

	    private SecuenciaEnum(final String nombreSecuencia) {
	        this.nombreSecuencia = nombreSecuencia;
	    }

	    public String getNombreSecuencia() {

	        return nombreSecuencia;
	    }

}
