package mx.gob.imss.simo.administracionusuario.common.mongodb.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;



public enum MongoManager {

	INSTANCE;
	Logger logger = Logger.getLogger(MongoManager.class);
    private MongoDatabase db;
    private Datastore datastore;

    private String user;
    private String password;
    private String base;
    private String host;
    private String host2;
    private String host3;
    private Integer port;
    private Integer port2;
    private Integer port3;

    private MongoManager() {
        readMongoServerConfig();
        //logger.info("In MongoManager Constructor");

        MongoClient mongoClient = new MongoClient(
                Arrays.asList(new ServerAddress(getHost(), getPort()), new ServerAddress(getHost2(), getPort2()),
                        new ServerAddress(getHost3(), getPort3())),
                Arrays.asList(MongoCredential.createCredential(getUser(), getBase(), getPassword().toCharArray())));

        this.db = mongoClient.getDatabase(getBase());

        Morphia morphia = new Morphia();

        this.datastore = morphia.createDatastore(mongoClient, getBase());
    }

    private void readMongoServerConfig() {

        Properties prop = new Properties();
        InputStream input = null;
        try {
            String filename = "database.properties";
            input = MongoManager.class.getClassLoader().getResourceAsStream(filename);

            if (input == null) {
            	logger.info("Archivo no encontrado " + filename);
                return;
            }

            prop.load(input);

            setUser(prop.getProperty("database.user"));
            setPassword(prop.getProperty("database.password"));
            setBase(prop.getProperty("database.base"));
            setHost(prop.getProperty("database.host"));
            setHost2(prop.getProperty("database.host2"));
            setHost3(prop.getProperty("database.host3"));
            setPort(Integer.valueOf(prop.getProperty("database.port")));
            setPort2(Integer.valueOf(prop.getProperty("database.port2")));
            setPort3(Integer.valueOf(prop.getProperty("database.port3")));

        } catch (IOException ex) {
            logger.error("Error al leer archivo database.properties", ex);
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                	 logger.error("Error al cerrar el Stream", e);
                	 e.printStackTrace();
                }
            }
        }
    }

    public MongoDatabase getDb() {

        return db;
    }

    public Datastore getDatastore() {

        return datastore;
    }

    private String getUser() {

        return user;
    }

    private void setUser(String user) {

        this.user = user;
    }

    private String getPassword() {

        return password;
    }

    private void setPassword(String password) {

        this.password = password;
    }

    private String getBase() {

        return base;
    }

    private void setBase(String base) {

        this.base = base;
    }

    private String getHost() {

        return host;
    }

    private void setHost(String host) {

        this.host = host;
    }

    private String getHost2() {

        return host2;
    }

    private void setHost2(String host2) {

        this.host2 = host2;
    }

    private String getHost3() {

        return host3;
    }

    private void setHost3(String host3) {

        this.host3 = host3;
    }

    private Integer getPort() {

        return port;
    }

    private void setPort(Integer port) {

        this.port = port;
    }

    private Integer getPort2() {

        return port2;
    }

    private void setPort2(Integer port2) {

        this.port2 = port2;
    }

    private Integer getPort3() {

        return port3;
    }

    private void setPort3(Integer port3) {

        this.port3 = port3;
    }
}
