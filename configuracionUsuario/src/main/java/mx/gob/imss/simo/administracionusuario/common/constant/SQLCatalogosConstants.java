package mx.gob.imss.simo.administracionusuario.common.constant;

public class SQLCatalogosConstants {
	
	 private SQLCatalogosConstants() {
	    }

	    /**
	     * Informacion Complementaria
	     */

	    public static final String OBTENER_CATALOGO_TIPOS_ASIGNACION= "SELECT * FROM SIC_ROL WHERE FEC_BAJA IS NULL";

	    public static final String OBTENER_CATALOGOS_ROLES= "SELECT * FROM SIC_ROL WHERE FEC_BAJA IS NULL";
	    
	    public static final String OBTENER_CATALOGOS_DELEGACIONES= "SELECT * FROM SIC_DELEGACION_IMSS WHERE FEC_BAJA IS NULL";
	    
	    public static final String OBTENER_CATALOGOS_UNIDADES= "SELECT * FROM SIC_UNIDAD_MEDICA WHERE CVE_DELEGACION_IMSS = ? AND FEC_BAJA IS NULL";
	    
	    public static final String OBTENER_CATALOGOS_ESTATUS= "SELECT * FROM SIC_ROL WHERE FEC_BAJA IS NULL";


}
