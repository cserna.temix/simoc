package mx.gob.imss.simo.administracionusuario.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class UnidadMedica {

    private String cveCluesSalud;
    private String cveDelegacionImss;
    private String desDelegacionImss;
    private String cveDelegacionSiap;
    private String cveEntidadFed;
    private String desEntidadFed;
    private String cvePrei;
    private String cvePresupuestal;
    private String desPresupuestal;
    private String refPersonalOperativo;
    private String cveTipoUnidadServicio;
    private Date fecBaja;
    private Date fecAlta;
    private boolean indAscripcion;
    private boolean indSimo;
    private String refJurisdiccionSsa;
    private String refNivelUnidad;
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadMedica other = (UnidadMedica) obj;
		if (cveCluesSalud == null) {
			if (other.cveCluesSalud != null)
				return false;
		} else if (!cveCluesSalud.equals(other.cveCluesSalud))
			return false;
		if (cveDelegacionImss == null) {
			if (other.cveDelegacionImss != null)
				return false;
		} else if (!cveDelegacionImss.equals(other.cveDelegacionImss))
			return false;
		if (cveDelegacionSiap == null) {
			if (other.cveDelegacionSiap != null)
				return false;
		} else if (!cveDelegacionSiap.equals(other.cveDelegacionSiap))
			return false;
		if (cveEntidadFed == null) {
			if (other.cveEntidadFed != null)
				return false;
		} else if (!cveEntidadFed.equals(other.cveEntidadFed))
			return false;
		if (cvePrei == null) {
			if (other.cvePrei != null)
				return false;
		} else if (!cvePrei.equals(other.cvePrei))
			return false;
		if (cvePresupuestal == null) {
			if (other.cvePresupuestal != null)
				return false;
		} else if (!cvePresupuestal.equals(other.cvePresupuestal))
			return false;
		if (cveTipoUnidadServicio == null) {
			if (other.cveTipoUnidadServicio != null)
				return false;
		} else if (!cveTipoUnidadServicio.equals(other.cveTipoUnidadServicio))
			return false;
		if (desDelegacionImss == null) {
			if (other.desDelegacionImss != null)
				return false;
		} else if (!desDelegacionImss.equals(other.desDelegacionImss))
			return false;
		if (desEntidadFed == null) {
			if (other.desEntidadFed != null)
				return false;
		} else if (!desEntidadFed.equals(other.desEntidadFed))
			return false;
		if (fecAlta == null) {
			if (other.fecAlta != null)
				return false;
		} else if (!fecAlta.equals(other.fecAlta))
			return false;
		if (fecBaja == null) {
			if (other.fecBaja != null)
				return false;
		} else if (!fecBaja.equals(other.fecBaja))
			return false;
		if (indAscripcion != other.indAscripcion)
			return false;
		if (indSimo != other.indSimo)
			return false;
		if (refJurisdiccionSsa == null) {
			if (other.refJurisdiccionSsa != null)
				return false;
		} else if (!refJurisdiccionSsa.equals(other.refJurisdiccionSsa))
			return false;
		if (refNivelUnidad == null) {
			if (other.refNivelUnidad != null)
				return false;
		} else if (!refNivelUnidad.equals(other.refNivelUnidad))
			return false;
		return true;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cveCluesSalud == null) ? 0 : cveCluesSalud.hashCode());
		result = prime * result + ((cveDelegacionImss == null) ? 0 : cveDelegacionImss.hashCode());
		result = prime * result + ((cveDelegacionSiap == null) ? 0 : cveDelegacionSiap.hashCode());
		result = prime * result + ((cveEntidadFed == null) ? 0 : cveEntidadFed.hashCode());
		result = prime * result + ((cvePrei == null) ? 0 : cvePrei.hashCode());
		result = prime * result + ((cvePresupuestal == null) ? 0 : cvePresupuestal.hashCode());
		result = prime * result + ((cveTipoUnidadServicio == null) ? 0 : cveTipoUnidadServicio.hashCode());
		result = prime * result + ((desDelegacionImss == null) ? 0 : desDelegacionImss.hashCode());
		result = prime * result + ((desEntidadFed == null) ? 0 : desEntidadFed.hashCode());
		result = prime * result + ((fecAlta == null) ? 0 : fecAlta.hashCode());
		result = prime * result + ((fecBaja == null) ? 0 : fecBaja.hashCode());
		result = prime * result + (indAscripcion ? 1231 : 1237);
		result = prime * result + (indSimo ? 1231 : 1237);
		result = prime * result + ((refJurisdiccionSsa == null) ? 0 : refJurisdiccionSsa.hashCode());
		result = prime * result + ((refNivelUnidad == null) ? 0 : refNivelUnidad.hashCode());
		return result;
	}
	@Override
	public String toString() {
		return "UnidadMedica [cveCluesSalud=" + cveCluesSalud + ", cveDelegacionImss=" + cveDelegacionImss
				+ ", desDelegacionImss=" + desDelegacionImss + ", cveDelegacionSiap=" + cveDelegacionSiap
				+ ", cveEntidadFed=" + cveEntidadFed + ", desEntidadFed=" + desEntidadFed + ", cvePrei=" + cvePrei
				+ ", cvePresupuestal=" + cvePresupuestal + ", cveTipoUnidadServicio=" + cveTipoUnidadServicio
				+ ", fecBaja=" + fecBaja
				+ ", fecAlta=" + fecAlta + ", indAscripcion=" + indAscripcion + ", indSimo=" + indSimo
				+ ", refJurisdiccionSsa=" + refJurisdiccionSsa + ", refNivelUnidad=" + refNivelUnidad + "]";
	}
    
    
    

}
