package mx.gob.imss.simo.administracionusuario.common.enumeracion;

import java.util.ArrayList;
import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;

public enum DominioEnum {
	
	METRO("metro", "METRO"), NTE("nte","NORTE"), CTO("cto","CENTRO"),SUR("sur","SUR"),OCC("occ","OCC");

	private String clave;
	private String descripcion;
    
    private DominioEnum(String clave,String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static DominioEnum obtenerPorDescripcion(String descripcion){
		if(descripcion !=null){
		for (DominioEnum estatusEnum : DominioEnum.values()) {
			if(estatusEnum.getClave().equals(descripcion)){
				return estatusEnum;
			}
		}
		}
		return null;
	}
	
	
	public static List<String> obtenerDominios(){
		List<String> catalogos = new ArrayList<String>();
		for(DominioEnum tipoValor : DominioEnum.values()){
			catalogos.add(new String(tipoValor.getClave()));
		}
		return catalogos;
	}
	
	public static List<CatalogoObject> obtenerDominio(){
		List<CatalogoObject> catalogos = new ArrayList<CatalogoObject>();
		for(DominioEnum tipoValor : DominioEnum.values()){
			catalogos.add(new CatalogoObject(tipoValor.getClave(),tipoValor.getDescripcion()));
		}
		return catalogos;
	}
}
