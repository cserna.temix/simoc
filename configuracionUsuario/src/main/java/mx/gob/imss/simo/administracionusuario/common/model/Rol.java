package mx.gob.imss.simo.administracionusuario.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Rol {

    private String cveRol;
    private String refRol;
    private String desRol;
    private Date fecBaja;
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rol other = (Rol) obj;
		if (cveRol == null) {
			if (other.cveRol != null)
				return false;
		} else if (!cveRol.equals(other.cveRol))
			return false;
		if (desRol == null) {
			if (other.desRol != null)
				return false;
		} else if (!desRol.equals(other.desRol))
			return false;
		if (fecBaja == null) {
			if (other.fecBaja != null)
				return false;
		} else if (!fecBaja.equals(other.fecBaja))
			return false;
		if (refRol == null) {
			if (other.refRol != null)
				return false;
		} else if (!refRol.equals(other.refRol))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cveRol == null) ? 0 : cveRol.hashCode());
		result = prime * result + ((desRol == null) ? 0 : desRol.hashCode());
		result = prime * result + ((fecBaja == null) ? 0 : fecBaja.hashCode());
		result = prime * result + ((refRol == null) ? 0 : refRol.hashCode());
		return result;
	}

	
    
}
