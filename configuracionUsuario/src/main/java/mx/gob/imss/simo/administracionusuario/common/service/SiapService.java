package mx.gob.imss.simo.administracionusuario.common.service;

import mx.gob.imss.simo.administracionusuario.common.model.DatosSiap;

//import mx.gob.imss.simo.hospitalizacion.common.model.DatosSiap;

public interface SiapService {

    DatosSiap consultarSiap(String matricula, String delegacion) throws Exception;
}
