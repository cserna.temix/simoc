package mx.gob.imss.simo.administracionusuario.administracion.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.administracionusuario.administracion.repository.AdministracionRepository;
import mx.gob.imss.simo.administracionusuario.administracion.service.AdministracionService;
import mx.gob.imss.simo.administracionusuario.common.model.Parametro;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;



@Service("administracionServices")
public class AdministracionServiceImpl implements AdministracionService{
	
	@Autowired
	private AdministracionRepository administracionRepository;

	@Override
	public Parametro obtenerParametro(String cveParametro) {
		return administracionRepository.obtenerParametro(cveParametro);
	}

	@Override
	public UnidadMedica obtenerDelegacionSiap(String cveDelegacionImss, String cvePresupuestal) {
		return administracionRepository.obtenerDelegacionSIAP(cveDelegacionImss, cvePresupuestal);
	}
	
	

}
