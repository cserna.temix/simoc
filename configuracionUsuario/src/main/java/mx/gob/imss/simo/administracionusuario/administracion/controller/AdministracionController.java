package mx.gob.imss.simo.administracionusuario.administracion.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.jfree.util.Log;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.confirmdialog.ConfirmDialog;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;

import lombok.Data;
import mx.gob.imss.simo.administracionusuario.administracion.service.impl.AdministracionServiceImpl;
import mx.gob.imss.simo.administracionusuario.common.constant.BaseConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.administracionusuario.common.controller.CommonController;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.EstatusEnum;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.TipoAsignacionEnum;
import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;
import mx.gob.imss.simo.administracionusuario.common.model.DatosSiap;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitUnidadMedica;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;

@Data
@ViewScoped
@ManagedBean(name = "administracionUsuarioController")
public class AdministracionController extends CommonController {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected transient List<AsignacionUnidad> lisAsignaciones;
	private CommandButton buttonObtenerUsuario;
	private CommandButton buttonAgregarAsignacion;
	private CommandButton buttonActualizarAsignacion;
	private CommandButton buttonActualizarTablaAsignacion;
	private CommandButton buttonActualizarRol;
	private CommandButton buttonGuardarAsignacion;
	private CommandButton buttonCancelarAsignacion;

	private CommandButton buttonGuardarUsuario;
	private CommandButton buttonCancelarUsuario;
	private CommandButton buttonBusquedaSIAP;
	private CommandButton buttonAgregarRol;
	private CommandButton buttonSalir;
	private CommandButton buttonSalirRol;
	private CommandButton buttonSalirAsignacion;
	private CommandButton buttonSalirAltaUsuario;
	private ConfirmDialog confirmacionUserSave;
	private CommandButton confirmAceptar;

	private List<Usuario> usuarios;

	private AsignacionUnidad asignacionModal;

	private Rol rolModal;

	private Rol rolUsuarioModal;

	private String cvePresupuestalEditar;
	private String stringRolDescripcion;
	private boolean selectBoxHuespedDisabled;

	private boolean metodoVisibleSaveAsignacion;
	private boolean metodoVisibleEditAsignacion;

	private String selectCheckTitular;
	private String selectCheckHuesped;

	private String dominio;

	private String labelCuentaDominio;

	private transient SelectOneMenu selectRol;

	@ManagedProperty("#{objetosSs}")
	private ObjetosEnSesionBean objetosSs;

	@Override
	@PostConstruct
	public void init() {
		super.init();
		logger.info("inicializa controller administracionUsuarioController");
		buttonObtenerUsuario = new CommandButton();
		buttonAgregarAsignacion = new CommandButton();
		buttonActualizarAsignacion = new CommandButton();
		buttonActualizarTablaAsignacion = new CommandButton();
		buttonActualizarRol = new CommandButton();
		buttonGuardarAsignacion = new CommandButton();
		buttonGuardarUsuario = new CommandButton();
		buttonCancelarUsuario = new CommandButton();
		buttonBusquedaSIAP = new CommandButton();
		buttonAgregarRol = new CommandButton();
		buttonCancelarAsignacion = new CommandButton();
		buttonSalir = new CommandButton();
		buttonSalirRol = new CommandButton();
		buttonSalirAsignacion = new CommandButton();
		buttonSalirAltaUsuario = new CommandButton();
		usuarios = new ArrayList<Usuario>();
		usuarioOperativo = new Usuario();
		habilitarBotones(Boolean.TRUE);
		habilitarModalUsuario(Boolean.TRUE);
		restaurarAsignacion();
		rolModal = new Rol();
		rolUsuarioModal = new Rol();
		logger.info("Delegacion objeto ss " + objetosSs.getDelegacionHeader());
		if (objetosSs.getDatosUsuario() != null) {
			logger.info("objeto ss cve presu " + objetosSs.getDatosUsuario().getCvePresupuestal().toString());
			logger.info("objeto ss delegacion " + objetosSs.getDatosUsuario().getCveDelegacionUmae());
		}
		selectBoxHuespedDisabled = false;
		metodoVisibleSaveAsignacion = true;
		metodoVisibleEditAsignacion = true;
		stringRolDescripcion = "";
		confirmAceptar = new CommandButton();
		confirmacionUserSave = new ConfirmDialog();
		dominio = new String();

		getButtonObtenerUsuario().setDisabled(Boolean.TRUE);
		getButtonGuardarAsignacion().setDisabled(Boolean.TRUE);
		getButtonActualizarAsignacion().setDisabled(Boolean.TRUE);
		getButtonAgregarRol().setDisabled(Boolean.TRUE);
		getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
		getButtonBusquedaSIAP().setDisabled(Boolean.TRUE);
		tieneFoco = "administrarUsuarioForm:idDominio";
		// tieneFoco = "idDominio";
	}

	public void limpiar() {
		// TODO Auto-generated method stub

	}

	public void obtenerUsuario() {
		try {
			if (validarBusquedaCuentaDominio()) {
				labelCuentaDominio = null;
				String cuentaDominio = getUsuarioOperativo().getRefPersonalOperativo();
				String dominio = getSelectDominio().getValue().toString();
				logger.info("dominio selecionado: " + dominio + " cuentaDominio: " + cuentaDominio);
				setUsuarioOperativo(new Usuario());
				setLisAsignaciones(new ArrayList<>());
				setStringRolDescripcion("");
				List<Usuario> usuarioDirectorioActivo = busquedaDirectorioActivo(dominio, cuentaDominio);
				if (usuarioDirectorioActivo == null || usuarioDirectorioActivo.isEmpty()) {
					setTieneFoco(getTextCuentaDominio().getClientId());
					setUsuarioOperativo(new Usuario());
					habilitarBotones(Boolean.TRUE);
					throw new AdministracionUsuarioException(MensajesErrorConstants.ME_106);
				} else if (usuarioDirectorioActivo.size() > 1) {
					logger.info("usuariosEncontrados" + usuarioDirectorioActivo.size());
					setUsuarios(usuarioDirectorioActivo);
					tieneFoco = getTextUserConsecutivo().getClientId();
					if (!getUsuarios().isEmpty()) {
						for (int i = 0; i < getUsuarios().size(); i++) {
							getUsuarios().get(i).setConsecutivo(i + 1);
						}
					}
					setNumeroUsuario(null);
					getTextUserConsecutivo().setValue("");
					habilitarBotones(Boolean.TRUE);
					RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_CUENTA_DOMINIO);
				} else if (usuarioDirectorioActivo.size() == 1
						&& !cuentaDominio.equals(usuarioDirectorioActivo.get(0).getRefPersonalOperativo())) {
					logger.info("usuariosEncontrados" + usuarioDirectorioActivo.size());
					setUsuarios(usuarioDirectorioActivo);
					tieneFoco = getTextUserConsecutivo().getClientId();
					if (!getUsuarios().isEmpty()) {
						for (int i = 0; i < getUsuarios().size(); i++) {
							getUsuarios().get(i).setConsecutivo(i + 1);
						}
					}
					setNumeroUsuario(null);
					getTextUserConsecutivo().setValue("");
					habilitarBotones(Boolean.TRUE);
					RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_CUENTA_DOMINIO);
				} else {
					List<Usuario> usuariosSimoc = usuarioService.listaUsuario(cuentaDominio);
					if (usuariosSimoc != null && !usuariosSimoc.isEmpty()) {
						setUsuarioOperativo(usuariosSimoc.get(0));
						if (getUsuarioOperativo().getAsignacionUnidad() != null
								&& !getUsuarioOperativo().getAsignacionUnidad().isEmpty()) {
							setLisAsignaciones(getUsuarioOperativo().getAsignacionUnidad());
							habilitarBotones(Boolean.FALSE);
							habilitarModalUsuario(Boolean.TRUE);
							setStringRolDescripcion(
									getUsuarioOperativo().getAsignacionUnidad().get(0).getRol().getDesRol());
						} else {
							throw new AdministracionUsuarioException(
									"No se encontraron asignaciones para este usuario");
						}
					} else {
						habilitarBotones(Boolean.TRUE);
						setTieneFoco(getConfirmAceptar().getClientId());
						labelCuentaDominio = cuentaDominio;
						String matricula = usuarioDirectorioActivo.get(0).getCveMatricula();
						if (matricula != null) {
							getUsuarioOperativo().setCveMatricula(matricula);
							logger.info(matricula);
						}
						RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_CONFIRMACION);
					}
				}
			}
		} catch (AdministracionUsuarioException e) {
			habilitarBotones(Boolean.TRUE);
			setTieneFoco(getTextCuentaDominio().getClientId());
			agregarMensajeError(e);
		}
	}

	public CommandButton getButtonObtenerUsuario() {
		return buttonObtenerUsuario;
	}

	public void setButtonObtenerUsuario(CommandButton buttonObtenerUsuario) {
		this.buttonObtenerUsuario = buttonObtenerUsuario;
	}

	public CommandButton getButtonAgregarAsignacion() {
		return buttonAgregarAsignacion;
	}

	public void setButtonAgregarAsignacion(CommandButton buttonAgregarAsignacion) {
		this.buttonAgregarAsignacion = buttonAgregarAsignacion;
	}

	public CommandButton getButtonAgregarRol() {
		return buttonAgregarRol;
	}

	public void setButtonAgregarRol(CommandButton buttonAgregarRol) {
		this.buttonAgregarRol = buttonAgregarRol;
	}

	public CommandButton getButtonActualizarAsignacion() {
		return buttonActualizarAsignacion;
	}

	public void setButtonActualizarAsignacion(CommandButton buttonActualizarAsignacion) {
		this.buttonActualizarAsignacion = buttonActualizarAsignacion;
	}

	public void mostrarUsuariosSeleccionado() {
		if (getNumeroUsuario() == null || getNumeroUsuario() > getUsuarios().size()) {
			setTieneFoco(getTextUserConsecutivo().getClientId());
		} else if (getNumeroUsuario() == 0) {
			setNumeroUsuario(null);
			setTieneFoco(getTextCuentaDominio().getClientId());
			habilitarBotones(Boolean.TRUE);
			setUsuarioOperativo(new Usuario());
			restaurarAsignacion();
			RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CUENTA_DOMINIO);
		} else {
			try {
				int numeroIndex = getNumeroUsuario() - 1;
				Usuario usuario = getUsuarios().get(numeroIndex);
				// obtener todo el usuario
				List<Usuario> usuariosSimoc = usuarioService.listaUsuario(usuario.getRefPersonalOperativo());
				if (usuariosSimoc != null && !usuariosSimoc.isEmpty()) {
					setUsuarioOperativo(usuariosSimoc.get(0));
					if (getUsuarioOperativo().getAsignacionUnidad() != null
							&& !getUsuarioOperativo().getAsignacionUnidad().isEmpty()) {
						setLisAsignaciones(getUsuarioOperativo().getAsignacionUnidad());
						habilitarBotones(Boolean.FALSE);
						habilitarModalUsuario(Boolean.TRUE);
						setStringRolDescripcion(
								getUsuarioOperativo().getAsignacionUnidad().get(0).getRol().getDesRol());
					} else {
						logger.error("No se encontraron asignaciones para este usuario");
					}
					RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CUENTA_DOMINIO);
				} else {
					habilitarBotones(Boolean.TRUE);
					setTieneFoco(getConfirmAceptar().getClientId());
					labelCuentaDominio = usuario.getRefPersonalOperativo();
					String matricula = "";
					if (usuario.getCveMatricula() != null) {
						matricula = usuario.getCveMatricula();
						logger.info("matricula: " + matricula);
					}
					getUsuarioOperativo().setRefPersonalOperativo(labelCuentaDominio);
					getUsuarioOperativo().setCveMatricula(matricula);
					setNumeroUsuario(null);
					getTextUserConsecutivo().setValue("");
					RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CUENTA_DOMINIO);
					RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_CONFIRMACION);
				}
			} catch (Exception e) { // provicional
				logger.error("Error :" + e.getMessage());
				setUsuarioOperativo(new Usuario());
				setNumeroUsuario(null);
				agregarMensajeError(new AdministracionUsuarioException("Ocurrio un error"));
				setTieneFoco(getTextCuentaDominio().getClientId());
				habilitarBotones(Boolean.TRUE);
				RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CUENTA_DOMINIO);
			}
		}
	}

	public void mostrarRolModal() {
		Rol rol = getUsuarioService().obtenerRol(getUsuarioOperativo().getRefPersonalOperativo());
		getSelectRol().setValue(rol.getCveRol());
		setTieneFoco(getSelectRol().getClientId());
		getButtonActualizarRol().setDisabled(Boolean.TRUE);
		RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_AGREGAR_ROL);
	}

	public void mostrarGuardarAsignacionModal() {
		restaurarAsignacion();
		reiniciarModalAsignacion();
		cargarCatalogoTipoAsignacion();
		getSelectAsigTipoAsignacion().setDisabled(false);
		metodoVisibleEditAsignacion = false;
		metodoVisibleSaveAsignacion = true;
		asignacionModal.setFecBajaUnidad(getUsuarioService().calcularFechaBaja(0));
		RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_ASIGNACION);
		setTieneFoco(getTextAsigCveDelegacion().getClientId());
	}

	public void mostrarActualizarAsignacionModal(String cvePresupuestal) {
		try {
			metodoVisibleEditAsignacion = true;
			metodoVisibleSaveAsignacion = false;
			setCvePresupuestalEditar(cvePresupuestal);
			cargarCatalogoTipoAsignacion();
			for (AsignacionUnidad asignacionUnidad : lisAsignaciones) {
				if (asignacionUnidad.getUnidadMedica() != null
						&& asignacionUnidad.getUnidadMedica().getCvePresupuestal() != null
						&& asignacionUnidad.getUnidadMedica().getCvePresupuestal().equals(cvePresupuestal)) {
					asignacionModal = asignacionUnidad;
					break;
				}
			}
			if (asignacionModal.getUnidadMedica().getCveDelegacionImss() != null
					|| !asignacionModal.getUnidadMedica().getCveDelegacionImss().isEmpty()) {
				setCatalogoClavePresupuestal(catalogosAdministracionServices
						.obtenerCatalogoUnidadesMedicas(asignacionModal.getUnidadMedica().getCveDelegacionImss()));
				setCatalogoClavePresupuestalString(convertirCatalogoString(getCatalogoClavePresupuestal()));
			}
			if (asignacionModal.getUnidadMedica().getCvePresupuestal() != null
					|| !asignacionModal.getUnidadMedica().getCvePresupuestal().isEmpty()) {
				for (String catalogo : getCatalogoClavePresupuestalString()) {
					if (catalogo.startsWith(asignacionModal.getUnidadMedica().getCvePresupuestal())) {
						getAutoAsigPresupuestal().setValue(catalogo);
					}
				}
			}
			if (asignacionModal.getIndActivoUnidad() != null && asignacionModal.getIndActivoUnidad()) {
				getSelectAsigEstatus().setValue(EstatusEnum.ACTIVO.getClave());
			} else {
				getSelectAsigEstatus().setValue(EstatusEnum.DESACTIVADOR.getClave());
			}
			if (asignacionModal.getIndTitular() != null && asignacionModal.getIndTitular()) {
				setCatalogoTipoAsignacion(TipoAsignacionEnum.obtenerCatalogo());
				setCatalogoTipoAsignacionString(convertirCatalogoString(getCatalogoTipoAsignacion()));
				getSelectAsigTipoAsignacion().setValue(TipoAsignacionEnum.TITULAR.getClave());
				getSelectAsigTipoAsignacion().setDisabled(true);
			} else {
				getSelectAsigTipoAsignacion().setValue(TipoAsignacionEnum.HUESPED.getClave());
				getSelectAsigTipoAsignacion().setDisabled(false);
			}
			RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_ASIGNACION);
		} catch (Exception e) {
			agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_020));
		}
	}

	public void guardarAsignacionModal() {
		if (validarGuardaddoAsignacion()) {
			String cvePresupuestalTotal = getAsignacionModal().getUnidadMedica().getCvePresupuestal();
			String tipoAsignacion = getSelectAsigTipoAsignacion().getValue().toString();
			String estatus = getSelectAsigEstatus().getValue().toString();

			String cvePresupuestal = cvePresupuestalTotal.substring(0, 13).trim();
			String desPresupuestal = cvePresupuestalTotal.substring(13);
			getAsignacionModal().getUnidadMedica().setCvePresupuestal(cvePresupuestal);
			getAsignacionModal().getUnidadMedica().setDesPresupuestal(desPresupuestal);
			getAsignacionModal().setIndTitular(tipoAsignacion.equals("1"));
			getAsignacionModal().setIndActivoUnidad(estatus.equals("1"));
			logger.error("cvePresupuestal : " + cvePresupuestal);
			try {
				getUsuarioService().insertarAsignacion(getAsignacionModal(), getUsuarioOperativo());
				RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_ASIGNACION);
				setLisAsignaciones(
						getUsuarioService().obtenerAsignaciones(getUsuarioOperativo().getRefPersonalOperativo()));
				agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
			} catch (AdministracionUsuarioException e) {
				logger.error("Mensaje error : " + e.getMessage());
				agregarMensajeError(e);
				for (String clave : e.getClaves()) {
					if (clave.equals(MensajesErrorConstants.ME_110)) {
						setCatalogoClavePresupuestal(new ArrayList<CatalogoObject>());
						setCatalogoClavePresupuestalString(new ArrayList<String>());
						getAutoAsigPresupuestal().setValue(BaseConstants.CADENA_VACIA);
						getTextAsigCveDelegacion().setValue(BaseConstants.CADENA_VACIA);
						getTextAsigDesDelegacion().setValue(BaseConstants.CADENA_VACIA);
						// getSelectAsigTipoAsignacion().resetValue();
						getSelectAsigEstatus().resetValue();
						setTieneFoco(getTextAsigCveDelegacion().getClientId());
						break;
					}
				}
			}
		}
	}

	public void editarAsignacionModal() {
		if (validarGuardaddoAsignacion()) {
			if (getCvePresupuestalEditar().length() > 13) {
				setCvePresupuestalEditar(getCvePresupuestalEditar().substring(0, 13).trim());
				logger.info("Modificado :" + getCvePresupuestalEditar());
			}
			try {
				String cvePresupuestalTotal = getAsignacionModal().getUnidadMedica().getCvePresupuestal();
				String tipoAsignacion = getSelectAsigTipoAsignacion().getValue().toString();
				String estatus = getSelectAsigEstatus().getValue().toString();

				String cvePresupuestal = cvePresupuestalTotal.substring(0, 13).trim();
				String desPresupuestal = cvePresupuestalTotal.substring(13);
				getAsignacionModal().getUnidadMedica().setCvePresupuestal(cvePresupuestal);
				getAsignacionModal().getUnidadMedica().setDesPresupuestal(desPresupuestal);
				getAsignacionModal().setIndTitular(tipoAsignacion.equals("1"));
				getAsignacionModal().setIndActivoUnidad(estatus.equals("1"));
				if (getAsignacionModal().getFecAltaUnidad() == null) {

				}
				getUsuarioService().updateAsignacion(getAsignacionModal(), getUsuarioOperativo(),
						getCvePresupuestalEditar());
				RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_ASIGNACION);
				setLisAsignaciones(
						getUsuarioService().obtenerAsignaciones(getUsuarioOperativo().getRefPersonalOperativo()));
				setCvePresupuestalEditar(null);
				agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
			} catch (AdministracionUsuarioException e) {
				setCvePresupuestalEditar(null);
				agregarMensajeError(e);
				for (String clave : e.getClaves()) {
					if (clave.equals(MensajesErrorConstants.ME_110)) {
						setCatalogoClavePresupuestal(new ArrayList<CatalogoObject>());
						setCatalogoClavePresupuestalString(new ArrayList<String>());
						getAutoAsigPresupuestal().setValue(BaseConstants.CADENA_VACIA);
						getTextAsigCveDelegacion().setValue(BaseConstants.CADENA_VACIA);
						getTextAsigDesDelegacion().setValue(BaseConstants.CADENA_VACIA);
						getSelectAsigEstatus().resetValue();
						try {
							setLisAsignaciones(getUsuarioService()
									.obtenerAsignaciones(getUsuarioOperativo().getRefPersonalOperativo()));
						} catch (AdministracionUsuarioException e1) {
							agregarMensajeError(e1);
						}
						setTieneFoco(getTextAsigCveDelegacion().getClientId());
						break;
					}
				}
			}
		}
	}

	public void guardarUsuarioSIMOC() {
		if (validarGuardarUsuario()) {
			try {
				 activeDirectoryService.agregarGrupoSeguridad(labelCuentaDominio);

				String cvePresupuestalTotal = getAutoUserPresupuestal().getValue().toString();

				String cvePresupuestal = cvePresupuestalTotal.substring(0, 13).trim();
				String desPresupuestal = cvePresupuestalTotal.substring(13);
				getUsuarioOperativo().setAsignacionUnidad(new ArrayList<AsignacionUnidad>());

				AsignacionUnidad asignacion = new AsignacionUnidad();
				asignacion.setUnidadMedica(new UnidadMedica());
				asignacion.getUnidadMedica().setCvePresupuestal(cvePresupuestal);
				asignacion.getUnidadMedica().setDesPresupuestal(desPresupuestal);
				asignacion.getUnidadMedica().setCveDelegacionImss(getUsuarioOperativo().getCveDelegacion());
				asignacion.getUnidadMedica().setDesDelegacionImss(getUsuarioOperativo().getDescDelegacion());
				asignacion.setRol(getRolUsuarioModal());
				asignacion.setIndTitular(true);
				asignacion.setIndActivoUnidad(true);
				asignacion.setFecAltaUnidad(new Date());
				asignacion.setFecBajaUnidad(getUsuarioService().calcularFechaBaja(1));

				getUsuarioOperativo().setRefPersonalOperativo(labelCuentaDominio);
				getUsuarioOperativo().getAsignacionUnidad().add(asignacion);
				getUsuarioOperativo().setIndBloqueado(0);
				getUsuarioOperativo().setCaIntentos(0);
				getUsuarioOperativo().setFecBaja(null);
				getUsuarioOperativo().setIndActivo(1);
				getUsuarioService().insertarUsuario(getUsuarioOperativo());
				RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_AGREGAR_USUARIO);
				setStringRolDescripcion(getRolUsuarioModal().getDesRol());
				agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
			} catch (AdministracionUsuarioException e) {
				agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_020));
			}
		}
	}

	public void cancelarUsuarioModal() throws AdministracionUsuarioException {
		reiniciarModalUsuario();
		String matricula = getUsuarioOperativo().getCveMatricula();
		getTextUserMatricula().resetValue();
		String cuentaDominio = getUsuarioOperativo().getRefPersonalOperativo();
		setUsuarioOperativo(new Usuario());
		getUsuarioOperativo().setCveMatricula(matricula);
		getUsuarioOperativo().setRefPersonalOperativo(cuentaDominio);
		getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
		getButtonBusquedaSIAP().setDisabled(Boolean.TRUE);

		getTextUserCveDelegacion().setDisabled(Boolean.TRUE);
		getAutoUserPresupuestal().setDisabled(Boolean.TRUE);
		getSelectUserRol().setDisabled(Boolean.TRUE);
		getSelectUserRol().setValue("");
		setTieneFoco(getTextUserMatricula().getClientId());
	}

	public void actualizarRolModal() {
		try {
			if (!validCampo(rolModal.getCveRol())) {
				getUsuarioService().insertarRol(rolModal, getUsuarioOperativo());
				setLisAsignaciones(
						getUsuarioService().obtenerAsignaciones(getUsuarioOperativo().getRefPersonalOperativo()));
				RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_AGREGAR_ROL);
				setStringRolDescripcion(getLisAsignaciones().get(0).getRol().getDesRol());
				agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
			} else {
				agregarMensajeError(new AdministracionUsuarioException(getArray(MensajesErrorConstants.ME_001),
						getArray(BaseConstants.REQUERIDO_ROL)));
				setTieneFoco(getSelectRol().getClientId());
			}
		} catch (AdministracionUsuarioException e) {
			setTieneFoco(getSelectRol().getClientId());
			agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_002));
		}
	}

	public void busquedaSIAPUsuario() {
		try {
			if (validarMatricula(getTextUserMatricula(), "Matricula")) {

				DatosSiap daosSIAP;
				String delegacion = objetosSs.getDatosUsuario().getCveDelegacionUmae();

				if (objetosSs.getDatosUsuario().getCveDelegacionUmae().equals("40")
						|| objetosSs.getDatosUsuario().getCveDelegacionUmae().equals("39")) {
					logger.debug("delegacion: " + objetosSs.getDatosUsuario().getCveDelegacionUmae());
					logger.debug("cvepresupuestal: " + objetosSs.getDatosUsuario().getCvePresupuestal());
					UnidadMedica unidadMedica = getAdministracionServices().obtenerDelegacionSiap(
							objetosSs.getDatosUsuario().getCveDelegacionUmae(),
							objetosSs.getDatosUsuario().getCvePresupuestal());
					delegacion = unidadMedica.getCveDelegacionSiap();
				}
				logger.debug("delegacion: " + delegacion);
				daosSIAP = siapService.consultarSiap(getUsuarioOperativo().getCveMatricula(), delegacion);
				logger.debug("matricula: " + getTextUserMatricula().getValue().toString());
				String matricula = getUsuarioOperativo().getCveMatricula();
				String cuentaDominio = labelCuentaDominio;
				setUsuarioOperativo(new Usuario());
				logger.debug("cuentadominio: " + cuentaDominio);
				getUsuarioOperativo().setRefPersonalOperativo(cuentaDominio);
				getUsuarioOperativo().setCveMatricula(matricula);
				getUsuarioOperativo().setRefNombre(daosSIAP.getNombre().toUpperCase());
				getUsuarioOperativo().setRefApellidoPaterno(daosSIAP.getApellidoPaterno().toUpperCase());
				if (daosSIAP.getApellidoMaterno() != null) {
					getUsuarioOperativo().setRefApellidoMaterno(daosSIAP.getApellidoMaterno().toUpperCase());
				}
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append(getUsuarioOperativo().getRefPersonalOperativo())
						.append(BaseConstants.DOMINIO_CORREO_IMSS);
				getUsuarioOperativo().setRefEmail(stringBuilder.toString());
				getUsuarioOperativo().setFecAlta(new Date());

				getTextUserCveDelegacion().setDisabled(Boolean.FALSE);
				getAutoUserPresupuestal().setDisabled(Boolean.FALSE);
				getSelectUserRol().setDisabled(Boolean.FALSE);

				getUsuarioOperativo().setCveDelegacion(BaseConstants.CADENA_VACIA);
				getUsuarioOperativo().setDescDelegacion(BaseConstants.CADENA_VACIA);
				getUsuarioOperativo().setCvePresupuestal(BaseConstants.CADENA_VACIA);
				getUsuarioOperativo().setDescPresupuestal(BaseConstants.CADENA_VACIA);
				getTextUserCveDelegacion().resetValue();
				getSelectUserRol().resetValue();

				setTieneFoco(getTextUserCveDelegacion().getClientId());
			} else {
				reiniciarModalUsuario();
				getTextUserCveDelegacion().setDisabled(Boolean.TRUE);
				getAutoUserPresupuestal().setDisabled(Boolean.TRUE);
				getSelectUserRol().setDisabled(Boolean.TRUE);

				getButtonBusquedaSIAP().setDisabled(Boolean.TRUE);
				getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
				setTieneFoco(getTextUserMatricula().getClientId());
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			reiniciarModalUsuario();
			getTextUserCveDelegacion().setDisabled(Boolean.TRUE);
			getTextUserDesDelegacion().setDisabled(Boolean.TRUE);
			getAutoUserPresupuestal().setDisabled(Boolean.TRUE);
			getSelectUserRol().setDisabled(Boolean.TRUE);
			String cuentaDominio = getUsuarioOperativo().getRefPersonalOperativo();
			String matricula = getUsuarioOperativo().getCveMatricula();
			setUsuarioOperativo(new Usuario());
			getUsuarioOperativo().setRefPersonalOperativo(cuentaDominio);
			getUsuarioOperativo().setCveMatricula(matricula);
			getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
			agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_996));

		}
	}

	public void validacionAsigDelegacion() {
		if (validacveDelegacion(getTextAsigCveDelegacion(), getTextAsigDesDelegacion(), 2,
				getCatalogoDelegacionString(), getCatalogoDelegacion(), "Clave Delegacion")) {
			setTieneFoco(getAutoAsigPresupuestal().getClientId());
		} else {
			getButtonGuardarAsignacion().setDisabled(Boolean.TRUE);
			getButtonActualizarAsignacion().setDisabled(Boolean.TRUE);

			getAsignacionModal().getUnidadMedica().setCveDelegacionImss(BaseConstants.CADENA_VACIA);
			getAsignacionModal().getUnidadMedica().setDesDelegacionImss(BaseConstants.CADENA_VACIA);
			getAsignacionModal().getUnidadMedica().setCvePresupuestal(BaseConstants.CADENA_VACIA);

			getTextAsigCveDelegacion().setValue(BaseConstants.CADENA_VACIA);
			getTextAsigDesDelegacion().setValue(BaseConstants.CADENA_VACIA);
			getAutoAsigPresupuestal().setValue(BaseConstants.CADENA_VACIA);

			setTieneFoco(getTextAsigCveDelegacion().getClientId());
			setCatalogoClavePresupuestal(new ArrayList<>());
			setCatalogoClavePresupuestalString(new ArrayList<>());
		}
	}

	public void validarAsigCvePresupuestal() throws AdministracionUsuarioException {
		if (validarCatalogoPresupuestal(getAutoAsigPresupuestal(), 12, getCatalogoClavePresupuestalString(),
				getCatalogoClavePresupuestal(), "Clave Presupuestal")) {
			if (getSelectAsigTipoAsignacion().isDisabled()) {
				// setTieneFoco(getSelectAsigEstatus().getClientId());
				setTieneFoco(getSelectAsigEstatus().getClientId());
				logger.info("foco estatus");
			} else {
				setTieneFoco(getSelectAsigTipoAsignacion().getClientId());
				logger.info("foco tipoasignacion");
			}
		} else {
			getButtonGuardarAsignacion().setDisabled(Boolean.TRUE);
			getButtonActualizarAsignacion().setDisabled(Boolean.TRUE);

			getAsignacionModal().getUnidadMedica().setCvePresupuestal(BaseConstants.CADENA_VACIA);
			getAutoAsigPresupuestal().setValue(BaseConstants.CADENA_VACIA);
			setTieneFoco(getAutoAsigPresupuestal().getClientId());
		}
	}

	public void validarAsigSelectTipoAsig() throws AdministracionUsuarioException {
		if (validarSelectMenuGuardar(getSelectAsigTipoAsignacion(), "Tipo asignacion")) {
			logger.info("validarAsigSelectTipoAsig estatus");
			setTieneFoco(getSelectAsigEstatus().getClientId());
		} else {
			getButtonGuardarAsignacion().setDisabled(Boolean.TRUE);
			getButtonActualizarAsignacion().setDisabled(Boolean.TRUE);
			getSelectAsigTipoAsignacion().resetValue();
			setTieneFoco(getSelectAsigTipoAsignacion().getClientId());
		}
	}

	public void validarAsigSelectEstatus() throws AdministracionUsuarioException {
		if (validarSelectMenuGuardar(getSelectAsigEstatus(), "Estatus")) {
			if (metodoVisibleEditAsignacion) {
				logger.info("metodoVisibleEditAsignacion");
				getButtonActualizarAsignacion().setDisabled(Boolean.FALSE);
				setTieneFoco(getButtonActualizarAsignacion().getClientId());
			} else if (metodoVisibleSaveAsignacion) {
				logger.info("metodoVisibleSaveAsignacion");
				setTieneFoco(getButtonGuardarAsignacion().getClientId());
				getButtonGuardarAsignacion().setDisabled(Boolean.FALSE);
			}
		} else {
			getButtonGuardarAsignacion().setDisabled(Boolean.TRUE);
			getButtonActualizarAsignacion().setDisabled(Boolean.TRUE);
			getSelectAsigEstatus().resetValue();
			setTieneFoco(getSelectAsigEstatus().getClientId());
		}
	}

	public void validarUserMatricula() throws AdministracionUsuarioException {
		if (validarMatricula(getTextUserMatricula(), "Matricula")) {
			getTextUserCveDelegacion().setDisabled(Boolean.TRUE);
			getAutoUserPresupuestal().setDisabled(Boolean.TRUE);
			getTextUserCveDelegacion().setValue(BaseConstants.CADENA_VACIA);
			setRolUsuarioModal(new Rol());
			getRolUsuarioModal().setCveRol("");
			getSelectUserRol().setDisabled(Boolean.TRUE);
			getSelectUserRol().setValue("");
			getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
			getButtonBusquedaSIAP().setDisabled(Boolean.FALSE);
			reiniciarModalUsuario();
			setTieneFoco(getButtonBusquedaSIAP().getClientId());
		} else {
			getButtonBusquedaSIAP().setDisabled(Boolean.TRUE);
			getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
			cancelarUsuarioModal();
			setTieneFoco(getTextUserMatricula().getClientId());
			getUsuarioOperativo().setCveMatricula(BaseConstants.CADENA_VACIA);
			getTextUserMatricula().setValue(BaseConstants.CADENA_VACIA);
		}
	}

	public void validarUserDelegacion() throws AdministracionUsuarioException {
		if (validacveDelegacion(getTextUserCveDelegacion(), getTextUserDesDelegacion(), 2,
				getCatalogoDelegacionString(), getCatalogoDelegacion(), "Clave Delegacion")) {
			setTieneFoco(getAutoUserPresupuestal().getClientId());
		} else {
			getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
			setTieneFoco(getTextUserCveDelegacion().getClientId());

			getUsuarioOperativo().setCveDelegacion("");
			getUsuarioOperativo().setDescDelegacion("");
			getUsuarioOperativo().setCvePresupuestal("");

			getTextUserCveDelegacion().setValue(BaseConstants.CADENA_VACIA);
			getAutoUserPresupuestal().setValue(BaseConstants.CADENA_VACIA);
			getTextUserDesDelegacion().setValue(BaseConstants.CADENA_VACIA);
			setCatalogoClavePresupuestal(new ArrayList<>());
			setCatalogoClavePresupuestalString(convertirCatalogoString(getCatalogoClavePresupuestal()));
		}
	}

	public void validarUserCvePresupuestal() throws AdministracionUsuarioException {
		if (validarCatalogoPresupuestal(getAutoUserPresupuestal(), 12, getCatalogoClavePresupuestalString(),
				getCatalogoClavePresupuestal(), "Clave Presupuestal")) {
			setTieneFoco(getSelectUserRol().getClientId());
		} else {
			getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
			setTieneFoco(getAutoUserPresupuestal().getClientId());

			getUsuarioOperativo().setCvePresupuestal("");
			getAutoUserPresupuestal().setValue(BaseConstants.CADENA_VACIA);
		}
	}

	public void validarUserRolSelecionado() {
		if (validarSelectMenuGuardar(getSelectUserRol(), "Rol")) {
			getButtonGuardarUsuario().setDisabled(Boolean.FALSE);
			setTieneFoco(getButtonGuardarUsuario().getClientId());
		} else {
			getButtonGuardarUsuario().setDisabled(Boolean.TRUE);
			getSelectUserRol().setValue(BaseConstants.CADENA_VACIA);
			setTieneFoco(getSelectUserRol().getClientId());
		}
	}

	public void validarRolSelecionado() {
		if (validarSelectMenuGuardar(getSelectRol(), "Rol")) {
			getButtonActualizarRol().setDisabled(Boolean.FALSE);
			setTieneFoco(getButtonActualizarRol().getClientId());
		} else {
			getButtonActualizarRol().setDisabled(Boolean.TRUE);
		}
	}

	public List<String> filtrarCatalogoClavePresupuestal(String query) {
		return filtrarCatalogo(getCatalogoClavePresupuestalString(), query, BaseConstants.LONGITUD_CVEPRESUPUESTA);
	}

	private List<Usuario> busquedaDirectorioActivo(String dominio, String refPersonalOperativo)
			throws AdministracionUsuarioException {
		return activeDirectoryService.searchDirectorioActivo(Boolean.FALSE, dominio, refPersonalOperativo);
	}

	private void habilitarBotones(Boolean condicion) {
		getButtonAgregarAsignacion().setDisabled(condicion);
		getButtonAgregarRol().setDisabled(condicion);
	}

	public void modificarEstatus() {
		String cvePresupuestalSelect = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("cvePresupuestalSelect");
		String estatus = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("indEstatus");
		logger.info("cvePresupuestalSelect : " + cvePresupuestalSelect + " estatus :" + estatus);
		SitUnidadMedica asignacion = getUsuarioService()
				.obtenerAsignacionTitular(getUsuarioOperativo().getRefPersonalOperativo());
		if (asignacion != null && asignacion.getCvePresupuestal() != null) {
			boolean estatusPersonal = false;
			if (asignacion.getCvePresupuestal().equals(cvePresupuestalSelect) && estatus.equals("true")) {
				logger.info("muestra modal");
				if (getUsuarioService().updateEstatusAllAsig(getUsuarioOperativo().getRefPersonalOperativo(), false)) {
					logger.info("Se actualizaron correctamente");
					estatusPersonal = Boolean.FALSE;
					selectBoxHuespedDisabled = Boolean.TRUE;
					getButtonAgregarAsignacion().setDisabled(Boolean.TRUE);
				} else {
					logger.info("No se actualizaron correctamente");
				}
			} else {
				getUsuarioService().updateEstatusAsignacion(getUsuarioOperativo().getRefPersonalOperativo(),
						cvePresupuestalSelect, estatus.equals("true") ? false : true);
				selectBoxHuespedDisabled = false;
				estatusPersonal = Boolean.TRUE;
				getButtonAgregarAsignacion().setDisabled(Boolean.FALSE);
			}
			String personacuentaDominio = getUsuarioOperativo().getRefPersonalOperativo();
			logger.info("personacuentaDominio: " + personacuentaDominio);
			if (getUsuarioService().updateEstatusPersonalOperativo(personacuentaDominio, estatusPersonal)) {
				logger.info("Se actualizaron correctamente");
			} else {
				logger.info("No se actualizo");
			}
		} else {
			getUsuarioService().updateEstatusAsignacion(getUsuarioOperativo().getRefPersonalOperativo(),
					cvePresupuestalSelect, estatus.equals("true") ? false : true);
		}
		try {
			setLisAsignaciones(
					getUsuarioService().obtenerAsignaciones(getUsuarioOperativo().getRefPersonalOperativo()));
		} catch (AdministracionUsuarioException e) {
			e.printStackTrace();
		}
	}

	private void cargarCatalogoTipoAsignacion() {
		SitUnidadMedica asignacion = getUsuarioService()
				.obtenerAsignacionTitular(getUsuarioOperativo().getRefPersonalOperativo());
		if (asignacion != null && asignacion.getCvePresupuestal() != null) {
			List<CatalogoObject> dtos = new ArrayList<>();
			dtos.add(new CatalogoObject(TipoAsignacionEnum.HUESPED.getClave(),
					TipoAsignacionEnum.HUESPED.getDescripcion()));
			setCatalogoTipoAsignacion(dtos);
			setCatalogoTipoAsignacionString(convertirCatalogoString(getCatalogoTipoAsignacion()));
			getSelectAsigTipoAsignacion().setValue(TipoAsignacionEnum.HUESPED.getClave());
		} else {
			setCatalogoTipoAsignacion(TipoAsignacionEnum.obtenerCatalogo());
			setCatalogoTipoAsignacionString(convertirCatalogoString(getCatalogoTipoAsignacion()));
		}
	}

	private boolean validarGuardaddoAsignacion() {
		boolean delegacion = validarDelegacionGuardado(getTextAsigCveDelegacion(), getCatalogoDelegacionString());
		boolean cvePresupuestal = validarCvePresupuestalGuardado(getAutoAsigPresupuestal(),
				getCatalogoClavePresupuestalString());
		boolean selectTipoAsignacion = validarSelectMenuGuardar(getSelectAsigTipoAsignacion(), "Tipo de asignacion");
		boolean selectEstatus = validarSelectMenuGuardar(getSelectAsigEstatus(), "Estatus");
		return delegacion && cvePresupuestal && selectTipoAsignacion && selectEstatus;
	}

	// validar guardado usuario
	private boolean validarGuardarUsuario() {
		boolean delegacionValidacion = validarDelegacionGuardado(getTextUserCveDelegacion(),
				getCatalogoDelegacionString());
		boolean cvePresupuestalValidacion = validarCvePresupuestalGuardado(getAutoUserPresupuestal(),
				getCatalogoClavePresupuestalString());
		boolean rolValidacion = validarSelectMenuGuardar(getSelectUserRol(), "Estatus");
		return delegacionValidacion && cvePresupuestalValidacion && rolValidacion;
	}

	public void confirmAcepto() {
		habilitarModalUsuario(Boolean.TRUE);
		reiniciarModalUsuario();
		getTextUserMatricula().resetValue();
		getTextUserCveDelegacion().setDisabled(Boolean.TRUE);
		getAutoUserPresupuestal().setDisabled(Boolean.TRUE);
		getSelectUserRol().setDisabled(Boolean.TRUE);

		setTieneFoco(getTextUserMatricula().getClientId());
		RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CONFIRMACION);
		RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_AGREGAR_USUARIO);
	}

	public void validarDominio() {
		if (validarSelectMenuGuardar(getSelectDominio(), "Dominio")) {
			setTieneFoco(getTextCuentaDominio().getClientId());
			getButtonObtenerUsuario().setDisabled(Boolean.TRUE);
		} else {
			logger.info("validarCuentaDominio correcto");
			setTieneFoco(getSelectDominio().getClientId());
			getButtonObtenerUsuario().setDisabled(Boolean.FALSE);
		}
	}

	public void validarCuentaDominio() {
		if (validCampo(getUsuarioOperativo().getRefPersonalOperativo())) {
			agregarMensajeError(new AdministracionUsuarioException(getArray(MensajesErrorConstants.ME_001),
					getArray("Cuenta dominio")));
			getUsuarioOperativo().setRefPersonalOperativo(BaseConstants.CADENA_VACIA);
			setTieneFoco(getTextCuentaDominio().getClientId());
			getButtonObtenerUsuario().setDisabled(Boolean.TRUE);
		} else {
			logger.info("validarCuentaDominio correcto");
			setTieneFoco(getButtonObtenerUsuario().getClientId());
			getButtonObtenerUsuario().setDisabled(Boolean.FALSE);
		}
	}

	public boolean validarBusquedaCuentaDominio() {
		if (validCampo(getUsuarioOperativo().getRefPersonalOperativo())) {
			getUsuarioOperativo().setRefPersonalOperativo(BaseConstants.CADENA_VACIA);
			setTieneFoco(getTextCuentaDominio().getClientId());
			getButtonObtenerUsuario().setDisabled(Boolean.TRUE);
			return false;
		} else if (validarSelectMenuGuardar(getSelectDominio(), "Dominio")) {
			Pattern pattern = Pattern.compile(BaseConstants.PATTERN_CUENTA_DOMINIO);
			Matcher matcher = pattern.matcher(getUsuarioOperativo().getRefPersonalOperativo());
			if (!matcher.matches()) {
				agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_171));
				getUsuarioOperativo().setRefPersonalOperativo(BaseConstants.CADENA_VACIA);
				setTieneFoco(getTextCuentaDominio().getClientId());
			} else {
				return true;
			}
		} else {
			getButtonObtenerUsuario().setDisabled(Boolean.TRUE);
		}
		return false;
	}

	public void cancelarAsignacionModal() {
		setCatalogoClavePresupuestal(new ArrayList<CatalogoObject>());
		setCatalogoClavePresupuestalString(new ArrayList<String>());
		getAutoAsigPresupuestal().setValue(BaseConstants.CADENA_VACIA);
		getTextAsigCveDelegacion().setValue(BaseConstants.CADENA_VACIA);
		getTextAsigDesDelegacion().setValue(BaseConstants.CADENA_VACIA);
		getSelectAsigEstatus().resetValue();
		restaurarAsignacion();
		getButtonGuardarAsignacion().setDisabled(Boolean.TRUE);
		getButtonActualizarAsignacion().setDisabled(Boolean.TRUE);
		setTieneFoco(getTextAsigCveDelegacion().getClientId());
	}

	private void restaurarAsignacion() {
		setAsignacionModal(new AsignacionUnidad());
		getAsignacionModal().setUnidadMedica(new UnidadMedica());
	}

	public void salir() throws IOException {
		getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
		Properties properties = new Properties();
		properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
		String url = properties.getProperty(BaseConstants.CVE_URL_CE);
		FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
	}

	public void salirModalRol() throws IOException {
		RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_AGREGAR_ROL);
	}

	public void salirModalAsignacion() throws IOException {
		RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_ASIGNACION);
	}

	public void salirModalAltaUsuario() throws IOException {
		RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_AGREGAR_USUARIO);
	}
}
