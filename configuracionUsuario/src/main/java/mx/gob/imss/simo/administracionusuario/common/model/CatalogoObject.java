package mx.gob.imss.simo.administracionusuario.common.model;


import lombok.Data;

@Data
public class CatalogoObject {

	private String clave;
    private String descripcion;
    
    public CatalogoObject (){
    }
    
    public CatalogoObject (String clave, String descripcion){
    	this.clave = clave;
    	this.descripcion = descripcion;
    }
}
