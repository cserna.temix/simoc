package mx.gob.imss.simo.administracionusuario.common.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class Usuario {

    private String refPersonalOperativo;
    private String cveMatricula;
    private String refNombreCompleto;
    
    private String refNombre;
    private String refApellidoPaterno;
    private String refApellidoMaterno;
    private String refEmail;
    private String refNumTelefono;
    private String refNumExtension;
    private Integer indActivo;
    private Integer indBloqueado;
    private Integer consecutivo;
    private String cvePresupuestal;
    private String descPresupuestal;
    private String cveDelegacion;
    private String descDelegacion;
    private Date fecBaja;
    private Date fecAlta;
    private List<AsignacionUnidad> asignacionUnidad;
    private Integer caIntentos;
    
    //Exclusivos de Directorio Activo
    private String carpeta;
    private String usuarioDominio;
	private String oficinaFisica;
    private String departamento;
    private String direccion;
    private List<String> gruposSeguridad;
    
    
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (asignacionUnidad == null) {
			if (other.asignacionUnidad != null)
				return false;
		} else if (!asignacionUnidad.equals(other.asignacionUnidad))
			return false;
		if (carpeta == null) {
			if (other.carpeta != null)
				return false;
		} else if (!carpeta.equals(other.carpeta))
			return false;
		if (consecutivo == null) {
			if (other.consecutivo != null)
				return false;
		} else if (!consecutivo.equals(other.consecutivo))
			return false;
		if (cveDelegacion == null) {
			if (other.cveDelegacion != null)
				return false;
		} else if (!cveDelegacion.equals(other.cveDelegacion))
			return false;
		if (cveMatricula == null) {
			if (other.cveMatricula != null)
				return false;
		} else if (!cveMatricula.equals(other.cveMatricula))
			return false;
		if (cvePresupuestal == null) {
			if (other.cvePresupuestal != null)
				return false;
		} else if (!cvePresupuestal.equals(other.cvePresupuestal))
			return false;
		if (departamento == null) {
			if (other.departamento != null)
				return false;
		} else if (!departamento.equals(other.departamento))
			return false;
		if (descDelegacion == null) {
			if (other.descDelegacion != null)
				return false;
		} else if (!descDelegacion.equals(other.descDelegacion))
			return false;
		if (descPresupuestal == null) {
			if (other.descPresupuestal != null)
				return false;
		} else if (!descPresupuestal.equals(other.descPresupuestal))
			return false;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (fecBaja == null) {
			if (other.fecBaja != null)
				return false;
		} else if (!fecBaja.equals(other.fecBaja))
			return false;
		if (gruposSeguridad == null) {
			if (other.gruposSeguridad != null)
				return false;
		} else if (!gruposSeguridad.equals(other.gruposSeguridad))
			return false;
		if (indActivo == null) {
			if (other.indActivo != null)
				return false;
		} else if (!indActivo.equals(other.indActivo))
			return false;
		if (indBloqueado == null) {
			if (other.indBloqueado != null)
				return false;
		} else if (!indBloqueado.equals(other.indBloqueado))
			return false;
		if (oficinaFisica == null) {
			if (other.oficinaFisica != null)
				return false;
		} else if (!oficinaFisica.equals(other.oficinaFisica))
			return false;
		if (refApellidoMaterno == null) {
			if (other.refApellidoMaterno != null)
				return false;
		} else if (!refApellidoMaterno.equals(other.refApellidoMaterno))
			return false;
		if (refApellidoPaterno == null) {
			if (other.refApellidoPaterno != null)
				return false;
		} else if (!refApellidoPaterno.equals(other.refApellidoPaterno))
			return false;
		if (refEmail == null) {
			if (other.refEmail != null)
				return false;
		} else if (!refEmail.equals(other.refEmail))
			return false;
		if (refNombre == null) {
			if (other.refNombre != null)
				return false;
		} else if (!refNombre.equals(other.refNombre))
			return false;
		if (refNombreCompleto == null) {
			if (other.refNombreCompleto != null)
				return false;
		} else if (!refNombreCompleto.equals(other.refNombreCompleto))
			return false;
		if (refNumExtension == null) {
			if (other.refNumExtension != null)
				return false;
		} else if (!refNumExtension.equals(other.refNumExtension))
			return false;
		if (refNumTelefono == null) {
			if (other.refNumTelefono != null)
				return false;
		} else if (!refNumTelefono.equals(other.refNumTelefono))
			return false;
		if (refPersonalOperativo == null) {
			if (other.refPersonalOperativo != null)
				return false;
		} else if (!refPersonalOperativo.equals(other.refPersonalOperativo))
			return false;
		if (usuarioDominio == null) {
			if (other.usuarioDominio != null)
				return false;
		} else if (!usuarioDominio.equals(other.usuarioDominio))
			return false;
		return true;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((asignacionUnidad == null) ? 0 : asignacionUnidad.hashCode());
		result = prime * result + ((carpeta == null) ? 0 : carpeta.hashCode());
		result = prime * result + ((consecutivo == null) ? 0 : consecutivo.hashCode());
		result = prime * result + ((cveDelegacion == null) ? 0 : cveDelegacion.hashCode());
		result = prime * result + ((cveMatricula == null) ? 0 : cveMatricula.hashCode());
		result = prime * result + ((cvePresupuestal == null) ? 0 : cvePresupuestal.hashCode());
		result = prime * result + ((departamento == null) ? 0 : departamento.hashCode());
		result = prime * result + ((descDelegacion == null) ? 0 : descDelegacion.hashCode());
		result = prime * result + ((descPresupuestal == null) ? 0 : descPresupuestal.hashCode());
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((fecBaja == null) ? 0 : fecBaja.hashCode());
		result = prime * result + ((gruposSeguridad == null) ? 0 : gruposSeguridad.hashCode());
		result = prime * result + ((indActivo == null) ? 0 : indActivo.hashCode());
		result = prime * result + ((indBloqueado == null) ? 0 : indBloqueado.hashCode());
		result = prime * result + ((oficinaFisica == null) ? 0 : oficinaFisica.hashCode());
		result = prime * result + ((refApellidoMaterno == null) ? 0 : refApellidoMaterno.hashCode());
		result = prime * result + ((refApellidoPaterno == null) ? 0 : refApellidoPaterno.hashCode());
		result = prime * result + ((refEmail == null) ? 0 : refEmail.hashCode());
		result = prime * result + ((refNombre == null) ? 0 : refNombre.hashCode());
		result = prime * result + ((refNombreCompleto == null) ? 0 : refNombreCompleto.hashCode());
		result = prime * result + ((refNumExtension == null) ? 0 : refNumExtension.hashCode());
		result = prime * result + ((refNumTelefono == null) ? 0 : refNumTelefono.hashCode());
		result = prime * result + ((refPersonalOperativo == null) ? 0 : refPersonalOperativo.hashCode());
		result = prime * result + ((usuarioDominio == null) ? 0 : usuarioDominio.hashCode());
		return result;
	}
    

}
