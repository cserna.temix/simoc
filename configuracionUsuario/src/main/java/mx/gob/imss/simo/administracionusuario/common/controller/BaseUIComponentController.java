package mx.gob.imss.simo.administracionusuario.common.controller;

import javax.annotation.PostConstruct;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import lombok.Data;

@Data
public class BaseUIComponentController {

    private static final long serialVersionUID = 2919809102671455638L;
    
    private InputMask maskFecha;
    
    private InputText textCuentaDominio;
    private InputText textRolTitular;
    private InputText textMatricula;
    private InputText textFechabaja;
    private InputText textUserConsecutivo;
    private SelectOneMenu selectRol;
    private SelectOneMenu selectDominio;
    
    
    //modalaltausuario
    private InputText textUserMatricula;
    private InputText textUserCveDelegacion;
    private InputText textUserDesDelegacion;
    private AutoComplete autoUserDelegacion;
    private InputText textUserCvePresupuestal;
    private InputText textUserDesPresupuestal;
    private AutoComplete autoUserPresupuestal;
    private InputText textUserFechaAlta;
    private InputText textUserApellidoMaterno;
    private InputText textUserApellidoPaterno;
    private InputText textUserNombre;
    private InputText textUserCorreoElectronico;
    private SelectOneMenu selectUserRol;
    
    
    //modalasignacion
    private InputText textAsigCveDelegacion;
    private InputText textAsigDesDelegacion;
    private AutoComplete autoAsigDelegacion;
    private InputText textAsigCvePresupuestal;
    private InputText textAsigDesPresupuestal;
    private AutoComplete autoAsigPresupuestal;
    private SelectOneMenu selectAsigEstatus;
    private SelectOneMenu selectAsigTipoAsignacion;
    private InputText textAsigApellidoMaterno;
    private InputText textAsigApellidoPaterno;
    private InputText textAsigNombre;
    private InputText textAsigFechaBaja;
    
    private OutputLabel labelNombre;
    
    
    //Modal rol
    
    @PostConstruct
    public void init() {

        autoAsigDelegacion = new AutoComplete();
        maskFecha = new InputMask();

        textUserMatricula = new InputText();
        textUserApellidoMaterno = new InputText();
        textUserApellidoPaterno = new InputText();
        textUserNombre = new InputText();
        textUserConsecutivo = new InputText();
        autoUserDelegacion = new AutoComplete();
        textUserCveDelegacion = new InputText();
        textUserCvePresupuestal = new InputText();
        textUserDesDelegacion = new InputText();
        textUserDesPresupuestal = new InputText();
        textUserCorreoElectronico = new InputText();
        textUserFechaAlta = new InputText();
        selectUserRol = new SelectOneMenu();

        textCuentaDominio = new InputText();
        textFechabaja = new InputText();
        
        
        textAsigCveDelegacion = new InputText();
        textAsigDesDelegacion = new InputText();
        textAsigCvePresupuestal = new InputText();
        textAsigDesPresupuestal = new InputText();
        
        textAsigApellidoMaterno = new InputText();
        textAsigApellidoPaterno = new InputText();
        textAsigNombre = new InputText();
        
        textAsigFechaBaja = new InputText();
        selectAsigEstatus = new SelectOneMenu();
        selectAsigTipoAsignacion = new SelectOneMenu();
         autoAsigDelegacion = new AutoComplete();
        autoAsigPresupuestal = new AutoComplete();
        selectRol = new SelectOneMenu();
        textAsigDesDelegacion.setDisabled(Boolean.TRUE);
		textAsigFechaBaja.setDisabled(Boolean.TRUE);
		selectDominio = new SelectOneMenu();
    }

}
