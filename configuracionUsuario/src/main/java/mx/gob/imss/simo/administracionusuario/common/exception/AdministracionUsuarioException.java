package mx.gob.imss.simo.administracionusuario.common.exception;


public class AdministracionUsuarioException extends BaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AdministracionUsuarioException(String... claves) {
        super(claves);
    }

    public AdministracionUsuarioException(String[] claves, Throwable throwable) {
        super(claves, throwable);
    }

    public AdministracionUsuarioException(String[] claves, String[] args) {
        super(claves, args);
    }

    public AdministracionUsuarioException(String[] claves, String[] args, Throwable throwable) {
        super(claves, args, throwable);
    }
}
