package mx.gob.imss.simo.administracionusuario.common.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import mx.gob.imss.simo.administracionusuario.common.constant.SQLConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.SecuenciaEnum;


public class BaseJDBCRepository {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected JdbcTemplate jdbcTemplate;
    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    protected BaseJDBCRepository() {
        super();
    }

    @Autowired
    protected void setDataSource(final DataSource dataSource) {

        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    protected long obtenerSiguienteValorSecuencia(SecuenciaEnum secuenciasEnum) {

        return jdbcTemplate.queryForObject(
                String.format(SQLConstants.OBTENER_SIGUIENTE_VALOR_SECUENCIA, secuenciasEnum.getNombreSecuencia()),
                new RowMapper<Long>() {

                    @Override
                    public Long mapRow(final ResultSet resulSet, final int rowNumber) throws SQLException {

                        return resulSet.getLong(1);
                    }

                });

    }

    protected String prepararConsultaClausulaIN(String consulta, Integer numeroParametros) {

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("IN (");
        for (int i = 0; i < numeroParametros; i++) {
            stringBuffer.append("?,");
        }
        stringBuffer.append(")");
        String clausula = stringBuffer.deleteCharAt(stringBuffer.length() - 2).toString();
        String nuevaConsulta = consulta.replace("IN ()", clausula);
        return nuevaConsulta;
    }

}
