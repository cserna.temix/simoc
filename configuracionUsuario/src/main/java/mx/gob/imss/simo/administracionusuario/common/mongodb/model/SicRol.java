package mx.gob.imss.simo.administracionusuario.common.mongodb.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("SIC_ROL")
public class SicRol {

	@Id
	private ObjectId id;
	private String cveRol;
	private String desRol;
	private Date fecBaja;

	public String getCveRol() {
		return cveRol;
	}

	public void setCveRol(String cveRol) {
		this.cveRol = cveRol;
	}

	public String getDesRol() {
		return desRol;
	}

	public void setDesRol(String desRol) {
		this.desRol = desRol;
	}

	public Date getFecBaja() {
		return fecBaja;
	}

	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	@Override
	public String toString() {
		return "SicRol [cveRol=" + cveRol + ", desRol=" + desRol+ ", fecBaja=" + fecBaja + "]";
	}
}
