package mx.gob.imss.simo.administracionusuario.administracion.repository;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.Parametro;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.model.PersonalOperativo;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;

public interface UsuarioRepository {

   List<AsignacionUnidad> obtenerAsignaciones(String usuario);
   
   List<Usuario> buscarUsuarioSimoc(String cveDominio);
   
   void altaUsuarioOperativo (Usuario personalOperativo);
		
	public void asignacionRol(String cveCuentaDominio,int cveRol);
	
	public void agregarAsignacion(String cvePresupuestal,int indTitular, int indEstatus,int cveRol,
			String cveCuentaDominio,Date fecAlta,Date fecBaja);
	
	boolean actualizarAsignacion(String cvePresupuestal,int indTitular,int indEstatus,
			Date fechaBaja,Date fecAlta,String cveDominio,String cvePresupuestalEditar);
	
	List<Rol> obtenerRol(String cveCuentaDominio);
	
	Parametro obtenerParamtro(String cveParametro);
	
	Rol obtenerRolById(String idRol);
	
	AsignacionUnidad buscarAsignacionUnidad(String cveDominio,String cvePresupuestal);
	
	boolean actualizarUsuarioOperativo(Usuario personalOperativo);
	
	boolean actualizarEstatusAsignacion (String cveCuentaDominio,String cvePresupuestal, int estatus);
		
	boolean actualizarEstatusTodasAsigUsuario (String cveCuentaDominio, int estatus);
	
	boolean eliminarAsignacion(String cveCuentaDominio,String cvePresupuestal);
	
	boolean actualizarEstatusUsuarioOperativo(String personalOperativo, int estatus,Date fechaBaja);
}
