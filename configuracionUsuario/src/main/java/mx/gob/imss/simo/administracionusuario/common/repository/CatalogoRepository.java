package mx.gob.imss.simo.administracionusuario.common.repository;

import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;

public interface CatalogoRepository {
	
	List<CatalogoObject> obtenerCatalogoRol();

    List<CatalogoObject> obtenerCatalogoTipoAsignacion();

    List<CatalogoObject> obtenerCatalogoDelegacion();
    
    List<CatalogoObject> obtenerCatalogoUnidadesMedicas(String claveDelegacion,String sqlCveCatalogo,String sqlDescCatlogo);
    
    List<CatalogoObject> obtenerCatalogoGeneric(String  table,String sqlCveCatalogo,String sqlDescCatlogo);
}
