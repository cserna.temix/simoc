package mx.gob.imss.simo.administracionusuario.common.exception;

public class BaseException extends Exception {

    private static final long serialVersionUID = -975672174980082608L;

    private final String[] args;
    private final String[] claves;
    private final Throwable throwable;

    protected BaseException() {
        super();
        this.args = null;
        this.claves = null;
        this.throwable = null;
    }

    public BaseException(String... claves) {
        this.args = null;
        this.throwable = null;
        this.claves = claves;
    }

    public BaseException(String[] claves, Throwable throwable) {
        this.args = null;
        this.claves = claves;
        this.throwable = throwable;
    }

    public BaseException(String[] claves, String[] args) {
        this.throwable = null;
        this.claves = claves;
        this.args = args;
    }

    public BaseException(String[] claves, String[] args, Throwable throwable) {
        this.claves = claves;
        this.args = args;
        this.throwable = throwable;
    }

    public String[] getArgs() {

        return args;
    }

    public String[] getClaves() {

        return claves.clone();
    }

    public Throwable getThrowable() {

        return throwable;
    }

}
