package mx.gob.imss.simo.administracionusuario.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.administracionusuario.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.administracionusuario.common.model.Parametro;


public class ParametroRowMapperHelper extends BaseHelper implements RowMapper<Parametro> {

	@Override
	public Parametro mapRow(ResultSet rs, int rowNum) throws SQLException {
		 Parametro parametro = new Parametro();
	        parametro.setClaveParametro(rs.getString(SQLColumnasConstants.CVE_PARAMETRO));
	        parametro.setDescripcionParametro(rs.getString(SQLColumnasConstants.DES_PARAMETRO));
	        parametro.setReferenciaParametro(rs.getString(SQLColumnasConstants.REF_PARAMETRO));

	        return parametro;
	}
}
