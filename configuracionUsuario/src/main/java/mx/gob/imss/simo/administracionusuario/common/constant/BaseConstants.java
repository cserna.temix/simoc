package mx.gob.imss.simo.administracionusuario.common.constant;

public class BaseConstants {

	
	 public static final String BUNDLE_MESSAGES = "bundle.messages";
	    public static final String BUNDLE_LOCALE = "bundle.locale";
	    public static final String IDIOMA_LOCALE = "idioma_locale";
	    public static final String PAIS_LOCALE = "pais_locale";

	    public static final String CADENA_VACIA = "";
	    public static final String CAMA_VACIA = "____";
	    public static final String DIAGONAL_INVERSA = "/";
	    public static final String ESPACIO = " ";
	    public static final String FECHA_VACIA = "__/__/____";
	    public static final String FORMATO_FECHA = "dd/MM/yyyy";
	    public static final String FORMATO_FECHA_ACCEDER = "yyyy/MM/dd";
	    public static final String FORMATO_FECHA_HORA = "dd/MM/yyyy HH:mm";
	    public static final String FORMATO_HORA = "HH:mm";
	    public static final String TIME_ZONE = "CDT";
	    public static final String HORA_VACIA = "__:__";
	    public static final String SIN_REGISTRO = "sin.registro";
	    public static final String NUMERO_CERO_STRING = "0";
	    public static final String NUMERO_DOCE_STRING = "12";
	    public static final String NUMERO_UNO_STRING = "01";
	    public static final String PATTERN_AGREGADO_SEXO = "^[FM]{1}$";
//	    public static final String PATTERN_CUENTA_DOMINIO = "[A-Za-z]+\\.([A-Za-z]+)*";
	    public static final String PATTERN_CUENTA_DOMINIO = "[A-Za-z]+(\\.[A-Za-z]{1,})$";
	    public static final String PATTERN_MARICULA ="^([0-9]){6,10}$";
	    public static final String SALA_VACIA = "__";
	    public static final String SELECCIONE = "--";
	    public static final String PREFIJO_CAMA_RN = "B";
	    public static final String PREFIJO_NOMBRE_RN = "RN";
	    public static final String DOMINIO_CORREO_IMSS = "@imss.gob.mx";

	    public static final int HORA_MAXIMA = 23;
	    public static final int FIN_AGREGADO_ANIO = 6;
	    public static final int FIN_AGREGADO_CALIDAD = 1;
	    public static final int FIN_AGREGADO_SEXO = 2;
	    public static final int INICIO_AGREGADO_ANIO = 2;
	    public static final int INICIO_AGREGADO_CALIDAD = 0;
	    public static final int INICIO_AGREGADO_REGIMEN = 6;
	    public static final int INICIO_AGREGADO_SEXO = 1;
	    public static final int LONGITUD_AGREGADO_MEDICO = 8;
	    public static final int LONGITUD_DELEGACION = 2;
	    public static final int LONGITUD_PLANIFICACION = 2;
	    public static final int LONGITUD_ESPECIALIDAD = 4;
	    public static final int LONGITUD_MINIMA_MATRICULA = 6;
	    public static final int LONGITUD_CVEPRESUPUESTA = 12;
	    public static final int LONGITUD_NSS = 10;
	    public static final int MINUTO_MAXIMO = 59;
	    public static final int SEMANAS_ANIO = 52;
	    public static final int DIAS_ANIO = 365;
	    public static final int TIPO_CONTRATO = 2;
	    public static final int IND_CERO = 0;
	    public static final int IND_UNO = 1;

	    public static final boolean AGREGAR_PACIENTE = true;
	    public static final boolean QUITAR_PACIENTE = false;
	    public static final boolean CAMA_OCUPADA = true;
	    public static final boolean CAMA_DESOCUPADA = false;

	    /*
	     * Constantes para validacion de especialidades
	     */
	    public static final String CLAVE_CUNERO_FISIOLOGICO = "60";
	    public static final String CLAVE_CUNERO_PATOLOGICO = "61";
	    public static final String CLAVE_UCI = "48";
	    public static final String CLAVE_UCI_NEONATOS = "49";
	    public static final String CLAVE_URGENCIAS = "50";
	    public static final String ESPECIALIDAD = "Especialidad";
	    public static final String ESPECIALIDAD_ALOJAMIENTO_CONJUNTO = "6000";
	    public static final String ESPECIALIDAD_CUNERO_PATOLOGICO = "6100";
	    public static final String ESPECIALIDAD_UCI = "4800";
	    public static final String ESPECIALIDAD_UCI_NEONATOS = "4900";
	    public static final String ESPECIALIDAD_URGENCIAS = "5000";
	    public static final String ESPECIALIDAD_CIRUGIA_AMBULATORIA = "0100";
	    public static final String ESPECIALIDAD_ALOJAMIENTO_CONJUNTO_CADENA = "6000 Alojamiento Conjunto";
	    public static final String CLAVE_URGENCIAS_TOCOCIRUGICA = "A6";
	    public static final String ESPECIALIDAD_PEDIATRIA = "3200";

	    public static final int ESPECIALIDAD_AUTORIZADA = 1;

	    /*
	     * Subtitulos
	     */
	    public static final String SUBTITULO_INGRESO_HOSPITALIZACION = "subtitulo.ingreso.hospitalizacion";
	    public static final String SUBTITULO_MOVIMIENTOS_INTRAHOSPITALARIOS = "subtitulo.movimientos.intrahospitalarios";
	    public static final String SUBTITULO_TOCOCIRUGIA = "subtitulo.tococirugia";

	    /*
	     * Id de campos para asignar foco
	     */
	    public static final String FOCO_AGREGADO_MEDICO = "idAgregadoMedico";
	    public static final String FOCO_AGREGADO_ANIO = "idAgregadoAnio";
	    public static final String FOCO_AGREGADO_CALIDAD = "idAgregadoCalidad";
	    public static final String FOCO_AGREGADO_REGIMEN = "idAgregadoRegimen";
	    public static final String FOCO_AGREGADO_SEXO = "idAgregadoSexo";
	    public static final String FOCO_APELLIDO_MATERNO = "idApellidoMaterno";
	    public static final String FOCO_APELLIDO_PATERNO = "idApellidoPaterno";
	    public static final String FOCO_ATENCION = "idAtencion";
	    public static final String FOCO_CAMA = "idCama";
	    public static final String FOCO_DELEGACION = "idDelegacion";
	    public static final String FOCO_EDAD = "idEdad";
	    public static final String FOCO_ESPECIALIDAD = "idEspecialidad";
	    public static final String FOCO_ESPECIALIDAD_ACTUAL = "idEspecialidadActual";
	    public static final String FOCO_ESPECIALIDAD_HOSPITALIZACION = "idEspecialidadHosp";
	    public static final String FOCO_FECHA_INGRESO = "idFechaIngreso";
	    public static final String FOCO_GUARDAR = "idGuardar";
	    public static final String FOCO_HORA_INGRESO = "idHoraIngreso";
	    public static final String FOCO_MATRICULA = "idMatricula";
	    public static final String FOCO_NOMBRE = "idNombre";
	    public static final String FOCO_NSS = "idNss";
	    public static final String FOCO_NSS_ORIGINAL = "idNssOriginal";
	    public static final String FOCO_NUMERO = "idNumero";
	    public static final String FOCO_SALA = "idSala";
	    public static final String FOCO_TIPO_INGRESO = "idTipoIngreso";
	    public static final String FOCO_TIPO_PARTO = "idTipoParto";
	    public static final String FOCO_TIPO_PROGRAMA = "idTipoPrograma";
	    public static final String FOCO_UBICACION = "idUbicacion";

	    public static final String FOCO_MODIFICACION_AGREGADO_ANIO = "idAgregadoAnioNuevo";
	    public static final String FOCO_MODIFICACION_AGREGADO_CALIDAD = "idAgregadoCalidadNuevo";
	    public static final String FOCO_MODIFICACION_AGREGADO_REGIMEN = "idAgregadoRegimenNuevo";
	    public static final String FOCO_MODIFICACION_AGREGADO_SEXO = "idAgregadoSexoNuevo";
	    public static final String FOCO_MODIFICACION_APELLIDO_MATERNO = "idApellidoMaternoNuevo";
	    public static final String FOCO_MODIFICACION_APELLIDO_PATERNO = "idApellidoPaternoNuevo";
	    public static final String FOCO_MODIFICACION_ATENCION = "idAtencionNuevo";
	    public static final String FOCO_MODIFICACION_CAMA = "idCamaNuevo";
	    public static final String FOCO_MODIFICACION_DELEGACION = "idDelegacionNuevo";
	    public static final String FOCO_MODIFICACION_EDAD = "idEdadNuevo";
	    public static final String FOCO_MODIFICACION_ESPECIALIDAD = "idEspecialidadNuevo";
	    public static final String FOCO_MODIFICACION_ESPECIALIDAD_ACTUAL = "idEspecialidadActualNuevo";
	    public static final String FOCO_MODIFICACION_ESPECIALIDAD_HOSPITALIZACION = "idEspecialidadHospNuevo";
	    public static final String FOCO_MODIFICACION_FECHA_INGRESO = "idFechaIngresoNuevo";
	    public static final String FOCO_MODIFICACION_GUARDAR = "idGuardarNuevo";
	    public static final String FOCO_MODIFICACION_HORA_INGRESO = "idHoraIngresoNuevo";
	    public static final String FOCO_MODIFICACION_MATRICULA = "idMatriculaNuevo";
	    public static final String FOCO_MODIFICACION_NOMBRE = "idNombreNuevo";
	    public static final String FOCO_MODIFICACION_NSS = "idNssNuevo";
	    public static final String FOCO_MODIFICACION_NUMERO = "idNumeroNuevo";
	    public static final String FOCO_MODIFICACION_SALA = "idSalaNuevo";
	    public static final String FOCO_MODIFICACION_TIPO_INGRESO = "idTipoIngresoNuevo";
	    public static final String FOCO_MODIFICACION_TIPO_PARTO = "idTipoPartoNuevo";
	    public static final String FOCO_MODIFICACION_TIPO_PROGRAMA = "idTipoProgramaNuevo";
	    public static final String FOCO_MODIFICACION_UBICACION = "idUbicacionNuevo";

	    public static final String FOCO_NUMERO_QUIROFANO = "idNumeroQuirofano";
	    public static final String FOCO_PROCEDIMIENTO = "idProcedimiento";
	    public static final String FOCO_HORA_ENTRADA_SALA = "idHoraSala";
	    public static final String FOCO_HORA_INICIO = "idHoraInicio";
	    public static final String FOCO_HORA_TERMINO = "idHoraTermino";
	    public static final String FOCO_HORA_SALIDA_SALA = "idHoraSalidaSala";
	    public static final String FOCO_PROCEDIMIENTO_QX = "idProcedimientoQuirurgico";
	    public static final String FOCO_CIRUJANO = "idCirujano";
	    public static final String FOCO_TIPO_INTERVENCION = "idTipoIntervencion";
	    public static final String FOCO_METODO_PLANIFICACION_FAMILIAR = "idMetodoPlanificacion";
	    public static final String FOCO_TIPO_ANESTESIA = "idTipoAnestesia";
	    public static final String FOCO_ANESTESIOLOGO = "idAnestesiologo";
	    public static final String FOCO_ID_FECHA = "idFecha";
	    public static final String FOCO_ID_SI = "idSi";
	    
	    
	    public static final String FOCO_CUENT_DOMINIO = "idCuentaDominio";
	    public static final String FOCO_DOMINIO = "idDominio";

	    /*
	     * Datos requeridos
	     */
	    public static final String REQUERIDO_UBICACION = "Ubicaci\u00F3n";
	    public static final String REQUERIDO_AGREGADO_MEDICO = "Agregado m\u00E9dico";
	    public static final String REQUERIDO_APELLIDO_MATERNO = "Apellido materno";
	    public static final String REQUERIDO_APELLIDO_PATERNO = "Apellido paterno";
	    public static final String REQUERIDO_CAMA = "Cama";
	    public static final String REQUERIDO_DELEGACION = "Delegaci\u00F3n";
	    public static final String REQUERIDO_EDAD = "Edad";
	    public static final String REQUERIDO_ESPECIALIDAD = "Especialidad";
	    public static final String REQUERIDO_FECHA = "Fecha";
	    public static final String REQUERIDO_FECHA_INICIO = "Fecha de inicio";
	    public static final String REQUERIDO_FECHA_FINAL = "Fecha final";
	    public static final String REQUERIDO_MES_ANIO = "Mes y a�o a visualizar";
	    public static final String REQUERIDO_PROCEDIMIENTO_INICIAL = "Procedimiento inicial";
	    public static final String REQUERIDO_PROCEDIMIENTO_FINAL = "Procedimiento final";
	    public static final String REQUERIDO_HORA_INGRESO = "Hora ingreso";
	    public static final String REQUERIDO_MATRICULA = "Matr\u00EDcula";
	    public static final String REQUERIDO_NOMBRE = "Nombre";
	    public static final String REQUERIDO_NSS = "No. Seguridad Social";
	    public static final String REQUERIDO_NSS_ORIGINAL = "No. Seguridad Social";
	    public static final String REQUERIDO_NUMERO = "No.";
	    public static final String REQUERIDO_NUMERO_SALA = "No. de Sala";
	    public static final String REQUERIDO_TIPO_INGRESO = "Tipo ingreso";
	    public static final String REQUERIDO_TIPO_PROGRAMA = "Tipo programa";
	    public static final String REQUERIDO_ESPECIALIDAD_HOSPITALIZACION = "Especialidad hospitalizaci\u00F3n";
	    public static final String REQUERIDO_PLANIFICACION = "M\u00E9todo de planificaci\u00F3n familiar";
	    public static final String REQUERIDO_PROCEDIMIENTO = "Procedimientos";
	    public static final String REQUERIDO_TIPO_ANESTESIA = "Tipo de anestesia";
	    public static final String REQUERIDO_TIPO_INTERVENCION = "Tipo de intervenci\u00F3n";
	    public static final String REQUERIDO_CANTIDAD = "Cantidad";
	    public static final String REQUERIDO_METODO_PLANIFICACION = "M�todo de Planificaci\u00F3n";
	    public static final String REQUERIDO_PROCEDIMIENTO_QX = "Procedimiento quir\u00FArgico";
	    public static final String PROCEDIMIENTO_QX = "procedimiento";
	    // Servicios Subrogados
	    public static final String REQUERIDO_TIPO_CONTRATO_CONVENIO = "Tipo de Contrato/Convenio";
	    public static final String REQUERIDO_FOLIO_CONTRATO_CONVENIO = "Folio Contrato/Convenio";
	    public static final String REQUERIDO_TIPO_SERVICIO = "Tipo de servicio";
	    public static final String REQUERIDO_MOTIVO_SUBROGACION = "Motivo de subrogaci\u00F3n";
	    public static final String REQUERIDO_SERVICIOS_SUBROGAR = "Servicios a subrogar";
	    public static final String REQUERIDO_CANTIDAD_EVENTOS = "Cantidad/Eventos";
	    public static final String REQUERIDO_COSTO_UNITARIO = "Costo unitario";
	    public static final String REQUERIDO_FECHA_SERVICIO = "Fecha de servicio";
	    public static final String REQUERIDO_CAUSA_DIRECTA_DEFUNCION = "Causa directa de la defunci\u00F3n";
	    // Interconsultas
	    public static final String REQUERIDO_DIAGNOSTICO_PRINCIPAL_INTERCON = "Diagn\u00F3stico principal";
	    public static final String REQUERIDO_DIAGNOSTICO_ADICIONAL_INTERCON = "Diagn\u00F3stico adicional";
	    public static final String REQUERIDO_DIAGNOSTICO_COMPLEMENTARIO_INTERCON = "Diagn\u00F3stico complementario";
	    public static final String REQUERIDO_TURNO = "Turno";
	    public static final String REQUERIDO_DURACION_INTERCON = "Duracion de interconsulta";
	    public static final String REQUERIDO_PRIMERA_VEZ = "Primera vez";
	    public static final String REQUERIDO_ALTA_INTERCONSULTA = "Alta";
	    public static final String CIE10 = "CIE-10";
	    public static final String CIE9 = "CIE-9";
	    public static final String PROCEDIMIENTO = "Procedimiento";
	    public static final String REQUERIDO_PROCEDIMIENTO_PRINCIPAL_INTERCON = "Procedimiento principal";
	    
	    //AgregarAsignacion
	    public static final String REQUERIDO_CLAVE_PRESUPUESTAL = "Clave presupuestal";

	    /**
	     * Modal
	     */
	    public static final String ABRIR_MODAL_CUENTA_DOMINIO = "PF('dlgUsuarios').show();";
	    public static final String CERRAR_MODAL_CUENTA_DOMINIO = "PF('dlgUsuarios').hide();";
	    
	    public static final String ABRIR_MODAL_AGREGAR_ROL = "PF('dlgRol').show();";
	    public static final String CERRAR_MODAL_AGREGAR_ROL = "PF('dlgRol').hide();";
	    
	    public static final String ABRIR_MODAL_AGREGAR_USUARIO = "PF('dlgAltaUsuarios').show();";
	    public static final String CERRAR_MODAL_AGREGAR_USUARIO= "PF('dlgAltaUsuarios').hide();";
	    
	    public static final String ABRIR_MODAL_ASIGNACION = "PF('dlgAsignacion').show();";
	    public static final String CERRAR_MODAL_ASIGNACION = "PF('dlgAsignacion').hide();";
	    
	    public static final String ABRIR_MODAL_CONFIRMACION = "PF('userSaveConfirm').show();";
	    public static final String CERRAR_MODAL_CONFIRMACION = "PF('userSaveConfirm').hide();";
	    
	    
//	    public static final String ABRIR_MODAL_PACIENTES_NUEVOS = "PF('dlgPacientesNuevos').show();";
//	    public static final String CERRAR_MODAL_PACIENTES_NUEVOS = "PF('dlgPacientesNuevos').hide();";
//
//	    public static final String ABRIR_MODAL_CAMA_PACIENTES = "PF('dlgCamaPacientes').show();";
//	    public static final String CERRAR_MODAL_CAMA_PACIENTES = "PF('dlgCamaPacientes').hide();";
//	    public static final String ABRIR_MODAL_CANTIDAD = "PF('idDlgCantidad').show();";
//	    public static final String CERRAR_MODAL_CANTIDAD = "PF('idDlgCantidad').hide();";
//	    public static final String ABRIR_MODAL_INTERVENCION = "PF('idDlgFechaIntervencion').show();";
//	    public static final String CERRAR_MODAL_INTERVENCION = "PF('idDlgFechaIntervencion').hide();";
//	    public static final String ABRIR_MODAL_RECIENNACIDOS = "PF('idDlgreciennacido').show();";
//	    public static final String CERRAR_MODAL_RECIENNACIDOS = "PF('idDlgreciennacido').hide();";
//	    public static final String ABRIR_MODAL_PROCEDIMIENTOS = "PF('dlgProcedimientos').show();";
//	    public static final String CERRAR_MODAL_PROCEDIMIENTOS = "PF('dlgProcedimientos').hide();";

	    public static final String MOSTRAR_MODAL_CIERRE_MES = "PF('dlgCierreMes').show();";
	    public static final String CERRAR_MODAL_CIERRE_MES = "PF('dlgCierreMes').hide();";

	    public static final String BANDERA_MODAL_PACIENTE = "BANDERAMODALPACIENTE";
	    public static final String BANDERA_MODAL_CAMA = "BANDERAMODALCAMA";
	    public static final String BANDERA_MODAL_CANTIDAD = "BANDERAMODALCANTIDAD";

	    public static final int INDEX_CERO = 0;
	    public static final int INDEX_UNO = 1;
	    public static final Integer INDEX_DOS = 2;

	    public static final int TIEMPO_ESPERA_MILISEGUNDOS = 1000;
	    public static final int TIEMPO_ESPERA = 3;

	    public static final String COMILLA_SIMPLE = "'";

	    public static final int LONGITUD_CLAVE_CIE_9MC = 4;

	    /**
	     * Cadena para colocar el foco en la celda
	     */
	    public static final String INDICE_SIGUIENTE_FOCO = "datosBuscarEditarHospForm:idIntervencionDataTable:%s:idProcedimientoQx";

	    /**
	     * Constante necesaria para la validacion de habilitar el combo
	     * planificacion familiar siempre y cuando sea mayor de diez a�os
	     */
	    public static final int EDAD_MINIMA_PLANIFICACION_FAMILIAR = 10;
	    public static final int EDAD_MINIMA_SEMANAS_PLANIFICACION_FAMILIAR = 542;

	    /**
	     * WS
	     */
	    public static final String SIMO_OSB = "SIMO_OSB";
	    public static final String SI = "Si";
	    public static final String NO = "No";

	    /**
	     * Control tabla intervenciones Qx
	     */
	    public static final int PROCEDIMIENTO_CERO = 0;
	    public static final int PROCEDIMIENTO_UNO = 1;
	    public static final int PROCEDIMIENTO_DOS = 2;
	    public static final int PROCEDIMIENTO_TRES = 3;
	    public static final int PROCEDIMIENTO_CUATRO = 4;
	    public static final int PROCEDIMIENTO_CINCO = 5;
	    public static final int PROCEDIMIENTO_SEIS = 6;
	    public static final int PROCEDIMIENTO_SIETE = 7;
	    public static final int PROCEDIMIENTO_OCHO = 8;
	    public static final int PROCEDIMIENTO_NUEVE = 9;
	    public static final int PROCEDIMIENTO_DIEZ = 10;

	    /**
	     * Control tabla Recien Nacido
	     */
	    public static final int RN_CERO = 0;
	    public static final int RN_UNO = 1;
	    public static final int RN_DOS = 2;
	    public static final int RN_TRES = 3;
	    public static final int RN_CUATRO = 4;
	    public static final int RN_CINCO = 5;
	    public static final int RN_SEIS = 6;
	    public static final int RN_SIETE = 7;
	    public static final int RN_OCHO = 8;
	    public static final int RN_NUEVE = 9;
	    public static final int RN_DIEZ = 10;

	    /**
	     * Campos de tabla editar hospitalizacion
	     */
	    public static final String nombreTexto = "textoCapturable";
	    public static final String ACTIVAR_CAMPOS_EDICION_INTERVENCION = "activarCamposEdicionTablaQx();";
	    public static final String ACTIVAR_BOTON_CANCELAR = "colocarFocoBotonCancelar();";

	    public static final String PAGINA_INICIO = "/pages/inicio.xhtml";
	    public static final String CVE_URL_CE = "url.ce";
	    public static final String PATH = "database.properties";

	    /**
	     * Ingreso Hospitalizacion
	     */
	    public static final String TIPO_INGRESO_PROGRAMADO = "1";
	    public static final String TIPO_INGRESO_DEFAULT = "--";
	    
	    
	    public static final String REQUERIDO_CVE_PRESUPUESTAL = "Clave presupuestal";
	    public static final String REQUERIDO_CVE_DELEGACIONAL = "Clave delegacion";
	    public static final String REQUERIDO_ROL = "Rol";
}
