package mx.gob.imss.simo.administracionusuario.common.model;

import java.util.List;

import lombok.Data;

@Data
public class CuentaDirectorioActivo {
	
	private String usuarioDominio;
	private String oficinaFisica;
    private String carpeta;
    private String nombreCompleto;
    private String telefono;
    private String matricula;
    private String email;
    private String cuentaDominio;
    private String departamento;
    private String direccion;
    private List<String> gruposSeguridad;
    

}
