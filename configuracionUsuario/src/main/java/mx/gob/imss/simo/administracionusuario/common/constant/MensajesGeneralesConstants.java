package mx.gob.imss.simo.administracionusuario.common.constant;

public class MensajesGeneralesConstants {

    public static final String MG_001 = "mg001";
    public static final String MG_002 = "mg002";
    public static final String MG_003 = "mg003";
    public static final String MG_004 = "mg004";
    public static final String MG_005 = "mg005";
    public static final String MG_006 = "mg006";
    public static final String MG_007 = "mg007";
    public static final String MG_008 = "mg008";
    public static final String MG_009 = "mg009";
    public static final String MG_010 = "mg010";
    public static final String MG_011 = "mg011";
    public static final String MG_012 = "mg012";
    public static final String MG_013 = "mg013";
    public static final String MG_014 = "mg014";
    public static final String MG_015 = "mg015";
    public static final String MG_016 = "mg016";
    public static final String MG_017 = "mg017";
    public static final String MG_018 = "mg018";
    public static final String MG_019 = "mg019";
    public static final String MG_020 = "mg020";
    public static final String MG_021 = "mg021";
    public static final String MG_022 = "mg022";
    public static final String MG_023 = "mg023";
    public static final String MG_024 = "mg024";
    public static final String MG_025 = "mg025";
    public static final String MG_026 = "mg026";
    public static final String MG_027 = "mg027";

}