package mx.gob.imss.simo.administracionusuario.common.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.Data;
import mx.gob.imss.simo.administracionusuario.administracion.repository.AdministracionRepository;
import mx.gob.imss.simo.administracionusuario.administracion.service.AdministracionService;
import mx.gob.imss.simo.administracionusuario.administracion.service.UsuarioService;
import mx.gob.imss.simo.administracionusuario.common.constant.BaseConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.DominioEnum;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.EstatusEnum;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.TipoAsignacionEnum;
import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.service.ActiveDirectoryService;
import mx.gob.imss.simo.administracionusuario.common.service.CatalogoService;
import mx.gob.imss.simo.administracionusuario.common.service.SiapService;

@Data
public class CommonController extends BaseUIComponentController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CatalogoObject> catalogoRol;
    private List<CatalogoObject> catalogoEstatus;
    private List<CatalogoObject> catalogoTipoAsignacion;
    private List<CatalogoObject> catalogoDominio;
    
    private List<CatalogoObject> catalogoDelegacion;
    private List<CatalogoObject> catalogoClavePresupuestal;
    
    private List<String> catalogoRolString;
    private List<String> catalogoEstatusString;
    private List<String> catalogoTipoAsignacionString;
    
    private List<String> catalogoDelegacionString;
    private List<String> catalogoClavePresupuestalString;
    
    protected Usuario usuarioOperativo;
    protected List<AsignacionUnidad> lisAsignaciones;
    
    @ManagedProperty("#{administracionServices}")
    private AdministracionService administracionServices;
    
    @ManagedProperty("#{catalogosAdministracionServices}")
    protected transient CatalogoService catalogosAdministracionServices;
    
    @ManagedProperty("#{usuarioService}")
    protected transient UsuarioService usuarioService;
    
    @ManagedProperty("#{activeDirectoryService}")
    protected transient ActiveDirectoryService activeDirectoryService;
    
    @ManagedProperty("#{siapService}")
    protected transient SiapService siapService;
    
    protected final Logger logger = Logger.getLogger(getClass());

    private ResourceBundle bundleUsuario;
    private Locale locale;

    private Integer numeroUsuario;

    protected String tieneFoco;
    
    @Override
    @PostConstruct
    public void init() {

        super.init();
       
        catalogoTipoAsignacion = new ArrayList<>();
        catalogoEstatus = new ArrayList<>();
        catalogoRol = new ArrayList<>();
        catalogoDelegacion = new ArrayList<>();
        catalogoDominio = new ArrayList<>();
        
        catalogoRolString = new ArrayList<>();
	    catalogoEstatusString = new ArrayList<>();
	    catalogoTipoAsignacionString = new ArrayList<String>();
	    catalogoDelegacionString = new ArrayList<>();
	    lisAsignaciones = new ArrayList<>();
		setCatalogoRol(catalogosAdministracionServices.obtenerCatalogoRol());
        setCatalogoRolString(convertirCatalogoString(getCatalogoRol()));
        
        setCatalogoDelegacion(catalogosAdministracionServices.obtenerCatalogoDelegaciones());
        setCatalogoDelegacionString(convertirCatalogoString(getCatalogoDelegacion()));
        logger.info("numero de delegaciones = "+getCatalogoDelegacion().size());
        
        setCatalogoTipoAsignacion(TipoAsignacionEnum.obtenerCatalogo());
        setCatalogoTipoAsignacionString(convertirCatalogoString(getCatalogoTipoAsignacion()));
        
        setCatalogoEstatus(EstatusEnum.obtenerCatalogo());
        setCatalogoEstatusString(convertirCatalogoString(getCatalogoEstatus()));
        
        setCatalogoDominio(DominioEnum.obtenerDominio());
        
        locale = new Locale(
				ResourceBundle.getBundle(BaseConstants.BUNDLE_LOCALE).getString(BaseConstants.IDIOMA_LOCALE),
				ResourceBundle.getBundle(BaseConstants.BUNDLE_LOCALE).getString(BaseConstants.PAIS_LOCALE));
		bundleUsuario = ResourceBundle.getBundle(BaseConstants.BUNDLE_MESSAGES, getLocale());
		tieneFoco = BaseConstants.FOCO_DOMINIO;
		logger.info("tieneFoco " + tieneFoco); 
    }
    
    public Usuario getPersonalOperativo() {
        return usuarioOperativo;
    }

    public void setPersonalOperativo(Usuario personalOperativo) {
        this.usuarioOperativo = personalOperativo;
    }
    
    
    protected void reiniciarModalAsignacion(){
		setCatalogoClavePresupuestal(new ArrayList<CatalogoObject>());
		setCatalogoClavePresupuestalString(new ArrayList<String>());
		getAutoAsigPresupuestal().setValue(BaseConstants.CADENA_VACIA);
		getTextAsigCveDelegacion().setValue(BaseConstants.CADENA_VACIA);
		getTextAsigDesDelegacion().setValue(BaseConstants.CADENA_VACIA);
		getSelectAsigTipoAsignacion().resetValue();
		getSelectAsigEstatus().resetValue();
	}
	
    protected void reiniciarModalUsuario(){
		setCatalogoClavePresupuestal(new ArrayList<CatalogoObject>());
		setCatalogoClavePresupuestalString(new ArrayList<String>());
		getAutoUserPresupuestal().resetValue();
		getTextUserCveDelegacion().resetValue();
		getTextUserDesDelegacion().resetValue();
		getTextUserNombre().resetValue();
		getTextUserApellidoPaterno().resetValue();
		getTextUserApellidoMaterno().resetValue();
		getTextUserFechaAlta().resetValue();
		getTextUserCorreoElectronico().resetValue();
		getSelectUserRol().resetValue();
//		getTextUserMatricula().resetValue();
	}
	
	protected void reiniciarModalRol(){
		getSelectRol().resetValue();
	}
	
	protected boolean validarCatalogoPresupuestal(AutoComplete autocomplete,int keydouwn, List<String> listString,List<CatalogoObject> catalogos, String campoNombre){
		logger.info("validarCvePresupuestal");
		logger.info("validarCvePresupuestal.value: " + autocomplete.getValue());
		if (autocomplete.getValue() != null && !validCampo(autocomplete.getValue().toString())) {
			logger.info("validarCvePresupuestal" + autocomplete.getValue() +" caracteres "+keydouwn );
			CatalogoObject datosCatalogo;
			datosCatalogo = validaAutoComplete(autocomplete, keydouwn, listString, catalogos);
			if (null != datosCatalogo) {
				usuarioOperativo.setCvePresupuestal(datosCatalogo.getClave());
				return true;
			} else {
				agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_021));
				return false;
			}
		} else {
			agregarMensajeError(new AdministracionUsuarioException(getArray(MensajesErrorConstants.ME_001),
                    getArray(campoNombre)));
			return false;
		}
	}
	
	protected boolean validarMatricula(InputText textCve, String campoNombre){
		if (textCve.getValue() != null && !validCampo(textCve.getValue().toString())) {
			 Pattern pattern = Pattern.compile(BaseConstants.PATTERN_MARICULA);
		        Matcher matcher = pattern.matcher(getUsuarioOperativo().getCveMatricula());
		        if (!matcher.matches()) {
		            agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_997));
		            return false;
		        }else{
		        	return true;
		        }
		} else {
			agregarMensajeError(new AdministracionUsuarioException(getArray(MensajesErrorConstants.ME_001),
                    getArray(campoNombre)));
			return false;
		}
	}
	
	protected boolean validacveDelegacion(InputText textCve, InputText textDesc, int keydown, List<String> catalogoString, List<CatalogoObject> catalogo,String campoNombre){
		logger.info("validarDelegacion");
		logger.info("Datos de cveDelegacion "+textCve.getValue().toString());
		if(textCve.getValue() != null && !validCampo(textCve.getValue().toString())){
		if(!isNumeric(textCve.getValue().toString())){
//			agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_103));
//			return false;
		}
		CatalogoObject datosCatalogo;
		datosCatalogo = texAutoComplete(textCve, textDesc, keydown,
				catalogoString, catalogo);
		logger.info("Datos de cveDelegacion "+textCve.getValue().toString());
		if (null != datosCatalogo) {
			usuarioOperativo.setCveDelegacion(datosCatalogo.getClave());
			textDesc.setValue(datosCatalogo.getDescripcion());
			setCatalogoClavePresupuestal(
					catalogosAdministracionServices.obtenerCatalogoUnidadesMedicas(datosCatalogo.getClave()));
			setCatalogoClavePresupuestalString(convertirCatalogoString(getCatalogoClavePresupuestal()));
			return true;
		} else {
			agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_103));
			return false;
		}
		}else{
			agregarMensajeError(new AdministracionUsuarioException(getArray(MensajesErrorConstants.ME_001),
                    getArray(campoNombre)));
						return false;
		}
	}
	
	protected void habilitarModalUsuario(Boolean condicion) {
		getTextUserApellidoMaterno().setDisabled(condicion);
		getTextUserApellidoPaterno().setDisabled(condicion);
		getTextUserNombre().setDisabled(condicion);
		getTextUserCorreoElectronico().setDisabled(condicion);
		getTextUserFechaAlta().setDisabled(condicion);
		getTextUserDesDelegacion().setDisabled(condicion);
		getTextUserDesPresupuestal().setDisabled(condicion);
	}
    
	protected boolean validCampo(String campo) {
		return campo == null || campo.isEmpty();
	}
	
	protected void desactivarCampo(InputText textImput, String campo){
		if(campo ==null || campo.isEmpty()){
			textImput.setDisabled(Boolean.TRUE);
		}
	}
	
	private boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
	
	protected boolean validarEstatus(boolean indEstatus){
		try {
			if(indEstatus){
				throw new AdministracionUsuarioException(MensajesErrorConstants.ME_185);
			}else{
				return true;
			}
		} catch (AdministracionUsuarioException e) {
			agregarMensajeError(e);
			return false;
		}
	}
	
	protected boolean validarDelegacionGuardado(InputText cveDelegacion, List<String> catalogo){
		if(cveDelegacion.getValue() ==null || validCampo(cveDelegacion.getValue().toString())){
			logger.info("error de campo : ");
			agregarMensajeError(new AdministracionUsuarioException(getArray(MensajesErrorConstants.ME_001),
                    getArray(BaseConstants.REQUERIDO_CVE_DELEGACIONAL)));
			setTieneFoco(cveDelegacion.getClientId());
			return false;
		}
		if(!isNumeric(cveDelegacion.getValue().toString())){
//			logger.info("error de isNumeric : ");
//			agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_103));
//			setTieneFoco(cveDelegacion.getClientId());
//			return false;
		}
		boolean enCatalogo= buscarEnCatalogo(cveDelegacion.getValue().toString(),catalogo);
		if (!enCatalogo) {
			logger.info("error de enCatalogo : ");
			agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_039));
			setTieneFoco(cveDelegacion.getClientId());
			return false;
		}
		return true;
	}
	
	protected boolean validarCvePresupuestalGuardado(AutoComplete cvePresupuestal, List<String> catalogo){
		if(cvePresupuestal.getValue()== null || validCampo(cvePresupuestal.getValue().toString())){
			logger.info("error de campo : ");
			agregarMensajeError(new AdministracionUsuarioException(getArray(MensajesErrorConstants.ME_001),
                    getArray(BaseConstants.REQUERIDO_CVE_PRESUPUESTAL)));
			setTieneFoco(cvePresupuestal.getClientId());
			return false;
		}
		boolean enCatalogo=  buscarEnCatalogo(cvePresupuestal.getValue().toString(),catalogo);
		if (!enCatalogo) {
			logger.info("error de enCatalogo : ");
			agregarMensajeError(new AdministracionUsuarioException(MensajesErrorConstants.ME_021));
			setTieneFoco(cvePresupuestal.getClientId());
			return false;
		}
		return true;
	}
	
	protected boolean validarSelectMenuGuardar(SelectOneMenu cveComponente,String campo){
			if(cveComponente.getValue() ==null || validCampo(cveComponente.getValue().toString())){
				logger.info("validarSelectMenuGuardar ");
				agregarMensajeError(new AdministracionUsuarioException(getArray(MensajesErrorConstants.ME_001),
	                    getArray(campo)));
				setTieneFoco(cveComponente.getClientId());
				return false;
			}
			return true;
	}
    
    public String[] getArray(String... strings) {

        return strings;
    }

    public List<String> convertirCatalogoString(List<CatalogoObject> datosCatalogo) {

        List<String> datos = new ArrayList<>();
        for (CatalogoObject dato : datosCatalogo) {
            datos.add(dato.getClave().concat(BaseConstants.ESPACIO.concat(dato.getDescripcion())));
        }
        return datos;
    }

    
    public FacesContext getContext() {

        return FacesContext.getCurrentInstance();
    }
    
    public ResourceBundle getBundle() {
        return bundleUsuario;
    }
    
    public String obtenerMensaje(String clave) {
        return getBundle().getString(clave);
    }
    
    public void agregarMensajeError(AdministracionUsuarioException e) {

        String[] claves = e.getClaves();
        String[] args = e.getArgs();
        if (claves != null) {
            if (args == null) {
                for (String clave : claves) {
                    String mensaje = obtenerMensaje(clave);
                    getContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, BaseConstants.CADENA_VACIA));
                }
            } else {
                String mensaje = String.format(obtenerMensaje(claves[0]), (Object[]) args);
                getContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, BaseConstants.CADENA_VACIA));
            }
        }
    }
    
    public CatalogoObject validaAutoComplete(AutoComplete componente, Integer max, List<String> listaString,
            List<CatalogoObject> listaCatalogo) {

    	CatalogoObject objeto = null;
        String textoIngresado;
        if (componente.getValue() != null) {
            textoIngresado = componente.getValue().toString();
            if (textoIngresado != null && !textoIngresado.isEmpty() && textoIngresado.length() >= max) {
                List<String> resultado = new ArrayList<>();
                String cadena;
                Iterator<String> it = listaString.iterator();
                while (it.hasNext()) {
                    cadena = it.next();
                    if (StringUtils.containsIgnoreCase(cadena, textoIngresado)) {
                        resultado.add(cadena);
                    }
                }
                if (!resultado.isEmpty()) {
                    componente.setValue(resultado.get(0));
                    int index;
                    if (componente.getValue() != null) {
                        index = listaString.indexOf(componente.getValue().toString());
                        objeto = listaCatalogo.get(index);
                    }
                } else {
                    componente.setValue(null);
                }

            }
        }
        return objeto;
    }
    
    public CatalogoObject texAutoComplete(InputText texto, InputText componente,Integer max, List<String> listaString,
            List<CatalogoObject> listaCatalogo) {

    	CatalogoObject objeto = null;
        String textoIngresado;
        if (texto.getValue() != null) {
            textoIngresado = texto.getValue().toString();
            if (textoIngresado != null && !textoIngresado.isEmpty() && textoIngresado.length() >= max) {
                List<String> resultado = new ArrayList<>();
                String cadena;
                Iterator<String> it = listaString.iterator();
                while (it.hasNext()) {
                    cadena = it.next();
                    if (StringUtils.containsIgnoreCase(cadena, textoIngresado)) {
                        resultado.add(cadena);
                    }
                }
                if (!resultado.isEmpty()) {
                    componente.setValue(resultado.get(0));
                    int index;
                    if (componente.getValue() != null) {
                        index = listaString.indexOf(componente.getValue().toString());
                        objeto = listaCatalogo.get(index);
                    }
                } else {
                    componente.setValue(null);
                }

            }
        }
        return objeto;
    }
    
    public boolean buscarEnCatalogo(String texto, List<String> listaString) {
    	logger.info("texto de " + texto);
    	logger.info("listaString  " + listaString.size());
    	boolean catalogo = false;
        String textoIngresado;
        if (texto != null) {
            textoIngresado = texto;
            if (textoIngresado != null && !textoIngresado.isEmpty()) {
                String cadena;
                Iterator<String> it = listaString.iterator();
                while (it.hasNext()) {
                    cadena = it.next();
                    logger.info("texto de " + cadena);
                    if(StringUtils.containsIgnoreCase(cadena, textoIngresado)){
                    	logger.info("correcto " + cadena+" " +textoIngresado);
                    	catalogo = true;
                    	break;
                    }
                }
            }
        }
        return catalogo;
    }
    
    public String getTieneFoco() {

        return tieneFoco;
    }

    public void setTieneFoco(String tieneFoco) {

        this.tieneFoco = tieneFoco;
    }
    
    public List<String> filtrarCatalogo(List<String> datosCatalogoString, String query, Integer longitud) {

        List<String> filtroCatalogo = new ArrayList<String>();
        if (query.length() < longitud) {
            for (String dato : datosCatalogoString) {
                if (dato.startsWith(query)) {
                    filtroCatalogo.add(dato);
                }
            }
        }
        return filtroCatalogo;
    }
    
    public void agregarMensajeInformativo(String[] claves, String[] args) {
        if (args == null) {
            for (String clave : claves) {
                String mensaje = obtenerMensaje(clave);
                getContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, mensaje, BaseConstants.CADENA_VACIA));
            }
        } else {
            String mensaje = String.format(obtenerMensaje(claves[0]), (Object[]) args);
            getContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, mensaje, BaseConstants.CADENA_VACIA));
        }
    }
	
}
