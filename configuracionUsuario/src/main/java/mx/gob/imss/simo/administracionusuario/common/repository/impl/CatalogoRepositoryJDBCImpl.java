package mx.gob.imss.simo.administracionusuario.common.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.administracionusuario.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.administracionusuario.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.administracionusuario.common.helper.CatalogoRowMapperHelper;
import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;
import mx.gob.imss.simo.administracionusuario.common.repository.BaseJDBCRepository;
import mx.gob.imss.simo.administracionusuario.common.repository.CatalogoRepository;

@Repository("catalogoRepository")
public class CatalogoRepositoryJDBCImpl extends BaseJDBCRepository implements CatalogoRepository{

	@Override
	public List<CatalogoObject> obtenerCatalogoRol() {
		// TODO Auto-generated method stub
		return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGOS_ROLES,
                new CatalogoRowMapperHelper(SQLColumnasConstants.CVE_ROL,SQLColumnasConstants.DES_ROL));
//		return null;
	}

	@Override
	public List<CatalogoObject> obtenerCatalogoTipoAsignacion() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<CatalogoObject> obtenerCatalogoDelegacion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CatalogoObject> obtenerCatalogoUnidadesMedicas(String cveDelegacion,String sqlCveCatalogo,String sqlDescCatlogo) {
		return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGOS_UNIDADES, new Object[] {cveDelegacion},
                new CatalogoRowMapperHelper(sqlCveCatalogo,sqlDescCatlogo));
	}

	@Override
	public List<CatalogoObject> obtenerCatalogoGeneric(String table,String sqlCveCatalogo,String sqlDescCatlogo) {
				return jdbcTemplate.query(table,
		                new CatalogoRowMapperHelper(sqlCveCatalogo,sqlDescCatlogo));
	}

}
