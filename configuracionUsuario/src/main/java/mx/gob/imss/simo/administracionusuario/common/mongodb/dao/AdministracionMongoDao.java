package mx.gob.imss.simo.administracionusuario.common.mongodb.dao;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SicRol;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitPersonalOperativo;
import mx.gob.imss.simo.administracionusuario.common.mongodb.model.SitUnidadMedica;

public interface AdministracionMongoDao {

	List<SitPersonalOperativo> buscarCuentaActiveD(String actDirectoryAccount);

	List<SitPersonalOperativo> buscarMatricula(String matricula);
	
	SitUnidadMedica buscarUnidadMedica(String actDirectoryAccount,String cvePresupuestal);
	
	List<SitUnidadMedica> buscarUnidadesMedicas(String actDirectoryAccount);
	
	SicRol buscarRol(String actDirectoryAccount);
	
	void guardarSitPersonalOperativo(SitPersonalOperativo sitPersonalOperativo);
	
	SitUnidadMedica guardarUnidadMedica(String actDirectoryAccount,SitUnidadMedica unidadMedica);
				
	boolean actualizarSicRol(String actDirectoryAccount,SicRol sicRol);
	
	SitUnidadMedica actualizarUnidadMedica(String actDirectoryAccount,SitUnidadMedica unidadMedica,String cvePresupuestal);
	
	//Modificacion estatus
	
	boolean actualizarEstatusPersonalOperativo(String actDirectoryAccount, Integer estatus, Date fechaBaja);
		
	boolean actualizarEstatusUnidadMedica(String actDirectoryAccount,int indEstatus,String cvePresupuestal);
	
	boolean actualizarEstatusAsignacionesUsuario(String actDirectoryAccount,int indEstatus);
		
}
