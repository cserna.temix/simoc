package mx.gob.imss.simo.administracionusuario.common.service.impl;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import mx.gob.imss.didt.cia.interoper.servicios.endpoint.ObtenerServicioResponse;
import mx.gob.imss.simo.administracionusuario.administracion.repository.AdministracionRepository;
import mx.gob.imss.simo.administracionusuario.common.constant.BaseConstants;
import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.helper.UsuarioHelper;
import mx.gob.imss.simo.administracionusuario.common.model.DatosSiap;
import mx.gob.imss.simo.administracionusuario.common.model.Parametro;
import mx.gob.imss.simo.administracionusuario.common.service.SiapService;
import mx.gob.imss.simo.master.client.ws.SimoMasterClientWS;
import mx.gob.imss.simo.siap.jaxb.ConsultaSimo;
import mx.gob.imss.simo.siap.jaxb.ConsultaSimoResponse;

@Service("siapService")
public class SiapServiceImpl implements SiapService {

	 protected final Logger logger = Logger.getLogger(getClass());
	 
    @Autowired
    private UsuarioHelper usuarioHelper;

    @Autowired
    private AdministracionRepository administracionRepository;

    private DatosSiap datosSiap;
    private String delegacion;
    private String matricula;
    private String errorExecution= null;

    private static final Integer TIEMPO_RESPUESTA = 3;

    @Override
    public DatosSiap consultarSiap(String matricula, String delegacion) throws Exception {
        setDelegacion(delegacion);
        setMatricula(matricula);
        logger.debug("delegacion : "+getDelegacion());
        logger.debug("matricula : "+getMatricula());
        setDatosSiap(new DatosSiap());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        try {
            executor.submit(new ConectaWsSiap()).get(TIEMPO_RESPUESTA, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
        	logger.error(e.getMessage());
        	logger.error(e);
            logger.debug("Error al buscar informacion del usuario en SIAP");
            throw new Exception(e.getMessage());
        } finally {
            executor.shutdown();
        }
        if(errorExecution !=null){
        	throw new Exception(errorExecution);
        }
        return getDatosSiap();
    }

    private class ConectaWsSiap implements Runnable {

        private static final String CONSULTAR_SIAP = "ConsultarSiap";
        private static final String VERSION = "version";

        @Override
        public void run() {
        	logger.debug("delegacion : "+getDelegacion());
        	logger.debug("matricula : "+getMatricula());
            ConsultaSimo requestSiap = new ConsultaSimo();
            requestSiap.setMatricula(getMatricula());
            requestSiap.setDelegacion(getDelegacion());
            ObtenerServicioResponse obtenerServicioResponse;
            try {
                Parametro parametro = administracionRepository.obtenerParametro(BaseConstants.SIMO_OSB);
                if(parametro!=null){
                	logger.debug("parametro : "+parametro.getReferenciaParametro());
                }
                obtenerServicioResponse = SimoMasterClientWS.obtenerServicio(requestSiap, CONSULTAR_SIAP, VERSION,
                        parametro.getReferenciaParametro());
                ConsultaSimoResponse csr = (ConsultaSimoResponse) SimoMasterClientWS.unmarshallObject(
                        (Element) obtenerServicioResponse.getEndPointCadisOut().getMensaje(),
                        ConsultaSimoResponse.class);
                if (csr.getConsultaSimoResult().getNewDataSet().getQry().getNUM() == 0) {
                    setDatosSiap(usuarioHelper.crearDatosSiap(csr));
                    //Mock
                    getDatosSiap().setMatricula(getMatricula());
                } else {
                	logger.error("error");
                    setDatosSiap(new DatosSiap());
                }
            } catch (JAXBException | UnsupportedEncodingException e) {
            	logger.error(e.getMessage());
                errorExecution= e.getMessage();
            } catch (MalformedURLException e) {
            	logger.error(e.getMessage());
                logger.info(e.getMessage(), e);
                errorExecution= e.getMessage();
            }
        }
    }

    public DatosSiap getDatosSiap() {

        return datosSiap;
    }

    public void setDatosSiap(DatosSiap datosSiap) {

        this.datosSiap = datosSiap;
    }

    public String getDelegacion() {

        return delegacion;
    }

    public void setDelegacion(String delegacion) {

        this.delegacion = delegacion;
    }

    public String getMatricula() {

        return matricula;
    }

    public void setMatricula(String matricula) {

        this.matricula = matricula;
    }
}
