package mx.gob.imss.simo.administracionusuario.common.helper;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.springframework.ldap.core.AttributesMapper;

import mx.gob.imss.simo.administracionusuario.common.model.Usuario;

public class DirectorioActivoMapper {
	
	public AttributesMapper personMapper() {
		return new AttributesMapper() {
			
			@Override
			public Usuario mapFromAttributes(final Attributes attributes) throws NamingException {
				final Usuario person = new Usuario();
				person.setRefPersonalOperativo((String) attributes.get("aquivaCuentaDominio").get());
				person.setRefNombreCompleto( (String) attributes.get("aquivanombrecompleto").get());
				person.setCveMatricula( (String) attributes.get("aquivaMatricula").get());
//				person.setEmail( (String) attributes.get("mail").get());
				return person;
			}
		};
	}
	
	public AttributesMapper groupMapper() {
		return new AttributesMapper() {
			
			@Override
			public List<String> mapFromAttributes(final Attributes attributes) throws NamingException {
				final List<String> userIds = new ArrayList<String>();
				final NamingEnumeration<?> uniquemembers = attributes.get("uniquemember").getAll();
				while (uniquemembers.hasMore()) {
					String userId = uniquemembers.next().toString().split(",")[0].split("=")[1];
					userIds.add(userId);
				}
				return userIds;
			}
		};
	}

}
