package mx.gob.imss.simo.administracionusuario.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.administracionusuario.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.EstatusEnum;
import mx.gob.imss.simo.administracionusuario.common.enumeracion.TipoAsignacionEnum;
import mx.gob.imss.simo.administracionusuario.common.model.AsignacionUnidad;
import mx.gob.imss.simo.administracionusuario.common.model.Rol;
import mx.gob.imss.simo.administracionusuario.common.model.UnidadMedica;

public class AsignacionRowMapperHelper  extends BaseHelper implements RowMapper<AsignacionUnidad>{

	@Override
	public AsignacionUnidad mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		AsignacionUnidad asignacionUnidad = new AsignacionUnidad();
		asignacionUnidad.setIndActivoUnidad(rs.getInt(SQLColumnasConstants.ESTATUS_PERSONAL_UNIDAD)==1 ? Boolean.TRUE:Boolean.FALSE);
		if(asignacionUnidad.getIndActivoUnidad()){
			asignacionUnidad.setDesEstatus(EstatusEnum.ACTIVO.getDescripcion());
		}else{
			asignacionUnidad.setDesEstatus(EstatusEnum.DESACTIVADOR.getDescripcion());
		}
		asignacionUnidad.setFecAltaUnidad(rs.getDate(SQLColumnasConstants.FEC_ALTA_PERSONAL_UNIDAD));
		asignacionUnidad.setFecBajaUnidad(rs.getDate(SQLColumnasConstants.FEC_BAJA_PERSONAL_UNIDAD));
		asignacionUnidad.setRol(new Rol());
		asignacionUnidad.getRol().setCveRol(rs.getString(SQLColumnasConstants.CVE_ROL));
		asignacionUnidad.getRol().setDesRol(rs.getString(SQLColumnasConstants.DES_ROL));
		
		asignacionUnidad.setUnidadMedica(new UnidadMedica());
		asignacionUnidad.getUnidadMedica().setCveDelegacionImss(rs.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
		asignacionUnidad.getUnidadMedica().setCveEntidadFed(rs.getString(SQLColumnasConstants.CVE_ENTIDAD_FED));
		asignacionUnidad.getUnidadMedica().setCvePresupuestal(rs.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
		asignacionUnidad.getUnidadMedica().setDesPresupuestal(rs.getString(SQLColumnasConstants.DES_UNIDAD_MEDICA));
		asignacionUnidad.getUnidadMedica().setDesDelegacionImss(rs.getString(SQLColumnasConstants.DES_DELEGACION_IMSS));
		Integer id = rs.getInt(SQLColumnasConstants.IND_TITULAR);
		asignacionUnidad.setIndTitular(id== 1);
		if(asignacionUnidad.getIndTitular()){
			asignacionUnidad.setDescTipoAsignacion(TipoAsignacionEnum.TITULAR.getDescripcion());	
		}else{
			asignacionUnidad.setDescTipoAsignacion(TipoAsignacionEnum.HUESPED.getDescripcion());
		}
		return asignacionUnidad;
	}

}
