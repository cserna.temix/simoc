package mx.gob.imss.simo.administracionusuario.common.service.impl;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.administracionusuario.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.administracionusuario.common.exception.AdministracionUsuarioException;
import mx.gob.imss.simo.administracionusuario.common.model.Usuario;
import mx.gob.imss.simo.administracionusuario.common.service.ActiveDirectoryService;

@Service("activeDirectoryService")
public class ActiveDirectoryServiceImpl  implements ActiveDirectoryService{
	
	protected final Logger logger = Logger.getLogger(getClass());
		
	@Value("${simoc.segurity.athentificacion}")
	public String segurityAuthentificacion;
	@Value("${simoc.segurity.principal}")
	public String segurityPrincipal;
	@Value("${simoc.segurity.credentials}")
	public String segurityCredential;
	@Value("${simoc.provider.url}")
	public String segurityProviderUrl;
	@Value("${simoc.provider.ldap.searchBase}")
	public String ldapSearchBase;
	@Value("${simoc.provider.url.oculto}")
	public String securityURLOculto;
	
	@Value("${parametro.directorio.departamento}")
	public String paramDepartamento;
	@Value("${parametro.directorio.telefono}")
	public String paramTelefono;
	@Value("${parametro.directorio.cuentadominio}")
	public String paramCuentaDominio;
	@Value("${parametro.directorio.direccion}")
	public String paramDireccion;
	@Value("${parametro.directorio.email}")
	public String paramEmail;
	@Value("${parametro.directorio.nombre}")
	public String paramNombre;
	@Value("${parametro.directorio.userDn}")
	public String userDn;
	
	
	@Value("${simoc.provider.ldap.searchBase.metro}")
	public String ldapSeachBaseMETRO;
	@Value("${simoc.provider.ldap.searchBase.occ}")
	public String ldapSeachBaseOCC;
	@Value("${simoc.provider.ldap.searchBase.nte}")
	public String ldapSeachBaseNTE;
	@Value("${simoc.provider.ldap.searchBase.cto}")
	public String ldapSeachBaseCTO;
	@Value("${simoc.provider.ldap.searchBase.sur}")
	public String ldapSeachBaseSUR;
	
	@Value("${simoc.provider.url.metro}")
	public String securityURLMETRO;
	@Value("${simoc.provider.url.occ}")
	public String securityURLOCC;
	@Value("${simoc.provider.url.nte}")
	public String securityURLNTE;
	@Value("${simoc.provider.url.cto}")
	public String securityURLCTO;
	@Value("${simoc.provider.url.sur}")
	public String securityURLSUR;
	
	@Value("${grupo.simoc.central.cn}")
	public String cnCentral;
	
	@Value("${grupo.simoc.pruebas.cn}")
	public String cnPrueba;

	@Value("${grupo.simoc.central.nombrecompleto}")
	public String nombreGrupoCentral;
	
	@Value("${grupo.simoc.pruebas.nombrecompleto}")
	public String nombreGrupoPrueba;
	
	@Value("${parametro.directorio.employeId}")
	public String employeId;
	
	public static final String MEMBEROF = "memberOf";
	public static final String MEMBER = "member";
	public static final String CN = "CN";
	
	
	public static final String NTE ="nte";
	public static final String NTE_CARPETA ="nte.imss.gob.mx";

	public static final String CTO ="cto";
	public static final String CTO_CARPETA ="cto.imss.gob.mx";
	
	public static final String SUR ="sur";
	public static final String SUR_CARPETA ="sur.imss.gob.mx";
	
	public static final String OCC ="occ";
	public static final String OCC_CARPETA ="occ.imss.gob.mx";

	public static final String METRO ="metro";
	public static final String METRO_CARPETA ="metro.imss.gob.mx";
	
	
private Hashtable<String, String> intConfiguracionesGlobales(){
      return getConfiguracion(segurityAuthentificacion,
    		  segurityPrincipal, 
    		  segurityCredential,
    		  segurityProviderUrl);
	}


private Hashtable<String, String> intConfiguraciones(String rutaDominio){
logger.info("ldapSearchBase : "+segurityAuthentificacion);
logger.info("ldapSearchBase : "+segurityPrincipal);
logger.info("ldapSearchBase : "+segurityCredential);
logger.info("ldapSearchBase : "+ rutaDominio);
  return getConfiguracion(segurityAuthentificacion,
		  segurityPrincipal, 
		  segurityCredential,
		  rutaDominio);
}
	
	private Hashtable<String, String> getConfiguracion(String segurityAutentification, 
			String segurityPrincipal, String segurityCredencials, String provade_url){
		
		Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION, segurityAutentification);
        env.put(Context.SECURITY_PRINCIPAL, segurityPrincipal);
        env.put(Context.SECURITY_CREDENTIALS, segurityCredencials);
        env.put(Context.PROVIDER_URL, provade_url);
      return env; 
	}
	
	private InitialLdapContext configuracionLdapContext(Hashtable<String, String> env) throws NamingException{
			return new InitialLdapContext(env, null);
	}

	
	private List<Usuario> searchDirectorio(String dominio,String cveDominio) throws AdministracionUsuarioException {
		try {
			String urlProvider=segurityProviderUrl;
			if (dominio.equals(METRO)) {
				dominio = ldapSeachBaseMETRO;
				urlProvider = securityURLMETRO;
			}
			if (dominio.equals(NTE)) {
				dominio = ldapSeachBaseNTE;
				urlProvider = securityURLNTE;
			}
			if (dominio.equals(CTO)) {
				dominio = ldapSeachBaseCTO;
				urlProvider = securityURLCTO;
			}
			if (dominio.equals(SUR)) {
				dominio = ldapSeachBaseSUR;
				urlProvider = securityURLSUR;
			}
			if (dominio.equals(OCC)) {
				dominio = ldapSeachBaseOCC; 
				urlProvider = securityURLOCC;
			}
			logger.info("ldapSearch: "+dominio);
			logger.info("cveDominio: "+cveDominio);
			logger.info("urlProvider: "+urlProvider);
			Hashtable<String, String> env=intConfiguraciones(urlProvider);
			InitialLdapContext ctx = configuracionLdapContext(env);
			NamingEnumeration<SearchResult> results = busquedaUsuarios(cveDominio, ctx,dominio);
			SearchResult searchResult = null;
			List<Usuario> cuentaDirectorioActivo = new ArrayList<Usuario>(); 
			int consecutivo =0;
			while(results.hasMoreElements()) {//mientras tenga mas elementos.
				searchResult = (SearchResult) results.nextElement();//guarde el siguiente
				Usuario cuentaDirectorio = new Usuario();
				if(searchResult.getAttributes().get(paramEmail)!=null){
				cuentaDirectorio.setRefEmail(searchResult.getAttributes().get(paramEmail).get().toString());//emailDominio
				}
				if(searchResult.getAttributes().get(paramNombre)!=null){
					cuentaDirectorio.setCarpeta(searchResult.getAttributes().get(paramNombre).get().toString());//Nombrecompleto
				}
				if(searchResult.getAttributes().get(employeId)!=null){
					String matricula = searchResult.getAttributes().get(employeId).get().toString();
					logger.info("matricula adctive "+matricula);
					if(matricula.matches("[0-9]*")){
						cuentaDirectorio.setCveMatricula(matricula);//matricula
						logger.info(matricula);
					}
				}
				if(searchResult.getAttributes().get(paramCuentaDominio)!=null){
				cuentaDirectorio.setRefPersonalOperativo(searchResult.getAttributes().get(paramCuentaDominio).get().toString());//cuentaDominio
				}
				if(searchResult.getAttributes().get(MEMBEROF) !=null){
					Attribute memberOf = searchResult.getAttributes().get(MEMBEROF);
					cuentaDirectorio.setGruposSeguridad(new ArrayList<>());
					for (int i = 0; i < memberOf.size(); i++) 
					{
						String atts = (String) memberOf.get(i).toString();
						if(atts.contains(cnCentral) || atts.contains(cnPrueba)){
							cuentaDirectorio.getGruposSeguridad().add(atts);
						}
					}
				}
				if (searchResult.getAttributes().get(userDn).get() != null) {
					String distinguishedName = searchResult.getAttributes().get(userDn).get().toString();
					if (distinguishedName.indexOf(NTE) != -1) {
						cuentaDirectorio.setCarpeta(NTE_CARPETA);
					}
					if (distinguishedName.indexOf(CTO) != -1) {
						cuentaDirectorio.setCarpeta(CTO_CARPETA);
					}
					if (distinguishedName.indexOf(SUR) != -1) {
						cuentaDirectorio.setCarpeta(SUR_CARPETA);
					}
					if (distinguishedName.indexOf(OCC) != -1) {
						cuentaDirectorio.setCarpeta(OCC_CARPETA);
					}
					if (distinguishedName.indexOf(METRO) != -1) {
						cuentaDirectorio.setCarpeta(METRO_CARPETA);
					}
				}
				cuentaDirectorio.setConsecutivo(++consecutivo);
				logger.info(cuentaDirectorio.getConsecutivo());
				cuentaDirectorioActivo.add(cuentaDirectorio);
			}
			return cuentaDirectorioActivo;            
		} catch (NamingException e) {
			logger.error("Error : "+e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_106);
		}
	}

	
	private void modificarGrupoSeguridad(String cveDominio,String usuarioDn, List<String> memberList,int dirContext ) throws AdministracionUsuarioException {
		logger.info("securityURLOculto : "+securityURLOculto);
		logger.info("cveDominio : "+cveDominio);
		logger.info("usuarioDn : "+usuarioDn);
		try {
			Hashtable<String, String> env=getConfiguracion(segurityAuthentificacion,segurityPrincipal,
					segurityCredential,securityURLOculto);
			InitialLdapContext ctx = configuracionLdapContext(env);
			if(memberList!=null && !memberList.isEmpty()){
				int index = 0;
				for(String memberSring : memberList){
					index=index+1;
						ModificationItem memberUser[] = new ModificationItem[1];
						memberUser[0] = new ModificationItem(dirContext, new BasicAttribute(MEMBER, usuarioDn));
			            ctx.modifyAttributes(memberSring, memberUser);
			            
//						ModificationItem memberUser[] = new ModificationItem[1];
//			            memberUser[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("member", userDn));
//			            ctx.modifyAttributes("CN=simo.central,OU=Grupos de Seguridad,DC=imss,DC=gob,DC=mx", memberUser);
				
//					    ModificationItem memberUser[] = new ModificationItem[1];
//		                memberUser[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("member", userDn));
//		                ctx.modifyAttributes("CN=SIMOPruebas,OU=Grupos de Seguridad,DC=imss,DC=gob,DC=mx", memberUser);
				}
			}
		} catch (NamingException e) {
			logger.error("Error NamingException: ",e);
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
		}
	}

	private NamingEnumeration<SearchResult> busquedaUsuarios(String cveDominio,InitialLdapContext ctx,String ldapSearchBase) throws AdministracionUsuarioException {
		try {
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String searchFilter =  "(&(objectClass=user)(sAMAccountName=" + cveDominio + "*))";
			NamingEnumeration<SearchResult> results = ctx.search(ldapSearchBase, searchFilter, searchControls);
			return results;
		} catch (NamingException e) {
			logger.error("Error al conectarse a directorio activo" + e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_106);
		}
	}
	
	private NamingEnumeration<SearchResult> busquedaUsuario(String cveDominio,InitialLdapContext ctx,String ldapSearchBase) throws AdministracionUsuarioException {
		try {
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String searchFilter =  "(&(objectClass=user)(sAMAccountName=" + cveDominio + "))";
			NamingEnumeration<SearchResult> results = ctx.search(ldapSearchBase, searchFilter, searchControls);
			return results;
		} catch (NamingException e) {
			logger.error("Error de NamingException : ",e);
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_106);
		}
	}

	
	@Override
	public void agregarGrupoSeguridad(String cveDominio) throws AdministracionUsuarioException {
		try {
			Hashtable<String, String> env = intConfiguracionesGlobales();
			InitialLdapContext ctx = configuracionLdapContext(env);
			String ldapSearchBase = this.ldapSearchBase;
			List<String> memberList = new ArrayList<>();
			memberList.add(nombreGrupoCentral);
			memberList.add(nombreGrupoPrueba);
			String usuaruoDn=null;
			NamingEnumeration<SearchResult> results = busquedaUsuario(cveDominio, ctx, ldapSearchBase);
			SearchResult searchResult = null;
			if(results.hasMoreElements()) {
				searchResult = (SearchResult) results.next();
				NamingEnumeration<String> var = searchResult.getAttributes().getIDs();//pones todas las propiedaes en una variable para poder recorrerlas
				String prop; 
				if (searchResult.getAttributes().get(userDn) != null) {
					usuaruoDn = searchResult.getAttributes().get(userDn).get().toString();
					logger.info("usuaruoDn : "+usuaruoDn);
					if(searchResult.getAttributes().get(MEMBEROF) != null){
						
						Attribute memberOf = searchResult.getAttributes().get(MEMBEROF);
						if(memberOf != null){
				            for (int i = 0; i < memberOf.size(); i++) 
				            {
				            	String atts = (String) memberOf.get(i).toString();
				            	omitirGrupoSeguridad(atts, memberList, cnCentral);
				            	omitirGrupoSeguridad(atts, memberList, cnPrueba);
			            	}
						}
					}
				}
			}
			logger.info("memberList : "+memberList.size());
			logger.info("userDn : "+userDn);
			if(usuaruoDn!=null){
				if(memberList!=null && !memberList.isEmpty()){
					modificarGrupoSeguridad(cveDominio, usuaruoDn, memberList, DirContext.ADD_ATTRIBUTE);
				}else{
					logger.info("Ya tiene los permisos necesarios : simo.central y SIMOPruebas");
				}
			}else{
				logger.error("Usuario Dn vacio :"+usuaruoDn);
				throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
			}
		} catch (NamingException e) {
			logger.error("Error NamingException"+e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
		}
	}
	
	private void omitirGrupoSeguridad(String atts,List<String> memberList,String cnGrupo){
		if(atts.contains(cnGrupo)){
        	Iterator<String > iterate = memberList.iterator();
        	while(iterate.hasNext()){
        		String grupo = iterate.next();
        		if(grupo.contains(cnGrupo)){
        			iterate.remove();
        		}
        	}
        }
	}
	
	@Override
	public List<Usuario> searchDirectorioActivo(boolean mock,String dominio,String cveDominio) throws AdministracionUsuarioException {
		List<Usuario> listUsuario = new ArrayList<>();
		if(mock){
			Random rand = new Random(); //instance of random class
		      int upperbound = 5;
		      String carpeta = "";
		      int int_random = rand.nextInt(upperbound); 
		      if (dominio.equals(NTE)) {
		    	  carpeta=NTE_CARPETA;
				}
				if (dominio.equals(CTO)) {
					carpeta=CTO_CARPETA;
				}
				if (dominio.equals(SUR)) {
					carpeta=SUR_CARPETA;
				}
				if (dominio.equals(OCC)) {
					carpeta =OCC_CARPETA;
				}
				if (dominio.equals(METRO)) {
					carpeta = METRO_CARPETA;
				}
				listUsuario.add(createMock(carpeta,cveDominio+"@imss.gob.mx",cveDominio));
				if(int_random!=0){
					
					for(int i =0; i <int_random;i++){
						listUsuario.add(createMock(carpeta,cveDominio+i+"@imss.gob.mx",cveDominio+i));
					}
				}
		}else{
			listUsuario = searchDirectorio(dominio, cveDominio);
		}
		return listUsuario;
	}
	
	private Usuario createMock(String carpeta,String email, String cveDominio){
		Usuario usuario = new Usuario();
		usuario.setCarpeta(carpeta);
		usuario.setRefEmail(email);
		usuario.setRefPersonalOperativo(cveDominio);
		return usuario;
	}


	@Override
	public void removerGrupoSeguridad(String cveDominio) throws AdministracionUsuarioException {
		try {
			Hashtable<String, String> env = intConfiguracionesGlobales();
			InitialLdapContext ctx = configuracionLdapContext(env);
			String ldapSearchBase = this.ldapSearchBase;
			logger.info("ldapSearchBase : "+ldapSearchBase);
			List<String> memberList = new ArrayList<>();
			String usuaruoDn=null;
			NamingEnumeration<SearchResult> results = busquedaUsuario(cveDominio, ctx, ldapSearchBase);
			SearchResult searchResult = null;
			logger.info("cveDominio : "+cveDominio);
			logger.info("userDn : "+userDn);
			logger.info("cnCentral : "+cnCentral);
			logger.info("cnPrueba : "+cnPrueba);
			if(results.hasMoreElements()) {
				searchResult = (SearchResult) results.next();
				if (searchResult.getAttributes().get(userDn) != null) {
					usuaruoDn = searchResult.getAttributes().get(userDn).get().toString();
					logger.info("usuaruoDn : "+usuaruoDn);
					if(searchResult.getAttributes().get(MEMBEROF) != null){
						
						Attribute memberOf = searchResult.getAttributes().get(MEMBEROF);
						if(memberOf != null){
				            for (int i = 0; i < memberOf.size(); i++) 
				            {
				            	String atts = (String) memberOf.get(i).toString();
				            	omitirGrupoSeguridad(atts, memberList, cnCentral);
				            	omitirGrupoSeguridad(atts, memberList, cnPrueba);
				            	
				            	if(atts.contains(cnCentral)){
				            		memberList.add(nombreGrupoCentral);
				                }else if(atts.contains(cnPrueba)){
				                	memberList.add(nombreGrupoPrueba);
				                }
			            	}
						}
					}
				}
			}
			logger.info("memberList : "+memberList.size());
			logger.info("userDn : "+userDn);
			if(usuaruoDn!=null){
				if(memberList!=null && !memberList.isEmpty()){
					modificarGrupoSeguridad(cveDominio, usuaruoDn, memberList, DirContext.REMOVE_ATTRIBUTE);
				}else{
					logger.info("Ya no tiene los permisos necesarios : simo.central y SIMOPruebas");
				}
			}else{
				logger.error("Usuario Dn vacio :"+usuaruoDn);
				throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
			}
		} catch (NamingException e) {
			logger.error("Error NamingException"+e.getMessage());
			throw new AdministracionUsuarioException(MensajesErrorConstants.ME_020);
		}
	}
	
}
