package mx.gob.imss.simo.administracionusuario.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.administracionusuario.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.administracionusuario.common.model.CatalogoObject;


public class CatalogoRowMapperHelper extends BaseHelper implements RowMapper<CatalogoObject>{
	
	String sqlCveCampo, sqlDescCampo;
	
	
	public CatalogoRowMapperHelper(String sqlCveCampo, String sqlDescCampo){
		this.sqlCveCampo = sqlCveCampo;
		this.sqlDescCampo = sqlDescCampo;
	}

	@Override
	public CatalogoObject mapRow(ResultSet resultSet, int rowId) throws SQLException {
		CatalogoObject catalogoObjeto = new CatalogoObject();
		catalogoObjeto.setClave(resultSet.getString(sqlCveCampo));
		catalogoObjeto.setDescripcion(resultSet.getString(sqlDescCampo));
        return catalogoObjeto;
	}

	
	public CatalogoObject catalogoMok(String valor){
		CatalogoObject catalogoMok = new CatalogoObject();
		catalogoMok.setClave(valor);
		catalogoMok.setDescripcion(valor+"catalogo");
		return catalogoMok;
	}
	
	public List<CatalogoObject> catalogoConjuntoMok(String ...valor){
		List<CatalogoObject> catalogo = new ArrayList<CatalogoObject>();
		for (String string : valor) {
			catalogo.add(catalogoMok(string));
		}
		return catalogo;
	}
}
