package mx.gob.imss.simo.administracionusuario.common.constant;

public class MensajesAdvertenciaConstants {

    public static final String MA_001 = "ma001";
    public static final String MA_002 = "ma002";
    public static final String MA_003 = "ma003";
    public static final String MA_004 = "ma004";
    public static final String MA_005 = "ma005";
    public static final String MA_006 = "ma006";
    public static final String MA_007 = "ma007";
    public static final String MA_008 = "ma008";
    public static final String MA_009 = "ma009";
    public static final String MA_010 = "ma010";
    public static final String MA_011 = "ma011";
    public static final String MA_012 = "ma012";
    public static final String MA_013 = "ma013";
    public static final String MA_014 = "ma014";

}