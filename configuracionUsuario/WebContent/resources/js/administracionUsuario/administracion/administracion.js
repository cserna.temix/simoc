var log = log4javascript.getNullLogger();

$(document).ready(
        function() {


            configuraPantalla(9,0,true);
            var $_FORM = "#administrarUsuarioForm\\:";
            var $_FORM_SF = "administrarUsuarioForm:";
          
            $('[id="administrarUsuarioForm:idDominio_input"]').focus();

            btn_click("#administrarUsuarioForm");
            console.log("idModAsigCveDelegacion");
            //Modal asignacion
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idModAsigCveDelegacion","rcModAsigCveDelegacion", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idModAsigCvePresupuestal", $_FORM_SF + "idModAsigCvePresupuestal",
                    "rcModAsigCvePresupuestal", 12);
            validateSelectOneMenuPressEnterTab($_FORM + "idModAsigTipoAsignacion", $_FORM_SF + "idModAsigTipoAsignacion", "rcModAsigSelectTipoAsig");
            validateSelectOneMenuPressEnterTab($_FORM + "idModAsigEstatus", $_FORM_SF + "idModAsigEstatus", "rcModAsigSelectEstatusAsig");
            
            //Modal rol
            validateSelectOneMenuPressEnterTab($_FORM + "idRolCatalogo", $_FORM_SF + "idRolCatalogo", "rcModRol");
                        
            //Modal usuario
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idUserMatricula","rcMatricula", 10);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idUserClaveDelegacion","rcModUserCveDelegacion", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idUserCvePresupuestal", $_FORM_SF + "idUserCvePresupuestal",
                    "rcModUserCvePresupuestal", 12);
            validateSelectOneMenuPressEnterTab($_FORM + "idUserRol", $_FORM_SF + "idUserRol", "rcModUserRol");
            
            //UsuarioDominio
            validateSelectOneMenuPressEnterTab($_FORM + "idDominio", $_FORM_SF + "idDominio", "rcSelectDominio");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCuentaDominio","rcCuestaDominio", 20);
            
            //Modal multiples usuarios
            validateInputChangeOrPressEnterTab($_FORM + "idNumeroUsuario", "rcNumeroUsuario");
            escDialog();
            
            $('[id="administrarUsuarioForm:idDominio"]').focus();
            
            
         // validacion de alfanumericos solamente
            $('[id="administrarUsuarioForm:idCuentaDominio"]').keydown(function(e) {
            	var shiftPresionado;
	          	  console.log("eeee keycode "+ e.keyCode);
	          	if (e.ctrlKey && e.altKey) {
	          	    if (e.which) {
	          	        return false;
	          	    }
	          	}
	          	if ((e.keyCode > 47 && e.keyCode < 65) || ((e.keyCode < 190 && e.keyCode > 186) || (e.keyCode > 190 && e.keyCode < 192)) || (e.keyCode > 218 && e.keyCode < 223)) {
	          		console.log("e keycode "+ e.keyCode);
	          	    return false;
	          	}
	          	if (e.shiftKey) {
	          	    shiftPresionado = true;
	          	} else {
	          	    shiftPresionado = false;
	          	}
	          	if (shiftPresionado
	          	        && ((e.keyCode > 47 && e.keyCode < 65) || ((e.keyCode < 190 && e.keyCode > 186) || (e.keyCode > 190 && e.keyCode < 192)) || (e.keyCode > 218 && e.keyCode < 223))) {
	          		console.log("e keycode "+ e.keyCode);
	          	    return false;
	          	}
	          	if (e.keyCode == 226) {
	          	    return false;
	          	}
	          	if (shiftPresionado && e.keyCode == 226) {
	          	    return false;
	          	}
            });
            
            var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
            if (foco == "administrarUsuarioForm:idDominio") {
            	 $('[id="administrarUsuarioForm:idDominio_input"]').focus();
	            aField=$('[id="administrarUsuarioForm:idDominio_input"]');
	        	setTimeout("aField.focus()", 2000);
            }
        });
        

function deshabilitaEnter() {
    btn_click("#administrarUsuarioForm");
}

// Inicio de usuario
function focoUserCveDelegacion() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idUserClaveDelegacion") {
        $('[id="administrarUsuarioForm:idUserClaveDelegacion"]').focus();
    }else{
    	if (foco == "administrarUsuarioForm:idUserCvePresupuestal") {
            $('[id="administrarUsuarioForm:idUserCvePresupuestal_input"]').focus();
        }
    }
}

function focoUserCvePresupuestal() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idUserCvePresupuestal") {
//        $('[id="administrarUsuarioForm:idUserCvePresupuestal_input"]').focus();
        aField=$('[id="administrarUsuarioForm:idUserCvePresupuestal_focus"]');
    	setTimeout("aField.focus()", 2000);
    }else if (foco == "administrarUsuarioForm:idUserRol") {
//        $('[id="administrarUsuarioForm:idUserRol_input"]').focus();
        aField=$('[id="administrarUsuarioForm:idUserRol_focus"]');
    	setTimeout("aField.focus()", 2000);
    }
}

function focoUserRol() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idUserRol") {
//        $('[id="administrarUsuarioForm:idUserRol_input"]').focus();
        aField=$('[id="administrarUsuarioForm:idUserRol_focus"]');
    	setTimeout("aField.focus()", 2000);
    }else if (foco == "administrarUsuarioForm:idGuardarUsuario") {
        $('[id="administrarUsuarioForm:idGuardarUsuario"]').focus();
    }
}

function focoGuardarUsuario() {
	focoUserCveDelegacion();
	focoUserCvePresupuestal();
	focoUserRol();
}

function focoUserCancelacion() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idUserMatricula") {
        $('[id="administrarUsuarioForm:idUserMatricula"]').focus();
    }
}

//Fin de usuario

function focoRol() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idRolCatalogo") {
//        $('[id="administrarUsuarioForm:idUserRol_input"]').focus();
        aField=$('[id="administrarUsuarioForm:idRolCatalogo_focus"]');
    	setTimeout("aField.focus()", 2000);
    }else if (foco == "administrarUsuarioForm:idIngresoRol") {
    	$(document.getElementById("administrarUsuarioForm:idIngresoRol")).focus();
        $('[id="administrarUsuarioForm:idIngresoRol"]').focus();
    }
}

function actualizarFocoGuardarRol() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idIngresoRol") {
    	$(document.getElementById("administrarUsuarioForm:idIngresoRol")).focus();
        $('[id="administrarUsuarioForm:idIngresoRol"]').focus();
    }
}

//Inicio focos de asignaciones
function actualizarFocoAsigCveDelegacion() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idModAsigCveDelegacion") {
        $('[id="administrarUsuarioForm:idModAsigCveDelegacion"]').focus();
    }else if(foco == "administrarUsuarioForm:idModAsigCvePresupuestal"){
    	$('[id="administrarUsuarioForm:idModAsigCvePresupuestal_input"]').focus();
    }
}

// AutoComplete de cve asignacion
function actualizarFocoAsigCvePresupuestal() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(document.activeElement.name);
    console.log(foco);
    if (foco == "administrarUsuarioForm:idModAsigCvePresupuestal") {
    	aField=$('[id="administrarUsuarioForm:idModAsigCvePresupuestal_focus"]');
    	setTimeout("aField.focus()", 2000);
    }else if (foco == "administrarUsuarioForm:idModAsigTipoAsignacion") {
    	aField=$('[id="administrarUsuarioForm:idModAsigTipoAsignacion_focus"]');
    	setTimeout("aField.focus()", 2000);
    }else  if (foco == "administrarUsuarioForm:idModAsigEstatus") {
    	aField=$('[id="administrarUsuarioForm:idModAsigEstatus_focus"]');
    	setTimeout("aField.focus()", 2000);
    }
    console.log(document.activeElement.name);
}

//Select de tipoasignacion
function actualizarFocoTipoAsignacionEstatus() {
	console.log(document.activeElement.name);
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idModAsigTipoAsignacion") {
        aField=$('[id="administrarUsuarioForm:idModAsigTipoAsignacion_focus"]');
    	setTimeout("aField.focus()", 2000);
    }else  if (foco == "administrarUsuarioForm:idModAsigEstatus") {
        aField=$('[id="administrarUsuarioForm:idModAsigEstatus_focus"]');
    	setTimeout("aField.focus()", 2000);
    }
    console.log(document.activeElement.name);
}

// Select de tipo de estatus
function actualizarFocoGuardarAsignacion() {
	console.log(document.activeElement.name);
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idModAsigEstatus") {
        aField=$('[id="administrarUsuarioForm:idModAsigEstatus_focus"]');
    	setTimeout("aField.focus()", 2000);
    }else if (foco == "administrarUsuarioForm:idIngresoAsignacion" ) {
        $('[id="administrarUsuarioForm:idIngresoAsignacion"]').focus();
    }else if(foco == "administrarUsuarioForm:idEditAsignacion") {
    	$('[id="administrarUsuarioForm:idEditAsignacion"]').focus();

    }
    console.log(document.activeElement.name);
}

//Boton guardar de asignacion
function focoGuardarAsignacion() {
	actualizarFocoAsigCveDelegacion();
	actualizarFocoAsigCvePresupuestal();
	actualizarFocoTipoAsignacionEstatus();	
}

//Boton cancelar de Asignacion
function focoCancelarAsignacion() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(document.activeElement.name);
    console.log(foco);
    if (foco == "administrarUsuarioForm:idModAsigCveDelegacion") {
        $('[id="administrarUsuarioForm:idModAsigCveDelegacion"]').focus();
    }
}

function actualizarSelectBoolean() {
    var selectDisable = $('[id="administrarUsuarioForm:banderaSelectBox"]').val();
    console.log(selectDisable);
        $("[id*=selectHuesped]").attr("disabled", selectDisable);
}

//select de dominio
function actualizarFocoDominio() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idDominio") {
        $('[id="administrarUsuarioForm:idDominio_input"]').focus();
    }else  if (foco == "administrarUsuarioForm:idCuentaDominio") {
        $('[id="administrarUsuarioForm:idCuentaDominio"]').focus();
    }
}

//Text de cuenta dominio
function actualizarFocoCuentaDominio() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idCuentaDominio") {
        $('[id="administrarUsuarioForm:idCuentaDominio"]').focus();
    }else if(foco == "administrarUsuarioForm:idObtenerUsuarioOperativo"){
    	$('[id="administrarUsuarioForm:idObtenerUsuarioOperativo"]').focus();
    }
}

//Foco boton de busquedaDominio
function focoDominio() {
    btn_click("#administrarUsuarioForm");
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(foco);
    if (foco == "administrarUsuarioForm:idDominio") {
        $('[id="administrarUsuarioForm:idDominio_input"]').focus();
    }else  if (foco == "administrarUsuarioForm:idCuentaDominio") {
        $('[id="administrarUsuarioForm:idCuentaDominio"]').focus();
    }else if (foco == "administrarUsuarioForm:idUserMatricula") {
    	$('[id="administrarUsuarioForm:idUserMatricula"]').focus();
    }
}

function focoMatricula() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    if (foco == "administrarUsuarioForm:idUserMatricula") {
    	$('[id="administrarUsuarioForm:idUserMatricula"]').focus();
    }else if(foco == "administrarUsuarioForm:idBusquedaSIAP"){
    	$('[id="administrarUsuarioForm:idBusquedaSIAP"]').focus();
    }
}

function focoBusquedaSIAP() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    if (foco == "administrarUsuarioForm:idBusquedaSIAP") {
    	$('[id="administrarUsuarioForm:idBusquedaSIAP"]').focus();
    }else if(foco == "administrarUsuarioForm:idUserClaveDelegacion"){
    	$('[id="administrarUsuarioForm:idUserClaveDelegacion"]').focus();
    }
}

function focoNumeroUsuario() {
    var foco = $('[id="administrarUsuarioForm:idHiddenFoco"]').val();
    console.log(document.activeElement.name);
    console.log(foco);
    if (foco == "administrarUsuarioForm:idAceptar") {
    	$('[id="administrarUsuarioForm:idAceptar"]').focus();
    	aField=$('[id="administrarUsuarioForm:idAceptar_focus"]');
    	setTimeout("aField.focus()", 2000);
    }
    console.log(document.activeElement.name);
}


function keyDownMatricula(){
$('[id="administrarUsuarioForm:idUserMatricula"]').keydown(function(e) {
//	keyDownNumeros(e);
	var shiftPresionado;
  	  console.log("eeee keycode "+ e.keyCode);
  	if (e.ctrlKey && e.altKey) {
  	    if (e.which) {
  	        return false;
  	    }
  	}
  	if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode==8 || e.keyCode==13 || e.keyCode==46) {
  		console.log("e keycode "+ e.keyCode);
  	    return true;
  	}else{
  		return false;
  	}
  	if (e.shiftKey) {
  	    shiftPresionado = true;
  	} else {
  	    shiftPresionado = false;
  	}
  	if (shiftPresionado) {
  		console.log("e keycode "+ e.keyCode);
  	    return false;
  	}
  	if (e.keyCode == 226) {
  	    return false;
  	}
  	if (shiftPresionado && e.keyCode == 226) {
  	    return false;
  	}
});
}

function keyDownUserCveDelegacion(){
	$('[id="administrarUsuarioForm:idUserClaveDelegacion"]').keydown(function(e) {
    	var shiftPresionado;
      	  console.log("eeee keycode "+ e.keyCode);
      	if (e.ctrlKey && e.altKey) {
      	    if (e.which) {
      	        return false;
      	    }
      	}
      	if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode==8 || e.keyCode==13 || e.keyCode==46) {
      		console.log("e keycode "+ e.keyCode);
      	    return true;
      	}else{
      		return false;
      	}
      	if (e.shiftKey) {
      	    shiftPresionado = true;
      	} else {
      	    shiftPresionado = false;
      	}
      	if (shiftPresionado) {
      		console.log("e keycode "+ e.keyCode);
      	    return false;
      	}
      	if (e.keyCode == 226) {
      	    return false;
      	}
      	if (shiftPresionado && e.keyCode == 226) {
      	    return false;
      	}
    });
}

// copia
function keyDownCveDelegacion(){
	  $('[id="administrarUsuarioForm:idModAsigCveDelegacion"]').keydown(function(e) {
		  var shiftPresionado;
      	  console.log("eeee keycode "+ e.keyCode);
      	if (e.ctrlKey && e.altKey) {
      	    if (e.which) {
      	        return false;
      	    }
      	}
      	if ((e.keyCode > 58 && e.keyCode < 65) || ((e.keyCode < 190 && e.keyCode > 186) || (e.keyCode > 190 && e.keyCode < 192)) || (e.keyCode > 218 && e.keyCode < 223)) {
      		console.log("e keycode "+ e.keyCode);
      	    return false;
      	}
      	if (e.shiftKey) {
      	    shiftPresionado = true;
      	} else {
      	    shiftPresionado = false;
      	}
      	if (shiftPresionado
      	        && ((e.keyCode > 47 && e.keyCode < 65) || ((e.keyCode < 190 && e.keyCode > 186) || (e.keyCode > 190 && e.keyCode < 192)) || (e.keyCode > 218 && e.keyCode < 223))) {
      		console.log("e keycode "+ e.keyCode);
      	    return false;
      	}
      	if (e.keyCode == 226) {
      	    return false;
      	}
      	if (shiftPresionado && e.keyCode == 226) {
      	    return false;
      	}
    });
}

