PrimeFaces.locales['es'] = {
				    closeText: 'Cerrar',
				    prevText: 'Anterior',
				    nextText: 'Siguiente',
				    monthNames: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
				    dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
				    dayNamesShort: ['Dom','Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
				    dayNamesMin: ['D','L','M','X','J','V','S'],
				    weekHeader: 'Semana',
				    firstDay: 1,
				    isRTL: false,
				    showMonthAfterYear: false,
				    yearSuffix: '',
				    timeOnlyTitle: 'Sólo hora',
				    timeText: 'Tiempo',
				    hourText: 'Hora',
				    minuteText: 'Minuto',
				    secondText: 'Segundo',
				    currentText: 'Fecha actual',
				    ampm: false,
				    month: 'Mes',
				    week: 'Semana',
				    day: 'Día',
				    allDayText : 'Todo el día'
				};
actualizarTabla('fechasReporte:btn_actualizarOculto');

/*
--Funcion general, enter, tabulador change input--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ 
*funcionStr= Nombre de la función (en este caso id del remoteCommand) que queramos ejecutar
*/
function valkeyET(id,funcionStr,desbloqueado){
	
	$(id).on('keydown change',function( event){					
		 if (event.shiftKey && event.which ==9) {
			 //event.shiftKey &amp;&amp; event.which ==9
			  // Shift-tab pressed
			 console.log("entra &&");
		 }else{
			  if (event.which ==13 || event.which ==9 || event.which ==null) {
				  if(desbloqueado){
					  var func=window[funcionStr];
					  console.log("-> simoScript.js id:"+id+"    funcionStr:"+funcionStr);
					  func();  
				  }
				  	
			  }
		 }
	});
}
/*
--Maximo autosalto input--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ 
*sig_id=id del siguiente componente (algunos componente como los selectone menu requieren _input) 
formCexBusqEd:idRegimen_focus sin # y sin \\
al que enviara el foco, para q se marque un evento change y ejecute 
en la otra funcion de valkeyET
*max= valor con el cual se ejecutará la acción
*/
function valKeyMax(id,sig_id,max){
	$(id).keyup(function(){
		if (this.value.length == max){
			document.getElementById(sig_id).focus();
		}
	});
}
/*SELECTONE MENU
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\
*idsf= mismo id del componente con todo y form formCexBusqEd:idEspecialidadAc quitar # y \\
*funcionStr= Nombre de la función (en este caso id del remoteCommand) que queramos ejecutar
*/
function selcetOneMenu_valKeyET(id,idsf,funcionStr){
	
	$(id+"_focus").on('keydown change',function( event){
		var elem = document.getElementById(id+"_panel");
		var theCSSprop = window.getComputedStyle(elem,null).getPropertyValue("display");
		 if(theCSSprop=='none'){
			 if (event.shiftKey && event.which ==9) {
				 //(event.shiftKey &amp;&amp; event.which ==9
				 console.log("entra &&");
			 // Shift-tab pressed
			 }else{
				  if (event.which ==13 || event.which ==9 || event.which ==null) {						  								 
					  var func=window[funcionStr];
					  console.log("id:"+id+"    funcionStr:"+funcionStr);
					  func();	
				  }
			 }
		 }
	});
}	


			

/*
--autocomplete_valkeyMax--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\
*idsf= mismo id del componente con todo y form formCexBusqEd:idEspecialidadAc quitar # y \\
*funcionStr= Nombre de la función (en este caso id del remoteCommand) que queramos ejecutar
*max= valor con el cual se ejecutará la acción
*/
function autocomplete_valKeyMax(id,idsf,funcionStr,max,desbloqueado){	
	console.log("autocomplete_valKeyMax id:"+id+"  idsf:"+idsf+"    funcionStr:"+funcionStr+"    max:"+max);
	$(id+"_input").keyup(function(){
	var str=document.getElementById(idsf+'_input');	
	 var func=window[funcionStr];
	 
	if (str.value.length == max){
		if(desbloqueado){
			func();	
		}
						
	}else if(str.value.length > max){
		//supr 46
		//8 backspace
		 if (event.which ==8 || event.which ==46 || event.which ==null) {	
			 document.getElementById(idsf+'_input').value="";
			 if(desbloqueado){
					func();	
				}			
		  }
	}
	
});	

		
}
/*Funcion que funciona para deshabilitar la ejecución del boton en
 * primefaces , solo recibe el nombre del formulario donde se encunetra
 * 
 * */
function btn_click(formulario){
	 $(formulario).keydown(function(event){
		 if(event.keyCode == 13 ) {
		      event.preventDefault();					      					   
		      if(document.activeElement.tagName=="BUTTON"){		    	 
			    	 var activo=document.getElementById(document.activeElement.getAttribute("id"));
			    	 activo.click();
			    	 activo.disabled = true;						    					    	
			  }
		    }
		  });
}


/*
--Funcion general, enter, tabulador change input--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ 
*sig_id=id del siguiente componente (algunos componente como los selectone menu requieren _input) 
formCexBusqEd:idRegimen_focus sin # y sin \\
*/
function valkeyET_changefoco(id,sig_id){
	
	$(id).on('keydown change',function( event){					
		 if (event.shiftKey && event.which ==9) {
			 //event.shiftKey &amp;&amp; event.which ==9
			  // Shift-tab pressed
			 console.log("entra &&");
		 }else{
			  if (event.which ==13 || event.which ==null) {				  
				  document.getElementById(sig_id).focus();
			  }
		 }
	});

}

/*
--Funcion general, enter, tabulador change input y cuando se alcanza el numero máximo--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ 
*funcionStr= Nombre de la función (en este caso id del remoteCommand) que queramos ejecutar
*max= valor con el cual se ejecutará la acción
*/	
function valKeyETandMAX(id,funcionStr,max,desbloqueado){
	$(id).on('keyup change',function( event){
		 var str= this.value;
		 var func=window[funcionStr];
		 if (event.shiftKey && event.which ==9) {
			  // Shift-tab pressed
		 }else{
			 console.log("valKeyETandMAX desbloqueado:"+desbloqueado+"  value:"+str);
			  if (event.which ==13 || event.which ==9 || event.which ==null ||(str.length >= max)) {
				  
				  if(desbloqueado){
					  func();  
				  }				  
			  }
		 }
	
	});
}


/*
--Funcion general para Mask con guiones bajos enter, tabulador change input y cuando se alcanza el numero máximo--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ 
*funcionStr= Nombre de la función (en este caso id del remoteCommand) que queramos ejecutar
*max= valor con el cual se ejecutará la acción
*/	
function valKeyETandMAXMask(id,funcionStr,max,desbloqueado){
	$(id).on('keyup change',function( event){
		var str= this.value.split('_').join('');
		 console.log(" valKeyETandMAXMask id:"+id+"    funcionStr:"+funcionStr);
		 var func=window[funcionStr];
		 if (event.shiftKey && event.which ==9) {
			  // Shift-tab pressed
		 }else{
			  if (event.which ==13 || event.which ==9 || event.which ==null ||(str.length >= max)) {						  
				  if(desbloqueado){
					  func();  
				  }
			  }
		 }
	
	});
}


//Valida formato de fecha
function validarFecha(fecha){
	//console.log("fecha:"+fecha);
	fecha=fecha.split("/"); 
	var dia=fecha[0]; 
	var mes=parseInt(fecha[1])-1; 
	var ano=fecha[2]; 
//	console.log("ano:"+ano);
//	console.log("mes:"+mes);
//	console.log("dia:"+dia);
	 
	var valor = new Date(ano, mes, dia);
	//console.log("valor:"+valor); 
	if(valor.getMonth()!=mes) 
	{ 
	//	console.log("mal fecha");
		return false;
	} 
	else if(valor.getUTCDate()!=dia) 
	{ 
	//	console.log("mal fecha"); 
		return false;
	} 
	else{ 
	//	console.log("bien la fecha");
		return true;
	} 

}


//Configurar pantalla

function setClassEncabezadoPieDePagina(encabezado, pieDePagina){
    document.getElementById('encabezado').removeAttribute('class');
    document.getElementById('pieDePagina').removeAttribute('class');
    document.getElementById('encabezado').setAttribute("class", encabezado);
    document.getElementById('pieDePagina').setAttribute("class", pieDePagina);
}

function deshabilitaBotonesEncabezado(){
    if(document.getElementById("idHeaderGeneral:btnp_usuario")==null){
     
    }else{
  
        document.getElementById("idHeaderGeneral:btnp_usuario").style.visibility = "collapse";
    }
     if(document.getElementById("idHeaderGeneral:btnp_ayuda")==null){
       
        }else{
          
            document.getElementById("idHeaderGeneral:btnp_ayuda").style.visibility = "collapse";
        }
     if(document.getElementById("idHeaderGeneral:btnp_salir")==null){
        
     }else{
         
         document.getElementById("idHeaderGeneral:btnp_salir").style.visibility = "collapse";
     }   
}

/*Funcion que sirve para la configuraciÃ³n visual de las pantalas
 * Color de encabezado y pie de pÃ¡gina, periodo, botones
 */
function configuraPantalla(pantallaModulo, tipoServicio, pantallaDeCaptura){
   
    //Diferente a portada
    setClassEncabezadoPieDePagina('encabezadoCaptura', 'pieDePaginaCaptura');
    cambiarColorPantalla(pantallaModulo);    
    evaluarTipoServicio(tipoServicio);   
    if(pantallaDeCaptura){
        deshabilitaBotonesEncabezado();
    }
}

function evaluarTipoServicio(tipoServicio){

	if(tipoServicio==0){
    switch(tipoServicio){
        //consulta externa
        case 1:
            
//            document.getElementById('idHeaderGeneral:servicioHospi').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='none';
            if(document.getElementById('header:idHeaderGeneral:servicioCex').value=="" || document.getElementById('header:idHeaderGeneral:servicioCex').value==null){
                document.getElementById('header:idHeaderGeneral:labelTipoServicio').style.display='none';
                document.getElementById('header:idHeaderGeneral:servicioCex').style.display='none';
            }else{
                document.getElementById('header:idHeaderGeneral:labelTipoServicio').style.display='inline';
                document.getElementById('header:idHeaderGeneral:servicioCex').style.display='inline';
            }
            break;
        //hospitalizacion
        case 2:
           
//            document.getElementById('idHeaderGeneral:servicioCex').style.display='none';
//            
//            document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='none';
            if(document.getElementById('header:idHeaderGeneral:servicioHospi').value==""){
                document.getElementById('header:idHeaderGeneral:labelTipoServicio').style.display='none';
                document.getElementById('header:idHeaderGeneral:servicioHospi').style.display='none';
            }else{
                document.getElementById('header:idHeaderGeneral:labelTipoServicio').style.display='inline';
                document.getElementById('header:idHeaderGeneral:servicioHospi').style.display='inline';
            }
            break;
        case 3:
            if(document.getElementById('header:idHeaderGeneral:servicioInfoComplementaria').value==""){
                document.getElementById('header:idHeaderGeneral:labelTipoServicio').style.display='none';
                document.getElementById('header:idHeaderGeneral:servicioInfoComplementaria').style.display='none';
            }else{header:
                document.getElementById('header:idHeaderGeneral:labelTipoServicio').style.display='inline';
                document.getElementById('header:idHeaderGeneral:servicioInfoComplementaria').style.display='inline';
            }
            break;
        case 4:
            
            if(document.getElementById('header:idHeaderGeneral:servicioInfoComplementaria').value==""){
                document.getElementById('header:idHeaderGeneral:labelTipoServicio').style.display='none';
                document.getElementById('header:idHeaderGeneral:servicioInfoComplementaria').style.display='none';
            }else{
                document.getElementById('header:idHeaderGeneral:labelTipoServicio').style.display='inline';
                document.getElementById('header:idHeaderGeneral:servicioSubrogados').style.display='inline';
            }
            break;
         default:
//             document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='none';
//             document.getElementById('idHeaderGeneral:servicioCex').style.display='none';
//             document.getElementById('idHeaderGeneral:servicioHospi').style.display='none';
//             document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='none';
//             document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='none';
         break;
    }
	}
    
}

function cambiarColorPantalla(pantallaModulo) {

    switch(pantallaModulo){
        //Administracion
        case 1:
            document.getElementById('encabezado').className += " encabezadoyPieAdministracion";
            document.getElementById('pieDePagina').className += " encabezadoyPieAdministracion";
            break;
        //Catalogos
        case 2:
            document.getElementById('encabezado').className += " encabezadoyPieCatalogos";
            document.getElementById('pieDePagina').className += " encabezadoyPieCatalogos";
            break;
        //Consulta Externa
        case 3:
            document.getElementById('encabezado').className += " encabezadoyPieConsultaExterna";
            document.getElementById('footer:footer_pG').className += " footer_pG_col_cex";
            style="background-color: red;"
            break;
        //Hospitalizacion
        case 4:
            document.getElementById('encabezado').className += " encabezadoyPieHospitalizacion";
            document.getElementById('pieDePagina').className += " encabezadoyPieHospitalizacion";
            break; 
        //Reporte
        case 5:
            document.getElementById('encabezado').className += " encabezadoyPieReportes";
            document.getElementById('footer:footer_pG').className += " footer_pG_col_reportes";
            break;
        //Enlaces
        case 6:
            document.getElementById('encabezado').className += " encabezadoyPieEnlaces";
            document.getElementById('pieDePagina').className += " encabezadoyPieEnlaces";
            break;
        //Utilerias
        case 7:
            document.getElementById('encabezado').className += " encabezadoyPieUtilerias";
            document.getElementById('pieDePagina').className += " encabezadoyPieUtilerias";
            break;
        //Interfaces
        case 8:
            document.getElementById('encabezado').className += " encabezadoyPieInterfaces";
            document.getElementById('pieDePagina').className += " encabezadoyPieInterfaces";
            break;
        default:
            document.getElementById('encabezado').className += " encabezadoyPiePrincipal";
            document.getElementById('pieDePagina').className += " encabezadoyPiePrincipal";
            break;
    }
    
}

function actualizarTabla(tabla) {
    setInterval(
            function(){
                document.getElementById(tabla).click();
                }, 90000);
}

