package mx.gob.imss.simo.reporteparte1.reporte.services;

import java.util.Iterator;

import org.springframework.stereotype.Service;

import mx.gob.imss.simo.reporteparte1.common.exception.ReporteParteIException;
import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.common.services.ReporteParte1CommonService;

@Service
public interface ColaDeReportesService extends ReporteParte1CommonService {

    /**
     * Busca en la Bd los reportes que se encuentran en la cola de acuerdo a un estatus
     * 
     * @return
     */
    Iterator<SitColaReportes> buscarReportesEnCola(Integer estatus, Integer limite);

    /**
     * Busca en la Bd los reportes que se encuentran en la cola
     * 
     * @return
     * @throws ReporteParteIException
     */
    // void buscarReportesEnCola() throws ReporteParteIException, InterruptedException;

    public Iterator<SitColaReportes> buscarReportesEnColaActivos(Integer estatus1, Integer estatus2, Integer estatus3);

    /**
     */
    public SitColaReportes buscarReporteExistenteEnCola(String cvePresupuestal, Integer clavePeriodo, Integer estatus,
            Long idThread);

    public Boolean insertarReporteEnCola(String cvePresupuestal, String desUnidad, Integer clavePeriodo,
            Integer volumen);

    public SitColaReportes actualizarReporteEnCola(SitColaReportes reporteEnCola, Integer estatusFinal)
            throws ReporteParteIException;

    /**
     * Estima el tiempo que tardar� en generarse un reporte
     * 
     * @return Minutos en generarse
     */
    public Integer tiempoProcesarReporte(Integer promRegistros);

    void buscarReportesEnColaParalelo() throws ReporteParteIException, InterruptedException;

}
