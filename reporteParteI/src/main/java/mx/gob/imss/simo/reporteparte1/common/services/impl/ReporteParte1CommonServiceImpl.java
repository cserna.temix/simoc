package mx.gob.imss.simo.reporteparte1.common.services.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.reporteparte1.common.model.SitControlCapturaCE;
import mx.gob.imss.simo.reporteparte1.common.model.SitUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.repository.ReporteParte1CommonRepository;
import mx.gob.imss.simo.reporteparte1.common.services.ReporteParte1CommonService;

@Service("reporteParte1CommonService")
public class ReporteParte1CommonServiceImpl implements ReporteParte1CommonService {

    @Autowired
    ReporteParte1CommonRepository reporteParte1CommonRepository;

    @Override
    public SitUnidadMedica buscarUnidadMedica(String cvePresupuestal) {

        return reporteParte1CommonRepository.buscarUnidadMedica(cvePresupuestal);
    }

    @Override
    public void actualizarControlCapturaCe(Boolean consutaExternaUr, String cvePresupuestal, String desPresupuestal,
            Integer periodo) {

        if (consutaExternaUr) {
            // AQUI se debe de insertar en la clase de control
            SitControlCapturaCE controlCapturaCE = new SitControlCapturaCE();
            SdUnidadMedica unidad = new SdUnidadMedica();
            unidad.setCvePresupuestal(cvePresupuestal);
            unidad.setDesUnidadMedica(desPresupuestal);
            controlCapturaCE.setSdUnidadMedica(unidad);
            // SicPeriodosImss
            // periodo=consultaExternaDao.obtenerPeriodImssPorFecha(consultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado());
            controlCapturaCE.setFecActualizaControl(new Date());
            controlCapturaCE.setIndControl(0);
            controlCapturaCE.setCvePeriodo(periodo);
            reporteParte1CommonRepository.actualizarControlCapturaCE(controlCapturaCE);

        }
    }

}
