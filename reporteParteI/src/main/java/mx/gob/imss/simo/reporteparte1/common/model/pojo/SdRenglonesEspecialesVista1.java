package mx.gob.imss.simo.reporteparte1.common.model.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SdRenglonesEspecialesVista1 {
	
	private SdTotalesReporte sdViolencia ;
	private List<SdTotalesReporte> sdParamedicos = new ArrayList <>();
	private SdTotalesReporte sdTococirugia;
	
}
