package mx.gob.imss.simo.reporteparte1.reporte.services.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.TimeZone;

import lombok.Data;
import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteIAccidentesyL;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdPeriodoReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdRenglonesEspecialesVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdTotalesReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdV2Especialidad;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista2;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;
import mx.gob.imss.simo.reporteparte1.common.services.impl.ReporteParte1CommonServiceImpl;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ProcesarReporteParteIRepository;
import mx.gob.imss.simo.reporteparte1.reporte.rules.ProcesarReporteParteIRules;
import mx.gob.imss.simo.reporteparte1.reporte.services.ProcesarReporteParteIService;

@Data
@Service("generarReporteParteIService")
public class ProcesarReporteParteIServiceImpl extends ReporteParte1CommonServiceImpl
        implements ProcesarReporteParteIService {

    SitReporteParteI sitReporte = new SitReporteParteI();

    @Autowired
    ProcesarReporteParteIRepository procesarReporteParteIRepository;

    @Autowired
    ProcesarReporteParteIRules procesarReporteParteIRules;

    SdUnidadMedica sdUnidadMedica = new SdUnidadMedica();
    SdPeriodoReporte sdPeriodoReporte = new SdPeriodoReporte();

    @Override
    public SdVistas extraerInfoMedicosReporte(String cvePresupuestal, String desUnidadMedica, Integer periodoInt) {

        SicPeriodosImss periodo = procesarReporteParteIRepository.obtenerPeriodImss(periodoInt);

        sdUnidadMedica.setCvePresupuestal(cvePresupuestal);
        sdUnidadMedica.setDesUnidadMedica(desUnidadMedica);
        sdPeriodoReporte.setCvePeriodo(periodo.getCvePeriodo());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        String fechaInicial = sdf.format(periodo.getFecInicial());
        String fechaFinal = sdf.format(periodo.getFecFinal());

        sdPeriodoReporte.setDesPeriodo("del  " + fechaInicial + " al " + fechaFinal);

        // Se extrae la información para la Vista 2
        Iterator<ExtractoReporteParteI> extraccionReporteEspecialidades = procesarReporteParteIRepository
                .extraerInfoMedicosReporteVista2(cvePresupuestal, periodo.getFecInicial(), periodo.getFecFinal(),
                        BaseConstants.TC_ESPECIALIDADES);
        Iterator<ExtractoReporteParteI> extraccionReporteUrgencias = procesarReporteParteIRepository
                .extraerInfoMedicosReporteVista2(cvePresupuestal, periodo.getFecInicial(), periodo.getFecFinal(),
                        BaseConstants.TC_URGENCIAS);
        Iterator<ExtractoReporteParteI> extraccionReporteTocoCirugia = procesarReporteParteIRepository
                .extraerInfoMedicosReporteVista2(cvePresupuestal, periodo.getFecInicial(), periodo.getFecFinal(),
                        BaseConstants.TC_TOCOCIRUGIA);
        Iterator<ExtractoReporteParteI> extraccionReporteParamedicos = procesarReporteParteIRepository
                .extraerInfoMedicosReporteVista2(cvePresupuestal, periodo.getFecInicial(), periodo.getFecFinal(),
                        BaseConstants.TC_PARAMEDICOS);
        Iterator<ExtractoReporteParteIAccidentesyL> extraccionReporteAccidentesYles = procesarReporteParteIRepository
                .extraerInfoMedicosReporteAccidentesyLes(cvePresupuestal, periodo.getFecInicial(),
                        periodo.getFecFinal());

        // pone la información de vistas

        SdVistas vistasEspecialidades = procesarReporteParteIRules
                .crearInformacionReporte(extraccionReporteEspecialidades, BaseConstants.TC_ESPECIALIDADES);
        SdVistas vistasUrgencias = procesarReporteParteIRules.crearInformacionReporte(extraccionReporteUrgencias,
                BaseConstants.TC_URGENCIAS);
        SdVistas vistasTococirugia = procesarReporteParteIRules.crearInformacionReporte(extraccionReporteTocoCirugia,
                BaseConstants.TC_TOCOCIRUGIA);
        SdVistas vistasParamedicos = procesarReporteParteIRules
                .crearInformacionReporteParamedicos(extraccionReporteParamedicos, BaseConstants.TC_PARAMEDICOS);
        SdTotalesReporte vistaAccidentesYlesiones = procesarReporteParteIRules
                .extraccionReporteAccidentesYles(extraccionReporteAccidentesYles);

        SdVistas sdVistasReporte = new SdVistas();

        // Se agrega lo de ----VISTA 1-------
        SdVista1 sdVista1 = new SdVista1();

        sdVista1.setSdRepEspecialidades(vistasEspecialidades.getSdVista1().getSdRepEspecialidades());
        sdVista1.setSdUrgencias(vistasUrgencias.getSdVista1().getSdRepEspecialidades());
        sdVista1.setSdTotalUnidad(
                procesarReporteParteIRules.sumarTotales(sdVista1.getSdRepEspecialidades(), sdVista1.getSdUrgencias()));

        // PARAMEDICOS
        SdRenglonesEspecialesVista1 renglonesEspeciales = new SdRenglonesEspecialesVista1();
        renglonesEspeciales
                .setSdParamedicos(vistasParamedicos.getSdVista1().getSdRenglonesEspeciales().getSdParamedicos());

        // PARAMEDICOS
        renglonesEspeciales.setSdTococirugia(vistasTococirugia.getSdVista1().getSdRepEspecialidades());
        renglonesEspeciales.setSdViolencia(vistaAccidentesYlesiones);

        sdVista1.setSdRenglonesEspeciales(renglonesEspeciales);

        sdVistasReporte.setSdVista1(sdVista1);

        // Se agrega lo de --------VISTA 2--------
        SdVista2 sdVista2 = new SdVista2();
        List<SdV2Especialidad> listadV2Especialidad = new ArrayList<>();

        listadV2Especialidad.addAll(vistasEspecialidades.getSdVista2().getSdV2Especialidad());
        listadV2Especialidad.addAll(vistasUrgencias.getSdVista2().getSdV2Especialidad());
        listadV2Especialidad.addAll(vistasTococirugia.getSdVista2().getSdV2Especialidad());
        listadV2Especialidad.addAll(vistasParamedicos.getSdVista2().getSdV2Especialidad());

        sdVista2.setSdV2Especialidad(listadV2Especialidad);

        sdVistasReporte.setSdVista1(sdVista1);
        sdVistasReporte.setSdVista2(sdVista2);

        return sdVistasReporte;
    }

    @Override
    public void insertarReporte(SitReporteParteI sitReporteParteI) {

        procesarReporteParteIRepository.insertarReporteParte1(sitReporteParteI);

    }

    @Override
    public SdPeriodoReporte getPeriodoReporte() {

        return this.sdPeriodoReporte;
    }

    @Override
    public SdUnidadMedica getUnidadMedica() {

        return this.sdUnidadMedica;
    }

    @Override
    public SitReporteParteI buscarReporteParte1(String cvePresupuestal, Integer clavePeriodo) {

        return procesarReporteParteIRepository.buscarReporteParte1(cvePresupuestal, clavePeriodo);
    }

    @Override
    public Boolean borrarReporteParte1(SitReporteParteI sitReporteParteI) {

        return procesarReporteParteIRepository.borrarReporteParte1(sitReporteParteI);
    }

}
