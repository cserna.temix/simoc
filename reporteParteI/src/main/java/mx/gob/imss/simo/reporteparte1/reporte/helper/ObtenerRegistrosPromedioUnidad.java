package mx.gob.imss.simo.reporteparte1.reporte.helper;

import java.time.LocalDate;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.aggregation.Accumulator;
import org.mongodb.morphia.aggregation.AggregationPipeline;
import org.mongodb.morphia.aggregation.Group;
import org.mongodb.morphia.aggregation.Projection;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mongodb.AggregationOptions;

import mx.gob.imss.simo.reporteparte1.common.core.MongoManager;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumColCex;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumOpMongo;
import mx.gob.imss.simo.reporteparte1.common.model.SitConsultaExterna;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdNumEncabezados;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ConsultarReportesParteIRepository;

@Component
@Scope("prototype")
public class ObtenerRegistrosPromedioUnidad extends Thread {

    Logger logger = Logger.getLogger(ObtenerRegistrosPromedioUnidad.class);

    private String cvePresupeuestal;
    private Integer promedioEncabezados;

    @Autowired
    private transient ConsultarReportesParteIRepository consultaReportesRepository;
    @Autowired
    private ConsultarReportesParteIRepository consultarReportesParteIRepository;

    public ObtenerRegistrosPromedioUnidad(String cvePresupeuestal) {

        super();
        this.cvePresupeuestal = cvePresupeuestal;
    }

    @Override
    public void run() {

        promedioEncabezados = consultarNumEncabezadosPromUnidad(cvePresupeuestal);

    }

    public Integer consultarNumEncabezadosPromUnidad(String unidadMedica) {

        LocalDate hoy = LocalDate.now();
        int dia = hoy.getDayOfMonth();
        int mes = hoy.getMonthValue();
        int anio = hoy.getYear();
        if (dia > 25) {
            mes = mes + 1;
            if (mes > 12) {
                mes = 1;
            }
        }
        LocalDate fechaFinLd = LocalDate.of(anio, mes - 6, 25);
        LocalDate fechaInicialLd = LocalDate.of(anio - 1, mes, 26);

        Date fecInicial = DateUtils.getDate(fechaInicialLd.toString() + " 00:00:00", DateUtils.DB_FORMAT_DATETIME);
        Date fecFinal = DateUtils.getDate(fechaFinLd.toString() + " 01:00:00", DateUtils.DB_FORMAT_DATETIME);

        Double numEncabezados = consultarNumEncabezadosPromUnidad(unidadMedica, fecInicial, fecFinal);

        numEncabezados = (numEncabezados / 6) * 12;
        return numEncabezados.intValue();
    }

    public Double consultarNumEncabezadosPromUnidad(String unidadMedica, Date fecInicial, Date fecFinal) {

        Double total = 0.0;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SitConsultaExterna> q = dsEsp.createQuery(SitConsultaExterna.class);
            // MATCH
            q.filter(EnumColCex.CVE_PRESUPUESTAL.getRutaCompleta(), unidadMedica);
            q.and(

                    q.criteria(EnumColCex.FEC_ALTA_ENCABEZADO.getRutaCompleta()).greaterThanOrEq(fecInicial),
                    q.criteria(EnumColCex.FEC_ALTA_ENCABEZADO.getRutaCompleta()).lessThanOrEq(fecFinal));

            AggregationPipeline aggr = dsEsp.createAggregation(SitConsultaExterna.class);
            aggr.match(q);
            aggr.group("sdEncabezado.0", Group.grouping("total", new Accumulator(EnumOpMongo.O_SUM.getValor(), 1)));
            aggr.project(Projection.projection("_id").suppress(), Projection.projection("total"));

            Iterator<SdNumEncabezados> encabezados = aggr.aggregate(SdNumEncabezados.class,
                    AggregationOptions.builder().build());
            if (encabezados != null) {
                while (encabezados.hasNext()) {
                    SdNumEncabezados totalE = encabezados.next();
                    if (totalE != null) {
                        total = totalE.getTotal();
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error consultarNumEncabezadosPromUnidad:" + e);
        }
        return total;
    }

    public String getCvePresupeuestal() {

        return cvePresupeuestal;
    }

    public void setCvePresupeuestal(String cvePresupeuestal) {

        this.cvePresupeuestal = cvePresupeuestal;
    }

    public Integer getPromedioEncabezados() {

        return promedioEncabezados;
    }

    public void setPromedioEncabezados(Integer promedioEncabezados) {

        this.promedioEncabezados = promedioEncabezados;
    }

}
