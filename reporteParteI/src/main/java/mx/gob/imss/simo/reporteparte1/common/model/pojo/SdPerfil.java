/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 */
public class SdPerfil {

    private Integer cvePerfil;
    private String desPerfil;

    public SdPerfil() {

        super();
    }

    public SdPerfil(Integer cvePerfil, String desPerfil) {

        super();
        this.cvePerfil = cvePerfil;
        this.desPerfil = desPerfil;
    }

    /**
     * @return the cvePerfil
     */
    public Integer getCvePerfil() {

        return cvePerfil;
    }

    /**
     * @param cvePerfil
     *            the cvePerfil to set
     */
    public void setCvePerfil(Integer cvePerfil) {

        this.cvePerfil = cvePerfil;
    }

    /**
     * @return the desPerfil
     */
    public String getDesPerfil() {

        return desPerfil;
    }

    /**
     * @param desPerfil
     *            the desPerfil to set
     */
    public void setDesPerfil(String desPerfil) {

        this.desPerfil = desPerfil;
    }

}
