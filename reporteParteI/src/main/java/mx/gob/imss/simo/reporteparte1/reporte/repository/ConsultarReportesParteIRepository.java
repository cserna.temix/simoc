package mx.gob.imss.simo.reporteparte1.reporte.repository;

import java.util.Date;

import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.common.model.SitControlCapturaCE;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;

public interface ConsultarReportesParteIRepository {

    SitReporteParteI buscarSitReportePorPeriodoUM(String unidadMedica, String periodo);

    SitControlCapturaCE buscarSitControlCapturaCEPorPeriodoUM(String unidadMedica, int periodo);

    SitColaReportes buscarSitColaReportesPorPeriodoUM(String unidadMedica, int periodo);

    void actualizarSitControlReporte(String selPeriodo, String cvePresupuestal, boolean indControl);

    Double consultarNumEncabezadosPromUnidad(String unidadMedica, Date fecInicial, Date fecFinal);
}
