package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdTipoCaptura;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;

@Data
@Document(collection = "SIT_PERIODO_OPERACION")
public class SitPeriodoOperacion {

    @Id
    private ObjectId id;
    private Integer cvePeriodo;
    private List<SdTipoCaptura> sdTipoCaptura;
    private List<SdUnidadMedica> sdUnidadMedica;
    private Integer indCierre;
    private Date fecCierre;

}
