package mx.gob.imss.simo.reporteparte1.common.repository.impl;

import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.aggregation.AggregationPipeline;
import org.mongodb.morphia.aggregation.Sort;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.stereotype.Repository;

import com.mongodb.AggregationOptions;

import mx.gob.imss.simo.reporteparte1.common.core.MongoManager;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumColaReporte;
import mx.gob.imss.simo.reporteparte1.common.model.SitControlCapturaCE;
import mx.gob.imss.simo.reporteparte1.common.model.SitUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.reporteparte1.common.repository.ReporteParte1CommonRepository;

@Repository("reporteParte1CommonRepository")
public class ReporteParte1CommonRepositoryImpl implements ReporteParte1CommonRepository {

    Logger logger = Logger.getLogger(ReporteParte1CommonRepositoryImpl.class);

    @Override
    public SitUnidadMedica buscarUnidadMedica(String cvePresupuestal) {

        SitUnidadMedica unidad = null;
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();
            unidad = ds.createQuery(SitUnidadMedica.class)
                    .filter(EnumColaReporte.CVE_PRESUPUESTAL.getValor(), cvePresupuestal).get();
        } catch (Exception e) {
            logger.error("Error buscarUnidadMedica:{}", e);
        }
        return unidad;

    }

    @Override
    public SicPeriodosImss obtenerPeriodImssPorFecha(Date fecha) {

        SicPeriodosImss periodo = null;
        Iterator<SicPeriodosImss> sicperiodos = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SicPeriodosImss> q = dsEsp.createQuery(SicPeriodosImss.class);
            // MATCH
            q.criteria("fecInicial").lessThanOrEq(fecha);
            AggregationPipeline aggr = dsEsp.createAggregation(SicPeriodosImss.class);
            aggr.match(q);
            aggr.sort(Sort.descending("cvePeriodo"));
            aggr.limit(1);

            sicperiodos = aggr.aggregate(SicPeriodosImss.class, AggregationOptions.builder().build());
            if (sicperiodos != null && sicperiodos.hasNext()) {
                periodo = sicperiodos.next();
            }
        } catch (Exception e) {
            logger.error("Error obtenerPeriodImssPorFecha:{}", e);
        }

        return periodo;

    }

    @Override
    public SitControlCapturaCE actualizarControlCapturaCE(SitControlCapturaCE controlCapturaCe) {

        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();

            Query<SitControlCapturaCE> updateQuery = ds.createQuery(SitControlCapturaCE.class)
                    .filter("sdUnidadMedica.cvePresupuestal", controlCapturaCe.getSdUnidadMedica().getCvePresupuestal())
                    .filter("cvePeriodo", controlCapturaCe.getCvePeriodo());

            UpdateOperations<SitControlCapturaCE> ops = ds.createUpdateOperations(SitControlCapturaCE.class)
                    .set("indControl", 0).set("fecActualizaControl", new Date())
                    .set("sdUnidadMedica.desUnidadMedica", controlCapturaCe.getSdUnidadMedica().getDesUnidadMedica());

            controlCapturaCe = ds.findAndModify(updateQuery, ops, false, true);

        } catch (Exception e) {
            logger.error("Error actualizarControlCapturaCE:{}", e);
        }

        return controlCapturaCe;
    }

}
