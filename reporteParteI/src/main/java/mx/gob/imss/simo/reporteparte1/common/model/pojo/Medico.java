/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

import java.util.List;

/**
 * @author francisco.rrios
 * 
 */
public class Medico {

	private List<String> nom;
	private List<String> apP;
	private List<String> apM;
	public List<String> getNom() {
		return nom;
	}
	public void setNom(List<String> nom) {
		this.nom = nom;
	}
	public List<String> getApP() {
		return apP;
	}
	public void setApP(List<String> apP) {
		this.apP = apP;
	}
	public List<String> getApM() {
		return apM;
	}
	public void setApM(List<String> apM) {
		this.apM = apM;
	}

	
	

}
