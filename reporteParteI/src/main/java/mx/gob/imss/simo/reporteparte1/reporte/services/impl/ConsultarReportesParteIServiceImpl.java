package mx.gob.imss.simo.reporteparte1.reporte.services.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jaspersoft.mongodb.connection.MongoDbConnection;

import lombok.Data;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.core.MongoManager;
import mx.gob.imss.simo.reporteparte1.common.dto.PeriodoDTO;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumEstatusReporteVista;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumReporteDescarga;
import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.common.model.SitControlCapturaCE;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.reporteparte1.common.repository.ReporteParte1CommonRepository;
import mx.gob.imss.simo.reporteparte1.reporte.helper.DateUtils;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ConsultarReportesParteIRepository;
import mx.gob.imss.simo.reporteparte1.reporte.services.ConsultarReportesParteIService;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

@Data
@Service("consultaReportesService")
public class ConsultarReportesParteIServiceImpl implements ConsultarReportesParteIService {

    Logger logger = Logger.getLogger(ConsultarReportesParteIServiceImpl.class);

    protected static final Integer IND_CON_CAMBIO_REGISTROS = 1;
    protected static final String URL_IMAGEN_ESTATUS_REPORTE_GENERANDO = "/img/estatus_reporte_generando.png";
    protected static final String URL_IMAGEN_ESTATUS_REPORTE = "/img/estatus_reporte.png";
    protected static final String URL_IMAGEN_ESTATUS_SIN_REPORTE = "/img/estatus_sin_reporte.png";
    protected static final String URL_IMAGEN_ESTATUS_VIGENTE = "/img/estatus_vigente.png";
    private static final String CHAR_ZERO = "0";
    private static final int INT_LONGITUD_ANIO = 4;
    private static final int INT_LONGITUD_MES = 2;
    private static final int MESES_TABLA_PERIODOS = 11;
    public static final Locale LOCALE = new Locale("es", "MX");

    private Integer maximoConsultas = 0;

    @Autowired
    private ConsultarReportesParteIRepository consultarReportesParteIRepository;

    @Autowired
    private ReporteParte1CommonRepository reporteParte1CommonRepository;

    public PeriodoDTO consultarReportesPorPeriodoUnidadMedica(PeriodoDTO periodo, String unidadMedica) {

        PeriodoDTO periodoEvaluado = null;
        SitReporteParteI sitReporte = consultarReportesParteIRepository.buscarSitReportePorPeriodoUM(unidadMedica,
                periodo.getPeriodo());

        /* Si el reporte existe, se valida que sea el vigente */
        if (sitReporte != null) {
            periodoEvaluado = validaReporteVigente(periodo, unidadMedica, sitReporte);
            periodoEvaluado.setTotalConsultasEspec(
                    sitReporte.getSdVistas().getSdVista1().getSdTotalUnidad().getTotalConsultas());
        } else {
            /* Se verifica si existe en la cola de reportes */
            SitColaReportes sitColaReportes = consultarReportesParteIRepository
                    .buscarSitColaReportesPorPeriodoUM(unidadMedica, Integer.parseInt(periodo.getPeriodo()));
            if (sitColaReportes != null) {
                if (sitColaReportes.getEstatus() == BaseConstants.ESTATUS_PROCESANDO) {
                    periodoEvaluado = new PeriodoDTO();
                    periodoEvaluado.setEstatus(EnumEstatusReporteVista.GENERANDO_REPORTE.getDescripcion());
                    periodoEvaluado.setRutaImagenEstatus(URL_IMAGEN_ESTATUS_REPORTE_GENERANDO);
                    periodoEvaluado.setEstatusCola(BaseConstants.ESTATUS_PROCESANDO);
                } else {
                    periodoEvaluado = new PeriodoDTO();
                    periodoEvaluado.setEstatus(EnumEstatusReporteVista.GENERANDO_REPORTE.getDescripcion());
                    periodoEvaluado.setRutaImagenEstatus(URL_IMAGEN_ESTATUS_REPORTE_GENERANDO);
                    periodoEvaluado.setEstatusCola(BaseConstants.ESTATUS_PENDIENTE);
                }
            } else {
                periodoEvaluado = new PeriodoDTO();
                periodoEvaluado.setEstatus(EnumEstatusReporteVista.SIN_GENERAR.getDescripcion());
                periodoEvaluado.setRutaImagenEstatus(URL_IMAGEN_ESTATUS_SIN_REPORTE);
            }
        }
        return periodoEvaluado;
    }

    /**
     * @param periodo
     * @param unidadMedica
     * @param sitReporte
     * @return
     */
    private PeriodoDTO validaReporteVigente(PeriodoDTO periodo, String unidadMedica, SitReporteParteI sitReporte) {

        PeriodoDTO periodoEvaluado;
        SitControlCapturaCE sitControl = consultarReportesParteIRepository
                .buscarSitControlCapturaCEPorPeriodoUM(unidadMedica, Integer.parseInt(periodo.getPeriodo()));
        if (sitControl != null && sitControl.getIndControl() != IND_CON_CAMBIO_REGISTROS) {
            periodoEvaluado = new PeriodoDTO();
            periodoEvaluado.setEstatus(EnumEstatusReporteVista.REPORTE_VIGENTE.getDescripcion());
            periodoEvaluado.setFechaGeneracionReporte(DateUtils.fechaAString(sitReporte.getFecGeneracion()));
            periodoEvaluado.setRutaImagenEstatus(URL_IMAGEN_ESTATUS_VIGENTE);
        } else if (sitControl != null && sitControl.getIndControl() == IND_CON_CAMBIO_REGISTROS) {
            /* Se verifica si existe en la cola de reportes */
            SitColaReportes sitColaReportes = consultarReportesParteIRepository
                    .buscarSitColaReportesPorPeriodoUM(unidadMedica, Integer.parseInt(periodo.getPeriodo()));
            if (sitColaReportes != null) {
                if (sitColaReportes.getEstatus() == BaseConstants.ESTATUS_PROCESANDO) {
                    periodoEvaluado = new PeriodoDTO();
                    periodoEvaluado.setEstatus(EnumEstatusReporteVista.GENERANDO_REPORTE.getDescripcion());
                    periodoEvaluado.setRutaImagenEstatus(URL_IMAGEN_ESTATUS_REPORTE_GENERANDO);
                    periodoEvaluado.setEstatusCola(BaseConstants.ESTATUS_PROCESANDO);
                } else {
                    periodoEvaluado = new PeriodoDTO();
                    periodoEvaluado.setEstatus(EnumEstatusReporteVista.GENERANDO_REPORTE.getDescripcion());
                    periodoEvaluado.setRutaImagenEstatus(URL_IMAGEN_ESTATUS_REPORTE_GENERANDO);
                    periodoEvaluado.setEstatusCola(BaseConstants.ESTATUS_PENDIENTE);
                }
            } else {
                periodoEvaluado = new PeriodoDTO();
                periodoEvaluado.setEstatus(EnumEstatusReporteVista.REPORTE.getDescripcion());
                periodoEvaluado.setFechaGeneracionReporte(DateUtils.fechaAString(sitReporte.getFecGeneracion()));
                periodoEvaluado.setRutaImagenEstatus(URL_IMAGEN_ESTATUS_REPORTE);
            }
        } else {
            periodoEvaluado = new PeriodoDTO();
            periodoEvaluado.setEstatus(EnumEstatusReporteVista.SIN_GENERAR.getDescripcion());
            periodoEvaluado.setRutaImagenEstatus(URL_IMAGEN_ESTATUS_SIN_REPORTE);
        }
        return periodoEvaluado;
    }

    public Date buscarPeriodoImssPorFecha(Date fechaActual) {

        SicPeriodosImss sicPeriodoImss = reporteParte1CommonRepository.obtenerPeriodImssPorFecha(fechaActual);
        return sicPeriodoImss != null ? sicPeriodoImss.getFecInicial() : null;
    }

    public List<PeriodoDTO> generarPeriodos(String cvePresupuestal) throws Exception {

        return obtenerMesesPrevios(DateUtils.crearFecha(), MESES_TABLA_PERIODOS, cvePresupuestal);
    }

    protected PeriodoDTO generaPeriodoDTO(int anio, int mes, String cvePresupuestal) {

        PeriodoDTO periodo = null;
        try {
            periodo = new PeriodoDTO();
            periodo.setMes(StringUtils.capitalize(new DateFormatSymbols(LOCALE).getMonths()[mes - 1]));
            periodo.setPeriodo(StringUtils.leftPad(Integer.toString(anio), INT_LONGITUD_ANIO, CHAR_ZERO)
                    + StringUtils.leftPad(Integer.toString(mes), INT_LONGITUD_MES, CHAR_ZERO));
            PeriodoDTO periodoComplemento = consultarReportesPorPeriodoUnidadMedica(periodo, cvePresupuestal);
            if (periodoComplemento != null) {
                periodo.setEstatus(periodoComplemento.getEstatus());
                periodo.setRutaImagenEstatus(periodoComplemento.getRutaImagenEstatus());
                periodo.setEstatusCola(periodoComplemento.getEstatusCola());
                periodo.setTotalConsultasEspec(periodoComplemento.getTotalConsultasEspec());
                if (periodoComplemento.getFechaGeneracionReporte() != null) {
                    periodo.setFechaGeneracionReporte(periodoComplemento.getFechaGeneracionReporte());
                }
            } else {
                logger.debug("El periodo complemento: " + periodo.getPeriodo() + " regres� null");
            }

        } catch (Exception e) {
            logger.debug("Error en el c�lculo de los periodos Reporte Parte I :" + e);
        }
        return periodo;
    }

    protected List<PeriodoDTO> obtenerMesesPrevios(Date fecha, int numeroMesesAtras, String cvePresupuestal) {

        List<PeriodoDTO> listaPeriodos = new ArrayList<>();
        Calendar calendar = Calendar.getInstance(LOCALE);
        calendar.setTime(fecha);

        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        int incremento = dia > 25 ? 2 : 1;
        int anio = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH) + incremento;
        maximoConsultas = 0;
        for (int i = 0; i <= numeroMesesAtras; i++) {
            PeriodoDTO periodoAux = generaPeriodoDTO(anio, mes, cvePresupuestal);
            if (periodoAux != null && periodoAux.getTotalConsultasEspec() != null
                    && periodoAux.getTotalConsultasEspec() > maximoConsultas) {
                maximoConsultas = periodoAux.getTotalConsultasEspec();
            }
            listaPeriodos.add(periodoAux);

            mes -= 1;
            if (mes == Calendar.JANUARY) {
                anio--;
                mes = Calendar.DECEMBER + 1;
            }
        }
        maximoConsultas = maximoConsultas + (maximoConsultas / 2);
        return listaPeriodos;
    }

    @Override
    public void actualizarSitControlReporte(String selPeriodo, String cvePresupuestal, boolean indControl)
            throws Exception {

        consultarReportesParteIRepository.actualizarSitControlReporte(selPeriodo, cvePresupuestal, indControl);
    }

    @Override
    public StreamedContent descargarReporte(String selPeriodo, ObjetosEnSesionBean objetosSs) throws Exception {

        StreamedContent streamedContent = null;
        if (selPeriodo != null && !selPeriodo.isEmpty()) {

            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            final ServletContext servletContext = (ServletContext) context.getContext();
            final Map<String, Object> model = new HashMap<>();

            final String logoImss = context.getRealPath(File.separator + "img" + File.separator + "imss_header.png");
            final String rutaSubReporte = context
                    .getRealPath(File.separator + "WEB-INF" + File.separator + "reporteParteI" + File.separator);

            MongoDbConnection connection = MongoManager.INSTANCE.getMongoDbConnection();

            model.put(EnumReporteDescarga.CVE_PERIODO.getValor(), Integer.valueOf(selPeriodo));
            model.put(EnumReporteDescarga.LOGO_IMSS.getValor(), logoImss);
            model.put(EnumReporteDescarga.CVE_PRESUPUESTAL.getValor(),
                    objetosSs.getDatosUsuario().getCvePresupuestal());
            model.put(EnumReporteDescarga.PERFIL.getValor(), objetosSs.getDatosUsuario().getPerfil());
            model.put(EnumReporteDescarga.SUBREPORT_DIR.getValor(), rutaSubReporte);

            String jasperPathReport = servletContext
                    .getRealPath(EnumReporteDescarga.PATH_REPORTE_PARTEI.getValor() + "reporteParteI.jasper");

            JasperPrint reportePrint = JasperFillManager.fillReport(jasperPathReport, model, connection);

            final ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            SimpleOutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(arrayOutputStream);
            SimpleExporterInput exporterInput = new SimpleExporterInput(reportePrint);

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(exporterInput);
            exporter.setExporterOutput(exporterOutput);
            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setDetectCellType(true);
            configuration.setCollapseRowSpan(false);
            exporter.setConfiguration(configuration);
            exporter.exportReport();

            arrayOutputStream.flush();

            File tempFile = Files.createTempFile(null, null).toFile();
            arrayOutputStream.writeTo(new FileOutputStream(tempFile));
            InputStream inputStream = new FileInputStream(tempFile);
            streamedContent = new DefaultStreamedContent(inputStream,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                    "reporte_parte_1_" + selPeriodo + ".xlsx");

        } else {
            // Verificar mensaje a mostrar para la selecci�n del periodo.

        }
        return streamedContent;
    }

    @Override
    public Integer consultarNumEncabezadosPromUnidad(String unidadMedica) {

        LocalDate hoy = LocalDate.now();
        int dia = hoy.getDayOfMonth();
        int mes = hoy.getMonthValue();
        int anio = hoy.getYear();
        if (dia > 25) {
            mes = mes + 1;
            if (mes > 12) {
                mes = 1;
            }
        }
        LocalDate fechaFinLd = LocalDate.of(anio, mes - 6, 25);
        LocalDate fechaInicialLd = LocalDate.of(anio - 1, mes, 26);

        Date fecInicial = DateUtils.getDate(fechaInicialLd.toString() + " 00:00:00", DateUtils.DB_FORMAT_DATETIME);
        Date fecFinal = DateUtils.getDate(fechaFinLd.toString() + " 01:00:00", DateUtils.DB_FORMAT_DATETIME);

        Double numEncabezados = consultarReportesParteIRepository.consultarNumEncabezadosPromUnidad(unidadMedica,
                fecInicial, fecFinal);
        numEncabezados = (numEncabezados / 6) * 12;
        return numEncabezados.intValue();
    }

    public Integer getMaximoConsultas() {

        return maximoConsultas;
    }

}
