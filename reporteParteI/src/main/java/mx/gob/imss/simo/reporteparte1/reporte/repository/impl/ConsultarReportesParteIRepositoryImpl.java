package mx.gob.imss.simo.reporteparte1.reporte.repository.impl;

import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.aggregation.Accumulator;
import org.mongodb.morphia.aggregation.AggregationPipeline;
import org.mongodb.morphia.aggregation.Group;
import org.mongodb.morphia.aggregation.Projection;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.stereotype.Repository;

import com.mongodb.AggregationOptions;

import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.core.MongoManager;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumColCex;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumColaReporte;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumOpMongo;
import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.common.model.SitConsultaExterna;
import mx.gob.imss.simo.reporteparte1.common.model.SitControlCapturaCE;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdNumEncabezados;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ConsultarReportesParteIRepository;

@Repository("consultaReportesRepository")
public class ConsultarReportesParteIRepositoryImpl implements ConsultarReportesParteIRepository {

    protected static final int IND_ACTUAL = 0;
    protected static final int IND_NO_ACTUAL = 1;
    Logger logger = Logger.getLogger(ConsultarReportesParteIRepositoryImpl.class);

    public SitReporteParteI buscarSitReportePorPeriodoUM(String unidadMedica, String periodo) {

        SitReporteParteI sitReporte = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            sitReporte = dsEsp.createQuery(SitReporteParteI.class)
                    .filter("sdUnidadMedica.cvePresupuestal", unidadMedica)
                    .filter("sdPeriodoReporte.cvePeriodo", Integer.valueOf(periodo)).get();
        } catch (Exception e) {
            logger.error("Error buscarSitReportePorPeriodoUM:" + e);
        }
        return sitReporte;
    }

    public SitControlCapturaCE buscarSitControlCapturaCEPorPeriodoUM(String unidadMedica, int periodo) {

        SitControlCapturaCE sitControlCaptura = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            sitControlCaptura = dsEsp.createQuery(SitControlCapturaCE.class)
                    .filter("sdUnidadMedica.cvePresupuestal", unidadMedica)
                    .filter(EnumColaReporte.CVE_PERIODO.getValor(), periodo).get();
        } catch (Exception e) {
            logger.error("Error buscarSitControlCapturaCEPorPeriodoUM:" + e);
        }
        return sitControlCaptura;
    }

    @Override
    public SitColaReportes buscarSitColaReportesPorPeriodoUM(String unidadMedica, int periodo) {

        SitColaReportes sitColaReportePendiente = null;
        SitColaReportes sitColaReporteProcesando = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            sitColaReportePendiente = dsEsp.createQuery(SitColaReportes.class)
                    .filter(EnumColaReporte.CVE_PRESUPUESTAL.getValor(), unidadMedica)
                    .filter(EnumColaReporte.CVE_PERIODO.getValor(), Integer.valueOf(periodo))
                    .filter("estatus", BaseConstants.ESTATUS_PENDIENTE).get();
            Datastore dsEspRp = MongoManager.INSTANCE.getDatastore();
            sitColaReporteProcesando = dsEspRp.createQuery(SitColaReportes.class)
                    .filter("cvePresupuestal", unidadMedica).filter("cvePeriodo", Integer.valueOf(periodo))
                    .filter("estatus", BaseConstants.ESTATUS_PROCESANDO).get();
        } catch (Exception e) {
            logger.error("Error buscarSitColaReportesPorPeriodoUM:" + e);
        }
        return sitColaReportePendiente != null ? sitColaReportePendiente : sitColaReporteProcesando;
    }

    @Override
    public void actualizarSitControlReporte(String selPeriodo, String cvePresupuestal, boolean indControl) {

        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();

            Query<SitControlCapturaCE> updateQuery = ds.createQuery(SitControlCapturaCE.class)
                    .filter("cvePresupuestal", cvePresupuestal).filter("cvePeriodo", Integer.valueOf(selPeriodo));

            UpdateOperations<SitControlCapturaCE> ops = ds.createUpdateOperations(SitControlCapturaCE.class)
                    .set("indControl", indControl ? IND_ACTUAL : IND_NO_ACTUAL).set("fecFin", new Date());

            ds.findAndModify(updateQuery, ops, false, true);

        } catch (Exception e) {
            logger.error("Error actualizarSitControlReporte:" + e);
        }
    }

    @Override
    public Double consultarNumEncabezadosPromUnidad(String unidadMedica, Date fecInicial, Date fecFinal) {

        Double total = 0.0;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SitConsultaExterna> q = dsEsp.createQuery(SitConsultaExterna.class);
            // MATCH
            q.filter(EnumColCex.CVE_PRESUPUESTAL.getRutaCompleta(), unidadMedica);
            q.and(

                    q.criteria(EnumColCex.FEC_ALTA_ENCABEZADO.getRutaCompleta()).greaterThanOrEq(fecInicial),
                    q.criteria(EnumColCex.FEC_ALTA_ENCABEZADO.getRutaCompleta()).lessThanOrEq(fecFinal));

            AggregationPipeline aggr = dsEsp.createAggregation(SitConsultaExterna.class);
            aggr.match(q);
            aggr.group("sdEncabezado.0", Group.grouping("total", new Accumulator(EnumOpMongo.O_SUM.getValor(), 1)));
            aggr.project(Projection.projection("_id").suppress(), Projection.projection("total"));

            Iterator<SdNumEncabezados> encabezados = aggr.aggregate(SdNumEncabezados.class,
                    AggregationOptions.builder().build());
            if (encabezados != null) {
                while (encabezados.hasNext()) {
                    SdNumEncabezados totalE = encabezados.next();
                    if (totalE != null) {
                        total = totalE.getTotal();
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error consultarNumEncabezadosPromUnidad:" + e);
        }
        return total;
    }

}
