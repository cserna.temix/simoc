package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Data;

@Data
@Entity(value = "SIT_COLA_REPORTES")
public class SitColaReportes {

    @Id
    ObjectId id;
    String cvePresupuestal;
    String desUnidad;
    String className;
    Integer cvePeriodo;
    Integer estatus;
    Date fecInicio;
    Date fecFin;
    Integer clasificacionVolumen;
    String nombreThread;
    Long idThread;

}
