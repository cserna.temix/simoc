package mx.gob.imss.simo.reporteparte1.common.model.catalogos;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("SIC_SUBROL")
public class SicSubRol {

	@Id
	private ObjectId id;
	private Integer cveSubrol;
	private String desSubrol;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Integer getCveSubrol() {
		return cveSubrol;
	}

	public void setCveSubrol(Integer cveSubrol) {
		this.cveSubrol = cveSubrol;
	}

	public String getDesSubrol() {
		return desSubrol;
	}

	public void setDesSubrol(String desSubrol) {
		this.desSubrol = desSubrol;
	}

}
