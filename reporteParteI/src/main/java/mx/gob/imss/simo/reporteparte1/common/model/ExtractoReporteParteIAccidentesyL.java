package mx.gob.imss.simo.reporteparte1.common.model;

import lombok.Data;

@Data
public class ExtractoReporteParteIAccidentesyL {

	Integer amTiempoTraDias;
	Integer amTotCons;
	Integer am1Vez;
	Integer amNoOtorgadas;
	Integer amNumVisitas;
	Integer amCitas;
	Integer amCitasCumplidas;
	Integer peEnUnidad;
	Integer peOtraUnidad;
	Integer peAltasEspeciales;
	Integer incExpedidas;
	Integer incDias;
	Double incProm;	
	Integer recNum;
	Double recPorcentajeAten;

}
