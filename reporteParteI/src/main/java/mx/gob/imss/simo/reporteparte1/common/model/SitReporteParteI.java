package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Data;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdPeriodoReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;

@Data
@Entity("SIT_REPORTE_PARTE_I")
public class SitReporteParteI {
	
	@Id
	private ObjectId id;
	private SdUnidadMedica sdUnidadMedica;
	private SdPeriodoReporte sdPeriodoReporte;
	private Date fecGeneracion;
	private Integer cveEstatusReporte;
	private SdVistas sdVistas;
	
	
}
