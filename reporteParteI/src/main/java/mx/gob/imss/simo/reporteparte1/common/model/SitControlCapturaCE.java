package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Data;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;

@Data
@Entity(value = "SIT_CONTROL_CAPTURA_CE")
public class SitControlCapturaCE {

    @Id
    ObjectId id;
    Integer cvePeriodo;
    SdUnidadMedica sdUnidadMedica;
    Integer indControl;
    Date fecActualizaControl;

}
