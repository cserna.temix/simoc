package mx.gob.imss.simo.reporteparte1.common.model.catalogos;

import java.util.List;
import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("SIC_ROL")
public class SicRol {

	@Id
	private ObjectId id;
	private String cveRol;
	private String desRol;
	private Date fecBaja;
	List<SicSubRol> sdSubRol;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getCveRol() {
		return cveRol;
	}

	public void setCveRol(String cveRol) {
		this.cveRol = cveRol;
	}

	public String getDesRol() {
		return desRol;
	}

	public void setDesRol(String desRol) {
		this.desRol = desRol;
	}

	public Date getFecBaja() {
		return fecBaja;
	}

	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	public List<SicSubRol> getSdSubRol() {
		return sdSubRol;
	}

	public void setSdSubRol(List<SicSubRol> sdSubRol) {
		this.sdSubRol = sdSubRol;
	}

	@Override
	public String toString() {
		return "SicRol [id=" + id + ", cveRol=" + cveRol + ", desRol=" + desRol
				+ ", fecBaja=" + fecBaja + ", sdSubRol=" + sdSubRol + "]";
	}

}
