/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicRol;

/**
 * @author francisco.rrios
 */
@Data
@Document(collection = "SIT_PERSONAL_OPERATIVO")
public class SitPersonalOperativo {

    @Id
    private ObjectId id;
    private String refCuentaActiveD;
    private String cveMatricula;
    private String refNombre;
    private String refApellidoPaterno;
    private String refApellidoMaterno;
    private List<SicRol> sdRol;
    private List<SitUnidadMedica> sdUnidadMedica;

    private String refNumTelefono;
    private String refNumExtension;
    private String refEmail;
    private Boolean indActivo;
    private int indBloqueado;
    private Date fecBaja;
    private Integer caIntentos;

}
