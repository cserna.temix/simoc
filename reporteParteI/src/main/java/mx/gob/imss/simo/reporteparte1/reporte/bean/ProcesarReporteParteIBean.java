package mx.gob.imss.simo.reporteparte1.reporte.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import lombok.Data;
import mx.gob.imss.simo.model.DatosUsuario;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.core.ReporteParteIConfiguration;
import mx.gob.imss.simo.reporteparte1.common.dto.PeriodoDTO;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumEstatusReporteVista;
import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.common.model.SitUnidadMedica;
import mx.gob.imss.simo.reporteparte1.reporte.services.ColaDeReportesService;
import mx.gob.imss.simo.reporteparte1.reporte.services.ConsultarReportesParteIService;
import mx.gob.imss.simo.reporteparte1.reporte.services.ProcesarReporteParteIService;

@Data
@ManagedBean(name = "procesarReporteParte1Bean")
@ViewScoped
public class ProcesarReporteParteIBean extends SpringBeanAutowiringSupport implements Serializable {

    private static final long serialVersionUID = -8280680946042064747L;
    static final Logger logger = LoggerFactory.getLogger(ProcesarReporteParteIBean.class);

    private static final String ID_FOCO_HIDDEN = "idHiddenFoco";
    private static final String IDIOMA = "idioma_locale";
    private static final String PAIS = "pais_locale";
    private static final String MESSAGES_PROPERTIES = "bundle.messages";
    private static final String BUNDLE_LOCALE = "bundle.locale";
    protected static final Locale LOCALE = new Locale(ResourceBundle.getBundle(BUNDLE_LOCALE).getString(IDIOMA),
            ResourceBundle.getBundle(BUNDLE_LOCALE).getString(PAIS));
    protected static final String MSG_SOLICITUD_EN_PROCESO = "msg_solicitud_en_proceso";
    protected static final String MSG_SOLICITUD_REPORTE_CANCELADO = "msg_solicitud_reporte_cancelado";
    protected static final String MSG_SOLICITUD_REPORTE_NO_SER_CANCELADO = "msg_solicitud_cancelar_reporte";
    protected static final String MSG_SOLICITUD_VIGENTE = "msg_solicitud_vigente";
    protected static final String MSG_UNIDAD_MIN = " mins";
    protected static final int INT_SELECCION_DEFAULT = 0;
    private StreamedContent file;
    private String tieneFoco;
    private ResourceBundle mensajesReporteApp;
    private String selPeriodo_;
    private String selPeriodo;
    private boolean deshabilitaDescargar;
    private boolean deshabilitaGenerar;
    private boolean habilitaCancelar;
    private boolean mostrarMensajeGenerando;
    private boolean mostrarMensajeVigente;
    private boolean mostrarModalNoVigente;
    private boolean mostrarModalCancelar;
    private boolean ejecutarBotonDescarga;

    private List<PeriodoDTO> periodosDisponibles = new ArrayList<>();
    private PeriodoDTO periodoSeleccionadoDTO;

    @ManagedProperty("#{objetosSs}")
    private ObjetosEnSesionBean objetosSs;

    @Autowired
    private transient ProcesarReporteParteIService procesarReporteParteIService;

    @Autowired
    private transient ColaDeReportesService colaDeReporteService;

    @Autowired
    private transient ConsultarReportesParteIService consultaReportesService;

    @PostConstruct
    public void init() {

        tieneFoco = ID_FOCO_HIDDEN;
        mensajesReporteApp = obtenerMensajesApp();
        try {

            periodosDisponibles = consultaReportesService.generarPeriodos(obtenerDatosUsuario().getCvePresupuestal());
            if (objetosSs.getMaxRegistrosCExEspc() == null || objetosSs.getMaxRegistrosCExEspc() == 0) {
                objetosSs.setMaxRegistrosCExEspc(consultaReportesService.getMaximoConsultas());
            }

            Collections.reverse(periodosDisponibles);
            if (periodoSeleccionadoDTO != null) {
                cargaSelectItemSeleccionado();
                validaBotonesHabilitados();
            } else {
                selPeriodo_ = periodosDisponibles.get(INT_SELECCION_DEFAULT).getPeriodo();
                selPeriodo = selPeriodo_;
                validaBotonesHabilitados();
            }

        } catch (Exception e) {
            logger.error("init:{}", e);
        }
    }

    public static ResourceBundle obtenerMensajesCitaMedicaReferencia() {

        return ResourceBundle.getBundle(BUNDLE_LOCALE, LOCALE);
    }

    public static ResourceBundle obtenerMensajesApp() {

        return ResourceBundle.getBundle(MESSAGES_PROPERTIES);
    }

    protected DatosUsuario obtenerDatosUsuario() {

        return objetosSs.getDatosUsuario();
    }

    protected void limpiarBanderas() {

        deshabilitaDescargar = Boolean.FALSE;
        deshabilitaGenerar = Boolean.FALSE;
        habilitaCancelar = Boolean.FALSE;
        mostrarModalNoVigente = Boolean.FALSE;
        mostrarMensajeVigente = Boolean.FALSE;
    }

    protected void cargaSelectItemSeleccionado() {

        selPeriodo_ = periodoSeleccionadoDTO.getPeriodo();
    }

    public void validaBotonesHabilitados() {

        selPeriodo = selPeriodo_ != null ? selPeriodo_ : StringUtils.EMPTY;
        limpiarBanderas();
        for (PeriodoDTO periodo : periodosDisponibles) {
            if (periodo.getPeriodo().equals(selPeriodo)) {
                periodoSeleccionadoDTO = periodo;
                if (periodo.getEstatus().equals(EnumEstatusReporteVista.GENERANDO_REPORTE.getDescripcion())) {
                    validarBotonesPorEstatus(EnumEstatusReporteVista.GENERANDO_REPORTE);
                }
                if (periodo.getEstatus().equals(EnumEstatusReporteVista.REPORTE.getDescripcion())) {
                    validarBotonesPorEstatus(EnumEstatusReporteVista.REPORTE);
                }
                if (periodo.getEstatus().equals(EnumEstatusReporteVista.REPORTE_VIGENTE.getDescripcion())) {
                    validarBotonesPorEstatus(EnumEstatusReporteVista.REPORTE_VIGENTE);
                }
                if (periodo.getEstatus().equals(EnumEstatusReporteVista.SIN_GENERAR.getDescripcion())) {
                    validarBotonesPorEstatus(EnumEstatusReporteVista.SIN_GENERAR);
                }
            }
        }
    }

    protected void validarBotonesPorEstatus(EnumEstatusReporteVista enumRv) {

        ejecutarBotonDescarga = Boolean.FALSE;
        switch (enumRv) {
            case GENERANDO_REPORTE:
                deshabilitaDescargar = Boolean.TRUE;
                deshabilitaGenerar = Boolean.TRUE;
                habilitaCancelar = Boolean.TRUE;
                break;
            case REPORTE:
                mostrarModalNoVigente = Boolean.TRUE;
                break;
            case REPORTE_VIGENTE:
                mostrarMensajeVigente = Boolean.TRUE;
                ejecutarBotonDescarga = Boolean.TRUE;
                break;
            case SIN_GENERAR:
                deshabilitaDescargar = Boolean.TRUE;
                break;
        }
    }

    public StreamedContent getFile() {

        try {
            setFile(consultaReportesService.descargarReporte(selPeriodo, objetosSs));
        } catch (Exception e) {
            logger.error("Error{}", e);
        }
        return file;
    }

    public void validarDescarga() {

        if (!mostrarModalNoVigente) {
            logger.error("Es el vigente");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dlg_confirmar').show();");
        }
    }

    public void actualizarSitControlReporte() throws Exception {

        setEjecutarBotonDescarga(Boolean.TRUE);
        cerrarModal();
    }

    public void abrirModalCancelar() {

        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg_cancelar').show();");
    }

    public void cancelarSitColaReporte() throws Exception {

        SitColaReportes colaReporte = new SitColaReportes();
        colaReporte.setCvePeriodo(Integer.parseInt(selPeriodo));
        colaReporte.setCvePresupuestal(obtenerDatosUsuario().getCvePresupuestal());
        colaReporte.setEstatus(BaseConstants.ESTATUS_PENDIENTE);
        colaDeReporteService.actualizarReporteEnCola(colaReporte, BaseConstants.ESTATUS_CANCELADO);
        mostrarMensaje(mensajesReporteApp.getString(MSG_SOLICITUD_REPORTE_CANCELADO));
    }

    public void cerrarModal() {

        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg_confirmar').hide();");
    }

    public void cerrarModalCancelar() {

        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg_cancelar').hide();");
    }

    public void validarCancelar() {

        mostrarModalCancelar = Boolean.TRUE;
        if (periodoSeleccionadoDTO.getEstatus() == EnumEstatusReporteVista.GENERANDO_REPORTE.getDescripcion()) {
            if (periodoSeleccionadoDTO.getEstatusCola() == BaseConstants.ESTATUS_PROCESANDO) {
                cerrarModalCancelar();
                mostrarMensaje(mensajesReporteApp.getString(MSG_SOLICITUD_REPORTE_NO_SER_CANCELADO));
            } else if (periodoSeleccionadoDTO.getEstatusCola() == BaseConstants.ESTATUS_PENDIENTE) {
                try {
                    cancelarSitColaReporte();
                    init();
                } catch (Exception e) {
                    logger.error("Error{}", e);
                }
            }
        }
    }

    private String calculaTiempoEspera() {

        logger.debug("calculaTiempoEspera objetosSs.getMaxRegistrosCExEspc():{}", objetosSs.getMaxRegistrosCExEspc());
        System.out.println(
                "calculaTiempoEspera objetosSs.getMaxRegistrosCExEspc():" + objetosSs.getMaxRegistrosCExEspc());
        return String.valueOf(colaDeReporteService.tiempoProcesarReporte(objetosSs.getMaxRegistrosCExEspc()))
                + MSG_UNIDAD_MIN;
    }

    protected void mostrarMensaje(String mensajeAMostrar) {

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, mensajeAMostrar, ""));
    }

    public void validaGenerarReporte() {

        if (selPeriodo != null && !selPeriodo.isEmpty()) {
            // El reporte no est� en cola, ni vigente.
            if (!mostrarMensajeGenerando && !mostrarMensajeVigente) {
                generarReporte();
                mostrarMensaje(mensajesReporteApp.getString(MSG_SOLICITUD_EN_PROCESO) + calculaTiempoEspera());
                init();
                return;
            }
            // El reporte est� vigente.
            if (mostrarMensajeVigente && !mostrarMensajeGenerando) {
                mostrarMensaje(mensajesReporteApp.getString(MSG_SOLICITUD_VIGENTE));
                return;
            }
            // El reporte est� en cola.
            if (mostrarMensajeGenerando && !mostrarMensajeVigente) {
                mostrarMensaje(
                        mensajesReporteApp.getString(MSG_SOLICITUD_EN_PROCESO) + calculaTiempoEspera() + " minuto");
                return;
            }
            // El reporte no est� vigente.
            if (mostrarModalNoVigente) {
                generarReporte();
                mostrarMensaje(mensajesReporteApp.getString(MSG_SOLICITUD_EN_PROCESO) + calculaTiempoEspera());
                init();
                return;
            }
        } else {
            // Verificar mensaje a mostrar para la selecci�n del periodo.
        }
    }

    protected void generarReporte() {

        if (selPeriodo != null && !selPeriodo.isEmpty()) {
            String cvePresupuestal = objetosSs.getDatosUsuario().getCvePresupuestal();
            Integer clavePeriodo = Integer.valueOf(selPeriodo);
            // Busca si existe en cola un reporte con estatus pendientes o en proceso con los mismos datos
            SitColaReportes reporteEnCola = colaDeReporteService.buscarReporteExistenteEnCola(cvePresupuestal,
                    clavePeriodo, BaseConstants.ESTATUS_PENDIENTE, null);
            SitUnidadMedica unidad = colaDeReporteService.buscarUnidadMedica(cvePresupuestal);
            if (reporteEnCola == null) {
                // PeriodoDTO periodo = buscarPeriodoSeleccionado(selPeriodo);
                Integer volumen = consultaReportesService.getMaximoConsultas();
                if (volumen == 0) {
                    volumen = ReporteParteIConfiguration.INSTANCE.getRegistrosPromedioDefault();
                }
                volumen = volumen / ReporteParteIConfiguration.INSTANCE.getRegistrsosTamanioUnidad();
                colaDeReporteService.insertarReporteEnCola(cvePresupuestal, unidad.getDesUnidadMedica(), clavePeriodo,
                        volumen);
            } else {
                if (reporteEnCola.getEstatus() == BaseConstants.ESTATUS_PENDIENTE) {
                    logger.info("Seguro que quiere cancelar");
                } else if (reporteEnCola.getEstatus() == BaseConstants.ESTATUS_PROCESANDO) {
                    logger.info("NO se puede cancelar cancelar");
                }
            }
        }

    }

    // private PeriodoDTO buscarPeriodoSeleccionado(String selPerido) {
    //
    // for (PeriodoDTO per : periodosDisponibles) {
    // if (per.getPeriodo().equals(selPerido)) {
    // return per;
    // }
    // }
    // return null;
    // }

    public void salir() throws Exception {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.FALSE);
        String urlInicio = objetosSs.getRutaPaginaInicio();
        FacesContext.getCurrentInstance().getExternalContext().redirect(urlInicio + BaseConstants.PAGINA_INICIO);
    }
}
