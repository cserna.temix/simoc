package mx.gob.imss.simo.reporteparte1.reporte.repository;

import java.util.Date;
import java.util.Iterator;

import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteIAccidentesyL;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicPeriodosImss;

public interface ProcesarReporteParteIRepository {


	/**
	 * M�todo para extraer informaci�n de la BD de Consulta Externa, para el Reporte Parte 1
	 * 	 * Sirve para generar diferentes consultas
	 * Tipo de Consulta
	 * 1- Especialidad
	 * 2- Urgencias 
	 * 3- Tococirug�a
	 * 4- Param�dicos 
	 * 
	 * @param cvePresupuestal
	 * @param fechaInicio
	 * @param fechaFin
	 * @param tipoConsulta
	 * @return Iterator<ExtractoReporteParteI> 
	 */
	public Iterator<ExtractoReporteParteI> extraerInfoMedicosReporteVista2(String cvePresupuestal, Date fechaInicio, Date fechaFin, Integer tipoConsulta);
	
	
	/**
	 * M�todo para extraer informaci�n de Accidentes y lesiones ,Violencias  de la BD de Consulta Externa, para el Reporte Parte 1 
	 * 
	 * @param cvePresupuestal
	 * @param fechaInicio
	 * @param fechaFin
	 * @return Iterator<ExtractoReporteParteIAccidentesyL> 
	 */
	public Iterator<ExtractoReporteParteIAccidentesyL> extraerInfoMedicosReporteAccidentesyLes(String cvePresupuestal, Date fechaInicio, Date fechaFin);
	
	
	/**
	 * M�todo para obtener un periodo IMSS
	 * @param clavePeriodo
	 * @return
	 */
	public SicPeriodosImss obtenerPeriodImss(Integer clavePeriodo);
	
	/**
	 * Realiza la inserci�n en la colecci�n SIT_REPORTE_PARTE_I
	 * @param sitReporteParteI
	 */
	public void insertarReporteParte1(SitReporteParteI sitReporteParteI);
	
	/**
	 * Realiza la busqueda de un reporte parte 1 en BD 
	 * @param cvePresupuestal
	 * @param clavePeriodo
	 * @return 
	 */
	public SitReporteParteI buscarReporteParte1(String cvePresupuestal, Integer clavePeriodo);
	
	/**
	 * Realiza el borrado en la BD de un reporte existente por Clave presupuestal y periodo
	 * @param cvePresupuestal
	 * @param clavePeriodo
	 * @return 
	 */
	public Boolean borrarReporteParte1(SitReporteParteI sitReporteParteI);
	
	
}
