package mx.gob.imss.simo.reporteparte1.reporte.repository.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.bson.Document;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.aggregation.Accumulator;
import org.mongodb.morphia.aggregation.AggregationPipeline;
import org.mongodb.morphia.aggregation.Group;
import org.mongodb.morphia.aggregation.Projection;
import org.mongodb.morphia.aggregation.Sort;
import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;

import com.mongodb.AggregationOptions;
import com.mongodb.WriteResult;
import com.mongodb.client.model.Aggregates;

import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.core.MongoManager;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumColCex;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumOpMongo;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteIAccidentesyL;
import mx.gob.imss.simo.reporteparte1.common.model.SitConsultaExterna;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.GroupMedico;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.Medico;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdMedico;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ProcesarReporteParteIRepository;

@Repository("procesarReporteParteIRepository")
public class ProcesarReporteParteIRepositoryImpl implements ProcesarReporteParteIRepository {

    Logger logger = Logger.getLogger(ProcesarReporteParteIRepositoryImpl.class);

    @Override
    public Iterator<ExtractoReporteParteI> extraerInfoMedicosReporteVista2(String cvePresupuestal, Date fechaInicio,
            Date fechaFin, Integer tipoConsulta) {

        Date fechaSumadaInicio = null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaInicio);
        calendar.add(Calendar.HOUR, -1);
        fechaSumadaInicio = calendar.getTime();

        Iterator<ExtractoReporteParteI> reporte = null;
        try {

            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            // Datastore dsEsp = MongoManager.INSTANCE.getDatastoreReplica();

            Query<SitConsultaExterna> q = dsEsp.createQuery(SitConsultaExterna.class);
            // MATCH
            q.filter(EnumColCex.CVE_PRESUPUESTAL.getRutaCompleta(), cvePresupuestal);
            q.and(

                    q.criteria(EnumColCex.FEC_ALTA_ENCABEZADO.getRutaCompleta()).greaterThanOrEq(fechaSumadaInicio),
                    q.criteria(EnumColCex.FEC_ALTA_ENCABEZADO.getRutaCompleta()).lessThanOrEq(fechaFin));

            switch (tipoConsulta) {
                case BaseConstants.TC_ESPECIALIDADES:
                    q.and(q.criteria(EnumColCex.NUM_TIPO_SERVICIO.getRutaCompleta()).notEqual(2),
                            q.criteria(EnumColCex.NUM_TIPO_SERVICIO.getRutaCompleta()).notEqual(3),
                            q.criteria(EnumColCex.NUM_TIPO_SERVICIO.getRutaCompleta()).notEqual(4));
                    break;
                case BaseConstants.TC_URGENCIAS:
                    q.and(q.criteria(EnumColCex.CVE_ESPECIALIDAD.getRutaCompleta()).contains("^50"));
                    break;
                case BaseConstants.TC_TOCOCIRUGIA:
                    q.and(q.criteria(EnumColCex.CVE_ESPECIALIDAD.getRutaCompleta()).contains("A600"));
                    break;
                case BaseConstants.TC_PARAMEDICOS:
                    q.and(q.criteria(EnumColCex.NUM_TIPO_SERVICIO.getRutaCompleta()).equal(4));
                    break;
                default:
                    logger.debug("Error al buscar el tipo de cpmsulta");
                    break;
            }

            AggregationPipeline aggr = dsEsp.createAggregation(SitConsultaExterna.class);
            aggr.match(q);
            aggr.unwind(EnumColCex.SD_ENCABEZADO.getValor());

            // PRIMER GROUP
            
            Map<String, Object> mapaCamposMedico = new HashMap<>();
            mapaCamposMedico.put(EnumColCex.P1_NOM.getValor(), EnumColCex.REF_NOMBRE.getRutaCompleta(Boolean.TRUE));
            mapaCamposMedico.put(EnumColCex.P1_APM.getValor(), EnumColCex.REF_APELLIDO_M.getRutaCompleta(Boolean.TRUE));
            mapaCamposMedico.put(EnumColCex.P1_APP.getValor(), EnumColCex.REF_APELLIDO_P.getRutaCompleta(Boolean.TRUE));

          
            
            switch (tipoConsulta) {
                case BaseConstants.TC_ESPECIALIDADES:
                    aggr.group(Group.id(
                            Group.grouping(EnumColCex.P1_MATRICULA.getValor(),
                                    EnumColCex.CVE_MATRICULA.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_NOM.getValor(), EnumColCex.REF_NOMBRE.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_APP.getValor(), EnumColCex.REF_APELLIDO_P.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_APM.getValor(), EnumColCex.REF_APELLIDO_M.getRutaCompleta()),
                            Group.grouping(EnumColCex.P1_ESP.getValor(),
                                    EnumColCex.CVE_ESPECIALIDAD.getRutaCompleta())
                    		
                    		),
                            //Group.grouping(EnumColCex.P1_CONTADOR.getValor(), new Accumulator("$sum",1)),
                    		
                    		Group.grouping(EnumColCex.P_MEDICO.getValor(),
                                    new Accumulator(EnumOpMongo.O_ADDTOSET.getValor(),
                                            new Document(mapaCamposMedico))),
                                            
                            Group.grouping(EnumColCex.P1_DES_ESP.getValor(),
                                    new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                            EnumColCex.DES_ESPECIALIDAD.getRutaCompleta())),
                            Group.grouping(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor(),
                                    new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                            EnumColCex.NOM_UNIDAD_MEDICA.getRutaCompleta())),

                            // Obtener información de atenciones médicas
                            Group.grouping(EnumColCex.P1_TIEMPO_TRA_HORAS.getValor(),
                                    new Accumulator(EnumOpMongo.O_PUSH.getValor(),
                                            new Document(EnumColCex.P1_HORAS.getValor(),
                                                    EnumColCex.NUM_HORAS_TRABAJADAS.getRutaCompleta(Boolean.TRUE)))),

                            agruparCond(EnumColCex.P1_TIEMPO_TRA_DIAS.getValor(),
                                    EnumColCex.NUM_CITAS_NO_CUMPLIDAS.getRutaCompleta(Boolean.TRUE),
                                    EnumOpMongo.C_GTE.getValor(), 0),

                            Group.grouping(EnumColCex.P1_NO_OTOR.getValor(),
                                    new Accumulator(EnumOpMongo.O_SUM.getValor(),
                                            EnumColCex.NUM_CONSULTAS_NO_OTORGADAS.getRutaCompleta())),
                            Group.grouping(EnumColCex.P1_CITAS.getValor(),
                                    new Accumulator(EnumOpMongo.O_SUM.getValor(),
                                            EnumColCex.NUM_CITAS_NO_CUMPLIDAS.getRutaCompleta())),

                            // PUSH sobre encabezado
                            Group.grouping(EnumColCex.P1_ENCABEZADO.getValor(),
                                    new Accumulator(EnumOpMongo.O_PUSH.getValor(),
                                            new Document(EnumColCex.P1_DOC_ENCABEZADO.getValor(),
                                                    EnumColCex.SD_ENCABEZADO.getValor(Boolean.TRUE))))                   		
                    		);                 
                    
	
                    break;

                case BaseConstants.TC_URGENCIAS:
                    aggr.group(Group.id(
                            Group.grouping(EnumColCex.P1_MATRICULA.getValor(),
                                    EnumColCex.CVE_MATRICULA.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_NOM.getValor(), EnumColCex.REF_NOMBRE.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_APP.getValor(), EnumColCex.REF_APELLIDO_P.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_APM.getValor(), EnumColCex.REF_APELLIDO_M.getRutaCompleta()),
                            Group.grouping(EnumColCex.P1_ESP.getValor(),
                                    EnumColCex.CVE_ESPECIALIDAD.getRutaCompleta())

                    		),
                            //Group.grouping(EnumColCex.P1_CONTADOR.getValor(), new Accumulator("$sum",1)),
                    		Group.grouping(EnumColCex.P_MEDICO.getValor(),
                                    new Accumulator(EnumOpMongo.O_ADDTOSET.getValor(),
                                            new Document(mapaCamposMedico))),

                            Group.grouping(EnumColCex.P1_DES_ESP.getValor(),
                                    new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                            EnumColCex.DES_ESPECIALIDAD.getRutaCompleta())),
                            
                            Group.grouping(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor(),
                                    new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                            EnumColCex.NOM_UNIDAD_MEDICA.getRutaCompleta())),

                            // Obtener información de atenciones médicas
                            Group.grouping(EnumColCex.P1_TIEMPO_TRA_HORAS.getValor(),
                                    new Accumulator(EnumOpMongo.O_PUSH.getValor(),
                                            new Document(EnumColCex.P1_HORAS.getValor(),
                                                    EnumColCex.NUM_HORAS_TRABAJADAS.getRutaCompleta(Boolean.TRUE)))),

                            agruparCond(EnumColCex.P1_TIEMPO_TRA_DIAS.getValor(),
                                    EnumColCex.NUM_CONSULTAS_NO_OTORGADAS.getRutaCompleta(Boolean.TRUE),
                                    EnumOpMongo.C_GTE.getValor(), 0),

                            // Group.grouping(EnumColCex.P1_NO_OTOR.getValor(),new
                            // Accumulator(EnumOpMongo.O_SUM.getValor(),
                            // EnumColCex.NUM_CONSULTAS_NO_OTORGADAS.getRutaCompleta())),
                            // Group.grouping(EnumColCex.P1_CITAS.getValor(),new
                            // Accumulator(EnumOpMongo.O_SUM.getValor(),
                            // EnumColCex.NUM_CONSULTAS_NO_OTORGADAS.getRutaCompleta())),
                            Group.grouping(EnumColCex.P1_NO_OTOR.getValor(),
                                    new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                            Group.grouping(EnumColCex.P1_CITAS.getValor(),
                                    new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),

                            // PUSH sobre encabezado
                            Group.grouping(EnumColCex.P1_ENCABEZADO.getValor(),
                                    new Accumulator(EnumOpMongo.O_PUSH.getValor(),
                                            new Document(EnumColCex.P1_DOC_ENCABEZADO.getValor(),
                                                    EnumColCex.SD_ENCABEZADO.getValor(Boolean.TRUE)))));
                    break;
                case BaseConstants.TC_TOCOCIRUGIA:
                    aggr.group(Group.id(
                            Group.grouping(EnumColCex.P1_MATRICULA.getValor(),
                                    EnumColCex.CVE_MATRICULA.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_NOM.getValor(), EnumColCex.REF_NOMBRE.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_APP.getValor(), EnumColCex.REF_APELLIDO_P.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_APM.getValor(), EnumColCex.REF_APELLIDO_M.getRutaCompleta()),
                            Group.grouping(EnumColCex.P1_ESP.getValor(),
                                    EnumColCex.CVE_ESPECIALIDAD.getRutaCompleta())
                    		),
                            //Group.grouping(EnumColCex.P1_CONTADOR.getValor(), new Accumulator("$sum",1)),
                    		Group.grouping(EnumColCex.P_MEDICO.getValor(),
                                    new Accumulator(EnumOpMongo.O_ADDTOSET.getValor(),
                                            new Document(mapaCamposMedico))),
                    		
                            Group.grouping(EnumColCex.P1_DES_ESP.getValor(),
                                    new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                            EnumColCex.DES_ESPECIALIDAD.getRutaCompleta())),
                            Group.grouping(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor(),
                                    new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                            EnumColCex.NOM_UNIDAD_MEDICA.getRutaCompleta())),

                            // Obtener información de atenciones médicas
                            Group.grouping(EnumColCex.P1_TIEMPO_TRA_HORAS.getValor(),
                                    new Accumulator(EnumOpMongo.O_PUSH.getValor(),
                                            new Document(EnumColCex.P1_HORAS.getValor(),
                                                    EnumColCex.NUM_HORAS_TRABAJADAS.getRutaCompleta(Boolean.TRUE)))),

                            agruparCond(EnumColCex.P1_TIEMPO_TRA_DIAS.getValor(),
                                    EnumColCex.NUM_CITAS_NO_CUMPLIDAS.getRutaCompleta(Boolean.TRUE),
                                    EnumOpMongo.C_GTE.getValor(), 0),

                            Group.grouping(EnumColCex.P1_NO_OTOR.getValor(),
                                    new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                            Group.grouping(EnumColCex.P1_CITAS.getValor(),
                                    new Accumulator(EnumOpMongo.O_SUM.getValor(),
                                            EnumColCex.NUM_CITAS_NO_CUMPLIDAS.getRutaCompleta())),
                            // PUSH sobre encabezado
                            Group.grouping(EnumColCex.P1_ENCABEZADO.getValor(),
                                    new Accumulator(EnumOpMongo.O_PUSH.getValor(),
                                            new Document(EnumColCex.P1_DOC_ENCABEZADO.getValor(),
                                                    EnumColCex.SD_ENCABEZADO.getValor(Boolean.TRUE)))));
                    break;
                case BaseConstants.TC_PARAMEDICOS:
                    aggr.group(Group.id(
                            Group.grouping(EnumColCex.P1_MATRICULA.getValor(),
                                    EnumColCex.CVE_MATRICULA.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_NOM.getValor(), EnumColCex.REF_NOMBRE.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_APP.getValor(), EnumColCex.REF_APELLIDO_P.getRutaCompleta()),
                            //Group.grouping(EnumColCex.P1_APM.getValor(), EnumColCex.REF_APELLIDO_M.getRutaCompleta()),
                            Group.grouping(EnumColCex.P1_ESP.getValor(),
                                    EnumColCex.CVE_ESPECIALIDAD.getRutaCompleta())
                    		),
                            //Group.grouping(EnumColCex.P1_CONTADOR.getValor(), new Accumulator("$sum",1)),
                    		
                    		Group.grouping(EnumColCex.P_MEDICO.getValor(),
                                    new Accumulator(EnumOpMongo.O_ADDTOSET.getValor(),
                                            new Document(mapaCamposMedico))),

                            Group.grouping(EnumColCex.P1_DES_ESP.getValor(),
                                    new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                            EnumColCex.DES_ESPECIALIDAD.getRutaCompleta())),
                            Group.grouping(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor(),
                                    new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                            EnumColCex.NOM_UNIDAD_MEDICA.getRutaCompleta())),

                            // Obtener información de atenciones médicas
                            Group.grouping(EnumColCex.P1_TIEMPO_TRA_HORAS.getValor(),
                                    new Accumulator(EnumOpMongo.O_PUSH.getValor(),
                                            new Document(EnumColCex.P1_HORAS.getValor(),
                                                    EnumColCex.NUM_HORAS_TRABAJADAS.getRutaCompleta(Boolean.TRUE)))),

                            agruparCond(EnumColCex.P1_TIEMPO_TRA_DIAS.getValor(),
                                    EnumColCex.NUM_CONSULTAS_NO_OTORGADAS.getRutaCompleta(Boolean.TRUE),
                                    EnumOpMongo.C_GTE.getValor(), 0),

                            Group.grouping(EnumColCex.P1_NO_OTOR.getValor(),
                                    new Accumulator(EnumOpMongo.O_SUM.getValor(),
                                            EnumColCex.NUM_CONSULTAS_NO_OTORGADAS.getRutaCompleta())),
                            Group.grouping(EnumColCex.P1_CITAS.getValor(),
                                    new Accumulator(EnumOpMongo.O_SUM.getValor(),
                                            EnumColCex.NUM_CITAS_NO_CUMPLIDAS.getRutaCompleta())),

                            // PUSH sobre encabezado
                            Group.grouping(EnumColCex.P1_ENCABEZADO.getValor(),
                                    new Accumulator(EnumOpMongo.O_PUSH.getValor(),
                                            new Document(EnumColCex.P1_DOC_ENCABEZADO.getValor(),
                                                    EnumColCex.SD_ENCABEZADO.getValor(Boolean.TRUE)))));
                    break;

                default:
                    return null;
                    

            }
            
            // Primer Project

            aggr.project(Projection.projection(EnumColCex.P1_ID.getValor()),
            		Projection.projection(EnumColCex.P_MEDICO.getValor()),
                    Projection.projection(EnumColCex.P1_TIEMPO_TRA_HORAS.getValor()),
                    Projection.projection(EnumColCex.P1_TIEMPO_TRA_DIAS.getValor()),
                    Projection.projection(EnumColCex.P1_CITAS.getValor()),
                    Projection.projection(EnumColCex.P1_NO_OTOR.getValor()),
                    Projection.projection(EnumColCex.P1_ENCABEZADO.getValor()),
                    Projection.projection(EnumColCex.P1_DES_ESP.getValor()),
                    Projection.projection(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor()));

            // Unwind
            aggr.unwind(EnumColCex.P1_ENCABEZADO.getValor());
            aggr.unwind(EnumColCex.SD_FORMATO.getRutaCompleta());

            Map<String, Object> mapaCamposMedicoP = new HashMap<>();
            mapaCamposMedicoP.put(EnumColCex.P1_NOM.getValor(), EnumColCex.P_REF_NOMBRE.getRutaCompleta(Boolean.TRUE));
            mapaCamposMedicoP.put(EnumColCex.P1_APM.getValor(), EnumColCex.P_REF_APELLIDO_M.getRutaCompleta(Boolean.TRUE));
            mapaCamposMedicoP.put(EnumColCex.P1_APP.getValor(), EnumColCex.P_REF_APELLIDO_P.getRutaCompleta(Boolean.TRUE));
           // mapaCamposMedicoP.put(EnumColCex.P1_ESP.getValor(), EnumColCex.P_CVE_ESPECIALIDAD.getRutaCompleta(Boolean.TRUE));
            
            if (tipoConsulta != BaseConstants.TC_PARAMEDICOS) {

                aggr.unwind(EnumColCex.SD_PASE_SERVICIO.getRutaCompleta());

                // Segundo Group
                
                
       
                aggr.group(Group.id(
                        Group.grouping(EnumColCex.P1_MATRICULA.getValor(),
                                EnumColCex.P_CVE_MATRICULA.getRutaCompleta()),
                        //Group.grouping(EnumColCex.P1_NOM.getValor(), EnumColCex.P_REF_NOMBRE.getRutaCompleta()),
                        //Group.grouping(EnumColCex.P1_APP.getValor(), EnumColCex.P_REF_APELLIDO_P.getRutaCompleta()),
                        //Group.grouping(EnumColCex.P1_APM.getValor(), EnumColCex.P_REF_APELLIDO_M.getRutaCompleta()),
                        Group.grouping(EnumColCex.P1_ESP.getValor(), EnumColCex.P_CVE_ESPECIALIDAD.getRutaCompleta())
                        ),              		
                		
                        //Group.grouping(EnumColCex.P1_CONTADOR.getValor(), new Accumulator("$sum",1)),
                		Group.grouping(EnumColCex.P_MEDICO.getValor(),
                                new Accumulator(EnumOpMongo.O_ADDTOSET.getValor(),
                                        new Document(mapaCamposMedicoP))),
                		
                        Group.grouping(EnumColCex.P1_DES_ESP.getValor(),
                                new Accumulator(EnumOpMongo.O_FIRST.getValor(), EnumColCex.P1_DES_ESP.getValor())),
                        Group.grouping(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor(),
                                new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                        EnumColCex.P1_DES_UNIDAD_MEDICA.getValor())),

                        // Obtener información de atenciones médicas
                        Group.grouping(EnumColCex.AM_TIEMPO_TRA_HORAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(),
                                        EnumColCex.P1_TIEMPO_TRA_HORAS.getValor())),
                        Group.grouping(EnumColCex.AM_TIMEPO_TRA_DIAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(),
                                        EnumColCex.P1_TIEMPO_TRA_DIAS.getValor())),
                        agruparCond(EnumColCex.AM_TOT_CONS.getValor(),
                                EnumColCex.SD_FORMATO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_GTE.getValor(), 1),
                        agruparCond(EnumColCex.AM_1VEZ.getValor(), EnumColCex.IND_1ER_VEZ.getRutaCompleta(Boolean.TRUE),
                                EnumOpMongo.C_GTE.getValor(), 1),

                        Group.grouping(EnumColCex.AM_NO_OTORGADAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), EnumColCex.P1_NO_OTOR.getValor())),
                        agruparCond(EnumColCex.AM_NUM_VISITAS.getValor(),
                                EnumColCex.IND_VISITA.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_GTE.getValor(), 1),
                        Group.grouping(EnumColCex.AM_CITAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), EnumColCex.P1_CITAS.getValor())),
                        agruparCond(EnumColCex.AM_CITAS_CUMPLIDAS.getValor(),
                                EnumColCex.IND_CITADO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_GTE.getValor(), 1),
                        // PE
                        agruparCond(EnumColCex.PE_NO_OTORGO.getValor(),
                                EnumColCex.CVE_PASE_SERVICIO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                                0),
                        agruparCond(EnumColCex.PE_EN_UNI.getValor(),
                                EnumColCex.CVE_PASE_SERVICIO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                                1),
                        agruparCond(EnumColCex.PE_OTRA_UNIDAD2.getValor(),
                                EnumColCex.CVE_PASE_SERVICIO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                                2),
                        agruparCond(EnumColCex.PE_ALTA3.getValor(),
                                EnumColCex.CVE_PASE_SERVICIO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                                3),
                        agruparCond(EnumColCex.PE_ALTA_PASE4.getValor(),
                                EnumColCex.CVE_PASE_SERVICIO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                                4),
                        // Incapacidad
                        agruparCond(EnumColCex.INC_EXPED.getValor(),
                                EnumColCex.NUM_DIAS_INCAPACIDAD.getRutaCompleta(Boolean.TRUE),
                                EnumOpMongo.C_GTE.getValor(), 1),
                        Group.grouping(EnumColCex.INC_DIAS.getValor(),
                                new Accumulator(EnumOpMongo.O_SUM.getValor(),
                                        EnumColCex.NUM_DIAS_INCAPACIDAD.getRutaCompleta())),
                        // Recetas
                        Group.grouping(EnumColCex.REC_NUM.getValor(), new Accumulator(EnumOpMongo.O_SUM.getValor(),
                                EnumColCex.NUM_RECETAS.getRutaCompleta())));
            } else {
                // Paramedicos
                // Segundo Group
                aggr.group(Group.id(
                        Group.grouping(EnumColCex.P1_MATRICULA.getValor(),
                                EnumColCex.P_CVE_MATRICULA.getRutaCompleta()),
                        //Group.grouping(EnumColCex.P1_NOM.getValor(), EnumColCex.P_REF_NOMBRE.getRutaCompleta()),
                        //Group.grouping(EnumColCex.P1_APP.getValor(), EnumColCex.P_REF_APELLIDO_P.getRutaCompleta()),
                        //Group.grouping(EnumColCex.P1_APM.getValor(), EnumColCex.P_REF_APELLIDO_M.getRutaCompleta()),
                        Group.grouping(EnumColCex.P1_ESP.getValor(), EnumColCex.P_CVE_ESPECIALIDAD.getRutaCompleta())
                		),
                		
                        //Group.grouping(EnumColCex.P1_CONTADOR.getValor(), new Accumulator("$sum",1)),
                		Group.grouping(EnumColCex.P_MEDICO.getValor(),
                                new Accumulator(EnumOpMongo.O_ADDTOSET.getValor(),
                                        new Document(mapaCamposMedicoP))),
                		
                        Group.grouping(EnumColCex.P1_DES_ESP.getValor(),
                                new Accumulator(EnumOpMongo.O_FIRST.getValor(), EnumColCex.P1_DES_ESP.getValor())),
                        Group.grouping(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor(),
                                new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                        EnumColCex.P1_DES_UNIDAD_MEDICA.getValor())),

                        // Obtener información de atenciones médicas
                        Group.grouping(EnumColCex.AM_TIEMPO_TRA_HORAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(),
                                        EnumColCex.P1_TIEMPO_TRA_HORAS.getValor())),
                        Group.grouping(EnumColCex.AM_TIMEPO_TRA_DIAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(),
                                        EnumColCex.P1_TIEMPO_TRA_DIAS.getValor())),
                        agruparCond(EnumColCex.AM_TOT_CONS.getValor(),
                                EnumColCex.SD_FORMATO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_GTE.getValor(), 1),
                        agruparCond(EnumColCex.AM_1VEZ.getValor(), EnumColCex.IND_1ER_VEZ.getRutaCompleta(Boolean.TRUE),
                                EnumOpMongo.C_GTE.getValor(), 1),

                        Group.grouping(EnumColCex.AM_NO_OTORGADAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), EnumColCex.P1_NO_OTOR.getValor())),
                        agruparCond(EnumColCex.AM_NUM_VISITAS.getValor(),
                                EnumColCex.IND_VISITA.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_GTE.getValor(), 1),
                        Group.grouping(EnumColCex.AM_CITAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), EnumColCex.P1_CITAS.getValor())),
                        agruparCond(EnumColCex.AM_CITAS_CUMPLIDAS.getValor(),
                                EnumColCex.IND_CITADO.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_GTE.getValor(), 1),
                        // PE
                        Group.grouping(EnumColCex.PE_NO_OTORGO.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                        Group.grouping(EnumColCex.PE_EN_UNI.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                        Group.grouping(EnumColCex.PE_OTRA_UNIDAD2.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                        Group.grouping(EnumColCex.PE_ALTA3.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                        Group.grouping(EnumColCex.PE_ALTA_PASE4.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                        // Incapacidad
                        Group.grouping(EnumColCex.INC_EXPED.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                        Group.grouping(EnumColCex.INC_DIAS.getValor(),
                                new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                        // Recetas
                        Group.grouping(EnumColCex.REC_NUM.getValor(), new Accumulator(EnumOpMongo.O_MAX.getValor(), 0))

                );
            }
            
            // Final Project
            aggr.project(Projection.projection(EnumColCex.P1_ID.getValor()),
            		Projection.projection(EnumColCex.P_MEDICO.getValor()),
                    Projection.projection(EnumColCex.AM_TIEMPO_TRA_HORAS.getValor()),
                    Projection.projection(EnumColCex.AM_TIMEPO_TRA_DIAS.getValor()),
                    Projection.projection(EnumColCex.AM_TOT_CONS.getValor()),
                    Projection.projection(EnumColCex.AM_1VEZ.getValor()),
                    Projection.projection(EnumColCex.AM_NO_OTORGADAS.getValor()),
                    Projection.projection(EnumColCex.AM_NUM_VISITAS.getValor()),
                    Projection.projection(EnumColCex.AM_CITAS.getValor(),
                            Projection.add(Projection.projection(EnumColCex.AM_CITAS_CUMPLIDAS.getValor()),
                                    Projection.projection(EnumColCex.AM_CITAS.getValor()))),
                    Projection.projection(EnumColCex.AM_CITAS_CUMPLIDAS.getValor()),
                    Projection.projection(EnumColCex.PE_EN_UNI.getValor()),
                    Projection.projection(EnumColCex.PE_OTRA_UNIDAD.getValor(),
                            Projection.add(Projection.projection(EnumColCex.PE_OTRA_UNIDAD2.getValor()),
                                    Projection.projection(EnumColCex.PE_ALTA_PASE4.getValor()))),
                    Projection.projection(EnumColCex.PE_ALTA_ESPECIALES.getValor(),
                            Projection.add(Projection.projection(EnumColCex.PE_ALTA3.getValor()),
                                    Projection.projection(EnumColCex.PE_ALTA_PASE4.getValor()))),
                    Projection.projection(EnumColCex.INC_EXPED.getValor()),
                    Projection.projection(EnumColCex.INC_DIAS.getValor()),
                    Projection.projection(EnumColCex.INC_PROM.getValor(),
                            Projection.projection(EnumOpMongo.E_COND.getValor(),
                                    Projection.projection(EnumOpMongo.E_IF.getValor(),
                                            Projection.expression(EnumOpMongo.C_EQ.getValor(),
                                                    Projection.projection(EnumColCex.INC_EXPED.getValor()), 0)),
                                    Projection.projection(EnumOpMongo.E_THEN.getValor(), Projection.add(0, 0)),
                                    Projection.projection(EnumOpMongo.E_ELSE.getValor(),
                                            Projection.divide(EnumColCex.INC_DIAS.getValor(Boolean.TRUE),
                                                    EnumColCex.INC_EXPED.getValor(Boolean.TRUE))))),
                    Projection.projection(EnumColCex.REC_NUM.getValor()),
                    Projection.projection(EnumColCex.REC_PORCENTAJE_ATEN.getValor(),
                            Projection.multiply((Projection.divide(Projection.projection("recNum"),
                                    Projection.projection("amTotCons"))), 100)),
                    Projection.projection(EnumColCex.P1_DES_ESP.getValor(),
                            Projection.expression("$arrayElemAt",
                                    Projection.projection(EnumColCex.P1_DES_ESP.getValor()), 0)),
                    Projection.projection(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor(), Projection.expression(
                            "$arrayElemAt", Projection.projection(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor()), 0)),
					Projection.projection(EnumColCex.P_MEDICO.getValor(),
                            Projection.expression("$arrayElemAt",
                                    Projection.projection(EnumColCex.P_MEDICO.getValor()), 0)));
            // Se ejecuta la consulta  
            
            
            
            reporte = aggr.aggregate(ExtractoReporteParteI.class, AggregationOptions.builder().allowDiskUse(true).build());
         
            /*try {
            	
            	logger.debug("AGGR-CONSULTA= "+aggr.toString());
            }
            catch (Exception exx)
            {
            	logger.debug("Error en el toString del AGGR: "+exx);	
            }*/
            

        } catch (Exception ex) {
            logger.error("Error extraerInfoMedicosReporteVista2:" + ex);
        }
        return reporte;

    }

    /**
     * Funcion que sirve para realizar la suma de una campo si cumple una condicion
     * 
     * @param campoNombre
     *            Nombre resultado de agrupar
     * @param campoAgrupar
     *            Campo a ser agrupado
     * @param condificion
     *            Condicion (eq,gte,lte)
     * @param valorComparar
     *            Valor de comparación de campoAgrupar y condicion
     * @return
     */
    private Group agruparCond(String campoNombre, String campoAgrupar, String condicion, Integer valorComparar) {

        return Group.grouping(campoNombre, new Accumulator(EnumOpMongo.O_SUM.getValor(), new Document(
                EnumOpMongo.E_COND.getValor(),
                new Document(EnumOpMongo.E_IF.getValor(),
                        new Document(condicion, Arrays.asList(campoAgrupar, valorComparar)))
                                .append(EnumOpMongo.E_THEN.getValor(), 1).append(EnumOpMongo.E_ELSE.getValor(), 0))));
    }

    public Date stringToDate(String strFecha) {

        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        Date fecha = null;
        try {

            fecha = formatoDelTexto.parse(strFecha);

        } catch (ParseException ex) {
            logger.error("Error stringToDate:" + ex);
        }
        return fecha;
    }

    @Override
    public SicPeriodosImss obtenerPeriodImss(Integer clavePeriodo) {

        SicPeriodosImss periodo = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            periodo = dsEsp.createQuery(SicPeriodosImss.class).filter("cvePeriodo", clavePeriodo).get();
        } catch (Exception e) {
            e.getMessage();
        }

        return periodo;

    }

    @Override
    public void insertarReporteParte1(SitReporteParteI sitReporteParteI) {

        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();
            ds.save(sitReporteParteI);
            logger.debug(" GUARDADO sitReporteParteI:" + sitReporteParteI.getId());
        } catch (Exception e) {
            logger.error("Error insertarReporteParte1:" + e);
        }

    }

    @Override
    public Iterator<ExtractoReporteParteIAccidentesyL> extraerInfoMedicosReporteAccidentesyLes(String cvePresupuestal,
            Date fechaInicio, Date fechaFin) {

        Iterator<ExtractoReporteParteIAccidentesyL> reporte = null;
        try {

            // Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            Datastore dsEsp = MongoManager.INSTANCE.getDatastoreReplica();

            Query<SitConsultaExterna> q = dsEsp.createQuery(SitConsultaExterna.class);
            // MATCH
            q.filter(EnumColCex.CVE_PRESUPUESTAL.getRutaCompleta(), cvePresupuestal);
            q.and(q.criteria(EnumColCex.FEC_ALTA_ENCABEZADO.getRutaCompleta()).greaterThanOrEq(fechaInicio),
                    q.criteria(EnumColCex.FEC_ALTA_ENCABEZADO.getRutaCompleta()).lessThanOrEq(fechaFin));
            q.and(q.criteria(EnumColCex.CVE_ESPECIALIDAD.getRutaCompleta()).contains("^50"));

            AggregationPipeline aggr = dsEsp.createAggregation(SitConsultaExterna.class);
            aggr.match(q);
            aggr.unwind(EnumColCex.SD_ENCABEZADO.getValor());
            aggr.unwind(EnumColCex.SD_FORMATO_AL.getRutaCompleta());

            Query<SitConsultaExterna> q2 = dsEsp.createQuery(SitConsultaExterna.class);
            // MATCH
            q2.or(q2.and(q.criteria(EnumColCex.DX_ADICIONAL_CIE10_AL.getRutaCompleta()).greaterThanOrEq("V010"),
                    q.criteria(EnumColCex.DX_ADICIONAL_CIE10_AL.getRutaCompleta()).lessThanOrEq("Y98X")),
                    q2.and(q.criteria(EnumColCex.DX_COMPLEMENTARIO_CIE10_AL.getRutaCompleta()).greaterThanOrEq("V010"),
                            q.criteria(EnumColCex.DX_COMPLEMENTARIO_CIE10_AL.getRutaCompleta()).lessThanOrEq("Y98X"))

            );
            aggr.match(q2);
            aggr.unwind(EnumColCex.SD_PASE_SERVICIO_AL.getRutaCompleta());

            // Segundo Group
            aggr.group("_id:null",

                    Group.grouping(EnumColCex.P1_DES_UNIDAD_MEDICA.getValor(),
                            new Accumulator(EnumOpMongo.O_FIRST.getValor(),
                                    EnumColCex.P1_DES_UNIDAD_MEDICA.getValor())),

                    // Obtener información de atenciones médicas
                    Group.grouping(EnumColCex.AM_TIEMPO_TRA_HORAS.getValor(),
                            new Accumulator(EnumOpMongo.O_MAX.getValor(), EnumColCex.P1_TIEMPO_TRA_HORAS.getValor())),
                    Group.grouping(EnumColCex.AM_TIMEPO_TRA_DIAS.getValor(),
                            new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                    agruparCond(EnumColCex.AM_TOT_CONS.getValor(),
                            EnumColCex.SD_FORMATO_AL.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_GTE.getValor(), 1),

                    Group.grouping(EnumColCex.AM_1VEZ.getValor(), new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                    // agruparCond(EnumColCex.AM_1VEZ.getValor(),EnumColCex.IND_1ER_VEZ.getRutaCompleta(Boolean.TRUE),EnumOpMongo.C_GTE.getValor(),1),

                    Group.grouping(EnumColCex.AM_NO_OTORGADAS.getValor(),
                            new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                    Group.grouping(EnumColCex.AM_NUM_VISITAS.getValor(),
                            new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                    Group.grouping(EnumColCex.AM_CITAS_CUMPLIDAS.getValor(),
                            new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),
                    Group.grouping(EnumColCex.AM_CITAS.getValor(), new Accumulator(EnumOpMongo.O_MAX.getValor(), 0)),

                    // PE
                    agruparCond(EnumColCex.PE_NO_OTORGO.getValor(),
                            EnumColCex.CVE_PASE_SERVICIO_AL.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                            0),
                    agruparCond(EnumColCex.PE_EN_UNI.getValor(),
                            EnumColCex.CVE_PASE_SERVICIO_AL.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                            1),
                    agruparCond(EnumColCex.PE_OTRA_UNIDAD2.getValor(),
                            EnumColCex.CVE_PASE_SERVICIO_AL.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                            2),
                    agruparCond(EnumColCex.PE_ALTA3.getValor(),
                            EnumColCex.CVE_PASE_SERVICIO_AL.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                            3),
                    agruparCond(EnumColCex.PE_ALTA_PASE4.getValor(),
                            EnumColCex.CVE_PASE_SERVICIO_AL.getRutaCompleta(Boolean.TRUE), EnumOpMongo.C_EQ.getValor(),
                            4),
                    // Incapacidad
                    agruparCond(EnumColCex.INC_EXPED.getValor(),
                            EnumColCex.NUM_DIAS_INCAPACIDAD_AL.getRutaCompleta(Boolean.TRUE),
                            EnumOpMongo.C_GTE.getValor(), 1),
                    Group.grouping(EnumColCex.INC_DIAS.getValor(),
                            new Accumulator(EnumOpMongo.O_SUM.getValor(),
                                    EnumColCex.NUM_DIAS_INCAPACIDAD_AL.getRutaCompleta())),
                    // Recetas
                    Group.grouping(EnumColCex.REC_NUM.getValor(), new Accumulator(EnumOpMongo.O_SUM.getValor(),
                            EnumColCex.NUM_RECETAS_AL.getRutaCompleta())));

            // Final Project
            aggr.project(Projection.projection(EnumColCex.AM_TIMEPO_TRA_DIAS.getValor()),
                    Projection.projection(EnumColCex.AM_TOT_CONS.getValor()),
                    Projection.projection(EnumColCex.AM_1VEZ.getValor()),
                    Projection.projection(EnumColCex.AM_NO_OTORGADAS.getValor()),
                    Projection.projection(EnumColCex.AM_NUM_VISITAS.getValor()),
                    Projection.projection(EnumColCex.AM_CITAS.getValor(),
                            Projection.add(Projection.projection(EnumColCex.AM_CITAS_CUMPLIDAS.getValor()),
                                    Projection.projection(EnumColCex.AM_CITAS.getValor()))),
                    Projection.projection(EnumColCex.AM_CITAS_CUMPLIDAS.getValor()),
                    Projection.projection(EnumColCex.PE_EN_UNI.getValor()),
                    Projection.projection(EnumColCex.PE_OTRA_UNIDAD.getValor(),
                            Projection.add(Projection.projection(EnumColCex.PE_OTRA_UNIDAD2.getValor()),
                                    Projection.projection(EnumColCex.PE_ALTA_PASE4.getValor()))),
                    Projection.projection(EnumColCex.PE_ALTA_ESPECIALES.getValor(),
                            Projection.add(Projection.projection(EnumColCex.PE_ALTA3.getValor()),
                                    Projection.projection(EnumColCex.PE_ALTA_PASE4.getValor()))),
                    Projection.projection(EnumColCex.INC_EXPED.getValor()),
                    Projection.projection(EnumColCex.INC_DIAS.getValor()),
                    Projection.projection(EnumColCex.INC_PROM.getValor(),
                            Projection.projection(EnumOpMongo.E_COND.getValor(),
                                    Projection.projection(EnumOpMongo.E_IF.getValor(),
                                            Projection.expression(EnumOpMongo.C_EQ.getValor(),
                                                    Projection.projection(EnumColCex.INC_EXPED.getValor()), 0)),
                                    Projection.projection(EnumOpMongo.E_THEN.getValor(), Projection.add(0, 0)),
                                    Projection.projection(EnumOpMongo.E_ELSE.getValor(),
                                            Projection.divide(EnumColCex.INC_DIAS.getValor(Boolean.TRUE),
                                                    EnumColCex.INC_EXPED.getValor(Boolean.TRUE))))),
                    Projection.projection(EnumColCex.REC_NUM.getValor()),
                    Projection.projection(EnumColCex.REC_PORCENTAJE_ATEN.getValor(), Projection.multiply(
                            (Projection.divide(Projection.projection("recNum"), Projection.projection("amTotCons"))),
                            100)));
            // Se ejecuta la consulta
            reporte = aggr.aggregate(ExtractoReporteParteIAccidentesyL.class, AggregationOptions.builder().allowDiskUse(true).build());
        } catch (Exception ex) {
            logger.error("Error extraerInfoMedicosReporteAccidentesyLes:" + ex);
        }
        return reporte;

    }

    public SicPeriodosImss obtenerPeriodImssPorFecha(Date fecha) {

        SicPeriodosImss periodo = null;
        Iterator<SicPeriodosImss> sicperiodos = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SicPeriodosImss> q = dsEsp.createQuery(SicPeriodosImss.class);
            // MATCH
            q.criteria(EnumColCex.FEC_INICIAL.getValor()).lessThanOrEq(fecha);

            AggregationPipeline aggr = dsEsp.createAggregation(SicPeriodosImss.class);
            aggr.match(q);
            aggr.sort(Sort.descending("cvePeriodo"));
            aggr.limit(1);

            sicperiodos = aggr.aggregate(SicPeriodosImss.class, AggregationOptions.builder().build());
        } catch (Exception e) {
            e.getMessage();
        }
        if (sicperiodos != null) {
            while (sicperiodos.hasNext()) {
                periodo = sicperiodos.next();
            }
        }
        return periodo;

    }

    @Override
    public SitReporteParteI buscarReporteParte1(String cvePresupuestal, Integer clavePeriodo) {

        SitReporteParteI reporte = null;
        Iterator<SitReporteParteI> sitReportes = null;
        try {
            // Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            Datastore dsEsp = MongoManager.INSTANCE.getDatastoreReplica();

            Query<SitReporteParteI> q = dsEsp.createQuery(SitReporteParteI.class);
            // MATCH
            q.criteria(EnumColCex.CVE_PRESUPUESTAL.getRutaCompleta()).equal(cvePresupuestal);
            q.criteria(EnumColCex.CVE_PERIODO_IMSS.getRutaCompleta()).equal(clavePeriodo);

            AggregationPipeline aggr = dsEsp.createAggregation(SitReporteParteI.class);
            aggr.match(q);
            aggr.limit(1);

            sitReportes = aggr.aggregate(SitReporteParteI.class, AggregationOptions.builder().build());
        } catch (Exception e) {
            e.getMessage();
        }
        if (sitReportes != null) {
            while (sitReportes.hasNext()) {
                reporte = sitReportes.next();
            }
        }

        return reporte;

    }

    @Override
    public Boolean borrarReporteParte1(SitReporteParteI sitReporteParteI) {

        Boolean borrado = Boolean.FALSE;
        try {
            if (sitReporteParteI != null) {
                Datastore ds = MongoManager.INSTANCE.getDatastore();
                WriteResult wr = ds.delete(sitReporteParteI);
                if (wr.getN() > 0) {
                    borrado = Boolean.TRUE;
                }
                logger.debug(" GUARDADO sitReporteParteI:" + sitReporteParteI.getId());

            } else {
                logger.debug("No existe reporte a borrar");
            }

        } catch (Exception e) {
            e.getMessage();
        }
        return borrado;

    }

}
