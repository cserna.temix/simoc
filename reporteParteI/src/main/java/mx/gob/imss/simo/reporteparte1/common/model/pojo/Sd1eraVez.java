/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class Sd1eraVez {

	private Integer cve1eraVez;
	private String des1eraVez;

	/**
	 * @return the cve1eraVez
	 */
	public Integer getCve1eraVez() {
		return cve1eraVez;
	}

	/**
	 * @param cve1eraVez
	 *            the cve1eraVez to set
	 */
	public void setCve1eraVez(Integer cve1eraVez) {
		this.cve1eraVez = cve1eraVez;
	}

	/**
	 * @return the des1eraVez
	 */
	public String getDes1eraVez() {
		return des1eraVez;
	}

	/**
	 * @param des1eraVez
	 *            the des1eraVez to set
	 */
	public void setDes1eraVez(String des1eraVez) {
		this.des1eraVez = des1eraVez;
	}

}
