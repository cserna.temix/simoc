/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author francisco.rrios
 */
@Document(collection = "SIT_CAPTURISTA")
public class SitCapturista {

    @Id
    private ObjectId id;
    private String refApellidoPaterno;
    private String refApellidoMaterno;
    private String refNombre;

    /**
     * @return the id
     */
    public ObjectId getId() {

        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(ObjectId id) {

        this.id = id;
    }

    /**
     * @return the refApellidoPaterno
     */
    public String getRefApellidoPaterno() {

        return refApellidoPaterno;
    }

    /**
     * @param refApellidoPaterno
     *            the refApellidoPaterno to set
     */
    public void setRefApellidoPaterno(String refApellidoPaterno) {

        this.refApellidoPaterno = refApellidoPaterno;
    }

    /**
     * @return the refApellidoMaterno
     */
    public String getRefApellidoMaterno() {

        return refApellidoMaterno;
    }

    /**
     * @param refApellidoMaterno
     *            the refApellidoMaterno to set
     */
    public void setRefApellidoMaterno(String refApellidoMaterno) {

        this.refApellidoMaterno = refApellidoMaterno;
    }

    /**
     * @return the refNombre
     */
    public String getRefNombre() {

        return refNombre;
    }

    /**
     * @param refNombre
     *            the refNombre to set
     */
    public void setRefNombre(String refNombre) {

        this.refNombre = refNombre;
    }

}
