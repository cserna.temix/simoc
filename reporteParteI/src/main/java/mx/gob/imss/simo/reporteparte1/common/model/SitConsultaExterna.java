/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdCapturista;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdDelegacion;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdEncabezado;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdPerfil;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;

/**
 * @author janiel
 */

@Entity(value = "SIT_CONSULTA_EXTERNA")
public class SitConsultaExterna {

    @Id
    private ObjectId id;
    private List<SdCapturista> sdCapturista = new ArrayList<>();
    private List<SdPerfil> sdPerfil = new ArrayList<>();
    private List<SdDelegacion> sdDelegacion = new ArrayList<>();
    private List<SdUnidadMedica> sdUnidadMedica = new ArrayList<>();
    private List<SdEncabezado> sdEncabezado = new ArrayList<>();
    private Date fecCaptura;

    public ObjectId getId() {

        return id;
    }

    public void setId(ObjectId id) {

        this.id = id;
    }

    public List<SdCapturista> getSdCapturista() {

        return sdCapturista;
    }

    public void setSdCapturista(List<SdCapturista> sdCapturista) {

        this.sdCapturista = sdCapturista;
    }

    public List<SdPerfil> getSdPerfil() {

        return sdPerfil;
    }

    public void setSdPerfil(List<SdPerfil> sdPerfil) {

        this.sdPerfil = sdPerfil;
    }

    public List<SdDelegacion> getSdDelegacion() {

        return sdDelegacion;
    }

    public void setSdDelegacion(List<SdDelegacion> sdDelegacion) {

        this.sdDelegacion = sdDelegacion;
    }

    public List<SdUnidadMedica> getSdUnidadMedica() {

        return sdUnidadMedica;
    }

    public void setSdUnidadMedica(List<SdUnidadMedica> sdUnidadMedica) {

        this.sdUnidadMedica = sdUnidadMedica;
    }

    public List<SdEncabezado> getSdEncabezado() {

        return sdEncabezado;
    }

    public void setSdEncabezado(List<SdEncabezado> sdEncabezado) {

        this.sdEncabezado = sdEncabezado;
    }

    public Date getFecCaptura() {

        return fecCaptura;
    }

    public void setFecCaptura(Date fecCaptura) {

        this.fecCaptura = fecCaptura;
    }

}
