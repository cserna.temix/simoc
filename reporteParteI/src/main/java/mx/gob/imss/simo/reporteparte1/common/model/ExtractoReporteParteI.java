package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.List;

import org.mongodb.morphia.annotations.Id;

import lombok.Data;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.Medico;

@Data
public class ExtractoReporteParteI {

	@Id
	AgrupacionId groupId;
	List<NumHorasTrabajadas> amTiempoTraHoras;
	Integer amTiempoTraDias;
	Integer amTotCons;
	Integer am1Vez;
	Integer amNoOtorgadas;
	Integer amNumVisitas;
	Integer amCitas;
	Integer amCitasCumplidas;
	Integer peEnUnidad;
	Integer peOtraUnidad;
	Integer peAltasEspeciales;
	Integer incExpedidas;
	Integer incDias;
	Double incProm;	
	Integer recNum;
	Double recPorcentajeAten;
	String desEsp;
	String desUnidad;
	Medico medico;
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ExtractoReporteParteI [groupId=");
		builder.append(groupId);
		builder.append(", medico=");
		builder.append(medico);
		builder.append(", amTiempoTraHoras=");
		builder.append(amTiempoTraHoras);
		builder.append(", amTiempoTraDias=");
		builder.append(amTiempoTraDias);
		builder.append(", amTotCons=");
		builder.append(amTotCons);
		builder.append(", am1vez=");
		builder.append(am1Vez);
		builder.append(", amNoOtorgadas=");
		builder.append(amNoOtorgadas);
		builder.append(", amNumVisitas=");
		builder.append(amNumVisitas);
		builder.append(", amCitas=");
		builder.append(amCitas);
		builder.append(", amCitasCumplidas=");
		builder.append(amCitasCumplidas);
		builder.append(", peEnUnidad=");
		builder.append(peEnUnidad);
		builder.append(", peOtraUnidad=");
		builder.append(peOtraUnidad);
		builder.append(", peAltasEspeciales=");
		builder.append(peAltasEspeciales);
		builder.append(", incExpedidas=");
		builder.append(incExpedidas);
		builder.append(", incDias=");
		builder.append(incDias);
		builder.append(", incProm=");
		builder.append(incProm);
		builder.append(", recNum=");
		builder.append(recNum);
		builder.append(", recPorcentajeAten=");
		builder.append(recPorcentajeAten);
		builder.append(", desEsp=");
		builder.append(desEsp);
		builder.append("]");
		return builder.toString();
	}
	
	

	
	
}
