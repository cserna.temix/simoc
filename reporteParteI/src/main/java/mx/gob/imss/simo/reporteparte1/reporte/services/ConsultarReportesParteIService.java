package mx.gob.imss.simo.reporteparte1.reporte.services;

import java.util.List;

import org.primefaces.model.StreamedContent;

import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.reporteparte1.common.dto.PeriodoDTO;

public interface ConsultarReportesParteIService {

    Integer consultarNumEncabezadosPromUnidad(String unidadMedica);

    PeriodoDTO consultarReportesPorPeriodoUnidadMedica(PeriodoDTO periodo, String unidadMedica) throws Exception;

    List<PeriodoDTO> generarPeriodos(String cvePresupuestal) throws Exception;

    void actualizarSitControlReporte(String selPeriodo, String cvePresupuestal, boolean indControl) throws Exception;

    StreamedContent descargarReporte(String selPeriodo, ObjetosEnSesionBean objetosSs) throws Exception;

    public Integer getMaximoConsultas();

}
