package mx.gob.imss.simo.reporteparte1.reporte.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.Data;
import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.core.ReporteParteIConfiguration;
import mx.gob.imss.simo.reporteparte1.common.exception.ReporteParteIException;
import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;
import mx.gob.imss.simo.reporteparte1.common.services.impl.ReporteParte1CommonServiceImpl;
import mx.gob.imss.simo.reporteparte1.reporte.helper.DateUtils;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ColaDeReportesRepository;
import mx.gob.imss.simo.reporteparte1.reporte.services.ColaDeReportesService;
import mx.gob.imss.simo.reporteparte1.reporte.services.ProcesarReporteParteIService;

@Data
@Service("colaDeReporteService")
public class ColaDeReportesServiceImpl extends ReporteParte1CommonServiceImpl implements ColaDeReportesService {

    static final Logger logger = LoggerFactory.getLogger(ColaDeReportesServiceImpl.class);

    private Integer contadorMin = 0;

    @Autowired
    ColaDeReportesRepository colaDeReportesRepository;

    @Autowired
    ProcesarReporteParteIService procesarReporteParteIService;

    @Override
    public Iterator<SitColaReportes> buscarReportesEnCola(Integer estatus, Integer limite) {

        return colaDeReportesRepository.buscarReportesEnCola(estatus, limite);
    }

    // Servicio ejecutado por el CRON
    // Busca reportes en la bd como una cola.
    @Override
    public void buscarReportesEnColaParalelo() throws ReporteParteIException, InterruptedException {

        logger.debug("-----*-*-*-*-*-*-*-**-INICIA buscarReportesEnColaParalelo");

        // Cada cierto tiempo se busca en la cola reportes que pudieron haber fallado
        if (contadorMin == ReporteParteIConfiguration.INSTANCE.getMinutsoBuscarFalla()) {
            // Busca posibles reportes en estatus procesando
            Iterator<SitColaReportes> reportesPosibleFalla = colaDeReportesRepository
                    .buscarReportesEnCola(BaseConstants.ESTATUS_PROCESANDO, null);
            if (reportesPosibleFalla != null && reportesPosibleFalla.hasNext()) {
                // mientras existen posibles reportes con falla se itera
                while (reportesPosibleFalla.hasNext()) {
                    SitColaReportes re = reportesPosibleFalla.next();
                    // Un reporte se considera que falla cuando lleva muchos tiempo en estatus procesando
                    if (re.getFecInicio() == null
                            || DateUtils.comparaFechas(new Date(), DateUtils.agregarMinutosAFecha(re.getFecInicio(),
                                    ReporteParteIConfiguration.INSTANCE.getMinutosReporteFallido()))) {
                        // Se actualiza el reporte a estatus con error
                        actualizarReporteEnCola(re, BaseConstants.ESTATUS_ERROR);
//                    	actualizarReporteEnCola(re, BaseConstants.ESTATUS_PENDIENTE);

                    }
                }
            } else {
                // Busca en la cola reportes solicitudes repetidas
                colaDeReportesRepository.borrarRepetidosColaReportes();
            }

            contadorMin = 0;
        } else {

            // Se buscan reportes en cola con estatus pendiente con el l�mite indicado en configuration.properties
            Iterator<SitColaReportes> reportesPendientes = colaDeReportesRepository.buscarReportesEnCola(
                    BaseConstants.ESTATUS_PENDIENTE, ReporteParteIConfiguration.INSTANCE.getLimiteReportesMinuto());

            
            Long idTh = Thread.currentThread().getId();
            
            if(reportesPendientes != null) {
            	logger.debug("BUSCAR REPORTES EN COLA PENDIENTES reportesProcesando: " + reportesPendientes.toString());
	            while (reportesPendientes.hasNext()) {
	                SitColaReportes sitR = reportesPendientes.next();
	                logger.debug("sitR reportesPendientes:" + sitR.getCvePeriodo());
	                logger.debug("logger sitR reportesPendientes:" + sitR.getCvePeriodo());
	                logger.debug("crP:" + sitR.getIdThread());
	                sitR.setIdThread(idTh);
	                actualizarReporteEnCola(sitR, BaseConstants.ESTATUS_PREPARANDO);
	            }
            }

            List<SitColaReportes> reportesEjecutar = new ArrayList<>();
            // Se buscan reportes en cola con estatus preparando sin l�mite
            Iterator<SitColaReportes> reportesPreparando = colaDeReportesRepository
                    .buscarReportesEnColaPorEstatus(BaseConstants.ESTATUS_PREPARANDO, idTh, null);
            int reportesTomados = 0;

            if(reportesPreparando != null) {            
	            while (reportesPreparando.hasNext()) {
	                logger.debug("WHILE reportesPreparando");
	                SitColaReportes crP = reportesPreparando.next();
	                logger.debug("crP:" + crP.getCvePeriodo());
	                // Se buscan reportes en BD que tenga la misma presupuestal, estatus y periodo
	                Iterator<SitColaReportes> rEstatusExistentes = colaDeReportesRepository
	                        .buscarReportesUnidadEnColaPorEstatus(crP.getCvePresupuestal(), crP.getCvePeriodo(),
	                                BaseConstants.ESTATUS_PREPARANDO, null);
	                // int numero = 0;
	                if (rEstatusExistentes.hasNext()) {
	                    logger.debug("rEstatusExistentes.hasNext()");
	                    SitColaReportes crEx = rEstatusExistentes.next();
	                    logger.debug("crEx:" + crEx.getCvePeriodo());
	                    // if (crEx.getIdThread().equals(crP.getIdThread()) && numero == 0) {
	                    // if (crEx.getDesUnidad() != null && !crEx.getDesUnidad().equals("")) {
	                    logger.debug("SE ACTUALIZA a PROCESANDO crEx:" + crEx.getCvePeriodo() + "descripcion:"
	                            + crEx.getDesUnidad() + "thread:" + crEx.getIdThread());
	                    crEx = actualizarReporteEnCola(crEx, BaseConstants.ESTATUS_PROCESANDO);
	
	                    // if (crEx != null && crEx.getDesUnidad() != null && !crEx.getDesUnidad().equals("")) {
//	                    logger.debug("DESPUES DE ACTUALIZAR PROCESANDO crEx:" + crEx.getCvePeriodo() + "descripcion:"
//	                            + crEx.getDesUnidad() + "thread:" + crEx.getIdThread());
	                    reportesTomados++;
	                    reportesEjecutar.add(crEx);
	                }
	
	            }
            }

            // Se busca al finalizar cuantos reportes pendientes quedan
            Iterator<SitColaReportes> reportesPendientesFinales = colaDeReportesRepository.buscarReportesEnCola(
                    BaseConstants.ESTATUS_PENDIENTE, ReporteParteIConfiguration.INSTANCE.getLimiteReportesMinuto());
            if(reportesPendientesFinales != null) {
	            while (reportesPendientesFinales.hasNext()) {
	                logger.debug("REPORTES AUN PENDIENTES:" + reportesPendientesFinales.next().getCvePeriodo());
	            }
            }
            logger.debug("reportesTomados:" + reportesTomados);
            logger.debug("ReporteParteIConfiguration.INSTANCE.getLimiteReportesMinuto():"
                    + ReporteParteIConfiguration.INSTANCE.getLimiteReportesMinuto());
            // SI los reportes ejecutados por instancia son menor al l�mte y aun quedan reportes pendientes
            Iterator<SitColaReportes> reportesPendientesFin = colaDeReportesRepository.buscarReportesEnCola(
                    BaseConstants.ESTATUS_PENDIENTE, ReporteParteIConfiguration.INSTANCE.getLimiteReportesMinuto());
            if (reportesTomados < ReporteParteIConfiguration.INSTANCE.getLimiteReportesMinuto()
                    && (reportesPendientesFin != null && reportesPendientesFin.hasNext())) {
                logger.debug("ENTRA DE NUEVO a  buscarReportesEnColaParalelo()***********");
                buscarReportesEnColaParalelo();
            }
            // Procesar reportes
            else {
                logger.debug("SE GENERAN LOS REPORTES***********");
                for (SitColaReportes reporte : reportesEjecutar) {
                    logger.debug("REPORTES a ejecutar:" + reporte.getCvePeriodo());
                }
                generarReportePorThread(reportesEjecutar);
            }

        }
        contadorMin++;
    }

    private void generarReportePorThread(List<SitColaReportes> reportesProcesando)
            throws ReporteParteIException, InterruptedException {

        for (SitColaReportes reporte : reportesProcesando) {
            // Se buscan los reportes en la cola
            SitColaReportes re = colaDeReportesRepository.buscarReporteExistenteEnCola(reporte.getCvePresupuestal(),
                    reporte.getCvePeriodo(), reporte.getEstatus(), reporte.getIdThread());
            logger.debug("GENERAR REPORTE re:" + re.getCvePeriodo() + "descripcion:" + re.getDesUnidad()
                    + "thread:" + re.getIdThread());
            SdVistas sdVistas = procesarReporteParteIService.extraerInfoMedicosReporte(re.getCvePresupuestal(),
                    re.getDesUnidad(), re.getCvePeriodo());
            SitReporteParteI sitReporteParteI = new SitReporteParteI();
            sitReporteParteI.setFecGeneracion(new Date());
            sitReporteParteI.setSdVistas(sdVistas);
            sitReporteParteI.setSdPeriodoReporte(procesarReporteParteIService.getPeriodoReporte());
            sitReporteParteI.setSdUnidadMedica(procesarReporteParteIService.getUnidadMedica());
            SitReporteParteI reporteBorrar = procesarReporteParteIService.buscarReporteParte1(re.getCvePresupuestal(),
                    re.getCvePeriodo());
            SitColaReportes reporteCola = buscarReporteExistenteEnCola(re.getCvePresupuestal(), re.getCvePeriodo(),
                    BaseConstants.ESTATUS_PROCESANDO, re.getIdThread());
            if (reporteCola.getDesUnidad() != null && !reporteCola.getDesUnidad().equals("")
                    && reporteCola.getFecInicio() != null) {
                if (reporteBorrar != null) {
                    if (procesarReporteParteIService.borrarReporteParte1(reporteBorrar)) {
                        procesarReporteParteIService.insertarReporte(sitReporteParteI);
                        // ACTUALIZAR EN COLA DE REPORTES

                        SitColaReportes rep = actualizarReporteEnCola(reporteCola, BaseConstants.ESTATUS_FINALIZADO);
                        actualizarControlCapturaCe(Boolean.TRUE, re.getCvePresupuestal(), re.getDesUnidad(),
                                re.getCvePeriodo());
                        if (rep != null) {
                            logger.debug("---Se finalizo presupuestal:" + rep.getCvePresupuestal() + " periodo:"
                                    + rep.getCvePeriodo() + " a las:" + rep.getFecFin());
                        }

                    }
                } else {
                    procesarReporteParteIService.insertarReporte(sitReporteParteI);
                    // ACTUALIZAR EN COLA DE REPORTES

                    SitColaReportes rep = actualizarReporteEnCola(reporteCola, BaseConstants.ESTATUS_FINALIZADO);
                    if (rep != null) {
                        actualizarControlCapturaCe(Boolean.TRUE, re.getCvePresupuestal(), re.getDesUnidad(),
                                re.getCvePeriodo());
                        logger.debug("---Se finalizo presupuestal:" + rep.getCvePresupuestal() + " periodo:"
                                + rep.getCvePeriodo() + " a las:" + rep.getFecFin());
                    }

                }
            } else {
                actualizarReporteEnCola(reporteCola, BaseConstants.ESTATUS_REPETIDO);
            }

        }
        logger.info("Terminado");
    }

    @Override
    public Iterator<SitColaReportes> buscarReportesEnColaActivos(Integer estatus1, Integer estatus2, Integer estatus3) {

        return colaDeReportesRepository.buscarReportesEnColaActivos(estatus1, estatus2, estatus3);
    }

    @Override
    public SitColaReportes buscarReporteExistenteEnCola(String cvePresupuestal, Integer clavePeriodo, Integer estatus,
            Long idThread) {

        return colaDeReportesRepository.buscarReporteExistenteEnCola(cvePresupuestal, clavePeriodo, estatus, idThread);
    }

    @Override
    public Boolean insertarReporteEnCola(String cvePresupuestal, String desUnidad, Integer clavePeriodo,
            Integer volumen) {

        return colaDeReportesRepository.insertarReporteEnCola(cvePresupuestal, desUnidad, clavePeriodo, volumen);
    }

    @Override
    public SitColaReportes actualizarReporteEnCola(SitColaReportes reporteEnCola, Integer estatusFinal)
            throws ReporteParteIException {

        return colaDeReportesRepository.actualizarReporteEnCola(reporteEnCola, estatusFinal);
    }

    @Override
    public Integer tiempoProcesarReporte(Integer promRegistros) {

        logger.debug("promRegistros:" + promRegistros);
        Integer tiempo = 0;

        Integer totalVolumenReportes = 0;
        // se buscan la cantidad de reportes en cola, para sacar un timepo promedio que tardara
        // en generarse el reporte
        Iterator<SitColaReportes> reportesEc = buscarReportesEnColaActivos(BaseConstants.ESTATUS_PENDIENTE,
                BaseConstants.ESTATUS_PROCESANDO, null);

        if (promRegistros == null || promRegistros == 0) {
            promRegistros = ReporteParteIConfiguration.INSTANCE.getRegistrosPromedioDefault();
        }
        Integer clasificacion = promRegistros / ReporteParteIConfiguration.INSTANCE.getRegistrsosTamanioUnidad();

        if (reportesEc != null) {
            while (reportesEc.hasNext()) {
                SitColaReportes rep = reportesEc.next();
                logger.debug("Es nulo??  rep:" + rep);
                if (rep.getClasificacionVolumen() != null) {
                    totalVolumenReportes = totalVolumenReportes + rep.getClasificacionVolumen();
                } else {
                    totalVolumenReportes = promRegistros;
                }

            }
        }
        // De acuerdo al total de reportes se calcula el timepo

        tiempo = totalVolumenReportes / ReporteParteIConfiguration.INSTANCE.getLimiteReportesMinuto();
        tiempo = tiempo + ReporteParteIConfiguration.INSTANCE.getTimepoEstandar() + clasificacion;
        logger.debug("timepo:" + tiempo);
        logger.debug("timepo:" + tiempo);
        return tiempo;
    }

}
