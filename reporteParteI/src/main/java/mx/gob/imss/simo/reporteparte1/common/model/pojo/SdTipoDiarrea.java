/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdTipoDiarrea {

	private String cveTipoDiarrea;
	private String desTipoDiarrea;

	/**
	 * @return the cveTipoDiarrea
	 */
	public String getCveTipoDiarrea() {
		return cveTipoDiarrea;
	}

	/**
	 * @param cveTipoDiarrea
	 *            the cveTipoDiarrea to set
	 */
	public void setCveTipoDiarrea(String cveTipoDiarrea) {
		this.cveTipoDiarrea = cveTipoDiarrea;
	}

	/**
	 * @return the desTipoDiarrea
	 */
	public String getDesTipoDiarrea() {
		return desTipoDiarrea;
	}

	/**
	 * @param desTipoDiarrea
	 *            the desTipoDiarrea to set
	 */
	public void setDesTipoDiarrea(String desTipoDiarrea) {
		this.desTipoDiarrea = desTipoDiarrea;
	}

}
