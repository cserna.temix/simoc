package mx.gob.imss.simo.reporteparte1.common.services;

import mx.gob.imss.simo.reporteparte1.common.model.SitUnidadMedica;

public interface ReporteParte1CommonService {

    public SitUnidadMedica buscarUnidadMedica(String cvePresupuestal);

    public void actualizarControlCapturaCe(Boolean consutaExternaUr, String cvePresupuestal, String desPresupuestal,
            Integer periodo);

}
