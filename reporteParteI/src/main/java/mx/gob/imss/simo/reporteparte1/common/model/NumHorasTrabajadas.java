package mx.gob.imss.simo.reporteparte1.common.model;

import lombok.Data;

@Data
public class NumHorasTrabajadas {

	String horas;
}
