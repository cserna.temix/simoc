/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdTipoUrgencia {

	private Integer cveTipoUrgencia;
	private String desTipoUrgencia;

	/**
	 * @return the cveTipoUrgencia
	 */
	public Integer getCveTipoUrgencia() {
		return cveTipoUrgencia;
	}

	/**
	 * @param cveTipoUrgencia
	 *            the cveTipoUrgencia to set
	 */
	public void setCveTipoUrgencia(Integer cveTipoUrgencia) {
		this.cveTipoUrgencia = cveTipoUrgencia;
	}

	/**
	 * @return the desTipoUrgencia
	 */
	public String getDesTipoUrgencia() {
		return desTipoUrgencia;
	}

	/**
	 * @param desTipoUrgencia
	 *            the desTipoUrgencia to set
	 */
	public void setDesTipoUrgencia(String desTipoUrgencia) {
		this.desTipoUrgencia = desTipoUrgencia;
	}

}
