package mx.gob.imss.simo.reporteparte1.reporte.rules;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteIAccidentesyL;
import mx.gob.imss.simo.reporteparte1.common.model.NumHorasTrabajadas;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdRenglonesEspecialesVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdTotalesReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdV2Especialidad;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdV2Medico;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista2;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;

@Component
public class ProcesarReporteParteIRules {

    DecimalFormat df = new DecimalFormat("#.##");

    public SdVistas crearInformacionReporte(Iterator<ExtractoReporteParteI> extraccionReporte, Integer tipoConsulta) {

        Integer horT = 0;
        Integer minT = 0;
        Integer numMedicos = 0;
        Integer tiempoTrabajadoDias = 0;
        Integer totalConsultas = 0;
        Integer num1EraVez = 0;
        Integer numNoOtorgadas = 0;
        Integer numVisitas = 0;
        Integer numCitas = 0;
        Integer numCitasCumplidas = 0;
        Integer numEnUnidad = 0;
        Integer numOtraUnidad = 0;
        Integer numAltasEspeciales = 0;
        Integer numExpedidas = 0;
        Integer numDias = 0;
        Integer numRecetas = 0;

        SdVista1 vista1 = new SdVista1();
        SdVista2 vista2 = new SdVista2();
        List<SdV2Especialidad> listaEspecialidades = new ArrayList<>();

        Integer ind = null;

        while (extraccionReporte.hasNext()) {

            ExtractoReporteParteI rp = extraccionReporte.next();

            // ID
            SdV2Especialidad especialidadV2 = null;

            especialidadV2 = buscarEspecialidad(listaEspecialidades, rp.getGroupId().getEsp().get(0));

            if (especialidadV2 == null) {
                especialidadV2 = new SdV2Especialidad();
                especialidadV2.setCveEspecialidad(rp.getGroupId().getEsp().get(0));
                especialidadV2.setDesEspecialidad(rp.getDesEsp());
                listaEspecialidades.add(especialidadV2);
                ind = listaEspecialidades.indexOf(especialidadV2);
                List<SdV2Medico> listaMedicos = new ArrayList<>();
                listaEspecialidades.get(ind).setSdV2Medico(listaMedicos);
            } else {
                ind = listaEspecialidades.indexOf(especialidadV2);
            }

            // Totales por Medico

            SdV2Medico medicoV2 = new SdV2Medico();
            medicoV2.setCveMatricula(rp.getGroupId().getMatricula().get(0));
            medicoV2.setRefApellidoPaterno("");
            medicoV2.setRefApellidoMaterno("");
            medicoV2.setRefNombre("");
            medicoV2 = validarNombreMed(rp, medicoV2);
            StringBuilder refVistaMedicoB = new StringBuilder();

            refVistaMedicoB.append(medicoV2.getRefNombre());
            refVistaMedicoB.append(" ");
            refVistaMedicoB.append(medicoV2.getRefApellidoPaterno());
            refVistaMedicoB.append(" ");
            refVistaMedicoB.append(medicoV2.getRefApellidoMaterno());

            // ��refVistaMedico es la concatenacion???
            medicoV2.setRefVistaMedico(refVistaMedicoB.toString());

            // Sumar Horas
            Integer hor = 0;
            Integer min = 0;

            List<Integer> tTiempo = capturarTotalHoras(rp.getAmTiempoTraHoras());
            hor = tTiempo.get(0);
            min = tTiempo.get(1);
            hor = hor + min / 60;
            min = min % 60;
            medicoV2.setTiempoTrabajadoHoras(obtenerHorasTrabajadas(hor, min));

            // Totales

            medicoV2.setTiempoHoras(hor);
            medicoV2.setTiempoMinutos(min);
            medicoV2.setTiempoTrabajadoDias(rp.getAmTiempoTraDias());
            medicoV2.setTotalConsultas(rp.getAmTotCons());
            medicoV2.setNum1EraVez(rp.getAm1Vez());
            if (tipoConsulta == BaseConstants.TC_TOCOCIRUGIA || tipoConsulta == BaseConstants.TC_URGENCIAS) {
                medicoV2.setNum1EraVez(medicoV2.getTotalConsultas());
            }

            medicoV2.setNumNoOtorgadas(rp.getAmNoOtorgadas());
            medicoV2.setPromedioHoras(obtenerPromedioHoras(medicoV2.getTiempoHoras(), medicoV2.getTiempoMinutos(),
                    medicoV2.getTotalConsultas()));
            medicoV2.setNumVisitas(rp.getAmNumVisitas());
            medicoV2.setNumCitas(rp.getAmCitas());
            medicoV2.setNumCitasCumplidas(rp.getAmCitasCumplidas());
            medicoV2.setNumEnUnidad(rp.getPeEnUnidad());
            medicoV2.setNumOtraUnidad(rp.getPeOtraUnidad());
            medicoV2.setNumAltasEspeciales(rp.getPeAltasEspeciales());
            medicoV2.setNumExpedidas(rp.getIncExpedidas());
            medicoV2.setNumDias(rp.getIncDias());
            medicoV2.setPromedio(rp.getIncProm());
            medicoV2.setNumRecetas(rp.getRecNum());
            medicoV2.setPorcentajeAtenciones(rp.getRecPorcentajeAten());

            // Se SUMAN los valores para la vista 1
            numMedicos = numMedicos + 1;
            tiempoTrabajadoDias = tiempoTrabajadoDias + medicoV2.getTiempoTrabajadoDias();
            totalConsultas = totalConsultas + medicoV2.getTotalConsultas();
            num1EraVez = num1EraVez + medicoV2.getNum1EraVez();
            numNoOtorgadas = numNoOtorgadas + medicoV2.getNumNoOtorgadas();
            numVisitas = numVisitas + medicoV2.getNumVisitas();
            numCitas = numCitas + medicoV2.getNumCitas();
            numCitasCumplidas = numCitasCumplidas + medicoV2.getNumCitasCumplidas();
            numEnUnidad = numEnUnidad + medicoV2.getNumEnUnidad();
            numOtraUnidad = numOtraUnidad + medicoV2.getNumOtraUnidad();
            numAltasEspeciales = numAltasEspeciales + medicoV2.getNumAltasEspeciales();
            numExpedidas = numExpedidas + medicoV2.getNumExpedidas();
            numDias = numDias + medicoV2.getNumDias();
            numRecetas = numRecetas + medicoV2.getNumRecetas();

            // Suman horas
            horT = horT + hor;
            minT = minT + min;
            listaEspecialidades.get(ind).setTipoServicio(getTipoServicioString(tipoConsulta));
            listaEspecialidades.get(ind).setOrdenServicioReporte(tipoConsulta);
            listaEspecialidades.get(ind).getSdV2Medico().add(medicoV2);

        }

        horT = horT + minT / 60;
        minT = minT % 60;

        vista2.setSdV2Especialidad(listaEspecialidades);

        SdTotalesReporte especVista1 = new SdTotalesReporte();

        especVista1.setNumMedicos(numMedicos);
        especVista1.setTiempoTrabajadoHoras(obtenerHorasTrabajadas(horT, minT));
        especVista1.setTiempoHoras(horT);
        especVista1.setTiempoMinutos(minT);
        especVista1.setNum1EraVez(num1EraVez);
        especVista1.setTiempoTrabajadoDias(tiempoTrabajadoDias);
        especVista1.setTotalConsultas(totalConsultas);
        especVista1.setNum1EraVez(num1EraVez);
        especVista1.setNumNoOtorgadas(numNoOtorgadas);

        especVista1.setPromedioHoras(obtenerPromedioHoras(especVista1.getTiempoHoras(), especVista1.getTiempoMinutos(),
                especVista1.getTotalConsultas()));

        especVista1.setNumVisitas(numVisitas);
        especVista1.setNumCitas(numCitas);
        especVista1.setNumCitasCumplidas(numCitasCumplidas);
        especVista1.setNumEnUnidad(numEnUnidad);
        especVista1.setNumOtraUnidad(numOtraUnidad);
        especVista1.setNumAltasEspeciales(numAltasEspeciales);
        especVista1.setNumExpedidas(numExpedidas);
        especVista1.setNumDias(numDias);
        especVista1.setPromedio(obtenerPromedioIncapacidades(especVista1.getNumExpedidas(), especVista1.getNumDias()));

        especVista1.setNumRecetas(numRecetas);
        especVista1.setPorcentajeAtenciones(
                obtenerPorcentajeAtenciones(especVista1.getTotalConsultas(), especVista1.getNumRecetas()));

        vista1.setSdRepEspecialidades(especVista1);
        List<SdVista1> listaSdVista1 = new ArrayList<>();
        List<SdVista2> listaSdVista2 = new ArrayList<>();
        List<SdVistas> listaSdVistas = new ArrayList<>();
        SdVistas sdVistas = new SdVistas();
        listaSdVista1.add(vista1);
        listaSdVista2.add(vista2);

        sdVistas.setSdVista1(vista1);
        sdVistas.setSdVista2(vista2);

        listaSdVistas.add(sdVistas);

        return sdVistas;

    }

    public SdVistas crearInformacionReporteParamedicos(Iterator<ExtractoReporteParteI> extraccionReporte,
            Integer tipoConsulta) {

        Integer horT = 0;
        Integer minT = 0;

        List<Integer> listaNumMedicos = new ArrayList<>();
        List<Integer> listaHoras = new ArrayList<>();
        List<Integer> listaMinutos = new ArrayList<>();
        List<Integer> listaTiempoTrabajadoDias = new ArrayList<>();
        List<Integer> listaTotalConsultas = new ArrayList<>();
        List<Integer> listaNum1EraVez = new ArrayList<>();
        List<Integer> listaNumNoOtorgadas = new ArrayList<>();
        List<Double> listaPromedioHoras = new ArrayList<>();
        List<Integer> listaNumVisitas = new ArrayList<>();
        List<Integer> listaNumCitas = new ArrayList<>();
        List<Integer> listaNumCitasCumplidas = new ArrayList<>();
        List<Integer> listaNumEnUnidad = new ArrayList<>();
        List<Integer> listaNumOtraUnidad = new ArrayList<>();
        List<Integer> listaNumAltasEspeciales = new ArrayList<>();
        List<Integer> listaNumExpedidas = new ArrayList<>();
        List<Integer> listaNumDias = new ArrayList<>();
        List<Double> listaPromedio = new ArrayList<>();
        List<Integer> listaNumRecetas = new ArrayList<>();
        List<Double> listaPorcentajeAtenciones = new ArrayList<>();

        SdVista1 vista1 = new SdVista1();
        SdVista2 vista2 = new SdVista2();
        List<SdV2Especialidad> listaEspecialidades = new ArrayList<>();

        Integer ind = null;

        while (extraccionReporte.hasNext()) {

            ExtractoReporteParteI rp = extraccionReporte.next();

            // ID
            SdV2Especialidad especialidadV2 = null;

            especialidadV2 = buscarEspecialidad(listaEspecialidades, rp.getGroupId().getEsp().get(0));

            if (especialidadV2 == null) {
                especialidadV2 = new SdV2Especialidad();
                especialidadV2.setCveEspecialidad(rp.getGroupId().getEsp().get(0));
                especialidadV2.setDesEspecialidad(rp.getDesEsp());
                listaEspecialidades.add(especialidadV2);
                ind = listaEspecialidades.indexOf(especialidadV2);
                List<SdV2Medico> listaMedicos = new ArrayList<>();
                listaEspecialidades.get(ind).setSdV2Medico(listaMedicos);

                // se agrega elemento
                listaNumMedicos.add(0);
                listaHoras.add(0);
                listaMinutos.add(0);
                listaTiempoTrabajadoDias.add(0);
                listaTotalConsultas.add(0);
                listaNum1EraVez.add(0);
                listaNumNoOtorgadas.add(0);
                listaPromedioHoras.add(0.0);
                listaNumVisitas.add(0);
                listaNumCitas.add(0);
                listaNumCitasCumplidas.add(0);
                listaNumEnUnidad.add(0);
                listaNumOtraUnidad.add(0);
                listaNumAltasEspeciales.add(0);
                listaNumExpedidas.add(0);
                listaNumDias.add(0);
                listaPromedio.add(0.0);
                listaNumRecetas.add(0);
                listaPorcentajeAtenciones.add(0.0);
            } else {
                ind = listaEspecialidades.indexOf(especialidadV2);
            }

            // Totales por Medico

            SdV2Medico medicoV2 = new SdV2Medico();
            medicoV2.setCveMatricula(rp.getGroupId().getMatricula().get(0));
            medicoV2.setRefApellidoPaterno("");
            medicoV2.setRefApellidoMaterno("");
            medicoV2.setRefNombre("");
            medicoV2 = validarNombreMed(rp, medicoV2);
            StringBuilder refVistaMedicoB = new StringBuilder();

            refVistaMedicoB.append(medicoV2.getRefNombre());
            refVistaMedicoB.append(" ");
            refVistaMedicoB.append(medicoV2.getRefApellidoPaterno());
            refVistaMedicoB.append(" ");
            refVistaMedicoB.append(medicoV2.getRefApellidoMaterno());

            medicoV2.setRefVistaMedico(refVistaMedicoB.toString());

            // Sumar Horas
            Integer hor = 0;
            Integer min = 0;

            List<Integer> tTiempo = capturarTotalHoras(rp.getAmTiempoTraHoras());
            hor = tTiempo.get(0);
            min = tTiempo.get(1);
            hor = hor + min / 60;
            min = min % 60;
            medicoV2.setTiempoTrabajadoHoras(obtenerHorasTrabajadas(hor, min));

            // Totales
            medicoV2.setTiempoTrabajadoDias(rp.getAmTiempoTraDias());
            medicoV2.setTotalConsultas(rp.getAmTotCons());
            medicoV2.setNum1EraVez(rp.getAm1Vez());
            medicoV2.setNumNoOtorgadas(rp.getAmNoOtorgadas());
            medicoV2.setPromedioHoras(0.0);
            medicoV2.setNumVisitas(rp.getAmNumVisitas());
            medicoV2.setNumCitas(rp.getAmCitas());
            medicoV2.setNumCitasCumplidas(rp.getAmCitasCumplidas());
            medicoV2.setNumEnUnidad(rp.getPeEnUnidad());
            medicoV2.setNumOtraUnidad(rp.getPeOtraUnidad());
            medicoV2.setNumAltasEspeciales(rp.getPeAltasEspeciales());
            medicoV2.setNumExpedidas(rp.getIncExpedidas());
            medicoV2.setNumDias(rp.getIncDias());
            medicoV2.setPromedio(rp.getIncProm());
            medicoV2.setNumRecetas(rp.getRecNum());
            medicoV2.setPorcentajeAtenciones(rp.getRecPorcentajeAten());

            // Suman horas
            horT = horT + hor;
            minT = minT + min;
            if (medicoV2.getTiempoHoras() == null) {
                medicoV2.setTiempoHoras(0);
                medicoV2.setTiempoMinutos(0);
            }
            medicoV2.setTiempoHoras(medicoV2.getTiempoHoras() + hor);
            medicoV2.setTiempoMinutos(medicoV2.getTiempoMinutos() + min);
            medicoV2.setPromedioHoras(obtenerPromedioHoras(medicoV2.getTiempoHoras(), medicoV2.getTiempoMinutos(),
                    medicoV2.getTotalConsultas()));

            // Se suman valores por especialidad
            listaNumMedicos.set(ind, listaNumMedicos.get(ind) + 1);

            listaTiempoTrabajadoDias.set(ind, listaTiempoTrabajadoDias.get(ind) + medicoV2.getTiempoTrabajadoDias());
            listaTotalConsultas.set(ind, listaTotalConsultas.get(ind) + medicoV2.getTotalConsultas());
            listaNum1EraVez.set(ind, listaNum1EraVez.get(ind) + medicoV2.getNum1EraVez());
            listaNumNoOtorgadas.set(ind, listaNumNoOtorgadas.get(ind) + medicoV2.getNumNoOtorgadas());
            // Se obtiene el promedio de horas totalconsultas/totalhoras

            listaPromedioHoras.set(ind, listaPromedioHoras.get(ind) + medicoV2.getPromedioHoras());
            listaNumVisitas.set(ind, listaNumVisitas.get(ind) + medicoV2.getNumVisitas());
            listaNumCitas.set(ind, listaNumCitas.get(ind) + medicoV2.getNumCitas());
            listaNumCitasCumplidas.set(ind, listaNumCitasCumplidas.get(ind) + medicoV2.getNumCitasCumplidas());
            listaNumEnUnidad.set(ind, listaNumEnUnidad.get(ind) + medicoV2.getNumEnUnidad());
            listaNumOtraUnidad.set(ind, listaNumOtraUnidad.get(ind) + medicoV2.getNumOtraUnidad());
            listaNumAltasEspeciales.set(ind, listaNumAltasEspeciales.get(ind) + medicoV2.getNumAltasEspeciales());
            listaNumExpedidas.set(ind, listaNumExpedidas.get(ind) + medicoV2.getNumExpedidas());
            listaNumDias.set(ind, listaNumDias.get(ind) + medicoV2.getNumDias());
            listaPromedio.set(ind, listaPromedio.get(ind) + medicoV2.getPromedio());
            listaNumRecetas.set(ind, listaNumRecetas.get(ind) + medicoV2.getNumRecetas());
            listaPorcentajeAtenciones.set(ind, listaPorcentajeAtenciones.get(ind) + medicoV2.getPorcentajeAtenciones());

            listaHoras.set(ind, medicoV2.getTiempoHoras());
            listaMinutos.set(ind, medicoV2.getTiempoMinutos());
            listaEspecialidades.get(ind).getSdV2Medico().add(medicoV2);
            listaEspecialidades.get(ind).setTipoServicio(getTipoServicioString(tipoConsulta));
            listaEspecialidades.get(ind).setOrdenServicioReporte(tipoConsulta);

        }

        vista2.setSdV2Especialidad(listaEspecialidades);

        int indice = 0;
        List<SdTotalesReporte> listTotalReportes = new ArrayList<>();
        for (SdV2Especialidad espP : listaEspecialidades) {

            SdTotalesReporte sdm = new SdTotalesReporte();
            // Especialidad

            Integer horasM = 0;
            Integer minutosM = 0;
            for (SdV2Medico medico : espP.getSdV2Medico()) {
                horasM = horasM + medico.getTiempoHoras();
                minutosM = minutosM + medico.getTiempoMinutos();
            }

            sdm.setTiempoHoras(horasM);
            sdm.setTiempoMinutos(minutosM);
            sdm.setTiempoTrabajadoHoras(obtenerHorasTrabajadas(sdm.getTiempoHoras(), sdm.getTiempoMinutos()));
            sdm.setCveEspecialidad(espP.getCveEspecialidad());
            sdm.setDesEspecialidad(espP.getDesEspecialidad());
            sdm.setRefVistaEspecialidad(espP.getRefVistaEspecialidad());

            // Se agregan a cada especialidad los totales de paramedicos
            sdm.setNumMedicos(listaNumMedicos.get(indice));
            sdm.setTiempoTrabajadoDias(listaTiempoTrabajadoDias.get(indice));
            sdm.setTotalConsultas(listaTotalConsultas.get(indice));
            sdm.setNum1EraVez(listaNum1EraVez.get(indice));
            sdm.setNumNoOtorgadas(listaNumNoOtorgadas.get(indice));
            sdm.setPromedioHoras(
                    obtenerPromedioHoras(sdm.getTiempoHoras(), sdm.getTiempoMinutos(), sdm.getTotalConsultas()));
            sdm.setNumVisitas(listaNumVisitas.get(indice));
            sdm.setNumCitas(listaNumCitas.get(indice));
            sdm.setNumCitasCumplidas(listaNumCitasCumplidas.get(indice));
            sdm.setNumEnUnidad(listaNumEnUnidad.get(indice));
            sdm.setNumOtraUnidad(listaNumOtraUnidad.get(indice));
            sdm.setNumAltasEspeciales(listaNumAltasEspeciales.get(indice));
            sdm.setNumExpedidas(listaNumExpedidas.get(indice));
            sdm.setNumDias(listaNumDias.get(indice));
            sdm.setPromedio(Math.round(listaPromedio.get(indice) * 100.0) / 100.0);
            sdm.setNumRecetas(listaNumRecetas.get(indice));
            sdm.setPorcentajeAtenciones((Math.round(listaPorcentajeAtenciones.get(indice) * 100.0) / 100.0) * 100);

            indice++;
            listTotalReportes.add(sdm);
        }
        SdRenglonesEspecialesVista1 renglones = new SdRenglonesEspecialesVista1();
        renglones.setSdParamedicos(listTotalReportes);

        vista1.setSdRenglonesEspeciales(renglones);
        SdVistas sdVistas = new SdVistas();
        sdVistas.setSdVista1(vista1);
        sdVistas.setSdVista2(vista2);

        return sdVistas;

    }

    private SdV2Medico validarNombreMed(ExtractoReporteParteI rp, SdV2Medico medicoV2) {

 /*       if (rp.getGroupId().getApP() != null && !rp.getGroupId().getApP().isEmpty()) {
            medicoV2.setRefApellidoPaterno(rp.getGroupId().getApP().get(0));
        }
        if (rp.getGroupId().getApM() != null && !rp.getGroupId().getApM().isEmpty()) {
            medicoV2.setRefApellidoMaterno(rp.getGroupId().getApM().get(0));
        }
        if (rp.getGroupId().getNom() != null && !rp.getGroupId().getNom().isEmpty()) {
            medicoV2.setRefNombre(rp.getGroupId().getNom().get(0));
        } */

        
        if (rp.getMedico().getApP() != null && !rp.getMedico().getApP().isEmpty()) {
            medicoV2.setRefApellidoPaterno(rp.getMedico().getApP().get(0));
        }
        else {
        	medicoV2.setRefApellidoPaterno("");
        }
        if (rp.getMedico().getApM() != null && !rp.getMedico().getApM().isEmpty()) {
            medicoV2.setRefApellidoMaterno(rp.getMedico().getApM().get(0));
        }
        else {
        	medicoV2.setRefApellidoMaterno("");
        }
        if (rp.getMedico().getNom() != null && !rp.getMedico().getNom().isEmpty()) {
            medicoV2.setRefNombre(rp.getMedico().getNom().get(0));
        }
        else {
        	medicoV2.setRefNombre("");
        }
        
        return medicoV2;
    }

    public SdTotalesReporte extraccionReporteAccidentesYles(
            Iterator<ExtractoReporteParteIAccidentesyL> extraccionReporte) {

        Integer totalConsultas = 0;

        Integer numEnUnidad = 0;
        Integer numOtraUnidad = 0;
        Integer numAltasEspeciales = 0;
        Integer numExpedidas = 0;
        Integer numDias = 0;
        Double promedio = 0.0;
        Integer numRecetas = 0;
        Double porcentajeAtenciones = 0.0;

        SdTotalesReporte rep = new SdTotalesReporte();

        while (extraccionReporte.hasNext()) {

            ExtractoReporteParteIAccidentesyL rp = extraccionReporte.next();

            // Totales

            rep.setTiempoTrabajadoDias(rp.getAmTiempoTraDias());
            rep.setTotalConsultas(rp.getAmTotCons());
            rep.setNum1EraVez(rp.getAm1Vez());
            rep.setNumNoOtorgadas(rp.getAmNoOtorgadas());
            rep.setPromedioHoras(0.0);
            rep.setNumVisitas(rp.getAmNumVisitas());
            rep.setNumCitas(rp.getAmCitas());
            rep.setNumCitasCumplidas(rp.getAmCitasCumplidas());
            rep.setNumEnUnidad(rp.getPeEnUnidad());
            rep.setNumOtraUnidad(rp.getPeOtraUnidad());
            rep.setNumAltasEspeciales(rp.getPeAltasEspeciales());
            rep.setNumExpedidas(rp.getIncExpedidas());
            rep.setNumDias(rp.getIncDias());
            rep.setPromedio(rp.getIncProm());
            rep.setNumRecetas(rp.getRecNum());
            rep.setPorcentajeAtenciones(rp.getRecPorcentajeAten());

            // Se SUMAN los valores

            totalConsultas = totalConsultas + rep.getTotalConsultas();

            numEnUnidad = numEnUnidad + rep.getNumEnUnidad();
            numOtraUnidad = numOtraUnidad + rep.getNumOtraUnidad();
            numAltasEspeciales = numAltasEspeciales + rep.getNumAltasEspeciales();
            numExpedidas = numExpedidas + rep.getNumExpedidas();
            numDias = numDias + rep.getNumDias();
            promedio = promedio + rep.getPromedio();
            numRecetas = numRecetas + rep.getNumRecetas();
            porcentajeAtenciones = porcentajeAtenciones + rep.getPorcentajeAtenciones();

        }

        SdTotalesReporte especVistaAccYles = new SdTotalesReporte();
        especVistaAccYles.setNumMedicos(0);
        especVistaAccYles.setTiempoTrabajadoHoras(obtenerHorasTrabajadas(0, 0));
        especVistaAccYles.setTiempoHoras(0);
        especVistaAccYles.setTiempoMinutos(0);
        especVistaAccYles.setNum1EraVez(0);
        especVistaAccYles.setTiempoTrabajadoDias(0);
        especVistaAccYles.setTotalConsultas(totalConsultas);
        especVistaAccYles.setNum1EraVez(0);
        especVistaAccYles.setNumNoOtorgadas(0);
        especVistaAccYles.setPromedioHoras(0.0);
        especVistaAccYles.setNumVisitas(0);
        especVistaAccYles.setNumCitas(0);
        especVistaAccYles.setNumCitasCumplidas(0);
        especVistaAccYles.setNumEnUnidad(numEnUnidad);
        especVistaAccYles.setNumOtraUnidad(numOtraUnidad);
        especVistaAccYles.setNumAltasEspeciales(numAltasEspeciales);
        especVistaAccYles.setNumExpedidas(numExpedidas);
        especVistaAccYles.setNumDias(numDias);
        especVistaAccYles.setPromedio(Math.round(promedio * 100.0) / 100.0);
        especVistaAccYles.setNumRecetas(numRecetas);
        especVistaAccYles.setPorcentajeAtenciones(
                obtenerPorcentajeAtenciones(especVistaAccYles.getTotalConsultas(), especVistaAccYles.getNumRecetas()));

        return especVistaAccYles;

    }

    public SdTotalesReporte sumarTotales(SdTotalesReporte primera, SdTotalesReporte segunda) {

        SdTotalesReporte suma = new SdTotalesReporte();
        suma.setNumMedicos(primera.getNumMedicos() + segunda.getNumMedicos());
        suma.setTiempoHoras(primera.getTiempoHoras() + segunda.getTiempoHoras());
        suma.setTiempoMinutos(primera.getTiempoMinutos() + segunda.getTiempoMinutos());
        if (suma.getTiempoMinutos() >= 60) {
            int horas = suma.getTiempoMinutos() / 60;
            suma.setTiempoHoras(suma.getTiempoHoras() + horas);
            suma.setTiempoMinutos(suma.getTiempoMinutos() % 60);
        }
        suma.setTiempoTrabajadoHoras(obtenerHorasTrabajadas(suma.getTiempoHoras(), suma.getTiempoMinutos()));
        suma.setTiempoTrabajadoDias(primera.getTiempoTrabajadoDias() + segunda.getTiempoTrabajadoDias());
        suma.setTotalConsultas(primera.getTotalConsultas() + segunda.getTotalConsultas());
        suma.setNum1EraVez(primera.getNum1EraVez() + segunda.getNum1EraVez());
        suma.setNumNoOtorgadas(primera.getNumNoOtorgadas() + segunda.getNumNoOtorgadas());
        suma.setPromedioHoras(
                obtenerPromedioHoras(suma.getTiempoHoras(), suma.getTiempoMinutos(), suma.getTotalConsultas()));
        suma.setNumVisitas(primera.getNumVisitas() + segunda.getNumVisitas());
        suma.setNumCitas(primera.getNumCitas() + segunda.getNumCitas());
        suma.setNumCitasCumplidas(primera.getNumCitasCumplidas() + segunda.getNumCitasCumplidas());
        suma.setNumEnUnidad(primera.getNumEnUnidad() + segunda.getNumEnUnidad());
        suma.setNumOtraUnidad(primera.getNumOtraUnidad() + segunda.getNumOtraUnidad());
        suma.setNumAltasEspeciales(primera.getNumAltasEspeciales() + segunda.getNumAltasEspeciales());
        suma.setNumExpedidas(primera.getNumExpedidas() + segunda.getNumExpedidas());
        suma.setNumDias(primera.getNumDias() + segunda.getNumDias());
        suma.setPromedio(obtenerPromedioIncapacidades(suma.getNumExpedidas(), suma.getNumDias()));
        suma.setNumRecetas(primera.getNumRecetas() + segunda.getNumRecetas());
        suma.setPorcentajeAtenciones(obtenerPorcentajeAtenciones(suma.getTotalConsultas(), suma.getNumRecetas()));

        return suma;

    }

    public List<Integer> capturarTotalHoras(List<NumHorasTrabajadas> horas) {

        Integer tHoras = 0;
        Integer tMinutos = 0;

        for (NumHorasTrabajadas hora : horas) {
            String[] horasString = hora.getHoras().split(":");
            tHoras = tHoras + Integer.valueOf(horasString[0]);
            tMinutos = tMinutos + Integer.valueOf(horasString[1]);
        }

        tHoras = tHoras + tMinutos / 60;
        tMinutos = tMinutos % 60;

        List<Integer> horasYmin = new ArrayList<>();
        horasYmin.add(tHoras);
        horasYmin.add(tMinutos);

        return horasYmin;
    }

    /**
     * Obtiene el cmapo promedio de horas
     * 
     * @param horas
     * @param minutos
     * @param totalConsultas
     * @return
     */
    public Double obtenerPromedioHoras(Integer horas, Integer minutos, Integer totalConsultas) {

        if (horas > 0) {
            Double minutosHora = 0.0;
            if (minutos > 0) {
                minutosHora = minutos / 60.0;
            }
            return Math.floor(((float) totalConsultas / (horas + minutosHora)) * 100.0) / 100.0;
        }
        return 0.00;
    }

    /**
     * @param numIncExpedidas
     * @param numIncDias
     * @return
     */
    public Double obtenerPromedioIncapacidades(Integer numIncExpedidas, Integer numIncDias) {

        if (numIncExpedidas != 0) {
            return (Math.floor((float) numIncDias / numIncExpedidas * 100.0) / 100.0);
        }
        return 0.00;
    }

    public Double obtenerPorcentajeAtenciones(Integer totalConsultas, Integer numRecetas) {

        if (totalConsultas != 0) {

            return Double.valueOf(df.format((float) numRecetas / totalConsultas * 100));
        }
        return 0.00;
    }

    public String obtenerHorasTrabajadas(Integer totalHoras, Integer totalMinutos) {

        if (totalMinutos >= 60) {
            totalHoras = totalHoras + totalMinutos / 60;
            totalMinutos = totalMinutos % 60;
        }

        String horasStringAux = "";
        String minutosStringAux = "";
        if (totalHoras < 10) {
            horasStringAux = "0";
        }
        if (totalMinutos < 10) {
            minutosStringAux = "0";
        }
        return horasStringAux + totalHoras + ":" + minutosStringAux + totalMinutos;
    }

    /**
     * Regresa la descripci�n del tipo de Servicios
     * 
     * @param tipoConsulta
     * @return
     */
    public String getTipoServicioString(Integer tipoConsulta) {

        switch (tipoConsulta) {
            case BaseConstants.TC_ESPECIALIDADES:
                return BaseConstants.STRING_ESPECIALIDADES;
            case BaseConstants.TC_URGENCIAS:
                return BaseConstants.STRING_URGENCIAS;
            case BaseConstants.TC_TOCOCIRUGIA:
                return BaseConstants.STRING_TOCOCIRUGIA;
            case BaseConstants.TC_PARAMEDICOS:
                return BaseConstants.STRING_PARAMEDICOS;
            default:
                return "";
        }
    }

    /**
     * M�todo que busca una especialidad y regresa su posicion en la lista
     * 
     * @return
     */
    public SdV2Especialidad buscarEspecialidad(List<SdV2Especialidad> listaEspecialidades, String clave) {

        Optional<SdV2Especialidad> especialidad = listaEspecialidades.stream()
                .filter(p -> p.getCveEspecialidad().equals(clave)).findFirst();
        return especialidad.isPresent() ? especialidad.get() : null;

    }

}
