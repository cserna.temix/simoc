package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.List;

import lombok.Data;

@Data
public class AgrupacionId {
	List<String> matricula;
	List<String> nom;
	List<String> apP;
	List<String> apM;
	List<String> esp;
}
