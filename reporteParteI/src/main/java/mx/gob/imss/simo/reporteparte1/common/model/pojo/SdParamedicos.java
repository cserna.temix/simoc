package mx.gob.imss.simo.reporteparte1.common.model.pojo;

import lombok.Data;

@Data
public class SdParamedicos {
	
	private String cveEspecialidad;
	private String desEspecialidad;
	private String refVistaEspecialidad;
	private Integer numMedicos;
	private String tiempoTrabajadoHoras;
	private Integer tiempoTrabajadoDias;
	private Integer totalConsultas;
	private Integer num1EraVez;
	private Integer numNoOtorgadas;
	private Double promedioHoras;
	private Integer numVisitas;
	private Integer numCitas;
	private Integer numCitasCumplidas;
	private Integer numEnUnidad;
	private Integer numOtraUnidad;
	private Integer numAltasEspeciales;
	private Integer numExpedidas;
	private Integer numDias;
	private Double promedio;
	private Integer numRecetas;
	private Double porcentajeAtenciones;
	
}
