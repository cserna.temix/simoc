package mx.gob.imss.simo.reporteparte1.common.exception;

public class ReporteParteIException extends BaseException {

    public ReporteParteIException(String... claves) {

        super(claves);
    }

    public ReporteParteIException(String[] claves, Throwable throwable) {

        super(claves, throwable);
    }

    public ReporteParteIException(String[] claves, String[] args) {

        super(claves, args);
    }

    public ReporteParteIException(String[] claves, String[] args, Throwable throwable) {

        super(claves, args, throwable);
    }

}
