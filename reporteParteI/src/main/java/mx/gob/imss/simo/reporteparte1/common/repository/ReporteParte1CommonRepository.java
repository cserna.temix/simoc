package mx.gob.imss.simo.reporteparte1.common.repository;

import java.util.Date;

import mx.gob.imss.simo.reporteparte1.common.model.SitControlCapturaCE;
import mx.gob.imss.simo.reporteparte1.common.model.SitUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicPeriodosImss;

public interface ReporteParte1CommonRepository {

    public SitUnidadMedica buscarUnidadMedica(String cvePresupuestal);

    public SicPeriodosImss obtenerPeriodImssPorFecha(Date fecha);

    public SitControlCapturaCE actualizarControlCapturaCE(SitControlCapturaCE controlCapturaCe);
}
