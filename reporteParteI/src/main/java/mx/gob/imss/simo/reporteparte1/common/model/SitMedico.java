package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.List;

import lombok.Data;

@Data
public class SitMedico {
	String cveMatricula;
	List<String> medico;
}
