/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Data;

/**
 * @author Janiel
 */
@Data
@Entity("SIC_PERIODOS_IMSS")
public class SicPeriodosImss {

    @Id
    private ObjectId id;
    private Integer cvePeriodo;
    private Integer numAnioPeriodo;
    private String numMesPeriodo;
    private Date fecInicial;
    private Date fecFinal;
    private Date fecBaja;
    private Integer indVigente;

}
