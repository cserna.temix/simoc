/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 */
public class SdAccidentesLesiones {

    private Integer cveAccidenteLesion;
    private String desAccidenteLesion;

    public SdAccidentesLesiones() {

        super();
    }

    public SdAccidentesLesiones(Integer cveAccidenteLesion, String desAccidenteLesion) {

        super();
        this.cveAccidenteLesion = cveAccidenteLesion;
        this.desAccidenteLesion = desAccidenteLesion;
    }

    /**
     * @return the cveAccidenteLesion
     */
    public Integer getCveAccidenteLesion() {

        return cveAccidenteLesion;
    }

    /**
     * @param cveAccidenteLesion
     *            the cveAccidenteLesion to set
     */
    public void setCveAccidenteLesion(Integer cveAccidenteLesion) {

        this.cveAccidenteLesion = cveAccidenteLesion;
    }

    /**
     * @return the desAccidenteLesion
     */
    public String getDesAccidenteLesion() {

        return desAccidenteLesion;
    }

    /**
     * @param desAccidenteLesion
     *            the desAccidenteLesion to set
     */
    public void setDesAccidenteLesion(String desAccidenteLesion) {

        this.desAccidenteLesion = desAccidenteLesion;
    }

}
