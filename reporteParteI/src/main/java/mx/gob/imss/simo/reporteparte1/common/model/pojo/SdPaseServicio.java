/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdPaseServicio {

	private Integer cvePaseServicio;
	private String desPaseServicio;

	/**
	 * @return the cvePaseServicio
	 */
	public Integer getCvePaseServicio() {
		return cvePaseServicio;
	}

	/**
	 * @param cvePaseServicio
	 *            the cvePaseServicio to set
	 */
	public void setCvePaseServicio(Integer cvePaseServicio) {
		this.cvePaseServicio = cvePaseServicio;
	}

	/**
	 * @return the desPaseServicio
	 */
	public String getDesPaseServicio() {
		return desPaseServicio;
	}

	/**
	 * @param desPaseServicio
	 *            the desPaseServicio to set
	 */
	public void setDesPaseServicio(String desPaseServicio) {
		this.desPaseServicio = desPaseServicio;
	}

}
