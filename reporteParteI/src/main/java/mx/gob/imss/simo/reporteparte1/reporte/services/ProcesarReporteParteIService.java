package mx.gob.imss.simo.reporteparte1.reporte.services;

import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdPeriodoReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;
import mx.gob.imss.simo.reporteparte1.common.services.ReporteParte1CommonService;
import mx.gob.imss.simo.reporteparte1.common.services.impl.ReporteParte1CommonServiceImpl;

public interface ProcesarReporteParteIService extends ReporteParte1CommonService{
	
	

		/**
		 * Método para extraer información de la BD de Consulta Externa, para el Reporte Parte 1
		 * @param cvePresupuestal
		 * @param fechaInicio
		 * @param fechaFin
		 * @return
		 */
		public SdVistas extraerInfoMedicosReporte(String cvePresupuestal, String desUnidadMedica, Integer periodoInt);
		
		/**
		 * Inserción de un objeto SitReporteParte1 en la BD Mongo
		 * @param sitReporteParteI
		 */
		public void insertarReporte(SitReporteParteI sitReporteParteI);
		
		public SdPeriodoReporte getPeriodoReporte();
		
		public SdUnidadMedica getUnidadMedica();
		
		/**
		 * Realiza la busqueda de un reporte parte 1 en BD 
		 * @param cvePresupuestal
		 * @param clavePeriodo
		 * @return 
		 */
		public SitReporteParteI buscarReporteParte1(String cvePresupuestal, Integer clavePeriodo);
		
		/**
		 * Realiza el borrado en la BD de un reporte existente por Clave presupuestal y periodo
		 * @param cvePresupuestal
		 * @param clavePeriodo
		 * @return 
		 */
		public Boolean borrarReporteParte1(SitReporteParteI sitReporteParteI);

}
