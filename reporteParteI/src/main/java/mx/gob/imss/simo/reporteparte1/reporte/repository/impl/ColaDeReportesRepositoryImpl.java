package mx.gob.imss.simo.reporteparte1.reporte.repository.impl;

import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.aggregation.AggregationPipeline;
import org.mongodb.morphia.aggregation.Sort;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.stereotype.Repository;

import com.mongodb.AggregationOptions;

import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.core.MongoManager;
import mx.gob.imss.simo.reporteparte1.common.core.ReporteParteIConfiguration;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumColaReporte;
import mx.gob.imss.simo.reporteparte1.common.exception.ReporteParteIException;
import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ColaDeReportesRepository;

@Repository("colaDeReportesRepository")
public class ColaDeReportesRepositoryImpl implements ColaDeReportesRepository {

    Logger logger = Logger.getLogger(ColaDeReportesRepositoryImpl.class);

    @Override
    public Iterator<SitColaReportes> buscarReportesEnCola(Integer estatus, Integer limite) {

        Iterator<SitColaReportes> reportesEncolados = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SitColaReportes> q = dsEsp.createQuery(SitColaReportes.class);
            // MATCH
            q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus);

            AggregationPipeline aggr = dsEsp.createAggregation(SitColaReportes.class);
            aggr.match(q);
            aggr.sort(Sort.descending(EnumColaReporte.FEC_INICIO.getValor()));
            if (limite != null) {
                aggr.limit(limite);
            }

            reportesEncolados = aggr.aggregate(SitColaReportes.class, AggregationOptions.builder().build());

        } catch (Exception e) {
            e.getMessage();
        }

        return reportesEncolados;
    }

    @Override
    public Iterator<SitColaReportes> buscarReportesEnCola() {

        Iterator<SitColaReportes> reportesEncolados = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SitColaReportes> q = dsEsp.createQuery(SitColaReportes.class);
            // MATCH
            q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(BaseConstants.ESTATUS_PENDIENTE);

            AggregationPipeline aggr = dsEsp.createAggregation(SitColaReportes.class);
            aggr.match(q);
            aggr.sort(Sort.descending(EnumColaReporte.FEC_INICIO.getValor()));
            aggr.limit(ReporteParteIConfiguration.INSTANCE.getLimiteReportesMinuto());

            reportesEncolados = aggr.aggregate(SitColaReportes.class, AggregationOptions.builder().build());

        } catch (Exception e) {
            e.getMessage();
        }

        return reportesEncolados;
    }

    @Override
    public Boolean insertarReporteEnCola(String cvePresupuestal, String desUnidad, Integer periodo, Integer volumen) {

        Boolean insertado = Boolean.FALSE;
        SitColaReportes colaReporte = new SitColaReportes();
        colaReporte.setFecInicio(new Date());
        colaReporte.setCvePresupuestal(cvePresupuestal);
        colaReporte.setDesUnidad(desUnidad);
        colaReporte.setCvePeriodo(periodo);
        colaReporte.setEstatus(BaseConstants.ESTATUS_PENDIENTE);
        colaReporte.setFecInicio(null);
        colaReporte.setClasificacionVolumen(volumen);

        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();
            ds.save(colaReporte);
            logger.debug(" GUARDADO SitColaReportes:" + colaReporte.getId());
            if (colaReporte.getId() != null) {
                insertado = Boolean.TRUE;
            }
        } catch (Exception e) {
            logger.debug("EXCEPCION:", e);
        }
        return insertado;

    }

    @Override
    public Iterator<SitColaReportes> buscarReportesEnColaActivos(Integer estatus1, Integer estatus2, Integer estatus3) {

        Iterator<SitColaReportes> reportesEncolados = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SitColaReportes> q = dsEsp.createQuery(SitColaReportes.class);
            // MATCH
            if (estatus3 != null) {
                q.or(q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus1),
                        q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus2),
                        q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus3));
            } else {
                q.or(q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus1),
                        q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus2));
            }

            AggregationPipeline aggr = dsEsp.createAggregation(SitColaReportes.class);
            aggr.match(q);
            aggr.sort(Sort.descending(EnumColaReporte.FEC_INICIO.getValor()));

            reportesEncolados = aggr.aggregate(SitColaReportes.class, AggregationOptions.builder().build());

        } catch (Exception e) {
            e.getMessage();
        }

        return reportesEncolados;
    }

    @Override
    public SitColaReportes buscarReporteExistenteEnCola(String cvePresupuestal, Integer clavePeriodo, Integer estatus,
            Long idThread) {

        SitColaReportes reporteEc = null;
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();

            if (idThread != null) {
                reporteEc = ds.createQuery(SitColaReportes.class)
                        .filter(EnumColaReporte.CVE_PRESUPUESTAL.getValor(), cvePresupuestal)
                        .filter(EnumColaReporte.CVE_PERIODO.getValor(), clavePeriodo)
                        .filter(EnumColaReporte.ESTATUS.getValor(), estatus)
                        .filter(EnumColaReporte.ID_THREAD.getValor(), idThread).get();
            } else {
                reporteEc = ds.createQuery(SitColaReportes.class)
                        .filter(EnumColaReporte.CVE_PRESUPUESTAL.getValor(), cvePresupuestal)
                        .filter(EnumColaReporte.CVE_PERIODO.getValor(), clavePeriodo)
                        .filter(EnumColaReporte.ESTATUS.getValor(), estatus).get();
            }

        } catch (Exception e) {
            logger.error("Error buscarReporteExistenteEnCola:" + e);
        }
        return reporteEc;
    }

    @Override
    public SitColaReportes actualizarReporteEnCola(SitColaReportes reporteEnCola, Integer estatusFinal)
            throws ReporteParteIException {

        Datastore ds = MongoManager.INSTANCE.getDatastore();
        Query<SitColaReportes> updateQuery = null;
        UpdateOperations<SitColaReportes> ops = null;

        if (reporteEnCola.getIdThread() != null && estatusFinal != 1 && estatusFinal != 0) {
            updateQuery = ds.createQuery(SitColaReportes.class)
                    .filter("cvePresupuestal", reporteEnCola.getCvePresupuestal())
                    .filter("cvePeriodo", reporteEnCola.getCvePeriodo()).filter("estatus", reporteEnCola.getEstatus())
                    .filter("idThread", reporteEnCola.getIdThread());
        } else {
            updateQuery = ds.createQuery(SitColaReportes.class)
                    .filter("cvePresupuestal", reporteEnCola.getCvePresupuestal())
                    .filter("cvePeriodo", reporteEnCola.getCvePeriodo()).filter("estatus", reporteEnCola.getEstatus());

        }

        if (estatusFinal == 0) {
            ops = ds.createUpdateOperations(SitColaReportes.class).set("estatus", estatusFinal);
        }
        if (estatusFinal == 1) {
            ops = ds.createUpdateOperations(SitColaReportes.class).set("estatus", estatusFinal)
                    .set("nombreThread", Thread.currentThread().getName()).set("idThread", reporteEnCola.getIdThread());
        }
        if (estatusFinal == 2 && !reporteEnCola.getDesUnidad().equals("")) {
            ops = ds.createUpdateOperations(SitColaReportes.class).set("estatus", estatusFinal)
                    .set("fecInicio", new Date()).set("nombreThread", Thread.currentThread().getName())
                    .set("idThread", reporteEnCola.getIdThread());
        }
        if (estatusFinal == 3 || estatusFinal == 4 || estatusFinal == 5 || estatusFinal == 6) {
            ops = ds.createUpdateOperations(SitColaReportes.class).set("estatus", estatusFinal).set("fecFin",
                    new Date())
            // .set("currentThread", Thread.currentThread())
            // .set("nombreThread", Thread.currentThread().getName())
            // .set("idThread", Thread.currentThread().getId())
            ;
        }
        System.out.println("ds:" + ds);
        if (ops != null) {
            reporteEnCola = ds.findAndModify(updateQuery, ops, false, false);
        }
        return reporteEnCola;
    }

    @Override
    public Iterator<SitColaReportes> buscarReportesUnidadEnColaPorEstatus(String cvePresupuestal, Integer clavePeriodo,
            Integer estatus, Long idThread) {

        Iterator<SitColaReportes> reportesEncolados = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SitColaReportes> q = dsEsp.createQuery(SitColaReportes.class);
            // MATCH
            if (idThread != null) {
                q.and(q.criteria(EnumColaReporte.CVE_PRESUPUESTAL.getValor()).equal(cvePresupuestal),
                        q.criteria(EnumColaReporte.CVE_PERIODO.getValor()).equal(clavePeriodo),
                        q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus),
                        q.criteria(EnumColaReporte.ID_THREAD.getValor()).equal(idThread));

            } else {
                q.and(q.criteria(EnumColaReporte.CVE_PRESUPUESTAL.getValor()).equal(cvePresupuestal),
                        q.criteria(EnumColaReporte.CVE_PERIODO.getValor()).equal(clavePeriodo),
                        q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus));

            }

            AggregationPipeline aggr = dsEsp.createAggregation(SitColaReportes.class);
            aggr.match(q);
            aggr.sort(Sort.ascending(EnumColaReporte.FEC_INICIO.getValor()));
            System.out.println("aggr:" + aggr);
            reportesEncolados = aggr.aggregate(SitColaReportes.class, AggregationOptions.builder().build());

        } catch (Exception e) {
            e.getMessage();
        }

        return reportesEncolados;

    }

    @Override
    public void borrarRepetidosColaReportes() {

        Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
        Query<SitColaReportes> q = dsEsp.createQuery(SitColaReportes.class).filter(EnumColaReporte.ESTATUS.getValor(),
                BaseConstants.ESTATUS_REPETIDO);
        dsEsp.delete(q);

    }

    @Override
    public Iterator<SitColaReportes> buscarReportesEnColaPorEstatus(Integer estatus, Long idThread, Integer limite) {

        Iterator<SitColaReportes> reportesEncolados = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SitColaReportes> q = dsEsp.createQuery(SitColaReportes.class);
            // MATCH
            if (idThread != null) {
                q.and(q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus),
                        q.criteria(EnumColaReporte.ID_THREAD.getValor()).equal(idThread));

            } else {
                q.and(q.criteria(EnumColaReporte.ESTATUS.getValor()).equal(estatus));

            }

            AggregationPipeline aggr = dsEsp.createAggregation(SitColaReportes.class);
            aggr.match(q);
            aggr.sort(Sort.ascending(EnumColaReporte.FEC_INICIO.getValor()));
            if (limite != null) {
                aggr.limit(limite);
            }
            System.out.println("aggr:" + aggr);
            reportesEncolados = aggr.aggregate(SitColaReportes.class, AggregationOptions.builder().build());

        } catch (Exception e) {
            e.getMessage();
        }

        return reportesEncolados;
    }

}
