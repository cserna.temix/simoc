/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class GroupMedico {

 private Medico medico;
 private Double contador;
 
 

public Double getContador() {
	return contador;
}

public void setContador(Double contador) {
	this.contador = contador;
}

public Medico getMedico() {
	return medico;
}

public void setMedico(Medico medico) {
	this.medico = medico;
}
 
 

}
