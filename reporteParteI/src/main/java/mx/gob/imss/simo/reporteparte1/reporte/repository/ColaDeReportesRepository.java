package mx.gob.imss.simo.reporteparte1.reporte.repository;

import java.util.Iterator;

import mx.gob.imss.simo.reporteparte1.common.exception.ReporteParteIException;
import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;

public interface ColaDeReportesRepository {

    Iterator<SitColaReportes> buscarReportesEnCola(Integer estatus, Integer limite);

    Iterator<SitColaReportes> buscarReportesEnCola();

    /**
     * Realiza la inserci�n en la colecci�n SIT_COLA_REPORTES
     * 
     * @param sitReporteParteI
     * @return
     */
    public Boolean insertarReporteEnCola(String cvePresupuestal, String desUnidad, Integer periodo, Integer volumen);

    /**
     * BUsca reportes en la SIT_COLA_REPORTES en estado pendiente y procesando
     * 
     * @param estatus1
     * @param estatus2
     * @return
     */
    public Iterator<SitColaReportes> buscarReportesEnColaActivos(Integer estatus1, Integer estatus2, Integer estatus3);

    public SitColaReportes buscarReporteExistenteEnCola(String cvePresupuestal, Integer clavePeriodo, Integer estatus,
            Long idThread);

    public SitColaReportes actualizarReporteEnCola(SitColaReportes reporteEnCola, Integer estatusFinal)
            throws ReporteParteIException;

    public void borrarRepetidosColaReportes();

    Iterator<SitColaReportes> buscarReportesUnidadEnColaPorEstatus(String cvePresupuestal, Integer clavePeriodo,
            Integer estatus, Long idThread);

    /**
     * @param estatus
     * @param idThread
     * @param limite
     * @return
     */
    Iterator<SitColaReportes> buscarReportesEnColaPorEstatus(Integer estatus, Long idThread, Integer limite);

}
