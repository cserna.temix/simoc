package mx.gob.imss.simo.reporteparte1.reporte.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    public static final String DB_FORMAT_DATETIME = "yyyy-M-d HH:mm:ss";
    private static final String FECHA_FORMATO_DEFAULT = "dd/MM/yyyy";
    private static final String FECHA_FORMATO_VISTA = "dd/MM/yyyy HH:mm";
    public static final Locale LOCALE = new Locale("es", "MX");

    private DateUtils() {

        // not publicly instantiable
    }

    public static Date getDate(String dateStr, String format) {

        final DateFormat formatter = new SimpleDateFormat(format, LOCALE);
        try {
            return formatter.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String fechaAString(Date fecha) {

        String fechaConvertida = "";
        SimpleDateFormat formatter;

        formatter = new SimpleDateFormat(FECHA_FORMATO_VISTA, LOCALE);
        fechaConvertida = formatter.format(fecha);

        return fechaConvertida;
    }

    public static Date crearFecha() {

        SimpleDateFormat sdf = new SimpleDateFormat(FECHA_FORMATO_DEFAULT, LOCALE);
        String fecha = sdf.format(new Date());
        sdf.setTimeZone(TimeZone.getDefault());
        try {
            return sdf.parse(fecha);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date agregarDiasAFecha(Date date, int numeroDias) {

        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, numeroDias);
        return cal.getTime();

    }

    public static Date agregarMinutosAFecha(Date date, int minutos) {

        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutos);
        return cal.getTime();

    }

    /**
     * Devuelve true si la fecha Inicial es mayor a la fecha final que se pasan
     * como parametro
     */
    public static boolean comparaFechas(Date fechaInicial, Date fechaFinal) {

        Calendar fechaIni = Calendar.getInstance();
        fechaIni.setTime(fechaInicial);
        Calendar fechaFin = Calendar.getInstance();
        fechaFin.setTime(fechaFinal);
        if (fechaIni.getTimeInMillis() > fechaFin.getTimeInMillis()) {
            return true;
        }
        return false;
    }

    public static Date mkDateIni(Date date) {

        if (date == null)
            return null;
        Calendar fecha = Calendar.getInstance();
        fecha.setTime(date);
        fecha.set(Calendar.HOUR_OF_DAY, 0);
        fecha.set(Calendar.MINUTE, 0);
        fecha.set(Calendar.SECOND, 0);
        fecha.set(Calendar.MILLISECOND, 0);
        return fecha.getTime();
    }
}
