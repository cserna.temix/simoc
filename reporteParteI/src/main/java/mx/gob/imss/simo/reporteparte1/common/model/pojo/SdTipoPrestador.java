/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdTipoPrestador {

	private Integer cveTipoPrestador;
	private String desTipoPrestador;

	/**
	 * @return the cveTipoPrestador
	 */
	public Integer getCveTipoPrestador() {
		return cveTipoPrestador;
	}

	/**
	 * @param cveTipoPrestador
	 *            the cveTipoPrestador to set
	 */
	public void setCveTipoPrestador(Integer cveTipoPrestador) {
		this.cveTipoPrestador = cveTipoPrestador;
	}

	/**
	 * @return the desTipoPrestador
	 */
	public String getDesTipoPrestador() {
		return desTipoPrestador;
	}

	/**
	 * @param desTipoPrestador
	 *            the desTipoPrestador to set
	 */
	public void setDesTipoPrestador(String desTipoPrestador) {
		this.desTipoPrestador = desTipoPrestador;
	}

}
