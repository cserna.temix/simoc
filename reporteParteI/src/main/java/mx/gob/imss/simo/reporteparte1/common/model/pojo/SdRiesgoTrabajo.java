/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rrios
 */
public class SdRiesgoTrabajo {

    private Integer cveRiesgoTrabajo;
    private String desRiesgoTrabajo;

    public SdRiesgoTrabajo() {

        super();
    }

    public SdRiesgoTrabajo(Integer cveRiesgoTrabajo, String desRiesgoTrabajo) {

        super();
        this.cveRiesgoTrabajo = cveRiesgoTrabajo;
        this.desRiesgoTrabajo = desRiesgoTrabajo;
    }

    /**
     * @return the cveRiesgoTrabajo
     */
    public Integer getCveRiesgoTrabajo() {

        return cveRiesgoTrabajo;
    }

    /**
     * @param cveRiesgoTrabajo
     *            the cveRiesgoTrabajo to set
     */
    public void setCveRiesgoTrabajo(Integer cveRiesgoTrabajo) {

        this.cveRiesgoTrabajo = cveRiesgoTrabajo;
    }

    /**
     * @return the desRiesgoTrabajo
     */
    public String getDesRiesgoTrabajo() {

        return desRiesgoTrabajo;
    }

    /**
     * @param desRiesgoTrabajo
     *            the desRiesgoTrabajo to set
     */
    public void setDesRiesgoTrabajo(String desRiesgoTrabajo) {

        this.desRiesgoTrabajo = desRiesgoTrabajo;
    }

}
