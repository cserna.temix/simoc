package mx.gob.imss.simo.reporteparte1.common.model.pojo;

import lombok.Data;

@Data
public class SdPeriodoReporte {
	
	private Integer cvePeriodo;
	private String desPeriodo;
	
	
}
