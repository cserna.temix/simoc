package mx.gob.imss.simo.reporteparte1.common.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SIT_BITACORA_ENVIO")
public class SitBitacoraEnvio {

    @Id
    private ObjectId id;
    private String cvePresupuestal;
    private String desUnidadMedica;
    private Date stpCreaArchivo;
    private Integer numRegistros;
    private String refRespServidor;
    private Integer indEnvio;
    private Date stpEnvio;

    public String getCvePresupuestal() {

        return cvePresupuestal;
    }

    public void setCvePresupuestal(String cvePresupuestal) {

        this.cvePresupuestal = cvePresupuestal;
    }

    public String getDesUnidadMedica() {

        return desUnidadMedica;
    }

    public void setDesUnidadMedica(String desUnidadMedica) {

        this.desUnidadMedica = desUnidadMedica;
    }

    public Date getStpCreaArchivo() {

        return stpCreaArchivo;
    }

    public void setStpCreaArchivo(Date stpCreaArchivo) {

        this.stpCreaArchivo = stpCreaArchivo;
    }

    public Integer getNumRegistros() {

        return numRegistros;
    }

    public void setNumRegistros(Integer numRegistros) {

        this.numRegistros = numRegistros;
    }

    public String getRefRespServidor() {

        return refRespServidor;
    }

    public void setRefRespServidor(String refRespServidor) {

        this.refRespServidor = refRespServidor;
    }

    public Integer getIndEnvio() {

        return indEnvio;
    }

    public void setIndEnvio(Integer indEnvio) {

        this.indEnvio = indEnvio;
    }

    public Date getStpEnvio() {

        return stpEnvio;
    }

    public void setStpEnvio(Date stpEnvio) {

        this.stpEnvio = stpEnvio;
    }
}
