/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.model.pojo;

/**
 * @author francisco.rodriguez
 * 
 */
public class SdConsultorio {

	private String cveConsultorio;
	private String desConsultorio;

	/**
	 * @return the cveConsultorio
	 */
	public String getCveConsultorio() {
		return cveConsultorio;
	}

	/**
	 * @param cveConsultorio
	 *            the cveConsultorio to set
	 */
	public void setCveConsultorio(String cveConsultorio) {
		this.cveConsultorio = cveConsultorio;
	}

	/**
	 * @return the desConsultorio
	 */
	public String getDesConsultorio() {
		return desConsultorio;
	}

	/**
	 * @param desConsultorio
	 *            the desConsultorio to set
	 */
	public void setDesConsultorio(String desConsultorio) {
		this.desConsultorio = desConsultorio;
	}

}
