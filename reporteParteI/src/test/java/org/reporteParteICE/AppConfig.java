package org.reporteParteICE;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import mx.gob.imss.simo.reporteparte1.reporte.services.ProcesarReporteParteIService;


	@Configuration
	@ComponentScan(basePackages = {"mx.gob.imss.simo.reporteParteI.reporte.services","mx.gob.imss.simo.reporteParteI","reporteParteI.reporte.repository","reporteParteI.reporte.helper"}, 
		basePackageClasses = ProcesarReporteParteIService.class)
	public class AppConfig {	
}
