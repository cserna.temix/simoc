package mx.gob.imss.simo.reporteparte1.reportes.rules;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.model.AgrupacionId;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteIAccidentesyL;
import mx.gob.imss.simo.reporteparte1.common.model.NumHorasTrabajadas;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdTotalesReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdV2Especialidad;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;
import mx.gob.imss.simo.reporteparte1.reporte.rules.ProcesarReporteParteIRules;

public class ProcesarReporteParteIRulesTest {

    @InjectMocks
    ProcesarReporteParteIRules procesarReporteParteIRules;

    final static Logger logger = LoggerFactory.getLogger(ProcesarReporteParteIRulesTest.class);

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        procesarReporteParteIRules = new ProcesarReporteParteIRules();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void crearInformacionReporteTest() {

        NumHorasTrabajadas numHorasTrabajadas = new NumHorasTrabajadas();
        numHorasTrabajadas.setHoras("20:0");
        List<NumHorasTrabajadas> amTiempoTraHoras = new ArrayList<NumHorasTrabajadas>();
        amTiempoTraHoras.add(numHorasTrabajadas);

        AgrupacionId agrupacionId = new AgrupacionId();
        List<String> apM = new ArrayList<String>();
        apM.add("López");

        List<String> apP = new ArrayList<String>();
        apP.add("López");

        List<String> nom = new ArrayList<String>();
        nom.add("Laura");

        List<String> esp = new ArrayList<String>();
        esp.add("Cardiología");

        List<String> matricula = new ArrayList<String>();
        matricula.add("111111111111");

        agrupacionId.setApM(apM);
        agrupacionId.setApP(apP);
        agrupacionId.setNom(nom);
        agrupacionId.setEsp(esp);
        agrupacionId.setMatricula(matricula);

        ExtractoReporteParteI extractoReporteParteI = new ExtractoReporteParteI();
        extractoReporteParteI.setAm1Vez(5);
        extractoReporteParteI.setAmCitas(10);
        extractoReporteParteI.setAmCitasCumplidas(15);
        extractoReporteParteI.setAmNoOtorgadas(20);
        extractoReporteParteI.setAmNumVisitas(30);
        extractoReporteParteI.setAmTiempoTraDias(10);
        extractoReporteParteI.setAmTiempoTraHoras(amTiempoTraHoras);
        extractoReporteParteI.setAmTotCons(15);
        extractoReporteParteI.setDesEsp("Cardiología");
        extractoReporteParteI.setDesUnidad("HGO 221 Toluca");
        extractoReporteParteI.setGroupId(agrupacionId);
        extractoReporteParteI.setIncDias(2);
        extractoReporteParteI.setIncExpedidas(5);
        extractoReporteParteI.setIncProm(6.0);
        extractoReporteParteI.setPeAltasEspeciales(20);
        extractoReporteParteI.setPeEnUnidad(30);
        extractoReporteParteI.setPeOtraUnidad(10);
        extractoReporteParteI.setRecNum(50);
        extractoReporteParteI.setRecPorcentajeAten(20.5);

        List<ExtractoReporteParteI> extractoReporteList = new ArrayList<ExtractoReporteParteI>();
        extractoReporteList.add(extractoReporteParteI);

        Iterator<ExtractoReporteParteI> extractoReporte = extractoReporteList.iterator();
        SdVistas sdVistasResult = new SdVistas();

        try {

            sdVistasResult = procesarReporteParteIRules.crearInformacionReporte(extractoReporte,
                    BaseConstants.TC_ESPECIALIDADES);
            Assert.assertNotNull(sdVistasResult);

            logger.info("Test crearInformacionReporte");
            logger.info("Clave Especialidad: {}",
                    sdVistasResult.getSdVista2().getSdV2Especialidad().get(0).getCveEspecialidad());
            logger.info("Número de Altas Especiales: {}",
                    sdVistasResult.getSdVista1().getSdRepEspecialidades().getNumAltasEspeciales());
            logger.info("Número de Citas: {}", sdVistasResult.getSdVista1().getSdRepEspecialidades().getNumCitas());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void crearInformacionReporteParamedicosTest() {

        NumHorasTrabajadas numHorasTrabajadas = new NumHorasTrabajadas();
        numHorasTrabajadas.setHoras("30:0");
        List<NumHorasTrabajadas> amTiempoTraHoras = new ArrayList<NumHorasTrabajadas>();
        amTiempoTraHoras.add(numHorasTrabajadas);

        AgrupacionId agrupacionId = new AgrupacionId();
        List<String> apM = new ArrayList<String>();
        apM.add("Sánchez");

        List<String> apP = new ArrayList<String>();
        apP.add("López");

        List<String> nom = new ArrayList<String>();
        nom.add("Laura");

        List<String> esp = new ArrayList<String>();
        esp.add("Cardiología");

        List<String> matricula = new ArrayList<String>();
        matricula.add("222222222222");

        agrupacionId.setApM(apM);
        agrupacionId.setApP(apP);
        agrupacionId.setNom(nom);
        agrupacionId.setEsp(esp);
        agrupacionId.setMatricula(matricula);

        ExtractoReporteParteI extractoReporteParteI = new ExtractoReporteParteI();
        extractoReporteParteI.setAm1Vez(5);
        extractoReporteParteI.setAmCitas(10);
        extractoReporteParteI.setAmCitasCumplidas(15);
        extractoReporteParteI.setAmNoOtorgadas(20);
        extractoReporteParteI.setAmNumVisitas(30);
        extractoReporteParteI.setAmTiempoTraDias(10);
        extractoReporteParteI.setAmTiempoTraHoras(amTiempoTraHoras);
        extractoReporteParteI.setAmTotCons(15);
        extractoReporteParteI.setDesEsp("Obstetricia");
        extractoReporteParteI.setDesUnidad("HGO 221 Toluca");
        extractoReporteParteI.setGroupId(agrupacionId);
        extractoReporteParteI.setIncDias(2);
        extractoReporteParteI.setIncExpedidas(5);
        extractoReporteParteI.setIncProm(6.0);
        extractoReporteParteI.setPeAltasEspeciales(20);
        extractoReporteParteI.setPeEnUnidad(30);
        extractoReporteParteI.setPeOtraUnidad(10);
        extractoReporteParteI.setRecNum(50);
        extractoReporteParteI.setRecPorcentajeAten(20.5);

        List<ExtractoReporteParteI> extractoReporteList = new ArrayList<ExtractoReporteParteI>();
        extractoReporteList.add(extractoReporteParteI);

        Iterator<ExtractoReporteParteI> extractoReporte = extractoReporteList.iterator();
        SdVistas sdVistasResult = new SdVistas();

        try {

            sdVistasResult = procesarReporteParteIRules.crearInformacionReporteParamedicos(extractoReporte,
                    BaseConstants.TC_PARAMEDICOS);
            Assert.assertNotNull(sdVistasResult);

            logger.info("Test crearInformacionReporteParamedicos");
            logger.info("Apellido Paterno: {}", sdVistasResult.getSdVista2().getSdV2Especialidad().get(0)
                    .getSdV2Medico().get(0).getRefApellidoPaterno());
            logger.info("Apellido Materno: {}", sdVistasResult.getSdVista2().getSdV2Especialidad().get(0)
                    .getSdV2Medico().get(0).getRefApellidoMaterno());
            logger.info("Apellido Nombre: {}",
                    sdVistasResult.getSdVista2().getSdV2Especialidad().get(0).getSdV2Medico().get(0).getRefNombre());
            logger.info("Clave Matricula: {}",
                    sdVistasResult.getSdVista2().getSdV2Especialidad().get(0).getSdV2Medico().get(0).getCveMatricula());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void extraccionReporteAccidentesYlesTest() {

        ExtractoReporteParteIAccidentesyL extractoReporteParteI01 = new ExtractoReporteParteIAccidentesyL();
        ExtractoReporteParteIAccidentesyL extractoReporteParteI02 = new ExtractoReporteParteIAccidentesyL();

        extractoReporteParteI01.setAmTiempoTraDias(8);
        extractoReporteParteI01.setAmTotCons(10);
        extractoReporteParteI01.setAm1Vez(3);
        extractoReporteParteI01.setAmNoOtorgadas(5);
        extractoReporteParteI01.setAmNumVisitas(2);
        extractoReporteParteI01.setAmCitas(1);
        extractoReporteParteI01.setAmCitasCumplidas(1);
        extractoReporteParteI01.setPeEnUnidad(0);
        extractoReporteParteI01.setPeOtraUnidad(1);
        extractoReporteParteI01.setPeAltasEspeciales(2);
        extractoReporteParteI01.setIncExpedidas(4);
        extractoReporteParteI01.setIncDias(23);
        extractoReporteParteI01.setIncProm(4.0);
        extractoReporteParteI01.setRecNum(9);
        extractoReporteParteI01.setRecPorcentajeAten(1.0);

        extractoReporteParteI02.setAmTiempoTraDias(8);
        extractoReporteParteI02.setAmTotCons(10);
        extractoReporteParteI02.setAm1Vez(3);
        extractoReporteParteI02.setAmNoOtorgadas(5);
        extractoReporteParteI02.setAmNumVisitas(2);
        extractoReporteParteI02.setAmCitas(1);
        extractoReporteParteI02.setAmCitasCumplidas(1);
        extractoReporteParteI02.setPeEnUnidad(0);
        extractoReporteParteI02.setPeOtraUnidad(1);
        extractoReporteParteI02.setPeAltasEspeciales(2);
        extractoReporteParteI02.setIncExpedidas(4);
        extractoReporteParteI02.setIncDias(23);
        extractoReporteParteI02.setIncProm(4.0);
        extractoReporteParteI02.setRecNum(9);
        extractoReporteParteI02.setRecPorcentajeAten(1.0);

        List<ExtractoReporteParteIAccidentesyL> extractoReporteList = new ArrayList<ExtractoReporteParteIAccidentesyL>();
        extractoReporteList.add(extractoReporteParteI01);
        extractoReporteList.add(extractoReporteParteI02);

        Iterator<ExtractoReporteParteIAccidentesyL> extractoReporte = extractoReporteList.iterator();
        SdTotalesReporte sdTotalesReporte = new SdTotalesReporte();

        try {

            sdTotalesReporte = procesarReporteParteIRules.extraccionReporteAccidentesYles(extractoReporte);
            Assert.assertNotNull(sdTotalesReporte);

            logger.info("Test extraccionReporteAccidentesYles");
            logger.info("Extracción Reporte Accidentes: {}", sdTotalesReporte);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void sumarTotalesTest() {

        SdTotalesReporte totalesReporte1 = new SdTotalesReporte();
        SdTotalesReporte totalesReporte2 = new SdTotalesReporte();

        totalesReporte1.setCveEspecialidad("1400");
        totalesReporte1.setDesEspecialidad("Cardiología");
        totalesReporte1.setRefVistaEspecialidad("** 1400 Cardiología **");
        totalesReporte1.setNumMedicos(10);
        totalesReporte1.setTiempoTrabajadoHoras("129:00");
        totalesReporte1.setTiempoHoras(129);
        totalesReporte1.setTiempoMinutos(0);
        totalesReporte1.setTiempoTrabajadoDias(10);
        totalesReporte1.setTotalConsultas(20);
        totalesReporte1.setNum1EraVez(5);
        totalesReporte1.setNumNoOtorgadas(6);
        totalesReporte1.setPromedioHoras(10.89);
        totalesReporte1.setNumVisitas(4);
        totalesReporte1.setNumCitas(8);
        totalesReporte1.setNumCitasCumplidas(2);
        totalesReporte1.setNumEnUnidad(1);
        totalesReporte1.setNumOtraUnidad(4);
        totalesReporte1.setNumAltasEspeciales(7);
        totalesReporte1.setNumAltasEspeciales(1);
        totalesReporte1.setNumExpedidas(0);
        totalesReporte1.setNumDias(3);
        totalesReporte1.setPromedio(11.05);
        totalesReporte1.setNumRecetas(9);
        totalesReporte1.setPorcentajeAtenciones(30.21);

        totalesReporte2.setCveEspecialidad("1400");
        totalesReporte2.setDesEspecialidad("Cardiología");
        totalesReporte2.setRefVistaEspecialidad("** 1400 Cardiología **");
        totalesReporte2.setNumMedicos(10);
        totalesReporte2.setTiempoTrabajadoHoras("129:00");
        totalesReporte2.setTiempoHoras(129);
        totalesReporte2.setTiempoMinutos(0);
        totalesReporte2.setTiempoTrabajadoDias(10);
        totalesReporte2.setTotalConsultas(20);
        totalesReporte2.setNum1EraVez(5);
        totalesReporte2.setNumNoOtorgadas(6);
        totalesReporte2.setPromedioHoras(10.89);
        totalesReporte2.setNumVisitas(4);
        totalesReporte2.setNumCitas(8);
        totalesReporte2.setNumCitasCumplidas(2);
        totalesReporte2.setNumEnUnidad(1);
        totalesReporte2.setNumOtraUnidad(4);
        totalesReporte2.setNumAltasEspeciales(7);
        totalesReporte2.setNumAltasEspeciales(1);
        totalesReporte2.setNumExpedidas(0);
        totalesReporte2.setNumDias(3);
        totalesReporte2.setPromedio(11.05);
        totalesReporte2.setNumRecetas(9);
        totalesReporte2.setPorcentajeAtenciones(30.21);

        SdTotalesReporte suma = new SdTotalesReporte();

        try {

            suma = procesarReporteParteIRules.sumarTotales(totalesReporte1, totalesReporte2);
            Assert.assertNotNull(suma);

            logger.info("Test sumarTotales");
            logger.info("Suma: {}", suma);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void capturarTotalHorasTest() {

        List<NumHorasTrabajadas> tiempoTraHoras = new ArrayList<>();
        NumHorasTrabajadas numHoras = new NumHorasTrabajadas();
        numHoras.setHoras("40:10");
        tiempoTraHoras.add(numHoras);

        List<Integer> horasYmin = new ArrayList<>();

        try {

            horasYmin = procesarReporteParteIRules.capturarTotalHoras(tiempoTraHoras);
            Assert.assertNotNull(horasYmin);

            logger.info("Test capturarTotalHoras");
            logger.info("Total de Horas: {}", horasYmin);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void buscarEspecialidadTest() {

        SdV2Especialidad v2especialidad1 = new SdV2Especialidad();
        SdV2Especialidad v2especialidad2 = new SdV2Especialidad();

        v2especialidad1.setCveEspecialidad("3100");
        v2especialidad1.setDesEspecialidad("Otorrinolaringología");
        v2especialidad1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");

        v2especialidad2.setCveEspecialidad("1400");
        v2especialidad2.setDesEspecialidad("Cardiología");
        v2especialidad2.setRefVistaEspecialidad("** 1400 Cardiología **");

        List<SdV2Especialidad> sdV2Especialidad = new ArrayList<>();
        sdV2Especialidad.add(v2especialidad1);
        sdV2Especialidad.add(v2especialidad2);

        SdV2Especialidad v2especialidadResult = new SdV2Especialidad();

        try {

            v2especialidadResult = procesarReporteParteIRules.buscarEspecialidad(sdV2Especialidad, "3100");
            Assert.assertNotNull(v2especialidadResult);

            logger.info("Test buscarEspecialidad");
            logger.info("Especialidad: {}", v2especialidadResult);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
