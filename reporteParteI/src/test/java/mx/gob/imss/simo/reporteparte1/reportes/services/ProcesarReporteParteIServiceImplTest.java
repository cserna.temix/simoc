package mx.gob.imss.simo.reporteparte1.reportes.services;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.reporteparte1.common.constant.BaseConstants;
import mx.gob.imss.simo.reporteparte1.common.model.AgrupacionId;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.ExtractoReporteParteIAccidentesyL;
import mx.gob.imss.simo.reporteparte1.common.model.NumHorasTrabajadas;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdPeriodoReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdRenglonesEspecialesVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdTotalesReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdV2Especialidad;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdV2Medico;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista2;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;
import mx.gob.imss.simo.reporteparte1.reporte.repository.impl.ProcesarReporteParteIRepositoryImpl;
import mx.gob.imss.simo.reporteparte1.reporte.rules.ProcesarReporteParteIRules;
import mx.gob.imss.simo.reporteparte1.reporte.services.impl.ProcesarReporteParteIServiceImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ProcesarReporteParteIServiceImpl.class)
public class ProcesarReporteParteIServiceImplTest {

    final static Logger logger = LoggerFactory.getLogger(ProcesarReporteParteIServiceImplTest.class);

    @InjectMocks
    ProcesarReporteParteIServiceImpl procesarReporteParteIService;

    @Mock
    ProcesarReporteParteIRepositoryImpl procesarReporteParteIRepository;

    @Mock
    ProcesarReporteParteIRules procesarReporteParteIRules;

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        procesarReporteParteIService = PowerMockito.spy(new ProcesarReporteParteIServiceImpl());
        MockitoAnnotations.initMocks(this);
    }

    SimpleDateFormat sdf = new SimpleDateFormat("YYYY/MM/DD");

    private SdTotalesReporte creaSdTotalesReporte() {

        SdTotalesReporte sdRepEspecialidades = new SdTotalesReporte();
        sdRepEspecialidades.setCveEspecialidad("3100");
        sdRepEspecialidades.setDesEspecialidad("Otorrinolaringología");
        sdRepEspecialidades.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdRepEspecialidades.setNumMedicos(10);
        sdRepEspecialidades.setTiempoTrabajadoHoras("123:00");
        sdRepEspecialidades.setTiempoHoras(123);
        sdRepEspecialidades.setTiempoMinutos(0);
        sdRepEspecialidades.setTiempoTrabajadoDias(8);
        sdRepEspecialidades.setTotalConsultas(12);
        sdRepEspecialidades.setNum1EraVez(5);
        sdRepEspecialidades.setNumNoOtorgadas(6);
        sdRepEspecialidades.setPromedioHoras(10.89);
        sdRepEspecialidades.setNumVisitas(4);
        sdRepEspecialidades.setNumCitas(8);
        sdRepEspecialidades.setNumCitasCumplidas(2);
        sdRepEspecialidades.setNumEnUnidad(1);
        sdRepEspecialidades.setNumOtraUnidad(4);
        sdRepEspecialidades.setNumAltasEspeciales(7);
        sdRepEspecialidades.setNumAltasEspeciales(1);
        sdRepEspecialidades.setNumExpedidas(0);
        sdRepEspecialidades.setNumDias(3);
        sdRepEspecialidades.setPromedio(10.05);
        sdRepEspecialidades.setNumRecetas(9);
        sdRepEspecialidades.setPorcentajeAtenciones(20.21);
        return sdRepEspecialidades;
    }

    private SdTotalesReporte creaSdTotalesReporte1() {

        SdTotalesReporte sdUrgencias = new SdTotalesReporte();
        sdUrgencias.setCveEspecialidad("3100");
        sdUrgencias.setDesEspecialidad("Otorrinolaringología");
        sdUrgencias.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdUrgencias.setNumMedicos(10);
        sdUrgencias.setTiempoTrabajadoHoras("123:00");
        sdUrgencias.setTiempoHoras(123);
        sdUrgencias.setTiempoMinutos(0);
        sdUrgencias.setTiempoTrabajadoDias(8);
        sdUrgencias.setTotalConsultas(12);
        sdUrgencias.setNum1EraVez(5);
        sdUrgencias.setNumNoOtorgadas(6);
        sdUrgencias.setPromedioHoras(10.89);
        sdUrgencias.setNumVisitas(4);
        sdUrgencias.setNumCitas(8);
        sdUrgencias.setNumCitasCumplidas(2);
        sdUrgencias.setNumEnUnidad(1);
        sdUrgencias.setNumOtraUnidad(4);
        sdUrgencias.setNumAltasEspeciales(7);
        sdUrgencias.setNumAltasEspeciales(1);
        sdUrgencias.setNumExpedidas(0);
        sdUrgencias.setNumDias(3);
        sdUrgencias.setPromedio(10.05);
        sdUrgencias.setNumRecetas(9);
        sdUrgencias.setPorcentajeAtenciones(20.21);
        return sdUrgencias;
    }

    @Test
    public void insertarReporteTest() {

        SitReporteParteI reporte = new SitReporteParteI();
        // unidad
        SdUnidadMedica unidad = new SdUnidadMedica();
        unidad.setCvePresupuestal("030103022151");
        unidad.setDesUnidadMedica("HGZMF 1 La Paz");
        reporte.setSdUnidadMedica(unidad);

        // periodo
        SdPeriodoReporte periodo = new SdPeriodoReporte();
        periodo.setCvePeriodo(201801);
        periodo.setDesPeriodo("del 26 de Diciembre 2017 al 25 de Enero de 2018");
        reporte.setSdPeriodoReporte(periodo);

        // fecGeneracion
        reporte.setFecGeneracion(new Date());

        // vistas
        SdVistas vistas = new SdVistas();

        // Vista 1
        SdVista1 vista1 = new SdVista1();

        SdTotalesReporte sdRepEspecialidades = creaSdTotalesReporte();

        vista1.setSdRepEspecialidades(sdRepEspecialidades);

        SdTotalesReporte sdUrgencias = creaSdTotalesReporte1();
        vista1.setSdUrgencias(sdUrgencias);

        SdTotalesReporte sdTotalUnidad = new SdTotalesReporte();
        sdTotalUnidad.setCveEspecialidad("3100");
        sdTotalUnidad.setDesEspecialidad("Otorrinolaringología");
        sdTotalUnidad.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdTotalUnidad.setNumMedicos(10);
        sdTotalUnidad.setTiempoTrabajadoHoras("123:00");
        sdTotalUnidad.setTiempoHoras(123);
        sdTotalUnidad.setTiempoMinutos(0);
        sdTotalUnidad.setTiempoTrabajadoDias(8);
        sdTotalUnidad.setTotalConsultas(12);
        sdTotalUnidad.setNum1EraVez(5);
        sdTotalUnidad.setNumNoOtorgadas(6);
        sdTotalUnidad.setPromedioHoras(10.89);
        sdTotalUnidad.setNumVisitas(4);
        sdTotalUnidad.setNumCitas(8);
        sdTotalUnidad.setNumCitasCumplidas(2);
        sdTotalUnidad.setNumEnUnidad(1);
        sdTotalUnidad.setNumOtraUnidad(4);
        sdTotalUnidad.setNumAltasEspeciales(7);
        sdTotalUnidad.setNumAltasEspeciales(1);
        sdTotalUnidad.setNumExpedidas(0);
        sdTotalUnidad.setNumDias(3);
        sdTotalUnidad.setPromedio(10.05);
        sdTotalUnidad.setNumRecetas(9);
        sdTotalUnidad.setPorcentajeAtenciones(20.21);

        vista1.setSdTotalUnidad(sdTotalUnidad);

        // renglones especiales
        SdRenglonesEspecialesVista1 sdRenglonesEspeciales = new SdRenglonesEspecialesVista1();

        SdTotalesReporte sdViolencia = new SdTotalesReporte();
        sdViolencia.setCveEspecialidad("3100");
        sdViolencia.setDesEspecialidad("Otorrinolaringología");
        sdViolencia.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdViolencia.setNumMedicos(10);
        sdViolencia.setTiempoTrabajadoHoras("123:00");
        sdViolencia.setTiempoHoras(123);
        sdViolencia.setTiempoMinutos(0);
        sdViolencia.setTiempoTrabajadoDias(8);
        sdViolencia.setTotalConsultas(12);
        sdViolencia.setNum1EraVez(5);
        sdViolencia.setNumNoOtorgadas(6);
        sdViolencia.setPromedioHoras(10.89);
        sdViolencia.setNumVisitas(4);
        sdViolencia.setNumCitas(8);
        sdViolencia.setNumCitasCumplidas(2);
        sdViolencia.setNumEnUnidad(1);
        sdViolencia.setNumOtraUnidad(4);
        sdViolencia.setNumAltasEspeciales(7);
        sdViolencia.setNumAltasEspeciales(1);
        sdViolencia.setNumExpedidas(0);
        sdViolencia.setNumDias(3);
        sdViolencia.setPromedio(10.05);
        sdViolencia.setNumRecetas(9);
        sdViolencia.setPorcentajeAtenciones(20.21);

        sdRenglonesEspeciales.setSdViolencia(sdViolencia);

        List<SdTotalesReporte> sdParamedicos = new ArrayList<>();
        SdTotalesReporte paramedico1 = new SdTotalesReporte();
        paramedico1.setCveEspecialidad("3100");
        paramedico1.setDesEspecialidad("Otorrinolaringología");
        paramedico1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        paramedico1.setNumMedicos(10);
        paramedico1.setTiempoTrabajadoHoras("123:00");
        paramedico1.setTiempoHoras(123);
        paramedico1.setTiempoMinutos(0);
        paramedico1.setTiempoTrabajadoDias(8);
        paramedico1.setTotalConsultas(12);
        paramedico1.setNum1EraVez(5);
        paramedico1.setNumNoOtorgadas(6);
        paramedico1.setPromedioHoras(10.89);
        paramedico1.setNumVisitas(4);
        paramedico1.setNumCitas(8);
        paramedico1.setNumCitasCumplidas(2);
        paramedico1.setNumEnUnidad(1);
        paramedico1.setNumOtraUnidad(4);
        paramedico1.setNumAltasEspeciales(7);
        paramedico1.setNumAltasEspeciales(1);
        paramedico1.setNumExpedidas(0);
        paramedico1.setNumDias(3);
        paramedico1.setPromedio(10.05);
        paramedico1.setNumRecetas(9);
        paramedico1.setPorcentajeAtenciones(20.21);

        SdTotalesReporte paramedico2 = new SdTotalesReporte();
        paramedico2.setCveEspecialidad("3100");
        paramedico2.setDesEspecialidad("Otorrinolaringología");
        paramedico2.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        paramedico2.setNumMedicos(10);
        paramedico2.setTiempoTrabajadoHoras("123:00");
        paramedico2.setTiempoHoras(123);
        paramedico2.setTiempoMinutos(0);
        paramedico2.setTiempoTrabajadoDias(8);
        paramedico2.setTotalConsultas(12);
        paramedico2.setNum1EraVez(5);
        paramedico2.setNumNoOtorgadas(6);
        paramedico2.setPromedioHoras(10.89);
        paramedico2.setNumVisitas(4);
        paramedico2.setNumCitas(8);
        paramedico2.setNumCitasCumplidas(2);
        paramedico2.setNumEnUnidad(1);
        paramedico2.setNumOtraUnidad(4);
        paramedico2.setNumAltasEspeciales(7);
        paramedico2.setNumAltasEspeciales(1);
        paramedico2.setNumExpedidas(0);
        paramedico2.setNumDias(3);
        paramedico2.setPromedio(10.05);
        paramedico2.setNumRecetas(9);
        paramedico2.setPorcentajeAtenciones(20.21);

        sdParamedicos.add(paramedico1);
        sdParamedicos.add(paramedico2);

        sdRenglonesEspeciales.setSdParamedicos(sdParamedicos);

        SdTotalesReporte sdTococirugia = new SdTotalesReporte();
        sdTococirugia.setCveEspecialidad("3100");
        sdTococirugia.setDesEspecialidad("Otorrinolaringología");
        sdTococirugia.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdTococirugia.setNumMedicos(10);
        sdTococirugia.setTiempoTrabajadoHoras("123:00");
        sdTococirugia.setTiempoHoras(123);
        sdTococirugia.setTiempoMinutos(0);
        sdTococirugia.setTiempoTrabajadoDias(8);
        sdTococirugia.setTotalConsultas(12);
        sdTococirugia.setNum1EraVez(5);
        sdTococirugia.setNumNoOtorgadas(6);
        sdTococirugia.setPromedioHoras(10.89);
        sdTococirugia.setNumVisitas(4);
        sdTococirugia.setNumCitas(8);
        sdTococirugia.setNumCitasCumplidas(2);
        sdTococirugia.setNumEnUnidad(1);
        sdTococirugia.setNumOtraUnidad(4);
        sdTococirugia.setNumAltasEspeciales(7);
        sdTococirugia.setNumAltasEspeciales(1);
        sdTococirugia.setNumExpedidas(0);
        sdTococirugia.setNumDias(3);
        sdTococirugia.setPromedio(10.05);
        sdTococirugia.setNumRecetas(9);
        sdTococirugia.setPorcentajeAtenciones(20.21);

        sdRenglonesEspeciales.setSdTococirugia(sdTococirugia);

        vista1.setSdRenglonesEspeciales(sdRenglonesEspeciales);

        vistas.setSdVista1(vista1);

        // VISTA 2

        SdVista2 vista2 = new SdVista2();

        List<SdV2Especialidad> sdV2Especialidad = new ArrayList<>();

        SdV2Especialidad v2especialidad1 = new SdV2Especialidad();
        v2especialidad1.setCveEspecialidad("3100");
        v2especialidad1.setDesEspecialidad("Otorrinolaringología");
        v2especialidad1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");

        List<SdV2Medico> sdV2MedicoLista1 = new ArrayList<>();
        SdV2Medico sdV2Medico1 = new SdV2Medico();
        sdV2Medico1.setCveMatricula("9282718");
        sdV2Medico1.setRefApellidoPaterno("Perez");
        sdV2Medico1.setRefApellidoMaterno("Jimenez");
        sdV2Medico1.setRefNombre("Antonio");
        sdV2Medico1.setRefVistaMedico("Antonio Perez Jimenez");
        sdV2Medico1.setTiempoTrabajadoHoras("123:00");
        sdV2Medico1.setTiempoTrabajadoDias(8);
        sdV2Medico1.setTotalConsultas(12);
        sdV2Medico1.setNum1EraVez(5);
        sdV2Medico1.setNumNoOtorgadas(6);
        sdV2Medico1.setPromedioHoras(10.89);
        sdV2Medico1.setNumVisitas(4);
        sdV2Medico1.setNumCitas(8);
        sdV2Medico1.setNumCitasCumplidas(2);
        sdV2Medico1.setNumEnUnidad(1);
        sdV2Medico1.setNumOtraUnidad(4);
        sdV2Medico1.setNumAltasEspeciales(7);
        sdV2Medico1.setNumAltasEspeciales(1);
        sdV2Medico1.setNumExpedidas(0);
        sdV2Medico1.setNumDias(3);
        sdV2Medico1.setPromedio(10.05);
        sdV2Medico1.setNumRecetas(9);
        sdV2Medico1.setPorcentajeAtenciones(20.21);

        SdV2Medico sdV2Medico2 = new SdV2Medico();
        sdV2Medico2.setCveMatricula("0982718");
        sdV2Medico2.setRefApellidoPaterno("Matinez");
        sdV2Medico2.setRefApellidoMaterno("Apodaca");
        sdV2Medico2.setRefNombre("José");
        sdV2Medico2.setRefVistaMedico("José Matinez Apodaca");
        sdV2Medico2.setTiempoTrabajadoHoras("173:00");
        sdV2Medico2.setTiempoTrabajadoDias(8);
        sdV2Medico2.setTotalConsultas(12);
        sdV2Medico2.setNum1EraVez(5);
        sdV2Medico2.setNumNoOtorgadas(6);
        sdV2Medico2.setPromedioHoras(10.89);
        sdV2Medico2.setNumVisitas(4);
        sdV2Medico2.setNumCitas(8);
        sdV2Medico2.setNumCitasCumplidas(2);
        sdV2Medico2.setNumEnUnidad(1);
        sdV2Medico2.setNumOtraUnidad(4);
        sdV2Medico2.setNumAltasEspeciales(7);
        sdV2Medico2.setNumAltasEspeciales(1);
        sdV2Medico2.setNumExpedidas(0);
        sdV2Medico2.setNumDias(3);
        sdV2Medico2.setPromedio(10.05);
        sdV2Medico2.setNumRecetas(9);
        sdV2Medico2.setPorcentajeAtenciones(20.21);

        sdV2MedicoLista1.add(sdV2Medico1);
        sdV2MedicoLista1.add(sdV2Medico2);

        v2especialidad1.setSdV2Medico(sdV2MedicoLista1);

        SdV2Especialidad v2especialidad2 = new SdV2Especialidad();
        v2especialidad2.setCveEspecialidad("3200");
        v2especialidad2.setDesEspecialidad("Otorrinolaringología");
        v2especialidad2.setRefVistaEspecialidad("** 3200 Otorrinolaringología **");

        List<SdV2Medico> sdV2MedicoLista2 = new ArrayList<>();
        SdV2Medico sdV2Medico21 = new SdV2Medico();
        sdV2Medico21.setCveMatricula("9282728");
        sdV2Medico21.setRefApellidoPaterno("Perez");
        sdV2Medico21.setRefApellidoMaterno("Jimenez");
        sdV2Medico21.setRefNombre("Antonio");
        sdV2Medico21.setRefVistaMedico("Antonio Perez Jimenez");
        sdV2Medico21.setTiempoTrabajadoHoras("223:00");
        sdV2Medico21.setTiempoTrabajadoDias(8);
        sdV2Medico21.setTotalConsultas(22);
        sdV2Medico21.setNum1EraVez(5);
        sdV2Medico21.setNumNoOtorgadas(6);
        sdV2Medico21.setPromedioHoras(20.89);
        sdV2Medico21.setNumVisitas(4);
        sdV2Medico21.setNumCitas(8);
        sdV2Medico21.setNumCitasCumplidas(2);
        sdV2Medico21.setNumEnUnidad(2);
        sdV2Medico21.setNumOtraUnidad(4);
        sdV2Medico21.setNumAltasEspeciales(7);
        sdV2Medico21.setNumAltasEspeciales(2);
        sdV2Medico21.setNumExpedidas(0);
        sdV2Medico21.setNumDias(3);
        sdV2Medico21.setPromedio(20.05);
        sdV2Medico21.setNumRecetas(9);
        sdV2Medico21.setPorcentajeAtenciones(20.22);

        SdV2Medico sdV2Medico22 = new SdV2Medico();
        sdV2Medico22.setCveMatricula("0982728");
        sdV2Medico22.setRefApellidoPaterno("Matinez");
        sdV2Medico22.setRefApellidoMaterno("Apodaca");
        sdV2Medico22.setRefNombre("José");
        sdV2Medico22.setRefVistaMedico("José Matinez Apodaca");
        sdV2Medico22.setTiempoTrabajadoHoras("273:00");
        sdV2Medico22.setTiempoTrabajadoDias(8);
        sdV2Medico22.setTotalConsultas(22);
        sdV2Medico22.setNum1EraVez(5);
        sdV2Medico22.setNumNoOtorgadas(6);
        sdV2Medico22.setPromedioHoras(20.89);
        sdV2Medico22.setNumVisitas(4);
        sdV2Medico22.setNumCitas(8);
        sdV2Medico22.setNumCitasCumplidas(2);
        sdV2Medico22.setNumEnUnidad(2);
        sdV2Medico22.setNumOtraUnidad(4);
        sdV2Medico22.setNumAltasEspeciales(7);
        sdV2Medico22.setNumAltasEspeciales(2);
        sdV2Medico22.setNumExpedidas(0);
        sdV2Medico22.setNumDias(3);
        sdV2Medico22.setPromedio(20.05);
        sdV2Medico22.setNumRecetas(9);
        sdV2Medico22.setPorcentajeAtenciones(20.22);

        sdV2MedicoLista2.add(sdV2Medico2);
        sdV2MedicoLista2.add(sdV2Medico2);

        v2especialidad2.setSdV2Medico(sdV2MedicoLista2);

        sdV2Especialidad.add(v2especialidad1);
        sdV2Especialidad.add(v2especialidad2);

        vista2.setSdV2Especialidad(sdV2Especialidad);
        vistas.setSdVista1(vista1);
        vistas.setSdVista2(vista2);

        reporte.setSdVistas(vistas);
        try {

            // Cuando se hace el insert en mongo, él asigna un id a la Entidad por lo que al
            // usar mock es necesario asignar ese Id como parte de un Answer
            Mockito.doAnswer(new Answer<Void>() {

                public Void answer(InvocationOnMock invocation) {

                    Object[] args = invocation.getArguments();
                    SitReporteParteI srp1 = (SitReporteParteI) args[0];
                    srp1.setId(new ObjectId());
                    return null;
                }
            }).when(procesarReporteParteIRepository).insertarReporteParte1(any(SitReporteParteI.class));
            procesarReporteParteIService.insertarReporte(reporte);
            logger.info("Fecha de generación: {}", reporte.getFecGeneracion());
            logger.info("Unidad Medica: {}", reporte.getSdUnidadMedica());
            logger.info("Periodo: {}", reporte.getSdPeriodoReporte());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void extraerInfoMedicosReporteTest() {

        SicPeriodosImss periodo = new SicPeriodosImss();

        try {
            periodo.setCvePeriodo(201801);
            periodo.setFecBaja(null);
            periodo.setFecInicial(sdf.parse("2017/12/26"));
            periodo.setFecFinal(sdf.parse("2018/01/25"));
            periodo.setId(new ObjectId());
            periodo.setIndVigente(1);
            periodo.setNumAnioPeriodo(2018);
            periodo.setNumMesPeriodo("01");
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try {
            String cvePresupuestal = "030103022151";
            String desunidadMedica = "HGZMF 1 La Paz";

            List<ExtractoReporteParteI> extraccionReporteEspecialidadesList11 = new ArrayList<>();

            ExtractoReporteParteI extr11 = sdExtractoReporteParte1Extr11(desunidadMedica);

            ExtractoReporteParteI extr12 = sdExtractoReporteParte1Extr12(desunidadMedica);

            extraccionReporteEspecialidadesList11.add(extr11);
            extraccionReporteEspecialidadesList11.add(extr12);

            Iterator<ExtractoReporteParteI> extraccionReporteEspecialidades = extraccionReporteEspecialidadesList11
                    .iterator();

            // EXTRACCION extraccionReporteUrgencias
            List<ExtractoReporteParteI> extraccionReporteUrgenciasList = new ArrayList<>();

            ExtractoReporteParteI extr21 = extractoReporteParteIExtr21(desunidadMedica);

            ExtractoReporteParteI extr22 = extractoReporteParteIExtr22(desunidadMedica);

            extraccionReporteUrgenciasList.add(extr21);
            extraccionReporteUrgenciasList.add(extr22);
            Iterator<ExtractoReporteParteI> extraccionReporteUrgencias = extraccionReporteUrgenciasList.iterator();

            // EXTRACCION ReporteTocoCirugia

            List<ExtractoReporteParteI> extraccionReporteTocoCirugialList = new ArrayList<>();

            ExtractoReporteParteI extr31 = extractoReporteParteIExtr31(desunidadMedica);

            ExtractoReporteParteI extr32 = extractoReporteParteIExtr32(desunidadMedica);

            extraccionReporteTocoCirugialList.add(extr31);
            extraccionReporteTocoCirugialList.add(extr32);
            Iterator<ExtractoReporteParteI> extraccionReporteTocoCirugia = extraccionReporteTocoCirugialList.iterator();
            // EXTRACCION ReporteParamedicos

            List<ExtractoReporteParteI> extraccionReporteParamedicosList = new ArrayList<>();

            AgrupacionId groupId41 = new AgrupacionId();
            List<String> matricula41 = new ArrayList<>();
            List<String> nom41 = new ArrayList<>();
            List<String> apP41 = new ArrayList<>();
            List<String> apM41 = new ArrayList<>();
            List<String> esp41 = new ArrayList<>();
            matricula41.add("9876875");
            nom41.add("Juan");
            apP41.add("Reyes");
            apM41.add("Perez");
            esp41.add("1600");

            groupId41.setMatricula(matricula41);
            groupId41.setNom(nom41);
            groupId41.setApP(apP41);
            groupId41.setApM(apM41);
            groupId41.setEsp(esp41);

            List<NumHorasTrabajadas> amTiempoTraHoras41 = new ArrayList<>();
            NumHorasTrabajadas numtra41 = new NumHorasTrabajadas();
            numtra41.setHoras("10:00");

            amTiempoTraHoras41.add(numtra41);

            ExtractoReporteParteI extr41 = extractoReporteParteIExtr41(desunidadMedica, groupId41, amTiempoTraHoras41);

            ExtractoReporteParteI extr42 = extractoReporteParteIExtr42(desunidadMedica);

            extraccionReporteParamedicosList.add(extr41);
            extraccionReporteParamedicosList.add(extr42);
            Iterator<ExtractoReporteParteI> extraccionReporteParamedicos = extraccionReporteParamedicosList.iterator();

            // ReporteParamedicos nReporteAccidentesYles

            List<ExtractoReporteParteIAccidentesyL> extraccionReporteAccidentesYlesList = new ArrayList<>();

            AgrupacionId groupId51 = new AgrupacionId();
            List<String> matricula51 = new ArrayList<>();
            List<String> nom51 = new ArrayList<>();
            List<String> apP51 = new ArrayList<>();
            List<String> apM51 = new ArrayList<>();
            List<String> esp51 = new ArrayList<>();
            matricula51.add("9876875");
            nom51.add("Juan");
            apP51.add("Reyes");
            apM51.add("Perez");
            esp51.add("1600");

            groupId51.setMatricula(matricula51);
            groupId51.setNom(nom51);
            groupId51.setApP(apP51);
            groupId51.setApM(apM51);
            groupId51.setEsp(esp51);

            List<NumHorasTrabajadas> amTiempoTraHoras51 = new ArrayList<>();
            NumHorasTrabajadas numtra51 = new NumHorasTrabajadas();
            numtra51.setHoras("10:00");

            amTiempoTraHoras51.add(numtra51);

            ExtractoReporteParteIAccidentesyL extr51 = new ExtractoReporteParteIAccidentesyL();
            extr51.setAmTiempoTraDias(8);
            extr51.setAmTotCons(10);
            extr51.setAm1Vez(3);
            extr51.setAmNoOtorgadas(5);
            extr51.setAmNumVisitas(2);
            extr51.setAmCitas(1);
            extr51.setAmCitasCumplidas(1);
            extr51.setPeEnUnidad(0);
            extr51.setPeOtraUnidad(1);
            extr51.setPeAltasEspeciales(2);
            extr51.setIncExpedidas(4);
            extr51.setIncDias(23);
            extr51.setIncProm(4.0);
            extr51.setRecNum(9);
            extr51.setRecPorcentajeAten(1.0);

            ExtractoReporteParteIAccidentesyL extr52 = new ExtractoReporteParteIAccidentesyL();

            AgrupacionId groupId52 = new AgrupacionId();
            List<String> matricula52 = new ArrayList<>();
            List<String> nom52 = new ArrayList<>();
            List<String> apP52 = new ArrayList<>();
            List<String> apM52 = new ArrayList<>();
            List<String> esp52 = new ArrayList<>();
            matricula52.add("9876875");
            nom52.add("Juan");
            apP52.add("Reyes");
            apM52.add("Perez");
            esp52.add("1600");

            groupId52.setMatricula(matricula52);
            groupId52.setNom(nom52);
            groupId52.setApP(apP52);
            groupId52.setApM(apM52);
            groupId52.setEsp(esp52);

            List<NumHorasTrabajadas> amTiempoTraHoras52 = new ArrayList<>();
            NumHorasTrabajadas numtra52 = new NumHorasTrabajadas();
            numtra52.setHoras("10:00");

            amTiempoTraHoras52.add(numtra52);

            extr52.setAmTiempoTraDias(8);
            extr52.setAmTotCons(10);
            extr52.setAm1Vez(3);
            extr52.setAmNoOtorgadas(5);
            extr52.setAmNumVisitas(2);
            extr52.setAmCitas(1);
            extr52.setAmCitasCumplidas(1);
            extr52.setPeEnUnidad(0);
            extr52.setPeOtraUnidad(1);
            extr52.setPeAltasEspeciales(2);
            extr52.setIncExpedidas(4);
            extr52.setIncDias(23);
            extr52.setIncProm(4.0);
            extr52.setRecNum(9);
            extr52.setRecPorcentajeAten(1.0);

            extraccionReporteAccidentesYlesList.add(extr51);
            extraccionReporteAccidentesYlesList.add(extr52);
            Iterator<ExtractoReporteParteIAccidentesyL> extraccionReporteAccidentesYles = extraccionReporteAccidentesYlesList
                    .iterator();

            // CREAR INFORMACION REPORTE especialidades

            SdVistas vistasEspecialidades = new SdVistas();
            SdVista1 vista1Especialidades = new SdVista1();

            SdTotalesReporte sdRepEspecialidades = new SdTotalesReporte();

            sdRepEspecialidades.setCveEspecialidad("2100");
            sdRepEspecialidades.setDesEspecialidad("Ginecología");
            sdRepEspecialidades.setRefVistaEspecialidad("** 2100 Ginecología **");
            sdRepEspecialidades.setNumMedicos(10);
            sdRepEspecialidades.setTiempoTrabajadoHoras("123:00");
            sdRepEspecialidades.setTiempoHoras(123);
            sdRepEspecialidades.setTiempoMinutos(0);
            sdRepEspecialidades.setTiempoTrabajadoDias(8);
            sdRepEspecialidades.setTotalConsultas(12);
            sdRepEspecialidades.setNum1EraVez(5);
            sdRepEspecialidades.setNumNoOtorgadas(6);
            sdRepEspecialidades.setPromedioHoras(10.89);
            sdRepEspecialidades.setNumVisitas(4);
            sdRepEspecialidades.setNumCitas(8);
            sdRepEspecialidades.setNumCitasCumplidas(2);
            sdRepEspecialidades.setNumEnUnidad(1);
            sdRepEspecialidades.setNumOtraUnidad(4);
            sdRepEspecialidades.setNumAltasEspeciales(7);
            sdRepEspecialidades.setNumAltasEspeciales(1);
            sdRepEspecialidades.setNumExpedidas(0);
            sdRepEspecialidades.setNumDias(3);
            sdRepEspecialidades.setPromedio(10.05);
            sdRepEspecialidades.setNumRecetas(9);
            sdRepEspecialidades.setPorcentajeAtenciones(20.21);

            vista1Especialidades.setSdRepEspecialidades(sdRepEspecialidades);
            List<SdV2Especialidad> listaEspecialidades = new ArrayList<>();

            SdV2Especialidad v2especialidades1 = new SdV2Especialidad();
            v2especialidades1.setCveEspecialidad("3100");
            v2especialidades1.setDesEspecialidad("Otorrinolaringología");
            v2especialidades1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");

            SdV2Medico sdV2MedicoEspecialidades12 = getSd2MedicoEspecialidad1(v2especialidades1);

            SdV2Especialidad v2especialidades2 = new SdV2Especialidad();
            v2especialidades2.setCveEspecialidad("3200");
            v2especialidades2.setDesEspecialidad("Otorrinolaringología");
            v2especialidades2.setRefVistaEspecialidad("** 3200 Otorrinolaringología **");

            List<SdV2Medico> sdV2MedicoEspecialidadesLista2 = new ArrayList<>();
            SdV2Medico sdV2MedicoEspecialidades21 = new SdV2Medico();
            sdV2MedicoEspecialidades21.setCveMatricula("9282728");
            sdV2MedicoEspecialidades21.setRefApellidoPaterno("Perez");
            sdV2MedicoEspecialidades21.setRefApellidoMaterno("Jimenez");
            sdV2MedicoEspecialidades21.setRefNombre("Antonio");
            sdV2MedicoEspecialidades21.setRefVistaMedico("Antonio Perez Jimenez");
            sdV2MedicoEspecialidades21.setTiempoTrabajadoHoras("223:00");
            sdV2MedicoEspecialidades21.setTiempoTrabajadoDias(8);
            sdV2MedicoEspecialidades21.setTotalConsultas(22);
            sdV2MedicoEspecialidades21.setNum1EraVez(5);
            sdV2MedicoEspecialidades21.setNumNoOtorgadas(6);
            sdV2MedicoEspecialidades21.setPromedioHoras(20.89);
            sdV2MedicoEspecialidades21.setNumVisitas(4);
            sdV2MedicoEspecialidades21.setNumCitas(8);
            sdV2MedicoEspecialidades21.setNumCitasCumplidas(2);
            sdV2MedicoEspecialidades21.setNumEnUnidad(2);
            sdV2MedicoEspecialidades21.setNumOtraUnidad(4);
            sdV2MedicoEspecialidades21.setNumAltasEspeciales(7);
            sdV2MedicoEspecialidades21.setNumAltasEspeciales(2);
            sdV2MedicoEspecialidades21.setNumExpedidas(0);
            sdV2MedicoEspecialidades21.setNumDias(3);
            sdV2MedicoEspecialidades21.setPromedio(20.05);
            sdV2MedicoEspecialidades21.setNumRecetas(9);
            sdV2MedicoEspecialidades21.setPorcentajeAtenciones(20.22);

            SdV2Medico sdV2MedicoEspecialidades22 = new SdV2Medico();
            sdV2MedicoEspecialidades22.setCveMatricula("0982728");
            sdV2MedicoEspecialidades22.setRefApellidoPaterno("Matinez");
            sdV2MedicoEspecialidades22.setRefApellidoMaterno("Apodaca");
            sdV2MedicoEspecialidades22.setRefNombre("José");
            sdV2MedicoEspecialidades22.setRefVistaMedico("José Matinez Apodaca");
            sdV2MedicoEspecialidades22.setTiempoTrabajadoHoras("273:00");
            sdV2MedicoEspecialidades22.setTiempoTrabajadoDias(8);
            sdV2MedicoEspecialidades22.setTotalConsultas(22);
            sdV2MedicoEspecialidades22.setNum1EraVez(5);
            sdV2MedicoEspecialidades22.setNumNoOtorgadas(6);
            sdV2MedicoEspecialidades22.setPromedioHoras(20.89);
            sdV2MedicoEspecialidades22.setNumVisitas(4);
            sdV2MedicoEspecialidades22.setNumCitas(8);
            sdV2MedicoEspecialidades22.setNumCitasCumplidas(2);
            sdV2MedicoEspecialidades22.setNumEnUnidad(2);
            sdV2MedicoEspecialidades22.setNumOtraUnidad(4);
            sdV2MedicoEspecialidades22.setNumAltasEspeciales(7);
            sdV2MedicoEspecialidades22.setNumAltasEspeciales(2);
            sdV2MedicoEspecialidades22.setNumExpedidas(0);
            sdV2MedicoEspecialidades22.setNumDias(3);
            sdV2MedicoEspecialidades22.setPromedio(20.05);
            sdV2MedicoEspecialidades22.setNumRecetas(9);
            sdV2MedicoEspecialidades22.setPorcentajeAtenciones(20.22);

            sdV2MedicoEspecialidadesLista2.add(sdV2MedicoEspecialidades12);
            sdV2MedicoEspecialidadesLista2.add(sdV2MedicoEspecialidades12);

            v2especialidades2.setSdV2Medico(sdV2MedicoEspecialidadesLista2);

            listaEspecialidades.add(v2especialidades1);
            listaEspecialidades.add(v2especialidades2);

            SdVista2 vista2Especialidades = new SdVista2();
            vista2Especialidades.setSdV2Especialidad(listaEspecialidades);
            vistasEspecialidades.setSdVista1(vista1Especialidades);
            vistasEspecialidades.setSdVista2(vista2Especialidades);

            // CREAR INFORMACION REPORTE urgencias
            SdVistas vistasUrgencias = new SdVistas();
            SdVista1 vista1Urgencias = new SdVista1();
            SdVista2 vista2Urgencias = new SdVista2();
            SdTotalesReporte sdRepUrgencias = new SdTotalesReporte();

            sdRepUrgencias.setCveEspecialidad("2100");
            sdRepUrgencias.setDesEspecialidad("Ginecología");
            sdRepUrgencias.setRefVistaEspecialidad("** 2100 Ginecología **");
            sdRepUrgencias.setNumMedicos(10);
            sdRepUrgencias.setTiempoTrabajadoHoras("123:00");
            sdRepUrgencias.setTiempoHoras(123);
            sdRepUrgencias.setTiempoMinutos(0);
            sdRepUrgencias.setTiempoTrabajadoDias(8);
            sdRepUrgencias.setTotalConsultas(12);
            sdRepUrgencias.setNum1EraVez(5);
            sdRepUrgencias.setNumNoOtorgadas(6);
            sdRepUrgencias.setPromedioHoras(10.89);
            sdRepUrgencias.setNumVisitas(4);
            sdRepUrgencias.setNumCitas(8);
            sdRepUrgencias.setNumCitasCumplidas(2);
            sdRepUrgencias.setNumEnUnidad(1);
            sdRepUrgencias.setNumOtraUnidad(4);
            sdRepUrgencias.setNumAltasEspeciales(7);
            sdRepUrgencias.setNumAltasEspeciales(1);
            sdRepUrgencias.setNumExpedidas(0);
            sdRepUrgencias.setNumDias(3);
            sdRepUrgencias.setPromedio(10.05);
            sdRepUrgencias.setNumRecetas(9);
            sdRepUrgencias.setPorcentajeAtenciones(20.21);

            vista1Urgencias.setSdRepEspecialidades(sdRepUrgencias);
            List<SdV2Especialidad> listaUrgencias = new ArrayList<>();

            SdV2Especialidad v2urgencias1 = new SdV2Especialidad();
            v2urgencias1.setCveEspecialidad("3100");
            v2urgencias1.setDesEspecialidad("Otorrinolaringología");
            v2urgencias1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");

            List<SdV2Medico> sdV2MedicoUrgenciasLista1 = new ArrayList<>();
            SdV2Medico sdV2MedicoUrgencias11 = new SdV2Medico();
            sdV2MedicoUrgencias11.setCveMatricula("9282718");
            sdV2MedicoUrgencias11.setRefApellidoPaterno("Perez");
            sdV2MedicoUrgencias11.setRefApellidoMaterno("Jimenez");
            sdV2MedicoUrgencias11.setRefNombre("Antonio");
            sdV2MedicoUrgencias11.setRefVistaMedico("Antonio Perez Jimenez");
            sdV2MedicoUrgencias11.setTiempoTrabajadoHoras("123:00");
            sdV2MedicoUrgencias11.setTiempoTrabajadoDias(8);
            sdV2MedicoUrgencias11.setTotalConsultas(12);
            sdV2MedicoUrgencias11.setNum1EraVez(5);
            sdV2MedicoUrgencias11.setNumNoOtorgadas(6);
            sdV2MedicoUrgencias11.setPromedioHoras(10.89);
            sdV2MedicoUrgencias11.setNumVisitas(4);
            sdV2MedicoUrgencias11.setNumCitas(8);
            sdV2MedicoUrgencias11.setNumCitasCumplidas(2);
            sdV2MedicoUrgencias11.setNumEnUnidad(1);
            sdV2MedicoUrgencias11.setNumOtraUnidad(4);
            sdV2MedicoUrgencias11.setNumAltasEspeciales(7);
            sdV2MedicoUrgencias11.setNumAltasEspeciales(1);
            sdV2MedicoUrgencias11.setNumExpedidas(0);
            sdV2MedicoUrgencias11.setNumDias(3);
            sdV2MedicoUrgencias11.setPromedio(10.05);
            sdV2MedicoUrgencias11.setNumRecetas(9);
            sdV2MedicoUrgencias11.setPorcentajeAtenciones(20.21);

            SdV2Medico sdV2MedicoUrgencias12 = new SdV2Medico();
            sdV2MedicoUrgencias12.setCveMatricula("0982718");
            sdV2MedicoUrgencias12.setRefApellidoPaterno("Matinez");
            sdV2MedicoUrgencias12.setRefApellidoMaterno("Apodaca");
            sdV2MedicoUrgencias12.setRefNombre("José");
            sdV2MedicoUrgencias12.setRefVistaMedico("José Matinez Apodaca");
            sdV2MedicoUrgencias12.setTiempoTrabajadoHoras("173:00");
            sdV2MedicoUrgencias12.setTiempoTrabajadoDias(8);
            sdV2MedicoUrgencias12.setTotalConsultas(12);
            sdV2MedicoUrgencias12.setNum1EraVez(5);
            sdV2MedicoUrgencias12.setNumNoOtorgadas(6);
            sdV2MedicoUrgencias12.setPromedioHoras(10.89);
            sdV2MedicoUrgencias12.setNumVisitas(4);
            sdV2MedicoUrgencias12.setNumCitas(8);
            sdV2MedicoUrgencias12.setNumCitasCumplidas(2);
            sdV2MedicoUrgencias12.setNumEnUnidad(1);
            sdV2MedicoUrgencias12.setNumOtraUnidad(4);
            sdV2MedicoUrgencias12.setNumAltasEspeciales(7);
            sdV2MedicoUrgencias12.setNumAltasEspeciales(1);
            sdV2MedicoUrgencias12.setNumExpedidas(0);
            sdV2MedicoUrgencias12.setNumDias(3);
            sdV2MedicoUrgencias12.setPromedio(10.05);
            sdV2MedicoUrgencias12.setNumRecetas(9);
            sdV2MedicoUrgencias12.setPorcentajeAtenciones(20.21);

            sdV2MedicoUrgenciasLista1.add(sdV2MedicoUrgencias11);
            sdV2MedicoUrgenciasLista1.add(sdV2MedicoUrgencias12);

            v2urgencias1.setSdV2Medico(sdV2MedicoUrgenciasLista1);

            SdV2Especialidad v2urgencias2 = new SdV2Especialidad();
            v2urgencias2.setCveEspecialidad("3200");
            v2urgencias2.setDesEspecialidad("Otorrinolaringología");
            v2urgencias2.setRefVistaEspecialidad("** 3200 Otorrinolaringología **");

            List<SdV2Medico> sdV2MedicoUrgenciasLista2 = new ArrayList<>();
            SdV2Medico sdV2MedicoUrgencias21 = new SdV2Medico();
            sdV2MedicoUrgencias21.setCveMatricula("9282728");
            sdV2MedicoUrgencias21.setRefApellidoPaterno("Perez");
            sdV2MedicoUrgencias21.setRefApellidoMaterno("Jimenez");
            sdV2MedicoUrgencias21.setRefNombre("Antonio");
            sdV2MedicoUrgencias21.setRefVistaMedico("Antonio Perez Jimenez");
            sdV2MedicoUrgencias21.setTiempoTrabajadoHoras("223:00");
            sdV2MedicoUrgencias21.setTiempoTrabajadoDias(8);
            sdV2MedicoUrgencias21.setTotalConsultas(22);
            sdV2MedicoUrgencias21.setNum1EraVez(5);
            sdV2MedicoUrgencias21.setNumNoOtorgadas(6);
            sdV2MedicoUrgencias21.setPromedioHoras(20.89);
            sdV2MedicoUrgencias21.setNumVisitas(4);
            sdV2MedicoUrgencias21.setNumCitas(8);
            sdV2MedicoUrgencias21.setNumCitasCumplidas(2);
            sdV2MedicoUrgencias21.setNumEnUnidad(2);
            sdV2MedicoUrgencias21.setNumOtraUnidad(4);
            sdV2MedicoUrgencias21.setNumAltasEspeciales(7);
            sdV2MedicoUrgencias21.setNumAltasEspeciales(2);
            sdV2MedicoUrgencias21.setNumExpedidas(0);
            sdV2MedicoUrgencias21.setNumDias(3);
            sdV2MedicoUrgencias21.setPromedio(20.05);
            sdV2MedicoUrgencias21.setNumRecetas(9);
            sdV2MedicoUrgencias21.setPorcentajeAtenciones(20.22);

            SdV2Medico sdV2MedicoUrgencias22 = new SdV2Medico();
            sdV2MedicoUrgencias22.setCveMatricula("0982728");
            sdV2MedicoUrgencias22.setRefApellidoPaterno("Matinez");
            sdV2MedicoUrgencias22.setRefApellidoMaterno("Apodaca");
            sdV2MedicoUrgencias22.setRefNombre("José");
            sdV2MedicoUrgencias22.setRefVistaMedico("José Matinez Apodaca");
            sdV2MedicoUrgencias22.setTiempoTrabajadoHoras("273:00");
            sdV2MedicoUrgencias22.setTiempoTrabajadoDias(8);
            sdV2MedicoUrgencias22.setTotalConsultas(22);
            sdV2MedicoUrgencias22.setNum1EraVez(5);
            sdV2MedicoUrgencias22.setNumNoOtorgadas(6);
            sdV2MedicoUrgencias22.setPromedioHoras(20.89);
            sdV2MedicoUrgencias22.setNumVisitas(4);
            sdV2MedicoUrgencias22.setNumCitas(8);
            sdV2MedicoUrgencias22.setNumCitasCumplidas(2);
            sdV2MedicoUrgencias22.setNumEnUnidad(2);
            sdV2MedicoUrgencias22.setNumOtraUnidad(4);
            sdV2MedicoUrgencias22.setNumAltasEspeciales(7);
            sdV2MedicoUrgencias22.setNumAltasEspeciales(2);
            sdV2MedicoUrgencias22.setNumExpedidas(0);
            sdV2MedicoUrgencias22.setNumDias(3);
            sdV2MedicoUrgencias22.setPromedio(20.05);
            sdV2MedicoUrgencias22.setNumRecetas(9);
            sdV2MedicoUrgencias22.setPorcentajeAtenciones(20.22);

            sdV2MedicoUrgenciasLista2.add(sdV2MedicoUrgencias12);
            sdV2MedicoUrgenciasLista2.add(sdV2MedicoUrgencias12);

            v2urgencias2.setSdV2Medico(sdV2MedicoUrgenciasLista2);

            listaUrgencias.add(v2urgencias1);
            listaUrgencias.add(v2urgencias2);

            vista2Urgencias.setSdV2Especialidad(listaUrgencias);
            vistasUrgencias.setSdVista1(vista1Urgencias);
            vistasUrgencias.setSdVista2(vista2Urgencias);

            // CREAR INFORMACION REPORTE tococirugia
            SdVistas vistasTococirugia = new SdVistas();
            SdVista1 vista1Tococirugia = new SdVista1();
            SdVista2 vista2Tococirugia = new SdVista2();
            SdTotalesReporte sdRepTococirugia = new SdTotalesReporte();

            sdRepTococirugia.setCveEspecialidad("2100");
            sdRepTococirugia.setDesEspecialidad("Ginecología");
            sdRepTococirugia.setRefVistaEspecialidad("** 2100 Ginecología **");
            sdRepTococirugia.setNumMedicos(10);
            sdRepTococirugia.setTiempoTrabajadoHoras("123:00");
            sdRepTococirugia.setTiempoHoras(123);
            sdRepTococirugia.setTiempoMinutos(0);
            sdRepTococirugia.setTiempoTrabajadoDias(8);
            sdRepTococirugia.setTotalConsultas(12);
            sdRepTococirugia.setNum1EraVez(5);
            sdRepTococirugia.setNumNoOtorgadas(6);
            sdRepTococirugia.setPromedioHoras(10.89);
            sdRepTococirugia.setNumVisitas(4);
            sdRepTococirugia.setNumCitas(8);
            sdRepTococirugia.setNumCitasCumplidas(2);
            sdRepTococirugia.setNumEnUnidad(1);
            sdRepTococirugia.setNumOtraUnidad(4);
            sdRepTococirugia.setNumAltasEspeciales(7);
            sdRepTococirugia.setNumAltasEspeciales(1);
            sdRepTococirugia.setNumExpedidas(0);
            sdRepTococirugia.setNumDias(3);
            sdRepTococirugia.setPromedio(10.05);
            sdRepTococirugia.setNumRecetas(9);
            sdRepTococirugia.setPorcentajeAtenciones(20.21);

            vista1Tococirugia.setSdRepEspecialidades(sdRepTococirugia);
            List<SdV2Especialidad> listaTococirugia = new ArrayList<>();

            SdV2Especialidad v2tococirugia1 = new SdV2Especialidad();
            v2tococirugia1.setCveEspecialidad("3100");
            v2tococirugia1.setDesEspecialidad("Otorrinolaringología");
            v2tococirugia1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");

            List<SdV2Medico> sdV2MedicoTococirugiaLista1 = new ArrayList<>();
            SdV2Medico sdV2MedicoTococirugia11 = new SdV2Medico();
            sdV2MedicoTococirugia11.setCveMatricula("9282718");
            sdV2MedicoTococirugia11.setRefApellidoPaterno("Perez");
            sdV2MedicoTococirugia11.setRefApellidoMaterno("Jimenez");
            sdV2MedicoTococirugia11.setRefNombre("Antonio");
            sdV2MedicoTococirugia11.setRefVistaMedico("Antonio Perez Jimenez");
            sdV2MedicoTococirugia11.setTiempoTrabajadoHoras("123:00");
            sdV2MedicoTococirugia11.setTiempoTrabajadoDias(8);
            sdV2MedicoTococirugia11.setTotalConsultas(12);
            sdV2MedicoTococirugia11.setNum1EraVez(5);
            sdV2MedicoTococirugia11.setNumNoOtorgadas(6);
            sdV2MedicoTococirugia11.setPromedioHoras(10.89);
            sdV2MedicoTococirugia11.setNumVisitas(4);
            sdV2MedicoTococirugia11.setNumCitas(8);
            sdV2MedicoTococirugia11.setNumCitasCumplidas(2);
            sdV2MedicoTococirugia11.setNumEnUnidad(1);
            sdV2MedicoTococirugia11.setNumOtraUnidad(4);
            sdV2MedicoTococirugia11.setNumAltasEspeciales(7);
            sdV2MedicoTococirugia11.setNumAltasEspeciales(1);
            sdV2MedicoTococirugia11.setNumExpedidas(0);
            sdV2MedicoTococirugia11.setNumDias(3);
            sdV2MedicoTococirugia11.setPromedio(10.05);
            sdV2MedicoTococirugia11.setNumRecetas(9);
            sdV2MedicoTococirugia11.setPorcentajeAtenciones(20.21);

            SdV2Medico sdV2MedicoTococirugia12 = new SdV2Medico();
            sdV2MedicoTococirugia12.setCveMatricula("0982718");
            sdV2MedicoTococirugia12.setRefApellidoPaterno("Matinez");
            sdV2MedicoTococirugia12.setRefApellidoMaterno("Apodaca");
            sdV2MedicoTococirugia12.setRefNombre("José");
            sdV2MedicoTococirugia12.setRefVistaMedico("José Matinez Apodaca");
            sdV2MedicoTococirugia12.setTiempoTrabajadoHoras("173:00");
            sdV2MedicoTococirugia12.setTiempoTrabajadoDias(8);
            sdV2MedicoTococirugia12.setTotalConsultas(12);
            sdV2MedicoTococirugia12.setNum1EraVez(5);
            sdV2MedicoTococirugia12.setNumNoOtorgadas(6);
            sdV2MedicoTococirugia12.setPromedioHoras(10.89);
            sdV2MedicoTococirugia12.setNumVisitas(4);
            sdV2MedicoTococirugia12.setNumCitas(8);
            sdV2MedicoTococirugia12.setNumCitasCumplidas(2);
            sdV2MedicoTococirugia12.setNumEnUnidad(1);
            sdV2MedicoTococirugia12.setNumOtraUnidad(4);
            sdV2MedicoTococirugia12.setNumAltasEspeciales(7);
            sdV2MedicoTococirugia12.setNumAltasEspeciales(1);
            sdV2MedicoTococirugia12.setNumExpedidas(0);
            sdV2MedicoTococirugia12.setNumDias(3);
            sdV2MedicoTococirugia12.setPromedio(10.05);
            sdV2MedicoTococirugia12.setNumRecetas(9);
            sdV2MedicoTococirugia12.setPorcentajeAtenciones(20.21);

            sdV2MedicoTococirugiaLista1.add(sdV2MedicoTococirugia11);
            sdV2MedicoTococirugiaLista1.add(sdV2MedicoTococirugia12);

            v2tococirugia1.setSdV2Medico(sdV2MedicoTococirugiaLista1);

            SdV2Especialidad v2tococirugia2 = new SdV2Especialidad();
            v2tococirugia2.setCveEspecialidad("3200");
            v2tococirugia2.setDesEspecialidad("Otorrinolaringología");
            v2tococirugia2.setRefVistaEspecialidad("** 3200 Otorrinolaringología **");

            List<SdV2Medico> sdV2MedicoTococirugiaLista2 = new ArrayList<>();
            SdV2Medico sdV2MedicoTococirugia21 = new SdV2Medico();
            sdV2MedicoTococirugia21.setCveMatricula("9282728");
            sdV2MedicoTococirugia21.setRefApellidoPaterno("Perez");
            sdV2MedicoTococirugia21.setRefApellidoMaterno("Jimenez");
            sdV2MedicoTococirugia21.setRefNombre("Antonio");
            sdV2MedicoTococirugia21.setRefVistaMedico("Antonio Perez Jimenez");
            sdV2MedicoTococirugia21.setTiempoTrabajadoHoras("223:00");
            sdV2MedicoTococirugia21.setTiempoTrabajadoDias(8);
            sdV2MedicoTococirugia21.setTotalConsultas(22);
            sdV2MedicoTococirugia21.setNum1EraVez(5);
            sdV2MedicoTococirugia21.setNumNoOtorgadas(6);
            sdV2MedicoTococirugia21.setPromedioHoras(20.89);
            sdV2MedicoTococirugia21.setNumVisitas(4);
            sdV2MedicoTococirugia21.setNumCitas(8);
            sdV2MedicoTococirugia21.setNumCitasCumplidas(2);
            sdV2MedicoTococirugia21.setNumEnUnidad(2);
            sdV2MedicoTococirugia21.setNumOtraUnidad(4);
            sdV2MedicoTococirugia21.setNumAltasEspeciales(7);
            sdV2MedicoTococirugia21.setNumAltasEspeciales(2);
            sdV2MedicoTococirugia21.setNumExpedidas(0);
            sdV2MedicoTococirugia21.setNumDias(3);
            sdV2MedicoTococirugia21.setPromedio(20.05);
            sdV2MedicoTococirugia21.setNumRecetas(9);
            sdV2MedicoTococirugia21.setPorcentajeAtenciones(20.22);

            SdV2Medico sdV2MedicoTococirugia22 = new SdV2Medico();
            sdV2MedicoTococirugia22.setCveMatricula("0982728");
            sdV2MedicoTococirugia22.setRefApellidoPaterno("Matinez");
            sdV2MedicoTococirugia22.setRefApellidoMaterno("Apodaca");
            sdV2MedicoTococirugia22.setRefNombre("José");
            sdV2MedicoTococirugia22.setRefVistaMedico("José Matinez Apodaca");
            sdV2MedicoTococirugia22.setTiempoTrabajadoHoras("273:00");
            sdV2MedicoTococirugia22.setTiempoTrabajadoDias(8);
            sdV2MedicoTococirugia22.setTotalConsultas(22);
            sdV2MedicoTococirugia22.setNum1EraVez(5);
            sdV2MedicoTococirugia22.setNumNoOtorgadas(6);
            sdV2MedicoTococirugia22.setPromedioHoras(20.89);
            sdV2MedicoTococirugia22.setNumVisitas(4);
            sdV2MedicoTococirugia22.setNumCitas(8);
            sdV2MedicoTococirugia22.setNumCitasCumplidas(2);
            sdV2MedicoTococirugia22.setNumEnUnidad(2);
            sdV2MedicoTococirugia22.setNumOtraUnidad(4);
            sdV2MedicoTococirugia22.setNumAltasEspeciales(7);
            sdV2MedicoTococirugia22.setNumAltasEspeciales(2);
            sdV2MedicoTococirugia22.setNumExpedidas(0);
            sdV2MedicoTococirugia22.setNumDias(3);
            sdV2MedicoTococirugia22.setPromedio(20.05);
            sdV2MedicoTococirugia22.setNumRecetas(9);
            sdV2MedicoTococirugia22.setPorcentajeAtenciones(20.22);

            sdV2MedicoTococirugiaLista2.add(sdV2MedicoTococirugia12);
            sdV2MedicoTococirugiaLista2.add(sdV2MedicoTococirugia12);

            v2tococirugia2.setSdV2Medico(sdV2MedicoTococirugiaLista2);

            listaTococirugia.add(v2tococirugia1);
            listaTococirugia.add(v2tococirugia2);

            vista2Tococirugia.setSdV2Especialidad(listaTococirugia);
            vistasTococirugia.setSdVista1(vista1Tococirugia);
            vistasTococirugia.setSdVista2(vista2Tococirugia);

            // CREAR INFORMACION REPORTE paramedicos
            SdVistas vistasParamedicos = new SdVistas();
            SdVista1 vista1Paramedicos = new SdVista1();
            SdVista2 vista2Paramedicos = new SdVista2();
            SdTotalesReporte sdRepParamedicos = new SdTotalesReporte();

            sdRepParamedicos.setCveEspecialidad("2100");
            sdRepParamedicos.setDesEspecialidad("Ginecología");
            sdRepParamedicos.setRefVistaEspecialidad("** 2100 Ginecología **");
            sdRepParamedicos.setNumMedicos(10);
            sdRepParamedicos.setTiempoTrabajadoHoras("123:00");
            sdRepParamedicos.setTiempoHoras(123);
            sdRepParamedicos.setTiempoMinutos(0);
            sdRepParamedicos.setTiempoTrabajadoDias(8);
            sdRepParamedicos.setTotalConsultas(12);
            sdRepParamedicos.setNum1EraVez(5);
            sdRepParamedicos.setNumNoOtorgadas(6);
            sdRepParamedicos.setPromedioHoras(10.89);
            sdRepParamedicos.setNumVisitas(4);
            sdRepParamedicos.setNumCitas(8);
            sdRepParamedicos.setNumCitasCumplidas(2);
            sdRepParamedicos.setNumEnUnidad(1);
            sdRepParamedicos.setNumOtraUnidad(4);
            sdRepParamedicos.setNumAltasEspeciales(7);
            sdRepParamedicos.setNumAltasEspeciales(1);
            sdRepParamedicos.setNumExpedidas(0);
            sdRepParamedicos.setNumDias(3);
            sdRepParamedicos.setPromedio(10.05);
            sdRepParamedicos.setNumRecetas(9);
            sdRepParamedicos.setPorcentajeAtenciones(20.21);

            vista1Paramedicos.setSdRepEspecialidades(sdRepParamedicos);
            List<SdV2Especialidad> listaParamedicos = new ArrayList<>();

            SdV2Especialidad v2paramedicos1 = new SdV2Especialidad();
            v2paramedicos1.setCveEspecialidad("3100");
            v2paramedicos1.setDesEspecialidad("Otorrinolaringología");
            v2paramedicos1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");

            List<SdV2Medico> sdV2MedicoParamedicosLista1 = new ArrayList<>();
            SdV2Medico sdV2MedicoParamedicos11 = new SdV2Medico();
            sdV2MedicoParamedicos11.setCveMatricula("9282718");
            sdV2MedicoParamedicos11.setRefApellidoPaterno("Perez");
            sdV2MedicoParamedicos11.setRefApellidoMaterno("Jimenez");
            sdV2MedicoParamedicos11.setRefNombre("Antonio");
            sdV2MedicoParamedicos11.setRefVistaMedico("Antonio Perez Jimenez");
            sdV2MedicoParamedicos11.setTiempoTrabajadoHoras("123:00");
            sdV2MedicoParamedicos11.setTiempoTrabajadoDias(8);
            sdV2MedicoParamedicos11.setTotalConsultas(12);
            sdV2MedicoParamedicos11.setNum1EraVez(5);
            sdV2MedicoParamedicos11.setNumNoOtorgadas(6);
            sdV2MedicoParamedicos11.setPromedioHoras(10.89);
            sdV2MedicoParamedicos11.setNumVisitas(4);
            sdV2MedicoParamedicos11.setNumCitas(8);
            sdV2MedicoParamedicos11.setNumCitasCumplidas(2);
            sdV2MedicoParamedicos11.setNumEnUnidad(1);
            sdV2MedicoParamedicos11.setNumOtraUnidad(4);
            sdV2MedicoParamedicos11.setNumAltasEspeciales(7);
            sdV2MedicoParamedicos11.setNumAltasEspeciales(1);
            sdV2MedicoParamedicos11.setNumExpedidas(0);
            sdV2MedicoParamedicos11.setNumDias(3);
            sdV2MedicoParamedicos11.setPromedio(10.05);
            sdV2MedicoParamedicos11.setNumRecetas(9);
            sdV2MedicoParamedicos11.setPorcentajeAtenciones(20.21);

            SdV2Medico sdV2MedicoParamedicos12 = new SdV2Medico();
            sdV2MedicoParamedicos12.setCveMatricula("0982718");
            sdV2MedicoParamedicos12.setRefApellidoPaterno("Matinez");
            sdV2MedicoParamedicos12.setRefApellidoMaterno("Apodaca");
            sdV2MedicoParamedicos12.setRefNombre("José");
            sdV2MedicoParamedicos12.setRefVistaMedico("José Matinez Apodaca");
            sdV2MedicoParamedicos12.setTiempoTrabajadoHoras("173:00");
            sdV2MedicoParamedicos12.setTiempoTrabajadoDias(8);
            sdV2MedicoParamedicos12.setTotalConsultas(12);
            sdV2MedicoParamedicos12.setNum1EraVez(5);
            sdV2MedicoParamedicos12.setNumNoOtorgadas(6);
            sdV2MedicoParamedicos12.setPromedioHoras(10.89);
            sdV2MedicoParamedicos12.setNumVisitas(4);
            sdV2MedicoParamedicos12.setNumCitas(8);
            sdV2MedicoParamedicos12.setNumCitasCumplidas(2);
            sdV2MedicoParamedicos12.setNumEnUnidad(1);
            sdV2MedicoParamedicos12.setNumOtraUnidad(4);
            sdV2MedicoParamedicos12.setNumAltasEspeciales(7);
            sdV2MedicoParamedicos12.setNumAltasEspeciales(1);
            sdV2MedicoParamedicos12.setNumExpedidas(0);
            sdV2MedicoParamedicos12.setNumDias(3);
            sdV2MedicoParamedicos12.setPromedio(10.05);
            sdV2MedicoParamedicos12.setNumRecetas(9);
            sdV2MedicoParamedicos12.setPorcentajeAtenciones(20.21);

            sdV2MedicoParamedicosLista1.add(sdV2MedicoParamedicos11);
            sdV2MedicoParamedicosLista1.add(sdV2MedicoParamedicos12);

            v2paramedicos1.setSdV2Medico(sdV2MedicoParamedicosLista1);

            SdV2Especialidad v2paramedicos2 = new SdV2Especialidad();
            v2paramedicos2.setCveEspecialidad("3200");
            v2paramedicos2.setDesEspecialidad("Otorrinolaringología");
            v2paramedicos2.setRefVistaEspecialidad("** 3200 Otorrinolaringología **");

            List<SdV2Medico> sdV2MedicoParamedicosLista2 = new ArrayList<>();
            SdV2Medico sdV2MedicoParamedicos21 = new SdV2Medico();
            sdV2MedicoParamedicos21.setCveMatricula("9282728");
            sdV2MedicoParamedicos21.setRefApellidoPaterno("Perez");
            sdV2MedicoParamedicos21.setRefApellidoMaterno("Jimenez");
            sdV2MedicoParamedicos21.setRefNombre("Antonio");
            sdV2MedicoParamedicos21.setRefVistaMedico("Antonio Perez Jimenez");
            sdV2MedicoParamedicos21.setTiempoTrabajadoHoras("223:00");
            sdV2MedicoParamedicos21.setTiempoTrabajadoDias(8);
            sdV2MedicoParamedicos21.setTotalConsultas(22);
            sdV2MedicoParamedicos21.setNum1EraVez(5);
            sdV2MedicoParamedicos21.setNumNoOtorgadas(6);
            sdV2MedicoParamedicos21.setPromedioHoras(20.89);
            sdV2MedicoParamedicos21.setNumVisitas(4);
            sdV2MedicoParamedicos21.setNumCitas(8);
            sdV2MedicoParamedicos21.setNumCitasCumplidas(2);
            sdV2MedicoParamedicos21.setNumEnUnidad(2);
            sdV2MedicoParamedicos21.setNumOtraUnidad(4);
            sdV2MedicoParamedicos21.setNumAltasEspeciales(7);
            sdV2MedicoParamedicos21.setNumAltasEspeciales(2);
            sdV2MedicoParamedicos21.setNumExpedidas(0);
            sdV2MedicoParamedicos21.setNumDias(3);
            sdV2MedicoParamedicos21.setPromedio(20.05);
            sdV2MedicoParamedicos21.setNumRecetas(9);
            sdV2MedicoParamedicos21.setPorcentajeAtenciones(20.22);

            SdV2Medico sdV2MedicoParamedicos22 = new SdV2Medico();
            sdV2MedicoParamedicos22.setCveMatricula("0982728");
            sdV2MedicoParamedicos22.setRefApellidoPaterno("Matinez");
            sdV2MedicoParamedicos22.setRefApellidoMaterno("Apodaca");
            sdV2MedicoParamedicos22.setRefNombre("José");
            sdV2MedicoParamedicos22.setRefVistaMedico("José Matinez Apodaca");
            sdV2MedicoParamedicos22.setTiempoTrabajadoHoras("273:00");
            sdV2MedicoParamedicos22.setTiempoTrabajadoDias(8);
            sdV2MedicoParamedicos22.setTotalConsultas(22);
            sdV2MedicoParamedicos22.setNum1EraVez(5);
            sdV2MedicoParamedicos22.setNumNoOtorgadas(6);
            sdV2MedicoParamedicos22.setPromedioHoras(20.89);
            sdV2MedicoParamedicos22.setNumVisitas(4);
            sdV2MedicoParamedicos22.setNumCitas(8);
            sdV2MedicoParamedicos22.setNumCitasCumplidas(2);
            sdV2MedicoParamedicos22.setNumEnUnidad(2);
            sdV2MedicoParamedicos22.setNumOtraUnidad(4);
            sdV2MedicoParamedicos22.setNumAltasEspeciales(7);
            sdV2MedicoParamedicos22.setNumAltasEspeciales(2);
            sdV2MedicoParamedicos22.setNumExpedidas(0);
            sdV2MedicoParamedicos22.setNumDias(3);
            sdV2MedicoParamedicos22.setPromedio(20.05);
            sdV2MedicoParamedicos22.setNumRecetas(9);
            sdV2MedicoParamedicos22.setPorcentajeAtenciones(20.22);

            sdV2MedicoParamedicosLista2.add(sdV2MedicoParamedicos12);
            sdV2MedicoParamedicosLista2.add(sdV2MedicoParamedicos12);

            v2paramedicos2.setSdV2Medico(sdV2MedicoParamedicosLista2);

            listaParamedicos.add(v2paramedicos1);
            listaParamedicos.add(v2paramedicos2);

            SdRenglonesEspecialesVista1 renglones = new SdRenglonesEspecialesVista1();
            List<SdTotalesReporte> listaDeParamedicos = new ArrayList<>();
            listaDeParamedicos.add(sdRepParamedicos);
            renglones.setSdParamedicos(listaDeParamedicos);
            vista1Paramedicos.setSdRenglonesEspeciales(renglones);

            vista2Paramedicos.setSdV2Especialidad(listaParamedicos);
            vistasParamedicos.setSdVista1(vista1Paramedicos);
            vistasParamedicos.setSdVista2(vista2Paramedicos);

            // CREAR INFORMACION REPORTE Accidentes y Lesiones

            SdTotalesReporte sdRepAccidentesYles = sdreaccidentes();

            // SUMA
            SdTotalesReporte sdRepSuma = sdreposuma();

            when(procesarReporteParteIRepository.obtenerPeriodImss(anyInt())).thenReturn(periodo);

            when(procesarReporteParteIRepository.extraerInfoMedicosReporteVista2(cvePresupuestal,
                    periodo.getFecInicial(), periodo.getFecFinal(), BaseConstants.TC_ESPECIALIDADES))
            				.thenReturn(extraccionReporteEspecialidades);
            when(procesarReporteParteIRepository.extraerInfoMedicosReporteVista2(cvePresupuestal,
                    periodo.getFecInicial(), periodo.getFecFinal(), BaseConstants.TC_URGENCIAS))
                            .thenReturn(extraccionReporteUrgencias);
            when(procesarReporteParteIRepository.extraerInfoMedicosReporteVista2(cvePresupuestal,
                    periodo.getFecInicial(), periodo.getFecFinal(), BaseConstants.TC_TOCOCIRUGIA))
                            .thenReturn(extraccionReporteTocoCirugia);
            when(procesarReporteParteIRepository.extraerInfoMedicosReporteVista2(cvePresupuestal,
                    periodo.getFecInicial(), periodo.getFecFinal(), BaseConstants.TC_PARAMEDICOS))
                            .thenReturn(extraccionReporteParamedicos);
            when(procesarReporteParteIRepository.extraerInfoMedicosReporteAccidentesyLes(cvePresupuestal,
                    periodo.getFecInicial(), periodo.getFecFinal())).thenReturn(extraccionReporteAccidentesYles);

            when(procesarReporteParteIRules.crearInformacionReporte(extraccionReporteEspecialidades, BaseConstants.TC_ESPECIALIDADES))
                    .thenReturn(vistasEspecialidades);
            when(procesarReporteParteIRules.crearInformacionReporte(extraccionReporteUrgencias, BaseConstants.TC_URGENCIAS))
                    .thenReturn(vistasUrgencias);
            when(procesarReporteParteIRules.crearInformacionReporte(extraccionReporteTocoCirugia, BaseConstants.TC_TOCOCIRUGIA))
                    .thenReturn(vistasTococirugia);
            when(procesarReporteParteIRules.crearInformacionReporteParamedicos(extraccionReporteParamedicos, BaseConstants.TC_PARAMEDICOS))
                    .thenReturn(vistasParamedicos);
            when(procesarReporteParteIRules.extraccionReporteAccidentesYles(extraccionReporteAccidentesYles))
                    .thenReturn(sdRepAccidentesYles);

            when(procesarReporteParteIRules.sumarTotales(sdRepEspecialidades, sdRepUrgencias)).thenReturn(sdRepSuma);

            SdVistas vistas = procesarReporteParteIService.extraerInfoMedicosReporte(cvePresupuestal, desunidadMedica,
                    periodo.getCvePeriodo());
            
            Assert.assertNotNull(vistas);
            
            logger.info("Vistas: {}", vistas);
            logger.info("Vista 1: {}", vistas.getSdVista1());
            logger.info("Vista 2: {}", vistas.getSdVista2());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private SdV2Medico getSd2MedicoEspecialidad1(SdV2Especialidad v2especialidades1) {

        List<SdV2Medico> sdV2MedicoEspecialidadesLista1 = new ArrayList<>();
        SdV2Medico sdV2MedicoEspecialidades11 = new SdV2Medico();
        sdV2MedicoEspecialidades11.setCveMatricula("9282718");
        sdV2MedicoEspecialidades11.setRefApellidoPaterno("Perez");
        sdV2MedicoEspecialidades11.setRefApellidoMaterno("Jimenez");
        sdV2MedicoEspecialidades11.setRefNombre("Antonio");
        sdV2MedicoEspecialidades11.setRefVistaMedico("Antonio Perez Jimenez");
        sdV2MedicoEspecialidades11.setTiempoTrabajadoHoras("123:00");
        sdV2MedicoEspecialidades11.setTiempoTrabajadoDias(8);
        sdV2MedicoEspecialidades11.setTotalConsultas(12);
        sdV2MedicoEspecialidades11.setNum1EraVez(5);
        sdV2MedicoEspecialidades11.setNumNoOtorgadas(6);
        sdV2MedicoEspecialidades11.setPromedioHoras(10.89);
        sdV2MedicoEspecialidades11.setNumVisitas(4);
        sdV2MedicoEspecialidades11.setNumCitas(8);
        sdV2MedicoEspecialidades11.setNumCitasCumplidas(2);
        sdV2MedicoEspecialidades11.setNumEnUnidad(1);
        sdV2MedicoEspecialidades11.setNumOtraUnidad(4);
        sdV2MedicoEspecialidades11.setNumAltasEspeciales(7);
        sdV2MedicoEspecialidades11.setNumAltasEspeciales(1);
        sdV2MedicoEspecialidades11.setNumExpedidas(0);
        sdV2MedicoEspecialidades11.setNumDias(3);
        sdV2MedicoEspecialidades11.setPromedio(10.05);
        sdV2MedicoEspecialidades11.setNumRecetas(9);
        sdV2MedicoEspecialidades11.setPorcentajeAtenciones(20.21);

        SdV2Medico sdV2MedicoEspecialidades12 = new SdV2Medico();
        sdV2MedicoEspecialidades12.setCveMatricula("0982718");
        sdV2MedicoEspecialidades12.setRefApellidoPaterno("Matinez");
        sdV2MedicoEspecialidades12.setRefApellidoMaterno("Apodaca");
        sdV2MedicoEspecialidades12.setRefNombre("José");
        sdV2MedicoEspecialidades12.setRefVistaMedico("José Matinez Apodaca");
        sdV2MedicoEspecialidades12.setTiempoTrabajadoHoras("173:00");
        sdV2MedicoEspecialidades12.setTiempoTrabajadoDias(8);
        sdV2MedicoEspecialidades12.setTotalConsultas(12);
        sdV2MedicoEspecialidades12.setNum1EraVez(5);
        sdV2MedicoEspecialidades12.setNumNoOtorgadas(6);
        sdV2MedicoEspecialidades12.setPromedioHoras(10.89);
        sdV2MedicoEspecialidades12.setNumVisitas(4);
        sdV2MedicoEspecialidades12.setNumCitas(8);
        sdV2MedicoEspecialidades12.setNumCitasCumplidas(2);
        sdV2MedicoEspecialidades12.setNumEnUnidad(1);
        sdV2MedicoEspecialidades12.setNumOtraUnidad(4);
        sdV2MedicoEspecialidades12.setNumAltasEspeciales(7);
        sdV2MedicoEspecialidades12.setNumAltasEspeciales(1);
        sdV2MedicoEspecialidades12.setNumExpedidas(0);
        sdV2MedicoEspecialidades12.setNumDias(3);
        sdV2MedicoEspecialidades12.setPromedio(10.05);
        sdV2MedicoEspecialidades12.setNumRecetas(9);
        sdV2MedicoEspecialidades12.setPorcentajeAtenciones(20.21);

        sdV2MedicoEspecialidadesLista1.add(sdV2MedicoEspecialidades11);
        sdV2MedicoEspecialidadesLista1.add(sdV2MedicoEspecialidades12);

        v2especialidades1.setSdV2Medico(sdV2MedicoEspecialidadesLista1);
        return sdV2MedicoEspecialidades12;
    }

    private ExtractoReporteParteI extractoReporteParteIExtr42(String desunidadMedica) {

        ExtractoReporteParteI extr42 = new ExtractoReporteParteI();

        AgrupacionId groupId42 = new AgrupacionId();
        List<String> matricula42 = new ArrayList<>();
        List<String> nom42 = new ArrayList<>();
        List<String> apP42 = new ArrayList<>();
        List<String> apM42 = new ArrayList<>();
        List<String> esp42 = new ArrayList<>();
        matricula42.add("9876875");
        nom42.add("Juan");
        apP42.add("Reyes");
        apM42.add("Perez");
        esp42.add("1600");

        groupId42.setMatricula(matricula42);
        groupId42.setNom(nom42);
        groupId42.setApP(apP42);
        groupId42.setApM(apM42);
        groupId42.setEsp(esp42);

        List<NumHorasTrabajadas> amTiempoTraHoras42 = new ArrayList<>();
        NumHorasTrabajadas numtra42 = new NumHorasTrabajadas();
        numtra42.setHoras("10:00");

        amTiempoTraHoras42.add(numtra42);

        extr42.setGroupId(groupId42);
        extr42.setAmTiempoTraHoras(amTiempoTraHoras42);
        extr42.setAmTiempoTraDias(8);
        extr42.setAmTotCons(10);
        extr42.setAm1Vez(3);
        extr42.setAmNoOtorgadas(5);
        extr42.setAmNumVisitas(2);
        extr42.setAmCitas(1);
        extr42.setAmCitasCumplidas(1);
        extr42.setPeEnUnidad(0);
        extr42.setPeOtraUnidad(1);
        extr42.setPeAltasEspeciales(2);
        extr42.setIncExpedidas(4);
        extr42.setIncDias(23);
        extr42.setIncProm(4.0);
        extr42.setRecNum(9);
        extr42.setRecPorcentajeAten(1.0);
        extr42.setDesEsp("4100 Otorrinolaringología");
        extr42.setDesUnidad(desunidadMedica);
        return extr42;
    }

    private ExtractoReporteParteI extractoReporteParteIExtr41(String desunidadMedica, AgrupacionId groupId41,
            List<NumHorasTrabajadas> amTiempoTraHoras41) {

        ExtractoReporteParteI extr41 = new ExtractoReporteParteI();
        extr41.setGroupId(groupId41);
        extr41.setAmTiempoTraHoras(amTiempoTraHoras41);
        extr41.setAmTiempoTraDias(8);
        extr41.setAmTotCons(10);
        extr41.setAm1Vez(3);
        extr41.setAmNoOtorgadas(5);
        extr41.setAmNumVisitas(2);
        extr41.setAmCitas(1);
        extr41.setAmCitasCumplidas(1);
        extr41.setPeEnUnidad(0);
        extr41.setPeOtraUnidad(1);
        extr41.setPeAltasEspeciales(2);
        extr41.setIncExpedidas(4);
        extr41.setIncDias(23);
        extr41.setIncProm(4.0);
        extr41.setRecNum(9);
        extr41.setRecPorcentajeAten(1.0);
        extr41.setDesEsp("4100 Otorrinolaringología");
        extr41.setDesUnidad(desunidadMedica);
        return extr41;
    }

    private ExtractoReporteParteI extractoReporteParteIExtr32(String desunidadMedica) {

        ExtractoReporteParteI extr32 = new ExtractoReporteParteI();

        AgrupacionId groupId32 = new AgrupacionId();
        List<String> matricula32 = new ArrayList<>();
        List<String> nom32 = new ArrayList<>();
        List<String> apP32 = new ArrayList<>();
        List<String> apM32 = new ArrayList<>();
        List<String> esp32 = new ArrayList<>();
        matricula32.add("9876875");
        nom32.add("Juan");
        apP32.add("Reyes");
        apM32.add("Perez");
        esp32.add("1600");

        groupId32.setMatricula(matricula32);
        groupId32.setNom(nom32);
        groupId32.setApP(apP32);
        groupId32.setApM(apM32);
        groupId32.setEsp(esp32);

        List<NumHorasTrabajadas> amTiempoTraHoras32 = new ArrayList<>();
        NumHorasTrabajadas numtra32 = new NumHorasTrabajadas();
        numtra32.setHoras("10:00");

        amTiempoTraHoras32.add(numtra32);

        extr32.setGroupId(groupId32);
        extr32.setAmTiempoTraHoras(amTiempoTraHoras32);
        extr32.setAmTiempoTraDias(8);
        extr32.setAmTotCons(10);
        extr32.setAm1Vez(3);
        extr32.setAmNoOtorgadas(5);
        extr32.setAmNumVisitas(2);
        extr32.setAmCitas(1);
        extr32.setAmCitasCumplidas(1);
        extr32.setPeEnUnidad(0);
        extr32.setPeOtraUnidad(1);
        extr32.setPeAltasEspeciales(2);
        extr32.setIncExpedidas(4);
        extr32.setIncDias(23);
        extr32.setIncProm(4.0);
        extr32.setRecNum(9);
        extr32.setRecPorcentajeAten(1.0);
        extr32.setDesEsp("3100 Otorrinolaringología");
        extr32.setDesUnidad(desunidadMedica);
        return extr32;
    }

    private ExtractoReporteParteI extractoReporteParteIExtr31(String desunidadMedica) {

        ExtractoReporteParteI extr31 = new ExtractoReporteParteI();

        AgrupacionId groupId31 = new AgrupacionId();
        List<String> matricula31 = new ArrayList<>();
        List<String> nom31 = new ArrayList<>();
        List<String> apP31 = new ArrayList<>();
        List<String> apM31 = new ArrayList<>();
        List<String> esp31 = new ArrayList<>();
        matricula31.add("9876875");
        nom31.add("Juan");
        apP31.add("Reyes");
        apM31.add("Perez");
        esp31.add("1600");

        groupId31.setMatricula(matricula31);
        groupId31.setNom(nom31);
        groupId31.setApP(apP31);
        groupId31.setApM(apM31);
        groupId31.setEsp(esp31);

        List<NumHorasTrabajadas> amTiempoTraHoras31 = new ArrayList<>();
        NumHorasTrabajadas numtra31 = new NumHorasTrabajadas();
        numtra31.setHoras("10:00");

        amTiempoTraHoras31.add(numtra31);

        extr31.setGroupId(groupId31);
        extr31.setAmTiempoTraHoras(amTiempoTraHoras31);
        extr31.setAmTiempoTraDias(8);
        extr31.setAmTotCons(10);
        extr31.setAm1Vez(3);
        extr31.setAmNoOtorgadas(5);
        extr31.setAmNumVisitas(2);
        extr31.setAmCitas(1);
        extr31.setAmCitasCumplidas(1);
        extr31.setPeEnUnidad(0);
        extr31.setPeOtraUnidad(1);
        extr31.setPeAltasEspeciales(2);
        extr31.setIncExpedidas(4);
        extr31.setIncDias(23);
        extr31.setIncProm(4.0);
        extr31.setRecNum(9);
        extr31.setRecPorcentajeAten(1.0);
        extr31.setDesEsp("3100 Otorrinolaringología");
        extr31.setDesUnidad(desunidadMedica);
        return extr31;
    }

    private ExtractoReporteParteI extractoReporteParteIExtr22(String desunidadMedica) {

        ExtractoReporteParteI extr22 = new ExtractoReporteParteI();

        AgrupacionId groupId22 = new AgrupacionId();
        List<String> matricula22 = new ArrayList<>();
        List<String> nom22 = new ArrayList<>();
        List<String> apP22 = new ArrayList<>();
        List<String> apM22 = new ArrayList<>();
        List<String> esp22 = new ArrayList<>();
        matricula22.add("9876875");
        nom22.add("Juan");
        apP22.add("Reyes");
        apM22.add("Perez");
        esp22.add("1600");

        groupId22.setMatricula(matricula22);
        groupId22.setNom(nom22);
        groupId22.setApP(apP22);
        groupId22.setApM(apM22);
        groupId22.setEsp(esp22);

        List<NumHorasTrabajadas> amTiempoTraHoras22 = new ArrayList<>();
        NumHorasTrabajadas numtra22 = new NumHorasTrabajadas();
        numtra22.setHoras("10:00");

        amTiempoTraHoras22.add(numtra22);

        extr22.setGroupId(groupId22);
        extr22.setAmTiempoTraHoras(amTiempoTraHoras22);
        extr22.setAmTiempoTraDias(8);
        extr22.setAmTotCons(10);
        extr22.setAm1Vez(3);
        extr22.setAmNoOtorgadas(5);
        extr22.setAmNumVisitas(2);
        extr22.setAmCitas(1);
        extr22.setAmCitasCumplidas(1);
        extr22.setPeEnUnidad(0);
        extr22.setPeOtraUnidad(1);
        extr22.setPeAltasEspeciales(2);
        extr22.setIncExpedidas(4);
        extr22.setIncDias(23);
        extr22.setIncProm(4.0);
        extr22.setRecNum(9);
        extr22.setRecPorcentajeAten(1.0);
        extr22.setDesEsp("3100 Otorrinolaringología");
        extr22.setDesUnidad(desunidadMedica);
        return extr22;
    }

    private ExtractoReporteParteI extractoReporteParteIExtr21(String desunidadMedica) {

        ExtractoReporteParteI extr21 = new ExtractoReporteParteI();

        AgrupacionId groupId21 = new AgrupacionId();
        List<String> matricula21 = new ArrayList<>();
        List<String> nom21 = new ArrayList<>();
        List<String> apP21 = new ArrayList<>();
        List<String> apM21 = new ArrayList<>();
        List<String> esp21 = new ArrayList<>();
        matricula21.add("9876875");
        nom21.add("Juan");
        apP21.add("Reyes");
        apM21.add("Perez");
        esp21.add("1600");

        groupId21.setMatricula(matricula21);
        groupId21.setNom(nom21);
        groupId21.setApP(apP21);
        groupId21.setApM(apM21);
        groupId21.setEsp(esp21);

        List<NumHorasTrabajadas> amTiempoTraHoras21 = new ArrayList<>();
        NumHorasTrabajadas numtra21 = new NumHorasTrabajadas();
        numtra21.setHoras("10:00");

        amTiempoTraHoras21.add(numtra21);

        extr21.setGroupId(groupId21);
        extr21.setAmTiempoTraHoras(amTiempoTraHoras21);
        extr21.setAmTiempoTraDias(8);
        extr21.setAmTotCons(10);
        extr21.setAm1Vez(3);
        extr21.setAmNoOtorgadas(5);
        extr21.setAmNumVisitas(2);
        extr21.setAmCitas(1);
        extr21.setAmCitasCumplidas(1);
        extr21.setPeEnUnidad(0);
        extr21.setPeOtraUnidad(1);
        extr21.setPeAltasEspeciales(2);
        extr21.setIncExpedidas(4);
        extr21.setIncDias(23);
        extr21.setIncProm(4.0);
        extr21.setRecNum(9);
        extr21.setRecPorcentajeAten(1.0);
        extr21.setDesEsp("3100 Otorrinolaringología");
        extr21.setDesUnidad(desunidadMedica);
        return extr21;
    }

    private ExtractoReporteParteI sdExtractoReporteParte1Extr11(String desunidadMedica) {

        ExtractoReporteParteI extr11 = new ExtractoReporteParteI();

        AgrupacionId groupId11 = new AgrupacionId();
        List<String> matricula11 = new ArrayList<>();
        List<String> nom11 = new ArrayList<>();
        List<String> apP11 = new ArrayList<>();
        List<String> apM11 = new ArrayList<>();
        List<String> esp11 = new ArrayList<>();
        matricula11.add("9876875");
        nom11.add("Juan");
        apP11.add("Reyes");
        apM11.add("Perez");
        esp11.add("1600");

        groupId11.setMatricula(matricula11);
        groupId11.setNom(nom11);
        groupId11.setApP(apP11);
        groupId11.setApM(apM11);
        groupId11.setEsp(esp11);

        List<NumHorasTrabajadas> amTiempoTraHoras11 = new ArrayList<>();
        NumHorasTrabajadas numtra11 = new NumHorasTrabajadas();
        numtra11.setHoras("10:00");

        amTiempoTraHoras11.add(numtra11);

        extr11.setGroupId(groupId11);
        extr11.setAmTiempoTraHoras(amTiempoTraHoras11);
        extr11.setAmTiempoTraDias(8);
        extr11.setAmTotCons(10);
        extr11.setAm1Vez(3);
        extr11.setAmNoOtorgadas(5);
        extr11.setAmNumVisitas(2);
        extr11.setAmCitas(1);
        extr11.setAmCitasCumplidas(1);
        extr11.setPeEnUnidad(0);
        extr11.setPeOtraUnidad(1);
        extr11.setPeAltasEspeciales(2);
        extr11.setIncExpedidas(4);
        extr11.setIncDias(23);
        extr11.setIncProm(4.0);
        extr11.setRecNum(9);
        extr11.setRecPorcentajeAten(1.0);
        extr11.setDesEsp("3100 Otorrinolaringología");
        extr11.setDesUnidad(desunidadMedica);
        return extr11;
    }

    private ExtractoReporteParteI sdExtractoReporteParte1Extr12(String desunidadMedica) {

        ExtractoReporteParteI extr12 = new ExtractoReporteParteI();

        AgrupacionId groupId12 = new AgrupacionId();
        List<String> matricula12 = new ArrayList<>();
        List<String> nom12 = new ArrayList<>();
        List<String> apP12 = new ArrayList<>();
        List<String> apM12 = new ArrayList<>();
        List<String> esp12 = new ArrayList<>();
        matricula12.add("9876875");
        nom12.add("Juan");
        apP12.add("Reyes");
        apM12.add("Perez");
        esp12.add("1600");

        groupId12.setMatricula(matricula12);
        groupId12.setNom(nom12);
        groupId12.setApP(apP12);
        groupId12.setApM(apM12);
        groupId12.setEsp(esp12);

        List<NumHorasTrabajadas> amTiempoTraHoras12 = new ArrayList<>();
        NumHorasTrabajadas numtra12 = new NumHorasTrabajadas();
        numtra12.setHoras("10:00");

        amTiempoTraHoras12.add(numtra12);

        extr12.setGroupId(groupId12);
        extr12.setAmTiempoTraHoras(amTiempoTraHoras12);
        extr12.setAmTiempoTraDias(8);
        extr12.setAmTotCons(10);
        extr12.setAm1Vez(3);
        extr12.setAmNoOtorgadas(5);
        extr12.setAmNumVisitas(2);
        extr12.setAmCitas(1);
        extr12.setAmCitasCumplidas(1);
        extr12.setPeEnUnidad(0);
        extr12.setPeOtraUnidad(1);
        extr12.setPeAltasEspeciales(2);
        extr12.setIncExpedidas(4);
        extr12.setIncDias(23);
        extr12.setIncProm(4.0);
        extr12.setRecNum(9);
        extr12.setRecPorcentajeAten(1.0);
        extr12.setDesEsp("3100 Otorrinolaringología");
        extr12.setDesUnidad(desunidadMedica);
        return extr12;
    }

    private SdTotalesReporte sdreaccidentes() {

        SdTotalesReporte sdRepAccidentesYles = new SdTotalesReporte();
        sdRepAccidentesYles.setCveEspecialidad("3100");
        sdRepAccidentesYles.setDesEspecialidad("Otorrinolaringología");
        sdRepAccidentesYles.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdRepAccidentesYles.setNumMedicos(10);
        sdRepAccidentesYles.setTiempoTrabajadoHoras("123:00");
        sdRepAccidentesYles.setTiempoHoras(123);
        sdRepAccidentesYles.setTiempoMinutos(0);
        sdRepAccidentesYles.setTiempoTrabajadoDias(8);
        sdRepAccidentesYles.setTotalConsultas(12);
        sdRepAccidentesYles.setNum1EraVez(5);
        sdRepAccidentesYles.setNumNoOtorgadas(6);
        sdRepAccidentesYles.setPromedioHoras(10.89);
        sdRepAccidentesYles.setNumVisitas(4);
        sdRepAccidentesYles.setNumCitas(8);
        sdRepAccidentesYles.setNumCitasCumplidas(2);
        sdRepAccidentesYles.setNumEnUnidad(1);
        sdRepAccidentesYles.setNumOtraUnidad(4);
        sdRepAccidentesYles.setNumAltasEspeciales(7);
        sdRepAccidentesYles.setNumAltasEspeciales(1);
        sdRepAccidentesYles.setNumExpedidas(0);
        sdRepAccidentesYles.setNumDias(3);
        sdRepAccidentesYles.setPromedio(10.05);
        sdRepAccidentesYles.setNumRecetas(9);
        sdRepAccidentesYles.setPorcentajeAtenciones(20.21);
        return sdRepAccidentesYles;
    }

    private SdTotalesReporte sdreposuma() {

        SdTotalesReporte sdRepSuma = new SdTotalesReporte();
        sdRepSuma.setCveEspecialidad("");
        sdRepSuma.setDesEspecialidad("");
        sdRepSuma.setRefVistaEspecialidad("");
        sdRepSuma.setNumMedicos(20);
        sdRepSuma.setTiempoTrabajadoHoras("263:00");
        sdRepSuma.setTiempoHoras(246);
        sdRepSuma.setTiempoMinutos(0);
        sdRepSuma.setTiempoTrabajadoDias(8);
        sdRepSuma.setTotalConsultas(24);
        sdRepSuma.setNum1EraVez(10);
        sdRepSuma.setNumNoOtorgadas(12);
        sdRepSuma.setPromedioHoras(21.45);
        sdRepSuma.setNumVisitas(8);
        sdRepSuma.setNumCitas(16);
        sdRepSuma.setNumCitasCumplidas(4);
        sdRepSuma.setNumEnUnidad(2);
        sdRepSuma.setNumOtraUnidad(8);
        sdRepSuma.setNumAltasEspeciales(14);
        sdRepSuma.setNumAltasEspeciales(2);
        sdRepSuma.setNumExpedidas(0);
        sdRepSuma.setNumDias(6);
        sdRepSuma.setPromedio(21.0);
        sdRepSuma.setNumRecetas(18);
        sdRepSuma.setPorcentajeAtenciones(40.44);
        return sdRepSuma;
    }

    @Test
    public void buscarReporteParte1Test() {

        SitReporteParteI sitReporteI = new SitReporteParteI();
        // ObjectId bjectId = new ObjectId();

        SdPeriodoReporte periodoReporte = new SdPeriodoReporte();
        SdUnidadMedica unidadMedica = new SdUnidadMedica();
        SdVistas vistas0 = new SdVistas();

        sitReporteI.setFecGeneracion(new Date());
        sitReporteI.setSdPeriodoReporte(periodoReporte);
        sitReporteI.setSdUnidadMedica(unidadMedica);
        sitReporteI.setSdVistas(vistas0);

        periodoReporte.setCvePeriodo(23);
        periodoReporte.setDesPeriodo("26 de marzo al 25 de abril");

        unidadMedica.setCvePresupuestal("12345678");
        unidadMedica.setDesUnidadMedica("UMF Guerrero");

        when(procesarReporteParteIRepository.buscarReporteParte1(anyString(), anyInt())).thenReturn(sitReporteI);
        SitReporteParteI sitReporteIResp = this.procesarReporteParteIRepository.buscarReporteParte1("3455666677", 34);
        Assert.assertNotNull(sitReporteI);
        logger.info("Periodo : " + sitReporteIResp.getSdPeriodoReporte());
        logger.info("Unidad Medica : " + sitReporteIResp.getSdUnidadMedica());

    }

    @Test
    public void getPeriodoReporteTest() {

        SdPeriodoReporte periodoReport = new SdPeriodoReporte();

        periodoReport.setCvePeriodo(23);
        periodoReport.setDesPeriodo("del 26 de agosto al 25 de septiembre");

        when(procesarReporteParteIService.getPeriodoReporte()).thenReturn(periodoReport);

        SdPeriodoReporte periodoReportResp = this.procesarReporteParteIService.getPeriodoReporte();
        Assert.assertEquals(periodoReport.getDesPeriodo(), periodoReportResp.getDesPeriodo());
        logger.info("Descripción periodo : " + periodoReportResp.getDesPeriodo());
        logger.info("Periodo : " + periodoReportResp.getCvePeriodo());
    }

    @Test
    public void getUnidadMedicaTest() {

        SdUnidadMedica unidadMedic = new SdUnidadMedica();

        unidadMedic.setCvePresupuestal("345678902");
        unidadMedic.setDesUnidadMedica("UHMF Cuahutemoc");

        when(procesarReporteParteIService.getUnidadMedica()).thenReturn(unidadMedic);
        // this.logger.info(" CVE Presupuestal antes :
        // "+procesarReporteParteIService.getUnidadMedica().getCvePresupuestal());
        SdUnidadMedica unidadMedicResp = this.procesarReporteParteIService.getUnidadMedica();
        Assert.assertEquals(unidadMedic.getCvePresupuestal(), unidadMedicResp.getCvePresupuestal());
        logger.info("  CVE Presupuestal : " + unidadMedicResp.getCvePresupuestal());
        logger.info("  Unidad Médica : " + unidadMedicResp.getDesUnidadMedica());
    }

    @Test
    public void borrarReporte1Test() {

        SitReporteParteI reporte1 = new SitReporteParteI();

        SdPeriodoReporte periodoReporte = new SdPeriodoReporte();
        SdUnidadMedica unidadMedica = new SdUnidadMedica();

        reporte1.setFecGeneracion(new Date());
        reporte1.setSdPeriodoReporte(periodoReporte);
        reporte1.setSdUnidadMedica(unidadMedica);

        periodoReporte.setCvePeriodo(34);
        periodoReporte.setDesPeriodo("26 de febrero al 25 de marzo");

        unidadMedica.setCvePresupuestal("35434735473");
        unidadMedica.setDesUnidadMedica("UMF Culiacán");

        Boolean valida = Boolean.TRUE;

        when(procesarReporteParteIRepository.borrarReporteParte1(reporte1)).thenReturn(valida);
        Boolean reporte1Resp = this.procesarReporteParteIRepository.borrarReporteParte1(reporte1);

        Assert.assertTrue(reporte1Resp);

        logger.info("Valida: {}", reporte1Resp);

    }

}
