/**
 * 
 */
package mx.gob.imss.simo.reporteparte1.common.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Date;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.reporteparte1.common.dto.PeriodoDTO;
import mx.gob.imss.simo.reporteparte1.common.enums.EnumEstatusReporteVista;
import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdPeriodoReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdTotalesReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;
import mx.gob.imss.simo.reporteparte1.common.repository.ReporteParte1CommonRepository;
import mx.gob.imss.simo.reporteparte1.reporte.helper.DateUtils;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ConsultarReportesParteIRepository;
import mx.gob.imss.simo.reporteparte1.reporte.services.impl.ConsultarReportesParteIServiceImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConsultarReportesParteIServiceImpl.class)
public class ConsultarReportesParteIServiceImplTest {

    final static Logger logger = LoggerFactory.getLogger(ConsultarReportesParteIServiceImpl.class);

    @InjectMocks
    ConsultarReportesParteIServiceImpl consultarReportesParteIServiceImpl;

    @Mock
    ConsultarReportesParteIRepository consultarReportesParteIRepository;

    @Mock
    ReporteParte1CommonRepository reporteParte1CommonRepository;

    @BeforeClass
    public static void init() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        consultarReportesParteIServiceImpl = PowerMockito.spy(new ConsultarReportesParteIServiceImpl());
        MockitoAnnotations.initMocks(this);
    }

    /*
     * El test generarPeriodosTest contiene consume los métodos protegidos: obtenerMesesPrevios() y generaPeriodoDTO().
     */
    @Test
    public void generarPeriodosTest() {

        PeriodoDTO periodoDTO = new PeriodoDTO();
        periodoDTO.setPeriodo("201710");
        periodoDTO.setEstatus(EnumEstatusReporteVista.REPORTE_VIGENTE.getDescripcion());
        periodoDTO.setFechaGeneracionReporte(DateUtils.fechaAString(DateUtils.crearFecha()));
        periodoDTO.setRutaImagenEstatus("/img/estatus_reporte.png");

        SdUnidadMedica unidad = new SdUnidadMedica();
        unidad.setCvePresupuestal("141609UA2151");
        unidad.setDesUnidadMedica("UMAA 1 Lagos Moreno");

        SdVistas sdVisitas = new SdVistas();

        SdPeriodoReporte sdPeriodoReporte = new SdPeriodoReporte();
        sdPeriodoReporte.setCvePeriodo(201801);

        SitColaReportes cola = new SitColaReportes();
        cola.setCvePeriodo(201710);
        cola.setDesUnidad("141609UA2151");
        cola.setFecInicio(new Date());

        SitReporteParteI sitReporteParteI = new SitReporteParteI();
        sitReporteParteI.setFecGeneracion(new Date());
        sitReporteParteI.setSdVistas(sdVisitas);
        sitReporteParteI.setSdPeriodoReporte(sdPeriodoReporte);
        sitReporteParteI.setSdUnidadMedica(unidad);

        try {
            when(consultarReportesParteIRepository.buscarSitReportePorPeriodoUM(any(String.class), any(String.class)))
                    .thenReturn(sitReporteParteI);
            when(consultarReportesParteIRepository.buscarSitColaReportesPorPeriodoUM(any(String.class), any(int.class)))
                    .thenReturn(cola);
            logger.info("Test generarPeriodosTest");
            logger.info("Clave Periodo: {}", periodoDTO.getPeriodo());
            logger.info("Estatus: {}", periodoDTO.getEstatus());
            logger.info("Fecha Generación: {}", periodoDTO.getFechaGeneracionReporte());
            logger.info("URL Imágen: {}", periodoDTO.getRutaImagenEstatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void descargarReporteTest() throws Exception {

        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        File tempFile = Files.createTempFile(null, null).toFile();
        arrayOutputStream.writeTo(new FileOutputStream(tempFile));
        InputStream inputStream = new FileInputStream(tempFile);
        StreamedContent streamedContent = new DefaultStreamedContent(inputStream,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                "reporte_parte_1_" + "141609UA2151" + ".xlsx");
        try {
            consultarReportesParteIServiceImpl.buscarPeriodoImssPorFecha(new Date());
            logger.info("Test descargarReporteTest");
            logger.info("Nombre del reporte generado: {}", streamedContent.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void buscarPeriodoImssPorFechaTest() {

        SicPeriodosImss sicPeriodo = new SicPeriodosImss();
        sicPeriodo.setCvePeriodo(201710);
        sicPeriodo.setFecBaja(new Date());
        sicPeriodo.setFecFinal(new Date());
        sicPeriodo.setFecInicial(new Date());

        try {
            when(reporteParte1CommonRepository.obtenerPeriodImssPorFecha(any(Date.class))).thenReturn(sicPeriodo);
            consultarReportesParteIServiceImpl.buscarPeriodoImssPorFecha(new Date());
            logger.info("Test consultarReportesPorPeriodoUnidadMedica");
            logger.info("Clave Periodo: {}", sicPeriodo.getCvePeriodo());
            logger.info("Fecha Baja: {}", sicPeriodo.getFecBaja());
            logger.info("Fecha Final: {}", sicPeriodo.getFecFinal());
            logger.info("Fecha Inicial: {}", sicPeriodo.getFecInicial());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void consultarReportesPorPeriodoUnidadMedicaTest() {

        PeriodoDTO periodoDTO = new PeriodoDTO();
        periodoDTO.setPeriodo("201710");
        periodoDTO.setEstatus(EnumEstatusReporteVista.REPORTE_VIGENTE.getDescripcion());
        periodoDTO.setFechaGeneracionReporte(DateUtils.fechaAString(DateUtils.crearFecha()));
        periodoDTO.setRutaImagenEstatus("/img/estatus_reporte.png");

        SdUnidadMedica unidad = new SdUnidadMedica();
        unidad.setCvePresupuestal("141609UA2151");
        unidad.setDesUnidadMedica("UMAA 1 Lagos Moreno");

        SdVistas sdVisitas = new SdVistas();

        SdPeriodoReporte sdPeriodoReporte = new SdPeriodoReporte();
        sdPeriodoReporte.setCvePeriodo(201801);

        SitColaReportes cola = new SitColaReportes();
        cola.setCvePeriodo(201710);
        cola.setDesUnidad("141609UA2151");
        cola.setFecInicio(new Date());
        
        SdVista1 vista1 = new SdVista1();

        SdTotalesReporte sdRepEspecialidades = new SdTotalesReporte();
        sdRepEspecialidades.setCveEspecialidad("3100");
        sdRepEspecialidades.setDesEspecialidad("Otorrinolaringología");
        sdRepEspecialidades.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdRepEspecialidades.setNumMedicos(10);
        sdRepEspecialidades.setTiempoTrabajadoHoras("123:00");
        sdRepEspecialidades.setTiempoHoras(123);
        sdRepEspecialidades.setTiempoMinutos(0);
        sdRepEspecialidades.setTiempoTrabajadoDias(8);
        sdRepEspecialidades.setTotalConsultas(12);
        sdRepEspecialidades.setNum1EraVez(5);
        sdRepEspecialidades.setNumNoOtorgadas(6);
        sdRepEspecialidades.setPromedioHoras(10.89);
        sdRepEspecialidades.setNumVisitas(4);
        sdRepEspecialidades.setNumCitas(8);
        sdRepEspecialidades.setNumCitasCumplidas(2);
        sdRepEspecialidades.setNumEnUnidad(1);
        sdRepEspecialidades.setNumOtraUnidad(4);
        sdRepEspecialidades.setNumAltasEspeciales(7);
        sdRepEspecialidades.setNumAltasEspeciales(1);
        sdRepEspecialidades.setNumExpedidas(0);
        sdRepEspecialidades.setNumDias(3);
        sdRepEspecialidades.setPromedio(10.05);
        sdRepEspecialidades.setNumRecetas(9);
        sdRepEspecialidades.setPorcentajeAtenciones(20.21);

        vista1.setSdRepEspecialidades(sdRepEspecialidades);
        
        SdTotalesReporte sdTotalUnidad = new SdTotalesReporte();
        sdTotalUnidad.setCveEspecialidad("3100");
        sdTotalUnidad.setDesEspecialidad("Otorrinolaringología");
        sdTotalUnidad.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdTotalUnidad.setNumMedicos(10);
        sdTotalUnidad.setTiempoTrabajadoHoras("123:00");
        sdTotalUnidad.setTiempoHoras(123);
        sdTotalUnidad.setTiempoMinutos(0);
        sdTotalUnidad.setTiempoTrabajadoDias(8);
        sdTotalUnidad.setTotalConsultas(12);
        sdTotalUnidad.setNum1EraVez(5);
        sdTotalUnidad.setNumNoOtorgadas(6);
        sdTotalUnidad.setPromedioHoras(10.89);
        sdTotalUnidad.setNumVisitas(4);
        sdTotalUnidad.setNumCitas(8);
        sdTotalUnidad.setNumCitasCumplidas(2);
        sdTotalUnidad.setNumEnUnidad(1);
        sdTotalUnidad.setNumOtraUnidad(4);
        sdTotalUnidad.setNumAltasEspeciales(7);
        sdTotalUnidad.setNumAltasEspeciales(1);
        sdTotalUnidad.setNumExpedidas(0);
        sdTotalUnidad.setNumDias(3);
        sdTotalUnidad.setPromedio(10.05);
        sdTotalUnidad.setNumRecetas(9);
        sdTotalUnidad.setPorcentajeAtenciones(20.21);

        vista1.setSdTotalUnidad(sdTotalUnidad);        
        
        sdVisitas.setSdVista1(vista1);

        SitReporteParteI sitReporteParteI = new SitReporteParteI();
        sitReporteParteI.setFecGeneracion(new Date());
        sitReporteParteI.setSdVistas(sdVisitas);
        sitReporteParteI.setSdPeriodoReporte(sdPeriodoReporte);
        sitReporteParteI.setSdUnidadMedica(unidad);

        try {
            when(consultarReportesParteIRepository.buscarSitReportePorPeriodoUM(any(String.class), any(String.class)))
                    .thenReturn(sitReporteParteI);
            when(consultarReportesParteIRepository.buscarSitColaReportesPorPeriodoUM(any(String.class), any(int.class)))
                    .thenReturn(cola);
            PeriodoDTO retorno = consultarReportesParteIServiceImpl.consultarReportesPorPeriodoUnidadMedica(periodoDTO,
                    unidad.getCvePresupuestal());

            logger.info("Test consultarReportesPorPeriodoUnidadMedica");
            logger.info("Clave Presupuestal: {}", sitReporteParteI.getSdUnidadMedica().getCvePresupuestal());
            logger.info("Descripción Unidad Médica: {}", sitReporteParteI.getSdUnidadMedica().getDesUnidadMedica());
            logger.info("Clave Periodo: {}", sitReporteParteI.getSdPeriodoReporte().getCvePeriodo());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
