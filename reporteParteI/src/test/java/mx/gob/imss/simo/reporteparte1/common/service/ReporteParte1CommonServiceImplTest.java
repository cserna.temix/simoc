package mx.gob.imss.simo.reporteparte1.common.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.reporteparte1.common.model.SitControlCapturaCE;
import mx.gob.imss.simo.reporteparte1.common.model.SitUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.repository.ReporteParte1CommonRepository;
import mx.gob.imss.simo.reporteparte1.common.services.impl.ReporteParte1CommonServiceImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ReporteParte1CommonServiceImpl.class)
public class ReporteParte1CommonServiceImplTest {

    final static Logger logger = LoggerFactory.getLogger(ReporteParte1CommonServiceImplTest.class);

    @InjectMocks
    ReporteParte1CommonServiceImpl reporteParte1CommonServiceImpl;

    @Mock
    ReporteParte1CommonRepository reporteParte1CommonRepository;

    SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYYY");

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        reporteParte1CommonServiceImpl = PowerMockito.spy(new ReporteParte1CommonServiceImpl());
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void actualizarControlCapturaCeTest() throws ParseException {

        SdUnidadMedica unidad = new SdUnidadMedica();
        unidad.setCvePresupuestal("141609UA2151");
        unidad.setDesUnidadMedica("UMAA 1 Lagos Moreno");

        SitControlCapturaCE sitControlCapturaCE = new SitControlCapturaCE();
        sitControlCapturaCE.setCvePeriodo(201802);
        sitControlCapturaCE.setFecActualizaControl(sdf.parse("02/03/2018"));
        sitControlCapturaCE.setIndControl(0);
        sitControlCapturaCE.setSdUnidadMedica(unidad);

        try {

            when(reporteParte1CommonRepository.actualizarControlCapturaCE(any(SitControlCapturaCE.class)))
                    .thenReturn(sitControlCapturaCE);
            reporteParte1CommonServiceImpl.actualizarControlCapturaCe(true, "141609UA2151", "UMAA 1 Lagos Moreno",
                    201802);

            logger.info("Test actualizarControlCapturaCe");
            logger.info("Clave Presupuestal: {}", sitControlCapturaCE.getSdUnidadMedica().getCvePresupuestal());
            logger.info("Unidad M�dica: {}", sitControlCapturaCE.getSdUnidadMedica().getDesUnidadMedica());
            logger.info("Clave Periodo: {}", sitControlCapturaCE.getCvePeriodo());
            logger.info("Indice Control: {}", sitControlCapturaCE.getIndControl());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void buscarUnidadMedicaTest() {

        SitUnidadMedica sitUnidadMedica = new SitUnidadMedica();
        sitUnidadMedica.setCveDelegacion("14");
        sitUnidadMedica.setCvePresupuestal("141609UA2151");
        sitUnidadMedica.setCveUnidadPresupuestal("2151");
        sitUnidadMedica.setDesDelegacion("Jalisco");
        sitUnidadMedica.setIndTitular(1);

        try {

            when(reporteParte1CommonRepository.buscarUnidadMedica(anyString())).thenReturn(sitUnidadMedica);
            SitUnidadMedica resultTest = reporteParte1CommonServiceImpl.buscarUnidadMedica("141609UA2151");
            Assert.assertNotNull(resultTest);
            Assert.assertEquals(resultTest, sitUnidadMedica);

            logger.info("Test buscarUnidadMedica");
            logger.info("Clave Presupuestal: {}", resultTest.getCvePresupuestal());
            logger.info("Unidad M�dica: {}", resultTest.getDesUnidadMedica());
            logger.info("Delegaci�n {}", resultTest.getDesDelegacion());
            logger.info("Clave Unidad Presupuestal {}", resultTest.getCveUnidadPresupuestal());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
