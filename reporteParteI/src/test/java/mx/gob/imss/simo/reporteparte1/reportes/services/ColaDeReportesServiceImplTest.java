package mx.gob.imss.simo.reporteparte1.reportes.services;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.reporteparte1.common.model.SitColaReportes;
import mx.gob.imss.simo.reporteparte1.common.model.SitControlCapturaCE;
import mx.gob.imss.simo.reporteparte1.common.model.SitReporteParteI;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdPeriodoReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdRenglonesEspecialesVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdTotalesReporte;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdUnidadMedica;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdV2Especialidad;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdV2Medico;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista1;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVista2;
import mx.gob.imss.simo.reporteparte1.common.model.pojo.SdVistas;
import mx.gob.imss.simo.reporteparte1.common.repository.ReporteParte1CommonRepository;
import mx.gob.imss.simo.reporteparte1.common.services.ReporteParte1CommonService;
import mx.gob.imss.simo.reporteparte1.reporte.repository.ColaDeReportesRepository;
import mx.gob.imss.simo.reporteparte1.reporte.services.ProcesarReporteParteIService;
import mx.gob.imss.simo.reporteparte1.reporte.services.impl.ColaDeReportesServiceImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ColaDeReportesServiceImpl.class)
public class ColaDeReportesServiceImplTest {

    final static Logger logger = LoggerFactory.getLogger(ColaDeReportesServiceImplTest.class);

    @InjectMocks
    ColaDeReportesServiceImpl colaDeReportesService;

    @Mock
    ProcesarReporteParteIService procesarReporteParteIService;

    @Mock
    ReporteParte1CommonService reporteParte1CommonService;

    @Mock
    ColaDeReportesRepository colaDeReportesRepository;

    @Mock
    ReporteParte1CommonRepository reporteParte1CommonRepository;

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        colaDeReportesService = PowerMockito.spy(new ColaDeReportesServiceImpl());

        MockitoAnnotations.initMocks(this);
    }

    SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYYY");

    @Test
    public void buscarReportesEnColaTestUno() throws ParseException {

        // Reportes Pendientes
        SitColaReportes sitColaReportes = new SitColaReportes();
        sitColaReportes.setCvePeriodo(201802);
        sitColaReportes.setCvePresupuestal("030103022151");
        sitColaReportes.setDesUnidad("HGZMF 1 La Paz");
        sitColaReportes.setEstatus(1);
        sitColaReportes.setFecFin(new Date());
        sitColaReportes.setFecInicio(new Date());

        List<SitColaReportes> sitColaReportesList = new ArrayList<SitColaReportes>();
        sitColaReportesList.add(sitColaReportes);
        Iterator<SitColaReportes> reportes = sitColaReportesList.iterator();

        // Vistas
        SdVistas vistas = new SdVistas();

        // Vista 1
        SdVista1 vista1 = new SdVista1();

        SdTotalesReporte sdRepEspecialidades = new SdTotalesReporte();
        sdRepEspecialidades.setCveEspecialidad("3100");
        sdRepEspecialidades.setDesEspecialidad("Otorrinolaringología");
        sdRepEspecialidades.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdRepEspecialidades.setNumMedicos(10);
        sdRepEspecialidades.setTiempoTrabajadoHoras("123:00");
        sdRepEspecialidades.setTiempoHoras(123);
        sdRepEspecialidades.setTiempoMinutos(0);
        sdRepEspecialidades.setTiempoTrabajadoDias(8);
        sdRepEspecialidades.setTotalConsultas(12);
        sdRepEspecialidades.setNum1EraVez(5);
        sdRepEspecialidades.setNumNoOtorgadas(6);
        sdRepEspecialidades.setPromedioHoras(10.89);
        sdRepEspecialidades.setNumVisitas(4);
        sdRepEspecialidades.setNumCitas(8);
        sdRepEspecialidades.setNumCitasCumplidas(2);
        sdRepEspecialidades.setNumEnUnidad(1);
        sdRepEspecialidades.setNumOtraUnidad(4);
        sdRepEspecialidades.setNumAltasEspeciales(7);
        sdRepEspecialidades.setNumAltasEspeciales(1);
        sdRepEspecialidades.setNumExpedidas(0);
        sdRepEspecialidades.setNumDias(3);
        sdRepEspecialidades.setPromedio(10.05);
        sdRepEspecialidades.setNumRecetas(9);
        sdRepEspecialidades.setPorcentajeAtenciones(20.21);

        vista1.setSdRepEspecialidades(sdRepEspecialidades);

        SdTotalesReporte sdUrgencias = new SdTotalesReporte();
        sdUrgencias.setCveEspecialidad("3100");
        sdUrgencias.setDesEspecialidad("Otorrinolaringología");
        sdUrgencias.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdUrgencias.setNumMedicos(10);
        sdUrgencias.setTiempoTrabajadoHoras("123:00");
        sdUrgencias.setTiempoHoras(123);
        sdUrgencias.setTiempoMinutos(0);
        sdUrgencias.setTiempoTrabajadoDias(8);
        sdUrgencias.setTotalConsultas(12);
        sdUrgencias.setNum1EraVez(5);
        sdUrgencias.setNumNoOtorgadas(6);
        sdUrgencias.setPromedioHoras(10.89);
        sdUrgencias.setNumVisitas(4);
        sdUrgencias.setNumCitas(8);
        sdUrgencias.setNumCitasCumplidas(2);
        sdUrgencias.setNumEnUnidad(1);
        sdUrgencias.setNumOtraUnidad(4);
        sdUrgencias.setNumAltasEspeciales(7);
        sdUrgencias.setNumAltasEspeciales(1);
        sdUrgencias.setNumExpedidas(0);
        sdUrgencias.setNumDias(3);
        sdUrgencias.setPromedio(10.05);
        sdUrgencias.setNumRecetas(9);
        sdUrgencias.setPorcentajeAtenciones(20.21);

        vista1.setSdUrgencias(sdUrgencias);

        SdTotalesReporte sdTotalUnidad = new SdTotalesReporte();
        sdTotalUnidad.setCveEspecialidad("3100");
        sdTotalUnidad.setDesEspecialidad("Otorrinolaringología");
        sdTotalUnidad.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdTotalUnidad.setNumMedicos(10);
        sdTotalUnidad.setTiempoTrabajadoHoras("123:00");
        sdTotalUnidad.setTiempoHoras(123);
        sdTotalUnidad.setTiempoMinutos(0);
        sdTotalUnidad.setTiempoTrabajadoDias(8);
        sdTotalUnidad.setTotalConsultas(12);
        sdTotalUnidad.setNum1EraVez(5);
        sdTotalUnidad.setNumNoOtorgadas(6);
        sdTotalUnidad.setPromedioHoras(10.89);
        sdTotalUnidad.setNumVisitas(4);
        sdTotalUnidad.setNumCitas(8);
        sdTotalUnidad.setNumCitasCumplidas(2);
        sdTotalUnidad.setNumEnUnidad(1);
        sdTotalUnidad.setNumOtraUnidad(4);
        sdTotalUnidad.setNumAltasEspeciales(7);
        sdTotalUnidad.setNumAltasEspeciales(1);
        sdTotalUnidad.setNumExpedidas(0);
        sdTotalUnidad.setNumDias(3);
        sdTotalUnidad.setPromedio(10.05);
        sdTotalUnidad.setNumRecetas(9);
        sdTotalUnidad.setPorcentajeAtenciones(20.21);

        vista1.setSdTotalUnidad(sdTotalUnidad);

        // renglones especiales
        SdRenglonesEspecialesVista1 sdRenglonesEspeciales = new SdRenglonesEspecialesVista1();

        SdTotalesReporte sdViolencia = new SdTotalesReporte();
        sdViolencia.setCveEspecialidad("3100");
        sdViolencia.setDesEspecialidad("Otorrinolaringología");
        sdViolencia.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdViolencia.setNumMedicos(10);
        sdViolencia.setTiempoTrabajadoHoras("123:00");
        sdViolencia.setTiempoHoras(123);
        sdViolencia.setTiempoMinutos(0);
        sdViolencia.setTiempoTrabajadoDias(8);
        sdViolencia.setTotalConsultas(12);
        sdViolencia.setNum1EraVez(5);
        sdViolencia.setNumNoOtorgadas(6);
        sdViolencia.setPromedioHoras(10.89);
        sdViolencia.setNumVisitas(4);
        sdViolencia.setNumCitas(8);
        sdViolencia.setNumCitasCumplidas(2);
        sdViolencia.setNumEnUnidad(1);
        sdViolencia.setNumOtraUnidad(4);
        sdViolencia.setNumAltasEspeciales(7);
        sdViolencia.setNumAltasEspeciales(1);
        sdViolencia.setNumExpedidas(0);
        sdViolencia.setNumDias(3);
        sdViolencia.setPromedio(10.05);
        sdViolencia.setNumRecetas(9);
        sdViolencia.setPorcentajeAtenciones(20.21);

        sdRenglonesEspeciales.setSdViolencia(sdViolencia);

        List<SdTotalesReporte> sdParamedicos = new ArrayList<>();
        SdTotalesReporte paramedico1 = new SdTotalesReporte();
        paramedico1.setCveEspecialidad("3100");
        paramedico1.setDesEspecialidad("Otorrinolaringología");
        paramedico1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        paramedico1.setNumMedicos(10);
        paramedico1.setTiempoTrabajadoHoras("123:00");
        paramedico1.setTiempoHoras(123);
        paramedico1.setTiempoMinutos(0);
        paramedico1.setTiempoTrabajadoDias(8);
        paramedico1.setTotalConsultas(12);
        paramedico1.setNum1EraVez(5);
        paramedico1.setNumNoOtorgadas(6);
        paramedico1.setPromedioHoras(10.89);
        paramedico1.setNumVisitas(4);
        paramedico1.setNumCitas(8);
        paramedico1.setNumCitasCumplidas(2);
        paramedico1.setNumEnUnidad(1);
        paramedico1.setNumOtraUnidad(4);
        paramedico1.setNumAltasEspeciales(7);
        paramedico1.setNumAltasEspeciales(1);
        paramedico1.setNumExpedidas(0);
        paramedico1.setNumDias(3);
        paramedico1.setPromedio(10.05);
        paramedico1.setNumRecetas(9);
        paramedico1.setPorcentajeAtenciones(20.21);

        SdTotalesReporte paramedico2 = new SdTotalesReporte();
        paramedico2.setCveEspecialidad("3100");
        paramedico2.setDesEspecialidad("Otorrinolaringología");
        paramedico2.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        paramedico2.setNumMedicos(10);
        paramedico2.setTiempoTrabajadoHoras("123:00");
        paramedico2.setTiempoHoras(123);
        paramedico2.setTiempoMinutos(0);
        paramedico2.setTiempoTrabajadoDias(8);
        paramedico2.setTotalConsultas(12);
        paramedico2.setNum1EraVez(5);
        paramedico2.setNumNoOtorgadas(6);
        paramedico2.setPromedioHoras(10.89);
        paramedico2.setNumVisitas(4);
        paramedico2.setNumCitas(8);
        paramedico2.setNumCitasCumplidas(2);
        paramedico2.setNumEnUnidad(1);
        paramedico2.setNumOtraUnidad(4);
        paramedico2.setNumAltasEspeciales(7);
        paramedico2.setNumAltasEspeciales(1);
        paramedico2.setNumExpedidas(0);
        paramedico2.setNumDias(3);
        paramedico2.setPromedio(10.05);
        paramedico2.setNumRecetas(9);
        paramedico2.setPorcentajeAtenciones(20.21);

        sdParamedicos.add(paramedico1);
        sdParamedicos.add(paramedico2);

        sdRenglonesEspeciales.setSdParamedicos(sdParamedicos);

        SdTotalesReporte sdTococirugia = new SdTotalesReporte();
        sdTococirugia.setCveEspecialidad("3100");
        sdTococirugia.setDesEspecialidad("Otorrinolaringología");
        sdTococirugia.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");
        sdTococirugia.setNumMedicos(10);
        sdTococirugia.setTiempoTrabajadoHoras("123:00");
        sdTococirugia.setTiempoHoras(123);
        sdTococirugia.setTiempoMinutos(0);
        sdTococirugia.setTiempoTrabajadoDias(8);
        sdTococirugia.setTotalConsultas(12);
        sdTococirugia.setNum1EraVez(5);
        sdTococirugia.setNumNoOtorgadas(6);
        sdTococirugia.setPromedioHoras(10.89);
        sdTococirugia.setNumVisitas(4);
        sdTococirugia.setNumCitas(8);
        sdTococirugia.setNumCitasCumplidas(2);
        sdTococirugia.setNumEnUnidad(1);
        sdTococirugia.setNumOtraUnidad(4);
        sdTococirugia.setNumAltasEspeciales(7);
        sdTococirugia.setNumAltasEspeciales(1);
        sdTococirugia.setNumExpedidas(0);
        sdTococirugia.setNumDias(3);
        sdTococirugia.setPromedio(10.05);
        sdTococirugia.setNumRecetas(9);
        sdTococirugia.setPorcentajeAtenciones(20.21);

        sdRenglonesEspeciales.setSdTococirugia(sdTococirugia);

        vista1.setSdRenglonesEspeciales(sdRenglonesEspeciales);

        vistas.setSdVista1(vista1);

        // VISTA 2

        SdVista2 vista2 = new SdVista2();

        List<SdV2Especialidad> sdV2Especialidad = new ArrayList<>();

        SdV2Especialidad v2especialidad1 = new SdV2Especialidad();
        v2especialidad1.setCveEspecialidad("3100");
        v2especialidad1.setDesEspecialidad("Otorrinolaringología");
        v2especialidad1.setRefVistaEspecialidad("** 3100 Otorrinolaringología **");

        List<SdV2Medico> sdV2MedicoLista1 = new ArrayList<>();
        SdV2Medico sdV2Medico1 = new SdV2Medico();
        sdV2Medico1.setCveMatricula("9282718");
        sdV2Medico1.setRefApellidoPaterno("Perez");
        sdV2Medico1.setRefApellidoMaterno("Jimenez");
        sdV2Medico1.setRefNombre("Antonio");
        sdV2Medico1.setRefVistaMedico("Antonio Perez Jimenez");
        sdV2Medico1.setTiempoTrabajadoHoras("123:00");
        sdV2Medico1.setTiempoTrabajadoDias(8);
        sdV2Medico1.setTotalConsultas(12);
        sdV2Medico1.setNum1EraVez(5);
        sdV2Medico1.setNumNoOtorgadas(6);
        sdV2Medico1.setPromedioHoras(10.89);
        sdV2Medico1.setNumVisitas(4);
        sdV2Medico1.setNumCitas(8);
        sdV2Medico1.setNumCitasCumplidas(2);
        sdV2Medico1.setNumEnUnidad(1);
        sdV2Medico1.setNumOtraUnidad(4);
        sdV2Medico1.setNumAltasEspeciales(7);
        sdV2Medico1.setNumAltasEspeciales(1);
        sdV2Medico1.setNumExpedidas(0);
        sdV2Medico1.setNumDias(3);
        sdV2Medico1.setPromedio(10.05);
        sdV2Medico1.setNumRecetas(9);
        sdV2Medico1.setPorcentajeAtenciones(20.21);

        SdV2Medico sdV2Medico2 = new SdV2Medico();
        sdV2Medico2.setCveMatricula("0982718");
        sdV2Medico2.setRefApellidoPaterno("Matinez");
        sdV2Medico2.setRefApellidoMaterno("Apodaca");
        sdV2Medico2.setRefNombre("José");
        sdV2Medico2.setRefVistaMedico("José Matinez Apodaca");
        sdV2Medico2.setTiempoTrabajadoHoras("173:00");
        sdV2Medico2.setTiempoTrabajadoDias(8);
        sdV2Medico2.setTotalConsultas(12);
        sdV2Medico2.setNum1EraVez(5);
        sdV2Medico2.setNumNoOtorgadas(6);
        sdV2Medico2.setPromedioHoras(10.89);
        sdV2Medico2.setNumVisitas(4);
        sdV2Medico2.setNumCitas(8);
        sdV2Medico2.setNumCitasCumplidas(2);
        sdV2Medico2.setNumEnUnidad(1);
        sdV2Medico2.setNumOtraUnidad(4);
        sdV2Medico2.setNumAltasEspeciales(7);
        sdV2Medico2.setNumAltasEspeciales(1);
        sdV2Medico2.setNumExpedidas(0);
        sdV2Medico2.setNumDias(3);
        sdV2Medico2.setPromedio(10.05);
        sdV2Medico2.setNumRecetas(9);
        sdV2Medico2.setPorcentajeAtenciones(20.21);

        sdV2MedicoLista1.add(sdV2Medico1);
        sdV2MedicoLista1.add(sdV2Medico2);

        v2especialidad1.setSdV2Medico(sdV2MedicoLista1);

        SdV2Especialidad v2especialidad2 = new SdV2Especialidad();
        v2especialidad2.setCveEspecialidad("3200");
        v2especialidad2.setDesEspecialidad("Otorrinolaringología");
        v2especialidad2.setRefVistaEspecialidad("** 3200 Otorrinolaringología **");

        List<SdV2Medico> sdV2MedicoLista2 = new ArrayList<>();
        SdV2Medico sdV2Medico21 = new SdV2Medico();
        sdV2Medico21.setCveMatricula("9282728");
        sdV2Medico21.setRefApellidoPaterno("Perez");
        sdV2Medico21.setRefApellidoMaterno("Jimenez");
        sdV2Medico21.setRefNombre("Antonio");
        sdV2Medico21.setRefVistaMedico("Antonio Perez Jimenez");
        sdV2Medico21.setTiempoTrabajadoHoras("223:00");
        sdV2Medico21.setTiempoTrabajadoDias(8);
        sdV2Medico21.setTotalConsultas(22);
        sdV2Medico21.setNum1EraVez(5);
        sdV2Medico21.setNumNoOtorgadas(6);
        sdV2Medico21.setPromedioHoras(20.89);
        sdV2Medico21.setNumVisitas(4);
        sdV2Medico21.setNumCitas(8);
        sdV2Medico21.setNumCitasCumplidas(2);
        sdV2Medico21.setNumEnUnidad(2);
        sdV2Medico21.setNumOtraUnidad(4);
        sdV2Medico21.setNumAltasEspeciales(7);
        sdV2Medico21.setNumAltasEspeciales(2);
        sdV2Medico21.setNumExpedidas(0);
        sdV2Medico21.setNumDias(3);
        sdV2Medico21.setPromedio(20.05);
        sdV2Medico21.setNumRecetas(9);
        sdV2Medico21.setPorcentajeAtenciones(20.22);

        SdV2Medico sdV2Medico22 = new SdV2Medico();
        sdV2Medico22.setCveMatricula("0982728");
        sdV2Medico22.setRefApellidoPaterno("Matinez");
        sdV2Medico22.setRefApellidoMaterno("Apodaca");
        sdV2Medico22.setRefNombre("José");
        sdV2Medico22.setRefVistaMedico("José Matinez Apodaca");
        sdV2Medico22.setTiempoTrabajadoHoras("273:00");
        sdV2Medico22.setTiempoTrabajadoDias(8);
        sdV2Medico22.setTotalConsultas(22);
        sdV2Medico22.setNum1EraVez(5);
        sdV2Medico22.setNumNoOtorgadas(6);
        sdV2Medico22.setPromedioHoras(20.89);
        sdV2Medico22.setNumVisitas(4);
        sdV2Medico22.setNumCitas(8);
        sdV2Medico22.setNumCitasCumplidas(2);
        sdV2Medico22.setNumEnUnidad(2);
        sdV2Medico22.setNumOtraUnidad(4);
        sdV2Medico22.setNumAltasEspeciales(7);
        sdV2Medico22.setNumAltasEspeciales(2);
        sdV2Medico22.setNumExpedidas(0);
        sdV2Medico22.setNumDias(3);
        sdV2Medico22.setPromedio(20.05);
        sdV2Medico22.setNumRecetas(9);
        sdV2Medico22.setPorcentajeAtenciones(20.22);

        sdV2MedicoLista2.add(sdV2Medico2);
        sdV2MedicoLista2.add(sdV2Medico2);

        v2especialidad2.setSdV2Medico(sdV2MedicoLista2);

        sdV2Especialidad.add(v2especialidad1);
        sdV2Especialidad.add(v2especialidad2);

        vista2.setSdV2Especialidad(sdV2Especialidad);
        vistas.setSdVista1(vista1);
        vistas.setSdVista2(vista2);

        // Reporte Parte I
        SitReporteParteI reporte = new SitReporteParteI();
        // unidad
        SdUnidadMedica unidad = new SdUnidadMedica();
        unidad.setCvePresupuestal("030103022151");
        unidad.setDesUnidadMedica("HGZMF 1 La Paz");
        reporte.setSdUnidadMedica(unidad);

        // periodo
        SdPeriodoReporte periodo = new SdPeriodoReporte();
        periodo.setCvePeriodo(201801);
        periodo.setDesPeriodo("del 26 de Diciembre 2017 al 25 de Enero de 2018");
        reporte.setSdPeriodoReporte(periodo);

        // fecGeneracion
        reporte.setFecGeneracion(new Date());

        // Estatus
        reporte.setSdVistas(vistas);

        SitControlCapturaCE sitControlCapturaCE = new SitControlCapturaCE();
        sitControlCapturaCE.setCvePeriodo(201802);
        sitControlCapturaCE.setFecActualizaControl(sdf.parse("05/09/2018"));
        sitControlCapturaCE.setIndControl(1);
        sitControlCapturaCE.setSdUnidadMedica(unidad);

        try {

            when(colaDeReportesRepository.buscarReportesEnCola(anyInt(), anyInt())).thenReturn(reportes);
            doReturn(reportes).when(colaDeReportesRepository).buscarReportesEnCola();
            when(procesarReporteParteIService.extraerInfoMedicosReporte(any(String.class), any(String.class), anyInt()))
                    .thenReturn(vistas);
            doReturn(periodo).when(procesarReporteParteIService).getPeriodoReporte();
            doReturn(unidad).when(procesarReporteParteIService).getUnidadMedica();
            when(procesarReporteParteIService.buscarReporteParte1(anyString(), anyInt())).thenReturn(reporte);
            when(procesarReporteParteIService.borrarReporteParte1(any(SitReporteParteI.class))).thenReturn(true);
            doNothing().when(procesarReporteParteIService).insertarReporte(any(SitReporteParteI.class));
            doNothing().when(reporteParte1CommonService).actualizarControlCapturaCe(any(Boolean.class), anyString(),
                    anyString(), anyInt());
            when(reporteParte1CommonRepository.actualizarControlCapturaCE(any(SitControlCapturaCE.class)))
                    .thenReturn(sitControlCapturaCE);
            when(colaDeReportesService.actualizarReporteEnCola(any(SitColaReportes.class), anyInt()))
                    .thenReturn(sitColaReportes);
            when(colaDeReportesRepository.actualizarReporteEnCola(any(SitColaReportes.class), anyInt()))
                    .thenReturn(sitColaReportes);
            colaDeReportesService.buscarReportesEnColaParalelo();
            logger.info("Clave Presupuestal: {}", sitColaReportes.getCvePresupuestal());
            logger.info("Unidad Médica: {}", sitColaReportes.getDesUnidad());
            logger.info("Clave Periodo: {}", sitColaReportes.getCvePeriodo());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void buscarReportesEnColaTestDos() {

        SitColaReportes sitColaReporteUno = new SitColaReportes();
        sitColaReporteUno.setCvePeriodo(201802);
        sitColaReporteUno.setCvePresupuestal("160102142151");
        sitColaReporteUno.setDesUnidad("HGO 221 Toluca");
        sitColaReporteUno.setEstatus(2);
        sitColaReporteUno.setFecFin(new Date());
        sitColaReporteUno.setFecInicio(new Date());

        SitColaReportes sitColaReporteDos = new SitColaReportes();
        sitColaReporteDos.setCvePeriodo(201803);
        sitColaReporteDos.setCvePresupuestal("160102142151");
        sitColaReporteDos.setDesUnidad("HGO 221 Toluca");
        sitColaReporteDos.setEstatus(2);
        sitColaReporteDos.setFecFin(new Date());
        sitColaReporteDos.setFecInicio(new Date());

        List<SitColaReportes> sitColaReportesList = new ArrayList<SitColaReportes>();
        sitColaReportesList.add(sitColaReporteUno);
        sitColaReportesList.add(sitColaReporteDos);
        Iterator<SitColaReportes> reportes = sitColaReportesList.iterator();

        try {
            when(colaDeReportesRepository.buscarReportesEnCola(anyInt(), anyInt())).thenReturn(reportes);
            Iterator<SitColaReportes> resultTest = colaDeReportesService.buscarReportesEnCola(2, null);
            Assert.assertNotNull(resultTest);
            Assert.assertEquals(resultTest, reportes);

            logger.info("Test buscarReportesEnCola");
            while (resultTest.hasNext()) {
                logger.info("Reportes en cola encontrados: {}", resultTest.next());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void tiempoProcesarReporteTest() {

        SitColaReportes sitColaReporteUno = new SitColaReportes();
        sitColaReporteUno.setCvePeriodo(201802);
        sitColaReporteUno.setCvePresupuestal("160102142151");
        sitColaReporteUno.setDesUnidad("HGO 221 Toluca");
        sitColaReporteUno.setEstatus(2);
        sitColaReporteUno.setFecFin(new Date());
        sitColaReporteUno.setFecInicio(new Date());

        SitColaReportes sitColaReporteDos = new SitColaReportes();
        sitColaReporteDos.setCvePeriodo(201803);
        sitColaReporteDos.setCvePresupuestal("160102142151");
        sitColaReporteDos.setDesUnidad("HGO 221 Toluca");
        sitColaReporteDos.setEstatus(1);
        sitColaReporteDos.setFecFin(new Date());
        sitColaReporteDos.setFecInicio(new Date());

        List<SitColaReportes> sitColaReportesList = new ArrayList<SitColaReportes>();
        sitColaReportesList.add(sitColaReporteUno);
        sitColaReportesList.add(sitColaReporteDos);
        Iterator<SitColaReportes> reportes = sitColaReportesList.iterator();

        try {

            when(colaDeReportesRepository.buscarReportesEnColaActivos(anyInt(), anyInt(), anyInt()))
                    .thenReturn(reportes);
            Integer totalReporte = colaDeReportesService.tiempoProcesarReporte(2800);

            Assert.assertNotNull(totalReporte);
            logger.info("Tiempo en generarse el reporte: {}", totalReporte);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void buscarReportesEnColaActivosTest() {

        SitColaReportes sitColaReporteUno = new SitColaReportes();
        sitColaReporteUno.setCvePeriodo(201802);
        sitColaReporteUno.setCvePresupuestal("160102142151");
        sitColaReporteUno.setDesUnidad("HGO 221 Toluca");
        sitColaReporteUno.setEstatus(1);
        sitColaReporteUno.setFecFin(new Date());
        sitColaReporteUno.setFecInicio(new Date());

        SitColaReportes sitColaReporteDos = new SitColaReportes();
        sitColaReporteDos.setCvePeriodo(201804);
        sitColaReporteDos.setCvePresupuestal("160102142151");
        sitColaReporteDos.setDesUnidad("HGO 221 Toluca");
        sitColaReporteDos.setEstatus(2);
        sitColaReporteDos.setFecFin(new Date());
        sitColaReporteDos.setFecInicio(new Date());

        List<SitColaReportes> sitColaReportesList = new ArrayList<SitColaReportes>();
        sitColaReportesList.add(sitColaReporteUno);
        sitColaReportesList.add(sitColaReporteDos);
        Iterator<SitColaReportes> reportes = sitColaReportesList.iterator();

        try {
            when(colaDeReportesRepository.buscarReportesEnColaActivos(anyInt(), anyInt(), anyInt()))
                    .thenReturn(reportes);
            Iterator<SitColaReportes> resultTest = colaDeReportesService.buscarReportesEnColaActivos(1, 2, 0);
            Assert.assertNotNull(resultTest);
            Assert.assertEquals(resultTest, reportes);

            logger.info("Test buscarReportesEnColaActivos");
            while (resultTest.hasNext()) {
                logger.info("Reportes en cola encontrados: {}", resultTest.next());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void insertarReporteEnColaTest() {

        String cvePresupuestal = "160102142151";
        String desUnidad = "HGO 221 Toluca";
        Integer cvePeriodo = 201804;
        Integer volumen = 3;

        try {

            when(colaDeReportesRepository.insertarReporteEnCola(anyString(), anyString(), anyInt(), anyInt()))
                    .thenReturn(true);
            Boolean resultTest = colaDeReportesService.insertarReporteEnCola(cvePresupuestal, desUnidad, cvePeriodo,
                    volumen);
            Assert.assertNotNull(resultTest);
            Assert.assertEquals(resultTest, true);
            logger.info("Test insertarReporteEnCola");
            logger.info("Resultado de la inserción del reporte: {}", resultTest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void actualizarReporteEnColaTest() {

        SitColaReportes sitColaReporteIni = new SitColaReportes();
        sitColaReporteIni.setCvePeriodo(201805);
        sitColaReporteIni.setCvePresupuestal("140134012151");
        sitColaReporteIni.setDesUnidad("HGZ 14 Guadalajara");
        sitColaReporteIni.setEstatus(1);
        sitColaReporteIni.setFecFin(new Date());
        sitColaReporteIni.setFecInicio(new Date());

        SitColaReportes sitColaReporteAct = new SitColaReportes();
        sitColaReporteAct.setCvePeriodo(201805);
        sitColaReporteAct.setCvePresupuestal("140134012151");
        sitColaReporteAct.setDesUnidad("HGZ 14 Guadalajara");
        sitColaReporteAct.setEstatus(2);
        sitColaReporteAct.setFecFin(new Date());
        sitColaReporteAct.setFecInicio(new Date());

        try {
            when(colaDeReportesRepository.actualizarReporteEnCola(any(SitColaReportes.class), anyInt()))
                    .thenReturn(sitColaReporteAct);
            SitColaReportes resultTest = colaDeReportesService.actualizarReporteEnCola(sitColaReporteAct, 2);
            Assert.assertNotNull(resultTest);
            Assert.assertEquals(resultTest, sitColaReporteAct);
            logger.info("Test actualizarReporteEnCola");
            logger.info("Clave Presupuestal: {}", resultTest.getCvePresupuestal());
            logger.info("Unidad Médica: {}", resultTest.getDesUnidad());
            logger.info("Clave Periodo: {}", resultTest.getCvePeriodo());
            logger.info("Estatus actualizado: {}", resultTest.getEstatus());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void buscarReporteExistenteEnColaTest() {

        SitColaReportes sitColaReporte = new SitColaReportes();
        sitColaReporte.setCvePeriodo(201802);
        sitColaReporte.setCvePresupuestal("141609UA2151");
        sitColaReporte.setDesUnidad("UMAA 1 Lagos Moreno");
        sitColaReporte.setEstatus(2);
        sitColaReporte.setFecFin(new Date());
        sitColaReporte.setFecInicio(new Date());

        String cvePresupuestal = "141609UA2151";
        Integer cvePeriodo = 201802;
        Integer estatus = 2;

        try {

            when(colaDeReportesRepository.buscarReporteExistenteEnCola(anyString(), anyInt(), anyInt(), anyLong()))
                    .thenReturn(sitColaReporte);
            SitColaReportes resultTest = colaDeReportesService.buscarReporteExistenteEnCola(cvePresupuestal, cvePeriodo,
                    estatus, null);
            Assert.assertNotNull(resultTest);
            Assert.assertEquals(resultTest, sitColaReporte);
            logger.info("Test buscarReporteExistenteEnCola");
            logger.info("Clave Presupuestal: {}", resultTest.getCvePresupuestal());
            logger.info("Unidad Médica: {}", resultTest.getDesUnidad());
            logger.info("Clave Periodo: {}", resultTest.getCvePeriodo());
            logger.info("Estatus: {}", resultTest.getEstatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
