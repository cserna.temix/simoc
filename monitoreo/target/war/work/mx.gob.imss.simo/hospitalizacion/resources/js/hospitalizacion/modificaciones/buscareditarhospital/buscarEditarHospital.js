//var log = log4javascript.getDefaultLogger();
var log = log4javascript.getNullLogger();

$(document)
        .ready(
                function() {

                    
                    btn_click("#datosBuscarEditarHospForm")
                    validateInputMaskChangeOrPressEnterTabOrMaxlength("#datosBuscarEditarHospForm\\:idNss", "rcNss", 10);
                    validateInputChangeOrPressEnterTabOrMaxlength("#datosBuscarEditarHospForm\\:idAgregadoMedico",
                            "rcAgregado", 8);
                    validateInputChangeOrPressEnterTabOrMaxlength("#datosBuscarEditarHospForm\\:idNumeroPaciente",
                            "rcMapeoPaciente", 2);
                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idTipoPartoToco",
                            "datosBuscarEditarHospForm:idTipoPartoToco", "rcMapeoTipoParto");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idMotivoAltaEgreso",
                            "datosBuscarEditarHospForm:idMotivoAltaEgreso", "rcMapeoMotivoAltaEgreso");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idInicialEgreso",
                            "rcMapeoInicialIngreso");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPrincipalEgresoDiag",
                            "rcMapeoPrincipalEgreso");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPrimerSecundarioEgreso",
                            "rcMapeoPrimerSecundarioEgreso");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idSegundoSecundarioEgreso",
                            "rcMapeoSegundoSecundarioEgreso");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPrimerComplicacionEgreso",
                            "rcMapeoPrimerComplicacionEgreso");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idSegundoComplicacionEgreso",
                            "rcMapeoSegundaComplicacionEgreso");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idDefuncionCausaDirecta",
                            "rcMapeoCausaDirectaDefuncion");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idDefuncionCausaBasica",
                            "rcMapeoCausaBasicaDefuncion");
                    escDialog();

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn1",
                            "datosBuscarEditarHospForm:idSexoRn1", "rcSexoRN1");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn1", "rcPesoRN1");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn1", "rcTallaRN1");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn2",
                            "datosBuscarEditarHospForm:idSexoRn2", "rcSexoRN2");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn2", "rcPesoRN2");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn2", "rcTallaRN2");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn3",
                            "datosBuscarEditarHospForm:idSexoRn3", "rcSexoRN3");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn3", "rcPesoRN3");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn3", "rcTallaRN3");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn4",
                            "datosBuscarEditarHospForm:idSexoRn4", "rcSexoRN4");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn4", "rcPesoRN4");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn4", "rcTallaRN4");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn5",
                            "datosBuscarEditarHospForm:idSexoRn5", "rcSexoRN5");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn5", "rcPesoRN5");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn5", "rcTallaRN5");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn6",
                            "datosBuscarEditarHospForm:idSexoRn6", "rcSexoRN6");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn6", "rcPesoRN6");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn6", "rcTallaRN6");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn7",
                            "datosBuscarEditarHospForm:idSexoRn7", "rcSexoRN7");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn7", "rcPesoRN7");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn7", "rcTallaRN7");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn8",
                            "datosBuscarEditarHospForm:idSexoRn8", "rcSexoRN8");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn8", "rcPesoRN8");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn8", "rcTallaRN8");

                    validateSelectOneMenuPressEnterTab("#datosBuscarEditarHospForm\\:idSexoRn9",
                            "datosBuscarEditarHospForm:idSexoRn9", "rcSexoRN9");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idPesoRn9", "rcPesoRN9");
                    validateInputChangeOrPressEnterTab("#datosBuscarEditarHospForm\\:idTallaRn9", "rcTallaRN9");
                   
                    //setColorEncabezado();
                    configuraPantalla(4,2,true);
                    ocultarDivs();

                });

function setColorEncabezado() {

    setClassEncabezadoPieDePagina('encabezadoCaptura', 'pieDePaginaCaptura');
}

function verificarDialogo() {

    if ($("#datosBuscarEditarHospForm\\:banderaNssHidden").val().toLowerCase() === 'true') {
        rcObtenerPaciente();

    }
}

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }

                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }

                    if (e.which === 9) {
                        return false;
                    }
                });

function desactivarSeccionRN() {

    $("#datosBuscarEditarHospForm\\:idCabecera").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN0").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN1").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN2").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN3").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN4").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN5").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN6").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN7").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN8").hide();
    $("#datosBuscarEditarHospForm\\:idRowRN9").hide();
}

function ocultarDivs() {    
    desactivarSeccionRN();
}

function activarSeccionesTablas(){
    var total = $("#datosBuscarEditarHospForm\\:totalRNTabla").val();
    $("#datosBuscarEditarHospForm\\:idRowRN0").show();
    if (total >= 1) {
        $("#datosBuscarEditarHospForm\\:idRowRN1").show();
    }
    if (total >= 2) {
        $("#datosBuscarEditarHospForm\\:idRowRN2").show();
    }
    if (total >= 3) {
        $("#datosBuscarEditarHospForm\\:idRowRN3").show();
    }
    if (total >= 4) {
        $("#datosBuscarEditarHospForm\\:idRowRN4").show();
    }
    if (total >= 5) {
        $("#datosBuscarEditarHospForm\\:idRowRN5").show();
    }
    if (total >= 6) {
        $("#datosBuscarEditarHospForm\\:idRowRN6").show();
    }
    if (total >= 7) {
        $("#datosBuscarEditarHospForm\\:idRowRN7").show();
    }
    if (total >= 8) {
        $("#datosBuscarEditarHospForm\\:idRowRN8").show();
    }
    if (total >= 9) {
        $("#datosBuscarEditarHospForm\\:idRowRN9").show();
    }

    setTimeout(habilitarCamposTablaQx, 100);
    setTimeout(colocarFocoQx, 400);
}

function prepararDatosRN(valor, posicion, rmc) {

    $("#datosBuscarEditarHospForm\\:posicion").val(posicion);
    $("#datosBuscarEditarHospForm\\:valor").val(valor);
    rmc();
}

function rcMapeoProcedimientoParticular(valor, posicion) {

    log.info("->id rcMapeoProcedimientoParticular:" + valor);
    log.info("->id rcMapeoProcedimientoParticular:" + posicion);
    $("#datosBuscarEditarHospForm\\:procedimientoValidar").val(valor);
    $("#datosBuscarEditarHospForm\\:procedimientoPosicion").val(posicion);

    rcMapeoProcedimiento();
}

function actualizarFocoEditar() {

    $("#datosBuscarEditarHospForm\\:idEditar").focus();
    setTimeout(desHabilitarCamposTablaQx, 100);

}

function actualizarFocoGuardar() {

    var foco = $('[id="datosBuscarEditarHospForm:idHiddenFoco"]').val();
    if (foco == "datosBuscarEditarHospForm:idGuardar") {
        $('[id="datosBuscarEditarHospForm:idGuardar"]').focus();
    } else {
        $('[id="datosBuscarEditarHospForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

function sendParams(value) {

    passToJSFMB([ {
        name : 'nameParam',
        value : value
    } ]);
}

function someFunction() {

    var value = "1";
    sendParams(value);

    rcProcedimiento1();
}

function rcMapeoProcedimiento2() {

    $("#datosBuscarEditarHospForm\\:procedimientoValidar").val('1');
    var valor = $("#datosBuscarEditarHospForm\\:procedimientoValidar").val();
    log.info("->cadena :" + valor);
    // scriptBloquear();
    rcProcedimientoGeneral();
    // agregarCampos();

}

function agregarCampos() {

    log.info("->agregarCampos :" + "------------");

}

function validarProcedimiento(event, object) {

    try {
        var shift = event.shiftKey;
        var tab = event.which == 9;
        var enter = event.which == 13;

        if (tab || enter) {
            log.info("-----------------------> validar Procedimiento :" + "------------");
            var idComponente = object.id;
            var valorComponente = object.value;
            $("#datosBuscarEditarHospForm\\:procedimientoValidar").val(idComponente + ":" + valorComponente);
            log.info("-----------------------> validar Procedimiento id Componente : " + idComponente + " valor : "
                    + valorComponente);
            scriptBloquear();
            rcProcedimientoGeneral();
        }else{
        	rcDeshabilitarGuardar();
        }
    } catch (e) {
    }

}

function scrollTo(elementID) {

    var id = elementID.replace(':', '\\:');
    var element = $('#' + id);
    var height = $(element).height();
    var index = $(element).parents('tr').index();
    var parentDiv = $(element).closest('div').parents('div');
    $(parentDiv).scrollTop((index * height) + (index * 1));
}

function activarCamposEdicionTablaQx() {
    log.info("-----------------------> activar campos Edicion Tabla Qx " + "------------");
    var objetosEdicion = new Array();
    objetosEdicion = $(".edicion_tabla");
    var text = "";
    var inicioIdFoco = "'[id=\"";
    var finIdFoco = "\"]'";
    var focoTabla = "";
    var cadena = "";

    for (var k = 0; k < objetosEdicion.length; k++) {
        text = objetosEdicion[k].id;
        log.info("-----------------------> Id " + " ------------ " + text);
        focoTabla = inicioIdFoco + text + finIdFoco;
        $("#datosBuscarEditarHospForm\\:idFocoTabla").val(text);
        log.info("=============== " + $("#datosBuscarEditarHospForm\\:idFocoTabla").val());
        rcFocoTablaQx();
        break;
    }
}

function deshabilitarCamposQx() {

    log.info("=============== #### " + $("#datosBuscarEditarHospForm\\:idFocoTabla").val());
    habilitarCamposTablaQx();
    setTimeout(colocarFocoQx, 400);

}

function colocarFocoBotonCancelar() {

    $('[id="datosBuscarEditarHospForm:idbotonCancelar"]').focus();
  
}

function colocarFocoQx() {

    $('[id="' + $("#datosBuscarEditarHospForm\\:idFocoTabla").val() + '"]').focus();
}

function habilitarCamposTablaQx() {

    $("#datosBuscarEditarHospForm\\:idCabecera").show();
    $(".edicion_tabla").removeAttr('disabled', 'disabled').removeClass('ui-state-disabled');
}

function desHabilitarCamposTablaQx() {

    $(".edicion_tabla").attr('disabled', 'disabled').addClass('ui-state-disabled');
}

function activarGuaradar() {
    var foco = $('[id="datosBuscarEditarHospForm:idHiddenFoco"]').val();
    if (foco === "datosBuscarEditarHospForm:idGuardar") {
        $('[id="datosBuscarEditarHospForm:idGuardar"]').focus();
    }
}
