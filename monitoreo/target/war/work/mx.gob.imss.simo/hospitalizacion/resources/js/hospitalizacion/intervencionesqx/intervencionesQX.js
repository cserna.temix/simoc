// var log = log4javascript.getDefaultLogger();
var log = log4javascript.getNullLogger();

$(document).ready(
        function() {

            btn_click("#capturaIntervencionQX")
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idFecha", "rcFecha", 10);
            validateInputChangeOrPressEnterTab("#capturaIntervencionQX\\:idNoQuirofano", "rcNoQuirofano");
            validateInputChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idNss", "rcNSS", 10);
            validateInputChangeOrPressEnterTab("#capturaIntervencionQX\\:idNoCama", "rcNoCama");
            validateInputChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idNumeroPaciente",
                    "rcMapeoPaciente", 4);

            validateAutocompletePressEnterTabOrMaxlength("#capturaIntervencionQX\\:idEspecialidad",
                    "capturaIntervencionQX:idEspecialidad", "rcEspecialidad", 4);

            validateInputChangeOrPressEnterTab("#capturaIntervencionQX\\:idProcedimientos", "rcIdProcedimientos");
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:horaEntrada", "rcHoraEntrada",
                    5);
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idHoraInicio", "rcHoraInicio",
                    5);
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idHoraTermino",
                    "rcHoraTermino", 5);
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idHoraSalidaSala",
                    "rcHoraSalidaSala", 5);

            validateInputChangeOrPressEnterTab("#capturaIntervencionQX\\:idAnestesiologo", "rcIdAnestesiologo");

            validateInputChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idNumeroCantidad",
                    "rcMapeoCantidad", 4);

            validateAutocompletePressEnterTabOrMaxlength("#capturaIntervencionQX\\:idMetodoPlanificacion",
                    "capturaIntervencionQX:idMetodoPlanificacion", "rcMetodoPlanificacion", 2);

            validateSelectOneMenuPressEnterTab("#capturaIntervencionQX\\:idTipoAnestesia",
                    "capturaIntervencionQX:idTipoAnestesia", "rcTipoAnestesia");

            //validateInputMaskChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idFechaIntervencion",
              //      "rcMapeoFechaIntervencion", 10);
            validateSelectOneMenuPressEnterTab("#capturaIntervencionQX\\:idTipoIntervencion",
                    "capturaIntervencionQX:idTipoIntervencion", "rcTipoIntervencion");

            validateInputChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idMatricula", "rcMatricula", 10);
            validateInputChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idNumeroCama", "rcMapeoCama", 4);

            //validateInputChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idProcedimientos",
              //      "rcMapeoModalProcedimiento", 2);

            validateInputChangeOrPressEnterTabOrMaxlength("#capturaIntervencionQX\\:idProcedimientoModal",
                    "rcClaveProcedimiento", 4);
            validateInputChangeOrPressEnterTabBlock("#capturaIntervencionQX\\:idCirujanoModal", "rcClaveCirujano");

            escDialogIQX();
            //setColorEncabezado();
            configuraPantalla(4,2,true);
        });

function verificarDialogo() {

    if ($("#capturaIntervencionQX\\:banderaNssHidden").val().toLowerCase() === 'true') {
        rcObtenerPaciente();
    }
}

function activarGuaradar() {

    var foco = $('[id="capturaIntervencionQX:idHiddenFoco"]').val();
    if (foco == "botonGuardar") {
        $('[id="capturaIntervencionQX:botonGuardar"]').focus();
    }
}

function focoGuardar() {
	 $('[id="capturaIntervencionQX:botonGuardar"]').focus();
}

function desactivarGuardar(){
	 $('[id="capturaIntervencionQX:botonGuardar"]').addClass('ui-state-disabled').attr('disabled', 'disabled');//desabilita boton
}

$(document).on("keydown", "input:enabled", function(e) {

    if (e.ctrlKey && e.altKey) {
        if (e.which) {
            return false;
        }
    }
});

var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }

                    if ((e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223)) {
                        return false;
                    }

                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }

                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }

                    if (e.keyCode == 226) {
                        return false;
                    }

                    if (shiftPresionado && e.keyCode == 226) {
                        return false;
                    }
                });

function escDialogIQX() {

    $(document).keyup(function(e) {

        if (e.keyCode == 27) { // esc code is 27
            PF('dlgPacientes').hide();
            PF('dlgCamaPacientes').hide();
            PF('idDlgFechaIntervencion').hide();
            PF('idDlgCantidad').hide();
            PF('dlgProcedimientos').hide();
        }
    });
}

function setColorEncabezado() {

    setClassEncabezadoPieDePagina('encabezadoCaptura', 'pieDePaginaCaptura');
}
