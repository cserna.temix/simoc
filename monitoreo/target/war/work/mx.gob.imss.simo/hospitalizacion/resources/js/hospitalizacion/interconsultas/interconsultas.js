/**
 * 
 */
var log = log4javascript.getNullLogger();


$(document).ready(
		function() {
			
			escDialog();
			var $_FORM = "#interconsultaForm\\:";
			 var $_FORM_SF = "interconsultaForm:";
			
			btn_click("#interconsultaForm");
			configuraPantalla(4,2,true);
			
			validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idFechaIntercon", "rcFechaIntercon", 10);
			//validateSelectOneMenuPressEnterTab("#interconsultaForm\\:especialidadSel", "interconsultaForm:especialidadSel", "rcEspacialidadIntercon");
			validateAutocompletePressEnterTabOrMaxlength($_FORM + "especialidadSel", $_FORM_SF
                    + "especialidadSel", "rcEspacialidadIntercon", 4);
			//validateInputChangeOrPressEnterTab($_FORM + "idMatricula", "rcMatricula", 6);
			validateInputChangeOrPressEnterTab($_FORM + "idMatricula", "rcMatricula");
			//validateSelectOneMenuPressEnterTab("#interconsultaForm\\:turnoSel", "interconsultaForm:turnoSel", "rcTurnoIntercon");
//			validateAutocompletePressEnterTabOrMaxlength($_FORM + "turnoSel", $_FORM_SF
//                    + "turnoSel", "rcTurnoIntercon", 1);
			validateSelectOneMenuPressEnterTab("#interconsultaForm\\:turnoSel", "interconsultaForm:turnoSel", "rcTurnoIntercon");
			
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNss", "rcNss", 10);
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idAgregadoMedico", "rcAgregadoMedico", 8);
			validateInputChangeOrPressEnterTab($_FORM + "idNombre", "rcNombre");
			validateInputChangeOrPressEnterTab($_FORM + "idApellidoMaterno", "rcApellidoMaterno");
			validateInputChangeOrPressEnterTab($_FORM + "idApellidoPaterno", "rcApellidoPaterno");
			//validateInputChangeOrPressEnterTab($_FORM + "idFechaNacimiento", "rcFechaNacimiento");
			//validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idFechaNacimiento", "rcFechaNacimiento", 10);
			validateInputChangeOrPressEnterTab($_FORM + "idNumeroPaciente", "rcNumeroPaciente");
			validateAutocompletePressEnterTabOrMaxlength($_FORM + "idDelegacion", $_FORM_SF + "idDelegacion",
                    "rcDelegacion", 2);
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumero", "rcNumero", 3);
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDuracionIntercon", "rcDuracionIntercon", 2);
			validateSelectOneMenuPressEnterTab("#interconsultaForm\\:idPrimVez", "interconsultaForm:idPrimVez", "rcPrimeraVezIntercon");
			//validateSelectOneMenuPressEnterTab("#interconsultaForm\\:idEspecialidadSolic", "interconsultaForm:idEspecialidadSolic", "rcEspecialidadSolic");
			validateAutocompletePressEnterTabOrMaxlength($_FORM + "idEspecialidadSolic", $_FORM_SF
                    + "idEspecialidadSolic", "rcEspecialidadSolic", 4);
			
			validateSelectOneMenuPressEnterTab("#interconsultaForm\\:idAltaIntercon", "interconsultaForm:idAltaIntercon", "rcAltaIntercon");
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagPrincipal", "rcDiagPrincipal", 4);
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagAdicional", "rcDiagAdicional", 4);
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idDiagComplement", "rcDiagComplement", 4);
			validateSelectOneMenuPressEnterTab("#interconsultaForm\\:idPrimVezDiagAdic", "interconsultaForm:idPrimVezDiagAdic", "rcPrimVezDiagAdicional");
			validateSelectOneMenuPressEnterTab("#interconsultaForm\\:idPrimVezDiagCompl", "interconsultaForm:idPrimVezDiagCompl", "rcPrimVezDiagComplement");
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idProcedimientoPrincipal", "rcProcedimientoPrincipal", 4);
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idProcedimientoAdicional", "rcProcedimientoAdicional", 4);
			validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idProcedimientoComplement", "rcProcedimientoComplement", 4);
			 actualizarFocoGuardar();
			 actualizarFocoGuardarDetalle();
			 actualizarFocoInterconsultaSi();
		});

function actualizarFocoGuardar() {
	
	btn_click("#interconsultaForm");	
    var foco = $('[id="interconsultaForm:idHiddenFoco"]').val();
    if (foco == "interconsultaForm:idGuardar") {
        $('[id="interconsultaForm:idGuardar"]').focus();
    } else {
        $('[id="interconsultaForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

function actualizarFocoGuardarDetalle() {
	
	 btn_click("#interconsultaForm");	
    var foco = $('[id="interconsultaForm:idHiddenFoco"]').val();
    if (foco == "interconsultaForm:idGuardarDetalle") {
        $('[id="interconsultaForm:idGuardarDetalle"]').focus();
    } else {
        $('[id="interconsultaForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
    btn_click("#interconsultaForm");	
}

function actualizarFocoInterconsultaSi() {

	 btn_click("#interconsultaForm");	
    var foco = $('[id="interconsultaForm:idHiddenFoco"]').val();
    if (foco == "interconsultaForm:btn_modal_si") {
        $('[id="interconsultaForm:btn_modal_si"]').focus();
    } else {
        $('[id="interconsultaForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
   
}

var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }

                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }

                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                });
