/**
 * 
 */
/**
 * var log se implenta generar log4j mediante la libreria importada en el head para deshabilitar utilizar var log =
 * log4javascript.getNullLogger(); y comentar var log = log4javascript.getDefaultLogger();
 */
var log = log4javascript.getDefaultLogger();
// var log = log4javascript.getNullLogger();

$(document).ready(function(){
	log.info("SE CARGA scriptsEjemplo");
	scriptsEjemplo();
	botonNoSubmit("nombreForm");
});

//PRUEBAS


//Funcion que sirve que no se permita poner ningun simbolo con las teclas alt gr o alt+ctrl
$(document).on("keydown", "input:enabled",function(e){	
	if (e.ctrlKey && e.altKey) {
//		switch(e.which) {
		if(e.which) {

	    	return false;
		}
	}
});

//Botonoes
function botonNoSubmit(idForm){
	$(document).on("keydown", "#"+idForm,function( event){	
 		 if(event.keyCode == 13 ) {
		      event.preventDefault();					      					   
		      if(document.activeElement.tagName=="BUTTON"){		    	 
			    	 var activo=document.getElementById(document.activeElement.getAttribute("id"));
			    	 activo.click();
			    	 //activo.disabled = true;
			    	 if($C<1){
	                     $C=$C+1;
	                     setTimeout(reiniciaContador, $TIEMPO_ESPERA);
	                     activo.click();
			    	 }			    
			  }
		    }
		  });
}




function scriptsEjemplo(){
	//validate InputMask con un Change o con una Longitud exacta o con Presionar la tecla Enter o Tabulador
	validateInputMaskChangeOrPressEnterTabOrMaxlength("#nombreForm\\:idInputMaskLongitud","rcInputMaskLongitud",10);
	//validate InputText con un Change o con una Longitud exacta o con Presionar la tecla Enter o Tabulador
	validateInputChangeOrPressEnterTabOrMaxlength("#nombreForm\\:idInputTextLongitudFija","rcInputTextLongitudFija",4);
	//validate InputText con un Change o  con Presionar la tecla Enter o Tabulador
	validateInputChangeOrPressEnterTab("#nombreForm\\:idInputTextLongitudIndefinida","rcInputTextLongitudIndefinida");

	validateSelectOneMenuPressEnterTab("#nombreForm\\:idSelectOneMenu","nombreForm:idSelectOneMenu","rcSelectOneMenuPf");
	validateAutocompletePressEnterTabOrMaxlength("#nombreForm\\:idAutoComplete","nombreForm:idAutoComplete","rcAutoCompletePf",3);

	//abre un dialog
	validateInputChangeOrPressEnterTabOrMaxlength("#nombreForm\\:idInputTextExactaOpenDialog","rcInputTextExactaOpenDialog",2);
	
	//input en dialog
	//validateInputDialogChangeOrPressEnterTabOrMaxlength(id,fStr,max,growl_widget,mensaje);
	validateInputDialogChangeOrPressEnterTabOrMaxlength("#nombreForm\\:idInputTextExactaEnDialog","rcInputTextExactaEnDialog",2,"growlMessage_widget","El campo EJEMPLO es requerido");
	//validateMaskDialogChangeOrPressEnterTabOrMaxlength(id,fStr,max,growl_widget,mensaje)
	validateMaskDialogChangeOrPressEnterTabOrMaxlength("#nombreForm\\:idInputMaskLongitudEnDialog","rcInputTextLongitudEnDialog",10,"growlMessage_widget","El campo MASK es requerido")
	
}



// estos inputs abren un Dialog

validateInputChangeOrPressEnterTab("#nombreForm\\:idInputTextLongitudOpenDialog","rcInputTextLongitudOpenDialog");

//INPUTS EN DIALOGS
//validate InputText dentro de un DIalog con un Change con una Longitud exacta o con Presionar la tecla Enter o Tabulador

//validate InputText dentro de un DIalog con un Change  o con Presionar la tecla Enter o Tabulador
validateInputDialogChangeOrPressEnterTab("#nombreForm\\:idInputTextIndefinidaEnDialog","rcInputTextIndefinidaEnDialog","growlMessage_widget","El campo InputTextTest2 es requerido");



//validate InputText con un Change o con una Longitud exacta o con Presionar la tecla Enter o Tabulador
validateSelectOneMenuPressEnterTab("#formCexBusqEd\\:idOcasservicio","formCexBusqEd:idOcasservicio","rc_Ocasservicio");