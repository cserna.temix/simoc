/**
 * var log se implenta generar log4j mediante la libreria importada en el head para deshabilitar utilizar var log =
 * log4javascript.getNullLogger(); y comentar var log = log4javascript.getDefaultLogger();
 */
// var log = log4javascript.getDefaultLogger();
var log = log4javascript.getNullLogger();

$(document).ready(
        function() {

            var $_FORM = "#capturarIngresoForm\\:";
            var $_FORM_SF = "capturarIngresoForm:";

            btn_click("#capturarIngresoForm")

            escDialog();
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idDelegacion", $_FORM_SF + "idDelegacion",
                    "rcDelegacion", 2);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idEspecialidad", $_FORM_SF + "idEspecialidad",
                    "rcEspecialidad", 4);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idEspecialidadHospitalizacion", $_FORM_SF
                    + "idEspecialidadHospitalizacion", "rcEspecialidadHospitalizacion", 4);
            validateInputChangeOrPressEnterTab($_FORM + "idApellidoMaterno", "rcApellidoMaterno");
            validateInputChangeOrPressEnterTab($_FORM + "idApellidoPaterno", "rcApellidoPaterno");
            validateInputChangeOrPressEnterTab($_FORM + "idCama", "rcCama");
            validateInputChangeOrPressEnterTab($_FORM + "idCamaRN", "rcCamaRN");
            validateInputChangeOrPressEnterTab($_FORM + "idNombre", "rcNombre");
            validateInputChangeOrPressEnterTab($_FORM + "idNumeroPaciente", "rcNumeroPaciente");
            validateInputChangeOrPressEnterTab($_FORM + "idNumeroCama", "rcNumeroCama");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idAgregadoMedico", "rcAgregadoMedico", 8);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCama", "rcCama", 4);
            //validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCamaRN", "rcCamaRN", 4);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idEdad", "rcEdad", 3);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idMatricula", "rcMatricula", 10);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNss", "rcNss", 10);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumero", "rcNumero", 3);
            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idFechaIngreso", "rcFechaIngreso", 10);
            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idHoraIngreso", "rcHoraIngreso", 5);
            validateSelectOneMenuPressEnterTab($_FORM + "idTipoIngreso", $_FORM_SF + "idTipoIngreso", "rcTipoIngreso");
            //setColorEncabezado();
            configuraPantalla(4,2,true);
        });

function actualizarFocoGuardar() {

    var foco = $('[id="capturarIngresoForm:idHiddenFoco"]').val();
    if (foco == "capturarIngresoForm:idGuardar") {
        $('[id="capturarIngresoForm:idGuardar"]').focus();
    } else {
        $('[id="capturarIngresoForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

function actualizarFocoEspecialidad() {

    var foco = $('[id="capturarIngresoForm:idHiddenFoco"]').val();
    if (foco == "capturarIngresoForm:idEspecialidad") {
        $('[id="capturarIngresoForm:idEspecialidad"]').focus();
    } else {
        $('[id="capturarIngresoForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

function desactivarBotonGuardar(){
	 $('[id="capturarIngresoForm:idGuardar"]').addClass('ui-state-disabled').attr('disabled', 'disabled');//desabilita boton
}


var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if ((e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223)) {
                        return false;
                    }
                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }
                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                    if (e.keyCode == 226) {
                        return false;
                    }
                    if (shiftPresionado && e.keyCode == 226) {
                        return false;
                    }
                });

function setColorEncabezado() {

    setClassEncabezadoPieDePagina('encabezadoCaptura', 'pieDePaginaCaptura');
}

function deshabilitaEnter() {

    btn_click("#capturarIngresoForm");
}
