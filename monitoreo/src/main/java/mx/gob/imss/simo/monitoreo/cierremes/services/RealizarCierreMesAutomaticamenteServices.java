package mx.gob.imss.simo.monitoreo.cierremes.services;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierreAutomaticoMes;

public interface RealizarCierreMesAutomaticamenteServices extends MonitoreoCommonServices {

    /**
     * @param fecha
     * @param numDias
     * @param feriados
     * @return
     */
    Date contarDiasHabiles(Date fecha, int numDias, List<Date> feriados);

    /**
     * @throws Exception
     */

    void cerrarMesAutomaticamente() throws Exception;

    /**
     * @param cveParametro
     * @return
     */
    Integer obtenerParametroBaseDatos(String cveParametro);

    /**
     * @param fechaActual
     * @return
     */
    List<Date> obtenerDiasNoHabiles(Date fechaActual);

    /**
     * @param clavePeriodo
     * @return
     */
    List<PeriodoOperacion> obtenerListaPresupuestalesAbiertasActivas(String clavePeriodo);

    /**
     * Obtener el listado de claves presupuestales para el cierre automatico de mes por tipo de captura
     * 
     * @return
     */
    List<UnidadMedicaCierreAutomaticoMes> obtenerUnidadesMedicasParaCierreAutomaticoDeMes();
}
