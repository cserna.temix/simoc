/**
 * 
 */
package mx.gob.imss.simo.monitoreo.diassincapturar.repository.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.repository.BaseJDBCRepository;
import mx.gob.imss.simo.model.UnidadMedica;
import mx.gob.imss.simo.monitoreo.common.constants.SQLConstants;
import mx.gob.imss.simo.monitoreo.diassincapturar.helper.ContadorRowMapperHelper;
import mx.gob.imss.simo.monitoreo.diassincapturar.helper.DelegacionDiasSinCapturarRowMapperHelper;
import mx.gob.imss.simo.monitoreo.diassincapturar.helper.MonitorDiasSinCapturarRowMapperHelper;
import mx.gob.imss.simo.monitoreo.diassincapturar.helper.PeriodoDiasSinCapturarRowMapperHelper;
import mx.gob.imss.simo.monitoreo.diassincapturar.helper.UnidadMedicaDiasSinCapturarRowMapperHelper;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.Contador;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.DelegacionDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorInsertDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.PeriodoDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.UnidadMedicaDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.repository.MonitorDiasSinCapturarRepository;

/**
 * @author jonathan.lopez
 */
@Repository
public class MonitorDiasSinCapturarRepositoryImpl extends BaseJDBCRepository
        implements MonitorDiasSinCapturarRepository {

    static final Logger log = LoggerFactory.getLogger(MonitorDiasSinCapturarRepositoryImpl.class);

    /**
     * Esta seccion contiene los metodos para el despliegue de datos en la vista del caso de uso
     */
    @Override
    public List<DelegacionDiasSinCapturar> obtenerDelegacionDiasSinCapturar(String cveDelegacion,
            boolean filtraDelegacion) {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_BUSCAR_DELEGACION;

        if (filtraDelegacion) {
            sql = sql.replaceAll(SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_DELEGACION,
                    SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_FILTRAR_DELEGACION);
            return jdbcTemplate.query(sql, new Object[] { cveDelegacion },
                    new DelegacionDiasSinCapturarRowMapperHelper());
        } else {
            sql = sql.replaceAll(SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_DELEGACION, "");
            return jdbcTemplate.query(sql, new DelegacionDiasSinCapturarRowMapperHelper());
        }
    }

    @Override
    public List<UnidadMedicaDiasSinCapturar> obtenerUnidadesMedicasDiasSinCapturar(String cveDelegacion,
            List<UnidadMedica> cvePresupuestal, boolean filtrarUnidadMedica) {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_BUSCAR_UNIDAD_MEDICA;
        StringBuffer listClavePresupuestal = new StringBuffer();
        if (filtrarUnidadMedica) {
            if (cvePresupuestal.size() == 1) {
                sql = sql.replaceAll(SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_UNIDADMEDICA,
                        SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_FILTRAR_UNIDADMEDICA);

                return jdbcTemplate.query(sql,
                        new Object[] { cveDelegacion, cvePresupuestal.get(0).getCvePresupuestal() },
                        new UnidadMedicaDiasSinCapturarRowMapperHelper());
            } else {

                String queryIn = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_FILTRAR_UNIDADMEDICA_IN;
                for (UnidadMedica unidadMedica : cvePresupuestal) {
                    listClavePresupuestal.append(unidadMedica.getCvePresupuestal()).append(",");
                }

                listClavePresupuestal.delete(listClavePresupuestal.length() - 1, listClavePresupuestal.length());
                String queryFinal = queryIn.concat(listClavePresupuestal + ")");
                sql = sql.replaceAll(SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_UNIDADMEDICA,
                        queryFinal);

                return jdbcTemplate.query(sql, new Object[] { cveDelegacion },
                        new UnidadMedicaDiasSinCapturarRowMapperHelper());
            }
        } else {
            sql = sql.replaceAll(SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_UNIDADMEDICA, "");
            return jdbcTemplate.query(sql, new Object[] { cveDelegacion },
                    new UnidadMedicaDiasSinCapturarRowMapperHelper());
        }
    }

    @Override
    public List<MonitorDiasSinCapturar> obtenerMonitorDiasSinCapturar(String cveDelegacion, String cvePresupuestal) {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_GRID;

        return jdbcTemplate.query(sql, new Object[] { cveDelegacion, cvePresupuestal },
                new MonitorDiasSinCapturarRowMapperHelper());
    }

    /**
     * Esta seccion contiene los metodos para el llenado de la informacion requerida en la vista
     */
    @Override
    public List<PeriodoDiasSinCapturar> obtenerPeriodosDiasSinCapturar() {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_PERIODOS;

        return jdbcTemplate.query(sql, new PeriodoDiasSinCapturarRowMapperHelper());
    }

    @Override
    public Map<Date, Integer> obtenerEgresosHospitalarios(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_EGRESOS_HOSPITALARIOS;
        logger.debug("obtenerEgresosHospitalarios::: " + periodoDiasSinCapturar.getCvePresupuestal() + ", "
                + SQLConstants.TIPO_FORMATO_EGRESOS_HOSPITALARIOS + ", " + periodoDiasSinCapturar.getFecInicial() + ", "
                + periodoDiasSinCapturar.getFecFinal());
        List<Contador> list = jdbcTemplate.query(sql,
                new Object[] { periodoDiasSinCapturar.getCvePresupuestal(),
                        SQLConstants.TIPO_FORMATO_EGRESOS_HOSPITALARIOS,
                        SQLConstants.TIPO_FORMATO_EGRESOS_CUNERO_PATOLOGICO, periodoDiasSinCapturar.getFecInicial(),
                        periodoDiasSinCapturar.getFecFinal() },
                new ContadorRowMapperHelper());

        return convierteListaMapa(list);
    }

    @Override
    public Map<Date, Integer> obtenerIntervencionesQuirurgicas(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_INTERVENCIONES_QUIRURGICAS;
        logger.debug("obtenerIntervencionesQuirurgicas::: " + periodoDiasSinCapturar.getCvePresupuestal() + ", "
                + periodoDiasSinCapturar.getFecInicial() + ", " + periodoDiasSinCapturar.getFecFinal());
        List<Contador> list = jdbcTemplate
                .query(sql,
                        new Object[] { periodoDiasSinCapturar.getCvePresupuestal(),
                                periodoDiasSinCapturar.getFecInicial(), periodoDiasSinCapturar.getFecFinal() },
                        new ContadorRowMapperHelper());

        return convierteListaMapa(list);
    }

    @Override
    public Map<Date, Integer> obtenerEgresosUrgencias(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_EGRESOS_HOSPITALARIOS_URGENCIAS;
        logger.debug("obtenerEgresosUrgencias::: " + periodoDiasSinCapturar.getCvePresupuestal() + ", "
                + SQLConstants.TIPO_FORMATO_EGRESOS_URGENCIAS + ", " + periodoDiasSinCapturar.getFecInicial() + ", "
                + periodoDiasSinCapturar.getFecFinal());
        List<Contador> list = jdbcTemplate.query(sql,
                new Object[] { periodoDiasSinCapturar.getCvePresupuestal(), SQLConstants.TIPO_FORMATO_EGRESOS_URGENCIAS,
                        periodoDiasSinCapturar.getFecInicial(), periodoDiasSinCapturar.getFecFinal() },
                new ContadorRowMapperHelper());

        return convierteListaMapa(list);
    }

    @Override
    public int borrarMonitorDiasSinCapturar() throws HospitalizacionException {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_DELETE;

        try {
            return jdbcTemplate.update(sql);
        } catch (Exception he) {
            throw new HospitalizacionException(he.getMessage());
        }
    }

    @Override
    public int insertarMonitorDiasSinCapturar(MonitorInsertDiasSinCapturar monitorInsertDiasSinCapturar)
            throws HospitalizacionException {

        String sql = SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_INSERT;
       
        try {
        	
            return jdbcTemplate.update(sql, new Object[] { monitorInsertDiasSinCapturar.getCveDelegacionImss(),
                    monitorInsertDiasSinCapturar.getCvePresupuestal(), monitorInsertDiasSinCapturar.getFechaCaptura(),
                    monitorInsertDiasSinCapturar.getRefUnidadMedica(),
                    monitorInsertDiasSinCapturar.getRefDelegacionImss(),
                    monitorInsertDiasSinCapturar.getNumCeEspecialidad(),
                    monitorInsertDiasSinCapturar.getNumCeUrgencia(),
                    monitorInsertDiasSinCapturar.getNumEgresoHospitalizacion(),
                    monitorInsertDiasSinCapturar.getNumIntQuirurgicas(),
                    monitorInsertDiasSinCapturar.getNumEgresoUrgencia(),
                    monitorInsertDiasSinCapturar.getNumColorCespecialidad(),
                    monitorInsertDiasSinCapturar.getNumColorCeurgencia(),
                    monitorInsertDiasSinCapturar.getNumColorEgresohosp(),
                    monitorInsertDiasSinCapturar.getNumColorIntqx(),
                    monitorInsertDiasSinCapturar.getNumColorEgresourg(), monitorInsertDiasSinCapturar.getNumColor() });
        	
        	
        } catch (Exception he) {
            throw new HospitalizacionException(he.getMessage());
        }
        
    }
    
    @Override
    public boolean validarRegistroMonitoreo(String delegacionIMSS,String clavePresupuestal,Date fecCaptura ){
    	boolean resultado = false;
        Integer resul = jdbcTemplate.queryForObject(SQLConstants.MONITOREAR_DIAS_SIN_CAPTURAR_BUSCAR,
                new Object[] { delegacionIMSS, clavePresupuestal, fecCaptura }, Integer.class);

        if (resul > 0) {
            resultado = true;
        }
        return resultado;
    }

    private Map<Date, Integer> convierteListaMapa(List<Contador> lista) {

        Map<Date, Integer> map = new HashMap<Date, Integer>();

        for (Contador contador : lista) {
            map.put(contador.getFecha(), contador.getNumero());
        }

        logger.debug("convierteListaMap - MAP: " + map);

        return map;
    }

    public Boolean consultarDiaInhabil(Date fecha) {

        boolean resultado = false;
        Integer resul = jdbcTemplate.queryForObject(SQLConstants.CONSULTAR_DIA_INHABIL, new Object[] { fecha },
                Integer.class);

        if (resul > 0) {
            resultado = true;
        }
        return resultado;
    }

    public Boolean consultarUnidadHuesped(String refPersonalOperativo) {

        boolean resultado = false;
        Integer resul = jdbcTemplate.queryForObject(SQLConstants.CONSULTAR_UNIDAD_HUESPED,
                new Object[] { refPersonalOperativo }, Integer.class);

        if (resul > 0) {
            resultado = true;
        }
        return resultado;
    }
}
