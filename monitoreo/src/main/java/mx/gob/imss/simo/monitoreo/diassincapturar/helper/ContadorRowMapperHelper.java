package mx.gob.imss.simo.monitoreo.diassincapturar.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.monitoreo.common.constants.SQLColumnasConstants;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.Contador;

/**
 * @author jonathan.lopez
 */
public class ContadorRowMapperHelper extends BaseHelper implements RowMapper<Contador> {
	
	@Override
	public Contador mapRow(ResultSet resultSet, int rowId) throws SQLException {
		Contador contador = new Contador();
	
		contador.setFecha(resultSet.getDate(SQLColumnasConstants.FECHA));
		contador.setNumero(resultSet.getInt(SQLColumnasConstants.NUMERO));
		
		return contador;
	}
}
