package mx.gob.imss.simo.monitoreo.cierremes.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierreAutomaticoMes;
import mx.gob.imss.simo.monitoreo.common.constants.SQLColumnasConstants;

/**
 * @author jonathan.lopez
 */
public class UnidadMedicaCierreAutomaticoMesRowMapperHelper extends BaseHelper
        implements RowMapper<UnidadMedicaCierreAutomaticoMes> {

    @Override
    public UnidadMedicaCierreAutomaticoMes mapRow(ResultSet resultSet, int rowId) throws SQLException {

        UnidadMedicaCierreAutomaticoMes unidadMedicaCierreAutomaticoMes = new UnidadMedicaCierreAutomaticoMes();

        unidadMedicaCierreAutomaticoMes.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        unidadMedicaCierreAutomaticoMes.setCveTipoCaptura(resultSet.getShort(SQLColumnasConstants.CVE_TIPO_CAPTURA));
        unidadMedicaCierreAutomaticoMes.setCvePeriodoImss(resultSet.getString(SQLColumnasConstants.CVE_PERIODO_IMSS));
        unidadMedicaCierreAutomaticoMes.setFecInicial(resultSet.getDate(SQLColumnasConstants.FEC_INICIAL));
        unidadMedicaCierreAutomaticoMes.setFecFinal(resultSet.getDate(SQLColumnasConstants.FEC_FINAL));

        return unidadMedicaCierreAutomaticoMes;
    }

}
