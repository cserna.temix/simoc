package mx.gob.imss.simo.monitoreo.diassincapturar.model;

import java.util.Date;

import lombok.Data;

@Data
public class PeriodoDiasSinCapturar {

	private String cveDelegacionImss;
	private String desDelegacionImss;
	private String cvePresupuestal;
	private String desUnidadMedica;
	private short indEspecialidadCe;
	private short indUrgenciaCe;
	private short indEgresoHospital;
	private short indIntQuirurgica;
	private short indEgresoUrgencia;
	private Date fecInicial;
	private Date fecFinal;
}
