package mx.gob.imss.simo.monitoreo.cierremes.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "SIC_PERIODOS_IMSS")
public class SicPeriodoImss {

    @Id
    private ObjectId id;
    private Integer cvePeriodo;
    private Integer numAnioPeriodo;
    private String numMesPeriodo;
    private Date fecInicial;
    private Date fecFinal;
    private Date fecBaja;
}
