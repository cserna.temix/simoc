package mx.gob.imss.simo.monitoreo.cierremes.model;

/**
 * @author francisco.rodriguez
 */

public class DelegacionCierre {

    private String cveDelegacionImss;
    private String desDelegacionImss;
    private String color;

    /**
     * @return the cveDelegacionImss
     */
    public String getCveDelegacionImss() {

        return cveDelegacionImss;
    }

    /**
     * @param cveDelegacionImss
     *            the cveDelegacionImss to set
     */
    public void setCveDelegacionImss(String cveDelegacionImss) {

        this.cveDelegacionImss = cveDelegacionImss;
    }

    /**
     * @return the desDelegacionImss
     */
    public String getDesDelegacionImss() {

        return desDelegacionImss;
    }

    /**
     * @param desDelegacionImss
     *            the desDelegacionImss to set
     */
    public void setDesDelegacionImss(String desDelegacionImss) {

        this.desDelegacionImss = desDelegacionImss;
    }

    /**
     * @return the color
     */
    public String getColor() {

        return color;
    }

    /**
     * @param color
     *            the color to set
     */
    public void setColor(String color) {

        this.color = color;
    }

}
