package mx.gob.imss.simo.monitoreo.common.constants;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;

public class MonitoreoConstants extends BaseConstants{
	
    public static final Integer LONGITUD_AUTOCOMPLETE_PRESUPUESTAL = 12;

	public static final String REQUERIDO_PRESUPUESTAL = "Presupuestal";
	
}
