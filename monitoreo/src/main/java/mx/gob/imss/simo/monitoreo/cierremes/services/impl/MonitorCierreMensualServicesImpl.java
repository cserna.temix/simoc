package mx.gob.imss.simo.monitoreo.cierremes.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.ConfCaptura;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.monitoreo.cierremes.model.DelegacionCierre;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierre;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierreAutomaticoMes;
import mx.gob.imss.simo.monitoreo.cierremes.repository.MonitoreoCommonOracleRepository;
import mx.gob.imss.simo.monitoreo.cierremes.services.MonitorCierreMensualServices;

/**
 * @author francisco.rodriguez
 */
@Service("monitorCierreMensualServices")
public class MonitorCierreMensualServicesImpl extends MonitoreoCommonServicesImpl
        implements MonitorCierreMensualServices {

    @Autowired
    private MonitoreoCommonOracleRepository monitoreoCommonOracleRepository;

    @Override
    public List<DelegacionCierre> obtenerDelegacionCierre(String annio, String mes, String delegacion, Integer rol) {

        String cadena = "";
        if (rol < 4) {
            cadena = " AND DI.CVE_DELEGACION_IMSS = '" + delegacion + "' ";
        }
        cadena = cadena + "))  GROUP BY CVE_DELEGACION_IMSS, DES_DELEGACION_IMSS ORDER BY 2 ";

        return monitoreoCommonOracleRepository.obtenerDelegacionCierre(annio, mes, cadena);
    }

    @Override
    public List<UnidadMedicaCierre> obtenerUnidadesCierre(String annio, String mes, String delegacion,
            String cvePresupuestal, Integer rol, List<mx.gob.imss.simo.model.UnidadMedica> unidades) {

        StringBuilder cadena = new StringBuilder("");
        if (rol < 3 && unidades != null) {
            cadena.append(" AND (");
            int i = 0;
            for (mx.gob.imss.simo.model.UnidadMedica unidadMedica : unidades) {
                if (i != 0) {
                    cadena.append(" OR");
                }
                cadena.append(" UM.CVE_PRESUPUESTAL = '" + unidadMedica.getCvePresupuestal() + "' ");
                i++;
            }
            cadena.append(")");
        }
        logger.info("cadena:" + cadena);
        // if (banUnidad) {
        // cadena = " AND UM.CVE_PRESUPUESTAL = '" + cvePresupuestal + "' ";
        // }
        cadena.append(")  ORDER BY 3  ");
        logger.info("adena:" + cadena);
        // monitoreoCommonOracleRepository.insertaPeriodosFaltantes(annio.concat(mes), delegacion);

        return monitoreoCommonOracleRepository.obtenerUnidadesCierre(annio, mes, delegacion, cadena.toString());
    }

    @Override
    public String cambiarEstatusPeriodo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss,
            Integer indicadorPeriodo, String unidadMedica, Integer indActual, Integer indMonitoreo)
            throws HospitalizacionException {

        PeriodoOperacion periodoOperacion = getHospitalizacionCommonRepository().buscarPeriodoAbierto(clavePresupuestal,
                tipoCaptura, clavePeriodoImss);

        if (periodoOperacion == null) {
            insertarPeriodoOperacion(clavePresupuestal, tipoCaptura, clavePeriodoImss, indicadorPeriodo, indActual,
                    indMonitoreo);
        } else {
            if (indicadorPeriodo == 1) {
                cerrarPeriodo(clavePresupuestal, tipoCaptura, clavePeriodoImss);
            } else {
                abrirPeriodo(clavePresupuestal, tipoCaptura, clavePeriodoImss);
            }
            actualizarPeriodoConsultaExterna(clavePresupuestal, clavePeriodoImss, indicadorPeriodo, unidadMedica);
        }
        return null;
    }

    /**
     * Cierra el Periodo de la Unidad y tipo Captura Seleccionado
     * 
     * @param clavePresupuestal
     * @param tipoCaptura
     * @param clavePeriodoImss
     */
    private void cerrarPeriodo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss) {

        getHospitalizacionCommonRepository().cerrarPeriodo(clavePresupuestal, tipoCaptura, clavePeriodoImss);

    }

    /**
     * Abre el Periodo de la Unidad y tipo Captura Seleccionado
     * 
     * @param clavePresupuestal
     * @param tipoCaptura
     * @param clavePeriodoImss
     */
    private void abrirPeriodo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss) {

        getHospitalizacionCommonRepository().abrirPeriodo(clavePresupuestal, tipoCaptura, clavePeriodoImss);

    }

    /**
     * Inserta el Periodo de la Unidad y tipo Captura Seleccionado
     * 
     * @param clavePresupuestal
     * @param tipoCaptura
     * @param clavePeriodoImss
     * @return
     * @throws HospitalizacionException
     */
    private long insertarPeriodoOperacion(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss,
            Integer indCerrado, Integer indActual, Integer indMonitoreo) throws HospitalizacionException {

        return getHospitalizacionCommonRepository().insertarPeriodoOperacion(clavePresupuestal, tipoCaptura,
                clavePeriodoImss, indCerrado, indActual, indMonitoreo);
    }

    /**
     * @param clavePresupuestal
     * @param clavePeriodoImss
     * @param indicadorPeriodo
     * @throws HospitalizacionException
     */
    private void actualizarPeriodoConsultaExterna(String clavePresupuestal, String clavePeriodoImss,
            Integer indicadorPeriodo, String unidadMedica) throws HospitalizacionException {

        monitoreoCommonOracleRepository.actualizarPeriodoActualIMSS(clavePeriodoImss, clavePresupuestal, unidadMedica,
                "1", indicadorPeriodo);
    }

    @Override
    public void cerrarPeriodoMonitoreo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss) {

        monitoreoCommonOracleRepository.cerrarPeriodoMonitoreo(clavePresupuestal, tipoCaptura, clavePeriodoImss);
    }

    @Override
    public void abrirPeriodoMonitoreo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss) {

        monitoreoCommonOracleRepository.abrirPeriodoMonitoreo(clavePresupuestal, tipoCaptura, clavePeriodoImss);

    }

    @Override
    public Integer obtenerUltimoPeriodoPermitidoMonitoreo(String clavePresupuestal) {

        return monitoreoCommonOracleRepository.obtenerUltimoPeriodoPermitidoMonitoreo(clavePresupuestal);
    }

    @Override
    public Integer obtenerUltimoPeriodoPermitidoMonitoreoTipoCaptura(String clavePresupuestal, Integer tipoCaptura) {

        return monitoreoCommonOracleRepository.obtenerUltimoPeriodoPermitidoMonitoreoTipoCaptura(clavePresupuestal,
                tipoCaptura);
    }

    @Override
    public Integer obtenerPeriodoAbiertoMonitoreo(String clavePresupuestal) {

        return monitoreoCommonOracleRepository.obtenerPeriodoAbiertoMonitoreo(clavePresupuestal);
    }

    @Override
    public Integer obtenerPeriodoAbiertoMonitoreoTipoCaptura(String clavePresupuestal, Integer tipoCaptura) {

        return monitoreoCommonOracleRepository.obtenerPeriodoAbiertoMonitoreoTipoCaptura(clavePresupuestal,
                tipoCaptura);
    }

    @Override
    public Integer obtenerTotalPeriodosAbiertos(String clavePresupuestal, Integer tipoCaptura) {

        return monitoreoCommonOracleRepository.obtenerTotalPeriodosAbiertos(clavePresupuestal, tipoCaptura);
    }

    @Override
    public Integer obtenerUltimoPeriodoCerrado(String clavePresupuestal) {

        return monitoreoCommonOracleRepository.obtenerUltimoPeriodoCerrado(clavePresupuestal);
    }

    @Override
    public Integer obtenerUltimoPeriodoCerradoTipoCaptura(String clavePresupuestal, Integer tipoCaptura) {

        return monitoreoCommonOracleRepository.obtenerUltimoPeriodoCerradoTipoCaptura(clavePresupuestal, tipoCaptura);
    }

    @Override
    public List<UnidadMedicaCierreAutomaticoMes> obtenerUnidadesMedicasParaCierreAutomaticoDeMes() {

        return monitoreoCommonOracleRepository.obtenerUnidadesMedicasParaCierreAutomaticoDeMes();
    }

    @Override
    public List<ConfCaptura> obtenerConfUnidad(String cvePresupuestal) {

        return monitoreoCommonOracleRepository.obtenerConfUnidad(cvePresupuestal);
    }

    @Override
    public List<PeriodoOperacion> obtenerPeriodosAbiertos(String cvePresupuestal, Integer tipoCaptura) {

        return monitoreoCommonOracleRepository.obtenerPeriodosAbiertos(cvePresupuestal, tipoCaptura);
    }
    
    @Override
    public void insertaBitacoraPeriodo(String cvePresupuestal, Integer tipoCaptura, String periodoImss, Integer movMonitoreo, String personalOperativo){
    	
    	monitoreoCommonOracleRepository.insertarBitacoraPeriodo(cvePresupuestal, tipoCaptura, periodoImss, movMonitoreo, personalOperativo);
    	
    }

    @Override
    public List<String> filtrarCatalogoPresupuestal(List<String> datosCatalogoString, String query, Integer longitud) {

        List<String> filtroCatalogo = new ArrayList<String>();
        if (query.length() < longitud) {
            for (String dato : datosCatalogoString) {
                if (dato.toUpperCase().contains(query.toUpperCase())) {
                    filtroCatalogo.add(dato);
                }
            }
        }
        return filtroCatalogo;
    }
}
