/**
 * 
 */
package mx.gob.imss.simo.monitoreo.cierremes.model;

import lombok.Data;

@Data
/**
 * @author francisco.rodriguez
 */
public class UnidadMedicaCierre {

    private String cveDelegacion;
    private String cvePresupuestal;
    private String desUnidadMedica;
    private Integer consultaExterna;
    private Integer hospitalizacion;
    private Integer informaComplemen;
    private Integer servicioSub;
    private Boolean boolConsultaExterna;
    private Boolean boolHospitalizacion;
    private Boolean boolInformaComplemen;
    private Boolean boolServicioSub;
    private Boolean cambioConsultaExterna = Boolean.FALSE;
    private Boolean cambioHospitalizacion = Boolean.FALSE;
    private Boolean cambioInformaComplemen = Boolean.FALSE;
    private Boolean cambioServicioSub = Boolean.FALSE;
    private Boolean confActivaConsultaExterna = Boolean.FALSE;
    private Boolean confActivaHospitalizacion = Boolean.FALSE;
    private Boolean confActivaInformaComplemen = Boolean.FALSE;
    private Boolean confActivaServicioSub = Boolean.FALSE;
    private String color;

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("UnidadMedicaCierre [cveDelegacion=");
        builder.append(cveDelegacion);
        builder.append(", cvePresupuestal=");
        builder.append(cvePresupuestal);
        builder.append(", desUnidadMedica=");
        builder.append(desUnidadMedica);
        builder.append(", consultaExterna=");
        builder.append(consultaExterna);
        builder.append(", hospitalizacion=");
        builder.append(hospitalizacion);
        builder.append(", informaComplemen=");
        builder.append(informaComplemen);
        builder.append(", servicioSub=");
        builder.append(servicioSub);
        builder.append(", boolConsultaExterna=");
        builder.append(boolConsultaExterna);
        builder.append(", boolHospitalizacion=");
        builder.append(boolHospitalizacion);
        builder.append(", boolInformaComplemen=");
        builder.append(boolInformaComplemen);
        builder.append(", boolServicioSub=");
        builder.append(boolServicioSub);
        builder.append(", cambioConsultaExterna=");
        builder.append(cambioConsultaExterna);
        builder.append(", cambioHospitalizacion=");
        builder.append(cambioHospitalizacion);
        builder.append(", cambioInformaComplemen=");
        builder.append(cambioInformaComplemen);
        builder.append(", cambioServicioSub=");
        builder.append(cambioServicioSub);
        builder.append(", confActivaConsultaExterna=");
        builder.append(confActivaConsultaExterna);
        builder.append(", confActivaHospitalizacion=");
        builder.append(confActivaHospitalizacion);
        builder.append(", confActivaInformaComplemen=");
        builder.append(confActivaInformaComplemen);
        builder.append(", confActivaServicioSub=");
        builder.append(confActivaServicioSub);
        builder.append(", color=");
        builder.append(color);
        builder.append("]");
        return builder.toString();
    }

}
