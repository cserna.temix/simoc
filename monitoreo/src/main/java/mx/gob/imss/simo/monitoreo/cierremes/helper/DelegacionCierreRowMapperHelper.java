package mx.gob.imss.simo.monitoreo.cierremes.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.monitoreo.cierremes.model.DelegacionCierre;

/**
 * @author francisco.rodriguez
 */
public class DelegacionCierreRowMapperHelper extends BaseHelper implements RowMapper<DelegacionCierre> {

    @Override
    public DelegacionCierre mapRow(ResultSet resultSet, int rowId) throws SQLException {

        DelegacionCierre delegacionCierre = new DelegacionCierre();
        delegacionCierre.setCveDelegacionImss(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
        delegacionCierre.setDesDelegacionImss(resultSet.getString(SQLColumnasConstants.DES_DELEGACION_IMSS));
        delegacionCierre.setColor(resultSet.getString(SQLColumnasConstants.COLOR));
        return delegacionCierre;
    }

}
