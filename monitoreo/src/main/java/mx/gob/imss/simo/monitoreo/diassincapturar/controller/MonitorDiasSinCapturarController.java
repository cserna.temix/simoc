/**
 * MonitorCierreMensualController.java
 */
package mx.gob.imss.simo.monitoreo.diassincapturar.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.PagesCommonConstants;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.model.DatosSesion;
import mx.gob.imss.simo.model.Rol;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.DelegacionDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.UnidadMedicaDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.service.impl.MonitorDiasSinCapturarServices;

/**
 * @author jonathan.lopez
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ViewScoped
@ManagedBean(name = "monitorDiasSinCapturarController")
public class MonitorDiasSinCapturarController extends HospitalizacionCommonController {

    /**
     * 
     */
    private static final long serialVersionUID = -6993507170694549551L;

    @ManagedProperty("#{monitorDiasSinCapturarServices}")
    private transient MonitorDiasSinCapturarServices monitorDiasSinCapturarServices;

    private transient Integer rolUsuario;
    private String tieneFoco = "";
    private String fecha = "";
    private String mes = "";
    // private boolean renderTablaDelegcionUnidad = true;
    // private boolean renderTablaMonitor = false;
    private boolean renderTablaDelegcionUnidad = false;
    private boolean renderTablaMonitor = false;

    private transient List<DelegacionDiasSinCapturar> listDelegaciones;
    private transient DelegacionDiasSinCapturar delegacionDiasSinCapturar;
    private transient List<UnidadMedicaDiasSinCapturar> listUnidades;
    private transient UnidadMedicaDiasSinCapturar unidadMedicaDiasSinCapturar;
    private transient List<MonitorDiasSinCapturar> listaMonitorDiasSinCapturar;

    @ManagedProperty("#{reporteMonitoreoDiasSinCapturar}")
    private transient AbstractJasperReportsSingleFormatView plantillaReporteMonitoreoDiasSinCapturar;

    // roles
    private static final String ROLE_ADMIN_CENTRAL = "ROLE_ADMIN_CENTRAL";
    private static final String ROLE_ADMIN_DELEG = "ROLE_ADMIN_DELEG";
    private static final String ROLE_ARIMAC_JEFE = "ROLE_ARIMAC_JEFE";
    private static final String ROLE_ARIMAC_CAPTURA = "ROLE_ARIMAC_CAPTURA";

    private static final Integer ROLE_VALOR_ADMIN_CENTRAL = 4;
    private static final Integer ROLE_VALOR_ADMIN_DELEG = 3;
    private static final Integer ROLE_VALOR_ARIMAC_JEFE = 2;
    private static final Integer ROLE_VALOR_ARIMAC_CAPTURA = 1;

    @Override
    @PostConstruct
    public void init() {

        super.init();
        // tieneFoco = "monitorCierre:idMesAnnio"; //TODO: Cambiar por foto de pantalla de dias sin capturar
        listDelegaciones = new ArrayList<DelegacionDiasSinCapturar>();
        delegacionDiasSinCapturar = new DelegacionDiasSinCapturar();
        listUnidades = new ArrayList<UnidadMedicaDiasSinCapturar>();
        unidadMedicaDiasSinCapturar = new UnidadMedicaDiasSinCapturar();
        logger.info("Presupuestal: " + obtenerDatosUsuario().getCvePresupuestal());
        logger.info("Perfil: " + obtenerDatosUsuario().getPerfil());
        rolUsuario = obtenerRol();
        filtrarTablaMonitoreo(rolUsuario);
        // obtenerListDelegacion();
    }

    private void filtrarTablaMonitoreo(int rolUsuario) {

        if ((rolUsuario == ROLE_VALOR_ARIMAC_CAPTURA || rolUsuario == ROLE_VALOR_ARIMAC_JEFE)
                && getObjetosSs().getPersonalOperativo().getSdUnidadMedica().size() == 1) {
            obtenerMonitorDiasSinCapturarFiltro();
            obtenerMonitorDiasSinCapturar();
        } else {
            obtenerListDelegacion();
        }
    }

    private Integer obtenerRol() {

        List<Rol> roles = getObjetosSs().getPersonalOperativo().getSdRol();
        logger.info("Id:" + getObjetosSs().getPersonalOperativo().getId());
        logger.info("Usuarios:" + getObjetosSs().getPersonalOperativo().getRefCuentaActiveD());
        logger.info("Roles:" + getObjetosSs().getPersonalOperativo().getSdRol());
        Integer rolInt = 0;
        for (Rol rol : roles) {
            rol.getCveRol();
            switch (rol.getCveRol()) {
                case ROLE_ADMIN_CENTRAL:
                    rolInt = ROLE_VALOR_ADMIN_CENTRAL;
                    logger.info("El Role es ROLE_VALOR_ADMIN_CENTRAL");
                    break;
                case ROLE_ADMIN_DELEG:
                    rolInt = ROLE_VALOR_ADMIN_DELEG;
                    logger.info("El Role es ROLE_VALOR_ADMIN_DELEG");
                    break;
                case ROLE_ARIMAC_JEFE:
                    rolInt = ROLE_VALOR_ARIMAC_JEFE;
                    logger.info("El Role es ROLE_VALOR_ARIMAC_JEFE");
                    break;
                case ROLE_ARIMAC_CAPTURA:
                    rolInt = ROLE_VALOR_ARIMAC_CAPTURA;
                    logger.info("El Role es ROLE_VALOR_ARIMAC_CAPTURA");
                    break;
            }
            logger.info("ROL:" + rolInt);
            break;
        }
        return rolInt;

    }

    public void obtenerMonitorDiasSinCapturarFiltro() {

        boolean filtraDelegacion = true;
        boolean filtrarUnidadMedica = true;
        String cveDelegacion = obtenerDatosUsuario().getCveDelegacionUmae();
        listDelegaciones = monitorDiasSinCapturarServices.obtenerDelegacionDiasSinCapturar(cveDelegacion,
                filtraDelegacion);
        delegacionDiasSinCapturar = listDelegaciones.get(0);
        listUnidades = monitorDiasSinCapturarServices.obtenerUnidadesMedicasDiasSinCapturar(
                delegacionDiasSinCapturar.getCveDelegacionImss(),
                getObjetosSs().getPersonalOperativo().getSdUnidadMedica(), filtrarUnidadMedica);
        unidadMedicaDiasSinCapturar = listUnidades.get(0);

    }

    public void obtenerListDelegacion() {

        renderTablaDelegcionUnidad = true;
        renderTablaMonitor = false;
        boolean filtraDelegacion = true;
        String cveDelegacion = obtenerDatosUsuario().getCveDelegacionUmae();
        int rol = obtenerRol();

        if (rol == ROLE_VALOR_ADMIN_CENTRAL) {
            filtraDelegacion = false;
        }

        // Lista de Delegaciones filtradas con logica de dias sin capturar para mostrar el semaforo correspondiente
        listDelegaciones = monitorDiasSinCapturarServices.obtenerDelegacionDiasSinCapturar(cveDelegacion,
                filtraDelegacion);
    }

    public void obtenerListUnidadMedica() {

        renderTablaDelegcionUnidad = true;
        renderTablaMonitor = false;
        boolean filtrarUnidadMedica = false;
        int rol = obtenerRol();

        if (rol == ROLE_VALOR_ADMIN_CENTRAL || rol == ROLE_VALOR_ADMIN_DELEG) {
            filtrarUnidadMedica = false;
        }
        if (rol == ROLE_VALOR_ARIMAC_JEFE || rol == ROLE_VALOR_ARIMAC_CAPTURA) {
            filtrarUnidadMedica = true;
        }

        // Lista de Unidades Medicas filtradas con logica de dias sin capturar para mostrar el semaforo correspondiente
        listUnidades = monitorDiasSinCapturarServices.obtenerUnidadesMedicasDiasSinCapturar(
                delegacionDiasSinCapturar.getCveDelegacionImss(),
                getObjetosSs().getPersonalOperativo().getSdUnidadMedica(), filtrarUnidadMedica);
    }

    public void obtenerMonitorDiasSinCapturar() {

        renderTablaDelegcionUnidad = false;
        renderTablaMonitor = true;
        String cveDelegacion = delegacionDiasSinCapturar.getCveDelegacionImss(); // "03";
        String cvePresupuestal = unidadMedicaDiasSinCapturar.getCvePresupuestal(); // "030103022151";

        // Lista de Dias Sin Capturar por delegacion y unidad medica
        listaMonitorDiasSinCapturar = monitorDiasSinCapturarServices.obtenerMonitorDiasSinCapturar(cveDelegacion,
                cvePresupuestal);
    }

    @Override
    public String obtenerNombrePagina() {

        return PagesCommonConstants.MONITOREAR_DIAS_SIN_CAPTURAR;
    }

    @Override
    public void salir() throws IOException {

        DatosSesion datosSesion = getDatosSesionService().getDatosSesion(getObjetosSs().getUserInfo(),
                getObjetosSs().getUsername());
        datosSesion.setMostrarDialogoDelegacion(Boolean.FALSE);
        super.getDatosSesionService().save(datosSesion);

        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }

    public void regresarMonitoreo() {

        renderTablaDelegcionUnidad = true;
        renderTablaMonitor = false;
    }

    public String getFecha() {

        fecha = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        return fecha;
    }

    public String getMes() {

        mes = new SimpleDateFormat("MMMM", new Locale("es", "MX")).format(new Date());
        mes = mes.replaceFirst(String.valueOf(mes.charAt(0)), String.valueOf(mes.charAt(0)).toUpperCase());
        return mes;
    }

    public void procesarMonitorDiasSinCapturar() {

        try {
            monitorDiasSinCapturarServices.procesarMonitorDiasSinCapturar();
            logger.info(
                    "procesarMonitorDiasSinCapturar - Se completo de manera exitosa el proceso de llenado de monitoreo dias sin capturar");
        } catch (HospitalizacionException he) {
            logger.error("procesarMonitorDiasSinCapturar - Error Monitor Dias Sin Capturar: " + he.getMessage());
        }
    }

    public void fileXlsSpring() {

        logger.info("Inicio generar reporte MonitorDiasSinCapturar");

        renderTablaDelegcionUnidad = false;
        renderTablaMonitor = true;
        String cveDelegacion = delegacionDiasSinCapturar.getCveDelegacionImss();
        String cvePresupuestal = unidadMedicaDiasSinCapturar.getCvePresupuestal();

        // Lista de Dias Sin Capturar por delegacion y unidad medica
        listaMonitorDiasSinCapturar = monitorDiasSinCapturarServices.obtenerMonitorDiasSinCapturar(cveDelegacion,
                cvePresupuestal);

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        final Map<String, Object> model = new HashMap<>();
        final String logoImss = context.getRealPath(File.separator + "img" + File.separator + "imss_header.png");

        logger.info("fileXlsSpring obtenerDatosUsuario().getCvePresupuestal(): {"
                + obtenerDatosUsuario().getCvePresupuestal() + "}");
        model.put("logoImss", logoImss);
        model.put("refDelegacionImss", delegacionDiasSinCapturar.getRefDelegacionImss());
        model.put("mes", getMes());
        model.put("refUnidadMedica", unidadMedicaDiasSinCapturar.getRefUnidadMedica());
        model.put("fecha", getFecha());
        model.put("data", listaMonitorDiasSinCapturar);

        try {
            plantillaReporteMonitoreoDiasSinCapturar.render(model, (HttpServletRequest) context.getRequest(),
                    (HttpServletResponse) context.getResponse());
            logger.info("Fin generar reporte MonitorDiasSinCapturar");
        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

}
