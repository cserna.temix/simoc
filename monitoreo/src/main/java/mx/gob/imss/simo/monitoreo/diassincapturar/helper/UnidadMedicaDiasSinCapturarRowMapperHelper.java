package mx.gob.imss.simo.monitoreo.diassincapturar.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.monitoreo.common.constants.SQLColumnasConstants;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.UnidadMedicaDiasSinCapturar;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

/**
 * @author jonathan.lopez
 */
public class UnidadMedicaDiasSinCapturarRowMapperHelper extends BaseHelper implements RowMapper<UnidadMedicaDiasSinCapturar> {

	@Override
	public UnidadMedicaDiasSinCapturar mapRow(ResultSet resultSet, int rowId) throws SQLException {
		UnidadMedicaDiasSinCapturar unidadMedicaDiasSinCapturar = new UnidadMedicaDiasSinCapturar();
		
		unidadMedicaDiasSinCapturar.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
		unidadMedicaDiasSinCapturar.setRefUnidadMedica(resultSet.getString(SQLColumnasConstants.REF_UNIDAD_MEDICA));
		unidadMedicaDiasSinCapturar.setColor(resultSet.getString(SQLColumnasConstants.COLOR));
		
		return unidadMedicaDiasSinCapturar;
	}

}
