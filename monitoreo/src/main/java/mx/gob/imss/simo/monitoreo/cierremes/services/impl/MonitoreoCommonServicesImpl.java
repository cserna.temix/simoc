package mx.gob.imss.simo.monitoreo.cierremes.services.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.monitoreo.cierremes.model.SitPeriodoOperacion;
import mx.gob.imss.simo.monitoreo.cierremes.repository.MonitoreoCommonOracleRepository;
import mx.gob.imss.simo.monitoreo.cierremes.services.MonitoreoCommonServices;
import mx.gob.imss.simo.repository.MonitoreoCommonMongoRepository;

/**
 * @author aduer.macedo
 * @author jonathan.lopez
 */
@Service("monitoreoCommonServices")
public class MonitoreoCommonServicesImpl extends HospitalizacionCommonServicesImpl implements MonitoreoCommonServices {

    @Autowired
    private MonitoreoCommonOracleRepository monitoreoCommonOracleRepository;
    @Autowired
    private MonitoreoCommonMongoRepository monitoreoCommonMongoRepository;

    @Override
    public PeriodosImss obtenerPeriodoImss(String anioPeriodo, String mesPeriodo) {

        logger.info("anioPeriodo:" + anioPeriodo);
        logger.info("mesPeriodo:" + mesPeriodo);

        return monitoreoCommonOracleRepository.obtenerPeriodoImss(Integer.parseInt(anioPeriodo),
                Integer.parseInt(mesPeriodo));
    }

    @Override
    public void cerrarEInsertarPeriodoConsultaExternaMongoOracle(String cvePresupuestal, Integer cveTipoCaptura,
            String cvePeriodoImss, String periodoSiguienteS) throws Exception {

        if (cveTipoCaptura == 1) {
            logger.info("Corresponde a CONSULTA EXTERNA");
            // Oracle
            cerrarInsertarPeriodo(cvePresupuestal, cveTipoCaptura, cvePeriodoImss, periodoSiguienteS);
            // Mongo
            cerrarInsertarPeriodoConsultaExterna(cvePresupuestal, cveTipoCaptura, Integer.parseInt(cvePeriodoImss),
                    Integer.parseInt(periodoSiguienteS));
        } else {
            logger.info("Corresponde a OTRO TIPO DE CAPTURA");
            try {
                cerrarInsertarPeriodo(cvePresupuestal, cveTipoCaptura, cvePeriodoImss, periodoSiguienteS);
            } catch (Exception e) {
                logger.debug("e .:" + e.getMessage());
                throw e;
            }
        }
    }

    @Override
    public void cerrarEInsertarPeriodoConsultaExternaMongoOracleManual(String cvePresupuestal, Integer cveTipoCaptura,
            String cvePeriodoImss, String periodoSiguienteS) throws Exception {

        if (cveTipoCaptura == 1) {
            logger.info("Corresponde a CONSULTA EXTERNA");
            // Oracle
            cerrarInsertarPeriodoManual(cvePresupuestal, cveTipoCaptura, cvePeriodoImss, periodoSiguienteS);
            // Mongo
            cerrarInsertarPeriodoConsultaExterna(cvePresupuestal, cveTipoCaptura, Integer.parseInt(cvePeriodoImss),
                    Integer.parseInt(periodoSiguienteS));
        } else {
            logger.info("Corresponde a OTRO TIPO DE CAPTURA");
            try {
                cerrarInsertarPeriodoManual(cvePresupuestal, cveTipoCaptura, cvePeriodoImss, periodoSiguienteS);
            } catch (Exception e) {
                logger.debug("e .:" + e.getMessage());
                throw e;
            }
        }
    }

    /**
     * @param cvePresupuestal
     * @param cveTipoCaptura
     * @param cvePeriodoImss
     * @param periodoSiguienteS
     * @throws Exception
     */
    public void cerrarInsertarPeriodoConsultaExterna(String cvePresupuestal, Integer cveTipoCaptura,
            Integer cvePeriodoImss, Integer periodoSiguienteS) throws Exception {

        logger.trace("INICIA cerrarInsertarPeriodoConsultaExterna MONGO");
        logger.info("cvePresupuestal:" + cvePresupuestal + " cveTipoCaptura:" + cveTipoCaptura + "   cvePeriodoImss:"
                + cvePeriodoImss + "   periodoSiguienteS:" + periodoSiguienteS);
        // Buscar periodo a Cerrar Consulta EXterna
        SitPeriodoOperacion sp = monitoreoCommonMongoRepository.getPeriodoACerrar(cveTipoCaptura, cvePresupuestal,
                cvePeriodoImss);
        Integer existe = monitoreoCommonMongoRepository.existePeriodo(cveTipoCaptura, cvePresupuestal,
                periodoSiguienteS);
        if (sp != null) {

            logger.info("SE CIERRA PERIODO MONGO");
            // Se modifica el periodo para cerrarlo
            logger.info("sp:" + sp.toString());
            sp.setIndCierre(1);
            sp.setFecCierre(new Date());
            logger.info("new Date():" + new Date());
            sp.setIndMonitoreo(0);
            sp.setIndActual(0);
            logger.info("update CEX");
            monitoreoCommonMongoRepository.save(sp);
            // Se inserta el nuevo periodo

            if (existe < 1) {
                sp.setId(null);
                sp.setIndCierre(0);
                sp.setIndActual(1);
                sp.setFecCierre(null);
                sp.setCvePeriodo(periodoSiguienteS);
                logger.info("insertar CEX");
                monitoreoCommonMongoRepository.insert(sp);
            }

        } else {
            logger.info("cerrarInsertarPeriodoConsultaExterna - No existe ese periodo abierto");
        }

    }

    public SitPeriodoOperacion obtenerPeriodoAcerrar(String cvePresupuestal, Integer cveTipoCaptura,
            Integer cvePeriodoImss) throws Exception {

        return monitoreoCommonMongoRepository.getPeriodoACerrar(cveTipoCaptura, cvePresupuestal, cvePeriodoImss);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void cerrarInsertarPeriodo(String cvePresupuestal, Integer cveTipoCaptura, String cvePeriodoImss,
            String periodoSiguienteS) throws Exception {

        if (monitoreoCommonOracleRepository.buscarBitacora(cvePresupuestal, cvePeriodoImss, 5) == 0) {
            monitoreoCommonOracleRepository.insertarBitacoraProceso(cvePresupuestal, cvePeriodoImss, "5", "2", "1",
                    null);
        }

        logger.info("ENTRA cerrarInsertarPeriodo");
        try {
            logger.info("cerrando..");
            // Se cierran el periodo
            getHospitalizacionCommonRepository().cerrarPeriodo(cvePresupuestal, cveTipoCaptura, cvePeriodoImss);

            logger.info("insertando..");
            // insertar siguiente periodo operacion
            if (monitoreoCommonOracleRepository.existePeriodoOperacion(cvePresupuestal, periodoSiguienteS,
                    cveTipoCaptura) < 1) {
                monitoreoCommonOracleRepository.insertarPeriodoOperacionNuevo(cvePresupuestal, cveTipoCaptura,
                        periodoSiguienteS, 0, 1, 0);
                if (monitoreoCommonOracleRepository.buscarBitacora(cvePresupuestal, cvePeriodoImss, 5) > 0) {
                    monitoreoCommonOracleRepository.updateBitacora(cvePresupuestal, cvePeriodoImss, 5, 4,
                            Boolean.FALSE);
                }
            }

        } catch (Exception e) {

            logger.info("e:" + e);
            logger.info("e message:" + e.getMessage());
            logger.info("e getLocalizedMessage():" + e.getLocalizedMessage());
            StringBuilder mensajeE = new StringBuilder();
            mensajeE.append(" ERROR:" + new Date());
            mensajeE.append(" Preuspuestal:");
            mensajeE.append(cvePresupuestal);
            logger.info("cvePresupuestal:" + cvePresupuestal);
            logger.info("cvePeriodoImss:" + cvePeriodoImss);
            logger.info("5,3,1:");

            mensajeE.append(" Periodo:");
            mensajeE.append(cvePeriodoImss);
            mensajeE.append(" Excepcion:");
            if (null != e.getMessage() && e.getMessage().length() > 155) {
                mensajeE.append(e.getMessage().substring(0, 150));
            } else {
                mensajeE.append("Error en labae de datos");
            }

            monitoreoCommonOracleRepository.insertarBitacoraProceso(cvePresupuestal, cvePeriodoImss, "5", "3", "1",
                    mensajeE);

        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void cerrarInsertarPeriodoManual(String cvePresupuestal, Integer cveTipoCaptura, String cvePeriodoImss,
            String periodoSiguienteS) throws Exception {

        logger.info("ENTRA cerrarInsertarPeriodoManual");
        try {
            // Se comprueba que es ultimo periodo y es actual
            if (monitoreoCommonOracleRepository.existePeriodoOperacion(cvePresupuestal, periodoSiguienteS,
                    cveTipoCaptura) < 1) {
                logger.info("cerrando..");
                // Se cierran el periodo
                getHospitalizacionCommonRepository().cerrarPeriodo(cvePresupuestal, cveTipoCaptura, cvePeriodoImss);
                logger.info("insertando..");
                // insertar siguiente periodo operacion
                monitoreoCommonOracleRepository.insertarPeriodoOperacionNuevo(cvePresupuestal, cveTipoCaptura,
                        periodoSiguienteS, 0, 1, 0);
            } else {
                monitoreoCommonOracleRepository.cerrarPeriodoMonitoreo(cvePresupuestal, cveTipoCaptura, cvePeriodoImss);
            }

        } catch (Exception e) {
            logger.info("Falla en:" + e);
        }

    }

    @Override
    public PeriodosImss obtenerSiguientePeriodoImss(String periodo) throws Exception {

        return monitoreoCommonOracleRepository.obtenerSiguientePeriodoImss(periodo);
    }

    @Override
    public void actualizarPeriodoConsultaExternaMongo(SitPeriodoOperacion sitPeriodoOperacion) {

        monitoreoCommonMongoRepository.save(sitPeriodoOperacion);
    }

}
