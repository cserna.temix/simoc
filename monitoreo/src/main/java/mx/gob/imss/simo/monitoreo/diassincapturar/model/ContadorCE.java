package mx.gob.imss.simo.monitoreo.diassincapturar.model;

import java.util.Date;

import lombok.Data;

/**
 * 
 * @author jonathan.lopez
 *
 */
@Data
public class ContadorCE {

	private Integer year;
	private Integer month;
	private Integer day;
	private Integer numero;
}
