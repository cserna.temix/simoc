package mx.gob.imss.simo.monitoreo.cierremes.model;

import lombok.Data;

@Data
public class SdTipoCaptura {

    private Integer cveTipoCaptura;
    private String desTipoCaptura;

    public SdTipoCaptura() {
        super();
    }

    public SdTipoCaptura(Integer cveTipoCaptura, String desTipoCaptura) {
        super();
        this.setCveTipoCaptura(cveTipoCaptura);
        this.setDesTipoCaptura(desTipoCaptura);
    }

}
