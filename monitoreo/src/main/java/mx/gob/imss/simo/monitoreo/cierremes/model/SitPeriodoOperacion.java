package mx.gob.imss.simo.monitoreo.cierremes.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
// import org.mongodb.morphia.annotations.Entity;
// import org.mongodb.morphia.annotations.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "SIT_PERIODO_OPERACION")
public class SitPeriodoOperacion {

    @Id
    private ObjectId id;
    private Integer cvePeriodo;
    private List<SdTipoCaptura> sdTipoCaptura;
    private List<SdUnidadMedica> sdUnidadMedica;
    private Integer indCierre;
    private Date fecCierre;
    private Integer indMonitoreo;
    private Integer indActual;

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("SitPeriodoOperacion [id=");
        builder.append(id);
        builder.append(", cvePeriodo=");
        builder.append(cvePeriodo);
        builder.append(", sdTipoCaptura=");
        builder.append(sdTipoCaptura);
        builder.append(", sdUnidadMedica=");
        builder.append(sdUnidadMedica);
        builder.append(", indCierre=");
        builder.append(indCierre);
        builder.append(", fecCierre=");
        builder.append(fecCierre);
        builder.append(", indMonitoreo=");
        builder.append(indMonitoreo);
        builder.append(", indActual=");
        builder.append(indActual);
        builder.append("]");
        return builder.toString();
    }

}
