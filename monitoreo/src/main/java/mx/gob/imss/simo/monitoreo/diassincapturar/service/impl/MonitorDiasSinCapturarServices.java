package mx.gob.imss.simo.monitoreo.diassincapturar.service.impl;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.model.UnidadMedica;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.DelegacionDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.PeriodoDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.UnidadMedicaDiasSinCapturar;

/**
 * @author jonathan.lopez
 */
public interface MonitorDiasSinCapturarServices extends HospitalizacionCommonServices {

    /**
     * Obtiene el listado de delegaciones con la validacion de dias sin capturar para el semaforo requerido en la vista.
     * 
     * @param cveDelegacion
     * @param filtraDelegacion
     * @return
     */
    List<DelegacionDiasSinCapturar> obtenerDelegacionDiasSinCapturar(String cveDelegacion, boolean filtraDelegacion);

    /**
     * Obtiene el listado de unidades medicas por delegacion con la validacion de dias sin capturar para el semaforo
     * requerido en la vista.
     * 
     * @param cveDelegacion
     * @param cvePresupuestal
     * @param filtrarUnidadMedica
     * @return
     */
    List<UnidadMedicaDiasSinCapturar> obtenerUnidadesMedicasDiasSinCapturar(String cveDelegacion,
            List<UnidadMedica> cvePresupuestal, boolean filtrarUnidadMedica);

    /**
     * Obtiene el listado de dias sin capturar.
     * 
     * @param cveDelegacion
     * @param cvePresupuestal
     * @return
     */
    List<MonitorDiasSinCapturar> obtenerMonitorDiasSinCapturar(String cveDelegacion, String cvePresupuestal);

    /**
     * Metodo que permite realizar el procesamiento para el monitod de dias sin captura
     */
    void procesarMonitorDiasSinCapturar() throws HospitalizacionException;

    /**
     * Obtiene el listado de periodos por unidad medica, as� como la configuracion por campo que especifica si se
     * ejecutara o no el query para llenar la tabla de dias sin captura
     * 
     * @param
     * @return
     */
    List<PeriodoDiasSinCapturar> obtenerPeriodosDiasSinCapturar();

    /**
     * Consulta si un personal operativo tiene unidades m�dicas hu�spedes.
     * 
     * @param
     * @return
     */
    boolean consultarUnidadesHuesped(String refPersonalOperativo);

}
