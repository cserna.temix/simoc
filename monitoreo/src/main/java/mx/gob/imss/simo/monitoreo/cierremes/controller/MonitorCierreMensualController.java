/**
 * MonitorCierreMensualController.java
 */
package mx.gob.imss.simo.monitoreo.cierremes.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.BeanUtils;

import com.ibm.icu.util.Calendar;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;
import mx.gob.imss.simo.hospitalizacion.common.model.ConfCaptura;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.model.DatosSesion;
import mx.gob.imss.simo.model.Rol;
import mx.gob.imss.simo.monitoreo.cierremes.model.DelegacionCierre;
import mx.gob.imss.simo.monitoreo.cierremes.model.SitPeriodoOperacion;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierre;
import mx.gob.imss.simo.monitoreo.cierremes.services.MonitorCierreMensualServices;
import mx.gob.imss.simo.monitoreo.cierremes.services.MonitoreoCommonServices;
import mx.gob.imss.simo.monitoreo.cierremes.services.RealizarCierreMesAutomaticamenteServices;
import mx.gob.imss.simo.monitoreo.common.constants.MonitoreoConstants;

/**
 * @author francisco.rodriguez
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ViewScoped
@ManagedBean(name = "monitorCierreMensualController")
public class MonitorCierreMensualController extends HospitalizacionCommonController {

    /**
     *
     */
    private static final long serialVersionUID = -6993507170694549551L;

    // private String tieneFoco = "";
    private static final String BANDERA_ROJA = "fRojo";
    private static final String BANDERA_VERDE = "fVerde";
    private static final String FECHA_CERO = "00";

    // roles
    // private static final String ROLE_SUPREMO="ROLE_SUPREMO";
    private static final String ROLE_ADMIN_CENTRAL = "ROLE_ADMIN_CENTRAL";
    private static final String ROLE_ADMIN_DELEG = "ROLE_ADMIN_DELEG";
    private static final String ROLE_ARIMAC_JEFE = "ROLE_ARIMAC_JEFE";
    private static final String ROLE_ARIMAC_CAPTURA = "ROLE_ARIMAC_CAPTURA";

    private static Integer roleValorSuperUsuario = 5;
    private static Integer roleValorAdminCentral = 4;
    private static Integer roleValorAdminDeleg = 3;
    private static Integer roleValorArimacJefe = 2;
    private static Integer roleValorArimacCaptura = 1;
        
    private boolean renderTablaDelegcion = true;
    private transient String annio;
    private transient String mes;
    private transient String periodoSelStr;

    @ManagedProperty("#{monitorCierreMensualServices}")
    private transient MonitorCierreMensualServices monitorCierreMensualServices;
    @ManagedProperty("#{monitoreoCommonServices}")
    private transient MonitoreoCommonServices monitoreoCommonServices;

    @ManagedProperty("#{realizarCierreMesAutomaticamenteServices}")
    private transient RealizarCierreMesAutomaticamenteServices realizarCierreMesAutomaticamenteServices;

    transient List<DelegacionCierre> listDelegaciones;
    transient List<UnidadMedicaCierre> listUnidades;
    transient List<UnidadMedicaCierre> listUnidadesInicial;
    transient DelegacionCierre delegacionCierre;
    transient UnidadMedicaCierre unidadMedicaCierre;
    transient InputMask periodoIM;
    transient String delegacion;
    transient String mesString;
    transient String desDelegacion;

    transient String cveUnidadMedicaCierre;
    transient String cveTipoCaptura;
    transient Integer rolUsuario;
    transient Boolean boolTrue = Boolean.TRUE;
    
    transient AutoComplete autoCompletePresupuestal;
    transient List<String> listaPresupuestalAutocomplete;
    transient List<UnidadMedicaCierre> listaUnidadesAuxiliar;
    
    private CommandButton botonConfirmarSi;
    
    @Override
    @PostConstruct
    public void init() {

        super.init();
        tieneFoco = "monitorCierre:idMesAnnio";
        listDelegaciones = new ArrayList<>();
        listUnidades = new ArrayList<>();
        delegacionCierre = new DelegacionCierre();
        unidadMedicaCierre = new UnidadMedicaCierre();
        logger.info("INICIA validarPeriodoIM");
        logger.info("presupuestal" + obtenerDatosUsuario().getCvePresupuestal());
        logger.info("perfil" + obtenerDatosUsuario().getPerfil());
        rolUsuario = obtenerRol();
        tieneFoco = "idMonitoreoCierreMes:idMesanio";
        
        setAutoCompletePresupuestal(new AutoComplete());
        listaPresupuestalAutocomplete = new ArrayList<>();
        listaUnidadesAuxiliar = new ArrayList<>();
    }

    public void validarPeriodoIM() {

        logger.info("INICIA validarPeriodoIM");
        logger.info("presupuestal" + obtenerDatosUsuario().getCvePresupuestal());
        logger.info("perfil" + obtenerDatosUsuario().getPerfil());

        if (inputmaskLlena(periodoIM)) {
            String[] periodoArray = periodoIM.getValue().toString().split("/");
            int mesPeriodo = Integer.parseInt(periodoArray[0].trim());

            if (periodoArray[0].equals(FECHA_CERO) || mesPeriodo > 12) {
                periodoIM.resetValue();
                agregarMensajeError(getArray(MensajesErrorConstants.ME_109), null);
            } else {
                if (validaFecha(periodoArray[0], periodoArray[1])) { // valida fecha introducida mayor

                    mes = periodoArray[0];
                    annio = periodoArray[1];
                    periodoSelStr = annio + mes;

                    buscarDelegaciones();          
                } else {

                    resetPeriodo();
                    agregarMensajeError(getArray(MensajesErrorConstants.ME_120), null);
                }
            }
        } else {
            setTieneFoco(periodoIM.getClientId());
            resetPeriodo();
            agregarMensajeError(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarEgresoConstants.REQUERIDO_MES_ANIO));
        }
    }

    private Integer obtenerRol() {

        List<Rol> roles = getObjetosSs().getPersonalOperativo().getSdRol();
        Integer rolInt = 0;
        for (Rol rol : roles) {
            rol.getCveRol();
            logger.info("Algun rol:" + rol.getCveRol());
            logger.info("Algun rol descrip :" + rol.getDesRol());
            switch (rol.getCveRol()) {
                case ROLE_ADMIN_CENTRAL:
                    rolInt = roleValorAdminCentral;
                    logger.info("el Role es ROLE_VALOR_ADMIN_CENTRAL");
                    break;
                case ROLE_ADMIN_DELEG:
                    rolInt = roleValorAdminDeleg;
                    logger.info("el Role es ROLE_VALOR_ADMIN_DELEG");
                    break;
                case ROLE_ARIMAC_JEFE:
                    rolInt = roleValorArimacJefe;
                    logger.info("el Role es ROLE_VALOR_ARIMAC_JEFE");
                    break;
                case ROLE_ARIMAC_CAPTURA:
                    rolInt = roleValorArimacCaptura;
                    logger.info("el Role es ROLE_VALOR_ARIMAC_CAPTURA");
                    break;
            }
            logger.info("ROL:" + rolInt);
            break;
        }
        return rolInt;

    }

    public void buscarDelegaciones() {

        // Lista de Delegaciones
        listDelegaciones = monitorCierreMensualServices.obtenerDelegacionCierre(annio, mes,
                obtenerDatosUsuario().getCveDelegacionUmae(), rolUsuario);

    }

    private UnidadMedicaCierre transformarIntToBool(UnidadMedicaCierre unidad) {

        asignarValoresBoolCex(unidad);
        asignarValoresBoolHospi(unidad);
        asignarValoresBoolServSub(unidad);
        asignarValoresBoolInfoCom(unidad);

        return unidad;
    }

    private void asignarValoresBoolHospi(UnidadMedicaCierre unidad) {

        if (unidad.getHospitalizacion() != null && unidad.getHospitalizacion() != 2) {
            if (unidad.getHospitalizacion() == 1) {
                unidad.setBoolHospitalizacion(Boolean.TRUE);
            } else {
                unidad.setBoolHospitalizacion(Boolean.FALSE);
            }
        } else {
            unidad.setBoolHospitalizacion(null);
        }
    }

    private void asignarValoresBoolCex(UnidadMedicaCierre unidad) {

        if (unidad.getConsultaExterna() != null && unidad.getConsultaExterna() != 2) {
            if (unidad.getConsultaExterna() == 1) {
                unidad.setBoolConsultaExterna(Boolean.TRUE);
            } else {
                unidad.setBoolConsultaExterna(Boolean.FALSE);
            }
        } else {
            unidad.setBoolConsultaExterna(null);
        }
    }

    private void asignarValoresBoolServSub(UnidadMedicaCierre unidad) {

        if (unidad.getServicioSub() != null && unidad.getServicioSub() != 2) {
            if (unidad.getServicioSub() == 1) {
                unidad.setBoolServicioSub(Boolean.TRUE);
            } else {
                unidad.setBoolServicioSub(Boolean.FALSE);
            }
        } else {
            unidad.setBoolServicioSub(null);
        }
    }

    private void asignarValoresBoolInfoCom(UnidadMedicaCierre unidad) {

        if (unidad.getInformaComplemen() != null && unidad.getInformaComplemen() != 2) {
            if (unidad.getInformaComplemen() == 1) {
                unidad.setBoolInformaComplemen(Boolean.TRUE);
            } else {
                unidad.setBoolInformaComplemen(Boolean.FALSE);
            }
        } else {
            unidad.setBoolInformaComplemen(null);
        }
    }

    private void transformarListaIntToBool(List<UnidadMedicaCierre> listUnidades) {

        for (UnidadMedicaCierre unidad : listUnidades) {

            if (unidad != null) {
                unidad = transformarIntToBool(unidad);
                // String color;
                boolean cex = (unidad.getBoolConsultaExterna() == null && !unidad.getConfActivaConsultaExterna())
                        || (unidad.getBoolConsultaExterna() != null && unidad.getBoolConsultaExterna());
                boolean hospi = (unidad.getBoolHospitalizacion() == null && !unidad.getConfActivaHospitalizacion())
                        || (unidad.getBoolHospitalizacion() != null && unidad.getBoolHospitalizacion());
                boolean infoCom = (unidad.getBoolInformaComplemen() == null && !unidad.getConfActivaInformaComplemen())
                        || (unidad.getBoolInformaComplemen() != null && unidad.getBoolInformaComplemen());
                boolean servSub = (unidad.getBoolServicioSub() == null && !unidad.getConfActivaServicioSub())
                        || (unidad.getBoolServicioSub() != null && unidad.getBoolServicioSub());

                unidad.getCvePresupuestal();
                int i = listUnidades.indexOf(unidad);
                listUnidades.get(i).setColor(evaluarColor(cex, hospi, infoCom, servSub));

                evaluarPermisosEdicion(i, unidad, 1);
                evaluarPermisosEdicion(i, unidad, 2);
                evaluarPermisosEdicion(i, unidad, 3);
                evaluarPermisosEdicion(i, unidad, 4);

            }
        }
    }

    private String evaluarColor(boolean cex, boolean hospi, boolean infoCom, boolean servSub) {

        if (cex && hospi && infoCom && servSub) {
            return BANDERA_VERDE;
        } else {
            return BANDERA_ROJA;
        }
    }

    private void evaluarPermisosEdicion(int i, UnidadMedicaCierre unidad, Integer tipoCaptura) {

        // SI EL ROLE ES MENOR A ROLE_SUPER_USUARIO
        // logger.info("-------------------------------INICIA evaluarPermisosEdicion");
        // logger.info("rolUsuario:" + rolUsuario);
        //
        // logger.info("periodoSelStr----:" + periodoSelStr);
        // logger.info("unidad----:" + unidad.getCvePresupuestal());
        if (!Objects.equals(rolUsuario, roleValorAdminCentral)) {
            inhabilitarCampos(i, 3);
        } else {

            Integer periodoPermitidoMinimo = monitorCierreMensualServices
                    .obtenerUltimoPeriodoPermitidoMonitoreoTipoCaptura(unidad.getCvePresupuestal(), tipoCaptura);
            // logger.info("periodoPermitidoMinimo:" + periodoPermitidoMinimo);
            if (periodoPermitidoMinimo == null) {
                // periodoPermitidoMinimo = monitorCierreMensualServices
                // .obtenerUltimoPeriodoCerradoTipoCaptura(unidad.getCvePresupuestal(),tipoCaptura);
                inhabilitarCamposTipoCaptura(i, 3, tipoCaptura);
            } else {
                if (periodoSelStr != null && periodoPermitidoMinimo != null) {
                    if (Integer.valueOf(periodoSelStr) > periodoPermitidoMinimo) {
                        inhabilitarCamposTipoCaptura(i, 3, tipoCaptura);
                    }
                }
            }

        }
    }

    private void inhabilitarCampos(int i, int comportamiento) {

        listUnidades.get(i).setConsultaExterna(comportamiento);
        listUnidades.get(i).setHospitalizacion(comportamiento);
        listUnidades.get(i).setInformaComplemen(comportamiento);
        listUnidades.get(i).setServicioSub(comportamiento);
    }

    private void inhabilitarCamposTipoCaptura(int i, int comportamiento, int tipoCaptura) {

        switch (tipoCaptura) {

            case 1:
                listUnidades.get(i).setConsultaExterna(comportamiento);
                break;
            case 2:
                listUnidades.get(i).setHospitalizacion(comportamiento);
                break;
            case 3:
                listUnidades.get(i).setInformaComplemen(comportamiento);
                break;
            case 4:
                listUnidades.get(i).setServicioSub(comportamiento);
                break;
            default:
                break;
        }
    }

    public void buscarUnidadMedica() {

        List<mx.gob.imss.simo.model.UnidadMedica> unidades = new ArrayList<>();
        // if (rolUsuario >= roleValorArimacJefe) {
        unidades = getObjetosSs().getPersonalOperativo().getSdUnidadMedica();
        // }

        // Lista de Delegaciones
        listUnidades = monitorCierreMensualServices.obtenerUnidadesCierre(annio, mes, delegacion,
                obtenerDatosUsuario().getCvePresupuestal(), rolUsuario, unidades);
        
        listaPresupuestalAutocomplete.clear();
        getListUnidades().forEach(
    			(o) -> listaPresupuestalAutocomplete.add(o.getDesUnidadMedica()));
        listaUnidadesAuxiliar = listUnidades;
        // for (UnidadMedicaCierre unidads : listUnidades) {
        // logger.info("ANTES unidads:" + unidads.toString());
        // List<ConfCaptura> confs=monitorCierreMensualServices.obtenerConfUnidad(unidads.getCvePresupuestal());
        // int i=listUnidades.indexOf(unidads);
        // logger.info("i:" + i);
        // for(ConfCaptura con: confs) {
        // logger.info("con:"+con.toString());
        // switch(con.getCveTipoCaptura()) {
        // case 1:
        // listUnidades.get(i).setConfActivaConsultaExterna(validarConfCaptura(con.getCveTipoCaptura(),
        // con.getIndConfActiva(), 1));
        // break;
        // case 2:
        // listUnidades.get(i).setConfActivaHospitalizacion(validarConfCaptura(con.getCveTipoCaptura(),
        // con.getIndConfActiva(), 2));
        // break;
        // case 3:
        // listUnidades.get(i).setConfActivaInformaComplemen(validarConfCaptura(con.getCveTipoCaptura(),
        // con.getIndConfActiva(), 3));
        // break;
        // case 4:
        // listUnidades.get(i).setConfActivaServicioSub(validarConfCaptura(con.getCveTipoCaptura(),
        // con.getIndConfActiva(), 4));
        // break;
        //
        // }
        //
        //
        //
        //
        //
        // }
        // logger.info("DESPUES:"+unidads.toString());
        //
        // }
    }

    private Boolean validarConfCaptura(Integer tipoCaptura, Integer activa, Integer servicioCaptura) {

        Boolean valido = Boolean.FALSE;
        logger.info("tipoCaptura:" + tipoCaptura);
        logger.info("servicioCaptura:" + servicioCaptura);
        logger.info("activa:" + activa);
        if (tipoCaptura == servicioCaptura) {
            if (activa == 1) {
                valido = Boolean.TRUE;
            } else {
                valido = Boolean.FALSE;
            }
        }
        return valido;
    }

    // private void actualizarCierre() {
    //
    // String cvePresupuestal = "030103022151";// Unidad Seleccionada
    // Integer tipoCaptura = 2;// -1 Consulta externa -2 Hospitalización -3 Información complementaria -4 Servicios
    // // subrogados
    // Integer indicadorPeriodo = 1;// -1 Cerrado -2 Abierto
    // try {
    // PeriodosImss periodoImss = monitorCierreMensualServices.obtenerPeriodoImss(annio, mes);
    // monitorCierreMensualServices.cambiarEstatusPeriodo(cvePresupuestal, tipoCaptura,
    // periodoImss.getClavePeriodo(), indicadorPeriodo, "1", 1, 1);
    // buscarUnidadMedica();
    // } catch (HospitalizacionException e) {
    // logger.error(ERROR_MOITOR + e.getMessage(), e);
    // agregarMensajeError(getArray(MensajesErrorConstants.ME_501), null);
    // }
    // }
    @Override
    public String obtenerNombrePagina() {

        return null;
    }

    public void salir() throws IOException {

        DatosSesion datosSesion = getDatosSesionService().getDatosSesion(getObjetosSs().getUserInfo(),
                getObjetosSs().getUsername());
        datosSesion.setMostrarDialogoDelegacion(Boolean.FALSE);
        super.getDatosSesionService().save(datosSesion);

        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }

    public void selectDelegacion(SelectEvent event) {

        logger.info("delegacion:" + ((DelegacionCierre) event.getObject()).getCveDelegacionImss());
        mesString = descripMes(Integer.valueOf(mes));
        delegacion = delegacionCierre.getCveDelegacionImss();
        desDelegacion = delegacionCierre.getDesDelegacionImss();
        actualizarListaUndadesPorDelegacion();
        setTieneFoco(getAutoCompletePresupuestal().getClientId());
    }

    private void actualizarListaUndadesPorDelegacion() {

        buscarUnidadMedica();

        for (UnidadMedicaCierre unidad : listUnidades) {
            logger.info("actualizarListaUndadesPorDelegacion unidad  ESCOGE:" + unidad);

        }
        listUnidadesInicial = null;
        listUnidadesInicial = new ArrayList<>();

        for (UnidadMedicaCierre uni : listUnidades) {
            UnidadMedicaCierre uC = new UnidadMedicaCierre();
            BeanUtils.copyProperties(uni, uC);
            listUnidadesInicial.add(uC);
        }

        for (UnidadMedicaCierre unidads : listUnidades) {
            logger.info("unidads:" + unidads.toString());
            List<ConfCaptura> confs = monitorCierreMensualServices.obtenerConfUnidad(unidads.getCvePresupuestal());
            int i = listUnidades.indexOf(unidads);
            for (ConfCaptura con : confs) {

                switch (con.getCveTipoCaptura()) {
                    case 1:
                        listUnidades.get(i).setConfActivaConsultaExterna(
                                validarConfCaptura(con.getCveTipoCaptura(), con.getIndConfActiva(), 1));
                        break;
                    case 2:
                        listUnidades.get(i).setConfActivaHospitalizacion(
                                validarConfCaptura(con.getCveTipoCaptura(), con.getIndConfActiva(), 2));
                        break;
                    case 3:
                        listUnidades.get(i).setConfActivaInformaComplemen(
                                validarConfCaptura(con.getCveTipoCaptura(), con.getIndConfActiva(), 3));
                        break;
                    case 4:
                        listUnidades.get(i).setConfActivaServicioSub(
                                validarConfCaptura(con.getCveTipoCaptura(), con.getIndConfActiva(), 4));
                        break;

                }
                // switch(con.getCveTipoCaptura()) {
                // Boolean activo=Boolean.FALSE;
                // case 1:
                // activo=Boolean.FALSE;
                // activo=validarConfCaptura(con.getCveTipoCaptura(), con.getIndConfActiva(), 1);
                // listUnidades.get(i).setConfActivaConsultaExterna(activo);
                // if(!activo) {
                // listUnidades.get(i).setBoolConsultaExterna(!activo);
                // }
                // break;
                // case 2:
                // activo=Boolean.FALSE;
                // activo=validarConfCaptura(con.getCveTipoCaptura(), con.getIndConfActiva(), 2);
                // listUnidades.get(i).setConfActivaHospitalizacion(activo);
                // if(!activo) {
                // listUnidades.get(i).setBoolHospitalizacion(!activo);
                // }
                // break;
                // case 3:
                // activo=Boolean.FALSE;
                // activo=validarConfCaptura(con.getCveTipoCaptura(), con.getIndConfActiva(), 3);
                // listUnidades.get(i).setConfActivaInformaComplemen(activo);
                // if(!activo) {
                // listUnidades.get(i).setBoolInformaComplemen(!activo);
                // }
                // break;
                // case 4:
                //
                // activo=validarConfCaptura(con.getCveTipoCaptura(), con.getIndConfActiva(), 4);
                // listUnidades.get(i).setConfActivaServicioSub(activo);
                // if(!activo) {
                // listUnidades.get(i).setBoolServicioSub(!activo);
                // }
                //
                // break;
                //
                // }

            }

        }
        transformarListaIntToBool(listUnidades);
        for (UnidadMedicaCierre unidads : listUnidades) {
            logger.info("IMPRIME unidads:" + unidads.toString());
        }

        renderTablaDelegcion = false;
    }

    public void modificarParametros() {

        cveUnidadMedicaCierre = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                .get("paramUnidad");
        cveTipoCaptura = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                .get("tipoCaptura");

        logger.debug("PARAMETRO modificarParametros: cveUnidadMedicaCierre:" + cveUnidadMedicaCierre + "cveTipoCaptura:"
                + cveTipoCaptura);
        Integer tipoC = Integer.valueOf(cveTipoCaptura);
        // Integer periodoAbiertoMonitoreoTipoCaptura =
        // monitorCierreMensualServices.obtenerTotalPeriodosAbiertos(cveUnidadMedicaCierre,tipoC);
        List<PeriodoOperacion> periodosAbiertos = monitorCierreMensualServices
                .obtenerPeriodosAbiertos(cveUnidadMedicaCierre, tipoC);
        int periodosAbiertosDif = 0;
        for (PeriodoOperacion po : periodosAbiertos) {
            if (periodosAbiertos.size() >= 2) {
                if (!po.getClavePeriodoIMSS().equals(periodoSelStr)) {
                    periodosAbiertosDif = periodosAbiertosDif + 1;
                }
            }
        }

        // logger.debug("periodoAbiertoMonitoreoTipoCaptura:"+periodoAbiertoMonitoreoTipoCaptura+"
        // cveTipoCaptura:"+cveTipoCaptura);

        if (periodosAbiertosDif >= 2) {

            bloquearCambioUnidadPerido();

        } else {
            gestionUnidadMedica(Boolean.TRUE);
        }

    }

    public void regresarMonitoreo() {

        renderTablaDelegcion = true;
        setTieneFoco(periodoIM.getClientId());
    }

    public void cancelarMonitoreo() {

        actualizarListaUndadesPorDelegacion();
    }

    public void confirmaCierreSi() throws NumberFormatException, Exception {

        agregarMensajeError(getArray(MensajesGeneralesConstants.MG_002), null);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('cierreUnidadConfirm').hide();");
        guardarCambiosUnidades();
        if (rolUsuario.equals(roleValorSuperUsuario)) {

            logger.info("Si es SUPER... USUARIO");
        }
		setTieneFoco(autoCompletePresupuestal.getClientId());
    }

    public void confirmaCierreNo() {

        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('cierreUnidadConfirm').hide();");
		setTieneFoco(autoCompletePresupuestal.getClientId());
    }

    public void confirfmaGuardarCambios() {

        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('cierreUnidadConfirm').show();");
        setTieneFoco(botonConfirmarSi.getClientId());
    }

    public void guardarCambiosUnidades() throws NumberFormatException, Exception {

        logger.info("EL PERIODO ES:" + periodoSelStr);

        for (UnidadMedicaCierre unidad : listUnidades) {

            Boolean cambio = Boolean.FALSE;
            logger.info("guardar: getCambioConsultaExterna:" + unidad.getCambioConsultaExterna());
            logger.info("guardar: getCambioHospitalizacion:" + unidad.getCambioHospitalizacion());
            logger.info("guardar: getCambioServicioSub:" + unidad.getCambioServicioSub());
            logger.info("guardar: getCambioInformaComplemen:" + unidad.getCambioInformaComplemen());
            // consulta Externa
            if (unidad.getCambioConsultaExterna()) {
                logger.info("CEX CAMBIA:" + periodoSelStr);
                cambio = Boolean.TRUE;
                abrirCerrarPeriodos(unidad.getCvePresupuestal(), 1, unidad.getBoolConsultaExterna(), periodoSelStr);
                abrirCerrarPeriodosMongo(unidad.getCvePresupuestal(), 1, unidad.getBoolConsultaExterna(),
                        periodoSelStr);
            }

            // demas servicios
            if (unidad.getCambioHospitalizacion()) {
                logger.info("HOSP CAMBIA :" + periodoSelStr);
                cambio = Boolean.TRUE;
                abrirCerrarPeriodos(unidad.getCvePresupuestal(), 2, unidad.getBoolHospitalizacion(), periodoSelStr);
            }
            if (unidad.getCambioInformaComplemen()) {
                logger.info("INFOCompl CAMBIA :" + periodoSelStr);
                cambio = Boolean.TRUE;
                abrirCerrarPeriodos(unidad.getCvePresupuestal(), 3, unidad.getBoolInformaComplemen(), periodoSelStr);
            }
            if (unidad.getCambioServicioSub()) {
                logger.info("ServSub CAMBIA :" + periodoSelStr);
                cambio = Boolean.TRUE;
                abrirCerrarPeriodos(unidad.getCvePresupuestal(), 4, unidad.getBoolServicioSub(), periodoSelStr);
            }
            if (cambio) {
                logger.info("GUARDAR ESTE CAMBIO:" + unidad.getCvePresupuestal());
            } else {
                logger.info("NO cambia:" + unidad.getCvePresupuestal());
            }

            logger.info("GUARDADO");

        }
        actualizarListaUndadesPorDelegacion();

    }

    private void abrirCerrarPeriodos(String cvePresupuestal, Integer tipoCaptura, Boolean valorTipoCaptura,
            String periodoImss) throws Exception {

        logger.info("--cvePresupuestal:" + cvePresupuestal);
        logger.info("tipoCaptura:" + tipoCaptura);
        logger.info("valorTipoCaptura:" + valorTipoCaptura);
        logger.info("periodoImss:" + periodoImss);
        if (tipoCaptura != null && valorTipoCaptura != null) {

            logger.info("----unidad GUARDAR :");
            if (valorTipoCaptura) {
                // Cerrar
                PeriodosImss periodoSiguiente = monitoreoCommonServices.obtenerSiguientePeriodoImss(periodoImss);
                monitoreoCommonServices.cerrarEInsertarPeriodoConsultaExternaMongoOracleManual(cvePresupuestal,
                        tipoCaptura, periodoImss, periodoSiguiente.getClavePeriodo());
                
                monitorCierreMensualServices.insertaBitacoraPeriodo(cvePresupuestal, tipoCaptura, periodoImss, 1, obtenerDatosUsuario().getUsuario());

                // monitorCierreMensualServices.cerrarPeriodoMonitoreo(cvePresupuestal, tipoCaptura, periodoImss);
            } else {
                // Abrir
                monitorCierreMensualServices.abrirPeriodoMonitoreo(cvePresupuestal, tipoCaptura, periodoImss);
                
                monitorCierreMensualServices.insertaBitacoraPeriodo(cvePresupuestal, tipoCaptura, periodoImss, 0, obtenerDatosUsuario().getUsuario());
            }
        } else {
            logger.info("----unidad GUARDAR NO cambio:");
        }
    }

    private void abrirCerrarPeriodosMongo(String cvePresupuestal, Integer tipoCaptura, Boolean valorTipoCaptura,
            String periodoImss) throws NumberFormatException, Exception {

        SitPeriodoOperacion sp = monitoreoCommonServices.obtenerPeriodoAcerrar(cvePresupuestal, tipoCaptura,
                Integer.valueOf(periodoImss));
        if (sp != null) {
            if (tipoCaptura != null) {

                logger.info("----unidad GUARDAR :");
                if (valorTipoCaptura) {
                    // Cerrar
                    sp.setIndCierre(1);
                    sp.setFecCierre(new Date());
                    sp.setIndActual(0);
                    sp.setIndMonitoreo(0);
                } else {

                    // Abrir
                    sp.setIndCierre(0);
                    sp.setFecCierre(null);
                    sp.setIndActual(0);
                    sp.setIndMonitoreo(1);
                }
                // cerar service que comunique al repository
                monitoreoCommonServices.actualizarPeriodoConsultaExternaMongo(sp);
            } else {
                logger.info("----unidad GUARDAR NO cambio:");
            }
        }
    }

    private void gestionUnidadMedica(Boolean aceptarBool) {

        logger.info("INICIA gestionUnidadMedica");
        logger.info(" checkvbox unidad:" + cveUnidadMedicaCierre);
        logger.info(" tipoCaptura:" + cveTipoCaptura);

        for (UnidadMedicaCierre unidad : listUnidades) {

            if (unidad != null && unidad.getCvePresupuestal().equals(cveUnidadMedicaCierre)) {
                logger.info(" la unidad a camabiar es:" + unidad.toString());
                // String color;
                // String colorAnterior = unidad.getColor();
                if (!aceptarBool) {
                    unidad = setBoolTipoCaptura(cveTipoCaptura, unidad);

                }
                boolean cex = (!unidad.getConfActivaConsultaExterna())
                        || (unidad.getBoolConsultaExterna() != null && unidad.getBoolConsultaExterna());
                boolean hospi = (!unidad.getConfActivaHospitalizacion())
                        || (unidad.getBoolHospitalizacion() != null && unidad.getBoolHospitalizacion());
                boolean infoCom = (!unidad.getConfActivaInformaComplemen())
                        || (unidad.getBoolInformaComplemen() != null && unidad.getBoolInformaComplemen());
                boolean servSub = (!unidad.getConfActivaServicioSub())
                        || (unidad.getBoolServicioSub() != null && unidad.getBoolServicioSub());

                switch (cveTipoCaptura) {
                    case "1":

                        unidad.setCambioConsultaExterna(Boolean.TRUE);
                        break;
                    case "2":

                        unidad.setCambioHospitalizacion(Boolean.TRUE);
                        break;
                    case "3":

                        unidad.setCambioInformaComplemen(Boolean.TRUE);
                        break;
                    case "4":

                        unidad.setCambioServicioSub(Boolean.TRUE);
                        break;
                }
                logger.info("unidad: " + unidad.getCvePresupuestal());
                logger.info("unidad: getCambioConsultaExterna:" + unidad.getCambioConsultaExterna());
                logger.info("unidad: getCambioHospitalizacion:" + unidad.getCambioHospitalizacion());
                logger.info("unidad: getCambioServicioSub:" + unidad.getCambioServicioSub());
                logger.info("unidad: getCambioInformaComplemen:" + unidad.getCambioInformaComplemen());

                int i = listUnidades.indexOf(unidad);
                listUnidades.get(i).setColor(evaluarColor(cex, hospi, infoCom, servSub));

            }
        }

    }

    private void bloquearCambioUnidadPerido() {

        logger.info("INICIA bloquearCambioUnidadPerido");

        for (UnidadMedicaCierre unidad : listUnidades) {

            if (unidad != null && unidad.getCvePresupuestal().equals(cveUnidadMedicaCierre)) {
                logger.info(" la unidad a camabiar es:" + unidad.toString());
                Boolean mandarMensaje = Boolean.TRUE;
                // String color;
                // String colorAnterior = unidad.getColor();
                unidad = setBoolTipoCaptura(cveTipoCaptura, unidad);
                if (cveTipoCaptura.equals("1") && !unidad.getBoolConsultaExterna()) {
                    logger.info("unidad.getBoolConsultaExterna():" + unidad.getBoolConsultaExterna());
                    if (unidad.getBoolConsultaExterna()) {
                        unidad.setBoolConsultaExterna(Boolean.FALSE);
                    } else {
                        unidad.setBoolConsultaExterna(Boolean.TRUE);
                    }

                    mandarMensaje = Boolean.FALSE;
                    logger.info("DESPUES unidad.getBoolConsultaExterna():" + unidad.getBoolConsultaExterna());
                }
                if (cveTipoCaptura.equals("2") && !unidad.getBoolHospitalizacion()) {
                    logger.info("unidad.getBoolHospitalizacion():" + unidad.getBoolHospitalizacion());

                    if (unidad.getBoolHospitalizacion()) {
                        unidad.setBoolHospitalizacion(Boolean.FALSE);
                    } else {
                        unidad.setBoolHospitalizacion(Boolean.TRUE);
                    }
                    mandarMensaje = Boolean.FALSE;
                    logger.info("DESPUES unidad.getBoolHospitalizacion():" + unidad.getBoolHospitalizacion());
                }
                if (cveTipoCaptura.equals("3") && !unidad.getBoolInformaComplemen()) {
                    logger.info("unidad.getBoolInformaComplemen():" + unidad.getBoolInformaComplemen());

                    if (unidad.getBoolInformaComplemen()) {
                        unidad.setBoolInformaComplemen(Boolean.FALSE);
                    } else {
                        unidad.setBoolInformaComplemen(Boolean.TRUE);
                    }
                    agregarMensajeError(getArray(MensajesErrorConstants.ME_801), getArray(unidad.getCvePresupuestal()));
                    mandarMensaje = Boolean.FALSE;
                    logger.info("DESPUES unidad.getBoolInformaComplemen():" + unidad.getBoolInformaComplemen());
                }
                if (cveTipoCaptura.equals("4") && !unidad.getBoolServicioSub()) {
                    logger.info("unidad.getBoolServicioSub():" + unidad.getBoolServicioSub());

                    if (unidad.getBoolServicioSub()) {
                        unidad.setBoolServicioSub(Boolean.FALSE);
                    } else {
                        unidad.setBoolServicioSub(Boolean.TRUE);
                    }
                    mandarMensaje = Boolean.FALSE;
                    logger.info("DESPUES unidad.getBoolServicioSub():" + unidad.getBoolServicioSub());
                }
                gestionUnidadMedica(Boolean.TRUE);
                if (mandarMensaje) {
                    agregarMensajeError(getArray(MensajesErrorConstants.ME_801), getArray(unidad.getCvePresupuestal()));
                }

            }
        }

    }

    private UnidadMedicaCierre setBoolTipoCaptura(String cveTipoCaptura, UnidadMedicaCierre unidad) {

        switch (cveTipoCaptura) {
            case "1":
                unidad.setBoolConsultaExterna(!unidad.getBoolConsultaExterna());
                unidad.setCambioConsultaExterna(Boolean.TRUE);
                break;
            case "2":
                unidad.setBoolHospitalizacion(!unidad.getBoolHospitalizacion());
                unidad.setCambioHospitalizacion(Boolean.TRUE);
                break;
            case "3":
                unidad.setBoolInformaComplemen(!unidad.getBoolInformaComplemen());
                unidad.setCambioInformaComplemen(Boolean.TRUE);
                break;
            case "4":
                unidad.setBoolServicioSub(!unidad.getBoolServicioSub());
                unidad.setCambioServicioSub(Boolean.TRUE);
                break;
        }
        return unidad;
    }

    private boolean validaFecha(String mes, String anio) {

        Calendar cal = Calendar.getInstance();
        int mesPeriodo = Integer.parseInt(mes);
        int anioPeriodo = Integer.parseInt(anio);
        int mesActual = cal.get(Calendar.MONTH) + 1;
        int anioActual = cal.get(Calendar.YEAR);

        return !((anioPeriodo > anioActual) || (anioPeriodo == anioActual && mesPeriodo > mesActual));

    }

    public void resetPeriodo() {

        annio = "";
        mes = "";
        periodoSelStr = "";
        periodoIM.resetValue();

    }

    private boolean inputmaskLlena(InputMask inputMask) {

        return inputMask.getValue() != null && !inputMask.getValue().toString().isEmpty()
                && !inputMask.getValue().toString().contains("_");
    }

    private String descripMes(Integer mes) {

        String[] mesArray = new String[] { "", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO",
                "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" };
        if (mes >= 1 && mes <= 12) {
            return mesArray[mes];
        }
        return "ERROR";
    }

    public void cerrarautomatico() throws Exception {

        logger.info("ENTRAEN cerrarautomatico");
        realizarCierreMesAutomaticamenteServices.cerrarMesAutomaticamente();
        logger.error("SALE cerrarautomatico");
    }
    
    public List<String> filtrarCatalogoPresupuestal(String query) {
        return monitorCierreMensualServices.filtrarCatalogoPresupuestal(getListaPresupuestalAutocomplete(), query, 
        		MonitoreoConstants.LONGITUD_AUTOCOMPLETE_PRESUPUESTAL);
    }
    
    public void validarPresupuestal() {  	
    	if (getAutoCompletePresupuestal().getValue() != null) {
            String textoIngresado = getAutoCompletePresupuestal().getValue().toString();   
            if(StringUtils.isNotEmpty(textoIngresado) 
            		&& textoIngresado.length() >= MonitoreoConstants.LONGITUD_AUTOCOMPLETE_PRESUPUESTAL){  
            	listUnidades = listaUnidadesAuxiliar.stream().filter(
            			(u) -> u.getDesUnidadMedica().equalsIgnoreCase(textoIngresado)
            				|| u.getCvePresupuestal().equalsIgnoreCase(
            						textoIngresado.substring(0, 
            								MonitoreoConstants.LONGITUD_AUTOCOMPLETE_PRESUPUESTAL))
            			).collect(Collectors.toList()); 
        		
            }
            if (listUnidades.isEmpty() || listUnidades == null){
            	listUnidades = listaUnidadesAuxiliar;
        		setTieneFoco(autoCompletePresupuestal.getClientId());
        		agregarMensajeError(getArray(MensajesErrorConstants.ME_104), null);
        	}
            autoCompletePresupuestal.resetValue();
    	}    	
    }
}
