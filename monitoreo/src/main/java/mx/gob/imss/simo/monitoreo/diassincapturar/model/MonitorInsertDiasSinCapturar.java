package mx.gob.imss.simo.monitoreo.diassincapturar.model;

import java.util.Date;

import lombok.Data;

@Data
public class MonitorInsertDiasSinCapturar {

	private String cveDelegacionImss;
	private String cvePresupuestal;
	private Date fechaCaptura;
	private String refUnidadMedica;
	private String refDelegacionImss;
	
	private Integer numCeEspecialidad;
	private Integer numCeUrgencia;
	private Integer numEgresoHospitalizacion;
	private Integer numIntQuirurgicas;
	private Integer numEgresoUrgencia;
	
	private Short numColorCespecialidad;
	private Short numColorCeurgencia;
	private Short numColorEgresohosp;
	private Short numColorIntqx;
	private Short numColorEgresourg;
	
	private Short numColor;
}
