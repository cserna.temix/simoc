package mx.gob.imss.simo.monitoreo.diassincapturar.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.monitoreo.common.constants.SQLColumnasConstants;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.DelegacionDiasSinCapturar;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

/**
 * @author jonathan.lopez
 *
 */
public class DelegacionDiasSinCapturarRowMapperHelper extends BaseHelper implements RowMapper<DelegacionDiasSinCapturar> {

	@Override
	public DelegacionDiasSinCapturar mapRow(ResultSet resultSet, int rowId) throws SQLException {

		DelegacionDiasSinCapturar delegacionDiasSinCapturar = new DelegacionDiasSinCapturar();
		delegacionDiasSinCapturar.setCveDelegacionImss(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
		delegacionDiasSinCapturar.setRefDelegacionImss(resultSet.getString(SQLColumnasConstants.REF_DELEGACION_IMSS));
		delegacionDiasSinCapturar.setColor(resultSet.getString(SQLColumnasConstants.COLOR));
		return delegacionDiasSinCapturar;
	}

}
