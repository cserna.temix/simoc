/**
 * 
 */
package mx.gob.imss.simo.monitoreo.diassincapturar.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.model.UnidadMedica;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.DelegacionDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorInsertDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.PeriodoDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.UnidadMedicaDiasSinCapturar;

/**
 * @author jonathan.lopez
 */
public interface MonitorDiasSinCapturarRepository {

    /**
     * Obtiene el listado de delegaciones con la validacion de dias sin capturar para el semaforo requerido en la vista.
     * 
     * @param cveDelegacion
     * @para filtraDelegacion
     * @return
     */
    List<DelegacionDiasSinCapturar> obtenerDelegacionDiasSinCapturar(String cveDelegacion, boolean filtraDelegacion);

    /**
     * Obtiene el listado de unidades medicas por delegacion con la validacion de dias sin capturar para el semaforo
     * requerido en la vista.
     * 
     * @param cvedelegacion
     * @param cvePresupuestal
     * @param filtrarUnidadMedica
     * @return
     */
    List<UnidadMedicaDiasSinCapturar> obtenerUnidadesMedicasDiasSinCapturar(String cveDelegacion,
            List<UnidadMedica> cvePresupuestal, boolean filtrarUnidadMedica);

    /**
     * Obtiene el listado de dias sin capturar.
     * 
     * @param cveDelegacion
     * @param cvePresupuestal
     * @return
     */
    List<MonitorDiasSinCapturar> obtenerMonitorDiasSinCapturar(String cveDelegacion, String cvePresupuestal);

    /**
     * Obtiene el listado de periodos por unidad medica, as� como la configuracion por campo que especifica si se
     * ejecutara o no el query para llenar la tabla de dias sin captura
     * 
     * @param
     * @return
     */
    List<PeriodoDiasSinCapturar> obtenerPeriodosDiasSinCapturar();

    /**
     * Obtiene el listado de egresos hospitalarios por unidad medica dentro de un periodo especificado
     * 
     * @param periodoDiasSinCapturar
     * @return
     */
    Map<Date, Integer> obtenerEgresosHospitalarios(PeriodoDiasSinCapturar periodoDiasSinCapturar);

    /**
     * Obtiene el listado de intervenciones quirurgicas por unidad medica dentro de un periodo especificado
     * 
     * @param periodoDiasSinCapturar
     * @return
     */
    Map<Date, Integer> obtenerIntervencionesQuirurgicas(PeriodoDiasSinCapturar periodoDiasSinCapturar);

    /**
     * Obtiene el listado de egresos urgencias por unidad medica dentro de un periodo especificado
     * 
     * @param periodoDiasSinCapturar
     * @return
     */
    Map<Date, Integer> obtenerEgresosUrgencias(PeriodoDiasSinCapturar periodoDiasSinCapturar);

    /**
     * Metodo que realiza el borrado de la tabla de monitor dias sin capturar para su posterior llenado
     * 
     * @param
     * @return
     */
    int borrarMonitorDiasSinCapturar() throws HospitalizacionException;

    /**
     * Metodo que realiza el llenado de la tabla de monitor dias sin capturar
     * 
     * @param monitorInsertDiasSinCapturar
     * @return
     */
    int insertarMonitorDiasSinCapturar(MonitorInsertDiasSinCapturar monitorInsertDiasSinCapturar)
            throws HospitalizacionException;

    /**
     * Metodo que verifica si la fecha corresponde a un dia inh�bil
     * 
     * @param fecha
     * @return
     */
    Boolean consultarDiaInhabil(Date fecha);

    /**
     * M�todo que consulta si un personal operativo tiene unidades m�dicas como hu�spedes
     * 
     * @param refPersonalOperativo
     * @return
     */
    Boolean consultarUnidadHuesped(String refPersonalOperativo);
    
    boolean validarRegistroMonitoreo(String delegacionIMSS,String clavePresupuestal,Date fecCaptura) ;
}
