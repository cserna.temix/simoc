package mx.gob.imss.simo.monitoreo.diassincapturar.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.model.UnidadMedica;
import mx.gob.imss.simo.monitoreo.common.constants.SQLConstants;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.DelegacionDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorInsertDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.PeriodoDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.UnidadMedicaDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.repository.MonitorDiasSinCapturarRepository;
import mx.gob.imss.simo.repository.ConsultaExternaDiasSinCapturarRepository;

/**
 * @author jonathan.lopez
 */
@Service("monitorDiasSinCapturarServices")
public class MonitorDiasSinCapturarServicesImpl extends HospitalizacionCommonServicesImpl
        implements MonitorDiasSinCapturarServices {

    final long MILISEGUNDOS_POR_DIA = 24 * 60 * 60 * 1000; // Milisegundos por dia

    @Autowired
    private MonitorDiasSinCapturarRepository monitorDiasSinCapturarRepository;

    @Autowired
    private ConsultaExternaDiasSinCapturarRepository consultaExternaDiasSinCapturarRepository;

    @Override
    public List<DelegacionDiasSinCapturar> obtenerDelegacionDiasSinCapturar(String cveDelegacion,
            boolean filtraDelegacion) {

        return monitorDiasSinCapturarRepository.obtenerDelegacionDiasSinCapturar(cveDelegacion, filtraDelegacion);
    }

    @Override
    public List<UnidadMedicaDiasSinCapturar> obtenerUnidadesMedicasDiasSinCapturar(String cveDelegacion,
            List<UnidadMedica> cvePresupuestal, boolean filtrarUnidadMedica) {

        return monitorDiasSinCapturarRepository.obtenerUnidadesMedicasDiasSinCapturar(cveDelegacion, cvePresupuestal,
                filtrarUnidadMedica);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public List<MonitorDiasSinCapturar> obtenerMonitorDiasSinCapturar(String cveDelegacion, String cvePresupuestal) {

        return monitorDiasSinCapturarRepository.obtenerMonitorDiasSinCapturar(cveDelegacion, cvePresupuestal);
    }

    @Override
    public List<PeriodoDiasSinCapturar> obtenerPeriodosDiasSinCapturar() {

        return monitorDiasSinCapturarRepository.obtenerPeriodosDiasSinCapturar();
    }

    @Override
    public boolean consultarUnidadesHuesped(String refPersonalOperativo) {

        return monitorDiasSinCapturarRepository.consultarUnidadHuesped(refPersonalOperativo);
    }

    @Override
    public void procesarMonitorDiasSinCapturar() throws HospitalizacionException {

        List<MonitorInsertDiasSinCapturar> listMonitorInsert = new ArrayList<MonitorInsertDiasSinCapturar>();

        Map<Date, Integer> mapConsultaExternaEspecialidades = null;
        Map<Date, Integer> mapConsultaExternaUrgencias = null;
        Map<Date, Integer> mapEgresosHospitalarios = null;
        Map<Date, Integer> mapIntervencionesQuirurgicas = null;
        Map<Date, Integer> mapEgresosUrgencias = null;

        List<PeriodoDiasSinCapturar> listPeriodos = obtenerPeriodosDiasSinCapturar();
        List<Date> listFechasPeriodo = null;
        logger.debug("obtenerPeriodosDiasSinCapturar: " + listPeriodos);

        for (PeriodoDiasSinCapturar periodoDiasSinCapturar : listPeriodos) {

            mapConsultaExternaEspecialidades = procesarEspecialidadCe(periodoDiasSinCapturar);
            mapConsultaExternaUrgencias = procesarUrgenciaCe(periodoDiasSinCapturar);
            mapEgresosHospitalarios = procesarEgresosHospitalarios(periodoDiasSinCapturar);
            mapIntervencionesQuirurgicas = procesarIntervencionesQuirurgicas(periodoDiasSinCapturar);
            mapEgresosUrgencias = procesarEgresosUrgencias(periodoDiasSinCapturar);

            // logger.debug("mapConsultaExternaEspecialidades: " + mapConsultaExternaEspecialidades);
            // logger.debug("mapConsultaExternaUrgencias: " + mapConsultaExternaUrgencias);
            // logger.debug("mapEgresosHospitalarios: " + mapEgresosHospitalarios);
            // logger.debug("mapIntervencionesQuirurgicas: " + mapIntervencionesQuirurgicas);
            // logger.debug("mapEgresosUrgencias: " + mapEgresosUrgencias);

            listFechasPeriodo = obtenerFechasDelPeriodo(periodoDiasSinCapturar.getFecInicial(),
                    periodoDiasSinCapturar.getFecFinal());
            logger.debug("obtenerFechasDelPeriodo - listFechasPeriodo: " + listFechasPeriodo);

            for (Date fechaPeriodo : listFechasPeriodo) {
                MonitorInsertDiasSinCapturar monitorInsertDiasSinCapturar = new MonitorInsertDiasSinCapturar();

                monitorInsertDiasSinCapturar.setCveDelegacionImss(periodoDiasSinCapturar.getCveDelegacionImss());
                monitorInsertDiasSinCapturar.setCvePresupuestal(periodoDiasSinCapturar.getCvePresupuestal());
                monitorInsertDiasSinCapturar.setFechaCaptura(fechaPeriodo);
                monitorInsertDiasSinCapturar.setRefUnidadMedica(periodoDiasSinCapturar.getDesUnidadMedica());
                monitorInsertDiasSinCapturar.setRefDelegacionImss(periodoDiasSinCapturar.getDesDelegacionImss());
                monitorInsertDiasSinCapturar
                        .setNumCeEspecialidad(mapConsultaExternaEspecialidades.get(fechaPeriodo) == null ? 0
                                : mapConsultaExternaEspecialidades.get(fechaPeriodo));
                monitorInsertDiasSinCapturar.setNumCeUrgencia(mapConsultaExternaUrgencias.get(fechaPeriodo) == null ? 0
                        : mapConsultaExternaUrgencias.get(fechaPeriodo));
                monitorInsertDiasSinCapturar
                        .setNumEgresoHospitalizacion(mapEgresosHospitalarios.get(fechaPeriodo) == null ? 0
                                : mapEgresosHospitalarios.get(fechaPeriodo));
                monitorInsertDiasSinCapturar.setNumIntQuirurgicas(mapIntervencionesQuirurgicas.get(fechaPeriodo) == null
                        ? 0 : mapIntervencionesQuirurgicas.get(fechaPeriodo));
                monitorInsertDiasSinCapturar.setNumEgresoUrgencia(
                        mapEgresosUrgencias.get(fechaPeriodo) == null ? 0 : mapEgresosUrgencias.get(fechaPeriodo));
                monitorInsertDiasSinCapturar.setNumColorCespecialidad(calcularColorSemaforoCampoFinDeSemana(
                        fechaPeriodo, monitorInsertDiasSinCapturar.getNumCeEspecialidad(),
                        periodoDiasSinCapturar.getIndEspecialidadCe()));
                // monitorInsertDiasSinCapturar.setNumColorCeurgencia(calcularColorSemaforoCampoFinDeSemana(fechaPeriodo,
                // monitorInsertDiasSinCapturar.getNumCeUrgencia()));
                monitorInsertDiasSinCapturar.setNumColorCeurgencia(calcularColorSemaforoCampo(fechaPeriodo,
                        monitorInsertDiasSinCapturar.getNumCeUrgencia(), periodoDiasSinCapturar.getIndUrgenciaCe()));
                monitorInsertDiasSinCapturar.setNumColorEgresohosp(calcularColorSemaforoCampo(fechaPeriodo,
                        monitorInsertDiasSinCapturar.getNumEgresoHospitalizacion(),
                        periodoDiasSinCapturar.getIndEgresoHospital()));
                monitorInsertDiasSinCapturar.setNumColorIntqx(
                        calcularColorSemaforoCampo(fechaPeriodo, monitorInsertDiasSinCapturar.getNumIntQuirurgicas(),
                                periodoDiasSinCapturar.getIndIntQuirurgica()));
                monitorInsertDiasSinCapturar.setNumColorEgresourg(
                        calcularColorSemaforoCampo(fechaPeriodo, monitorInsertDiasSinCapturar.getNumEgresoUrgencia(),
                                periodoDiasSinCapturar.getIndEgresoUrgencia()));
                monitorInsertDiasSinCapturar.setNumColor(calcularColorGeneralDelRegistro(monitorInsertDiasSinCapturar));

                listMonitorInsert.add(monitorInsertDiasSinCapturar);
            }
        }

        logger.debug("procesarMonitorDiasSinCapturar - listMonitorInsert: " + listMonitorInsert);

        borrarMonitorDiasSinCapturar();

        insertarMonitorDiasSinCapturar(listMonitorInsert);
    }

    private Map<Date, Integer> procesarEspecialidadCe(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        if (periodoDiasSinCapturar.getIndEspecialidadCe() == 1) {
            return consultaExternaDiasSinCapturarRepository.obtenerConsultaExternaEspecialidad(periodoDiasSinCapturar);
        } else {
            return new HashMap<Date, Integer>();
        }
    }

    private Map<Date, Integer> procesarUrgenciaCe(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        if (periodoDiasSinCapturar.getIndUrgenciaCe() == 1) {
            return consultaExternaDiasSinCapturarRepository.obtenerConsultaExternaUrgencia(periodoDiasSinCapturar);
        } else {
            return new HashMap<Date, Integer>();
        }
    }

    private Map<Date, Integer> procesarEgresosHospitalarios(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        if (periodoDiasSinCapturar.getIndEgresoHospital() == 1) {
            return monitorDiasSinCapturarRepository.obtenerEgresosHospitalarios(periodoDiasSinCapturar);
        } else {
            return new HashMap<Date, Integer>();
        }
    }

    private Map<Date, Integer> procesarIntervencionesQuirurgicas(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        if (periodoDiasSinCapturar.getIndIntQuirurgica() == 1) {
            return monitorDiasSinCapturarRepository.obtenerIntervencionesQuirurgicas(periodoDiasSinCapturar);
        } else {
            return new HashMap<Date, Integer>();
        }
    }

    private Map<Date, Integer> procesarEgresosUrgencias(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        if (periodoDiasSinCapturar.getIndEgresoUrgencia() == 1) {
            return monitorDiasSinCapturarRepository.obtenerEgresosUrgencias(periodoDiasSinCapturar);
        } else {
            return new HashMap<Date, Integer>();
        }
    }

    private List<Date> obtenerFechasDelPeriodo(Date fechaInicial, Date fechaFinal) {

        List<Date> list = new ArrayList<Date>();

        Calendar calendarTmp = Calendar.getInstance();
        calendarTmp.setTime(fechaInicial);
        Date fechaHoy = Calendar.getInstance().getTime();

        if (fechaHoy.compareTo(fechaFinal) > 0) {
            logger.debug("obtenerFechasDelPeriodo - La fecha de HOY es > a la fechaFinal - fechaHoy: " + fechaHoy
                    + ", fechaFinal: " + fechaFinal);
            fechaHoy = (Date) fechaFinal.clone();
        }
        // logger.debug("obtenerFechasDelPeriodo - calendarTmp: " + calendarTmp.getTime() + ", fechaHoy: " + fechaHoy +
        // ", comparacion: " + (calendarTmp.getTime().compareTo(fechaHoy) <= 0));

        while (calendarTmp.getTime().compareTo(fechaHoy) <= 0) {

            list.add(calendarTmp.getTime());
            calendarTmp.add(Calendar.DAY_OF_YEAR, 1);
            // logger.debug("obtenerFechasDelPeriodo - calendarTmp: " + calendarTmp.getTime() + ", fechaHoy: " +
            // fechaHoy + ", comparacion: " + (calendarTmp.getTime().compareTo(fechaHoy) <= 0));
        }

        return list;
    }

    private short calcularColorSemaforoCampo(Date fecha, Integer numero, short indServicio) {

        short color = SQLConstants.NUM_SEMAFORO_BLANCO;

        Date fechaHoy = Calendar.getInstance().getTime();
        int numDias = (int) ((fechaHoy.getTime() - fecha.getTime()) / MILISEGUNDOS_POR_DIA);

        if (numero == null || (numero.equals(0) && indServicio == 1)) {
            if (numDias <= SQLConstants.DIAS_SEMAFORO_VERDE) {
                color = SQLConstants.NUM_SEMAFORO_VERDE;
            } else if (numDias >= SQLConstants.DIAS_SEMAFORO_AMARICIO_INICIO
                    && numDias <= SQLConstants.DIAS_SEMAFORO_AMARICIO_FIN) {
                color = SQLConstants.NUM_SEMAFORO_AMARILLO;
            } else if (numDias >= SQLConstants.DIAS_SEMAFORO_ROJO) {
                color = SQLConstants.NUM_SEMAFORO_ROJO;
            }
        }

        return color;
    }

    private short calcularColorSemaforoCampoCE(Date fecha, Integer numero, short indServicio) {

        short color = SQLConstants.NUM_SEMAFORO_BLANCO;

        Date fechaHoy = Calendar.getInstance().getTime();
        int numDias = (int) ((fechaHoy.getTime() - fecha.getTime()) / MILISEGUNDOS_POR_DIA);
        int numDiasInhabiles = calculaNumDiasInhabiles(fecha, fechaHoy);
        numDias = numDias - numDiasInhabiles;

        if (numero == null || (numero.equals(0) && indServicio == 1)) {
            if (numDias <= SQLConstants.DIAS_SEMAFORO_VERDE) {
                color = SQLConstants.NUM_SEMAFORO_VERDE;
            } else if (numDias >= SQLConstants.DIAS_SEMAFORO_AMARICIO_INICIO
                    && numDias <= SQLConstants.DIAS_SEMAFORO_AMARICIO_FIN) {
                color = SQLConstants.NUM_SEMAFORO_AMARILLO;
            } else if (numDias >= SQLConstants.DIAS_SEMAFORO_ROJO) {
                color = SQLConstants.NUM_SEMAFORO_ROJO;
            }
        }

        return color;
    }

    private int calculaNumDiasInhabiles(Date fechaInicio, Date fechaFin) {

        int diasInhabiles = 0;

        Calendar calendarInicio = Calendar.getInstance();
        calendarInicio.setTime(fechaInicio);

        Calendar calendarFin = Calendar.getInstance();
        calendarFin.setTime(fechaFin);

        while (calendarFin.after(calendarInicio)) {
            if (calendarInicio.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                    || calendarInicio.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
                    || monitorDiasSinCapturarRepository.consultarDiaInhabil(calendarInicio.getTime())) {
                System.out.println("Dia de la semana" + calendarInicio.get(Calendar.DAY_OF_WEEK));
                diasInhabiles = diasInhabiles + 1;
            }
            calendarInicio.add(Calendar.DATE, 1);
        }

        return diasInhabiles;

    }

    private short calcularColorSemaforoCampoFinDeSemana(Date fecha, Integer numero, short indServicio) {

        short color = SQLConstants.NUM_SEMAFORO_BLANCO;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        int diaDeLaSemana = calendar.get(Calendar.DAY_OF_WEEK);

        if (numero == null || (numero.equals(0) && indServicio == 1)) {
            if (diaDeLaSemana == Calendar.SATURDAY || diaDeLaSemana == Calendar.SUNDAY
                    || monitorDiasSinCapturarRepository.consultarDiaInhabil(fecha)) {
                color = SQLConstants.NUM_SEMAFORO_GRIS;
            } else {
                color = calcularColorSemaforoCampoCE(fecha, numero, indServicio);
            }
        }

        return color;
    }

    private short calcularColorGeneralDelRegistro(MonitorInsertDiasSinCapturar monitorInsertDiasSinCapturar) {

        short color = SQLConstants.NUM_SEMAFORO_VERDE;

        List<Short> colorList = new ArrayList<Short>();
        colorList.add(monitorInsertDiasSinCapturar.getNumColorCespecialidad());
        colorList.add(monitorInsertDiasSinCapturar.getNumColorCeurgencia());
        colorList.add(monitorInsertDiasSinCapturar.getNumColorEgresohosp());
        colorList.add(monitorInsertDiasSinCapturar.getNumColorIntqx());
        colorList.add(monitorInsertDiasSinCapturar.getNumColorEgresourg());

        if (colorList.contains(SQLConstants.NUM_SEMAFORO_ROJO)) {
            color = SQLConstants.NUM_SEMAFORO_ROJO;
        } else if (colorList.contains(SQLConstants.NUM_SEMAFORO_AMARILLO)) {
            color = SQLConstants.NUM_SEMAFORO_AMARILLO;
        }

        return color;
    }

    private void borrarMonitorDiasSinCapturar() throws HospitalizacionException {

        monitorDiasSinCapturarRepository.borrarMonitorDiasSinCapturar();
        logger.info("borrarMonitorDiasSinCapturar - Se completo de manera exitosa el borrado");
    }

    private void insertarMonitorDiasSinCapturar(List<MonitorInsertDiasSinCapturar> listMonitorInsert)
            throws HospitalizacionException {

        for (MonitorInsertDiasSinCapturar monitorInsertDiasSinCapturar : listMonitorInsert) {
        	if(!monitorDiasSinCapturarRepository.validarRegistroMonitoreo(monitorInsertDiasSinCapturar.getCveDelegacionImss(),
        			monitorInsertDiasSinCapturar.getCvePresupuestal(), monitorInsertDiasSinCapturar.getFechaCaptura())){
        		monitorDiasSinCapturarRepository.insertarMonitorDiasSinCapturar(monitorInsertDiasSinCapturar);
        	}
        }
        logger.info("insertarMonitorDiasSinCapturar - Se completo de manera exitosa la insercion de los registros");
    }

}
