package mx.gob.imss.simo.repository.impl;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.UnwindOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.monitoreo.common.constants.SQLConstants;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.ContadorCE;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.PeriodoDiasSinCapturar;
import mx.gob.imss.simo.repository.ConsultaExternaDiasSinCapturarRepository;

/**
 * @author jonathan.lopez
 */
@Repository
public class ConsultaExternaDiasSinCapturarRepositoryImpl implements ConsultaExternaDiasSinCapturarRepository {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public ConsultaExternaDiasSinCapturarRepositoryImpl(MongoTemplate mongoTemplate) {

        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Map<Date, Integer> obtenerConsultaExternaEspecialidad(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        MatchOperation matchOperation = getMatchOperationEspecialidad(periodoDiasSinCapturar.getCvePresupuestal(),
                SQLConstants.TIPO_SERVICIO_ESPECIALIDADES, periodoDiasSinCapturar.getFecInicial(),
                periodoDiasSinCapturar.getFecFinal());

        System.out.println("*****PeriodoDiasSinCapturar:" + periodoDiasSinCapturar.getCvePresupuestal() + " --"
                + SQLConstants.TIPO_SERVICIO_ESPECIALIDADES + " --" + periodoDiasSinCapturar.getFecInicial() + " --"
                + periodoDiasSinCapturar.getFecFinal());
        Map<Date, Integer> mapEspecialidad = aggregate(matchOperation);

        return mapEspecialidad;
    }

    @Override
    public Map<Date, Integer> obtenerConsultaExternaUrgencia(PeriodoDiasSinCapturar periodoDiasSinCapturar) {

        MatchOperation matchOperation = getMatchOperationUrgencia(periodoDiasSinCapturar.getCvePresupuestal(),
                SQLConstants.TIPO_SERVICIO_URGENCIAS, periodoDiasSinCapturar.getFecInicial(),
                periodoDiasSinCapturar.getFecFinal());
        Map<Date, Integer> mapUrgencia = aggregate(matchOperation);

        return mapUrgencia;
    }

    private Map<Date, Integer> aggregate(MatchOperation matchOperation) {

        UnwindOperation unwind1 = Aggregation.unwind("sdEncabezado");
        UnwindOperation unwind2 = Aggregation.unwind("sdEncabezado.sdFormato");
        GroupOperation groupOperation = getGroupOperation();
        ProjectionOperation projectionOperation = getProjectOperation();

        Aggregation aggr = Aggregation.newAggregation(matchOperation, unwind1, unwind2, projectionOperation,
                groupOperation);
        System.out.println("----------------------aggraggraggr" + aggr);
        List<ContadorCE> list = mongoTemplate.aggregate(
                Aggregation.newAggregation(matchOperation, unwind1, unwind2, projectionOperation, groupOperation),
                SQLConstants.TABLA_CONSULTA_EXTERNA, ContadorCE.class).getMappedResults();

        return convierteListaMapa(list);
    }

    private MatchOperation getMatchOperationEspecialidad(String cvePresupuestal, Object[] tipoServicio,
            Date fechaInicial, Date fechaFinal) {

        Criteria priceCriteria = where("sdUnidadMedica.cvePresupuestal").is(cvePresupuestal)
                .and("sdEncabezado.fecAltaEncabezado").gte(fechaInicial).lte(fechaFinal)
                .and("sdEncabezado.sdEspecialidad.numTipoServicio").in(tipoServicio);
        // .and("sdEncabezado.sdEspecialidad.numTipoServicio").is(tipoServicio);

        return match(priceCriteria);
    }

    private MatchOperation getMatchOperationUrgencia(String cvePresupuestal, Object[] tipoServicio, Date fechaInicial,
            Date fechaFinal) {

        Criteria priceCriteria = where("sdUnidadMedica.cvePresupuestal").is(cvePresupuestal)
                .and("sdEncabezado.fecAltaEncabezado").gte(fechaInicial).lte(fechaFinal)
                .and("sdEncabezado.sdEspecialidad.numTipoServicio").in(tipoServicio);

        return match(priceCriteria);
    }

    private GroupOperation getGroupOperation() {

        // return group("sdEncabezado.fecAltaEncabezado")
        // { $dateToString: { format: "%Y-%m-%d", date: "$sdEncabezado.fecAltaEncabezado" } }
        // return group("$dateToString: { format: \"%Y-%m-%d\", date: \"$sdEncabezado.fecAltaEncabezado\" }")
        // return group(fields().and("year").and("month").and("day"))
        // .count().as("count");

        // return group("_id: { $dateToString: { format: '%Y-%m-%d', date: '$sdEncabezado.fecAltaEncabezado' } }")
        // .count().as("count");

        return group("year", "month", "day").count().as("numero");
    }

    private ProjectionOperation getProjectOperation() {

        return project().andExpression("year($sdEncabezado.fecAltaEncabezado)").as("year")
                .andExpression("month($sdEncabezado.fecAltaEncabezado)").as("month")
                .andExpression("dayOfMonth($sdEncabezado.fecAltaEncabezado)").as("day");

        // return project().andExpression("year('$sdEncabezado.fecAltaEncabezado')").as("year")
        // .andExpression("month('$sdEncabezado.fecAltaEncabezado')").as("month")
        // .andExpression("dayOfMonth('$sdEncabezado.fecAltaEncabezado')").as("day")
        // .andExpression("count").as("numero");
    }

    // private Map<Date, Integer> convierteListaMapa(List<Contador> lista) {
    //
    // Map<Date, Integer> map = new HashMap<Date, Integer>();
    //
    // for(Contador contador : lista) {
    // map.put(contador.getFecha(), contador.getNumero());
    // }
    //// logger.debug("convierteListaMap - MAP: " + map);
    //
    // return map;
    // }

    private Map<Date, Integer> convierteListaMapa(List<ContadorCE> lista) {

        Map<Date, Integer> map = new HashMap<Date, Integer>();

        for (ContadorCE contador : lista) {
            map.put(obtieneFechaDeContador(contador), contador.getNumero());
        }
        System.out.println("convierteListaMap - MAP: " + map);

        return map;
    }

    private Date obtieneFechaDeContador(ContadorCE contador) {

        Calendar c = Calendar.getInstance();
        c.clear();
        c.set(contador.getYear(), contador.getMonth() - 1, contador.getDay());

        return c.getTime();
    }
}
