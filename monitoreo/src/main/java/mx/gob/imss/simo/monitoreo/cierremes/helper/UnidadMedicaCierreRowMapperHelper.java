package mx.gob.imss.simo.monitoreo.cierremes.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierre;

/**
 * @author francisco.rodriguez
 */
public class UnidadMedicaCierreRowMapperHelper extends BaseHelper implements RowMapper<UnidadMedicaCierre> {

    @Override
    public UnidadMedicaCierre mapRow(ResultSet resultSet, int rowId) throws SQLException {

        UnidadMedicaCierre unidadMedicaCierre = new UnidadMedicaCierre();
        unidadMedicaCierre.setCveDelegacion(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION));
        unidadMedicaCierre.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        unidadMedicaCierre.setDesUnidadMedica(resultSet.getString(SQLColumnasConstants.DES_UNIDAD_MEDICA));
        unidadMedicaCierre.setConsultaExterna(resultSet.getInt(SQLColumnasConstants.CONSULTA_EXTERNA));
        unidadMedicaCierre.setHospitalizacion(resultSet.getInt(SQLColumnasConstants.HOSPITALIZACION));
        unidadMedicaCierre.setInformaComplemen(resultSet.getInt(SQLColumnasConstants.INFORMA_COMPLEMEN));
        unidadMedicaCierre.setServicioSub(resultSet.getInt(SQLColumnasConstants.SERVICIO_SUB));
        unidadMedicaCierre.setColor(resultSet.getString(SQLColumnasConstants.COLOR));
        return unidadMedicaCierre;
    }

}
