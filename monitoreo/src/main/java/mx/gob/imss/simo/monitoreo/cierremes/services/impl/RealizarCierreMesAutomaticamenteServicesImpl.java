package mx.gob.imss.simo.monitoreo.cierremes.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierreAutomaticoMes;
import mx.gob.imss.simo.monitoreo.cierremes.repository.MonitoreoCommonOracleRepository;
import mx.gob.imss.simo.monitoreo.cierremes.services.RealizarCierreMesAutomaticamenteServices;

@Data
@Service("realizarCierreMesAutomaticamenteServices")
public class RealizarCierreMesAutomaticamenteServicesImpl extends MonitoreoCommonServicesImpl
        implements RealizarCierreMesAutomaticamenteServices {

    @Autowired
    private MonitoreoCommonOracleRepository monitoreoCommonOracleRepository;

    private static final String PARAMETRO_DIAS_CIERRE = "DIAS_CIERRE_AUTOMATICO";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void cerrarMesAutomaticamente() throws Exception {

        System.out.println("ENTRA EN cerrarMesAutomaticamente----------------");
        logger.info("Inicia cerrarMesAutomaticamente");
        Date hoy = Calendar.getInstance().getTime();
        // Obtener d�as parametizables para cerrar el periodo
        Integer numDiasCerrar = obtenerParametroBaseDatos(PARAMETRO_DIAS_CIERRE);
        List<Date> diasFeriados = obtenerDiasNoHabiles(new Date());
        // Es el d�a fechaFinPeriodo+Numero parametizable de d�as
        List<UnidadMedicaCierreAutomaticoMes> listCierreAutomatico = obtenerUnidadesMedicasParaCierreAutomaticoDeMes();

        for (UnidadMedicaCierreAutomaticoMes unidadMedicaCierreAutomaticoMes : listCierreAutomatico) {
            Date fechaCierreMesXUnidad = contarDiasHabiles(unidadMedicaCierreAutomaticoMes.getFecFinal(), numDiasCerrar,
                    diasFeriados);

            if (hoy.compareTo(fechaCierreMesXUnidad) > 0) {
                logger.debug("Se realiza el cierre de mes para: " + unidadMedicaCierreAutomaticoMes);
                PeriodosImss periodoSiguiente = monitoreoCommonOracleRepository
                        .obtenerSiguientePeriodoImss(unidadMedicaCierreAutomaticoMes.getCvePeriodoImss());

                cerrarEInsertarPeriodoConsultaExternaMongoOracle(unidadMedicaCierreAutomaticoMes.getCvePresupuestal(),
                        Integer.valueOf(unidadMedicaCierreAutomaticoMes.getCveTipoCaptura()),
                        unidadMedicaCierreAutomaticoMes.getCvePeriodoImss(), periodoSiguiente.getClavePeriodo());

            } else {
                logger.debug("No se realiza el cierre de mes para: " + unidadMedicaCierreAutomaticoMes);
            }
        }

        logger.info("Termina cerrarMesAutomaticamente");
    }

    @Override
    public Date contarDiasHabiles(Date fecha, int numDias, List<Date> feriados) {

        logger.trace("INICIO contarDiasHabiles:");
        // Se agrega la fecha a partir de la cual se contar�
        Calendar diasCalendar = Calendar.getInstance();
        diasCalendar.setTime(fecha);
        int d = 0;

        while (d < numDias) {

            diasCalendar.add(Calendar.DAY_OF_MONTH, 1);

            if (diasCalendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                    && diasCalendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && !feriados.contains(diasCalendar.getTime())) {

                logger.debug("diasCalendar.getTime(): " + diasCalendar.getTime()
                        + ", feriados.contains(diasCalendar.getTime()): " + feriados.contains(diasCalendar.getTime()));
                d++;
            }
        }

        Date fechaFinal = diasCalendar.getTime();
        logger.info("fechaFinal  :" + fechaFinal);

        return fechaFinal;
    }

    @Override
    public List<PeriodoOperacion> obtenerListaPresupuestalesAbiertasActivas(String clavePeriodo) {

        logger.info("clavePeriodo:" + clavePeriodo);
        return monitoreoCommonOracleRepository.obtenerListaPresupuestalesAbiertasActivas(clavePeriodo);
    }

    @Override
    public Integer obtenerParametroBaseDatos(String parametro) {

        String desParametro = monitoreoCommonOracleRepository.obtenerParametroBaseDatos(parametro);
        if (null != desParametro) {
            return Integer.valueOf(desParametro);
        }
        return 0;
    }

    @Override
    public List<Date> obtenerDiasNoHabiles(Date fechaActual) {

        return monitoreoCommonOracleRepository.obtenerDiasNoHabiles(fechaActual);
    }

    @Override
    public List<UnidadMedicaCierreAutomaticoMes> obtenerUnidadesMedicasParaCierreAutomaticoDeMes() {

        return monitoreoCommonOracleRepository.obtenerUnidadesMedicasParaCierreAutomaticoDeMes();
    }
}
