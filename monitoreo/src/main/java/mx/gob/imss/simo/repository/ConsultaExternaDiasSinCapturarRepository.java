package mx.gob.imss.simo.repository;

import java.util.Date;
import java.util.Map;

import mx.gob.imss.simo.monitoreo.diassincapturar.model.PeriodoDiasSinCapturar;

/**
 * @author jonathan.lopez
 */
public interface ConsultaExternaDiasSinCapturarRepository {

	Map<Date, Integer> obtenerConsultaExternaEspecialidad(PeriodoDiasSinCapturar periodoDiasSinCapturar);

	Map<Date, Integer> obtenerConsultaExternaUrgencia(PeriodoDiasSinCapturar periodoDiasSinCapturar);
}
