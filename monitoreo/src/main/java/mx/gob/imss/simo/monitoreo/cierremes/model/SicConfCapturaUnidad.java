package mx.gob.imss.simo.monitoreo.cierremes.model;

import lombok.Data;

@Data
public class SicConfCapturaUnidad {

	 private String cvePresupuestal;
	 private Integer cveTipoCaptura;
	 private Integer indConfActiva;
	 
}
