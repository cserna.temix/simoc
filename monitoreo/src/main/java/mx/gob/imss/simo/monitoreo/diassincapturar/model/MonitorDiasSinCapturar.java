package mx.gob.imss.simo.monitoreo.diassincapturar.model;

import lombok.Data;

@Data
public class MonitorDiasSinCapturar {

	private String fechaCaptura;
	private Integer numCeEspecialidad;
	private String colorCespecialidad;
	private Integer numCeUrgencia;
	private String colorCeurgencia;
	private Integer numEgresoHospitalizacion;
	private String colorEgresohosp;
	private Integer numIntQuirurgicas;
	private String colorIntqx;
	private Integer numEgresoUrgencia;
	private String colorEgresourg;
	
}
