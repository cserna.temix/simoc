/**
 * 
 */
package mx.gob.imss.simo.monitoreo.diassincapturar.model;

/**
 * @author jonathan.lopez
 */
public class UnidadMedicaDiasSinCapturar {

	private String cvePresupuestal;
	private String refUnidadMedica;
	private String color;
	
	/**
	 * @return the cvePresupuestal
	 */
	public String getCvePresupuestal() {
		return cvePresupuestal;
	}
	/**
	 * @param cvePresupuestal the cvePresupuestal to set
	 */
	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}
	/**
	 * @return the refUnidadMedica
	 */
	public String getRefUnidadMedica() {
		return refUnidadMedica;
	}
	/**
	 * @param refUnidadMedica the refUnidadMedica to set
	 */
	public void setRefUnidadMedica(String refUnidadMedica) {
		this.refUnidadMedica = refUnidadMedica;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	
}
