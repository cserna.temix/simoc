package mx.gob.imss.simo.monitoreo.diassincapturar.model;

/**
 * @author jonathan.lopez
 */
public class DelegacionDiasSinCapturar {

    private String cveDelegacionImss;
    private String refDelegacionImss;
    private String color;
    
	/**
	 * @return the cveDelegacionImss
	 */
	public String getCveDelegacionImss() {
		return cveDelegacionImss;
	}
	/**
	 * @param cveDelegacionImss the cveDelegacionImss to set
	 */
	public void setCveDelegacionImss(String cveDelegacionImss) {
		this.cveDelegacionImss = cveDelegacionImss;
	}
	/**
	 * @return the refDelegacionImss
	 */
	public String getRefDelegacionImss() {
		return refDelegacionImss;
	}
	/**
	 * @param refDelegacionImss the refDelegacionImss to set
	 */
	public void setRefDelegacionImss(String refDelegacionImss) {
		this.refDelegacionImss = refDelegacionImss;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

    
}
