package mx.gob.imss.simo.monitoreo.common.constants;

public class SQLColumnasConstants {

    private SQLColumnasConstants() {
        super();
    }

    /**
     * Monitor Dias Sin Capturar
     */
    public static final String CVE_DELEGACION_IMSS = "cve_delegacion_imss";
    public static final String REF_DELEGACION_IMSS = "ref_delegacion_imss";
    public static final String DES_DELEGACION_IMSS = "des_delegacion_imss";

    public static final String CVE_PRESUPUESTAL = "cve_presupuestal";
    public static final String REF_UNIDAD_MEDICA = "ref_unidad_medica";
    public static final String DES_UNIDAD_MEDICA = "des_unidad_medica";

    public static final String COLOR = "color";

    public static final String FECHA_CAPTURA = "fecha_captura_monitor";
    public static final String NUM_CE_ESPECIALIDAD = "num_ce_especialidad";
    public static final String COLOR_CESPECIALIDAD = "color_cespecialidad";
    public static final String NUM_CE_URGENCIA = "num_ce_urgencia";
    public static final String COLOR_CEURGENCIA = "color_ceurgencia";
    public static final String NUM_EGRESO_HOSPITALIZACION = "num_egreso_hospitalizacion";
    public static final String COLOR_EGRESOHOSP = "color_egresohosp";
    public static final String NUM_INT_QUIRURGICAS = "num_int_quirurgicas";
    public static final String COLOR_INTQX = "color_intqx";
    public static final String NUM_EGRESO_URGENCIA = "num_egreso_urgencia";
    public static final String COLOR_EGRESOURG = "color_egresourg";

    public static final String IND_ESPECIALIDAD_CE = "ind_especialidad_ce";
    public static final String IND_URGENCIA_CE = "ind_urgencia_ce";
    public static final String IND_EGRESO_HOSPITAL = "ind_egreso_hospital";
    public static final String IND_INT_QUIRURGICA = "ind_int_quirurgica";
    public static final String IND_EGRESO_URGENCIA = "ind_egreso_urgencia";
    public static final String FEC_INICIAL = "fec_inicial";
    public static final String FEC_FINAL = "fec_final";

    public static final String FECHA = "fecha";
    public static final String NUMERO = "numero";

    /**
     * Monitor Cierre de Mes
     */
    public static final String CVE_TIPO_CAPTURA = "cve_tipo_captura";
    public static final String CVE_PERIODO_IMSS = "cve_periodo_imss";

}
