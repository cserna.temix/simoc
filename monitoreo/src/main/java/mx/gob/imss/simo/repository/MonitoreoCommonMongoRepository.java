package mx.gob.imss.simo.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import mx.gob.imss.simo.monitoreo.cierremes.model.SitPeriodoOperacion;

public interface MonitoreoCommonMongoRepository extends MongoRepository<SitPeriodoOperacion, ObjectId> {

    @Query("{'sdTipoCaptura.cveTipoCaptura':?0,'sdUnidadMedica.cvePresupuestal':?1,'cvePeriodo':?2}")
    SitPeriodoOperacion getPeriodoACerrar(Integer cveTipoCaptura, String cvePreupuestal, Integer cvePeriodo);

    @Query(value = "{'sdTipoCaptura.cveTipoCaptura':?0,'sdUnidadMedica.cvePresupuestal':?1,'cvePeriodo':?2}", count = true)
    Integer existePeriodo(Integer cveTipoCaptura, String cvePreupuestal, Integer cvePeriodo);

}
