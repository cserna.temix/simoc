package mx.gob.imss.simo.monitoreo.diassincapturar.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.monitoreo.common.constants.SQLColumnasConstants;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorDiasSinCapturar;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

/**
 * @author jonathan.lopez
 */
public class MonitorDiasSinCapturarRowMapperHelper extends BaseHelper implements RowMapper<MonitorDiasSinCapturar> {

	@Override
	public MonitorDiasSinCapturar mapRow(ResultSet resultSet, int rowId) throws SQLException {
		MonitorDiasSinCapturar monitorDiasSinCapturar = new MonitorDiasSinCapturar();

		monitorDiasSinCapturar.setFechaCaptura(resultSet.getString(SQLColumnasConstants.FECHA_CAPTURA));
		monitorDiasSinCapturar.setNumCeEspecialidad(resultSet.getInt(SQLColumnasConstants.NUM_CE_ESPECIALIDAD));
		monitorDiasSinCapturar.setColorCespecialidad(resultSet.getString(SQLColumnasConstants.COLOR_CESPECIALIDAD));
		monitorDiasSinCapturar.setNumCeUrgencia(resultSet.getInt(SQLColumnasConstants.NUM_CE_URGENCIA));
		monitorDiasSinCapturar.setColorCeurgencia(resultSet.getString(SQLColumnasConstants.COLOR_CEURGENCIA));
		monitorDiasSinCapturar.setNumEgresoHospitalizacion(resultSet.getInt(SQLColumnasConstants.NUM_EGRESO_HOSPITALIZACION));
		monitorDiasSinCapturar.setColorEgresohosp(resultSet.getString(SQLColumnasConstants.COLOR_EGRESOHOSP));
		monitorDiasSinCapturar.setNumIntQuirurgicas(resultSet.getInt(SQLColumnasConstants.NUM_INT_QUIRURGICAS));
		monitorDiasSinCapturar.setColorIntqx(resultSet.getString(SQLColumnasConstants.COLOR_INTQX));
		monitorDiasSinCapturar.setNumEgresoUrgencia(resultSet.getInt(SQLColumnasConstants.NUM_EGRESO_URGENCIA));
		monitorDiasSinCapturar.setColorEgresourg(resultSet.getString(SQLColumnasConstants.COLOR_EGRESOURG));
		
		return monitorDiasSinCapturar;
	}

}
