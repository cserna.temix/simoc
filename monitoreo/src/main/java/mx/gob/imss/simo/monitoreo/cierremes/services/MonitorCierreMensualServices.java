package mx.gob.imss.simo.monitoreo.cierremes.services;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.ConfCaptura;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.monitoreo.cierremes.model.DelegacionCierre;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierre;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierreAutomaticoMes;

/**
 * @author francisco.rodriguez
 */
public interface MonitorCierreMensualServices extends MonitoreoCommonServices {

    /**
     * Delegaciones conforme al perfil y periodo seleccionado.
     * 
     * @param annio
     * @param mes
     * @param delegacion
     * @param rol
     * @return
     */
    List<DelegacionCierre> obtenerDelegacionCierre(String annio, String mes, String delegacion, Integer rol);

    /**
     * Unidades Cerradas
     * 
     * @param annio
     * @param mes
     * @param delegacion
     * @param cvePresupuestal
     * @param banUnidad
     * @return
     */
    List<UnidadMedicaCierre> obtenerUnidadesCierre(String annio, String mes, String delegacion, String cvePresupuestal,
            Integer rol, List<mx.gob.imss.simo.model.UnidadMedica> unidades);

    /**
     * Negocio que valida si el periodo se cierra o abre
     * 
     * @param clavePresupuestal
     * @param tipoCaptura
     * @param clavePeriodoImss
     * @param indicadorPeriodo
     * @return
     * @throws HospitalizacionException
     */
    String cambiarEstatusPeriodo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss,
            Integer indicadorPeriodo, String unidadMedica, Integer indActual, Integer indMonitoreo)
            throws HospitalizacionException;

    /**
     * @param clavePresupuestal
     * @param tipoCaptura
     * @param clavePeriodoImss
     */
    void cerrarPeriodoMonitoreo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    /**
     * @param clavePresupuestal
     * @param tipoCaptura
     * @param clavePeriodoImss
     */
    void abrirPeriodoMonitoreo(String clavePresupuestal, Integer tipoCaptura, String clavePeriodoImss);

    /**
     * Se obtiene el �ltimo periodo que ser� permitido para ser abierto por monitoreo
     * 
     * @param clavePresupuestal
     * @return Periodo permitido
     */
    Integer obtenerUltimoPeriodoPermitidoMonitoreo(String clavePresupuestal);

    /**
     * Obtener el periodo, si exise alg�n periodo abierto por monitoreo,
     * 
     * @param clavePresupuestal
     * @return Periodo abierto o null
     */
    Integer obtenerPeriodoAbiertoMonitoreo(String clavePresupuestal);

    /**
     * Obtener el periodo mas reciente cerrado
     * 
     * @param clavePresupuestal
     * @return
     */
    Integer obtenerUltimoPeriodoCerrado(String clavePresupuestal);

    /**
     * Obtener el listado de claves presupuestales para el cierre automatico de mes por tipo de captura
     * 
     * @return
     */
    List<UnidadMedicaCierreAutomaticoMes> obtenerUnidadesMedicasParaCierreAutomaticoDeMes();

    Integer obtenerUltimoPeriodoPermitidoMonitoreoTipoCaptura(String clavePresupuestal, Integer tipoCaptura);

    Integer obtenerPeriodoAbiertoMonitoreoTipoCaptura(String clavePresupuestal, Integer tipoCaptura);

    Integer obtenerUltimoPeriodoCerradoTipoCaptura(String clavePresupuestal, Integer tipoCaptura);

    Integer obtenerTotalPeriodosAbiertos(String clavePresupuestal, Integer tipoCaptura);

    List<ConfCaptura> obtenerConfUnidad(String cvePresupuestal);

    List<PeriodoOperacion> obtenerPeriodosAbiertos(String cvePresupuestal, Integer tipoCaptura);
    
    void insertaBitacoraPeriodo(String cvePresupuestal, Integer tipoCaptura, String periodoImss, Integer movMonitoreo, String personalOperativo);

    List<String> filtrarCatalogoPresupuestal(List<String> datosCatalogoString, String query, Integer longitud);
}
