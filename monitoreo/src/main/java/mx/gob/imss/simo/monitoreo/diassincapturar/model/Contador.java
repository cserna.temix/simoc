package mx.gob.imss.simo.monitoreo.diassincapturar.model;

import java.util.Date;

import lombok.Data;

/**
 * 
 * @author jonathan.lopez
 *
 */
@Data
public class Contador {

	private Date fecha;
	private int numero;
}
