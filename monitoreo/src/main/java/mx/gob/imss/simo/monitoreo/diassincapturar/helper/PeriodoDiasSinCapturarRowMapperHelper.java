package mx.gob.imss.simo.monitoreo.diassincapturar.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.monitoreo.common.constants.SQLColumnasConstants;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.PeriodoDiasSinCapturar;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

/**
 * @author jonathan.lopez
 */
public class PeriodoDiasSinCapturarRowMapperHelper extends BaseHelper implements RowMapper<PeriodoDiasSinCapturar> {

	@Override
	public PeriodoDiasSinCapturar mapRow(ResultSet resultSet, int rowId) throws SQLException {
		PeriodoDiasSinCapturar periodoDiasSinCapturar = new PeriodoDiasSinCapturar();

		periodoDiasSinCapturar.setCveDelegacionImss(resultSet.getString(SQLColumnasConstants.CVE_DELEGACION_IMSS));
		periodoDiasSinCapturar.setDesDelegacionImss(resultSet.getString(SQLColumnasConstants.DES_DELEGACION_IMSS));
		periodoDiasSinCapturar.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
		periodoDiasSinCapturar.setDesUnidadMedica(resultSet.getString(SQLColumnasConstants.DES_UNIDAD_MEDICA));
		periodoDiasSinCapturar.setIndEspecialidadCe(resultSet.getShort(SQLColumnasConstants.IND_ESPECIALIDAD_CE));
		periodoDiasSinCapturar.setIndUrgenciaCe(resultSet.getShort(SQLColumnasConstants.IND_URGENCIA_CE));
		periodoDiasSinCapturar.setIndEgresoHospital(resultSet.getShort(SQLColumnasConstants.IND_EGRESO_HOSPITAL));
		periodoDiasSinCapturar.setIndIntQuirurgica(resultSet.getShort(SQLColumnasConstants.IND_INT_QUIRURGICA));
		periodoDiasSinCapturar.setIndEgresoUrgencia(resultSet.getShort(SQLColumnasConstants.IND_EGRESO_URGENCIA));
		periodoDiasSinCapturar.setFecInicial(resultSet.getDate(SQLColumnasConstants.FEC_INICIAL));
		periodoDiasSinCapturar.setFecFinal(resultSet.getDate(SQLColumnasConstants.FEC_FINAL));
		
		return periodoDiasSinCapturar;
	}

}
