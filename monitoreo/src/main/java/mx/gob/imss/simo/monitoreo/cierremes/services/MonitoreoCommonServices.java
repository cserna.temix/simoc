package mx.gob.imss.simo.monitoreo.cierremes.services;

import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.monitoreo.cierremes.model.SitPeriodoOperacion;

public interface MonitoreoCommonServices extends HospitalizacionCommonServices {

    /**
     * Periodos
     * 
     * @param anioPeriodo
     * @param mesPeriodo
     * @return
     */
    PeriodosImss obtenerPeriodoImss(String anioPeriodo, String mesPeriodo);

    /**
     * @param cvePresupuestal
     * @param cveTipoCaptura
     * @param cvePeriodoImss
     * @param periodoSiguienteS
     * @throws Exception
     */
    public void cerrarInsertarPeriodo(String cvePresupuestal, Integer cveTipoCaptura, String cvePeriodoImss,
            String periodoSiguienteS) throws Exception;

    /**
     * @param cvePresupuestal
     * @param cveTipoCaptura
     * @param cvePeriodoImss
     * @param periodoSiguienteS
     * @throws Exception
     */
    public void cerrarInsertarPeriodoManual(String cvePresupuestal, Integer cveTipoCaptura, String cvePeriodoImss,
            String periodoSiguienteS) throws Exception;

    /**
     * @param cvePresupuestal
     * @param cveTipoCaptura
     * @param cvePeriodoImss
     * @param periodoSiguienteS
     * @throws Exception
     */
    void cerrarEInsertarPeriodoConsultaExternaMongoOracle(String cvePresupuestal, Integer cveTipoCaptura,
            String cvePeriodoImss, String periodoSiguienteS) throws Exception;

    /**
     * @param cvePresupuestal
     * @param cveTipoCaptura
     * @param cvePeriodoImss
     * @param periodoSiguienteS
     * @throws Exception
     */

    void cerrarEInsertarPeriodoConsultaExternaMongoOracleManual(String cvePresupuestal, Integer cveTipoCaptura,
            String cvePeriodoImss, String periodoSiguienteS) throws Exception;

    /**
     * @param periodo
     * @return
     * @throws Exception
     */
    PeriodosImss obtenerSiguientePeriodoImss(String periodo) throws Exception;

    /**
     * @param cvePresupuestal
     * @param cveTipoCaptura
     * @param cvePeriodoImss
     * @param periodoSiguienteS
     * @return
     * @throws Exception
     */
    SitPeriodoOperacion obtenerPeriodoAcerrar(String cvePresupuestal, Integer cveTipoCaptura, Integer cvePeriodoImss)
            throws Exception;

    /**
     * @return
     */
    void actualizarPeriodoConsultaExternaMongo(SitPeriodoOperacion sitPeriodoOperacion);

}
