package mx.gob.imss.simo.monitoreo.common.constants;

public class SQLConstants {

    /* CU: Monitorear dias sin capturar */
    public static final short DIAS_SEMAFORO_VERDE = 5;
    public static final short DIAS_SEMAFORO_AMARICIO_INICIO = 6;
    public static final short DIAS_SEMAFORO_AMARICIO_FIN = 7;
    public static final short DIAS_SEMAFORO_ROJO = 8;

    public static final short NUM_SEMAFORO_VERDE = 1;
    public static final short NUM_SEMAFORO_AMARILLO = 2;
    public static final short NUM_SEMAFORO_ROJO = 3;
    public static final short NUM_SEMAFORO_GRIS = 4;
    public static final short NUM_SEMAFORO_BLANCO = 5;

    public static final Object[] TIPO_SERVICIO_ESPECIALIDADES = { 4, 5 };
    public static final Object[] TIPO_SERVICIO_URGENCIAS = { 2, 3 };
    public static final short TIPO_FORMATO_EGRESOS_HOSPITALARIOS = 11;
    public static final short TIPO_FORMATO_EGRESOS_CUNERO_PATOLOGICO = 15;
    public static final short TIPO_FORMATO_EGRESOS_URGENCIAS = 12;
    public static final String TABLA_CONSULTA_EXTERNA = "SIT_CONSULTA_EXTERNA";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_DELEGACION = "%FILTRO_DELEGACION%";
    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_UNIDADMEDICA = "%FILTRO_UNIDADMEDICA%";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_FILTRAR_DELEGACION = " where cve_delegacion_imss = ? ";
    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_FILTRAR_UNIDADMEDICA = " and cve_presupuestal = ? ";
    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_FILTRAR_UNIDADMEDICA_IN = " and cve_presupuestal in (";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_BUSCAR_DELEGACION = "select cve_delegacion_imss, concat(cve_delegacion_imss, concat(' ', ref_delegacion_imss)) ref_delegacion_imss, "
            + "      case when max(num_color) = 1 then 'fVerde' else case when max(num_color) = 2 then 'fAmarillo' else 'fRojo' end end color "
            + "from sit_dias_sin_captura " + MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_DELEGACION // -- parametro
                                                                                                      // delegacion en
                                                                                                      // caso de que sea
                                                                                                      // un usuario
                                                                                                      // especifico
            + "group by cve_delegacion_imss, ref_delegacion_imss " + "order by cve_delegacion_imss";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_BUSCAR_UNIDAD_MEDICA = "select cve_presupuestal, concat(cve_presupuestal, concat(' ', ref_unidad_medica)) ref_unidad_medica, "
            + "      case when max(num_color) = 1 then 'fVerde' else case when max(num_color) = 2 then 'fAmarillo' else 'fRojo' end end color "
            + "from sit_dias_sin_captura " + "where cve_delegacion_imss = ? " // -- parametro delegacion seleccionada
            + MONITOREAR_DIAS_SIN_CAPTURAR_CADENA_REEMPLAZO_UNIDADMEDICA // -- parametro unidad medica en caso de que
                                                                         // sea un usuario especifico
            + "group by cve_presupuestal, ref_unidad_medica " + "order by cve_presupuestal";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_GRID = "select to_char(fecha_captura, 'dd/mm/yyyy') fecha_captura_monitor, num_ce_especialidad, num_ce_urgencia, num_egreso_hospitalizacion, num_int_quirurgicas, num_egreso_urgencia, "
            + "      case when num_color_cespecialidad = 1 then 'fVerde' else case when num_color_cespecialidad = 2 then 'fAmarillo' "
            + "            else case when num_color_cespecialidad = 3 then 'fRojo' else case when num_color_cespecialidad = 4 then 'fGris' "
            + "            else 'fBlanco' end end end end color_cespecialidad, "
            + "      case when num_color_ceurgencia = 1 then 'fVerde' else case when num_color_ceurgencia = 2 then 'fAmarillo' "
            + "            else case when num_color_ceurgencia = 3 then 'fRojo' else case when num_color_ceurgencia = 4 then 'fGris' "
            + "            else 'fBlanco' end end end end color_ceurgencia, "
            + "      case when num_color_egresohosp = 1 then 'fVerde' else case when num_color_egresohosp = 2 then 'fAmarillo' "
            + "            else case when num_color_egresohosp = 3 then 'fRojo' else case when num_color_egresohosp = 4 then 'fGris' "
            + "            else 'fBlanco' end end end end color_egresohosp, "
            + "      case when num_color_intqx = 1 then 'fVerde' else case when num_color_intqx = 2 then 'fAmarillo' "
            + "            else case when num_color_intqx = 3 then 'fRojo' else case when num_color_intqx = 4 then 'fGris' "
            + "            else 'fBlanco' end end end end color_intqx, "
            + "      case when num_color_egresourg = 1 then 'fVerde' else case when num_color_egresourg = 2 then 'fAmarillo' "
            + "            else case when num_color_egresourg = 3 then 'fRojo' else case when num_color_egresourg = 4 then 'fGris' "
            + "            else 'fBlanco' end end end end color_egresourg " + "from sit_dias_sin_captura "
            + "where cve_delegacion_imss = ? " // -- parametro delegacion seleccionada
            + "      and cve_presupuestal = ? " // -- parametro unidad medica seleccionada
            + "order by fecha_captura";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_PERIODOS = "select sdi.cve_delegacion_imss, sdi.des_delegacion_imss, sumed.cve_presupuestal, sumed.des_unidad_medica, "
            + "    scf.ind_especialidad_ce, scf.ind_urgencia_ce, scf.ind_egreso_hospital, scf.ind_int_quirurgica, scf.ind_egreso_urgencia, "
            + "    trunc(min(spi.fec_inicial)) fec_inicial, trunc(max(spi.fec_final)) fec_final "
            + "from sic_conf_dia_sincaptura scf, sit_periodo_operacion spo, sic_periodos_imss spi, sic_unidad_medica sumed, sic_delegacion_imss sdi "
            + "where scf.cve_presupuestal = spo.cve_presupuestal and spo.cve_periodo_imss = spi.cve_periodo_imss "
            + "    and spo.ind_cerrado = 0 and spo.ind_actual = 1 and spo.ind_monitoreo = 0 "
            + "    and (spo.cve_tipo_captura = 1 or spo.cve_tipo_captura = 2) " // -- parametro que indica si es
                                                                                // consulta externa o hospitalizacion
            + "    and sumed.cve_presupuestal = scf.cve_presupuestal and sumed.cve_delegacion_imss = sdi.cve_delegacion_imss "
            + "group by sdi.cve_delegacion_imss, sdi.des_delegacion_imss, sumed.cve_presupuestal, sumed.des_unidad_medica, "
            + "    scf.cve_presupuestal, scf.ind_especialidad_ce, scf.ind_urgencia_ce, scf.ind_egreso_hospital, "
            + "    scf.ind_int_quirurgica, scf.ind_egreso_urgencia";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_EGRESOS_HOSPITALARIOS_URGENCIAS = "select trunc(se.fec_egreso) fecha, count(1) numero "
            + "from sit_ingreso si, sit_egreso se " + "where si.cve_presupuestal = ? and se.cve_tipo_formato = ? "
            + "      and si.cve_ingreso = se.cve_egreso " + "      and trunc(se.fec_egreso) between ? and ? "
            + "group by trunc(se.fec_egreso) " + "order by 1";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_EGRESOS_HOSPITALARIOS = "select trunc(se.fec_egreso) fecha, count(1) numero "
            + "from sit_ingreso si, sit_egreso se " + "where si.cve_presupuestal = ? and se.cve_tipo_formato in (?,?) "
            + "      and si.cve_ingreso = se.cve_egreso " + "      and trunc(se.fec_egreso) between ? and ? "
            + "group by trunc(se.fec_egreso) " + "order by 1";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_INTERVENCIONES_QUIRURGICAS = "select trunc(siq.fec_intervencion_iqx) fecha, count(1) numero "
            + "from sit_ingreso si, sit_intervencion_quirurgica siq " + "where si.cve_presupuestal = ? "
            + "      and si.cve_ingreso = siq.cve_ingreso "
            + "      and trunc(siq.fec_intervencion_iqx) between ? and ? " + "group by trunc(siq.fec_intervencion_iqx) "
            + "order by 1";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_DELETE = "delete from sit_dias_sin_captura";
    
    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_BUSCAR = "select count(sd.cve_presupuestal) from sit_dias_sin_captura sd where sd.cve_delegacion_imss = ? and sd.cve_presupuestal = ? and sd.fecha_captura = ? ";

    public static final String MONITOREAR_DIAS_SIN_CAPTURAR_INSERT = "insert into sit_dias_sin_captura ( "
            + "      cve_delegacion_imss, cve_presupuestal, fecha_captura, ref_unidad_medica, ref_delegacion_imss, num_ce_especialidad, num_ce_urgencia, "
            + "      num_egreso_hospitalizacion, num_int_quirurgicas, num_egreso_urgencia, num_color_cespecialidad, "
            + "      num_color_ceurgencia, num_color_egresohosp, num_color_intqx, num_color_egresourg, num_color) "
            + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    /* CU: Monitorear cierre de mes */
    public static final String OBTENER_DIAS_FERIADOS_IMSS = "SELECT * FROM SIC_DIA_FESTIVO DFE WHERE DFE.FEC_DIA_FESTIVO <=(TO_DATE(?, 'DD/MM/YYYY')) "
            + "AND EXTRACT(YEAR FROM DFE.FEC_DIA_FESTIVO)=EXTRACT(YEAR FROM TO_DATE(?, 'DD/MM/YYYY'))";

    public static final String OBTENER_PERIODOS_OPERACION_ACTIVOS_POR_PERIODO = "SELECT DISTINCT OP.* FROM SIT_PERIODO_OPERACION OP,SIC_CONF_CAPTURA_UNIDAD AC "
            + "WHERE  OP.CVE_PERIODO_IMSS=? AND OP.IND_CERRADO='0' AND OP.IND_MONITOREO='0' AND AC.IND_CONF_ACTIVA='1'";

    public static final String BUSCAR_SIGUIENTE_PERIODO_IMSS = "SELECT * FROM SIC_PERIODOS_IMSS SI WHERE SI.CVE_PERIODO_IMSS > ? AND  rownum = 1";

    public static final String INSERTAR_BITACORA_PROCESO = "INSERT INTO SIT_BITACORA_PROCESO(CVE_PRESUPUESTAL, CVE_PERIODO_IMSS, CVE_PROCESO, CVE_ESTADO_PROCESO, NUM_INTENTO, REF_ERROR, FEC_INICIO, FEC_FIN)"
            + " VALUES(?,?,?,?,?,?,SYSDATE,SYSDATE)";

    public static final String UPDATE_FIN_SIT_BITACORA_PROCESO = "UPDATE SIT_BITACORA_PROCESO "
            + " SET CVE_ESTADO_PROCESO = ?, FEC_FIN = SYSDATE WHERE CVE_PRESUPUESTAL = ?  AND CVE_PERIODO_IMSS = ? "
            + " AND CVE_PROCESO = ? ";

    public static final String SELECT_SIT_BITACORA_PROCESO = " SELECT COUNT(BP.CVE_PRESUPUESTAL) RESULTADO FROM SIT_BITACORA_PROCESO BP "
            + " WHERE BP.CVE_PRESUPUESTAL = ? AND BP.CVE_PERIODO_IMSS = ? AND BP.CVE_PROCESO = ? ";

    public static final String INSERT_NUEVO_PERIODO_OPERACION = "INSERT INTO SIT_PERIODO_OPERACION (CVE_PRESUPUESTAL, CVE_TIPO_CAPTURA, CVE_PERIODO_IMSS, IND_CERRADO, FEC_CIERRE,IND_ACTUAL, IND_MONITOREO) VALUES (?, ?, ?, ?, '', ?, ?)";

    public static final String CERRAR_PERIODO_UNIDAD_MONITOREO = "UPDATE SIT_PERIODO_OPERACION O SET O.IND_CERRADO = 1, O.IND_ACTUAL = 0, O.IND_MONITOREO=0 WHERE O.CVE_PRESUPUESTAL = ? AND O.CVE_TIPO_CAPTURA = ? AND O.CVE_PERIODO_IMSS = ?";

    public static final String ABRIR_PERIODO_UNIDAD_MONITOREO = "UPDATE SIT_PERIODO_OPERACION O SET O.IND_CERRADO = 0,  O.IND_ACTUAL = 0, O.IND_MONITOREO=1 WHERE O.CVE_PRESUPUESTAL = ? AND O.CVE_TIPO_CAPTURA = ? AND O.CVE_PERIODO_IMSS = ?";

    public static final String OBTENER_ULTIMO_PERIODO_PERMITIDO = "SELECT max(SI.CVE_PERIODO_IMSS) FROM SIC_PERIODOS_IMSS SI  "
            + "WHERE IND_VIGENTE='1' AND SI.CVE_PERIODO_IMSS<(SELECT MIN(S.CVE_PERIODO_IMSS) FROM SIT_PERIODO_OPERACION S "
            + "WHERE  IND_ACTUAL=1 AND CVE_PRESUPUESTAL=? )";

    public static final String OBTENER_ULTIMO_PERIODO_PERMITIDO_TIPO_CAPTURA = "SELECT max(SI.CVE_PERIODO_IMSS) FROM SIC_PERIODOS_IMSS SI  "
            + "WHERE IND_VIGENTE='1' AND SI.CVE_PERIODO_IMSS<=(SELECT MIN(S.CVE_PERIODO_IMSS) FROM SIT_PERIODO_OPERACION S "
            + "WHERE  IND_ACTUAL=1 AND CVE_PRESUPUESTAL=? AND S.CVE_TIPO_CAPTURA=? )";

    public static final String OBTENER_PERIODOS_ABIERTOS_TOTALES = "SELECT COUNT(*) FROM SIT_PERIODO_OPERACION WHERE CVE_PRESUPUESTAL=? AND CVE_TIPO_CAPTURA=? AND (IND_CERRADO=0 OR IND_MONITOREO=1)";

    public static final String OBTENER_PERIODO_ABIERTO_POR_MONITOREO = "SELECT max(S.CVE_PERIODO_IMSS) FROM SIT_PERIODO_OPERACION S WHERE S.CVE_PRESUPUESTAL=? AND IND_MONITOREO=1";

    public static final String OBTENER_PERIODO_ABIERTO_POR_MONITOREO_TIPO_CAPTURA = "SELECT max(S.CVE_PERIODO_IMSS) FROM SIT_PERIODO_OPERACION S WHERE S.CVE_PRESUPUESTAL=? AND S.CVE_TIPO_CAPTURA=? AND IND_MONITOREO=1";

    public static final String OBTENER_PERIODO_MAS_RECIENTE_CERRADO = "SELECT max(S.CVE_PERIODO_IMSS) FROM SIT_PERIODO_OPERACION S WHERE S.CVE_PRESUPUESTAL=? AND IND_CERRADO=1 order by S.CVE_PERIODO_IMSS";

    public static final String OBTENER_PERIODO_MAS_RECIENTE_CERRADO_TIPO_CAPTURA = "SELECT max(S.CVE_PERIODO_IMSS) FROM SIT_PERIODO_OPERACION S WHERE S.CVE_PRESUPUESTAL=? AND  IND_CERRADO=1 AND S.CVE_TIPO_CAPTURA=? order by S.CVE_PERIODO_IMSS";

    public static final String EXISTE_PERIODO_OPERACION = "SELECT COUNT(OP.CVE_PERIODO_IMSS) FROM SIT_PERIODO_OPERACION OP WHERE CVE_PRESUPUESTAL=? AND CVE_PERIODO_IMSS=? AND  CVE_TIPO_CAPTURA=?";

    public static final String CONSULTAR_DIA_INHABIL = "SELECT COUNT(DFE.FEC_DIA_FESTIVO) FROM SIC_DIA_FESTIVO DFE WHERE DFE.FEC_DIA_FESTIVO = ?";

    public static final String CONSULTAR_UNIDAD_HUESPED = "SELECT COUNT(*)FROM SIC_PERSONAL_UNIDAD where REF_PERSONAL_OPERATIVO = ? AND IND_TITULAR = 0";

    public static final String OBTENER_UNIDADESMEDICAS_CIERRE_AUTOMATICO_MES = "select spo.cve_presupuestal, spo.cve_tipo_captura, spo.cve_periodo_imss, spi.fec_inicial, spi.fec_final "
            + "from sit_periodo_operacion spo, sic_conf_captura_unidad sccu, sic_periodos_imss spi "
            + "where spo.ind_cerrado = 0 and spo.ind_monitoreo = 0 and spo.ind_actual = 1 and sccu.ind_conf_activa = 1 "
            + "      and spo.cve_presupuestal = sccu.cve_presupuestal and spo.cve_tipo_captura = sccu.cve_tipo_captura "
            + "      and spo.cve_periodo_imss = spi.cve_periodo_imss order by  spo.cve_presupuestal, spo.cve_periodo_imss, spo.cve_tipo_captura";

    public static final String BUSCAR_UNIDADES_PERIODO = "SELECT * FROM SIT_PERIODO_OPERACION OP WHERE OP.cve_presupuestal=? AND OP.cve_periodo_imss=?";

    public static final String BUSCAR_UNIDADES_CONF = "SELECT CONF.* FROM  SIC_UNIDAD_MEDICA UN,  SIC_CONF_CAPTURA_UNIDAD CONF  WHERE UN.CVE_PRESUPUESTAL=? AND CONF.CVE_PRESUPUESTAL=UN.CVE_PRESUPUESTAL";

    public static final String OBTENER_PERIODOS_ABIERTOS = "SELECT * FROM SIT_PERIODO_OPERACION WHERE CVE_PRESUPUESTAL=? AND CVE_TIPO_CAPTURA=? AND (IND_CERRADO=0 OR IND_MONITOREO=1)";
    
    public static final String INSERTAR_BITACORA_CIERRE_MES = "INSERT INTO SIT_BITACORA_CIERRE_MES (CVE_BITACORA_CIERRE_MES, CVE_PRESUPUESTAL, CVE_TIPO_CAPTURA, CVE_PERIODO_IMSS, IND_MOV_MONITOREO, FEC_MOV_MONITOREO, REF_PERSONAL_OPERATIVO)"
    		+ "VALUES (?, ?, ?, ?, ?, SYSDATE, ?)";
}
