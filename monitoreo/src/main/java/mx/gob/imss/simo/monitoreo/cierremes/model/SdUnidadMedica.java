/**
 * 
 */
package mx.gob.imss.simo.monitoreo.cierremes.model;

/**
 * @author francisco.rrios
 */
public class SdUnidadMedica {

    private String cvePresupuestal;
    private String desUnidadMedica;

    public SdUnidadMedica() {
        super();
    }

    public SdUnidadMedica(String cvePresupuestal, String desUnidadMedica) {
        super();
        this.cvePresupuestal = cvePresupuestal;
        this.desUnidadMedica = desUnidadMedica;
    }

    /**
     * @return the Presupuestal
     */
    public String getCvePresupuestal() {

        return cvePresupuestal;
    }

    /**
     * @param cvePresupuestal
     *            the cvePresupuestal to set
     */
    public void setCvePresupuestal(String cvePresupuestal) {

        this.cvePresupuestal = cvePresupuestal;
    }

    /**
     * @return the desUnidadMedica
     */
    public String getDesUnidadMedica() {

        return desUnidadMedica;
    }

    /**
     * @param desUnidadMedica
     *            the desUnidadMedica to set
     */
    public void setDesUnidadMedica(String desUnidadMedica) {

        this.desUnidadMedica = desUnidadMedica;
    }

}
