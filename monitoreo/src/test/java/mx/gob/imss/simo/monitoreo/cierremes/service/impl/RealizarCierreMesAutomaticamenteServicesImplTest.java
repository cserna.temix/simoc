package mx.gob.imss.simo.monitoreo.cierremes.service.impl;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierreAutomaticoMes;
import mx.gob.imss.simo.monitoreo.cierremes.repository.MonitoreoCommonOracleRepository;
import mx.gob.imss.simo.monitoreo.cierremes.services.MonitoreoCommonServices;
import mx.gob.imss.simo.monitoreo.cierremes.services.RealizarCierreMesAutomaticamenteServices;
import mx.gob.imss.simo.monitoreo.cierremes.services.impl.MonitoreoCommonServicesImpl;
import mx.gob.imss.simo.monitoreo.cierremes.services.impl.RealizarCierreMesAutomaticamenteServicesImpl;

public class RealizarCierreMesAutomaticamenteServicesImplTest {

    @Mock
    RealizarCierreMesAutomaticamenteServices realizarCierreMesAutomaticamenteServicesImpl;

    @Mock
    MonitoreoCommonOracleRepository monitoreoCommonOracleRepository;

    @Mock
    MonitoreoCommonServicesImpl monitoreoCommonServicesImpl;

    @Mock
    MonitoreoCommonServices monitoreoCommonServices;

    final static Logger logger = LoggerFactory.getLogger(RealizarCierreMesAutomaticamenteServicesImplTest.class);
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        realizarCierreMesAutomaticamenteServicesImpl = new RealizarCierreMesAutomaticamenteServicesImpl();
        monitoreoCommonServicesImpl = new MonitoreoCommonServicesImpl();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void cerrarMesAutomaticamenteTest() throws ParseException {

        PeriodosImss periodoSiguiente = new PeriodosImss();
        periodoSiguiente.setClavePeriodo("201802");
        periodoSiguiente.setDescripcionMesPeriodo("Febrero");
        periodoSiguiente.setFechaBaja(null);
        periodoSiguiente.setFechaFinal(formatoFecha.parse("25/02/2018"));
        periodoSiguiente.setFechaInicial(formatoFecha.parse("26/01/2018"));
        periodoSiguiente.setIndicadorVigente(true);
        periodoSiguiente.setNumeroAnioPeriodo("2018");

        UnidadMedicaCierreAutomaticoMes cierreAutomatico = new UnidadMedicaCierreAutomaticoMes();
        cierreAutomatico.setCvePeriodoImss("201802");
        cierreAutomatico.setCvePresupuestal("160102142151");
        cierreAutomatico.setCveTipoCaptura(new Short("2"));
        cierreAutomatico.setFecFinal(formatoFecha.parse("26/01/2018"));
        cierreAutomatico.setFecInicial(formatoFecha.parse("25/02/2018"));

        List<UnidadMedicaCierreAutomaticoMes> listCierreAutomatico = new ArrayList<UnidadMedicaCierreAutomaticoMes>();
        listCierreAutomatico.add(cierreAutomatico);

        Date fechaCierreMesXUnidad = formatoFecha.parse("28/01/2018");

        try {

            realizarCierreMesAutomaticamenteServicesImpl.cerrarMesAutomaticamente();
            when(realizarCierreMesAutomaticamenteServicesImpl.obtenerUnidadesMedicasParaCierreAutomaticoDeMes())
                    .thenReturn(listCierreAutomatico);
            when(monitoreoCommonOracleRepository.obtenerSiguientePeriodoImss(anyString())).thenReturn(periodoSiguiente);
            doNothing().when(monitoreoCommonServicesImpl).cerrarInsertarPeriodo(anyString(), anyInt(), anyString(),
                    anyString());
            when(monitoreoCommonOracleRepository.buscarBitacora(anyString(), anyString(), anyInt()))
                    .thenReturn(anyInt());
            when(monitoreoCommonOracleRepository.existePeriodoOperacion(anyString(), anyString(), 0)).thenReturn(1);

            logger.info("Test cerrarMesAutomaticamente");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void contarDiasHabilesTest() throws ParseException {

        Date fecha = formatoFecha.parse("26/01/2018");

        List<Date> listDiasFeriados = new ArrayList<Date>();
        listDiasFeriados.add(formatoFecha.parse("27/01/2018"));

        Date fechaFinal = formatoFecha.parse("31/01/2018");
        
		Mockito.when(realizarCierreMesAutomaticamenteServicesImpl.contarDiasHabiles(fecha, 3, listDiasFeriados))
		.thenReturn(fechaFinal);

        Date resultTest = realizarCierreMesAutomaticamenteServicesImpl.contarDiasHabiles(fecha, 3, listDiasFeriados);
        Assert.assertNotNull(resultTest);
        logger.info("Test contarDiasHabiles");
        logger.info("Fecha Final: {}" + resultTest);

    }

    @Test
    public void obtenerListaPresupuestalesAbiertasActivasTest() {

        PeriodoOperacion periodoOperacion = new PeriodoOperacion();
        periodoOperacion.setClavePeriodoIMSS("201802");
        periodoOperacion.setClavePresupuestal("160102142151");
        periodoOperacion.setFechaCierre(null);
        periodoOperacion.setIndicadorActual(true);
        periodoOperacion.setIndicadorCerrado(false);
        periodoOperacion.setIndicadorMonitoreo(true);
        periodoOperacion.setTipoCaptura(3);

        List<PeriodoOperacion> periodoOperacionList = new ArrayList<PeriodoOperacion>();
        periodoOperacionList.add(periodoOperacion);

        try {

            when(monitoreoCommonOracleRepository.obtenerListaPresupuestalesAbiertasActivas(anyString()))
                    .thenReturn(periodoOperacionList);
            List<PeriodoOperacion> resultTest = realizarCierreMesAutomaticamenteServicesImpl
                    .obtenerListaPresupuestalesAbiertasActivas("201802");
            Assert.assertNotNull(resultTest);
            logger.info("Test obtenerListaPresupuestalesAbiertasActivas");
            logger.info("Clave presupuestal activa: {}" + resultTest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void obtenerParametroBaseDatosTest() {

        String desParametro = "10";
        String parametro = "N�mero de d�as h�biles para ejecutar el proceso de cierre autom�tico";

        try {

            when(monitoreoCommonOracleRepository.obtenerParametroBaseDatos(anyString())).thenReturn(desParametro);
            int resultTest = realizarCierreMesAutomaticamenteServicesImpl.obtenerParametroBaseDatos(parametro);
            Assert.assertNotNull(resultTest);
            logger.info("Test obtenerParametroBaseDatos");
            logger.info("REF_PARAMETRO: {}" + resultTest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void obtenerDiasNoHabilesTest() throws ParseException {

        List<Date> listDiasFeriados = new ArrayList<Date>();
        listDiasFeriados.add(formatoFecha.parse("27/01/2018"));
        try {

            when(monitoreoCommonOracleRepository.obtenerDiasNoHabiles(formatoFecha.parse("26/01/2018")))
                    .thenReturn(listDiasFeriados);
            List<Date> resultTest = realizarCierreMesAutomaticamenteServicesImpl
                    .obtenerDiasNoHabiles(formatoFecha.parse("26/01/2018"));
            Assert.assertNotNull(resultTest);
            logger.info("Test obtenerDiasNoHabiles");
            logger.info("Dias no habiles: {}" + resultTest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void obtenerUnidadesMedicasParaCierreAutomaticoDeMesTest() throws ParseException {

        UnidadMedicaCierreAutomaticoMes unidadMedicaCierreAutomaticoMes = new UnidadMedicaCierreAutomaticoMes();
        unidadMedicaCierreAutomaticoMes.setCvePeriodoImss("201802");
        unidadMedicaCierreAutomaticoMes.setCvePresupuestal("160102142151");
        unidadMedicaCierreAutomaticoMes.setCveTipoCaptura(null);
        unidadMedicaCierreAutomaticoMes.setFecInicial(formatoFecha.parse("26/01/2018"));
        unidadMedicaCierreAutomaticoMes.setFecFinal(formatoFecha.parse("25/02/2018"));

        List<UnidadMedicaCierreAutomaticoMes> listUnidadMedicaCierre = new ArrayList<UnidadMedicaCierreAutomaticoMes>();
        listUnidadMedicaCierre.add(unidadMedicaCierreAutomaticoMes);

        try {
            doReturn(listUnidadMedicaCierre).when(monitoreoCommonOracleRepository)
                    .obtenerUnidadesMedicasParaCierreAutomaticoDeMes();

            List<UnidadMedicaCierreAutomaticoMes> resultTest = realizarCierreMesAutomaticamenteServicesImpl
                    .obtenerUnidadesMedicasParaCierreAutomaticoDeMes();
            Assert.assertNotNull(resultTest);

            logger.info("Test obtenerUnidadesMedicasParaCierreAutomaticoDeMes");
            logger.info("Unidades M�dicas para cierre de mes autom�tico: {}" + resultTest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
