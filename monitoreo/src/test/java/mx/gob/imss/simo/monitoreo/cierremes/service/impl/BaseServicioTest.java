package mx.gob.imss.simo.monitoreo.cierremes.service.impl;

import java.util.Random;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Esta clase establece por medio de anotaciones la configuracion para ejecutar
 * los tests.
 * - Primero se indica que JUnit sera el encargado de ejecutar las mismas y
 * evaluar los resultados. - Despues el archivo que contiene la configuracion de
 * spring para iniciar el contendor y sus beans. - Se activa el profile con el
 * que se debe iniciar el contenedor. - Se indica el nombre del bean que se
 * encargara de manejar la transaccion. Ademas para el caso de los tests, por
 * definicion siempre debemos regresar al estado de los datos en que iniciamos,
 * asi que al terminar cada test se hara rollback. - Por ultimo indicamos
 * con @Transactional que todas los test, a menos que indiquemos lo contrario,
 * iniciaran una transaccion.
 * 
 * @author Brian Hernandez Garcia
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)

@ContextConfiguration(locations={ "classpath:*/test-main-context.xml"		
		})
//@ContextConfiguration(locations = {"classpath:*application-context-test.xml" })
//@ContextConfiguration(classes = {TestContext.class })0


@ActiveProfiles("testing")
public abstract class BaseServicioTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected String buildStringWithRandomCharacters(final int length) {

        logger.debug("Inicializando prueba");
        final StringBuilder result = new StringBuilder(length);
        final Random random = new Random();
        for (int index = 0; index < length; index++) {
            result.append(Character.toChars(random.nextInt(26) + 65));
        }
        return result.toString();
    }

    protected String buildStringOfSpaces(final int length) {

        logger.debug("Obteniendo random");
        return String.format("%1$" + length + 's', "");
    }
    
    

}
