package mx.gob.imss.simo.monitoreo.diassincapturar.service.implTest;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.model.UnidadMedica;
import mx.gob.imss.simo.monitoreo.diassincapturar.helper.PeriodoDiasSinCapturarRowMapperHelper;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.DelegacionDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.MonitorInsertDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.PeriodoDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.model.UnidadMedicaDiasSinCapturar;
import mx.gob.imss.simo.monitoreo.diassincapturar.repository.impl.MonitorDiasSinCapturarRepositoryImpl;
import mx.gob.imss.simo.monitoreo.diassincapturar.service.impl.MonitorDiasSinCapturarServicesImpl;

public class MonitorDiasSinCapturarServiceImplTest {

	@InjectMocks
	MonitorDiasSinCapturarServicesImpl monitorDiasSinCapturarServices;

	@Mock
	MonitorDiasSinCapturarRepositoryImpl monitorDiasSinCapturarRepositoryImpl;

	@Mock
	PeriodoDiasSinCapturarRowMapperHelper periodoDiasSinCapturarRowMapperHelper;

	@Mock
	MonitorDiasSinCapturarServicesImpl monitorDiasSinCapturarServicesImplMock;

	final static Logger logger = LoggerFactory.getLogger(MonitorDiasSinCapturarServiceImplTest.class);

	@BeforeClass
	public static void ini() {

		BasicConfigurator.configure();
	}

	@Before
	public void setup() {

		monitorDiasSinCapturarRepositoryImpl = new MonitorDiasSinCapturarRepositoryImpl();
		periodoDiasSinCapturarRowMapperHelper = new PeriodoDiasSinCapturarRowMapperHelper();
		monitorDiasSinCapturarServicesImplMock = new MonitorDiasSinCapturarServicesImpl();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void obtenerDelegacionDiasSinCapturarTest() {

		String cveDelegacion = "IZTA123";
		boolean filtraDelegacion = true;

		List<DelegacionDiasSinCapturar> delegacionDiasSinCapturarList = new ArrayList<DelegacionDiasSinCapturar>();
		DelegacionDiasSinCapturar delegacionDiasSinCapturar = new DelegacionDiasSinCapturar();
		delegacionDiasSinCapturarList.add(delegacionDiasSinCapturar);

		try {
			Mockito.when(
					monitorDiasSinCapturarRepositoryImpl.obtenerDelegacionDiasSinCapturar(anyString(), anyBoolean()))
					.thenReturn(delegacionDiasSinCapturarList);

			monitorDiasSinCapturarServices.obtenerDelegacionDiasSinCapturar(cveDelegacion, filtraDelegacion);

			Assert.assertEquals(cveDelegacion, "IZTA123");
			Assert.assertTrue(filtraDelegacion);

			logger.info("Calve Delegación:{}", cveDelegacion);
			logger.info("Filtra Delegación:{}", filtraDelegacion);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	@Test
	public void obtenerUnidadesMedicasDiasSinCapturarTest() {

		String cveDelegacion = "CUAU456";

		List<UnidadMedica> unidadMedicaList = new ArrayList<UnidadMedica>();
		UnidadMedica unidadMedica = new UnidadMedica();
		unidadMedica.setCvePresupuestal("1");
		unidadMedicaList.add(unidadMedica);

		List<UnidadMedicaDiasSinCapturar> unidadMedicaDiasSinCapturarList = new ArrayList<UnidadMedicaDiasSinCapturar>();
		UnidadMedicaDiasSinCapturar unidadMedicaDiasSinCapturar = new UnidadMedicaDiasSinCapturar();
		unidadMedicaDiasSinCapturarList.add(unidadMedicaDiasSinCapturar);

		boolean filtrarUnidadMedica = true;

		try {
			Mockito.when(monitorDiasSinCapturarRepositoryImpl.obtenerUnidadesMedicasDiasSinCapturar(anyString(),
					anyList(), anyBoolean())).thenReturn(unidadMedicaDiasSinCapturarList);
			monitorDiasSinCapturarServices.obtenerUnidadesMedicasDiasSinCapturar(cveDelegacion, unidadMedicaList,
					filtrarUnidadMedica);

			Assert.assertEquals(cveDelegacion, "CUAU456");
			Assert.assertTrue(filtrarUnidadMedica);

			logger.info("Clave delegación:{}", cveDelegacion);
			logger.info("Filtrar unidad medica:{}", filtrarUnidadMedica);
			logger.info("Presupuestal:{}", unidadMedica.getCvePresupuestal());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void obtenerMonitorDiasSinCapturarTest() {

		String cveDelegacion = "NEZ4567";
		String cvePresupuestal = "34656783768";
		List<MonitorDiasSinCapturar> monitorDiasSinCapturarList = new ArrayList<MonitorDiasSinCapturar>();
		MonitorDiasSinCapturar monitorDiasSinCapturar = new MonitorDiasSinCapturar();
		monitorDiasSinCapturar.setColorEgresohosp("Rojo");
		monitorDiasSinCapturarList.add(monitorDiasSinCapturar);

		try {
			Mockito.when(monitorDiasSinCapturarRepositoryImpl.obtenerMonitorDiasSinCapturar(anyString(), anyString()))
					.thenReturn(monitorDiasSinCapturarList);
			monitorDiasSinCapturarServices.obtenerMonitorDiasSinCapturar(cveDelegacion, cvePresupuestal);

			Assert.assertEquals(cveDelegacion, "NEZ4567");
			Assert.assertNotNull(monitorDiasSinCapturar.getColorEgresohosp());

			logger.info("Presupuestal:{}", cvePresupuestal);
			logger.info("Egreso:{}", monitorDiasSinCapturar.getColorEgresohosp());
			logger.info("Delegación:{}", cveDelegacion);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void obtenerPeriodosDiasSinCapturarTest() {
		String sql = "1";
		ResultSet resultSet;
		int rowId = 2;
		List<PeriodoDiasSinCapturar> periodoDiasSinCapturarList = new ArrayList<PeriodoDiasSinCapturar>();
		PeriodoDiasSinCapturar periodoDiasSinCapturar = new PeriodoDiasSinCapturar();
		periodoDiasSinCapturar.setCvePresupuestal("2345678");
		periodoDiasSinCapturar.setCveDelegacionImss("CUAJI23");
		periodoDiasSinCapturar.setDesDelegacionImss("CUAJIMALPA");
		periodoDiasSinCapturar.setDesUnidadMedica("HGRZ 32");
		periodoDiasSinCapturar.setIndEspecialidadCe((short) 0);
		periodoDiasSinCapturar.setIndUrgenciaCe((short) 0);
		periodoDiasSinCapturar.setIndEgresoHospital((short) 0);
		periodoDiasSinCapturar.setIndIntQuirurgica((short) 0);
		periodoDiasSinCapturar.setIndEgresoUrgencia((short) 0);
		periodoDiasSinCapturar.setFecInicial(new Date());
		periodoDiasSinCapturar.setFecFinal(new Date());
		periodoDiasSinCapturarList.add(periodoDiasSinCapturar);
		try {

			Mockito.when(monitorDiasSinCapturarRepositoryImpl.obtenerPeriodosDiasSinCapturar())
					.thenReturn(periodoDiasSinCapturarList);

			monitorDiasSinCapturarServices.obtenerPeriodosDiasSinCapturar();

			Assert.assertEquals(periodoDiasSinCapturar.getDesUnidadMedica(), "HGRZ 32");
			Assert.assertNotNull(periodoDiasSinCapturar.getDesDelegacionImss(), "CUAJIMALPA");

			logger.info("Presupuestal:{}", periodoDiasSinCapturar.getCvePresupuestal());
			logger.info("Unidad médica:{}", periodoDiasSinCapturar.getDesUnidadMedica());
			logger.info("Clave delegación:{}", periodoDiasSinCapturar.getCveDelegacionImss());
			logger.info("Delegación:{}", periodoDiasSinCapturar.getDesDelegacionImss());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void procesarMonitorDiasSinCapturarTest() {
		List<MonitorInsertDiasSinCapturar> monitorInsertDiasSinCapturarList = new ArrayList<MonitorInsertDiasSinCapturar>();
		MonitorInsertDiasSinCapturar monitorInsertDiasSinCapturar = new MonitorInsertDiasSinCapturar();
		monitorInsertDiasSinCapturar.setFechaCaptura(new Date());
		monitorInsertDiasSinCapturar.setCveDelegacionImss("NEZA456");
		monitorInsertDiasSinCapturarList.add(monitorInsertDiasSinCapturar);

		List<PeriodoDiasSinCapturar> periodoDiasSinCapturarList = new ArrayList<PeriodoDiasSinCapturar>();
		PeriodoDiasSinCapturar periodoDiasSinCapturar = new PeriodoDiasSinCapturar();
		periodoDiasSinCapturar.setCvePresupuestal("5679943256");
		periodoDiasSinCapturar.setDesDelegacionImss("NEZAHUALCOYOTL");
		periodoDiasSinCapturarList.add(periodoDiasSinCapturar);
		

		try {

			Mockito.when(monitorDiasSinCapturarServicesImplMock.obtenerPeriodosDiasSinCapturar())
					.thenReturn(periodoDiasSinCapturarList);
			monitorDiasSinCapturarServices.procesarMonitorDiasSinCapturar();
			
			Assert.assertEquals(monitorInsertDiasSinCapturar.getCveDelegacionImss(), "NEZA456");
			Assert.assertNotNull(periodoDiasSinCapturar.getDesDelegacionImss(), "NEZAHUALCOYOTL");
			
			logger.info("Presupuestal:{}", monitorInsertDiasSinCapturar.getCveDelegacionImss());
			logger.info("Unidad médica:{}", monitorInsertDiasSinCapturar.getFechaCaptura());
			logger.info("Clave delegación:{}", periodoDiasSinCapturar.getCvePresupuestal());
			logger.info("Delegación:{}", periodoDiasSinCapturar.getDesDelegacionImss());

		
		
		} catch (HospitalizacionException e) {
			e.printStackTrace();
		}
	}
}