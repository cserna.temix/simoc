package mx.gob.imss.simo.monitoreo.cierremes.helper;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.monitoreo.cierremes.model.DelegacionCierre;

@RunWith(MockitoJUnitRunner.class)
public class DelegacionCierreRowMapperHelperTest {

    @Mock
    private DataSource ds;

    @Mock
    private Connection c;

    @Mock
    private PreparedStatement stmt;

    @Mock
    private ResultSet rs;

    @InjectMocks
    DelegacionCierreRowMapperHelper delegacionCierreRowMapperHelper;

    DelegacionCierre delegacionImss;

    final static Logger logger = LoggerFactory.getLogger(DelegacionCierreRowMapperHelper.class);

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        try {
            assertNotNull(ds);
            when(c.prepareStatement(any(String.class))).thenReturn(stmt);
            when(ds.getConnection()).thenReturn(c);

            DelegacionCierre delegacionImss = new DelegacionCierre();
            delegacionImss.setCveDelegacionImss("222");
            delegacionImss.setDesDelegacionImss("M�xico Poniente");
            delegacionImss.setColor("rojo");

            when(rs.first()).thenReturn(true);
            when(rs.getString(1)).thenReturn(delegacionImss.getCveDelegacionImss());
            when(rs.getString(2)).thenReturn(delegacionImss.getDesDelegacionImss());
            when(rs.getString(3)).thenReturn(delegacionImss.getColor());
            when(stmt.executeQuery()).thenReturn(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        delegacionCierreRowMapperHelper = PowerMockito.spy(new DelegacionCierreRowMapperHelper());
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void delegacionCierreTest() {

        DelegacionCierre delegacionCierre = new DelegacionCierre();
        try {
            delegacionCierre.setCveDelegacionImss(rs.getString(1));
            delegacionCierre.setDesDelegacionImss(rs.getString(2));
            delegacionCierre.setColor(rs.getString(3));
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            logger.info("Test delegacionCierre");
            logger.info("Clave Delegaci�n IMSS: {}", delegacionCierre.getCveDelegacionImss());
            logger.info("Descripci�n Delegaci�n IMSS: {}", delegacionCierre.getDesDelegacionImss());
            logger.info("Color: {}", delegacionCierre.getColor());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
