package mx.gob.imss.simo.monitoreo.cierremes.helper;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;

import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierreAutomaticoMes;

@RunWith(MockitoJUnitRunner.class)
// @RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-main-context.xml" })
public class UnidadMedicaCierreAutomaticoMesRowMapperHelperTest {

    @Mock
    private DataSource ds;

    @Mock
    private Connection c;

    @Mock
    private PreparedStatement stmt;

    @Mock
    private ResultSet rs;

    @InjectMocks
    UnidadMedicaCierreAutomaticoMesRowMapperHelper unidadMedicaCierreAutomaticoMesRowMapperHelper;

    final static Logger logger = LoggerFactory.getLogger(UnidadMedicaCierreAutomaticoMesRowMapperHelper.class);

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        try {
            assertNotNull(ds);
            when(c.prepareStatement(any(String.class))).thenReturn(stmt);
            when(ds.getConnection()).thenReturn(c);

            UnidadMedicaCierreAutomaticoMes unidadMedicaCierreAutomaticoMes = new UnidadMedicaCierreAutomaticoMes();
            unidadMedicaCierreAutomaticoMes.setCvePresupuestal("040412252110");
            unidadMedicaCierreAutomaticoMes.setCveTipoCaptura(new Short("2"));
            unidadMedicaCierreAutomaticoMes.setCvePeriodoImss("201806");

            when(rs.first()).thenReturn(true);
            when(rs.getString(1)).thenReturn(unidadMedicaCierreAutomaticoMes.getCvePresupuestal());
            when(rs.getShort(2)).thenReturn(unidadMedicaCierreAutomaticoMes.getCveTipoCaptura());
            when(rs.getString(3)).thenReturn(unidadMedicaCierreAutomaticoMes.getCvePeriodoImss());
            when(stmt.executeQuery()).thenReturn(rs);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        unidadMedicaCierreAutomaticoMesRowMapperHelper = PowerMockito
                .spy(new UnidadMedicaCierreAutomaticoMesRowMapperHelper());
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapRowTest() {

        UnidadMedicaCierreAutomaticoMes unidadMedicaCierreAutomaticoMes = new UnidadMedicaCierreAutomaticoMes();
        try {
            unidadMedicaCierreAutomaticoMes.setCvePresupuestal(rs.getString(1));
            unidadMedicaCierreAutomaticoMes.setCveTipoCaptura(rs.getShort(2));
            unidadMedicaCierreAutomaticoMes.setCvePeriodoImss(rs.getString(3));
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            logger.info("Test mapRow");
            logger.info("Clave Presupuestal: {}", unidadMedicaCierreAutomaticoMes.getCvePresupuestal());
            logger.info("Clave tipo captura: {}", unidadMedicaCierreAutomaticoMes.getCveTipoCaptura());
            logger.info("CLave periodo IMSS: {}", unidadMedicaCierreAutomaticoMes.getCvePeriodoImss());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
