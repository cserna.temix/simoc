package mx.gob.imss.simo.monitoreo.cierremes.services.implTest;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.model.UnidadMedica;
import mx.gob.imss.simo.monitoreo.cierremes.model.DelegacionCierre;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierre;
import mx.gob.imss.simo.monitoreo.cierremes.model.UnidadMedicaCierreAutomaticoMes;
import mx.gob.imss.simo.monitoreo.cierremes.repository.impl.MonitoreoCommonOracleRepositoryImpl;
import mx.gob.imss.simo.monitoreo.cierremes.services.impl.MonitorCierreMensualServicesImpl;

public class MonitorCierreMensualServiceImplTest {

	@InjectMocks
	MonitorCierreMensualServicesImpl monitorCierreMensualServices;

	@Mock
	MonitoreoCommonOracleRepositoryImpl monitoreoCommonOracleRepositoryImplMock;

	@Mock
	HospitalizacionCommonRepositoryJDBCImpl hospitalizacionCommonRepositoryJDBCImpl;

	final static Logger logger = LoggerFactory.getLogger(MonitorCierreMensualServiceImplTest.class);

	@BeforeClass
	public static void ini() {

		BasicConfigurator.configure();
	}

	@Before
	public void setup() {

		monitoreoCommonOracleRepositoryImplMock = new MonitoreoCommonOracleRepositoryImpl();
		hospitalizacionCommonRepositoryJDBCImpl = new HospitalizacionCommonRepositoryJDBCImpl();

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void obtenerDelegacionCierreTest() {

		String annio = "2017";
		String mes = "Marzo";
		String delegacion = "Cuajimalpa";
		Integer rol = 3;
		String cadena = "cierre";

		List<DelegacionCierre> delegacionCierreList = new ArrayList<DelegacionCierre>();
		DelegacionCierre delegacionCierre = new DelegacionCierre();
		delegacionCierreList.add(delegacionCierre);

		try {
			Mockito.when(monitoreoCommonOracleRepositoryImplMock.obtenerDelegacionCierre(anyString(), anyString(),
					anyString())).thenReturn(delegacionCierreList);
			monitorCierreMensualServices.obtenerDelegacionCierre(annio, mes, delegacion, rol);

			Assert.assertEquals(delegacion, "Cuajimalpa");
			logger.info("A�o:{}", annio);
			logger.info("Rol:{}", rol);
			logger.info("Mes:{}", mes);
			logger.info("Cadena:{}", cadena);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void obtenerUnidadesCierreTest() {

		String annio = "2018";
		String mes = "Abril";
		String delegacion = "Madero";
		String cvePresupuestal = "34534562";
		Integer rol = 2;
		int i = 0;
		String cadena = "AND";
		String clavePeriodoIMSS = "QWE234";

		List<mx.gob.imss.simo.model.UnidadMedica> unidadesList = new ArrayList<mx.gob.imss.simo.model.UnidadMedica>();
		UnidadMedica unidadMedica = new UnidadMedica();
		unidadMedica.setDesUnidadMedica(null);
		unidadesList.add(unidadMedica);

		List<UnidadMedicaCierre> unidadMedicaCierreList = new ArrayList<UnidadMedicaCierre>();
		UnidadMedicaCierre unidadMedicaCierre = new UnidadMedicaCierre();
		unidadMedicaCierreList.add(unidadMedicaCierre);

		try {
			doNothing().when(monitoreoCommonOracleRepositoryImplMock).insertaPeriodosFaltantes(anyString(),
					anyString());
			Mockito.when(monitoreoCommonOracleRepositoryImplMock.obtenerUnidadesCierre(anyString(), anyString(),
					anyString(), anyString())).thenReturn(unidadMedicaCierreList);
			monitorCierreMensualServices.obtenerUnidadesCierre(annio, mes, delegacion, cvePresupuestal, rol,
					unidadesList);

			Assert.assertEquals(clavePeriodoIMSS, "QWE234");
			Assert.assertNotNull(rol);
			logger.info("Delegaci�n:{}", delegacion);
			logger.info("Presupuestal:{}", cvePresupuestal);
			logger.info("Rol:{}", rol);
			logger.info("Periodo IMSS:{}", clavePeriodoIMSS);
			logger.info("I:{}", i);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void cambiarEstatusPeriodoTest() {

		String clavePresupuestal = "24354666";
		Integer tipoCaptura = 2;
		String clavePeriodoImss = "201703";
		Integer indicadorPeriodo = 1;
		String unidadMedica = "HGR 24 Oaxaca";
		Integer indActual = 1;
		Integer indMonitoreo = 3;

		String value = "value1";

		PeriodoOperacion periodoOperacion = new PeriodoOperacion();
		periodoOperacion.setClavePeriodoIMSS("");
		periodoOperacion.setTipoCaptura(2);

		try {
			Mockito.when(
					hospitalizacionCommonRepositoryJDBCImpl.buscarPeriodoAbierto(anyString(), anyInt(), anyString()))
					.thenReturn(periodoOperacion);

			monitorCierreMensualServices.cambiarEstatusPeriodo(clavePresupuestal, tipoCaptura, clavePeriodoImss,
					indicadorPeriodo, unidadMedica, indActual, indMonitoreo);

			Assert.assertEquals(unidadMedica, "HGR 24 Oaxaca");
			Assert.assertNotNull(indActual);

			logger.info("Presupuestal:{}", clavePresupuestal);
			logger.info("Tipo captura:{}", tipoCaptura);
			logger.info("Periodo IMSS:{}", clavePeriodoImss);
			logger.info("Periodo:{}", indicadorPeriodo);
			logger.info("Unidad m�dica:{}", unidadMedica);
			logger.info("Ind actual:{}", indActual);
			logger.info("Ind monitoreo:{}", indMonitoreo);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void cerrarPeriodoMonitoreoTest() {

		String clavePresupuestal = "2564959534";
		Integer tipoCaptura = 3;
		String clavePeriodoImss = "201708";

		doNothing().when(monitoreoCommonOracleRepositoryImplMock).cerrarPeriodoMonitoreo(anyString(), anyInt(),
				anyString());
		monitorCierreMensualServices.cerrarPeriodoMonitoreo(clavePresupuestal, tipoCaptura, clavePeriodoImss);

		Assert.assertEquals(clavePresupuestal, "2564959534");
		Assert.assertNotNull(tipoCaptura);
		logger.info("Periodo IMSS:{}", clavePeriodoImss);
		logger.info("Tipo captura:{}", tipoCaptura);
		logger.info("Presupuestal:{}", clavePresupuestal);

	}

	@Test
	public void abrirPeriodoMonitoreoTest() {

		String clavePresupuestal = "2343546";
		Integer tipoCaptura = 4;
		String clavePeriodoImss = "201602";

		doNothing().when(monitoreoCommonOracleRepositoryImplMock).abrirPeriodoMonitoreo(anyString(), anyInt(),
				anyString());
		monitorCierreMensualServices.abrirPeriodoMonitoreo(clavePresupuestal, tipoCaptura, clavePeriodoImss);

		Assert.assertEquals(clavePresupuestal, "2343546");
		Assert.assertNotNull(tipoCaptura);
		logger.info("Periodo IMSS:{}", clavePeriodoImss);
		logger.info("Tipo captura:{}", tipoCaptura);
		logger.info("Presupuestal:{}", clavePresupuestal);
	}

	@Test
	public void obtenerUltimoPeriodoPermitidoMonitoreoTest() {

		String clavePresupuestal = "78423478236";

		Integer value = 2;

		Mockito.when(monitoreoCommonOracleRepositoryImplMock.obtenerUltimoPeriodoPermitidoMonitoreo(anyString()))
				.thenReturn(value);
		monitorCierreMensualServices.obtenerUltimoPeriodoPermitidoMonitoreo(clavePresupuestal);

		Assert.assertEquals(clavePresupuestal, "78423478236");
		Assert.assertNotNull(value);
		logger.info("Presupuestal:{}", clavePresupuestal);
		logger.info("Valor:{}", value);
	}

	@Test
	public void obtenerPeriodoAbiertoMonitoreoTest() {

		String clavePresupuestal = "98438423842";
		Integer valor1 = 2;

		Mockito.when(monitoreoCommonOracleRepositoryImplMock.obtenerPeriodoAbiertoMonitoreo(anyString()))
				.thenReturn(valor1);
		monitorCierreMensualServices.obtenerPeriodoAbiertoMonitoreo(clavePresupuestal);

		Assert.assertEquals(clavePresupuestal, "98438423842");
		Assert.assertNotNull(valor1);
		logger.info("Presupuestal:{}", clavePresupuestal);
		logger.info("Valor:{}", valor1);

	}

	@Test
	public void obtenerUltimoPeriodoCerrado() {

		String clavePresupuestal = "939930303";

		Integer value1 = 7;

		Mockito.when(monitoreoCommonOracleRepositoryImplMock.obtenerUltimoPeriodoCerrado(anyString()))
				.thenReturn(value1);
		monitorCierreMensualServices.obtenerUltimoPeriodoCerrado(clavePresupuestal);

		Assert.assertEquals(clavePresupuestal, "939930303");
		Assert.assertNotNull(value1);
		logger.info("Presupuestal:{}", clavePresupuestal);
		logger.info("Valor:{}", value1);
	}

	@Test
	public void obtenerUnidadesMedicasParaCierreAutomaticoDeMesTest() {

		List<UnidadMedicaCierreAutomaticoMes> unidadMedicaCierreAutomaticoMesList = new ArrayList<UnidadMedicaCierreAutomaticoMes>();
		UnidadMedicaCierreAutomaticoMes unidadMedicaCierreAutomaticoMes = new UnidadMedicaCierreAutomaticoMes();
		unidadMedicaCierreAutomaticoMes.setFecFinal(new Date());
		unidadMedicaCierreAutomaticoMes.setCvePresupuestal("1234567890");
		unidadMedicaCierreAutomaticoMesList.add(unidadMedicaCierreAutomaticoMes);

		Mockito.when(monitoreoCommonOracleRepositoryImplMock.obtenerUnidadesMedicasParaCierreAutomaticoDeMes())
				.thenReturn(unidadMedicaCierreAutomaticoMesList);
		monitorCierreMensualServices.obtenerUnidadesMedicasParaCierreAutomaticoDeMes();

		Assert.assertEquals(unidadMedicaCierreAutomaticoMes.getCvePresupuestal(), "1234567890");
		logger.info("Presupuestal:{}", unidadMedicaCierreAutomaticoMes.getCvePresupuestal());
		logger.info("Valor:{}", unidadMedicaCierreAutomaticoMes.getFecFinal());

	}
	
	@Test
	public void insertaBitacoraPeriodoTest(){
		
		doNothing().when(monitoreoCommonOracleRepositoryImplMock).insertarBitacoraPeriodo(anyString(), anyInt(),
				anyString(), anyInt(), anyString());
		
		monitorCierreMensualServices.insertaBitacoraPeriodo("1234567890",2,"202108",1,"brenda.martinezr");
		logger.info("Test insertaBitacoraPeriodo");
		logger.info("Se regitro movimiento en la bitacora de periodos");
		
	}

	@Test
	public void filtrarCatalogoPresupuestalTest(){
		List<String> listaCatalago = new ArrayList<>();
		listaCatalago.add("103124825953 HGZ 1 Pachuca");
		listaCatalago.add("192846936108 HGZM 5 Tulancingo");
		
		List<String> listaCatalagoFiltrada = new ArrayList<>();
		listaCatalagoFiltrada.add("103124825953 HGZ 1 Pachuca");
		
		String textoIngresado = "103124825953 HGZ 1 Pachuca";
		int longitud = 12;
		try{
			
			monitorCierreMensualServices.filtrarCatalogoPresupuestal(
					listaCatalago, textoIngresado, longitud);
			
			Assert.assertEquals(listaCatalagoFiltrada.get(0), textoIngresado);
	
			logger.info("Test filtrarCatalogoPresupuestal");
			logger.info("Unidad:{}", textoIngresado);
			logger.info("Lista Filtrada:{}", listaCatalagoFiltrada);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
