var log;
$(document).ready(function(){
    log=activaLog(false);
    log.info("SE CARGA cierreMEs");
    btn_click("#idMonitoreoCierreMes");
    
    var $_FORM = "#idMonitoreoCierreMes\\:";
    var $_FORM_SF = "idMonitoreoCierreMes:";
    
    validateInputMaskChangeOrPressEnterTabOrMaxlength("#idMonitoreoCierreMes\\:idMesanio", "rcPeriodo", 7);
    validateAutocompletePressEnterTabOrMaxlength($_FORM + "idPresupuestal", 
    		$_FORM_SF + "idPresupuestal", "rcPresupuestal", 12);
    borrarClass();
    configuraPantalla(1,0,true);
    log.info("SE CARGA input");
    
});

var shiftPresionado;

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if ((e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223)) {
                        return false;
                    }
                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }
                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                    if (e.keyCode == 226) {
                        return false;
                    }
                    if (shiftPresionado && e.keyCode == 226) {
                        return false;
                    }
                });

function borrarClass(){
    log.info("borrarClass");
    document.getElementById('idAreaTrabajo').className = "areaDeTrabajoTablas";
    
}

