/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdDelegacion {

	private String cveDelegacion;
	private String desDelegacion;

	
	public SdDelegacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SdDelegacion(String cveDelegacion, String desDelegacion) {
		super();
		this.cveDelegacion = cveDelegacion;
		this.desDelegacion = desDelegacion;
	}

	/**
	 * @return the cveDelegacion
	 */
	public String getCveDelegacion() {
		return cveDelegacion;
	}

	/**
	 * @param cveDelegacion
	 *            the cveDelegacion to set
	 */
	public void setCveDelegacion(String cveDelegacion) {
		this.cveDelegacion = cveDelegacion;
	}

	/**
	 * @return the desDelegacion
	 */
	public String getDesDelegacion() {
		return desDelegacion;
	}

	/**
	 * @param desDelegacion
	 *            the desDelegacion to set
	 */
	public void setDesDelegacion(String desDelegacion) {
		this.desDelegacion = desDelegacion;
	}

}
