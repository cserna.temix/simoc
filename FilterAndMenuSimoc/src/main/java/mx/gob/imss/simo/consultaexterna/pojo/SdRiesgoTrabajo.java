/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdRiesgoTrabajo {

	private Integer cveRiesgoTrabajo;
	private String desRiesgoTrabajo;

	public SdRiesgoTrabajo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SdRiesgoTrabajo(Integer cveRiesgoTrabajo, String desRiesgoTrabajo) {
		super();
		this.cveRiesgoTrabajo = cveRiesgoTrabajo;
		this.desRiesgoTrabajo = desRiesgoTrabajo;
	}

	/**
	 * @return the cveRiesgoTrabajo
	 */
	public Integer getCveRiesgoTrabajo() {
		return cveRiesgoTrabajo;
	}

	/**
	 * @param cveRiesgoTrabajo
	 *            the cveRiesgoTrabajo to set
	 */
	public void setCveRiesgoTrabajo(Integer cveRiesgoTrabajo) {
		this.cveRiesgoTrabajo = cveRiesgoTrabajo;
	}

	/**
	 * @return the desRiesgoTrabajo
	 */
	public String getDesRiesgoTrabajo() {
		return desRiesgoTrabajo;
	}

	/**
	 * @param desRiesgoTrabajo
	 *            the desRiesgoTrabajo to set
	 */
	public void setDesRiesgoTrabajo(String desRiesgoTrabajo) {
		this.desRiesgoTrabajo = desRiesgoTrabajo;
	}

}
