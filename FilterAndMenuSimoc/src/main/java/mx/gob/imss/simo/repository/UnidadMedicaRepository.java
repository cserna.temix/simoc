package mx.gob.imss.simo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.imss.simo.model.UnidadMedica;

public interface UnidadMedicaRepository extends MongoRepository<UnidadMedica, ObjectId> {

    List<UnidadMedica> findByCveDelegacionOrderByDesDelegacionAsc(String cveDelegacion);
}
