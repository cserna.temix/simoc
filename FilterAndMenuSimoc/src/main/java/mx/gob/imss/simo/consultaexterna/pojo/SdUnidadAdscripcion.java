/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdUnidadAdscripcion {

	private String cvePresupuestal;
	private String desUnidadMedica;

	/**
	 * @return the cvePresupuestal
	 */
	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	/**
	 * @param cvePresupuestal
	 *            the cvePresupuestal to set
	 */
	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	/**
	 * @return the desUnidadMedica
	 */
	public String getDesUnidadMedica() {
		return desUnidadMedica;
	}

	/**
	 * @param desUnidadMedica
	 *            the desUnidadMedica to set
	 */
	public void setDesUnidadMedica(String desUnidadMedica) {
		this.desUnidadMedica = desUnidadMedica;
	}

}
