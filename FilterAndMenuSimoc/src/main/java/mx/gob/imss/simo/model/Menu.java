package mx.gob.imss.simo.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SIC_MENU_APLICATIVO")
public class Menu {

    @Id
    private ObjectId id;

    private String cveRol;

    private String cveMenuAplicativo;
    private String desMenuAplicativo;
    private String refAccion;

    private List<MenuItem> sdAdministracion;
    private List<MenuItem> sdConsultaExterna;
    private List<MenuItem> sdReportes;
    private List<MenuItem> sdHospitalizacion;
    private List<MenuItem> sdEnlace;
    private List<MenuItem> sdAdminUsuario;

    public String getCveMenuAplicativo() {

        return cveMenuAplicativo;
    }

    public void setCveMenuAplicativo(String cveMenuAplicativo) {

        this.cveMenuAplicativo = cveMenuAplicativo;
    }

    public String getDesMenuAplicativo() {

        return desMenuAplicativo;
    }

    public void setDesMenuAplicativo(String desMenuAplicativo) {

        this.desMenuAplicativo = desMenuAplicativo;
    }

    public String getRefAccion() {

        return refAccion;
    }

    public void setRefAccion(String refAccion) {

        this.refAccion = refAccion;
    }

    public List<MenuItem> getSdAdministracion() {

        return sdAdministracion;
    }

    public void setSdAdministracion(List<MenuItem> sdAdministracion) {

        this.sdAdministracion = sdAdministracion;
    }

    public List<MenuItem> getSdConsultaExterna() {

        return sdConsultaExterna;
    }

    public void setSdConsultaExterna(List<MenuItem> sdConsultaExterna) {

        this.sdConsultaExterna = sdConsultaExterna;
    }

    public List<MenuItem> getSdReportes() {

        return sdReportes;
    }

    public void setSdReportes(List<MenuItem> sdReportes) {

        this.sdReportes = sdReportes;
    }

    public List<MenuItem> getSdEnlace() {

        return sdEnlace;
    }

    public void setSdEnlace(List<MenuItem> sdEnlace) {

        this.sdEnlace = sdEnlace;
    }

    public List<MenuItem> getSdHospitalizacion() {

        return sdHospitalizacion;
    }

    public void setSdHospitalizacion(List<MenuItem> sdHospitalizacion) {

        this.sdHospitalizacion = sdHospitalizacion;
    }

    public ObjectId getId() {

        return id;
    }

    public void setId(ObjectId id) {

        this.id = id;
    }

    public String getCveRol() {

        return cveRol;
    }

    public void setCveRol(String cveRol) {

        this.cveRol = cveRol;
    }

    public List<MenuItem> getSdAdminUsuario() {
		return sdAdminUsuario;
	}

	public void setSdAdminUsuario(List<MenuItem> sdAdminUsuario) {
		this.sdAdminUsuario = sdAdminUsuario;
	}

	@Override
    public String toString() {

        return "Menu [id=" + id + ", cveRol=" + cveRol + ", cveMenuAplicativo=" + cveMenuAplicativo
                + ", desMenuAplicativo=" + desMenuAplicativo + ", refAccion=" + refAccion + ", sdAdministracion="
                + sdAdministracion + ", sdConsultaExterna=" + sdConsultaExterna + ", sdReportes=" + sdReportes
                + ", sdHospitalizacion=" + sdHospitalizacion + ", sdEnlace=" + sdEnlace +", sdAdminUsuario=" + sdAdminUsuario + "]";
    }

}
