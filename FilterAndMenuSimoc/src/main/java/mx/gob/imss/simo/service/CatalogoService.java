package mx.gob.imss.simo.service;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.model.DelegacionIMSS;
import mx.gob.imss.simo.model.UnidadMedica;

public interface CatalogoService {

    List<DelegacionIMSS> obtenerDelegacionesIMSS();

    List<UnidadMedica> obtenerUnidadesMedicasPorDelegacion(String cveDelegacion);

    String getPeriodoActual(String cvePresupuestal, Integer tipoCaptura) throws SQLException;
    
    public Date obtenerUltimaFechaCaptura(String cvePresupuestal, Integer tipoCaptura) throws Exception;
    
    public String getPeriodoMonitoreo(String cvePresupuestal, Integer tipoCaptura)throws SQLException;
    
}
