package mx.gob.imss.simo.model;

import java.util.Date;


public class PeriodoOperacion {

    private String clavePresupuestal;
    private int tipoCaptura;
    private String clavePeriodoIMSS;
    private boolean indicadorCerrado;
    private Date fechaCierre;
    private boolean indicadorActual;
    
    
    public String getClavePresupuestal() {
		return clavePresupuestal;
	}
	public void setClavePresupuestal(String clavePresupuestal) {
		this.clavePresupuestal = clavePresupuestal;
	}
	public int getTipoCaptura() {
		return tipoCaptura;
	}
	public void setTipoCaptura(int tipoCaptura) {
		this.tipoCaptura = tipoCaptura;
	}
	public String getClavePeriodoIMSS() {
		return clavePeriodoIMSS;
	}
	public void setClavePeriodoIMSS(String clavePeriodoIMSS) {
		this.clavePeriodoIMSS = clavePeriodoIMSS;
	}
	public boolean isIndicadorCerrado() {
		return indicadorCerrado;
	}
	public void setIndicadorCerrado(boolean indicadorCerrado) {
		this.indicadorCerrado = indicadorCerrado;
	}
	public Date getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(Date fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public boolean isIndicadorActual() {
		return indicadorActual;
	}
	public void setIndicadorActual(boolean indicadorActual) {
		this.indicadorActual = indicadorActual;
	}
	public boolean isIndicadorMonitoreo() {
		return indicadorMonitoreo;
	}
	public void setIndicadorMonitoreo(boolean indicadorMonitoreo) {
		this.indicadorMonitoreo = indicadorMonitoreo;
	}
	private boolean indicadorMonitoreo;

}

