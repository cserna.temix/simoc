package mx.gob.imss.simo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.model.Item;
import mx.gob.imss.simo.model.Menu;
import mx.gob.imss.simo.model.MenuItem;
import mx.gob.imss.simo.repository.MenuRepository;

@Service("menuServiceJar")
public class MenuServiceImpl implements mx.gob.imss.simo.service.MenuService {

    static final Logger logger = Logger.getLogger(MenuServiceImpl.class);

    public static final String HOSPITALIZACION = "HOSPITALIZACION";
    public static final String CONSULTA_EXTERNA = "CONSULTA_EXTERNA";
    public static final String MONITOREO = "MONITOREO";
    public static final String ENLACES = "ENLACES";
    public static final String USUARIOS = "USUARIOS";
    public static final String REPORTE_PARTE_I = "REPORTE_PARTE_I";
    public static final String ADMINISTRACION_USUARIO = "ADMINISTRACION_USUARIO";

    @Value("${url.ce}")
    private String AMBIENTE_CE;
    @Value("${url.hosp}")
    private String AMBIENTE_HOSP;
    @Value("${url.monitoreo}")
    private String AMBIENTE_MONITOREO;
    @Value("${url.enlaces}")
    private String AMBIENTE_ENLACES;
    @Value("${url.usuarios}")
    private String AMBIENTE_USUARIOS;
    @Value("${url.reporteParteI}")
    private String AMBIENTE_REPORTE_PARTE_I;
//    @Value("${url.adminusuario}")
//    private String AMBIENTE_ADMINISTRACION_USUARIO;

    @Autowired
    MenuRepository menuRepository;

    @Override
    public List<Menu> getMenu(String role) {

        // logger.info("AMBIENTES EN EJECUCION: " + AMBIENTE_CE + ", " + AMBIENTE_HOSP + ", " + AMBIENTE_ENLACES + ", "
        // + AMBIENTE_MONITOREO + ", " + AMBIENTE_USUARIOS);
        return menuRepository.findByCveRolOrderByCveMenuAplicativoAsc(role);
    }

    @Override
    public MenuModel createMenu(String role) {

        MenuModel model;

        List<Menu> menu = getMenu(role);

        logger.info("Creando menu para el rol: " + role);

        String nivel1 = "ui-icon-bullet";

        model = new DefaultMenuModel();

        DefaultSubMenu rootMenu = new DefaultSubMenu();
        List<MenuElement> menu0 = new ArrayList<>();

        for (Menu men : menu) {
            // logger.info("Agregando menu: " + men.getDesMenuAplicativo());

            DefaultSubMenu submenu0 = new DefaultSubMenu(men.getDesMenuAplicativo(), nivel1);

            submenu0.setStyleClass("imagenMenuBar");
            submenu0.setStyleClass("menuContenedores");
            submenu0.setId("menu_p");

            // Submenu Administracion
            if (men.getSdAdministracion() != null) {
                for (MenuItem adm : men.getSdAdministracion()) {
                    DefaultSubMenu item = new DefaultSubMenu(adm.getDesMenuAplicativo());
                    if (adm.getSdUsuarios() != null) {
                        for (Item admItem : adm.getSdUsuarios()) {
                            DefaultMenuItem adminSubItem = new DefaultMenuItem(admItem.getDesMenuAplicativo());
                            if (isUsuarios(admItem.getAplicativo())) {
                                adminSubItem.setHref(AMBIENTE_USUARIOS.concat(admItem.getRefAccion()));
                            }
                            if (isAdminUsuario(admItem.getAplicativo())) {
                                adminSubItem.setHref(AMBIENTE_USUARIOS.concat(admItem.getRefAccion()));
                            }
                            item.addElement(adminSubItem);
                        }
                        submenu0.addElement(item);
                    }

                    if (adm.getSdMonitoreo() != null) {
                        for (Item admItem : adm.getSdMonitoreo()) {
                            DefaultMenuItem subMoitoreo = new DefaultMenuItem(admItem.getDesMenuAplicativo());
                            item.addElement(subMoitoreo);
                            if (isMonitoreo(admItem.getAplicativo())) {
                                subMoitoreo.setHref(AMBIENTE_MONITOREO.concat(admItem.getRefAccion()));
                            } else if (isHospitalizacion(admItem.getAplicativo())) {
                                subMoitoreo.setHref(AMBIENTE_HOSP.concat(admItem.getRefAccion()));
                            } else if (isConsultaExterna(admItem.getAplicativo())) {
                                subMoitoreo.setHref(AMBIENTE_CE.concat(admItem.getRefAccion()));
                            } else if (isEnlaces(admItem.getAplicativo())) {
                                subMoitoreo.setHref(AMBIENTE_ENLACES.concat(admItem.getRefAccion()));
                            } else if (isReporteParte1(admItem.getAplicativo())) {
                                subMoitoreo.setHref(AMBIENTE_REPORTE_PARTE_I.concat(admItem.getRefAccion()));
                            }

                        }
                        submenu0.addElement(item);
                    }
                }
            }

            // Submenu Consulta Externa
            if (men.getSdConsultaExterna() != null) {
                for (MenuItem cons : men.getSdConsultaExterna()) {
                    DefaultMenuItem consItem = new DefaultMenuItem(cons.getDesMenuAplicativo());
                    if (isConsultaExterna(cons.getAplicativo())) {
                        consItem.setHref(AMBIENTE_CE.concat(cons.getRefAccion()));
                    }
                    submenu0.addElement(consItem);
                }
            }

            // Submenu Reportes
            if (men.getSdReportes() != null) {
                for (MenuItem rep : men.getSdReportes()) {
                    DefaultSubMenu repSub = new DefaultSubMenu(rep.getDesMenuAplicativo());

                    if (rep.getSdConsultaExterna() != null) {
                        for (Item consItem : rep.getSdConsultaExterna()) {
                            DefaultMenuItem consExtItem = new DefaultMenuItem(consItem.getDesMenuAplicativo());
                            if (isReporteParte1(consItem.getAplicativo())) {
                                consExtItem.setHref(AMBIENTE_REPORTE_PARTE_I.concat(consItem.getRefAccion()));
                            }
                            repSub.addElement(consExtItem);
                        }
                        submenu0.addElement(repSub);
                    } else if (rep.getSdHospitalizacion() != null) {
                        for (Item hospItem : rep.getSdHospitalizacion()) {
                            DefaultMenuItem hospSubItem = new DefaultMenuItem(hospItem.getDesMenuAplicativo());
                            if (isHospitalizacion(hospItem.getAplicativo())) {
                                hospSubItem.setHref(AMBIENTE_HOSP.concat(hospItem.getRefAccion()));
                            }
                            repSub.addElement(hospSubItem);
                        }
                        submenu0.addElement(repSub);
                    }
                }
            }

            // Submenu Hospitalizacion
            if (men.getSdHospitalizacion() != null) {
                for (MenuItem hosp : men.getSdHospitalizacion()) {
                    DefaultMenuItem hospItem = new DefaultMenuItem(hosp.getDesMenuAplicativo());

                    DefaultSubMenu hospSub = new DefaultSubMenu(hosp.getDesMenuAplicativo());
                    DefaultSubMenu hospSubEgreso = new DefaultSubMenu(hosp.getDesMenuAplicativo());
                    if (hosp.getSdIngreso4306() != null) {
                        for (Item hospSubItem : hosp.getSdIngreso4306()) {
                            DefaultMenuItem hospSubItemIngreso = new DefaultMenuItem(
                                    hospSubItem.getDesMenuAplicativo());
                            if (isHospitalizacion(hospSubItem.getAplicativo())) {
                                hospSubItemIngreso.setHref(AMBIENTE_HOSP.concat(hospSubItem.getRefAccion()));
                            }
                            hospSub.addElement(hospSubItemIngreso);
                        }
                        submenu0.addElement(hospSub);
                    } else

                    if (hosp.getSdEgresos() != null) {
                        for (Item hospSubItem : hosp.getSdEgresos()) {
                            DefaultMenuItem hospSubItemEgreso = new DefaultMenuItem(hospSubItem.getDesMenuAplicativo());
                            if (isHospitalizacion(hospSubItem.getAplicativo())) {
                                hospSubItemEgreso.setHref(AMBIENTE_HOSP.concat(hospSubItem.getRefAccion()));
                            }
                            hospSubEgreso.addElement(hospSubItemEgreso);
                        }
                        submenu0.addElement(hospSubEgreso);
                    } else {
                        if (isHospitalizacion(hosp.getAplicativo())) {
                            hospItem.setHref(AMBIENTE_HOSP.concat(hosp.getRefAccion()));
                        }
                        submenu0.addElement(hospItem);
                    }
                }
            }

            // Submenu Enlaces
            if (men.getSdEnlace() != null) {
                for (MenuItem enlace : men.getSdEnlace()) {
                    DefaultMenuItem enlaceItem = new DefaultMenuItem(enlace.getDesMenuAplicativo());
                    if (isEnlaces(enlace.getAplicativo())) {
                        enlaceItem.setHref(AMBIENTE_ENLACES.concat(enlace.getRefAccion()));
                    }
                    submenu0.addElement(enlaceItem);
                }
            }
            
           // Submenu Admin usuarios
//            if (men.getSdAdminUsuario() != null) {
//                for (MenuItem enlace : men.getSdAdminUsuario()) {
//                    DefaultMenuItem enlaceItem = new DefaultMenuItem(enlace.getDesMenuAplicativo());
//                    if (isAdminUsuario(enlace.getAplicativo())) {
//                        enlaceItem.setHref(AMBIENTE_ADMINISTRACION_USUARIO.concat(enlace.getRefAccion()));
//                    }
//                    submenu0.addElement(enlaceItem);
//                }
//            }
            menu0.add(submenu0);
        }
        rootMenu.setElements(menu0);
        model.addElement(rootMenu);
        return model;
    }

    private Boolean isConsultaExterna(String aplicativo) {

        return aplicativo.equals(CONSULTA_EXTERNA);
    }

    private Boolean isReporteParte1(String aplicativo) {

        return aplicativo.equals(REPORTE_PARTE_I);
    }

    private Boolean isHospitalizacion(String aplicativo) {

        return aplicativo.equals(HOSPITALIZACION);
    }

    private Boolean isEnlaces(String aplicativo) {

        return aplicativo.equals(ENLACES);
    }

    private Boolean isMonitoreo(String aplicativo) {

        return aplicativo.equals(MONITOREO);
    }

    private Boolean isUsuarios(String aplicativo) {

        return aplicativo.equals(USUARIOS);
    }
    
    private Boolean isAdminUsuario(String aplicativo) {

        return aplicativo.equals(ADMINISTRACION_USUARIO);
    }

    public void setMenuRepository(MenuRepository menuRepository) {

        this.menuRepository = menuRepository;
    }

    public String getAmbienteCE() {

        return AMBIENTE_CE;
    }

}
