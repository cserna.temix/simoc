package mx.gob.imss.simo.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.imss.simo.model.DelegacionIMSS;

public interface DelegacionRepository extends MongoRepository<DelegacionIMSS, ObjectId> {

}
