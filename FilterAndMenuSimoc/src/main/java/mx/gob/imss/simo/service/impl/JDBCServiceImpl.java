package mx.gob.imss.simo.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.service.JDBCService;

@Service("jdbcService")
public class JDBCServiceImpl implements JDBCService {

    @Value("${oracle.host}")
    private String HOST;
    @Value("${oracle.port}")
    private String PORT;
    @Value("${oracle.sid}")
    private String SID;
    @Value("${oracle.user}")
    private String USER;
    @Value("${oracle.pass}")
    private String PASSWORD;

    @Override
    public Connection getConnectionOracle() throws SQLException {

        Connection dbConnection = null;

        dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@" + HOST + ":" + PORT + ":" + SID, USER,
                PASSWORD);
        return dbConnection;
    }

}
