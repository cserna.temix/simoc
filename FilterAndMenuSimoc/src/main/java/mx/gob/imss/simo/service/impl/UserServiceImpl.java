package mx.gob.imss.simo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.model.PersonalOperativo;
import mx.gob.imss.simo.repository.UserRepository;
import mx.gob.imss.simo.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public PersonalOperativo getUsuario(String username) {

        return userRepository.findOneByRefCuentaActiveD(username);
    }
    
    @Override
    public PersonalOperativo getUsuarioByActivo(String username, Integer indActivo) {
        return userRepository.findByRefCuentaActiveDAndIndActivo(username,indActivo);
    }

    public UserRepository getUserRepository() {

        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

}
