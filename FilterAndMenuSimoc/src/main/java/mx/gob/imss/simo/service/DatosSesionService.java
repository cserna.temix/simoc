package mx.gob.imss.simo.service;

import mx.gob.imss.simo.model.DatosSesion;

public interface DatosSesionService {

    DatosSesion getDatosSesion(String idSessionNetIQ, String username);

    DatosSesion save(DatosSesion datosSesion);

    void delete(DatosSesion datosSesion);
}
