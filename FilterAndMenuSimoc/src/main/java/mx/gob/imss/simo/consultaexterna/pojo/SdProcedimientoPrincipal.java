/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdProcedimientoPrincipal {

	private String cveCie9mc;
	private String desCie9mc;

	
	public SdProcedimientoPrincipal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SdProcedimientoPrincipal(String cveCie9mc, String desCie9mc) {
		super();
		this.cveCie9mc = cveCie9mc;
		this.desCie9mc = desCie9mc;
	}

	/**
	 * @return the cveCie9mc
	 */
	public String getCveCie9mc() {
		return cveCie9mc;
	}

	/**
	 * @param cveCie9mc
	 *            the cveCie9mc to set
	 */
	public void setCveCie9mc(String cveCie9mc) {
		this.cveCie9mc = cveCie9mc;
	}

	/**
	 * @return the desCie9mc
	 */
	public String getDesCie9mc() {
		return desCie9mc;
	}

	/**
	 * @param desCie9mc
	 *            the desCie9mc to set
	 */
	public void setDesCie9mc(String desCie9mc) {
		this.desCie9mc = desCie9mc;
	}

}
