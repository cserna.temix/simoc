package mx.gob.imss.simo.service;

import java.sql.Connection;
import java.sql.SQLException;

public interface JDBCService {

    Connection getConnectionOracle() throws SQLException;
}
