package mx.gob.imss.simo.repository.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.helper.PeriodoOperacionImssRowMapperHelper;
import mx.gob.imss.simo.helper.ReporteGeneradoRowMapHelper;
import mx.gob.imss.simo.model.PeriodoOperacion;
import mx.gob.imss.simo.repository.BaseJDBCRepository;
import mx.gob.imss.simo.repository.PeriodoOperacionRepository;
import mx.gob.imss.simo.service.JDBCService;

@Repository("periodoOperacionRepository")
public class PeriodoOperacionRepositoryImpl extends BaseJDBCRepository implements PeriodoOperacionRepository {

    static final Logger logger = Logger.getLogger(PeriodoOperacionRepositoryImpl.class);

    @Autowired
    JDBCService jdbcService;

    @Override
    public PeriodoOperacion getPeriodoActual(String cvePresupuestal, Integer tipoCaptura) throws SQLException {
    	System.out.println("cvePresupuestal:"+cvePresupuestal);
    	System.out.println("tipoCaptura:"+tipoCaptura);
    	 PeriodoOperacion po=null;
    	if(tipoCaptura>1){
    		 String sql = "SELECT * FROM HOSP_OWNER.SIT_PERIODO_OPERACION P WHERE P.CVE_PRESUPUESTAL = ? AND P.CVE_TIPO_CAPTURA = ? AND P.IND_CERRADO = 0 AND  P.IND_ACTUAL = 1";
        	po= jdbcTemplate.queryForObject(sql,
                      new Object[] { cvePresupuestal, tipoCaptura }, new PeriodoOperacionImssRowMapperHelper());
    	}
    	
    	  if(po!=null){
    		  System.out.println("po:"+po.getClavePeriodoIMSS());
    		  
    	  }
    	 
    	 return po;

    }
    
    public Date obtenerUltimaFechaCaptura(String cvePresupuestal, Integer tipoCaptura) throws Exception{
    	
    	Date regreso = null;
    	if(tipoCaptura>1){
    		String sql= "  SELECT   FEC_GENERACION" + 
    				"    FROM   HOSP_OWNER.sit_reporte_generado" + 
    				"   WHERE       cve_tipo_reporte = 3" + 
    				"           AND cve_presupuestal = ? " + 
    				"           AND ROWNUM = 1" + 
    				"ORDER BY   fec_generacion DESC ";
    		regreso = jdbcTemplate.queryForObject(sql,
    				new Object[] { cvePresupuestal }, new ReporteGeneradoRowMapHelper());
    	}    	
    	return regreso;
    }

    public JDBCService getJdbcService() {

        return jdbcService;
    }

    public void setJdbcService(JDBCService jdbcService) {

        this.jdbcService = jdbcService;
    }
    
    @Override
	public PeriodoOperacion getPeriodoMonitoreo(String cvePresupuestal, Integer tipoCaptura) throws SQLException {
		 PeriodoOperacion pm=null;
	    	
	    	if(tipoCaptura>1){
	    		 String sql = "SELECT * FROM HOSP_OWNER.SIT_PERIODO_OPERACION P WHERE P.CVE_PRESUPUESTAL = ? AND P.CVE_TIPO_CAPTURA = ? AND P.IND_CERRADO = 0 AND P.IND_ACTUAL = 0 AND P.IND_MONITOREO = 1";
	    		 List<PeriodoOperacion> periodoOperacion = jdbcTemplate.query(sql,
	                      new Object[] { cvePresupuestal, tipoCaptura }, new PeriodoOperacionImssRowMapperHelper());
	    		 
	    		 if (null != periodoOperacion && !periodoOperacion.isEmpty()) {
	    			 pm = periodoOperacion.get(0);
	             }
	    	}
	    	
	    	return pm;
	
	}

}
