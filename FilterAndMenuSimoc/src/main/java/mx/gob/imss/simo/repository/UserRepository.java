package mx.gob.imss.simo.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.imss.simo.model.PersonalOperativo;

public interface UserRepository extends MongoRepository<PersonalOperativo, ObjectId> {

    PersonalOperativo findOneByRefCuentaActiveD(String username);
    
    PersonalOperativo findByRefCuentaActiveDAndIndActivo(String username, Integer indActivo);
}
