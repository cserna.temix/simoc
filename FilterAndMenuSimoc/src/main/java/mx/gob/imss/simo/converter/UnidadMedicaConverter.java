package mx.gob.imss.simo.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import mx.gob.imss.simo.model.UnidadMedica;

@FacesConverter("unidadMedicaConverter")
public class UnidadMedicaConverter implements Converter {

    static final Logger logger = Logger.getLogger(UnidadMedicaConverter.class);

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {

        if (value != null && value.trim().length() > 0) {
            try {
                UnidadMedica unidad = new UnidadMedica();
                unidad.setCvePresupuestal(value);
                return unidad;
            } catch (Exception e) {
                logger.error("Error en la conversion Unidad Medica", e);
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en la conversion",
                        "Unidad Medica no valida"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {

        if (object != null) {
            return String.valueOf(((UnidadMedica) object).getCvePresupuestal());
        } else {
            return null;
        }
    }
}
