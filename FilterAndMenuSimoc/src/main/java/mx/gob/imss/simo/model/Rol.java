package mx.gob.imss.simo.model;

import java.util.Date;

public class Rol {

    private String cveRol;
    private String desRol;
    private Date fecBaja;
    private Integer indSuperAdministrador;

    public String getCveRol() {

        return cveRol;
    }

    public void setCveRol(String cveRol) {

        this.cveRol = cveRol;
    }

    public String getDesRol() {

        return desRol;
    }

    public void setDesRol(String desRol) {

        this.desRol = desRol;
    }

    public Date getFecBaja() {

        return fecBaja;
    }

    public void setFecBaja(Date fecBaja) {

        this.fecBaja = fecBaja;
    }

    public Integer getIndSuperAdministrador() {

        return indSuperAdministrador;
    }

    public void setIndSuperAdministrador(Integer indSuperAdministrador) {

        this.indSuperAdministrador = indSuperAdministrador;
    }

    @Override
    public String toString() {

        return "Rol [cveRol=" + cveRol + ", desRol=" + desRol + ", fecBaja=" + fecBaja + ", indSuperAdministrador="
                + indSuperAdministrador + "]";
    }

}
