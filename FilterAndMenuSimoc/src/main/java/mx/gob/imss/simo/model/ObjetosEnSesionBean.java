package mx.gob.imss.simo.model;

import java.io.Serializable;
import java.util.Date;

import org.primefaces.context.RequestContext;
import org.primefaces.model.menu.MenuModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("session")
@Component(value = "objetosSs")
public class ObjetosEnSesionBean implements Serializable {

    private static final long serialVersionUID = -298082286199949718L;

    private String contexto;
    private String reqURI;
    private String role;
    private String username;
    private String userInfo;
    private String periodoActual;
    private String periodoActualFormato;
    private String periodoMonitoreo;
    private String periodoMonitoreoFormato;
    public Date fechaPeriodoCaptura;

    private String colorHeader;
    private transient DatosUsuario datosUsuario;
    private boolean mostrarDialogoDelegaciones;
    private transient MenuModel menu;
    private transient PersonalOperativo personalOperativo;

    // CE
    private Boolean busquedaCex = Boolean.FALSE;
    private String usuarioHeader;
    private String perfilHeader;
    private String delegacionHeader;
    private String unidadMedicaHeader;
    private String fechaHeader;
    private String formBean;
    private UsuarioDetalleDto usuarioDetalleDto;
    private ConsultaExternaDto consultaExternaDtoSesion;
    private ConsultaExternaDto ceDatosBusqueda;

    private String rutaPaginaInicio = "";
    private Integer maxRegistrosCExEspc = 0;

    public ObjetosEnSesionBean() {

        datosUsuario = new DatosUsuario();
        usuarioDetalleDto = new UsuarioDetalleDto();
        this.setUsuarioHeader("");
        this.setPerfilHeader("");
        this.setDelegacionHeader("");
        this.setUnidadMedicaHeader("");

    }

    public void cargaDelegacionUnidadModal() {

        if (delegacionHeader.isEmpty()) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('myDialogVar').show();");
        }
    }

    public String getColorHeader() {

        return colorHeader;
    }

    public void setColorHeader(String colorHeader) {

        this.colorHeader = colorHeader;
    }

    public DatosUsuario getDatosUsuario() {

        return datosUsuario;
    }

    public void setDatosUsuario(DatosUsuario datosUsuario) {

        this.datosUsuario = datosUsuario;
    }

    public boolean getMostrarDialogoDelegaciones() {

        return mostrarDialogoDelegaciones;
    }

    public void setMostrarDialogoDelegaciones(boolean mostrarDialogoDelegaciones) {

        this.mostrarDialogoDelegaciones = mostrarDialogoDelegaciones;
    }

    public MenuModel getMenu() {

        return menu;
    }

    public void setMenu(MenuModel menu) {

        this.menu = menu;
    }

    public Boolean getBusquedaCex() {

        return busquedaCex;
    }

    public void setBusquedaCex(Boolean busquedaCex) {

        this.busquedaCex = busquedaCex;
    }

    public String getUsuarioHeader() {

        return usuarioHeader;
    }

    public void setUsuarioHeader(String usuarioHeader) {

        this.usuarioHeader = usuarioHeader;
    }

    public String getPerfilHeader() {

        return perfilHeader;
    }

    public void setPerfilHeader(String perfilHeader) {

        this.perfilHeader = perfilHeader;
    }

    public String getDelegacionHeader() {

        return delegacionHeader;
    }

    public void setDelegacionHeader(String delegacionHeader) {

        this.delegacionHeader = delegacionHeader;
    }

    public String getUnidadMedicaHeader() {

        return unidadMedicaHeader;
    }

    public void setUnidadMedicaHeader(String unidadMedicaHeader) {

        this.unidadMedicaHeader = unidadMedicaHeader;
    }

    public String getFechaHeader() {

        return fechaHeader;
    }

    public void setFechaHeader(String fechaHeader) {

        this.fechaHeader = fechaHeader;
    }

    public String getFormBean() {

        return formBean;
    }

    public void setFormBean(String formBean) {

        this.formBean = formBean;
    }

    public UsuarioDetalleDto getUsuarioDetalleDto() {

        return usuarioDetalleDto;
    }

    public void setUsuarioDetalleDto(UsuarioDetalleDto usuarioDetalleDto) {

        this.usuarioDetalleDto = usuarioDetalleDto;
    }

    public ConsultaExternaDto getConsultaExternaDtoSesion() {

        return consultaExternaDtoSesion;
    }

    public void setConsultaExternaDtoSesion(ConsultaExternaDto consultaExternaDtoSesion) {

        this.consultaExternaDtoSesion = consultaExternaDtoSesion;
    }

    public ConsultaExternaDto getCeDatosBusqueda() {

        return ceDatosBusqueda;
    }

    public void setCeDatosBusqueda(ConsultaExternaDto ceDatosBusqueda) {

        this.ceDatosBusqueda = ceDatosBusqueda;
    }

    public String getRole() {

        return role;
    }

    public void setRole(String role) {

        this.role = role;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getUserInfo() {

        return userInfo;
    }

    public void setUserInfo(String userInfo) {

        this.userInfo = userInfo;
    }

    public PersonalOperativo getPersonalOperativo() {

        return personalOperativo;
    }

    public void setPersonalOperativo(PersonalOperativo personalOperativo) {

        this.personalOperativo = personalOperativo;
    }

    public String getPeriodoActual() {

        return periodoActual;
    }

    public void setPeriodoActual(String periodoActual) {

        this.periodoActual = periodoActual;
    }

    public String getPeriodoActualFormato() {

        return periodoActualFormato;
    }

    public void setPeriodoActualFormato(String periodoActualFormato) {

        this.periodoActualFormato = periodoActualFormato;
    }

    public String getContexto() {

        return contexto;
    }

    public void setContexto(String contexto) {

        this.contexto = contexto;
    }

    public String getReqURI() {

        return reqURI;
    }

    public void setReqURI(String reqURI) {

        this.reqURI = reqURI;
    }

    public String getRutaPaginaInicio() {

        return rutaPaginaInicio;
    }

    public void setRutaPaginaInicio(String rutaPaginaInicio) {

        this.rutaPaginaInicio = rutaPaginaInicio;
    }

    public Integer getMaxRegistrosCExEspc() {

        return maxRegistrosCExEspc;
    }

    public void setMaxRegistrosCExEspc(Integer maxRegistrosCExEspc) {

        this.maxRegistrosCExEspc = maxRegistrosCExEspc;
    }

	public Date getFechaPeriodoCaptura() {
		return fechaPeriodoCaptura;
	}

	public void setFechaPeriodoCaptura(Date fechaPeriodoCaptura) {
		this.fechaPeriodoCaptura = fechaPeriodoCaptura;
	}
	public String getPeriodoMonitoreo() {
		return periodoMonitoreo;
	}

	public void setPeriodoMonitoreo(String periodoMonitoreo) {
		this.periodoMonitoreo = periodoMonitoreo;
	}
	public String getPeriodoMonitoreoFormato() {

        return periodoMonitoreoFormato;
    }

    public void setPeriodoMonitoreoFormato(String periodoMonitoreoFormato) {

        this.periodoMonitoreoFormato = periodoMonitoreoFormato;
    }
}
