package mx.gob.imss.simo.model;

import java.util.List;

public class MenuItem extends Item {

    // De Administracion
    private List<Item> sdUsuarios;
    private List<Item> sdMonitoreo;

    // De Consulta Extena
    private List<Item> sdConsultaExterna;

    // De Catalogos
    private List<Item> sdUnidadesMedicas;

    // de sdHospitalizacion
    private List<Item> sdIngreso4306;
    private List<Item> sdEgresos;

    // De sdReportes
    private List<Item> sdHospitalizacion;
    private List<Item> sdPrincipalesMotivosConsulta;
    private List<Item> sdCausaEgresos;
    private List<Item> sdCausaEgresosHospitalarios;
    private List<Item> sdInformeQuirofano;
    private List<Item> sdInformeCortaEstancia;
    private List<Item> sdInformeServicioUrgencias;

    private List<Item> sdUtilerias;

    private List<Item> sdBuscadores;

    public List<Item> getSdUsuarios() {

        return sdUsuarios;
    }

    public void setSdUsuarios(List<Item> sdUsuarios) {

        this.sdUsuarios = sdUsuarios;
    }

    public List<Item> getSdMonitoreo() {

        return sdMonitoreo;
    }

    public void setSdMonitoreo(List<Item> sdMonitoreo) {

        this.sdMonitoreo = sdMonitoreo;
    }

    public List<Item> getSdUnidadesMedicas() {

        return sdUnidadesMedicas;
    }

    public void setSdUnidadesMedicas(List<Item> sdUnidadesMedicas) {

        this.sdUnidadesMedicas = sdUnidadesMedicas;
    }

    public List<Item> getSdIngreso4306() {

        return sdIngreso4306;
    }

    public void setSdIngreso4306(List<Item> sdIngreso4306) {

        this.sdIngreso4306 = sdIngreso4306;
    }

    public List<Item> getSdEgresos() {

        return sdEgresos;
    }

    public void setSdEgresos(List<Item> sdEgresos) {

        this.sdEgresos = sdEgresos;
    }

    public List<Item> getSdConsultaExterna() {

        return sdConsultaExterna;
    }

    public void setSdConsultaExterna(List<Item> sdConsultaExterna) {

        this.sdConsultaExterna = sdConsultaExterna;
    }

    public List<Item> getSdHospitalizacion() {

        return sdHospitalizacion;
    }

    public void setSdHospitalizacion(List<Item> sdHospitalizacion) {

        this.sdHospitalizacion = sdHospitalizacion;
    }

    public List<Item> getSdPrincipalesMotivosConsulta() {

        return sdPrincipalesMotivosConsulta;
    }

    public void setSdPrincipalesMotivosConsulta(List<Item> sdPrincipalesMotivosConsulta) {

        this.sdPrincipalesMotivosConsulta = sdPrincipalesMotivosConsulta;
    }

    public List<Item> getSdCausaEgresos() {

        return sdCausaEgresos;
    }

    public void setSdCausaEgresos(List<Item> sdCausaEgresos) {

        this.sdCausaEgresos = sdCausaEgresos;
    }

    public List<Item> getSdCausaEgresosHospitalarios() {

        return sdCausaEgresosHospitalarios;
    }

    public void setSdCausaEgresosHospitalarios(List<Item> sdCausaEgresosHospitalarios) {

        this.sdCausaEgresosHospitalarios = sdCausaEgresosHospitalarios;
    }

    public List<Item> getSdInformeQuirofano() {

        return sdInformeQuirofano;
    }

    public void setSdInformeQuirofano(List<Item> sdInformeQuirofano) {

        this.sdInformeQuirofano = sdInformeQuirofano;
    }

    public List<Item> getSdInformeCortaEstancia() {

        return sdInformeCortaEstancia;
    }

    public void setSdInformeCortaEstancia(List<Item> sdInformeCortaEstancia) {

        this.sdInformeCortaEstancia = sdInformeCortaEstancia;
    }

    public List<Item> getSdInformeServicioUrgencias() {

        return sdInformeServicioUrgencias;
    }

    public void setSdInformeServicioUrgencias(List<Item> sdInformeServicioUrgencias) {

        this.sdInformeServicioUrgencias = sdInformeServicioUrgencias;
    }

    public List<Item> getSdUtilerias() {

        return sdUtilerias;
    }

    public void setSdUtilerias(List<Item> sdUtilerias) {

        this.sdUtilerias = sdUtilerias;
    }

    public List<Item> getSdBuscadores() {

        return sdBuscadores;
    }

    public void setSdBuscadores(List<Item> sdBuscadores) {

        this.sdBuscadores = sdBuscadores;
    }

}
