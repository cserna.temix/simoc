package mx.gob.imss.simo.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class SessionKiller extends HttpServlet {

    private static final long serialVersionUID = 970156834855977966L;

    static final Logger logger = Logger.getLogger(SessionKiller.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        logger.info("KILLING SESSION FROM  " + req.getContextPath());
        HttpSession ses = req.getSession(false);

        if (null != ses) {
            ses.invalidate();
            logger.info("SESSION INVALIDATED!");
        }

    }

}
