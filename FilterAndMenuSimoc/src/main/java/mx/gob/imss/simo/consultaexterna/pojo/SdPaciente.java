/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author francisco.rrios
 * 
 */
public class SdPaciente {

	private String refAgregadoMedico;
	private String refApellidoPaterno;
	private String refApellidoMaterno;
	private String refNombre;
	private String numNss;
	private Date fecNacimiento;
//	private String indVigencia;
	private Integer numEdad;
	private List<SdUnidadAdscripcion> sdUnidadAdscripcion = new ArrayList<SdUnidadAdscripcion>();
	private String cveIdee;
	private String cveCurp;

	private Integer indVigente;
	private Integer indPacEncontrado;
	private Date stpUltConsultaVig;

	/**
	 * @return the refAgregadoMedico
	 */
	public String getRefAgregadoMedico() {
		return refAgregadoMedico;
	}

	/**
	 * @param refAgregadoMedico
	 *            the refAgregadoMedico to set
	 */
	public void setRefAgregadoMedico(String refAgregadoMedico) {
		this.refAgregadoMedico = refAgregadoMedico;
	}

	/**
	 * @return the refApellidoPaterno
	 */
	public String getRefApellidoPaterno() {
		return refApellidoPaterno;
	}

	/**
	 * @param refApellidoPaterno
	 *            the refApellidoPaterno to set
	 */
	public void setRefApellidoPaterno(String refApellidoPaterno) {
		this.refApellidoPaterno = refApellidoPaterno;
	}

	/**
	 * @return the refApellidoMaterno
	 */
	public String getRefApellidoMaterno() {
		return refApellidoMaterno;
	}

	/**
	 * @param refApellidoMaterno
	 *            the refApellidoMaterno to set
	 */
	public void setRefApellidoMaterno(String refApellidoMaterno) {
		this.refApellidoMaterno = refApellidoMaterno;
	}

	/**
	 * @return the refNombre
	 */
	public String getRefNombre() {
		return refNombre;
	}

	/**
	 * @param refNombre
	 *            the refNombre to set
	 */
	public void setRefNombre(String refNombre) {
		this.refNombre = refNombre;
	}

	/**
	 * @return the numNss
	 */
	public String getNumNss() {
		return numNss;
	}

	/**
	 * @param numNss
	 *            the numNss to set
	 */
	public void setNumNss(String numNss) {
		this.numNss = numNss;
	}

	/**
	 * @return the fecNacimiento
	 */
	public Date getFecNacimiento() {
		return fecNacimiento;
	}

	/**
	 * @param fecNacimiento
	 *            the fecNacimiento to set
	 */
	public void setFecNacimiento(Date fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

	/**
	 * @return the indVigencia
	 */
//	public String getIndVigencia() {
//		return indVigencia;
//	}
//
//	/**
//	 * @param indVigencia
//	 *            the indVigencia to set
//	 */
//	public void setIndVigencia(String indVigencia) {
//		this.indVigencia = indVigencia;
//	}

	/**
	 * @return the sdUnidadAdscripcion
	 */
	public List<SdUnidadAdscripcion> getSdUnidadAdscripcion() {
		return sdUnidadAdscripcion;
	}

	/**
	 * @param sdUnidadAdscripcion
	 *            the sdUnidadAdscripcion to set
	 */
	public void setSdUnidadAdscripcion(List<SdUnidadAdscripcion> sdUnidadAdscripcion) {
		this.sdUnidadAdscripcion = sdUnidadAdscripcion;
	}

	/**
	 * @return the numEdad
	 */
	public Integer getNumEdad() {
		return numEdad;
	}

	/**
	 * @param numEdad
	 *            the numEdad to set
	 */
	public void setNumEdad(Integer numEdad) {
		this.numEdad = numEdad;
	}

	public String getCveIdee() {
		return cveIdee;
	}

	public void setCveIdee(String cveIdee) {
		this.cveIdee = cveIdee;
	}

	public String getCveCurp() {
		return cveCurp;
	}

	public void setCveCurp(String cveCurp) {
		this.cveCurp = cveCurp;
	}

	public Integer getIndVigente() {
		return indVigente;
	}

	public void setIndVigente(Integer indVigente) {
		this.indVigente = indVigente;
	}

	public Integer getIndPacEncontrado() {
		return indPacEncontrado;
	}

	public void setIndPacEncontrado(Integer indPacEncontrado) {
		this.indPacEncontrado = indPacEncontrado;
	}

	public Date getStpUltConsultaVig() {
		return stpUltConsultaVig;
	}

	public void setStpUltConsultaVig(Date stpUltConsultaVig) {
		this.stpUltConsultaVig = stpUltConsultaVig;
	}

}
