/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author francisco.rrios
 * 
 */
public class SdDiagnosticos {

	private List<SdDiagnosticoPrincipal> sdDiagnosticoPrincipal = new ArrayList<SdDiagnosticoPrincipal>();
	private List<SdDiagnosticoAdicional> sdDiagnosticoAdicional = new ArrayList<SdDiagnosticoAdicional>();
	private List<SdDiagnosticoComplementario> sdDiagnosticoComplementario = new ArrayList<SdDiagnosticoComplementario>();

	/**
	 * @return the sdDiagnosticoPrincipal
	 */
	public List<SdDiagnosticoPrincipal> getSdDiagnosticoPrincipal() {
		return sdDiagnosticoPrincipal;
	}

	/**
	 * @param sdDiagnosticoPrincipal
	 *            the sdDiagnosticoPrincipal to set
	 */
	public void setSdDiagnosticoPrincipal(
			List<SdDiagnosticoPrincipal> sdDiagnosticoPrincipal) {
		this.sdDiagnosticoPrincipal = sdDiagnosticoPrincipal;
	}

	/**
	 * @return the sdDiagnosticoAdicional
	 */
	public List<SdDiagnosticoAdicional> getSdDiagnosticoAdicional() {
		return sdDiagnosticoAdicional;
	}

	/**
	 * @param sdDiagnosticoAdicional
	 *            the sdDiagnosticoAdicional to set
	 */
	public void setSdDiagnosticoAdicional(
			List<SdDiagnosticoAdicional> sdDiagnosticoAdicional) {
		this.sdDiagnosticoAdicional = sdDiagnosticoAdicional;
	}

	/**
	 * @return the sdDiagnosticoComplementario
	 */
	public List<SdDiagnosticoComplementario> getSdDiagnosticoComplementario() {
		return sdDiagnosticoComplementario;
	}

	/**
	 * @param sdDiagnosticoComplementario
	 *            the sdDiagnosticoComplementario to set
	 */
	public void setSdDiagnosticoComplementario(
			List<SdDiagnosticoComplementario> sdDiagnosticoComplementario) {
		this.sdDiagnosticoComplementario = sdDiagnosticoComplementario;
	}

}
