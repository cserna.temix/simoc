package mx.gob.imss.simo.service;

import mx.gob.imss.simo.model.PersonalOperativo;

public interface UserService {

    PersonalOperativo getUsuario(String username);
    
    PersonalOperativo getUsuarioByActivo(String username, Integer indActivo);
}
