package mx.gob.imss.simo.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import mx.gob.imss.simo.model.DatosSesion;

public interface DatosSesionRepository extends MongoRepository<DatosSesion, ObjectId> {

    DatosSesion findOneByNetIQSessionIdAndUsuario(String netIQSessionId, String username);

    @Override
    <S extends DatosSesion> S save(S entity);

    @Override
    void delete(DatosSesion entity);

}
