/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author francisco.rrios
 * 
 */
public class SdEncabezado {

	private Date fecAltaEncabezado;
	private List<SdEspecialidades> sdEspecialidad = new ArrayList<SdEspecialidades>();
	private List<SdMedico> sdMedico = new ArrayList<SdMedico>();
	private List<SdConsultorio> sdConsultorio = new ArrayList<SdConsultorio>();
	private List<SdTipoPrestador> sdTipoPrestador = new ArrayList<SdTipoPrestador>();
	private List<SdTurno> sdTurno = new ArrayList<SdTurno>();
	private String numHorasTrabajadas;
	private Integer numConsultasNoOtorgadas;
	private Integer numCitasNoCumplidas;
	private List<SdFormato> sdFormato = new ArrayList<SdFormato>();

	/**
     * 
     */
	public SdEncabezado() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param fecAltaEncabezado
	 * @param medico
	 * @param consultorio
	 * @param numHorasTrabajadas
	 * @param numConsultasNoOtorgadas
	 * @param numCitasNoCumplidas
	 */
	public SdEncabezado(Date fecAltaEncabezado, String numHorasTrabajadas,
			Integer numConsultasNoOtorgadas, Integer numCitasNoCumplidas) {
		super();
		this.fecAltaEncabezado = fecAltaEncabezado;
		this.numHorasTrabajadas = numHorasTrabajadas;
		this.numConsultasNoOtorgadas = numConsultasNoOtorgadas;
		this.numCitasNoCumplidas = numCitasNoCumplidas;
	}

	/**
	 * @return the fecAltaEncabezado
	 */
	public Date getFecAltaEncabezado() {
		return fecAltaEncabezado;
	}

	/**
	 * @param fecAltaEncabezado
	 *            the fecAltaEncabezado to set
	 */
	public void setFecAltaEncabezado(Date fecAltaEncabezado) {
		this.fecAltaEncabezado = fecAltaEncabezado;
	}

	/**
	 * @return the sdEspecialidad
	 */
	public List<SdEspecialidades> getSdEspecialidad() {
		return sdEspecialidad;
	}

	/**
	 * @param sdEspecialidad
	 *            the sdEspecialidad to set
	 */
	public void setSdEspecialidad(List<SdEspecialidades> sdEspecialidad) {
		this.sdEspecialidad = sdEspecialidad;
	}

	/**
	 * @return the sdMedico
	 */
	public List<SdMedico> getSdMedico() {
		return sdMedico;
	}

	/**
	 * @param sdMedico
	 *            the sdMedico to set
	 */
	public void setSdMedico(List<SdMedico> sdMedico) {
		this.sdMedico = sdMedico;
	}

	/**
	 * @return the sdConsultorio
	 */
	public List<SdConsultorio> getSdConsultorio() {
		return sdConsultorio;
	}

	/**
	 * @param sdConsultorio
	 *            the sdConsultorio to set
	 */
	public void setSdConsultorio(List<SdConsultorio> sdConsultorio) {
		this.sdConsultorio = sdConsultorio;
	}

	/**
	 * @return the sdTipoPrestador
	 */
	public List<SdTipoPrestador> getSdTipoPrestador() {
		return sdTipoPrestador;
	}

	/**
	 * @param sdTipoPrestador
	 *            the sdTipoPrestador to set
	 */
	public void setSdTipoPrestador(List<SdTipoPrestador> sdTipoPrestador) {
		this.sdTipoPrestador = sdTipoPrestador;
	}

	/**
	 * @return the sdTurno
	 */
	public List<SdTurno> getSdTurno() {
		return sdTurno;
	}

	/**
	 * @param sdTurno
	 *            the sdTurno to set
	 */
	public void setSdTurno(List<SdTurno> sdTurno) {
		this.sdTurno = sdTurno;
	}

	/**
	 * @return the numHorasTrabajadas
	 */
	public String getNumHorasTrabajadas() {
		return numHorasTrabajadas;
	}

	/**
	 * @param numHorasTrabajadas
	 *            the numHorasTrabajadas to set
	 */
	public void setNumHorasTrabajadas(String numHorasTrabajadas) {
		this.numHorasTrabajadas = numHorasTrabajadas;
	}

	/**
	 * @return the numConsultasNoOtorgadas
	 */
	public Integer getNumConsultasNoOtorgadas() {
		return numConsultasNoOtorgadas;
	}

	/**
	 * @param numConsultasNoOtorgadas
	 *            the numConsultasNoOtorgadas to set
	 */
	public void setNumConsultasNoOtorgadas(Integer numConsultasNoOtorgadas) {
		this.numConsultasNoOtorgadas = numConsultasNoOtorgadas;
	}

	/**
	 * @return the numCitasNoCumplidas
	 */
	public Integer getNumCitasNoCumplidas() {
		return numCitasNoCumplidas;
	}

	/**
	 * @param numCitasNoCumplidas
	 *            the numCitasNoCumplidas to set
	 */
	public void setNumCitasNoCumplidas(Integer numCitasNoCumplidas) {
		this.numCitasNoCumplidas = numCitasNoCumplidas;
	}

	/**
	 * @return the sdFormato
	 */
	public List<SdFormato> getSdFormato() {
		return sdFormato;
	}

	/**
	 * @param sdFormato
	 *            the sdFormato to set
	 */
	public void setSdFormato(List<SdFormato> sdFormato) {
		this.sdFormato = sdFormato;
	}

}
