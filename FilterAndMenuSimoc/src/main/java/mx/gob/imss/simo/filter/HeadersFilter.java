package mx.gob.imss.simo.filter;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.model.DatosSesion;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.model.PersonalOperativo;
import mx.gob.imss.simo.model.Rol;
import mx.gob.imss.simo.service.CatalogoService;
import mx.gob.imss.simo.service.DatosSesionService;
import mx.gob.imss.simo.service.MenuService;
import mx.gob.imss.simo.service.UserService;

/**
 * Servlet Filter implementation class HeadersFilter
 */
@Component
public class HeadersFilter implements Filter {

    static final Logger logger = Logger.getLogger(HeadersFilter.class);
    private static final String OBJETO_SESION = "objetosSs";

    private static final String CONSULTA_EXTERNA_CTX = "/SIMOCentral";
    private static final String HOSPITALIZACION_CTX = "/hospitalizacion";
    private static final String CONSULTA_EXTERNA_CTX2 = "/SIMOCentral2";
    private static final String HOSPITALIZACION_CTX2 = "/hospitalizacion2";
    private static final String ENLACES_CTX = "/enlaces";
    private static final String MONITOREO_CTX = "/monitoreo";
    private static final String ENLACES_CTX2 = "/enlaces2";
    private static final String MONITOREO_CTX2 = "/monitoreo2";
    private static final String CAPTURA_INFO_COMPLEMENTARIA = "InformacionComplementaria";
    private static final String SUBROGADOS = "ServiciosSubrogados";
    private static final String DB_PROPERTIES = "database.properties";

    @Autowired
    UserService userService;
    @Autowired
    DatosSesionService datosSesionService;
    @Autowired
    MenuService menuService;
    @Autowired
    CatalogoService catalogoService;

    /**
     * Default constructor.
     */
    public HeadersFilter() {

        /*
         * 
         */
    }

    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {

        // logger.info(">>>>>>>>>>>>>>>Filter Destroyed!!!");
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

//         logger.info("=====Filter Ready=====");

        HttpSession session = ((HttpServletRequest) request).getSession();
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String contexto = req.getContextPath();
        String reqURI = req.getRequestURI();
        String role = req.getHeader("SIMOC_ROL");
        String username = req.getHeader("SIMOC_USERNAME");
        String userInfo = req.getHeader("SIMOC_USR_INFO");

        DatosSesion datSes;
        ObjetosEnSesionBean os;
        PersonalOperativo user;
        MenuModel menu;
        // logger.info("Invocando Filtro desde: " + contexto);
        // logger.info("NetIQ SIMOC_ROL=" + role);
        // logger.info("NetIQ SIMOC_USERNAME=" + username);
        // logger.info("NetIQ SIMOC_USR_INFO=" + userInfo);
        // logger.info("Weblogic session.getId()=" + session.getId());
        // logger.info("Weblogic session.getCreationTime()=" + session.getCreationTime() + ", "
        // + new Date(session.getCreationTime()));
        // logger.info("Weblogic session.getLastAccessedTime()=" + session.getLastAccessedTime() + ", "
        // + new Date(session.getLastAccessedTime()));

        os = (ObjetosEnSesionBean) session.getAttribute(OBJETO_SESION);
        
        if (null == os) {
            os = new ObjetosEnSesionBean();
            os.setMostrarDialogoDelegaciones(Boolean.TRUE);
            os.setContexto(contexto);
            os.setReqURI(reqURI);
//             logger.info("OS es NULL, creando nueva instancia...");
            session.setAttribute(OBJETO_SESION, os);
        }else {
            os.setContexto(contexto);
            os.setReqURI(reqURI);
        }
        if (null == username) {
            logger.error("Atributos Header NULOS ----------");
            logger.error("Atributos Header nulos, creando datos de prueba...");
            role = "ROLE_ADMIN_CENTRAL";
			username = "jesus.emanuel";
			userInfo = "wwwwwwwwwwwwwwwwwwwwwwwww";
			logger.error("::Role:UserName:UserInfo:: " + role + " " + username + " " + userInfo);

        }
        if (null == os.getPersonalOperativo()) {
//             logger.info("Obteniendo datos del usuario...");
            user = userService.getUsuario(username);
            os.setPersonalOperativo(user);
            if (null == user) {
                logger.error("Usuario no encontrado en la base de datos: " + username);
            } else {
                logger.info(user.getRefCuentaActiveD());
                setDatosUsuario(os, user, userInfo, username);
            }
        } else {
            user = userService.getUsuario(username);
            setDatosUsuario(os, user, userInfo, username);
        }

        // if (null == os.getPropertiesFilter()) {
        // os.setPropertiesFilter(new Properties());
        // os.getPropertiesFilter().load(getClass().getClassLoader().getResourceAsStream(DB_PROPERTIES));
        // }

        if (null == os.getMenu()) {
        	logger.info("Role : "+role);
            String[] parts = role.split(",");
            // logger.info("----------parts:"+parts[0]);
            menu = menuService.createMenu(parts[0]);
            os.setRutaPaginaInicio(menuService.getAmbienteCE());
            // logger.info("Menu creado...");
             logger.info("Asignando menu...");
//            os.setMenu(menu);
            PersonalOperativo personalOperativoActivo = userService.getUsuarioByActivo(username, 1);
            if(personalOperativoActivo !=null){
            	logger.info(personalOperativoActivo.getRefCuentaActiveD());
            	logger.info(personalOperativoActivo.getIndActivo());
            	os.setMenu(menu);
        	}else{
        		logger.info("Usuario inactivo");
        	}
        	
        }
        os.setRole(role);
        os.setUsername(username);
        os.setUserInfo(userInfo);
        datSes = datosSesionService.getDatosSesion(userInfo, username);
        if (null == datSes) {
            os.setMostrarDialogoDelegaciones(Boolean.TRUE);
        } else {
            if (null != datSes.getMostrarDialogoDelegacion()) {
                os.setMostrarDialogoDelegaciones(datSes.getMostrarDialogoDelegacion());
            }
        }
        // logger.info("Filter MostrarDialogoDelegaciones: " + os.getMostrarDialogoDelegaciones());
        session.setAttribute(OBJETO_SESION, os);

        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        // res.setHeader("Access-Control-Allow-Origin","*");
        chain.doFilter(request, res);
    }

    private void setDatosUsuario(ObjetosEnSesionBean os, PersonalOperativo user, String userInfo, String username) {

        DatosSesion datosSesion = datosSesionService.getDatosSesion(userInfo, username);
        String periodoActual = "000000";
        String periodoMonitoreo = "";
        Date fechaPeriodoCaptura_ = null;

        os.getDatosUsuario().setUsuario(user.getRefCuentaActiveD());
        os.getDatosUsuario().setNombre(user.getRefNombre());
        os.getDatosUsuario().setAppPaterno(user.getRefApellidoPaterno());
        os.getDatosUsuario().setAppMaterno(user.getRefApellidoMaterno());
        for (Rol rol : user.getSdRol()) {
            os.getDatosUsuario().setPerfil(rol.getDesRol());
        }

        if (null != datosSesion) {
            // logger.info("datosSesion presupuestal:"+datosSesion.getCvePresupuestalAct()+"
            // usuario:"+datosSesion.getUsuario()+" contexto:"+os.getContexto()+"os.getReqURI()"+os.getReqURI());
            os.getDatosUsuario().setCvePresupuestal(datosSesion.getCvePresupuestalAct());
            os.getDatosUsuario().setCveDelegacionUmae(datosSesion.getCveDelegacionAct());
            os.getDatosUsuario().setDesDelegacionAct(datosSesion.getDesDelegacionAct());
            os.getDatosUsuario().setDesUnidadMedicaAct(datosSesion.getDesUnidadMedicaAct());
            try {
                if (getTipoCaptura(os.getContexto(), os.getReqURI()) != 1
                        || getTipoCaptura(os.getContexto(), os.getReqURI()) != 0) {
                    periodoActual = catalogoService.getPeriodoActual(datosSesion.getCvePresupuestalAct(),
                            getTipoCaptura(os.getContexto(), os.getReqURI()));
                    periodoMonitoreo = catalogoService.getPeriodoMonitoreo(datosSesion.getCvePresupuestalAct(),
                            getTipoCaptura(os.getContexto(), os.getReqURI()));
                    if(getTipoCaptura(os.getContexto(), os.getReqURI()).equals(new Integer(2))) {
	                    Date ultimaFechaCaptura = catalogoService.obtenerUltimaFechaCaptura(
	                            datosSesion.getCvePresupuestalAct(), getTipoCaptura(os.getContexto(), os.getReqURI()));
	                    Calendar calendar = Calendar.getInstance();
	                    calendar.setTime(ultimaFechaCaptura);
	                    calendar.add(Calendar.DAY_OF_YEAR, 1);
	                    os.fechaPeriodoCaptura = calendar.getTime();
                    }
                }
                os.setPeriodoActual(periodoActual);
                os.setPeriodoActualFormato(getPeriodo(periodoActual));
                os.setPeriodoMonitoreo(periodoMonitoreo);
                os.setPeriodoMonitoreoFormato(getPeriodo(periodoMonitoreo));
            
            } catch (Exception e) {
                logger.error("Error al obtener periodo actual", e);
            }
            os.getUsuarioDetalleDto().setUsuario(user.getRefCuentaActiveD());
            os.getUsuarioDetalleDto().setNombre(user.getRefNombre());
            os.getUsuarioDetalleDto().setAppPaterno(user.getRefApellidoPaterno());
            os.getUsuarioDetalleDto().setAppMaterno(user.getRefApellidoMaterno());
            os.getUsuarioDetalleDto().setPerfil(os.getDatosUsuario().getPerfil());
            os.getUsuarioDetalleDto().setCvePresupuestal(os.getDatosUsuario().getCvePresupuestal());
            os.getUsuarioDetalleDto().setCveDelegacionUmae(os.getDatosUsuario().getCveDelegacionUmae());
            os.getUsuarioDetalleDto().setDesDelegacionAct(os.getDatosUsuario().getDesDelegacionAct());
            os.getUsuarioDetalleDto().setDesUnidadMedicaAct(os.getDatosUsuario().getDesUnidadMedicaAct());

        } else {
            try {
                if (getTipoCaptura(os.getContexto(), os.getReqURI()) != 1
                        || getTipoCaptura(os.getContexto(), os.getReqURI()) != 0) {
                    periodoActual = catalogoService.getPeriodoActual(
                            os.getPersonalOperativo().getSdUnidadMedica().get(0).getCvePresupuestal(),
                            getTipoCaptura(os.getContexto(), os.getReqURI()));
                    periodoMonitoreo = catalogoService.getPeriodoMonitoreo(os.getPersonalOperativo().getSdUnidadMedica().get(0).getCvePresupuestal(),
                            getTipoCaptura(os.getContexto(), os.getReqURI()));
                }
                os.setPeriodoActual(periodoActual);
                os.setPeriodoActualFormato(getPeriodo(periodoActual));
                os.setPeriodoMonitoreo(periodoMonitoreo);
                os.setPeriodoMonitoreoFormato(getPeriodo(periodoMonitoreo));                
            } catch (SQLException e) {
                logger.error("Error al obtener periodo actual", e);
            }
            os.getUsuarioDetalleDto().setUsuario(user.getRefCuentaActiveD());
            os.getUsuarioDetalleDto().setNombre(user.getRefNombre());
            os.getUsuarioDetalleDto().setAppPaterno(user.getRefApellidoPaterno());
            os.getUsuarioDetalleDto().setAppMaterno(user.getRefApellidoMaterno());
            os.getUsuarioDetalleDto().setPerfil(os.getDatosUsuario().getPerfil());
            os.getUsuarioDetalleDto().setCvePresupuestal(os.getDatosUsuario().getCvePresupuestal());
            os.getUsuarioDetalleDto().setCveDelegacionUmae(os.getDatosUsuario().getCveDelegacionUmae());
            os.getUsuarioDetalleDto().setDesDelegacionAct(os.getDatosUsuario().getDesDelegacionAct());
            os.getUsuarioDetalleDto().setDesUnidadMedicaAct(os.getDatosUsuario().getDesUnidadMedicaAct());
            os.getDatosUsuario()
                    .setCveDelegacionUmae(os.getPersonalOperativo().getSdUnidadMedica().get(0).getCveDelegacion());
            os.getDatosUsuario()
                    .setDesDelegacionAct(os.getPersonalOperativo().getSdUnidadMedica().get(0).getDesDelegacion());
            os.getDatosUsuario()
                    .setCvePresupuestal(os.getPersonalOperativo().getSdUnidadMedica().get(0).getCvePresupuestal());
            os.getDatosUsuario()
                    .setDesUnidadMedicaAct(os.getPersonalOperativo().getSdUnidadMedica().get(0).getDesUnidadMedica());
        }

    }

    private String getPeriodo(String periodoActual) {

        SimpleDateFormat formatoMesAnio = new SimpleDateFormat("MMMM YYYY", new Locale("es", "MX"));
        String periodo = "";
        String anio;
        String mes;
        Calendar cal;
        if (null != periodoActual) {
            anio = periodoActual.substring(0, 4);
            mes = periodoActual.substring(4, periodoActual.length());
            cal = new GregorianCalendar(Integer.valueOf(anio), Integer.valueOf(mes) - 1, 1);
            periodo = formatoMesAnio.format(cal.getTime());
        }

        return periodo.toUpperCase();
    }

    private Integer getTipoCaptura(String contexto, String reqURI) {

        Integer tipoCaptura = 0;
        // logger.info("REQUEST URI: " + reqURI);

        switch (contexto) {
            case CONSULTA_EXTERNA_CTX:
            case CONSULTA_EXTERNA_CTX2:
                tipoCaptura = 1;
                break;
            case HOSPITALIZACION_CTX:
            case HOSPITALIZACION_CTX2:
                tipoCaptura = 2;
                break;
            case MONITOREO_CTX2:
            case MONITOREO_CTX:
            case ENLACES_CTX:
            case ENLACES_CTX2:
                if (reqURI.contains(CAPTURA_INFO_COMPLEMENTARIA)) {
                    tipoCaptura = 3;
                } else if (reqURI.contains(SUBROGADOS)) {
                    tipoCaptura = 4;
                }
                break;

            default:
                break;
        }
        return tipoCaptura;
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(FilterConfig fConfig) throws ServletException {

    	logger.info("ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
    	
        /*
         * 
         */
    }
}
