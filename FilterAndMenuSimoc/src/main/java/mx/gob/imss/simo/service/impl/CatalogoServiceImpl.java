package mx.gob.imss.simo.service.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.model.DelegacionIMSS;
import mx.gob.imss.simo.model.PeriodoOperacion;
import mx.gob.imss.simo.model.UnidadMedica;
import mx.gob.imss.simo.repository.DelegacionRepository;
import mx.gob.imss.simo.repository.PeriodoOperacionRepository;
import mx.gob.imss.simo.repository.UnidadMedicaRepository;
import mx.gob.imss.simo.service.CatalogoService;

@Service("catalogoService")
public class CatalogoServiceImpl implements CatalogoService {

    @Autowired
    DelegacionRepository delegacionRepository;

    @Autowired
    UnidadMedicaRepository unidadMedicaRepository;

    @Autowired
    PeriodoOperacionRepository periodoOperacionRepository;

    @Override
    public List<DelegacionIMSS> obtenerDelegacionesIMSS() {

        return delegacionRepository.findAll();
    }

    @Override
    public List<UnidadMedica> obtenerUnidadesMedicasPorDelegacion(String cveDelegacion) {

        return unidadMedicaRepository.findByCveDelegacionOrderByDesDelegacionAsc(cveDelegacion);
    }

    @Override
    public String getPeriodoActual(String cvePresupuestal, Integer tipoCaptura) throws SQLException {

        PeriodoOperacion po= periodoOperacionRepository.getPeriodoActual(cvePresupuestal, tipoCaptura);
        if(po!=null){
        	return	po.getClavePeriodoIMSS();
        }else{
        	return "000000";
        }
         
    }
    
    public Date obtenerUltimaFechaCaptura(String cvePresupuestal, Integer tipoCaptura) throws Exception {

        return periodoOperacionRepository.obtenerUltimaFechaCaptura(cvePresupuestal, tipoCaptura);
    }  
    
    @Override
    public  String getPeriodoMonitoreo(String cvePresupuestal, Integer tipoCaptura)throws SQLException{
      PeriodoOperacion pm = periodoOperacionRepository.getPeriodoMonitoreo(cvePresupuestal, tipoCaptura);
      
      if(pm!=null){
    	 return	pm.getClavePeriodoIMSS();
      }else{
      	return null;
      }     
    }

}
