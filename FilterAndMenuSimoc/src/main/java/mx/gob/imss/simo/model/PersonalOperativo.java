package mx.gob.imss.simo.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SIT_PERSONAL_OPERATIVO")
public class PersonalOperativo {

    @Id
    private ObjectId id;
    private String refCuentaActiveD;
    private String cveMatricula;
    private String refNombre;
    private String refApellidoPaterno;
    private String refApellidoMaterno;
    private List<Rol> sdRol;
    private List<UnidadMedica> sdUnidadMedica;

    private String refNumTelefono;
    private String refNumExtension;
    private String refEmail;
    private Integer indActivo;
    private int indBloqueado;
    private Date fecBaja;
    private Integer caIntentos;

    public ObjectId getId() {

        return id;
    }

    public void setId(ObjectId id) {

        this.id = id;
    }

    public String getRefCuentaActiveD() {

        return refCuentaActiveD;
    }

    public void setRefCuentaActiveD(String refCuentaActiveD) {

        this.refCuentaActiveD = refCuentaActiveD;
    }

    public String getCveMatricula() {

        return cveMatricula;
    }

    public void setCveMatricula(String cveMatricula) {

        this.cveMatricula = cveMatricula;
    }

    public String getRefNombre() {

        return refNombre;
    }

    public void setRefNombre(String refNombre) {

        this.refNombre = refNombre;
    }

    public String getRefApellidoPaterno() {

        return refApellidoPaterno;
    }

    public void setRefApellidoPaterno(String refApellidoPaterno) {

        this.refApellidoPaterno = refApellidoPaterno;
    }

    public String getRefApellidoMaterno() {

        return refApellidoMaterno;
    }

    public void setRefApellidoMaterno(String refApellidoMaterno) {

        this.refApellidoMaterno = refApellidoMaterno;
    }

    public List<Rol> getSdRol() {

        return sdRol;
    }

    public void setSdRol(List<Rol> sdRol) {

        this.sdRol = sdRol;
    }

    public List<UnidadMedica> getSdUnidadMedica() {

        return sdUnidadMedica;
    }

    public void setSdUnidadMedica(List<UnidadMedica> sdUnidadMedica) {

        this.sdUnidadMedica = sdUnidadMedica;
    }

    public String getRefNumTelefono() {

        return refNumTelefono;
    }

    public void setRefNumTelefono(String refNumTelefono) {

        this.refNumTelefono = refNumTelefono;
    }

    public String getRefNumExtension() {

        return refNumExtension;
    }

    public void setRefNumExtension(String refNumExtension) {

        this.refNumExtension = refNumExtension;
    }

    public String getRefEmail() {

        return refEmail;
    }

    public void setRefEmail(String refEmail) {

        this.refEmail = refEmail;
    }

    public Integer getIndActivo() {

        return indActivo;
    }

    public void setIndActivo(Integer indActivo) {

        this.indActivo = indActivo;
    }

    public int getIndBloqueado() {

        return indBloqueado;
    }

    public void setIndBloqueado(int indBloqueado) {

        this.indBloqueado = indBloqueado;
    }

    public Date getFecBaja() {

        return fecBaja;
    }

    public void setFecBaja(Date fecBaja) {

        this.fecBaja = fecBaja;
    }

    public Integer getCaIntentos() {

        return caIntentos;
    }

    public void setCaIntentos(Integer caIntentos) {

        this.caIntentos = caIntentos;
    }

    @Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PersonalOperativo [id=");
		builder.append(id);
		builder.append(", refCuentaActiveD=");
		builder.append(refCuentaActiveD);
		builder.append(", cveMatricula=");
		builder.append(cveMatricula);
		builder.append(", refNombre=");
		builder.append(refNombre);
		builder.append(", refApellidoPaterno=");
		builder.append(refApellidoPaterno);
		builder.append(", refApellidoMaterno=");
		builder.append(refApellidoMaterno);
		builder.append(", sdRol=");
		builder.append(sdRol);
		builder.append(", sdUnidadMedica=");
		builder.append(sdUnidadMedica);
		builder.append(", refNumTelefono=");
		builder.append(refNumTelefono);
		builder.append(", refNumExtension=");
		builder.append(refNumExtension);
		builder.append(", refEmail=");
		builder.append(refEmail);
		builder.append(", indActivo=");
		builder.append(indActivo);
		builder.append(", indBloqueado=");
		builder.append(indBloqueado);
		builder.append(", fecBaja=");
		builder.append(fecBaja);
		builder.append(", caIntentos=");
		builder.append(caIntentos);
		builder.append("]");
		return builder.toString();
	}

}
