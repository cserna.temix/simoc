package mx.gob.imss.simo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;

public class ConsultaExternaDto implements Serializable {

    private static final long serialVersionUID = -4434618593536770894L;
    private String idCapturista;
    private String idPerfil;
    private String idDelegacion;
    private String idUnidadMedica;

    private Date fecNacimiento;
    private Integer edad;
    private String nombresMedi;
    private String aPaternoMedi;
    private String aMaternoMedi;
    private boolean vigencia;
    private String ocasionServicio;

    private String id;
    private Integer numConsulta;
    private Date fecha;
    private String matriculaMedico;
    private String nombreMedico;
    private String cveEspecialidad;
    private String especialidad;
    private String especialidadSelc;
    private String desEspecialidad;
    private String tipoPrestador;
    private String desTipoPrestador;
    private String consultorio;
    private String horasTrabajadas;
    private String turno;
    private String desTurno;
    private Integer consultasNoOtorgadas;
    private Integer citasNoCumplidas;
    private String nss;
    private String agregadoMedico;
    private String nombre;
    private String apePaterno;
    private String apeMaterno;
    private String unidadAdscripcion;
    private String nombreUnidadMed;
    private String horaCita;
    private Integer primeraVez;
    private Integer citado;
    private Integer pase;
    private Integer recetas;
    private Integer diasIncapacidad;
    private Integer visitas;
    private Integer metodoPpf;
    private Integer cantidadMpf;
    private Integer accidentesLesiones;
    private Integer riesgosTrabajo;
    private Integer actiPerParamedico;
    private Integer tipoDocumento;
    private String codigoCie;
    private Integer cveTipoUrgencia;

    private String diagnosticoPrincipal;
    private String desDiagPrin;
    private String cveInforAdicPrin;
    private String cveTipoPrin;
    private Date fecCirugiaPrin;

    private Integer diagAcc;

    private String diagnosticoAdicional;
    private String desDiagAdic;
    private Integer primeraVezDiagAdic;
    private String cveInforAdicAdic;
    private String cveTipoAdic;
    private Date fecCirugiaAdic;

    private String diagnosticoComplemento;
    private String desDiagComp;
    private Integer primeraVezDiagComp;
    private String cveInforAdicComp;
    private String cveTipoComp;
    private Date fecCirugiaComp;

    private String procedimientoPrincipal;
    private String desProcPrin;
    private String procedimientoAdicional;
    private String desProcAdic;
    private String procedimientoComplemento;
    private String desProcComp;

    private Date fecCaptura;
    private String morfologia;

    private Boolean editable = Boolean.TRUE;
    private Boolean consultar = Boolean.FALSE;

    private int cveRamaAseguramiento;
    private String descRamaAseguramiento;
    private Integer cveCD4Principal;
    private Integer cveCD4Adicional;
    private Integer cveCD4Compl;
    private Integer numTipoServicio;
    private Integer cveTipoEmbarazo;
    private Integer cveTipoEmbarazoAdicional;
    private Integer cveTipoEmbarazoComplementario;

    private String cveCurp;
    private Integer indVigente;
    private Integer indPacEncontrado;
    private Date stpUltConsultaVig;
    private String cveIdee;
    private Integer cveTipoInfoAdDiagPrin;
    private Integer cveTipoInfoAdDiagAdic;
    private Integer cveTipoInfoAdDiagCompl;
    
    private Date stpCaptura;
	private Date stpActualiza;
	
	private List<SdEncabezado> sdEncabezado = new ArrayList<>();
	
	private List<SdUnidadMedica> sdUnidadMedica = new ArrayList<>();

    public ConsultaExternaDto() {
    }

    public ConsultaExternaDto(Date fecha, String matriculaMedico, String consultorio, String horasTrabajadas,
            Integer consultasNoOtorgadas, Integer citasNoCumplidas) {
        this.fecha = fecha;
        this.matriculaMedico = matriculaMedico;
        this.consultorio = consultorio;
        this.horasTrabajadas = horasTrabajadas;
        this.consultasNoOtorgadas = consultasNoOtorgadas;
        this.citasNoCumplidas = citasNoCumplidas;
    }

    public String getIdCapturista() {

        return idCapturista;
    }

    public void setIdCapturista(String idCapturista) {

        this.idCapturista = idCapturista;
    }

    public String getIdPerfil() {

        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {

        this.idPerfil = idPerfil;
    }

    public String getIdDelegacion() {

        return idDelegacion;
    }

    public void setIdDelegacion(String idDelegacion) {

        this.idDelegacion = idDelegacion;
    }

    public String getIdUnidadMedica() {

        return idUnidadMedica;
    }

    public void setIdUnidadMedica(String idUnidadMedica) {

        this.idUnidadMedica = idUnidadMedica;
    }

    public Date getFecNacimiento() {

        return fecNacimiento;
    }

    public void setFecNacimiento(Date fecNacimiento) {

        this.fecNacimiento = fecNacimiento;
    }

    public Integer getEdad() {

        return edad;
    }

    public void setEdad(Integer edad) {

        this.edad = edad;
    }

    public String getNombresMedi() {

        return nombresMedi;
    }

    public void setNombresMedi(String nombresMedi) {

        this.nombresMedi = nombresMedi;
    }

    public String getaPaternoMedi() {

        return aPaternoMedi;
    }

    public void setaPaternoMedi(String aPaternoMedi) {

        this.aPaternoMedi = aPaternoMedi;
    }

    public String getaMaternoMedi() {

        return aMaternoMedi;
    }

    public void setaMaternoMedi(String aMaternoMedi) {

        this.aMaternoMedi = aMaternoMedi;
    }

    public boolean isVigencia() {

        return vigencia;
    }

    public void setVigencia(boolean vigencia) {

        this.vigencia = vigencia;
    }

    public String getOcasionServicio() {

        return ocasionServicio;
    }

    public void setOcasionServicio(String ocasionServicio) {

        this.ocasionServicio = ocasionServicio;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public Integer getNumConsulta() {

        return numConsulta;
    }

    public void setNumConsulta(Integer numConsulta) {

        this.numConsulta = numConsulta;
    }

    public Date getFecha() {

        return fecha;
    }

    public void setFecha(Date fecha) {

        this.fecha = fecha;
    }

    public String getMatriculaMedico() {

        return matriculaMedico;
    }

    public void setMatriculaMedico(String matriculaMedico) {

        this.matriculaMedico = matriculaMedico;
    }

    public String getNombreMedico() {

        return nombreMedico;
    }

    public void setNombreMedico(String nombreMedico) {

        this.nombreMedico = nombreMedico;
    }

    public String getCveEspecialidad() {

        return cveEspecialidad;
    }

    public void setCveEspecialidad(String cveEspecialidad) {

        this.cveEspecialidad = cveEspecialidad;
    }

    public String getEspecialidad() {

        return especialidad;
    }

    public void setEspecialidad(String especialidad) {

        this.especialidad = especialidad;
    }

    public String getEspecialidadSelc() {

        return especialidadSelc;
    }

    public void setEspecialidadSelc(String especialidadSelc) {

        this.especialidadSelc = especialidadSelc;
    }

    public String getDesEspecialidad() {

        return desEspecialidad;
    }

    public void setDesEspecialidad(String desEspecialidad) {

        this.desEspecialidad = desEspecialidad;
    }

    public String getTipoPrestador() {

        return tipoPrestador;
    }

    public void setTipoPrestador(String tipoPrestador) {

        this.tipoPrestador = tipoPrestador;
    }

    public String getDesTipoPrestador() {

        return desTipoPrestador;
    }

    public void setDesTipoPrestador(String desTipoPrestador) {

        this.desTipoPrestador = desTipoPrestador;
    }

    public String getConsultorio() {

        return consultorio;
    }

    public void setConsultorio(String consultorio) {

        this.consultorio = consultorio;
    }

    public String getHorasTrabajadas() {

        return horasTrabajadas;
    }

    public void setHorasTrabajadas(String horasTrabajadas) {

        this.horasTrabajadas = horasTrabajadas;
    }

    public String getTurno() {

        return turno;
    }

    public void setTurno(String turno) {

        this.turno = turno;
    }

    public String getDesTurno() {

        return desTurno;
    }

    public void setDesTurno(String desTurno) {

        this.desTurno = desTurno;
    }

    public Integer getConsultasNoOtorgadas() {

        return consultasNoOtorgadas;
    }

    public void setConsultasNoOtorgadas(Integer consultasNoOtorgadas) {

        this.consultasNoOtorgadas = consultasNoOtorgadas;
    }

    public Integer getCitasNoCumplidas() {

        return citasNoCumplidas;
    }

    public void setCitasNoCumplidas(Integer citasNoCumplidas) {

        this.citasNoCumplidas = citasNoCumplidas;
    }

    public String getNss() {

        return nss;
    }

    public void setNss(String nss) {

        this.nss = nss;
    }

    public String getAgregadoMedico() {

        return agregadoMedico;
    }

    public void setAgregadoMedico(String agregadoMedico) {

        this.agregadoMedico = agregadoMedico;
    }

    public String getNombre() {

        return nombre;
    }

    public void setNombre(String nombre) {

        this.nombre = nombre;
    }

    public String getApePaterno() {

        return apePaterno;
    }

    public void setApePaterno(String apePaterno) {

        this.apePaterno = apePaterno;
    }

    public String getApeMaterno() {

        return apeMaterno;
    }

    public void setApeMaterno(String apeMaterno) {

        this.apeMaterno = apeMaterno;
    }

    public String getUnidadAdscripcion() {

        return unidadAdscripcion;
    }

    public void setUnidadAdscripcion(String unidadAdscripcion) {

        this.unidadAdscripcion = unidadAdscripcion;
    }

    public String getNombreUnidadMed() {

        return nombreUnidadMed;
    }

    public void setNombreUnidadMed(String nombreUnidadMed) {

        this.nombreUnidadMed = nombreUnidadMed;
    }

    public String getHoraCita() {

        return horaCita;
    }

    public void setHoraCita(String horaCita) {

        this.horaCita = horaCita;
    }

    public Integer getPrimeraVez() {

        return primeraVez;
    }

    public void setPrimeraVez(Integer primeraVez) {

        this.primeraVez = primeraVez;
    }

    public Integer getCitado() {

        return citado;
    }

    public void setCitado(Integer citado) {

        this.citado = citado;
    }

    public Integer getPase() {

        return pase;
    }

    public void setPase(Integer pase) {

        this.pase = pase;
    }

    public Integer getRecetas() {

        return recetas;
    }

    public void setRecetas(Integer recetas) {

        this.recetas = recetas;
    }

    public Integer getDiasIncapacidad() {

        return diasIncapacidad;
    }

    public void setDiasIncapacidad(Integer diasIncapacidad) {

        this.diasIncapacidad = diasIncapacidad;
    }

    public Integer getVisitas() {

        return visitas;
    }

    public void setVisitas(Integer visitas) {

        this.visitas = visitas;
    }

    public Integer getMetodoPpf() {

        return metodoPpf;
    }

    public void setMetodoPpf(Integer metodoPpf) {

        this.metodoPpf = metodoPpf;
    }

    public Integer getCantidadMpf() {

        return cantidadMpf;
    }

    public void setCantidadMpf(Integer cantidadMpf) {

        this.cantidadMpf = cantidadMpf;
    }

    public Integer getAccidentesLesiones() {

        return accidentesLesiones;
    }

    public void setAccidentesLesiones(Integer accidentesLesiones) {

        this.accidentesLesiones = accidentesLesiones;
    }

    public Integer getRiesgosTrabajo() {

        return riesgosTrabajo;
    }

    public void setRiesgosTrabajo(Integer riesgosTrabajo) {

        this.riesgosTrabajo = riesgosTrabajo;
    }

    public Integer getActiPerParamedico() {

        return actiPerParamedico;
    }

    public void setActiPerParamedico(Integer actiPerParamedico) {

        this.actiPerParamedico = actiPerParamedico;
    }

    public Integer getTipoDocumento() {

        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {

        this.tipoDocumento = tipoDocumento;
    }

    public String getCodigoCie() {

        return codigoCie;
    }

    public void setCodigoCie(String codigoCie) {

        this.codigoCie = codigoCie;
    }

    public Integer getCveTipoUrgencia() {

        return cveTipoUrgencia;
    }

    public void setCveTipoUrgencia(Integer cveTipoUrgencia) {

        this.cveTipoUrgencia = cveTipoUrgencia;
    }

    public String getDiagnosticoPrincipal() {

        return diagnosticoPrincipal;
    }

    public void setDiagnosticoPrincipal(String diagnosticoPrincipal) {

        this.diagnosticoPrincipal = diagnosticoPrincipal;
    }

    public String getDesDiagPrin() {

        return desDiagPrin;
    }

    public void setDesDiagPrin(String desDiagPrin) {

        this.desDiagPrin = desDiagPrin;
    }

    public String getCveInforAdicPrin() {

        return cveInforAdicPrin;
    }

    public void setCveInforAdicPrin(String cveInforAdicPrin) {

        this.cveInforAdicPrin = cveInforAdicPrin;
    }

    public String getCveTipoPrin() {

        return cveTipoPrin;
    }

    public void setCveTipoPrin(String cveTipoPrin) {

        this.cveTipoPrin = cveTipoPrin;
    }

    public Date getFecCirugiaPrin() {

        return fecCirugiaPrin;
    }

    public void setFecCirugiaPrin(Date fecCirugiaPrin) {

        this.fecCirugiaPrin = fecCirugiaPrin;
    }

    public Integer getDiagAcc() {

        return diagAcc;
    }

    public void setDiagAcc(Integer diagAcc) {

        this.diagAcc = diagAcc;
    }

    public String getDiagnosticoAdicional() {

        return diagnosticoAdicional;
    }

    public void setDiagnosticoAdicional(String diagnosticoAdicional) {

        this.diagnosticoAdicional = diagnosticoAdicional;
    }

    public String getDesDiagAdic() {

        return desDiagAdic;
    }

    public void setDesDiagAdic(String desDiagAdic) {

        this.desDiagAdic = desDiagAdic;
    }

    public Integer getPrimeraVezDiagAdic() {

        return primeraVezDiagAdic;
    }

    public void setPrimeraVezDiagAdic(Integer primeraVezDiagAdic) {

        this.primeraVezDiagAdic = primeraVezDiagAdic;
    }

    public String getCveInforAdicAdic() {

        return cveInforAdicAdic;
    }

    public void setCveInforAdicAdic(String cveInforAdicAdic) {

        this.cveInforAdicAdic = cveInforAdicAdic;
    }

    public String getCveTipoAdic() {

        return cveTipoAdic;
    }

    public void setCveTipoAdic(String cveTipoAdic) {

        this.cveTipoAdic = cveTipoAdic;
    }

    public Date getFecCirugiaAdic() {

        return fecCirugiaAdic;
    }

    public void setFecCirugiaAdic(Date fecCirugiaAdic) {

        this.fecCirugiaAdic = fecCirugiaAdic;
    }

    public String getDiagnosticoComplemento() {

        return diagnosticoComplemento;
    }

    public void setDiagnosticoComplemento(String diagnosticoComplemento) {

        this.diagnosticoComplemento = diagnosticoComplemento;
    }

    public String getDesDiagComp() {

        return desDiagComp;
    }

    public void setDesDiagComp(String desDiagComp) {

        this.desDiagComp = desDiagComp;
    }

    public Integer getPrimeraVezDiagComp() {

        return primeraVezDiagComp;
    }

    public void setPrimeraVezDiagComp(Integer primeraVezDiagComp) {

        this.primeraVezDiagComp = primeraVezDiagComp;
    }

    public String getCveInforAdicComp() {

        return cveInforAdicComp;
    }

    public void setCveInforAdicComp(String cveInforAdicComp) {

        this.cveInforAdicComp = cveInforAdicComp;
    }

    public String getCveTipoComp() {

        return cveTipoComp;
    }

    public void setCveTipoComp(String cveTipoComp) {

        this.cveTipoComp = cveTipoComp;
    }

    public Date getFecCirugiaComp() {

        return fecCirugiaComp;
    }

    public void setFecCirugiaComp(Date fecCirugiaComp) {

        this.fecCirugiaComp = fecCirugiaComp;
    }

    public String getProcedimientoPrincipal() {

        return procedimientoPrincipal;
    }

    public void setProcedimientoPrincipal(String procedimientoPrincipal) {

        this.procedimientoPrincipal = procedimientoPrincipal;
    }

    public String getDesProcPrin() {

        return desProcPrin;
    }

    public void setDesProcPrin(String desProcPrin) {

        this.desProcPrin = desProcPrin;
    }

    public String getProcedimientoAdicional() {

        return procedimientoAdicional;
    }

    public void setProcedimientoAdicional(String procedimientoAdicional) {

        this.procedimientoAdicional = procedimientoAdicional;
    }

    public String getDesProcAdic() {

        return desProcAdic;
    }

    public void setDesProcAdic(String desProcAdic) {

        this.desProcAdic = desProcAdic;
    }

    public String getProcedimientoComplemento() {

        return procedimientoComplemento;
    }

    public void setProcedimientoComplemento(String procedimientoComplemento) {

        this.procedimientoComplemento = procedimientoComplemento;
    }

    public String getDesProcComp() {

        return desProcComp;
    }

    public void setDesProcComp(String desProcComp) {

        this.desProcComp = desProcComp;
    }

    public Date getFecCaptura() {

        return fecCaptura;
    }

    public void setFecCaptura(Date fecCaptura) {

        this.fecCaptura = fecCaptura;
    }

    public String getMorfologia() {

        return morfologia;
    }

    public void setMorfologia(String morfologia) {

        this.morfologia = morfologia;
    }

    public Boolean getEditable() {

        return editable;
    }

    public void setEditable(Boolean editable) {

        this.editable = editable;
    }

    public Boolean getConsultar() {

        return consultar;
    }

    public void setConsultar(Boolean consultar) {

        this.consultar = consultar;
    }

    public int getCveRamaAseguramiento() {

        return cveRamaAseguramiento;
    }

    public void setCveRamaAseguramiento(int cveRamaAseguramiento) {

        this.cveRamaAseguramiento = cveRamaAseguramiento;
    }

    public String getDescRamaAseguramiento() {

        return descRamaAseguramiento;
    }

    public void setDescRamaAseguramiento(String descRamaAseguramiento) {

        this.descRamaAseguramiento = descRamaAseguramiento;
    }

    public Integer getCveCD4Principal() {

        return cveCD4Principal;
    }

    public void setCveCD4Principal(Integer cveCD4Principal) {

        this.cveCD4Principal = cveCD4Principal;
    }

    public Integer getCveCD4Adicional() {

        return cveCD4Adicional;
    }

    public void setCveCD4Adicional(Integer cveCD4Adicional) {

        this.cveCD4Adicional = cveCD4Adicional;
    }

    public Integer getCveCD4Compl() {

        return cveCD4Compl;
    }

    public void setCveCD4Compl(Integer cveCD4Compl) {

        this.cveCD4Compl = cveCD4Compl;
    }

    public Integer getNumTipoServicio() {

        return numTipoServicio;
    }

    public void setNumTipoServicio(Integer numTipoServicio) {

        this.numTipoServicio = numTipoServicio;
    }

    public Integer getCveTipoEmbarazo() {

        return cveTipoEmbarazo;
    }

    public void setCveTipoEmbarazo(Integer cveTipoEmbarazo) {

        this.cveTipoEmbarazo = cveTipoEmbarazo;
    }

    public Integer getCveTipoEmbarazoAdicional() {

        return cveTipoEmbarazoAdicional;
    }

    public void setCveTipoEmbarazoAdicional(Integer cveTipoEmbarazoAdicional) {

        this.cveTipoEmbarazoAdicional = cveTipoEmbarazoAdicional;
    }

    public Integer getCveTipoEmbarazoComplementario() {

        return cveTipoEmbarazoComplementario;
    }

    public void setCveTipoEmbarazoComplementario(Integer cveTipoEmbarazoComplementario) {

        this.cveTipoEmbarazoComplementario = cveTipoEmbarazoComplementario;
    }

    public String getCveCurp() {

        return cveCurp;
    }

    public void setCveCurp(String cveCurp) {

        this.cveCurp = cveCurp;
    }

    public Integer getIndVigente() {

        return indVigente;
    }

    public void setIndVigente(Integer indVigente) {

        this.indVigente = indVigente;
    }

    public Integer getIndPacEncontrado() {

        return indPacEncontrado;
    }

    public void setIndPacEncontrado(Integer indPacEncontrado) {

        this.indPacEncontrado = indPacEncontrado;
    }

    public Date getStpUltConsultaVig() {

        return stpUltConsultaVig;
    }

    public void setStpUltConsultaVig(Date stpUltConsultaVig) {

        this.stpUltConsultaVig = stpUltConsultaVig;
    }

    public String getCveIdee() {

        return cveIdee;
    }

    public void setCveIdee(String cveIdee) {

        this.cveIdee = cveIdee;
    }

    public Integer getCveTipoInfoAdDiagPrin() {

        return cveTipoInfoAdDiagPrin;
    }

    public void setCveTipoInfoAdDiagPrin(Integer cveTipoInfoAdDiagPrin) {

        this.cveTipoInfoAdDiagPrin = cveTipoInfoAdDiagPrin;
    }

    public Integer getCveTipoInfoAdDiagAdic() {

        return cveTipoInfoAdDiagAdic;
    }

    public void setCveTipoInfoAdDiagAdic(Integer cveTipoInfoAdDiagAdic) {

        this.cveTipoInfoAdDiagAdic = cveTipoInfoAdDiagAdic;
    }

    public Integer getCveTipoInfoAdDiagCompl() {

        return cveTipoInfoAdDiagCompl;
    }

    public void setCveTipoInfoAdDiagCompl(Integer cveTipoInfoAdDiagCompl) {

        this.cveTipoInfoAdDiagCompl = cveTipoInfoAdDiagCompl;
    }

	public Date getStpActualiza() {
		return stpActualiza;
	}

	public void setStpActualiza(Date stpActualiza) {
		this.stpActualiza = stpActualiza;
	}

	public Date getStpCaptura() {
		return stpCaptura;
	}

	public void setStpCaptura(Date stpCaptura) {
		this.stpCaptura = stpCaptura;
	}

	public List<SdEncabezado> getSdEncabezado() {
		return sdEncabezado;
	}

	public void setSdEncabezado(List<SdEncabezado> sdEncabezado) {
		this.sdEncabezado = sdEncabezado;
	}

	public List<SdUnidadMedica> getSdUnidadMedica() {
		return sdUnidadMedica;
	}

	public void setSdUnidadMedica(List<SdUnidadMedica> sdUnidadMedica) {
		this.sdUnidadMedica = sdUnidadMedica;
	}
	
	
   
}
