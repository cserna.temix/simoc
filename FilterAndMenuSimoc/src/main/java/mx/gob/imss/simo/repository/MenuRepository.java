package mx.gob.imss.simo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import mx.gob.imss.simo.model.Menu;

public interface MenuRepository extends MongoRepository<Menu, ObjectId>, PagingAndSortingRepository<Menu, ObjectId> {

    List<Menu> findByCveRolOrderByCveMenuAplicativoAsc(String role);
}
