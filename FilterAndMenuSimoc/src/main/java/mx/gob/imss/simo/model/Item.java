package mx.gob.imss.simo.model;

public class Item {

    private String cveMenuAplicativo;
    private String desMenuAplicativo;
    private String refAccion;
    private String aplicativo;

    public String getCveMenuAplicativo() {

        return cveMenuAplicativo;
    }

    public void setCveMenuAplicativo(String cveMenuAplicativo) {

        this.cveMenuAplicativo = cveMenuAplicativo;
    }

    public String getDesMenuAplicativo() {

        return desMenuAplicativo;
    }

    public void setDesMenuAplicativo(String desMenuAplicativo) {

        this.desMenuAplicativo = desMenuAplicativo;
    }

    public String getRefAccion() {

        return refAccion;
    }

    public void setRefAccion(String refAccion) {

        this.refAccion = refAccion;
    }

    public String getAplicativo() {

        return aplicativo;
    }

    public void setAplicativo(String aplicativo) {

        this.aplicativo = aplicativo;
    }

    @Override
    public String toString() {

        return "Item [cveMenuAplicativo=" + cveMenuAplicativo + ", desMenuAplicativo=" + desMenuAplicativo
                + ", refAccion=" + refAccion + ", aplicativo=" + aplicativo + "]";
    }
}
