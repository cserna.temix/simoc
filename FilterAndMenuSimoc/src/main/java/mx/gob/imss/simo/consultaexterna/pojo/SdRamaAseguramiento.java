/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdRamaAseguramiento {

	private Integer cveRamaAseguramiento;
	private String desRamaAseguramiento;

	public SdRamaAseguramiento() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SdRamaAseguramiento(Integer cveRamaAseguramiento, String desRamaAseguramiento) {
		super();
		this.cveRamaAseguramiento = cveRamaAseguramiento;
		this.desRamaAseguramiento = desRamaAseguramiento;
	}

	/**
	 * @return the cveRamaAseguramiento
	 */
	public Integer getCveRamaAseguramiento() {
		return cveRamaAseguramiento;
	}

	/**
	 * @param cveRamaAseguramiento
	 *            the cveRamaAseguramiento to set
	 */
	public void setCveRamaAseguramiento(Integer cveRamaAseguramiento) {
		this.cveRamaAseguramiento = cveRamaAseguramiento;
	}

	/**
	 * @return the desRamaAseguramiento
	 */
	public String getDesRamaAseguramiento() {
		return desRamaAseguramiento;
	}

	/**
	 * @param desRamaAseguramiento
	 *            the desRamaAseguramiento to set
	 */
	public void setDesRamaAseguramiento(String desRamaAseguramiento) {
		this.desRamaAseguramiento = desRamaAseguramiento;
	}

}
