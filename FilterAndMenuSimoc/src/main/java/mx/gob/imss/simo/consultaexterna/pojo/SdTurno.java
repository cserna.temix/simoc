/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdTurno {

	private Integer cveTurno;
	private String desTurno;

	/**
	 * @return the cveTurno
	 */
	public Integer getCveTurno() {
		return cveTurno;
	}

	/**
	 * @param cveTurno
	 *            the cveTurno to set
	 */
	public void setCveTurno(Integer cveTurno) {
		this.cveTurno = cveTurno;
	}

	/**
	 * @return the desTurno
	 */
	public String getDesTurno() {
		return desTurno;
	}

	/**
	 * @param desTurno
	 *            the desTurno to set
	 */
	public void setDesTurno(String desTurno) {
		this.desTurno = desTurno;
	}

}
