/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdEspecialidades {

	private Integer numTipoServicio;
	private String cveEspecialidad;
	private String desEspecialidad;

	
	/**
	 * @return the numTipoServicio
	 */
	public Integer getNumTipoServicio() {
		return numTipoServicio;
	}

	/**
	 * @param numTipoServicio the numTipoServicio to set
	 */
	public void setNumTipoServicio(Integer numTipoServicio) {
		this.numTipoServicio = numTipoServicio;
	}

	/**
	 * @return the cveEspecialidad
	 */
	public String getCveEspecialidad() {
		return cveEspecialidad;
	}

	/**
	 * @param cveEspecialidad
	 *            the cveEspecialidad to set
	 */
	public void setCveEspecialidad(String cveEspecialidad) {
		this.cveEspecialidad = cveEspecialidad;
	}

	/**
	 * @return the desEspecialidad
	 */
	public String getDesEspecialidad() {
		return desEspecialidad;
	}

	/**
	 * @param desEspecialidad
	 *            the desEspecialidad to set
	 */
	public void setDesEspecialidad(String desEspecialidad) {
		this.desEspecialidad = desEspecialidad;
	}

}
