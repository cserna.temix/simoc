/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdCapturista {

	private String refApellidoPaterno;
	private String refApellidoMaterno;
	private String refNombre;
	
	
	public SdCapturista() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SdCapturista(String refApellidoPaterno, String refApellidoMaterno,
			String refNombre) {
		super();
		this.refApellidoPaterno = refApellidoPaterno;
		this.refApellidoMaterno = refApellidoMaterno;
		this.refNombre = refNombre;
	}
	/**
	 * @return the refApellidoPaterno
	 */
	public String getRefApellidoPaterno() {
		return refApellidoPaterno;
	}
	/**
	 * @param refApellidoPaterno the refApellidoPaterno to set
	 */
	public void setRefApellidoPaterno(String refApellidoPaterno) {
		this.refApellidoPaterno = refApellidoPaterno;
	}
	/**
	 * @return the refApellidoMaterno
	 */
	public String getRefApellidoMaterno() {
		return refApellidoMaterno;
	}
	/**
	 * @param refApellidoMaterno the refApellidoMaterno to set
	 */
	public void setRefApellidoMaterno(String refApellidoMaterno) {
		this.refApellidoMaterno = refApellidoMaterno;
	}
	/**
	 * @return the refNombre
	 */
	public String getRefNombre() {
		return refNombre;
	}
	/**
	 * @param refNombre the refNombre to set
	 */
	public void setRefNombre(String refNombre) {
		this.refNombre = refNombre;
	}

	
}
