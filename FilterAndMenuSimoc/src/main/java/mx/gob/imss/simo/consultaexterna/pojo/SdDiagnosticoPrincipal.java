/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author francisco.rrios
 * 
 */
public class SdDiagnosticoPrincipal {

	private String cveCie10;
	private String desCie10;
	private List<SdInformacionAdicional> sdInformacionAdicional = new ArrayList<SdInformacionAdicional>();
	private List<SdTipoDiarrea> sdTipoDiarrea = new ArrayList<SdTipoDiarrea>();
	private Date fecCirugia;
	private Integer ind1EraVez;

	
	public SdDiagnosticoPrincipal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SdDiagnosticoPrincipal(String cveCie10, String desCie10) {
		super();
		this.cveCie10 = cveCie10;
		this.desCie10 = desCie10;
	}

	/**
	 * @return the cveCie10
	 */
	public String getCveCie10() {
		return cveCie10;
	}

	/**
	 * @param cveCie10
	 *            the cveCie10 to set
	 */
	public void setCveCie10(String cveCie10) {
		this.cveCie10 = cveCie10;
	}

	/**
	 * @return the desCie10
	 */
	public String getDesCie10() {
		return desCie10;
	}

	/**
	 * @param desCie10
	 *            the desCie10 to set
	 */
	public void setDesCie10(String desCie10) {
		this.desCie10 = desCie10;
	}

	/**
	 * @return the sdInformacionAdicional
	 */
	public List<SdInformacionAdicional> getSdInformacionAdicional() {
		return sdInformacionAdicional;
	}

	/**
	 * @param sdInformacionAdicional
	 *            the sdInformacionAdicional to set
	 */
	public void setSdInformacionAdicional(
			List<SdInformacionAdicional> sdInformacionAdicional) {
		this.sdInformacionAdicional = sdInformacionAdicional;
	}


	/**
	 * @return the fecCirugia
	 */
	public Date getFecCirugia() {
		return fecCirugia;
	}

	/**
	 * @param fecCirugia
	 *            the fecCirugia to set
	 */
	public void setFecCirugia(Date fecCirugia) {
		this.fecCirugia = fecCirugia;
	}

	/**
	 * @return the ind1EraVez
	 */
	public Integer getInd1EraVez() {
		return ind1EraVez;
	}

	/**
	 * @param ind1EraVez
	 *            the ind1EraVez to set
	 */
	public void setInd1EraVez(Integer ind1EraVez) {
		this.ind1EraVez = ind1EraVez;
	}

	public List<SdTipoDiarrea> getSdTipoDiarrea() {
		return sdTipoDiarrea;
	}

	public void setSdTipoDiarrea(List<SdTipoDiarrea> sdTipoDiarrea) {
		this.sdTipoDiarrea = sdTipoDiarrea;
	}

}
