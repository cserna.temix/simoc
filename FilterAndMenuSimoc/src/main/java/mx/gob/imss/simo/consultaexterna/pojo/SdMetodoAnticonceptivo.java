/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdMetodoAnticonceptivo {

	private Integer cveMetodoAnticonceptivo;
	private String desMetodoAnticonceptivo;
	private Integer canMetodoAnticonceptivo;

	
	public SdMetodoAnticonceptivo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SdMetodoAnticonceptivo(Integer cveMetodoAnticonceptivo,
			String desMetodoAnticonceptivo) {
		super();
		this.cveMetodoAnticonceptivo = cveMetodoAnticonceptivo;
		this.desMetodoAnticonceptivo = desMetodoAnticonceptivo;
	}

	/**
	 * @return the cveMetodoAnticonceptivo
	 */
	public Integer getCveMetodoAnticonceptivo() {
		return cveMetodoAnticonceptivo;
	}

	/**
	 * @param cveMetodoAnticonceptivo
	 *            the cveMetodoAnticonceptivo to set
	 */
	public void setCveMetodoAnticonceptivo(Integer cveMetodoAnticonceptivo) {
		this.cveMetodoAnticonceptivo = cveMetodoAnticonceptivo;
	}

	/**
	 * @return the desMetodoAnticonceptivo
	 */
	public String getDesMetodoAnticonceptivo() {
		return desMetodoAnticonceptivo;
	}

	/**
	 * @param desMetodoAnticonceptivo
	 *            the desMetodoAnticonceptivo to set
	 */
	public void setDesMetodoAnticonceptivo(String desMetodoAnticonceptivo) {
		this.desMetodoAnticonceptivo = desMetodoAnticonceptivo;
	}

	/**
	 * @return the canMetodoAnticonceptivo
	 */
	public Integer getCanMetodoAnticonceptivo() {
		return canMetodoAnticonceptivo;
	}

	/**
	 * @param canMetodoAnticonceptivo
	 *            the canMetodoAnticonceptivo to set
	 */
	public void setCanMetodoAnticonceptivo(Integer canMetodoAnticonceptivo) {
		this.canMetodoAnticonceptivo = canMetodoAnticonceptivo;
	}

}
