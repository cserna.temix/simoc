package mx.gob.imss.simo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DatosUsuario implements Serializable {

    private static final long serialVersionUID = -2643128014715888190L;
    private String usuario;
    private String nombre;
    private String appPaterno;
    private String appMaterno;
    private List<String> roles;
    private String perfil;
    private Integer cvePerfil;
    private String cvePresupuestal;
    private String cvePresupuestalAux;

    public String getCvePresupuestalAux() {

        return cvePresupuestalAux;
    }

    public void setCvePresupuestalAux(String cvePresupuestalAux) {

        this.cvePresupuestalAux = cvePresupuestalAux;
    }

    private String cveDelegacionUmae;
    private String desUnidadMedicaAct;
    private String desDelegacionAct;

    public DatosUsuario() {

        roles = new ArrayList<>();
    }

    public Boolean tieneRole(String cveRole) {

        return roles.contains(cveRole);
    }

    public String getUsuario() {

        return usuario;
    }

    public void setUsuario(String usuario) {

        this.usuario = usuario;
    }

    public String getPerfil() {

        return perfil;
    }

    public void setPerfil(String perfil) {

        this.perfil = perfil;
    }

    public Integer getCvePerfil() {

        return cvePerfil;
    }

    public void setCvePerfil(Integer cvePerfil) {

        this.cvePerfil = cvePerfil;
    }

    public String getCvePresupuestal() {

        return cvePresupuestal;
    }

    public void setCvePresupuestal(String cvePresupuestal) {

        this.cvePresupuestal = cvePresupuestal;
    }

    public String getCveDelegacionUmae() {

        return cveDelegacionUmae;
    }

    public void setCveDelegacionUmae(String cveDelegacionUmae) {

        this.cveDelegacionUmae = cveDelegacionUmae;
    }

    public String getNombre() {

        return nombre;
    }

    public void setNombre(String nombre) {

        this.nombre = nombre;
    }

    public String getAppPaterno() {

        return appPaterno;
    }

    public void setAppPaterno(String appPaterno) {

        this.appPaterno = appPaterno;
    }

    public String getAppMaterno() {

        return appMaterno;
    }

    public void setAppMaterno(String appMaterno) {

        this.appMaterno = appMaterno;
    }

    public List<String> getRoles() {

        return roles;
    }

    public void setRoles(List<String> roles) {

        this.roles = roles;
    }

    public String getDesUnidadMedicaAct() {

        return desUnidadMedicaAct;
    }

    public void setDesUnidadMedicaAct(String desUnidadMedicaAct) {

        this.desUnidadMedicaAct = desUnidadMedicaAct;
    }

    public String getDesDelegacionAct() {

        return desDelegacionAct;
    }

    public void setDesDelegacionAct(String desDelegacionAct) {

        this.desDelegacionAct = desDelegacionAct;
    }

}
