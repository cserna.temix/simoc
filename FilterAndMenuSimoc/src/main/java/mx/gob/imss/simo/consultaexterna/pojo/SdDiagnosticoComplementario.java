/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author francisco.rrios
 * 
 */
public class SdDiagnosticoComplementario {

	private String cveCie10;
	private String desCie10;
	private Integer ind1EraVez;
	private List<SdInformacionAdicional> sdInformacionAdicional = new ArrayList<SdInformacionAdicional>();
	private List<SdTipoDiarrea> SdTipoDiarrea = new ArrayList<SdTipoDiarrea>();
	private List<SdAccidentesLesiones> sdAccidentesLesiones = new ArrayList<SdAccidentesLesiones>();
	private Date fecCirugia;

	public SdDiagnosticoComplementario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SdDiagnosticoComplementario(String cveCie10, String desCie10,
			Integer ind1EraVez) {
		super();
		this.cveCie10 = cveCie10;
		this.desCie10 = desCie10;
		this.ind1EraVez = ind1EraVez;
	}

	/**
	 * @return the cveCie10
	 */
	public String getCveCie10() {
		return cveCie10;
	}

	/**
	 * @param cveCie10
	 *            the cveCie10 to set
	 */
	public void setCveCie10(String cveCie10) {
		this.cveCie10 = cveCie10;
	}

	/**
	 * @return the desCie10
	 */
	public String getDesCie10() {
		return desCie10;
	}

	/**
	 * @param desCie10
	 *            the desCie10 to set
	 */
	public void setDesCie10(String desCie10) {
		this.desCie10 = desCie10;
	}

	/**
	 * @return the ind1EraVez
	 */
	public Integer getInd1EraVez() {
		return ind1EraVez;
	}

	/**
	 * @param ind1EraVez
	 *            the ind1EraVez to set
	 */
	public void setInd1EraVez(Integer ind1EraVez) {
		this.ind1EraVez = ind1EraVez;
	}

	/**
	 * @return the sdInformacionAdicional
	 */
	public List<SdInformacionAdicional> getSdInformacionAdicional() {
		return sdInformacionAdicional;
	}

	/**
	 * @param sdInformacionAdicional
	 *            the sdInformacionAdicional to set
	 */
	public void setSdInformacionAdicional(
			List<SdInformacionAdicional> sdInformacionAdicional) {
		this.sdInformacionAdicional = sdInformacionAdicional;
	}

	/**
	 * @return the SdTipoDiarrea
	 */
	public List<SdTipoDiarrea> getSdTipoDiarrea() {
		return SdTipoDiarrea;
	}

	/**
	 * @param SdTipoDiarrea
	 *            the SdTipoDiarrea to set
	 */
	public void setSdTipoDiarrea(List<SdTipoDiarrea> SdTipoDiarrea) {
		this.SdTipoDiarrea = SdTipoDiarrea;
	}

	/**
	 * @return the fecCirugia
	 */
	public Date getFecCirugia() {
		return fecCirugia;
	}

	/**
	 * @param fecCirugia
	 *            the fecCirugia to set
	 */
	public void setFecCirugia(Date fecCirugia) {
		this.fecCirugia = fecCirugia;
	}

	/**
	 * @return the sdAccidentesLesiones
	 */
	public List<SdAccidentesLesiones> getSdAccidentesLesiones() {
		return sdAccidentesLesiones;
	}

	/**
	 * @param sdAccidentesLesiones
	 *            the sdAccidentesLesiones to set
	 */
	public void setSdAccidentesLesiones(
			List<SdAccidentesLesiones> sdAccidentesLesiones) {
		this.sdAccidentesLesiones = sdAccidentesLesiones;
	}

}
