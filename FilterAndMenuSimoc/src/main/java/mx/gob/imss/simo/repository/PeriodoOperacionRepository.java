package mx.gob.imss.simo.repository;

import java.sql.SQLException;
import java.util.Date;

import mx.gob.imss.simo.model.PeriodoOperacion;

public interface PeriodoOperacionRepository {

    PeriodoOperacion getPeriodoActual(String cvePresupuestal, Integer tipoCaptura) throws SQLException;
    Date obtenerUltimaFechaCaptura(String cvePresupuestal, Integer tipoCaptura) throws Exception;
    public PeriodoOperacion getPeriodoMonitoreo(String cvePresupuestal, Integer tipoCaptura) throws SQLException;
}
