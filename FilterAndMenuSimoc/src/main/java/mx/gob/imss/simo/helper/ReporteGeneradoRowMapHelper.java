package mx.gob.imss.simo.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

public class ReporteGeneradoRowMapHelper implements RowMapper<Date>{
	
    public static final String FEC_GENERACION = "FEC_GENERACION";
	
    public Date mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        Date ultimaFechaCaptura = new Date();
        ultimaFechaCaptura = resultSet.getDate(FEC_GENERACION);
        return ultimaFechaCaptura;
    }
}
