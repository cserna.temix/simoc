package mx.gob.imss.simo.controller;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.model.DatosSesion;
import mx.gob.imss.simo.model.DelegacionIMSS;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.model.PersonalOperativo;
import mx.gob.imss.simo.model.Rol;
import mx.gob.imss.simo.model.UnidadMedica;
import mx.gob.imss.simo.service.CatalogoService;
import mx.gob.imss.simo.service.DatosSesionService;
import mx.gob.imss.simo.service.UserService;

@Component(value = "portadaController")
@Scope("session")
public class PortadaController implements Serializable {

    private static final long serialVersionUID = 6279916692954857928L;

    private static boolean renderIcons;

    public static final String LOGOUT_SISTEMA = "/logout";

    static final Logger logger = Logger.getLogger(PortadaController.class);

    @Value("${url.ce.servlet}")
    private String AMBIENTE_CE;

    @Value("${url.hosp.servlet}")
    private String AMBIENTE_HOSP;

    @Value("${url.monitoreo.servlet}")
    private String AMBIENTE_MONITOREO;

    @Value("${url.enlaces.servlet}")
    private String AMBIENTE_ENALCES;

    @Value("${url.aglogout}")
    private String ACCESS_GATEWAY_LOGOUT;

    @Autowired
    private transient UserService userService;

    @Autowired
    private transient CatalogoService catalogoService;

    @Autowired
    ObjetosEnSesionBean objetosSs;

    private transient PersonalOperativo usuario;

    @Autowired
    transient DatosSesionService datosSesionService;

    private transient List<DelegacionIMSS> delegaciones;
    private transient List<UnidadMedica> unidadesMedicas;
    
    private transient List<DelegacionIMSS> delegacionesUnidades;
    private transient List<UnidadMedica> unidadesMedicasActivas;
    
    private transient DelegacionIMSS delegacionSelected = new DelegacionIMSS();
    private transient UnidadMedica unidadSelected = new UnidadMedica();

    private transient AutoComplete delegacionAcPf;
    private transient AutoComplete unidadMedicaAcPf;

    private Boolean menuRendered = Boolean.FALSE;
    private Boolean btnAceptarDisabled = Boolean.TRUE;
    private Boolean opcionesUsuarioDisabled = Boolean.TRUE;
    private Boolean btnCancelarRendered = Boolean.FALSE;
    private boolean delegacionDisabled;
    private UnidadMedica unidadMedTitular;

    public void mostrarDialogo() {

                setRenderIcons(Boolean.TRUE);

       
        if (getObjetosSs().getMostrarDialogoDelegaciones() == Boolean.TRUE) {
            cargaDatosUsuario();
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('delegacionUnidadDialog').show();");
        }
    }

    public void mostrarDialogoIcono() {

        getObjetosSs().setMostrarDialogoDelegaciones(Boolean.TRUE);
        mostrarDialogo();
    }

    public void cargaDatosUsuario() {

        delegaciones = new ArrayList<>();
        delegacionesUnidades = new ArrayList<>();
        unidadesMedicas = new ArrayList<>();
        unidadesMedicasActivas = new ArrayList<>();
        delegacionAcPf = new AutoComplete();
        unidadMedTitular = new UnidadMedica();
//        setDelegacionDisabled(Boolean.TRUE);

        unidadMedicaAcPf = new AutoComplete();
        delegaciones = catalogoService.obtenerDelegacionesIMSS();

       
        // Consulta usuario por username y obtener las unidades medicas(PERSONAL OPERATIVO)
          
//        logger.info("objetosSs:"+objetosSs.getUsername());
//        logger.info("objetosSs:"+objetosSs.getUserInfo());
        usuario = userService.getUsuario(objetosSs.getUsername());
       
        unidadesMedicas = usuario.getSdUnidadMedica();

//        if (unidadesMedicas.size() > 1) {
//            setOpcionesUsuarioDisabled(Boolean.FALSE);
//        }

//        for (UnidadMedica unidadMed : unidadesMedicas) {
//            if (unidadMed.getIndTitular() == 1) {
//                // Se asigna por default la unidad donde es titular
//                unidadSelected = unidadMed;
//            }
//        }
//        
//
//
//        delegacionSelected = findDelegacion(delegaciones, unidadSelected.getCveDelegacion());
//
//        for (Rol auth : usuario.getSdRol()) {
//            // Se agregan los todos los roles
//            objetosSs.getDatosUsuario().getRoles().add(auth.getCveRol());
//            //
//            if ("ROLE_ADMIN_CENTRAL".equals(auth.getCveRol())) {
//                /**
//                 * Si el usuario es ADMINISTRDOR CENTRAL se muestran todas las
//                 * delegaciones y todas unidades medicas
//                 **/
//                setOpcionesUsuarioDisabled(Boolean.FALSE);
//                setDelegacionDisabled(Boolean.FALSE);
//            }
//
//            if ("ROLE_ADMIN_DELEG".equals(auth.getCveRol())) {
//                unidadesMedicas = catalogoService
//                        .obtenerUnidadesMedicasPorDelegacion(getDelegacionSelected().getCveDelegacionImss());
//                setOpcionesUsuarioDisabled(Boolean.FALSE);
//            }
//        }
        
        
        Set<DelegacionIMSS> delegacionesImss = new HashSet<DelegacionIMSS>();
        for (UnidadMedica unidadMed : usuario.getSdUnidadMedica()) {
        	if(unidadMed.getIndActivoUnidad() == 1){
        		getUnidadesMedicasActivas().add(unidadMed);
	            if (unidadMed.getIndTitular() == 1 && unidadMed.getIndActivoUnidad() == 1) {
	                // Se asigna por default la unidad donde es titular
	            	unidadMedTitular = unidadMed;
	                unidadSelected = unidadMed;
	                
	            }
	            if(!delegacionesImss.contains(unidadMed)){
	            	DelegacionIMSS delegacionImss =new DelegacionIMSS();
	            	delegacionImss.setCveDelegacionImss(unidadMed.getCveDelegacion());
	            	delegacionImss.setDesDelegacionImss(unidadMed.getDesDelegacion());
	            	delegacionesImss.add(delegacionImss);
	            }
        	}
        }
        if(unidadMedTitular.getIndTitular() != null  && unidadMedTitular.getIndTitular() == 1 && unidadMedTitular.getIndActivoUnidad() ==1){

			unidadesMedicas = getUnidadesMedicasActivas();
			if (unidadesMedicas.size() > 1) {
				setOpcionesUsuarioDisabled(Boolean.FALSE);
			}

			delegacionesUnidades = delegacionesImss.stream().collect(Collectors.toCollection(ArrayList::new));
			logger.info("delegacionesUnidades : " + delegacionesUnidades.size());
			logger.info("delegaciones : " + delegaciones.size());
			logger.info("cveDelegacionseleccionada : " + unidadSelected.getCveDelegacion());
			delegacionSelected = findDelegacion(delegaciones, unidadSelected.getCveDelegacion());

			for (Rol auth : usuario.getSdRol()) {
				// Se agregan los todos los roles
				objetosSs.getDatosUsuario().getRoles().add(auth.getCveRol());
				logger.info("rol : " + auth.getCveRol());
				//
				if ("ROLE_ADMIN_CENTRAL".equals(auth.getCveRol())) {
					/**
					 * Si el usuario es ADMINISTRDOR CENTRAL se muestran todas
					 * las delegaciones y todas unidades medicas
					 **/
					unidadesMedicas = catalogoService
							.obtenerUnidadesMedicasPorDelegacion(getDelegacionSelected().getCveDelegacionImss());
					setOpcionesUsuarioDisabled(Boolean.FALSE);
					setDelegacionDisabled(Boolean.FALSE);
				}

				if ("ROLE_ADMIN_DELEG".equals(auth.getCveRol())) {
					logger.info("rol captura y rol admin");
					delegaciones = delegacionesUnidades;
					delegacionSelected = findDelegacion(delegaciones, unidadSelected.getCveDelegacion());
					unidadesMedicas = catalogoService
							.obtenerUnidadesMedicasPorDelegacion(getDelegacionSelected().getCveDelegacionImss());
					setOpcionesUsuarioDisabled(Boolean.FALSE);
					setDelegacionDisabled(Boolean.FALSE);
				}

				if ("ROLE_ARIMAC_CAPTURA".equals(auth.getCveRol()) || "ROLE_ARIMAC_JEFE".equals(auth.getCveRol())) {
					logger.info("rol captura y rol jefe");
					delegaciones = delegacionesUnidades;
					delegacionSelected = findDelegacion(delegaciones, unidadSelected.getCveDelegacion());
					unidadesMedicas = obtenerUnidadesByDelegacionUsuario(
							getDelegacionSelected().getCveDelegacionImss());
					setOpcionesUsuarioDisabled(Boolean.FALSE);
					setDelegacionDisabled(Boolean.FALSE);
				}
			}
        }else{
        	unidadSelected =null;
        	unidadesMedicas = new ArrayList<>();
        	delegacionSelected = null;
        	delegaciones = new ArrayList<>();
		}
    }

    public void cargaUsuario() {

        DelegacionIMSS delegacion;
        UnidadMedica unidad;
        DatosSesion datosSesion;
        DatosSesion datosSesionActualizado;

        if (getUnidadSelected().getCvePresupuestal() != null
                && getDelegacionSelected().getCveDelegacionImss() != null) {

            setMenuRendered(Boolean.TRUE);

            // OBTENER DELEGACION
            delegacion = findDelegacion(delegaciones, getDelegacionSelected().getCveDelegacionImss());

            // OBTENER UNIDAD MEDICA
            unidad = findUnidadMedica(unidadesMedicas, getUnidadSelected().getCvePresupuestal());

            datosSesion = datosSesionService.getDatosSesion(objetosSs.getUserInfo(), objetosSs.getUsername());

            if (null == datosSesion) {
                datosSesion = new DatosSesion();
                datosSesion.setUsuario(objetosSs.getUsername());
                datosSesion.setRol(objetosSs.getRole());
                datosSesion.setNetIQSessionId(objetosSs.getUserInfo());
                datosSesion.setCveUnidadMedicaAct(null == unidad ? "" : unidad.getCveDelegacion());
                datosSesion.setDesUnidadMedicaAct(null == unidad ? "" : unidad.getDesUnidadMedica());
                datosSesion.setCveDelegacionAct(null == delegacion ? "" : delegacion.getCveDelegacionImss());
                datosSesion.setDesDelegacionAct(null == delegacion ? "" : delegacion.getDesDelegacionImss());
                datosSesion.setCvePresupuestalAct(null == unidad ? "" : unidad.getCvePresupuestal());
                datosSesion.setMostrarDialogoDelegacion(objetosSs.getMostrarDialogoDelegaciones());
                datosSesion.setFecAcceso(new Date());

                datosSesionActualizado = datosSesionService.save(datosSesion);
            } else {
                datosSesion.setCveUnidadMedicaAct(null == unidad ? "" : unidad.getCveDelegacion());
                datosSesion.setDesUnidadMedicaAct(null == unidad ? "" : unidad.getDesUnidadMedica());
                datosSesion.setCveDelegacionAct(null == delegacion ? "" : delegacion.getCveDelegacionImss());
                datosSesion.setDesDelegacionAct(null == delegacion ? "" : delegacion.getDesDelegacionImss());
                datosSesion.setCvePresupuestalAct(null == unidad ? "" : unidad.getCvePresupuestal());

                datosSesionActualizado = datosSesionService.save(datosSesion);
            }


            objetosSs.getDatosUsuario().setDesDelegacionAct(datosSesionActualizado.getDesDelegacionAct());
            objetosSs.getDatosUsuario().setDesUnidadMedicaAct(datosSesionActualizado.getDesUnidadMedicaAct());
            objetosSs.getDatosUsuario().setCvePresupuestal(datosSesionActualizado.getCvePresupuestalAct());
            objetosSs.getDatosUsuario().setCveDelegacionUmae(datosSesionActualizado.getCveDelegacionAct());
            objetosSs.getUsuarioDetalleDto().setDesDelegacionAct(datosSesionActualizado.getDesDelegacionAct());
            objetosSs.getUsuarioDetalleDto().setDesUnidadMedicaAct(datosSesionActualizado.getDesUnidadMedicaAct());
            objetosSs.getUsuarioDetalleDto().setCvePresupuestal(datosSesionActualizado.getCvePresupuestalAct());
            objetosSs.getUsuarioDetalleDto().setCveDelegacionUmae(datosSesionActualizado.getCveDelegacionAct());
            RequestContext context = RequestContext.getCurrentInstance();

            context.execute("PF('delegacionUnidadDialog').hide();");

        }
    }

    private DelegacionIMSS findDelegacion(List<DelegacionIMSS> delegaciones, String cveDelegacion) {

        return delegaciones.stream().filter(x -> cveDelegacion.equals(x.getCveDelegacionImss())).findFirst()
                .orElse(null);
    }

    private UnidadMedica findUnidadMedica(List<UnidadMedica> unidades, String cvePresupuestal) {

        return unidades.stream().filter(x -> cvePresupuestal.equals(x.getCvePresupuestal())).findFirst().orElse(null);
    }

    public List<DelegacionIMSS> completeDelegacion(String query) {

        List<DelegacionIMSS> allDelegaciones = getDelegaciones();
        List<DelegacionIMSS> filteredDelegaciones = new ArrayList<>();

        for (int i = 0; i < allDelegaciones.size(); i++) {
            DelegacionIMSS deleg = allDelegaciones.get(i);
            if (deleg.toString().toLowerCase().contains(query.toLowerCase())) {
                filteredDelegaciones.add(deleg);
            }
        }
        return filteredDelegaciones;
    }
    
    private List<UnidadMedica> obtenerUnidadesByDelegacionUsuario(String cveDelegacion){
    	logger.info("cveDelegacion :" + cveDelegacion);
    	 List<UnidadMedica> allUnidades = getUnidadesMedicasActivas();
         List<UnidadMedica> filteredUnidades = new ArrayList<>();

         for (int i = 0; i < allUnidades.size(); i++) {
             UnidadMedica unidad = allUnidades.get(i);
             if (unidad.getCveDelegacion().equals(cveDelegacion)) {
                 filteredUnidades.add(unidad);
             }
         }
         return filteredUnidades;
    }

    public void onItemSelectDelegacion(SelectEvent event) {
    	
    	if(unidadesMedicas!=null && !unidadesMedicas.isEmpty()){
        String rolString = objetosSs.getDatosUsuario().getRoles().get(0);
        if ("ROLE_ADMIN_CENTRAL".equals(rolString) || "ROLE_ADMIN_DELEG".equals(rolString)) {
        	 unidadesMedicas = catalogoService
                     .obtenerUnidadesMedicasPorDelegacion(getDelegacionSelected().getCveDelegacionImss());
        	 unidadSelected = null;
        }
        if ("ROLE_ARIMAC_CAPTURA".equals(rolString) || "ROLE_ARIMAC_JEFE".equals(rolString)) {
            unidadesMedicas = obtenerUnidadesByDelegacionUsuario(getDelegacionSelected().getCveDelegacionImss());
            unidadSelected = null;
        }
    	}
    }

    public List<UnidadMedica> completeUnidad(String query) {

        List<UnidadMedica> allUnidades = getUnidadesMedicas();
        List<UnidadMedica> filteredUnidades = new ArrayList<>();

        for (int i = 0; i < allUnidades.size(); i++) {
            UnidadMedica unidad = allUnidades.get(i);
            if (unidad.toString().toLowerCase().contains(query.toLowerCase())) {
                filteredUnidades.add(unidad);
            }
        }
        return filteredUnidades;
    }

    public String logout() {

        try {

            URL urlCE = new URL(AMBIENTE_CE.concat(LOGOUT_SISTEMA));
            URL urlHosp = new URL(AMBIENTE_HOSP.concat(LOGOUT_SISTEMA));
            //URL urlMonitoreo = new URL(AMBIENTE_MONITOREO.concat(LOGOUT_SISTEMA));
            //URL urlEnlaces = new URL(AMBIENTE_ENALCES.concat(LOGOUT_SISTEMA));
            HttpURLConnection connCE = (HttpURLConnection) urlCE.openConnection();
            HttpURLConnection connHosp = (HttpURLConnection) urlHosp.openConnection();
            //HttpURLConnection connMonitoreo = (HttpURLConnection) urlMonitoreo.openConnection();
            //HttpURLConnection connEnlaces = (HttpURLConnection) urlEnlaces.openConnection();

            // Eliminar de SIT_SESSION_DATA
            deleteSessionData();

            connHosp.getInputStream();
            //connMonitoreo.getInputStream();
            //connEnlaces.getInputStream();
            connCE.getInputStream();

            HttpSession ses = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
           // logger.info("#####Session ID: " + ses.getId());
            ses.invalidate();
//            FacesContext.getCurrentInstance().getExternalContext().redirect(ACCESS_GATEWAY_LOGOUT);

        } catch (IOException ex) {
            logger.error("Error en Logout", ex);
//            try {
//                FacesContext.getCurrentInstance().getExternalContext().redirect(ACCESS_GATEWAY_LOGOUT);
//            } catch (Exception e) {
//                logger.error("Error al redirigir al Logout NetIQ", e);
//            }
        }
        
        return "logout";
    }

    public void deleteSessionData() {

        DatosSesion ds = datosSesionService.getDatosSesion(objetosSs.getUserInfo(), objetosSs.getUsername());
        if (null != ds) {
            datosSesionService.delete(ds);
        }
    }

    public ObjetosEnSesionBean getObjetosSs() {

        return objetosSs;
    }

    public void setObjetosSs(ObjetosEnSesionBean objetosSs) {

        this.objetosSs = objetosSs;
    }

    public Boolean getBtnAceptarDisabled() {

        return btnAceptarDisabled;
    }

    public void setBtnAceptarDisabled(Boolean btnAceptarDisabled) {

        this.btnAceptarDisabled = btnAceptarDisabled;
    }

    public Boolean getMenuRendered() {

        return menuRendered;
    }

    public void setMenuRendered(Boolean menuRendered) {

        this.menuRendered = menuRendered;
    }

    public Boolean getBtnCancelarRendered() {

        return btnCancelarRendered;
    }

    public void setBtnCancelarRendered(Boolean btnCancelarRendered) {

        this.btnCancelarRendered = btnCancelarRendered;
    }

    public PersonalOperativo getUsuario() {

        return usuario;
    }

    public void setUsuario(PersonalOperativo usuario) {

        this.usuario = usuario;
    }

    public AutoComplete getDelegacionAcPf() {

        return delegacionAcPf;
    }

    public void setDelegacionAcPf(AutoComplete delegacionAcPf) {

        this.delegacionAcPf = delegacionAcPf;
    }

    public AutoComplete getUnidadMedicaAcPf() {

        return unidadMedicaAcPf;
    }

    public void setUnidadMedicaAcPf(AutoComplete unidadMedicaAcPf) {

        this.unidadMedicaAcPf = unidadMedicaAcPf;
    }

    public Boolean getOpcionesUsuarioDisabled() {

        return opcionesUsuarioDisabled;
    }

    public void setOpcionesUsuarioDisabled(Boolean opcionesUsuarioDisabled) {

        this.opcionesUsuarioDisabled = opcionesUsuarioDisabled;
    }

    public List<DelegacionIMSS> getDelegaciones() {

        return delegaciones;
    }

    public void setDelegaciones(List<DelegacionIMSS> delegaciones) {

        this.delegaciones = delegaciones;
    }

    public List<UnidadMedica> getUnidadesMedicas() {

        return unidadesMedicas;
    }

    public void setUnidadesMedicas(List<UnidadMedica> unidadesMedicas) {

        this.unidadesMedicas = unidadesMedicas;
    }

    public DelegacionIMSS getDelegacionSelected() {

        return delegacionSelected;
    }

    public void setDelegacionSelected(DelegacionIMSS delegacionSelected) {

        this.delegacionSelected = delegacionSelected;
    }

    public UnidadMedica getUnidadSelected() {

        return unidadSelected;
    }

    public void setUnidadSelected(UnidadMedica unidadSelected) {

        this.unidadSelected = unidadSelected;
    }

    public boolean getDelegacionDisabled() {

        return delegacionDisabled;
    }

    public void setDelegacionDisabled(boolean delegacionDisabled) {

        this.delegacionDisabled = delegacionDisabled;
    }

    public boolean getRenderIcons() {

        return renderIcons;
    }

    public static void setRenderIcons(boolean renderIcons) {

        PortadaController.renderIcons = renderIcons;
    }

    @Override
    public String toString() {

        return "PortadaController [userService=" + userService + "]";
    }

    public UserService getUserService() {

        return userService;
    }

    public void setUserService(UserService userService) {

        this.userService = userService;
    }

    public CatalogoService getCatalogoService() {

        return catalogoService;
    }

    public void setCatalogoService(CatalogoService catalogoService) {

        this.catalogoService = catalogoService;
    }

    public void setDatosSesionService(DatosSesionService datosSesionService) {

        this.datosSesionService = datosSesionService;
    }

	public List<DelegacionIMSS> getDelegacionesUnidades() {
		return delegacionesUnidades;
	}

	public void setDelegacionesUnidades(List<DelegacionIMSS> delegacionesUnidades) {
		this.delegacionesUnidades = delegacionesUnidades;
	}

	public List<UnidadMedica> getUnidadesMedicasActivas() {
		return unidadesMedicasActivas;
	}

	public void setUnidadesMedicasActivas(List<UnidadMedica> unidadesMedicasActivas) {
		this.unidadesMedicasActivas = unidadesMedicasActivas;
	}
    
    

}
