package mx.gob.imss.simo.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "SIC_DELEGACION_IMSS")
public class DelegacionIMSS {

    @Id
    private ObjectId id;
    @Field("cveDelegacion")
    private String cveDelegacionImss;
    @Field("desDelegacion")
    private String desDelegacionImss;
    private String refAbreviaturaDel;
    private String refMarca;
    private Date fecBaja;

    public String getCveDelegacionImss() {

        return cveDelegacionImss;
    }

    public void setCveDelegacionImss(String cveDelegacionImss) {

        this.cveDelegacionImss = cveDelegacionImss;
    }

    public String getDesDelegacionImss() {

        return desDelegacionImss;
    }

    public void setDesDelegacionImss(String desDelegacionImss) {

        this.desDelegacionImss = desDelegacionImss;
    }

    public String getRefAbreviaturaDel() {

        return refAbreviaturaDel;
    }

    public void setRefAbreviaturaDel(String refAbreviaturaDel) {

        this.refAbreviaturaDel = refAbreviaturaDel;
    }

    public String getRefMarca() {

        return refMarca;
    }

    public void setRefMarca(String refMarca) {

        this.refMarca = refMarca;
    }

    public Date getFecBaja() {

        return fecBaja;
    }

    public void setFecBaja(Date fecBaja) {

        this.fecBaja = fecBaja;
    }

    @Override
    public String toString() {

        return cveDelegacionImss + " " + desDelegacionImss;
    }

    public ObjectId getId() {

        return id;
    }

    public void setId(ObjectId id) {

        this.id = id;
    }

}
