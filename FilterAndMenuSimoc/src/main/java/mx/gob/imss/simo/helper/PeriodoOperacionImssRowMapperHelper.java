package mx.gob.imss.simo.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.model.PeriodoOperacion;

public class PeriodoOperacionImssRowMapperHelper  implements RowMapper<PeriodoOperacion> {

    public static final String CVE_PRESUPUESTAL = "CVE_PRESUPUESTAL";
    public static final String CVE_PERIODO_IMSS = "CVE_PERIODO_IMSS";   
    public static final String IND_ACTUAL = "IND_ACTUAL";
    public static final String IND_MONITOREO = "IND_MONITOREO";
    public static final String CVE_TIPO_CAPTURA = "CVE_TIPO_CAPTURA";
    public static final String IND_CERRADO = "IND_CERRADO";
    public static final String FEC_CIERRE = "FEC_CIERRE";
	
    @Override
    public PeriodoOperacion mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        PeriodoOperacion periodo = new PeriodoOperacion();
        periodo.setClavePresupuestal(resultSet.getString(CVE_PRESUPUESTAL));
        periodo.setTipoCaptura(resultSet.getInt(CVE_TIPO_CAPTURA));
        periodo.setClavePeriodoIMSS(resultSet.getString(CVE_PERIODO_IMSS));
        periodo.setIndicadorCerrado(resultSet.getBoolean(IND_CERRADO));
        periodo.setFechaCierre(resultSet.getDate(FEC_CIERRE));
        periodo.setIndicadorActual(resultSet.getBoolean(IND_ACTUAL));
        periodo.setIndicadorMonitoreo(resultSet.getBoolean(IND_MONITOREO));
        return periodo;
    }

}
