package mx.gob.imss.simo.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SIT_SESSION_DATA")
public class DatosSesion {

    @Id
    private ObjectId id;
    private String usuario;
    private String rol;
    private String netIQSessionId;
    private String cveUnidadMedicaAct;
    private String desUnidadMedicaAct;
    private String desDelegacionAct;
    private String cveDelegacionAct;
    private String cvePresupuestalAct;
    private Boolean mostrarDialogoDelegacion;
    private Date fecAcceso;

    public ObjectId getId() {

        return id;
    }

    public void setId(ObjectId id) {

        this.id = id;
    }

    public String getUsuario() {

        return usuario;
    }

    public void setUsuario(String usuario) {

        this.usuario = usuario;
    }

    public String getRol() {

        return rol;
    }

    public void setRol(String rol) {

        this.rol = rol;
    }

    public String getNetIQSessionId() {

        return netIQSessionId;
    }

    public void setNetIQSessionId(String netIQSessionId) {

        this.netIQSessionId = netIQSessionId;
    }

    public String getCveUnidadMedicaAct() {

        return cveUnidadMedicaAct;
    }

    public void setCveUnidadMedicaAct(String cveUnidadMedicaAct) {

        this.cveUnidadMedicaAct = cveUnidadMedicaAct;
    }

    public String getDesUnidadMedicaAct() {

        return desUnidadMedicaAct;
    }

    public void setDesUnidadMedicaAct(String desUnidadMedicaAct) {

        this.desUnidadMedicaAct = desUnidadMedicaAct;
    }

    public String getDesDelegacionAct() {

        return desDelegacionAct;
    }

    public void setDesDelegacionAct(String desDelegacionAct) {

        this.desDelegacionAct = desDelegacionAct;
    }

    public String getCveDelegacionAct() {

        return cveDelegacionAct;
    }

    public void setCveDelegacionAct(String cveDelegacionAct) {

        this.cveDelegacionAct = cveDelegacionAct;
    }

    public String getCvePresupuestalAct() {

        return cvePresupuestalAct;
    }

    public void setCvePresupuestalAct(String cvePresupuestalAct) {

        this.cvePresupuestalAct = cvePresupuestalAct;
    }

    public Boolean getMostrarDialogoDelegacion() {

        return mostrarDialogoDelegacion;
    }

    public void setMostrarDialogoDelegacion(Boolean mostrarDialogoDelegacion) {

        this.mostrarDialogoDelegacion = mostrarDialogoDelegacion;
    }

    public Date getFecAcceso() {

        return fecAcceso;
    }

    public void setFecAcceso(Date fecAcceso) {

        this.fecAcceso = fecAcceso;
    }

    @Override
    public String toString() {

        return "DatosSesion [id=" + id + ", usuario=" + usuario + ", rol=" + rol + ", netIQSessionId=" + netIQSessionId
                + ", cveUnidadMedicaAct=" + cveUnidadMedicaAct + ", desUnidadMedicaAct=" + desUnidadMedicaAct
                + ", desDelegacionAct=" + desDelegacionAct + ", cveDelegacionAct=" + cveDelegacionAct
                + ", cvePresupuestalAct=" + cvePresupuestalAct + ", mostrarDialogoDelegacion="
                + mostrarDialogoDelegacion + ", fecAcceso=" + fecAcceso + "]";
    }

}
