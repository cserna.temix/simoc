/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author francisco.rrios
 * 
 */
public class SdProcedimientos {

	private List<SdProcedimientoPrincipal> sdProcedimientoPrincipal = new ArrayList<SdProcedimientoPrincipal>();
	private List<SdProcedimientoAdicional> sdProcedimientoAdicional = new ArrayList<SdProcedimientoAdicional>();
	private List<SdProcedimientoComplementario> sdProcedimientoComplementario = new ArrayList<SdProcedimientoComplementario>();

	/**
	 * @return the sdProcedimientoPrincipal
	 */
	public List<SdProcedimientoPrincipal> getSdProcedimientoPrincipal() {
		return sdProcedimientoPrincipal;
	}

	/**
	 * @param sdProcedimientoPrincipal
	 *            the sdProcedimientoPrincipal to set
	 */
	public void setSdProcedimientoPrincipal(
			List<SdProcedimientoPrincipal> sdProcedimientoPrincipal) {
		this.sdProcedimientoPrincipal = sdProcedimientoPrincipal;
	}

	/**
	 * @return the sdProcedimientoAdicional
	 */
	public List<SdProcedimientoAdicional> getSdProcedimientoAdicional() {
		return sdProcedimientoAdicional;
	}

	/**
	 * @param sdProcedimientoAdicional
	 *            the sdProcedimientoAdicional to set
	 */
	public void setSdProcedimientoAdicional(
			List<SdProcedimientoAdicional> sdProcedimientoAdicional) {
		this.sdProcedimientoAdicional = sdProcedimientoAdicional;
	}

	/**
	 * @return the sdProcedimientoComplementario
	 */
	public List<SdProcedimientoComplementario> getSdProcedimientoComplementario() {
		return sdProcedimientoComplementario;
	}

	/**
	 * @param sdProcedimientoComplementario
	 *            the sdProcedimientoComplementario to set
	 */
	public void setSdProcedimientoComplementario(
			List<SdProcedimientoComplementario> sdProcedimientoComplementario) {
		this.sdProcedimientoComplementario = sdProcedimientoComplementario;
	}

}
