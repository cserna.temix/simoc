package mx.gob.imss.simo.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import mx.gob.imss.simo.model.DelegacionIMSS;

@FacesConverter("delegacionConverter")
public class DelegacionConverter implements Converter {

    static final Logger logger = Logger.getLogger(DelegacionConverter.class);

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {

        if (value != null && value.trim().length() > 0) {
            try {
                DelegacionIMSS del = new DelegacionIMSS();
                del.setCveDelegacionImss(value);
                return del;
            } catch (Exception e) {
                logger.error("Error en la conversion Delegacion", e);
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en la conversion",
                        "Delegacion no valida"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {

        if (object != null) {
            return String.valueOf(((DelegacionIMSS) object).getCveDelegacionImss());
        } else {
            return null;
        }
    }
}
