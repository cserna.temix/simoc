package mx.gob.imss.simo.service;

import java.util.List;

import org.primefaces.model.menu.MenuModel;

public interface MenuService {

    List<mx.gob.imss.simo.model.Menu> getMenu(String role);

    MenuModel createMenu(String role);

    String getAmbienteCE();
}
