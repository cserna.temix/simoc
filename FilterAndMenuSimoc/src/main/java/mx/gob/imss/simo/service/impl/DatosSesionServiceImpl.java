package mx.gob.imss.simo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.model.DatosSesion;
import mx.gob.imss.simo.repository.DatosSesionRepository;
import mx.gob.imss.simo.service.DatosSesionService;

@Service("datosSesionService")
public class DatosSesionServiceImpl implements DatosSesionService {

    @Autowired
    DatosSesionRepository datosSesionRepository;

    @Override
    public DatosSesion getDatosSesion(String netIQSessionId, String username) {

        return datosSesionRepository.findOneByNetIQSessionIdAndUsuario(netIQSessionId, username);
    }

    @Override
    public DatosSesion save(DatosSesion datosSesion) {

        return datosSesionRepository.save(datosSesion);
    }

    @Override
    public void delete(DatosSesion datosSesion) {

        datosSesionRepository.delete(datosSesion);

    }

    public DatosSesionRepository getDatosSesionRepository() {

        return datosSesionRepository;
    }

    public void setDatosSesionRepository(DatosSesionRepository datosSesionRepository) {

        this.datosSesionRepository = datosSesionRepository;
    }

}
