package mx.gob.imss.simo.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SIT_UNIDAD_MEDICA")
public class UnidadMedica {

	private Integer indTitular;
	private String cvePresupuestal;
	private String desUnidadMedica;
	private String cveDelegacion;
	private String desDelegacion;
	private Date fecBajaUnidad;
	private Date fecAltaUnidad;
    private int indActivoUnidad;

	public Integer getIndTitular() {
		return indTitular;
	}

	public void setIndTitular(Integer indTitular) {
		this.indTitular = indTitular;
	}

	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	public String getDesUnidadMedica() {
		return desUnidadMedica;
	}

	public void setDesUnidadMedica(String desUnidadMedica) {
		this.desUnidadMedica = desUnidadMedica;
	}

	public String getCveDelegacion() {
		return cveDelegacion;
	}

	public void setCveDelegacion(String cveDelegacion) {
		this.cveDelegacion = cveDelegacion;
	}

	public String getDesDelegacion() {
		return desDelegacion;
	}

	public void setDesDelegacion(String desDelegacion) {
		this.desDelegacion = desDelegacion;
	}

	public Date getFecBajaUnidad() {
		return fecBajaUnidad;
	}

	public void setFecBajaUnidad(Date fecBajaUnidad) {
		this.fecBajaUnidad = fecBajaUnidad;
	}

	public Date getFecAltaUnidad() {
		return fecAltaUnidad;
	}

	public void setFecAltaUnidad(Date fecAltaUnidad) {
		this.fecAltaUnidad = fecAltaUnidad;
	}

	public int getIndActivoUnidad() {
		return indActivoUnidad;
	}

	public void setIndActivoUnidad(int indActivoUnidad) {
		this.indActivoUnidad = indActivoUnidad;
	}

	@Override
	public String toString() {
		return "UnidadMedica [indTitular=" + indTitular + ", cvePresupuestal=" + cvePresupuestal + ", desUnidadMedica="
				+ desUnidadMedica + ", cveDelegacion=" + cveDelegacion + ", desDelegacion=" + desDelegacion
				+ ", fecBajaUnidad=" + fecBajaUnidad + ", fecAltaUnidad=" + fecAltaUnidad + ", indActivoUnidad="
				+ indActivoUnidad + "]";
	}
}
