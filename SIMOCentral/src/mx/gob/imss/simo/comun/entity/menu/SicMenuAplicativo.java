/**
 * 
 */
package mx.gob.imss.simo.comun.entity.menu;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author janiel.mb
 *
 */
@Entity("SIC_MENU_APLICATIVO")
public class SicMenuAplicativo {

	@Id
	private ObjectId id;
	private Integer cveMenuAplicativo;
	private String desMenuAplicativo;
	
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public Integer getCveMenuAplicativo() {
		return cveMenuAplicativo;
	}
	public void setCveMenuAplicativo(Integer cveMenuAplicativo) {
		this.cveMenuAplicativo = cveMenuAplicativo;
	}
	public String getDesMenuAplicativo() {
		return desMenuAplicativo;
	}
	public void setDesMenuAplicativo(String desMenuAplicativo) {
		this.desMenuAplicativo = desMenuAplicativo;
	}
	
	
}


