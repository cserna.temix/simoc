package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("SIC_CIE10_VIGILANCIA_EPI")
public class SicCie10VigilanciaEpi {

	@Id
	private ObjectId id;
	private String cveCie10VigEpi;
	private String desCie10VigEpi;
	private String refLetra;
	private Integer numDigito;
	private Integer cveEpi;
	private String desEpi;
	private Integer numConsecutivo;
	private Date fecBaja;
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getCveCie10VigEpi() {
		return cveCie10VigEpi;
	}
	public void setCveCie10VigEpi(String cveCie10VigEpi) {
		this.cveCie10VigEpi = cveCie10VigEpi;
	}
	public String getDesCie10VigEpi() {
		return desCie10VigEpi;
	}
	public void setDesCie10VigEpi(String desCie10VigEpi) {
		this.desCie10VigEpi = desCie10VigEpi;
	}
	public String getRefLetra() {
		return refLetra;
	}
	public void setRefLetra(String refLetra) {
		this.refLetra = refLetra;
	}
	public Integer getNumDigito() {
		return numDigito;
	}
	public void setNumDigito(Integer numDigito) {
		this.numDigito = numDigito;
	}
	public Integer getCveEpi() {
		return cveEpi;
	}
	public void setCveEpi(Integer cveEpi) {
		this.cveEpi = cveEpi;
	}
	public String getDesEpi() {
		return desEpi;
	}
	public void setDesEpi(String desEpi) {
		this.desEpi = desEpi;
	}
	public Integer getNumConsecutivo() {
		return numConsecutivo;
	}
	public void setNumConsecutivo(Integer numConsecutivo) {
		this.numConsecutivo = numConsecutivo;
	}
	public Date getFecBaja() {
		return fecBaja;
	}
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}
	
	
}
