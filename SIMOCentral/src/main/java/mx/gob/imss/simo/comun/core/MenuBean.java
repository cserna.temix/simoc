/**
 * 
 */
package mx.gob.imss.simo.comun.core;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.comun.enums.EnumColores;
import mx.gob.imss.simo.comun.enums.EnumNavegacion;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;

/**
 * @author janmac
 *
 */


@Component
@Scope("session")
public class MenuBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	ObjetosEnSesionBean objetosSs;
	
	public MenuBean() {
	}
	
	/*           PORTADA         */
	public String irPortada(){
		objetosSs.setColorHeader(EnumColores.PRINCIPAL.getValor());
		return EnumNavegacion.PORTADA.getValor();
	}
	
	/*           CONSULTA EXTERNA         */
	
	/**Registrar atención de Consulta externa en SIMO
	 * SGMP_F07_DST_F23_EspCU_SIMO_RegistrarAtnCE
	 * 
	 * @return url de la página
	 */
	public String irConExtRegistra(){
		objetosSs.setColorHeader(EnumColores.CONSULTA_EXTERNA.getValor());
		return EnumNavegacion.CONSULTA_EXTERNA.getValor();

	}
	
	public String irConExtRegistraEditar(){
		objetosSs.setColorHeader(EnumColores.CONSULTA_EXTERNA.getValor());
		return EnumNavegacion.CONSULTA_EXTERNA.getValor();
	}
	
	public String irConExtEditar(){
		objetosSs.setColorHeader(EnumColores.CONSULTA_EXTERNA.getValor());
		objetosSs.setBusquedaCex(Boolean.TRUE);
		return EnumNavegacion.CONSULTA_EXTERNA.getValor();
	}
	
	/**Editar atención de Consulta externa en SIMO
	 * SGMP_F07_DST_F23_EspCU_SIMO_EditarAtnCE
	 * 
	 * @return url de la página
	 */
	public String irConExtBusquedaEdicion(){
		objetosSs.setColorHeader(EnumColores.CONSULTA_EXTERNA.getValor());
		return EnumNavegacion.CONSULTA_EXTERNA_BUSQ_ED.getValor();
	}
	public String irConExtEditarCancelar(){
		objetosSs.setColorHeader(EnumColores.CONSULTA_EXTERNA.getValor());
		objetosSs.setBusquedaCex(Boolean.TRUE);
		return EnumNavegacion.CONSULTA_EXTERNA_BUSQ_ED.getValor();
	}
	
	public String irCierreMes(){
		objetosSs.setColorHeader(EnumColores.REPORTES.getValor());
		return EnumNavegacion.CIERRE_MES.getValor();
	}
	
	public ObjetosEnSesionBean getObjetosSs() {
		return objetosSs;
	}

	public void setObjetosSs(ObjetosEnSesionBean objetosSs) {
		this.objetosSs = objetosSs;
	}

}
