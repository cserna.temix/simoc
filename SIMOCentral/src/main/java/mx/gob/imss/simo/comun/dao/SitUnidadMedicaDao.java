/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.negocio.SitUnidadMedica;

/**
 * @author francisco.rrios
 * 
 */
public interface SitUnidadMedicaDao {

	/**
	 * @return
	 */
	List<SitUnidadMedica> burcarTodos();

	/**
	 * @param idUnidadMedica
	 * @return
	 */
	List<SitUnidadMedica> burcarPorId(String idUnidadMedica);

	/**
	 * @param cveDelegacion
	 * @return
	 */
	List<SitUnidadMedica> burcarPorCveDelegacion(String cveDelegacion);

}
