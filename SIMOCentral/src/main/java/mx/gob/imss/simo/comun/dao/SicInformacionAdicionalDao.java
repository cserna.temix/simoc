/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicional;

/**
 * @author francisco.rrios
 * 
 */
public interface SicInformacionAdicionalDao {

	/**
	 * @return
	 */
	List<SicInformacionAdicional> burcarTodos();

	/**
	 * @param cveInformacionAdicional
	 * @return
	 */
	List<SicInformacionAdicional> burcarPorId(String cveInformacionAdicional);
	
	/**
	 * @param tipo
	 * @return
	 */
	List<SicInformacionAdicional> burcarPorTipo(Integer tipo);
}
