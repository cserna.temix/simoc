/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_CITADO")
public class SicCIitado {

	@Id
	private ObjectId id;
	private Integer cveCitado;
	private String desCitado;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveCitado
	 */
	public Integer getCveCitado() {
		return cveCitado;
	}

	/**
	 * @param cveCitado
	 *            the cveCitado to set
	 */
	public void setCveCitado(Integer cveCitado) {
		this.cveCitado = cveCitado;
	}

	/**
	 * @return the desCitado
	 */
	public String getDesCitado() {
		return desCitado;
	}

	/**
	 * @param desCitado
	 *            the desCitado to set
	 */
	public void setDesCitado(String desCitado) {
		this.desCitado = desCitado;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
