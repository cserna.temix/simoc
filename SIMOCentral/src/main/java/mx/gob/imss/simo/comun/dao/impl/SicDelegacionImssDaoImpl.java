/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.mapping.Mapper;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicDelegacionImssDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicDelegacionImss;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicDelegacionImssDao")
public class SicDelegacionImssDaoImpl implements SicDelegacionImssDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicDelegacionImss> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicDelegacionImss> listDelegacionesUMAES = new ArrayList<SicDelegacionImss>();
		try {
			//connectionMongo = new ConnectionMongo();
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listDelegacionesUMAES = dsEsp.createQuery(
					SicDelegacionImss.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} finally {
			//connectionMongo.cerrarConexion();
		}

		return listDelegacionesUMAES;
	}

	@Override
	public List<SicDelegacionImss> burcarPorId(String idDelUMAES) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicDelegacionImss> listDelegacionesUMAES = new ArrayList<SicDelegacionImss>();
		try {
			//connectionMongo = new ConnectionMongo();
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listDelegacionesUMAES = dsEsp
					.createQuery(SicDelegacionImss.class)
					.field(Mapper.ID_KEY).equal(new ObjectId(idDelUMAES))
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} finally {
			//connectionMongo.cerrarConexion();
		}

		return listDelegacionesUMAES;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
