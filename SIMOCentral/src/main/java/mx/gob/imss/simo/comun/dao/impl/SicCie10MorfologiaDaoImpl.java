/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicCie10MorfologiaDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie10Morfologia;

/**
 * @author francisco.rrios
 *
 */
@Repository("sicCie10MorfologiaDao")
public class SicCie10MorfologiaDaoImpl implements SicCie10MorfologiaDao {

	final static Logger logger = Logger.getLogger(SicCie10MorfologiaDaoImpl.class.getName());

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicCie10Morfologia> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicCie10Morfologia> listCie10Morfologia = new ArrayList<SicCie10Morfologia>();
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();

			listCie10Morfologia = ds.createQuery(SicCie10Morfologia.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listCie10Morfologia;
	}

	@Override
	public List<SicCie10Morfologia> burcarPorId(String cveCie10Morfologia) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicCie10Morfologia> listCie10Morfologia = new ArrayList<SicCie10Morfologia>();
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();

			listCie10Morfologia = ds.createQuery(SicCie10Morfologia.class).field("cveCie10Morfologia")
					.equal(cveCie10Morfologia).asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listCie10Morfologia;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicCie10MorfologiaDao#burcarPorVigentes(java.
	 * lang.String)
	 */
	@Override
	public List<SicCie10Morfologia> burcarPorVigentes(String cveCie10Morfologia) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicCie10Morfologia> listCie10Morfologia = new ArrayList<SicCie10Morfologia>();
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SicCie10Morfologia> query = ds.createQuery(SicCie10Morfologia.class);
			query.field("cveCie10Morfologia").equal(cveCie10Morfologia);
//			query.filter("fecBaja exists", 0);

			listCie10Morfologia = query.asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listCie10Morfologia;
	}

	@Override
	public SicCie10Morfologia buscarPorId(String cveCie10Morfologia) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		SicCie10Morfologia morf = null;
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SicCie10Morfologia> query = ds.createQuery(SicCie10Morfologia.class);
			query.field("cveCie10Morfologia").equal(cveCie10Morfologia);

			morf = query.get();

		} catch (Exception e) {
			e.getMessage();
		}
		return morf;
	}

}
