/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_TIPO_ANESTECIA")
public class SicTipoAnestecia {

	@Id
	private ObjectId id;
	private Integer cveTipoAnestecia;
	private String desTipoAnestecia;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTipoAnestecia
	 */
	public Integer getCveTipoAnestecia() {
		return cveTipoAnestecia;
	}

	/**
	 * @param cveTipoAnestecia
	 *            the cveTipoAnestecia to set
	 */
	public void setCveTipoAnestecia(Integer cveTipoAnestecia) {
		this.cveTipoAnestecia = cveTipoAnestecia;
	}

	/**
	 * @return the desTipoAnestecia
	 */
	public String getDesTipoAnestecia() {
		return desTipoAnestecia;
	}

	/**
	 * @param desTipoAnestecia
	 *            the desTipoAnestecia to set
	 */
	public void setDesTipoAnestecia(String desTipoAnestecia) {
		this.desTipoAnestecia = desTipoAnestecia;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
