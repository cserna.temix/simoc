/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTurno;

/**
 * @author francisco.rrios
 * 
 */
public interface SicTurnoDao {

	/**
	 * @return
	 */
	List<SicTurno> burcarTodos();

	/**
	 * @param cveTurno
	 * @return
	 */
	List<SicTurno> burcarPorId(Integer cveTurno);
}
