/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoProgramaDao;

/**
 * @author francisco.rodriguez
 *
 */
public class SicTipoProgramaDaoImpl implements SicTipoProgramaDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoProgramaDao#burcarTodos()
	 */
	@Override
	public List<SicTipoPrograma> burcarTodos() {
//		SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoPrograma> listTipoProgramao = new ArrayList<SicTipoPrograma>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listTipoProgramao = dsEsp.createQuery(SicTipoPrograma.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoProgramao;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoProgramaDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicTipoPrograma> burcarPorId(Integer cveTipoPrograma) {
//		SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoPrograma> listTipoProgramao = new ArrayList<SicTipoPrograma>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listTipoProgramao = dsEsp.createQuery(SicTipoPrograma.class)
					.field("cveTipoPrograma").equal(cveTipoPrograma).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoProgramao;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
