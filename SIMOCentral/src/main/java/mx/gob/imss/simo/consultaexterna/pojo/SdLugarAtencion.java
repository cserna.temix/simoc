/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdLugarAtencion {

	private Integer cveLugarAtencion;
	private String desLugarAtencion;

	/**
	 * @return the cveLugarAtencion
	 */
	public Integer getCveLugarAtencion() {
		return cveLugarAtencion;
	}

	/**
	 * @param cveLugarAtencion
	 *            the cveLugarAtencion to set
	 */
	public void setCveLugarAtencion(Integer cveLugarAtencion) {
		this.cveLugarAtencion = cveLugarAtencion;
	}

	/**
	 * @return the desLugarAtencion
	 */
	public String getDesLugarAtencion() {
		return desLugarAtencion;
	}

	/**
	 * @param desLugarAtencion
	 *            the desLugarAtencion to set
	 */
	public void setDesLugarAtencion(String desLugarAtencion) {
		this.desLugarAtencion = desLugarAtencion;
	}

}
