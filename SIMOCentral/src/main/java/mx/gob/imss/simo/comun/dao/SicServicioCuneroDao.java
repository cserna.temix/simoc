/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicServicioCunero;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicServicioCuneroDao {

	/**
	 * @return
	 */
	List<SicServicioCunero> burcarTodos();

	/**
	 * @param cveServicioCunero
	 * @return
	 */
	List<SicServicioCunero> burcarPorId(Integer cveServicioCunero);
}
