/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_TIPO_PRESTADOR")
public class SicTipoPrestador {

	@Id
	private ObjectId id;
	private Integer cveTipoPrestador;
	private String desTipoPrestador;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTipoPrestador
	 */
	public Integer getCveTipoPrestador() {
		return cveTipoPrestador;
	}

	/**
	 * @param cveTipoPrestador
	 *            the cveTipoPrestador to set
	 */
	public void setCveTipoPrestador(Integer cveTipoPrestador) {
		this.cveTipoPrestador = cveTipoPrestador;
	}

	/**
	 * @return the desTipoPrestador
	 */
	public String getDesTipoPrestador() {
		return desTipoPrestador;
	}

	/**
	 * @param desTipoPrestador
	 *            the desTipoPrestador to set
	 */
	public void setDesTipoPrestador(String desTipoPrestador) {
		this.desTipoPrestador = desTipoPrestador;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
