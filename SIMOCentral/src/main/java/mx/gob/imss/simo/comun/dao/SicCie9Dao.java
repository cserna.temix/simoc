/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicCie9;

/**
 * @author francisco.rrios
 * 
 */
public interface SicCie9Dao {

	/**
	 * @return
	 */
	List<SicCie9> burcarTodos();

	/**
	 * @param cveProcedimiento
	 * @return
	 */
	SicCie9 buscarPorId(String cveProcedimiento);

	/**
	 * @param cveProcedimiento
	 * @param edad
	 * @param sexo
	 * @param edadAnn
	 * @return
	 */
	List<SicCie9> burcarEdadSexo(String cveProcedimiento, int edad, Integer sexo, boolean edadAnn);
}
