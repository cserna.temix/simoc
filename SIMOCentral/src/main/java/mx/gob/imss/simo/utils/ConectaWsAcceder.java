/**
 * 
 */
package mx.gob.imss.simo.utils;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.mongodb.morphia.Datastore;
import org.w3c.dom.Element;

import com.mongodb.client.MongoDatabase;

import mx.gob.imss.didt.cia.interoper.servicios.endpoint.ObtenerServicioResponse;
import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dto.AccederDto;
import mx.gob.imss.simo.comun.entity.catalogos.SicConstantes;
import mx.gob.imss.simo.comun.entity.negocio.SitUnidadMedica;
import mx.gob.imss.simo.master.client.ws.SimoMasterClientWS;
import mx.gob.imss.simo.vigencia.jaxb.GetInfo;
import mx.gob.imss.simo.vigencia.jaxb.GetInfoResponse;
import mx.gob.imss.simo.vigencia.jaxb.InfoAseguradoVO;

/**
 * @author aduer.macedo
 *
 */
public class ConectaWsAcceder extends Thread{

	final static Logger logger = Logger.getLogger(ConectaWsAcceder.class.getName());
	
	private String nss;
	private String cpid;
	private String aMedico;
	private String simo_osb;



	private List<AccederDto> listaConsulExtDto=null;
//	private ConnectionMongo connectionMongo;
	
	
	public ConectaWsAcceder(String nss, String cpid, String aMedico, String simo_osb){
//		this.connectionMongo=connectionMongo;
		this.nss=nss;
		this.cpid=cpid;
		this.aMedico=aMedico;
		this.simo_osb=simo_osb;
	}
	
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}

//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

	public void run(){
		
			
		// Request Vigencia
				GetInfo getInfo = new GetInfo();
				getInfo.setCpid(cpid);
				getInfo.setNss(nss);
				AccederDto consulExtDto = null;
				ObtenerServicioResponse response; 
				try {
					logger.info("cpid "+cpid);
					logger.info("nss: "+nss);
					response = SimoMasterClientWS.obtenerServicio(getInfo,"ConsultarAcceder", "version", this.obtenerUrlConstante(simo_osb));

					//logger.info("ACCEDERresponse:"+response);
					GetInfoResponse	infoResponse = (GetInfoResponse) SimoMasterClientWS
							.unmarshallObject((Element) response.getEndPointCadisOut().getMensaje(), GetInfoResponse.class);
					logger.info("Beneficiarios: "+ infoResponse.getReturn().getBeneficiarios());
					if(infoResponse.getReturn().getCodigoError()==0){
						logger.info("ACCEDER encontro el NSS");
					}else{
						listaConsulExtDto=new ArrayList<AccederDto>();
						logger.info("ACCEDER no encontro el NSS");
						consulExtDto = new AccederDto();
						consulExtDto.setEncontrado(Boolean.FALSE);
						listaConsulExtDto.add(consulExtDto);

					}

					if (aMedico.equals(infoResponse.getReturn().getAgregadoMedico().getValue())) {
						logger.info("Info Response Agregado"+ infoResponse.getReturn().getAgregadoMedico());
						listaConsulExtDto=new ArrayList<AccederDto>();
							consulExtDto = new AccederDto();
							consulExtDto.setNombre(infoResponse.getReturn().getNombre().getValue());
							consulExtDto.setApePaterno(infoResponse.getReturn().getPaterno().getValue());
							consulExtDto.setApeMaterno(infoResponse.getReturn().getMaterno().getValue());
							consulExtDto.setUnidadAdscripcion(infoResponse.getReturn().getClavePresupuestal().getValue());
							consulExtDto.setCveUnidadMed(infoResponse.getReturn().getDhUMF().getValue());
							//consulExtDto.setDescUnidadMed(buscarDescUnidadMedica(infoResponse.getReturn().getClavePresupuestal().getValue()));
							consulExtDto.setDescUnidadMed(infoResponse.getReturn().getClavePresupuestal().getValue());
							consulExtDto.setFecNacimiento(infoResponse.getReturn().getFechaNacimiento().getValue());
							consulExtDto.setVigencia(infoResponse.getReturn().getConDerechoSm().getValue().equals("SI")?true:false);
							consulExtDto.setEncontrado(Boolean.TRUE);
							consulExtDto.setAgregadoMedico(infoResponse.getReturn().getAgregadoMedico().getValue());
							consulExtDto.setIndVigente(infoResponse.getReturn().getConDerechoSm().getValue().equals("SI")?1:0);
							consulExtDto.setIndPacEncontrado(1);
							consulExtDto.setCveCurp(infoResponse.getReturn().getCurp().getValue());
							consulExtDto.setStpUltConsultaVig(new Date());
							consulExtDto.setCveIdee(infoResponse.getReturn().getIdee() != null
								? infoResponse.getReturn().getIdee().getValue() : null);

							listaConsulExtDto.add(consulExtDto);
					} else {
						listaConsulExtDto=new ArrayList<AccederDto>();
						consulExtDto = new AccederDto();
						consulExtDto.setEncontrado(Boolean.FALSE);
						if (infoResponse.getReturn().getBeneficiarios() != null) {
							for (InfoAseguradoVO beneficiario : infoResponse
									.getReturn().getBeneficiarios()) {
								//if (aMedico.equals(beneficiario.getAgregadoMedico().getValue())) {
										consulExtDto = new AccederDto();
										consulExtDto.setNombre(beneficiario.getNombre().getValue());
										consulExtDto.setApePaterno(beneficiario.getPaterno().getValue());
										consulExtDto.setApeMaterno(beneficiario.getMaterno().getValue());
										consulExtDto.setUnidadAdscripcion(beneficiario.getClavePresupuestal().getValue());
										consulExtDto.setCveUnidadMed(beneficiario.getDhUMF().getValue());
										consulExtDto.setDescUnidadMed(buscarDescUnidadMedica(beneficiario.getClavePresupuestal().getValue()));
										consulExtDto.setFecNacimiento(beneficiario.getFechaNacimiento().getValue());
										consulExtDto.setVigencia(beneficiario.getConDerechoSm().getValue().equals("SI")?true:false);
										consulExtDto.setEncontrado(Boolean.TRUE);
										
										consulExtDto.setIndVigente(beneficiario.getConDerechoSm().getValue().equals("SI")?1:0);
										consulExtDto.setIndPacEncontrado(1);
										consulExtDto.setCveCurp(beneficiario.getCurp().getValue());
										consulExtDto.setStpUltConsultaVig(new Date());
										consulExtDto.setCveIdee(infoResponse.getReturn().getIdee() != null
												? infoResponse.getReturn().getIdee().getValue() : null);

										consulExtDto.setAgregadoMedico(beneficiario.getAgregadoMedico().getValue());

										listaConsulExtDto.add(consulExtDto);


								//	break;
						//		}
							}
						}
						//Se añade el asegurado a la lista
						consulExtDto = new AccederDto();
						consulExtDto.setNombre(infoResponse.getReturn().getNombre().getValue());
						consulExtDto.setApePaterno(infoResponse.getReturn().getPaterno().getValue());
						consulExtDto.setApeMaterno(infoResponse.getReturn().getMaterno().getValue());
						consulExtDto.setUnidadAdscripcion(infoResponse.getReturn().getClavePresupuestal().getValue());
						consulExtDto.setCveUnidadMed(infoResponse.getReturn().getDhUMF().getValue());
						//consulExtDto.setDescUnidadMed(buscarDescUnidadMedica(infoResponse.getReturn().getClavePresupuestal().getValue()));
						consulExtDto.setDescUnidadMed(infoResponse.getReturn().getClavePresupuestal().getValue());
						consulExtDto.setFecNacimiento(infoResponse.getReturn().getFechaNacimiento().getValue());
						consulExtDto.setVigencia(infoResponse.getReturn().getConDerechoSm().getValue().equals("SI")?true:false);
						consulExtDto.setEncontrado(Boolean.TRUE);

						consulExtDto.setIndVigente(infoResponse.getReturn().getConDerechoSm().getValue().equals("SI")?1:0);
						consulExtDto.setIndPacEncontrado(1);
						consulExtDto.setCveCurp(infoResponse.getReturn().getCurp().getValue());
						consulExtDto.setStpUltConsultaVig(new Date());
						consulExtDto.setCveIdee(infoResponse.getReturn().getIdee() != null
								? infoResponse.getReturn().getIdee().getValue() : null);
						consulExtDto.setAgregadoMedico(infoResponse.getReturn().getAgregadoMedico().getValue());
						listaConsulExtDto.add(consulExtDto);
					}
				} catch (JAXBException e) {
					logger.info(e.getMessage());			
				} catch(MalformedURLException me) {
					logger.info(me.getMessage());
				}catch (UnsupportedEncodingException ue) {
					logger.info(ue.getMessage());
				}
			
		}

	private String obtenerUrlConstante(String value){
		String url = "";
		//SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();			
			url = ds.createQuery(SicConstantes.class).filter("cveConstante", value).get().getRefValor();

		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return url;
	}
	
	private String buscarDescUnidadMedica(String cvePresupuestal) {
		String descUnidadMedica = "";
//		SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
			SitUnidadMedica uni = ds.createQuery(SitUnidadMedica.class).filter("cvePresupuestal", cvePresupuestal).get();
			descUnidadMedica = uni.getDesUnidadMedica();
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return descUnidadMedica;
	}

	public String getNss() {
		return nss;
	}

	public void setNss(String nss) {
		this.nss = nss;
	}

	public String getCpid() {
		return cpid;
	}

	public void setCpid(String cpid) {
		this.cpid = cpid;
	}

	public String getaMedico() {
		return aMedico;
	}

	public void setaMedico(String aMedico) {
		this.aMedico = aMedico;
	}

	public String getSimo_osb() {
		return simo_osb;
	}

	public void setSimo_osb(String simo_osb) {
		this.simo_osb = simo_osb;
	}
	public List<AccederDto> getListaConsulExtDto() {
		return listaConsulExtDto;
	}

	public void setListaConsulExtDto(List<AccederDto> listaConsulExtDto) {
		this.listaConsulExtDto = listaConsulExtDto;
	}

	
}
