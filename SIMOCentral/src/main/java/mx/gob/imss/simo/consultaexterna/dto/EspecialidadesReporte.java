package mx.gob.imss.simo.consultaexterna.dto;

import java.util.List;

public class EspecialidadesReporte {
	
	private String cveEspecialidad;
	private String matricula;
	private String nombre;
	private int indicadorMedico;
	private boolean agregado;
	private List<DatosReporteParteI> medicos;
	
	
	public String getCveEspecialidad() {
		return cveEspecialidad;
	}
	public void setCveEspecialidad(String cveEspecialidad) {
		this.cveEspecialidad = cveEspecialidad;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getIndicadorMedico() {
		return indicadorMedico;
	}
	public void setIndicadorMedico(int indicadorMedico) {
		this.indicadorMedico = indicadorMedico;
	}
	public List<DatosReporteParteI> getMedicos() {
		return medicos;
	}
	public void setMedicos(List<DatosReporteParteI> medicos) {
		this.medicos = medicos;
	}
	/**
	 * @return the agregado
	 */
	public boolean isAgregado() {
		return agregado;
	}
	/**
	 * @param agregado the agregado to set
	 */
	public void setAgregado(boolean agregado) {
		this.agregado = agregado;
	}

	
	

}
