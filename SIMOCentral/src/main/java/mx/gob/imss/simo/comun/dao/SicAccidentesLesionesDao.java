/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicAccidentesLesiones;

/**
 * @author francisco.rrios
 * 
 */
public interface SicAccidentesLesionesDao {

	/**
	 * @return
	 */
	List<SicAccidentesLesiones> burcarTodos();

	/**
	 * @param idAccidentesLesiones
	 * @return
	 */
	List<SicAccidentesLesiones> burcarPorId(Integer cveAccidentesLesiones);
}
