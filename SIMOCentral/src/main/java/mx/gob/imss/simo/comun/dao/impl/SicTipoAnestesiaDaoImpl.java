/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoAnestesiaDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoAnestesia;

/**
 * @author francisco.rodriguez
 *
 */
@Repository("sicTipoAnestesiaDao")
public class SicTipoAnestesiaDaoImpl implements SicTipoAnestesiaDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoAnestesiaDao#burcarTodos()
	 */
	@Override
	public List<SicTipoAnestesia> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoAnestesia> listTipoAnestesia = new ArrayList<SicTipoAnestesia>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoAnestesia = dsEsp.createQuery(SicTipoAnestesia.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoAnestesia;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoAnestesiaDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicTipoAnestesia> burcarPorId(Integer cveTipoAnestecia) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoAnestesia> listTipoAnestesia = new ArrayList<SicTipoAnestesia>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoAnestesia = dsEsp.createQuery(SicTipoAnestesia.class)
					.field("cveTipoAnestecia").equal(cveTipoAnestecia).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoAnestesia;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
