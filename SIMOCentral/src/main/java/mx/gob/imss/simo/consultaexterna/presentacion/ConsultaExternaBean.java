package mx.gob.imss.simo.consultaexterna.presentacion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import lombok.NonNull;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import mx.gob.imss.simo.comun.core.AbstractBaseBean;
import mx.gob.imss.simo.comun.core.ReglasNegocio;
import mx.gob.imss.simo.comun.dto.AccederDto;
import mx.gob.imss.simo.comun.dto.CatalogoDto;
import mx.gob.imss.simo.comun.dto.SiapDto;
import mx.gob.imss.simo.comun.dto.SicCie10DTO;
import mx.gob.imss.simo.comun.dto.SicCie9DTO;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie10Morfologia;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicionalVIH;
import mx.gob.imss.simo.comun.enums.EnumDiagMorfologiasTumor;
import mx.gob.imss.simo.comun.enums.EnumEspecialidades;
import mx.gob.imss.simo.comun.enums.EnumModuloAtencion;
import mx.gob.imss.simo.comun.enums.EnumNavegacion;
import mx.gob.imss.simo.comun.enums.EnumRamaAseguramiento;
import mx.gob.imss.simo.comun.enums.EnumTipoInformacionAdicional;
import mx.gob.imss.simo.comun.service.CatalogosService;
import mx.gob.imss.simo.consultaexterna.dto.ControlMedicoEncabezadoDto;
import mx.gob.imss.simo.consultaexterna.pojo.SdInformacionAdicional;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoDiarrea;
import mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService;
import mx.gob.imss.simo.model.ConsultaExternaDto;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.ws.service.AccederWsService;

@ManagedBean(name = "consultaExterna")
@ViewScoped
public class ConsultaExternaBean extends AbstractBaseBean {

    final static Log logger = LogFactory.getLog(ConsultaExternaBean.class.getName());

    private static final long serialVersionUID = 1L;

    @ManagedProperty("#{catalogosService}")
    private CatalogosService catalogosService;

    @ManagedProperty("#{consultaExternaService}")
    private ConsultaExternaService consultaExternaService;

    @ManagedProperty("#{accederService}")
    private AccederWsService accederService;

    @ManagedProperty("#{objetosSs}")
    ObjetosEnSesionBean objetosSs;

    private ConsultaExternaDto consulExtDto;
    private ControlMedicoEncabezadoDto controlMedicoEncabezadoDto;

    RequestContext context = RequestContext.getCurrentInstance();

    private Boolean dialogExiste;
    private Boolean dialogCantidad;
    private Boolean dialogEdad;

    private Boolean dialogFecCirugiaPrin;
    private Boolean dialogDiarreaPrin;
    private Boolean dialogFecCirugiaAdic;
    private Boolean dialogDiarreaAdic;
    private Boolean dialogFecCirugiaCom;
    private Boolean dialogDiarreaCom;

    private List<CatalogoDto> listadoEspecialidad;
    private List<String> listaEspecialidadStr;
    private List<CatalogoDto> listadoConsultorio;
    private List<String> listaConsultorioStr;
    private List<CatalogoDto> listadoTurnos;
    private List<CatalogoDto> listadoTipoPrestador;
    private List<CatalogoDto> listadoAgreMed;
    private List<String> listaRegimenStr;
    private List<CatalogoDto> listadoPases;
    private List<CatalogoDto> listadoMetPPF;
    private List<String> listaMetPpfStr;
    private List<CatalogoDto> listadoAccLesiones;
    private List<CatalogoDto> listadoRiesgos;
    private List<CatalogoDto> listadoTipUrgencia;
    private List<CatalogoDto> lista1Vez;
    private List<SdInformacionAdicional> listaInfoAdicional;
    private List<SdTipoDiarrea> listaTipoDiarrea;


    private List<AccederDto> listaBeneficiarios;


    private AccederDto beneficiario;

    private boolean desBotonAgregar;
    private boolean deshabilitadoMed;
    private boolean deshabilitadoMedS;
    private boolean deshabilitadoMedA;
    private boolean deshabilitadoMedR;
    private boolean deshabilitado;
    private boolean deshabilitadoA;
    private boolean deshabilitadoR = true;
    private boolean deshabilitadoAgre;
    private boolean deshabilitadoAgreA;
    private boolean deshabilitadoAgreR = true;
    private boolean deshabilitadoNom;
    private boolean deshabilitadoNomA;
    private boolean deshabilitadoNomR = true;
    private boolean datosDerecho = false;
    private boolean datosUniAds = false;
    private boolean atenion = false;
    private boolean metodoPf = false;
    private boolean metodoPfCan = false;
    private boolean validaFecha;
    private boolean validaMatricula;
    private boolean validaEspecialidad;
    private boolean validaTipoPrestado;
    private boolean validaConsultorio;
    private boolean validaTurno;
    private boolean validaHoasTrab;
    private boolean validaConsulVsCitas;
    private boolean validaNumRecetas = true;
    private boolean validaDiasInca = true;
    private boolean urgencia = true;
    private boolean diasInca = true;
    private boolean edicion = false;
    private boolean editable;
    private boolean editableDiag;
    private boolean pvez = false;
    private boolean apMat = false;
    private boolean apareceRiesgoTrabajo = true;

    private Integer contador;
    private Integer auxConsultasNo;
    private Integer posicion;
    private Integer campo;
    private Integer diagPrin = 0;
    private Integer diagAdi = 0;
    private Integer diagCom = 0;
    private Integer diagAcc = 0;

    private String color;
    private String colorNss;
    private String derecho;
    private String sexo;
    private String anno;
    private String regimen;
    private String valNombreCom;
    private String foco;

    private boolean fechaNac = false;
    private boolean sem = false;
    private boolean ann = false;

    //Especialidades de Urgencias
    private static final String CVE_URGENCIAS = "5000";
    private static final String CVE_TOCO_CIRUGIA = "A600";
    private static final String CVE_URGENCIAS_PRIMER_CONTACTO = "5001";
    private static final String CVE_URGENCIAS_OBSERVACION_INTERMEDIA = "5002";
    private static final String CVE_URGENCIAS_OFTALMOLOGICAS = "5003";

    //Turnos
    private static final String TURNO_MATUTINO = "1";
    private static final String TURNO_VESPERTINO = "2";
    private static final String TURNO_NOCTURNO = "3";
    private static final String JORNADA_ACUMULADA = "4";


    // binding
    private InputMask fechaPf;
    private InputText matriculaMedico;
    private InputText nombreMedico;
    private AutoComplete especialidadAcPf;
    private SelectOneMenu tipoPrestador;
    private AutoComplete consultorioAcPf;
    private InputMask horasTrabajadas;
    private SelectOneMenu turno;
    private InputText consultasNoOtorgadas;
    private InputText citasNoCumplidas;
    private InputText nss;
    private InputText agMeDerechoPf;
    private InputText agMeSexoPf;
    private InputText agMeAnioPf;
    private AutoComplete agMeRegAcPf;
    private InputText nombre;
    private InputText apePaterno;
    private InputText apeMaterno;
    private InputText edad;
    private SelectOneMenu primeraVez;
    private SelectOneMenu citado;
    private SelectOneMenu pase;
    private InputText recetas;
    private InputText diasIncapacidad;
    private SelectOneMenu visitas;
    private AutoComplete metodoPpfAcPf;
    private InputText cantidadMpf;
    private SelectOneMenu accidentesLesiones;
    private SelectOneMenu riesgosTrabajo;
    private SelectOneMenu cveTipoUrgencia;

    private InputText diagnosticoPrincipal;
    private InputText desDiagPrin;
    private SelectOneMenu cveInforAdicPrin;
    private InputText cveTipoPrin;
    private InputMask fecCirugiaPrin;

    private InputText diagnosticoAdicional;
    private InputText desDiagAdic;
    private SelectOneMenu primeraVezDiagAdic;
    private SelectOneMenu cveInforAdicAdic;
    private SelectOneMenu cveTipoAdic;
    private InputMask fecCirugiaAdic;

    private InputText diagnosticoComplemento;
    private InputText desDiagComp;
    private SelectOneMenu primeraVezDiagComp;
    private SelectOneMenu cveInforAdicComp;
    private SelectOneMenu cveTipoComp;
    private InputMask fecCirugiaComp;

    private InputText procedimientoPrincipal;
    private InputText desProcPrin;
    private InputText procedimientoAdicional;
    private InputText desProcAdic;
    private InputText procedimientoComplemento;
    private InputText desProcComp;
    private InputMask cveMorfologia;
    private String desMorfologia;

    private Boolean paraEdicion = Boolean.FALSE;

    private boolean editableRiesgoTrabajo;
    private Boolean morfologiaVisible;
    private Integer edadEnSemanas;

    private Integer cveTipoInformacionAdicDiagP;
    private Integer cveTipoInformacionAdicDiagA;
    private Integer cveTipoInformacionAdicDiagC;
    private Integer numIntento;


    private InputText numBeneficiario;

    //Repetidos
    private String dxPOriginal;
    private Boolean esRepetido = Boolean.FALSE;


    private List<String> listaSupervicionEmbarazo = new ArrayList<String>(Arrays.asList("Z340", "Z348", "Z349", "Z34X",
            "Z350", "Z351", "Z352", "Z353", "Z354", "Z355", "Z356", "Z357", "Z358", "Z359", "Z35X"));

    private List<String> listaParamedicas = new ArrayList<String>(
            Arrays.asList("6201", "6301", "6401", "6601", "6901", "7001", "9801"));
    private boolean verMetodoPlanFamiliar = false;
    private List<SicInformacionAdicionalVIH> listaInfAdicionalVIH = new ArrayList<SicInformacionAdicionalVIH>();

    private InputText cveCD4PBinding;
    private InputText cveCD4ABinding;
    private InputText cveCD4CBinding;
    private InputText cveTipoEmbarazo;
    private InputText cveTipoEmbarazoAdicional;
    private InputText cveTipoEmbarazoComplementario;

    //Campos que aparecen dinamicamente

    private boolean muestraTipoUrgencia;
    private boolean muestraCitado;
    private boolean muestraPase;
    private boolean muestraRecetas;
    private boolean muestraVisita;
    private boolean muestraRiesgoTrabajo;

    @PostConstruct
    private void inicio() {
    	logger.info("Inicia inicio()");
        iniciarBinding();
        cargaInicial();
        listadoEspecialidad = catalogosService.llenaComboEspeUnidad(
                objetosSs.getUsuarioDetalleDto().getCvePresupuestal(), EnumModuloAtencion.CONSULTA_EXTERNA.getClave());
        listadoTurnos = catalogosService.llenaComboTurnos();
        listadoTipoPrestador = catalogosService.llenaComboTipoPrestador();
        cargaListadosAutocomplete();
        //RequestContext.getCurrentInstance().execute("PF('dlgPacientes').hide();");
        logger.info("Termina inicio()");
    }

    public void cargaInicial() {
        contador = 0;// Cargar por consulta
        posicion = 1;
        campo = 0;
        foco = "form:idFecha";// fechaPf.getClientId();

        desBotonAgregar = true;
        listadoEspecialidad = new ArrayList<CatalogoDto>();
        listadoConsultorio = new ArrayList<CatalogoDto>();
        listadoTipoPrestador = new ArrayList<CatalogoDto>();
        listadoTurnos = new ArrayList<CatalogoDto>();
        listadoAgreMed = new ArrayList<CatalogoDto>();
        listadoPases = new ArrayList<CatalogoDto>();
        listadoMetPPF = new ArrayList<CatalogoDto>();
        listadoAccLesiones = new ArrayList<CatalogoDto>();
        listadoRiesgos = new ArrayList<CatalogoDto>();
        listadoTipUrgencia = new ArrayList<CatalogoDto>();
        lista1Vez = new ArrayList<CatalogoDto>();
        listaInfoAdicional = new ArrayList<SdInformacionAdicional>();
        listaTipoDiarrea = new ArrayList<SdTipoDiarrea>();
        listaEspecialidadStr = new ArrayList<String>();
        listaConsultorioStr = new ArrayList<String>();
        listaRegimenStr = new ArrayList<String>();
        listaMetPpfStr = new ArrayList<String>();

        deshabilitadoMedS = false;
        deshabilitadoMedA = false;
        deshabilitadoMedR = true;
        deshabilitadoMed = false;
        deshabilitado = false;
        deshabilitadoA = false;
        deshabilitadoR = true;
        deshabilitadoAgre = false;
        deshabilitadoAgreA = false;
        deshabilitadoAgreR = true;
        deshabilitadoNom = false;
        deshabilitadoNomA = false;
        deshabilitadoNomR = true;
        datosDerecho = true;
        datosUniAds = true;
        validaFecha = false;
        validaMatricula = false;
        validaEspecialidad = false;
        validaTipoPrestado = false;
        validaConsultorio = false;
        validaTurno = false;
        validaHoasTrab = false;
        validaConsulVsCitas = false;
        valNombreCom = "";
        inhabilitarDxPx();

        //logger.debug("objetosSs.getColorHeader():" + objetosSs.getColorHeader());

        if (objetosSs.getConsultaExternaDtoSesion() != null) {
            if (objetosSs.getBusquedaCex()) {
                paraEdicion = Boolean.TRUE;


            } else {
                paraEdicion = Boolean.FALSE;
            }
            objetosSs.setBusquedaCex(Boolean.FALSE);

            consulExtDto = objetosSs.getConsultaExternaDtoSesion();
            consulExtDto = consultaExternaService.buscarAtenEdicion(consulExtDto);


            cargarBilding(consulExtDto);
            if (paraEdicion) {
                muestraCampos();
                if (diagnosticoPrincipal.getValue() != null) {
                    //Conservar diagnóstico Principal
                    dxPOriginal = diagnosticoPrincipal.getValue().toString();
                } else {
                    dxPOriginal = "";
                }

            }

            if (objetosSs.getConsultaExternaDtoSesion().getEditable() == Boolean.TRUE) {
                // Editable
                edicion = true;
                contador = consulExtDto.getNumConsulta();
                derecho = consulExtDto.getAgregadoMedico().substring(0, 1);
                sexo = consulExtDto.getAgregadoMedico().substring(1, 2);
                anno = consulExtDto.getAgregadoMedico().substring(2, 6);
                regimen = consulExtDto.getAgregadoMedico().substring(6);
                consulExtDto.setNumTipoServicio(objetosSs.getConsultaExternaDtoSesion().getNumTipoServicio());
                cveTipoInformacionAdicDiagP = consulExtDto.getCveTipoInfoAdDiagPrin();
                cveTipoInformacionAdicDiagA = consulExtDto.getCveTipoInfoAdDiagAdic();
                cveTipoInformacionAdicDiagC = consulExtDto.getCveTipoInfoAdDiagCompl();


                if (derecho.equals("1") && regimen.toUpperCase().equals("OR")) {
                    diasInca = true;
                    muestraRiesgoTrabajo = true;
                } else {
                    diasInca = false;
                    muestraRiesgoTrabajo = false;
                }

                atenion = true;
                listadoAgreMed = catalogosService.llenaComboAgreMed();
                listaRegimenStr = cargaListaString(listadoAgreMed);
                listadoPases = catalogosService.llenaComboPases();
                listadoAccLesiones = catalogosService.llenaComboAccLesiones();
                listadoRiesgos = catalogosService.llenaComboRiesgos();
                listadoTipUrgencia = catalogosService.llenaComboTipoUrgencia();
                foco = "form:primeraVez";
                metodoPf = true;


                if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA
                        .getClave()) {
                    consulExtDto.setPrimeraVez(1);
                    primeraVez.setValue(1);
                }

                nss.setDisabled(Boolean.TRUE);
                agMeDerechoPf.setDisabled(Boolean.TRUE);
                agMeSexoPf.setDisabled(Boolean.TRUE);
                agMeAnioPf.setDisabled(Boolean.TRUE);
                agMeRegAcPf.setDisabled(Boolean.TRUE);

                if (diagnosticoAdicional.getValue() != null) {
                    diagnosticoAdicional.setDisabled(Boolean.FALSE);
                    primeraVezDiagAdic.setDisabled(Boolean.FALSE);
                }
                if (diagnosticoComplemento.getValue() != null) {
                    diagnosticoComplemento.setDisabled(Boolean.FALSE);
                    primeraVezDiagComp.setDisabled(Boolean.FALSE);
                }

                if (procedimientoPrincipal.getValue() == null) {
                    procedimientoPrincipal.setDisabled(Boolean.TRUE);
                }

                if (procedimientoAdicional.getValue() != null) {
                    procedimientoAdicional.setDisabled(Boolean.FALSE);
                }
                if (procedimientoComplemento.getValue() != null) {
                    procedimientoComplemento.setDisabled(Boolean.FALSE);
                }

                if (muestraRiesgoTrabajo) {
                    if (riesgosTrabajo.getValue() != null) {
                        editableRiesgoTrabajo = false;
                    } else {
                        editableRiesgoTrabajo = true;
                    }
                }

                if (this.recetas.getValue() == null) {
                    this.recetas.setDisabled(Boolean.TRUE);
                }

                if (this.diasIncapacidad.getValue() == null) {
                    this.diasIncapacidad.setDisabled(Boolean.TRUE);
                }

                Integer edadPaciente = Integer.parseInt(edad.getValue().toString());
                listadoMetPPF = catalogosService.llenaComboMetPPFXSexoXEdad(sexo.toUpperCase().equals("M") ? 1 : 2, edadPaciente);
                if (consulExtDto.getCantidadMpf() != null) {
                    metodoPfCan = true;
                }

                if (!listadoMetPPF.isEmpty()) {
                    verMetodoPlanFamiliar = true;
                    if (metodoPpfAcPf.getValue() != null) {
                        listaMetPpfStr = cargaListaString(listadoMetPPF);
                        for (String metodo : listaMetPpfStr) {
                            if (metodo.startsWith(metodoPpfAcPf.getValue().toString())) {
                                metodoPpfAcPf.setValue(metodo);
                            }
                        }
                    } else {
                        metodoPpfAcPf.setDisabled(Boolean.TRUE);
                    }
                }

                objetosSs.setConsultaExternaDtoSesion(null);
                campo = 28;
            } else {
                objetosSs.setConsultaExternaDtoSesion(null);
            }

        } else {
            editable = false;
            editableDiag = false;
            consulExtDto = new ConsultaExternaDto();
        }
    }

    public void cargaListadosAutocomplete() {
        listaEspecialidadStr = cargaListaString(listadoEspecialidad);
    }

    public void inhabilitarDxPx() {
        diagnosticoAdicional.setDisabled(Boolean.TRUE);
        primeraVezDiagAdic.setDisabled(Boolean.TRUE);
        diagnosticoComplemento.setDisabled(Boolean.TRUE);
        primeraVezDiagComp.setDisabled(Boolean.TRUE);
        procedimientoAdicional.setDisabled(Boolean.TRUE);
        procedimientoComplemento.setDisabled(Boolean.TRUE);
    }

    public void cargarBilding(ConsultaExternaDto consulExtDto) {
        fechaPf.setValue(consulExtDto.getFecha());
        matriculaMedico.setValue(consulExtDto.getMatriculaMedico());
        nombreMedico.setValue(consulExtDto.getNombreMedico());
        especialidadAcPf.setValue(consulExtDto.getCveEspecialidad());
        tipoPrestador.setValue(consulExtDto.getTipoPrestador());
        consultorioAcPf.setValue(consulExtDto.getConsultorio());
        horasTrabajadas.setValue(consulExtDto.getHorasTrabajadas());
        turno.setValue(consulExtDto.getTurno());
        consultasNoOtorgadas.setValue(consulExtDto.getConsultasNoOtorgadas());
        citasNoCumplidas.setValue(consulExtDto.getCitasNoCumplidas());
        nss.setValue(consulExtDto.getNss());
        agMeDerechoPf.setValue(consulExtDto.getAgregadoMedico().substring(0, 1));
        agMeSexoPf.setValue(consulExtDto.getAgregadoMedico().substring(1, 2));
        agMeAnioPf.setValue(consulExtDto.getAgregadoMedico().substring(2, 6));
        agMeRegAcPf.setValue(consulExtDto.getAgregadoMedico().substring(6));
        nombre.setValue(consulExtDto.getNombre());
        apePaterno.setValue(consulExtDto.getApePaterno());
        apeMaterno.setValue(consulExtDto.getApeMaterno());
        edad.setValue(consulExtDto.getEdad());
        primeraVez.setValue(consulExtDto.getPrimeraVez());
        citado.setValue(consulExtDto.getCitado());
        pase.setValue(consulExtDto.getPase());
        recetas.setValue(consulExtDto.getRecetas());
        diasIncapacidad.setValue(consulExtDto.getDiasIncapacidad());
        visitas.setValue(consulExtDto.getVisitas());
        metodoPpfAcPf.setValue(consulExtDto.getMetodoPpf());
        cantidadMpf.setValue(consulExtDto.getCantidadMpf());
        accidentesLesiones.setValue(consulExtDto.getAccidentesLesiones());
        riesgosTrabajo.setValue(consulExtDto.getRiesgosTrabajo());
        cveTipoUrgencia.setValue(consulExtDto.getCveTipoUrgencia());
        diagnosticoPrincipal.setValue(consulExtDto.getDiagnosticoPrincipal());
        desDiagPrin.setValue(consulExtDto.getDesDiagPrin());
        cveInforAdicPrin.setValue(consulExtDto.getCveInforAdicPrin());
        cveTipoPrin.setValue(consulExtDto.getCveTipoPrin());
        fecCirugiaPrin.setValue(consulExtDto.getFecCirugiaPrin());
        diagnosticoAdicional.setValue(consulExtDto.getDiagnosticoAdicional());
        desDiagAdic.setValue(consulExtDto.getDesDiagAdic());
        primeraVezDiagAdic.setValue(consulExtDto.getPrimeraVezDiagAdic());
        cveInforAdicAdic.setValue(consulExtDto.getCveInforAdicAdic());
        cveTipoAdic.setValue(consulExtDto.getCveTipoAdic());
        fecCirugiaAdic.setValue(consulExtDto.getFecCirugiaAdic());
        diagnosticoComplemento.setValue(consulExtDto.getDiagnosticoComplemento());
        desDiagComp.setValue(consulExtDto.getDesDiagComp());
        primeraVezDiagComp.setValue(consulExtDto.getPrimeraVezDiagComp());
        cveInforAdicComp.setValue(consulExtDto.getCveInforAdicComp());
        cveTipoComp.setValue(consulExtDto.getCveTipoComp());
        fecCirugiaComp.setValue(consulExtDto.getFecCirugiaComp());
        procedimientoPrincipal.setValue(consulExtDto.getProcedimientoPrincipal());
        desProcPrin.setValue(consulExtDto.getDesProcPrin());
        procedimientoAdicional.setValue(consulExtDto.getProcedimientoAdicional());
        desProcAdic.setValue(consulExtDto.getDesProcAdic());
        procedimientoComplemento.setValue(consulExtDto.getProcedimientoComplemento());
        desProcComp.setValue(consulExtDto.getDesProcComp());
        cveMorfologia.setValue(consulExtDto.getMorfologia());
    }

    public void iniciarBinding() {
        fechaPf = new InputMask();
        matriculaMedico = new InputText();
        nombreMedico = new InputText();
        especialidadAcPf = new AutoComplete();
        tipoPrestador = new SelectOneMenu();
        consultorioAcPf = new AutoComplete();
        horasTrabajadas = new InputMask();
        turno = new SelectOneMenu();
        consultasNoOtorgadas = new InputText();
        citasNoCumplidas = new InputText();
        nss = new InputText();
        agMeDerechoPf = new InputText();
        agMeSexoPf = new InputText();
        agMeAnioPf = new InputText();
        agMeRegAcPf = new AutoComplete();
        nombre = new InputText();
        apePaterno = new InputText();
        apeMaterno = new InputText();
        edad = new InputText();
        primeraVez = new SelectOneMenu();
        citado = new SelectOneMenu();
        pase = new SelectOneMenu();
        recetas = new InputText();
        diasIncapacidad = new InputText();
        visitas = new SelectOneMenu();
        metodoPpfAcPf = new AutoComplete();
        cantidadMpf = new InputText();
        accidentesLesiones = new SelectOneMenu();
        riesgosTrabajo = new SelectOneMenu();
        cveTipoUrgencia = new SelectOneMenu();
        diagnosticoPrincipal = new InputText();
        desDiagPrin = new InputText();
        cveInforAdicPrin = new SelectOneMenu();
        cveTipoPrin = new InputText();
        fecCirugiaPrin = new InputMask();
        diagnosticoAdicional = new InputText();
        desDiagAdic = new InputText();
        primeraVezDiagAdic = new SelectOneMenu();
        cveInforAdicAdic = new SelectOneMenu();
        cveTipoAdic = new SelectOneMenu();
        fecCirugiaAdic = new InputMask();
        diagnosticoComplemento = new InputText();
        desDiagComp = new InputText();
        primeraVezDiagComp = new SelectOneMenu();
        cveInforAdicComp = new SelectOneMenu();
        cveTipoComp = new SelectOneMenu();
        fecCirugiaComp = new InputMask();
        procedimientoPrincipal = new InputText();
        desProcPrin = new InputText();
        procedimientoAdicional = new InputText();
        desProcAdic = new InputText();
        procedimientoComplemento = new InputText();
        desProcComp = new InputText();
        cveMorfologia = new InputMask();
        desMorfologia = new String();
    }

    public void cargaElementosResultado(ConsultaExternaDto consulExtDtoRes) {
        consulExtDto.setFecha(consulExtDtoRes.getFecha());
        consulExtDto.setEspecialidad(consulExtDtoRes.getDesEspecialidad());
        // consulExtDto.set("descripcion especialidad ");
        consulExtDto.setMatriculaMedico(consulExtDtoRes.getMatriculaMedico());
        consulExtDto.setNumConsulta(consulExtDtoRes.getNumConsulta());
        consulExtDto.setNss(consulExtDtoRes.getNss());
        consulExtDto.setNombre(consulExtDtoRes.getNombre());
        consulExtDto.setAgregadoMedico(consulExtDtoRes.getAgregadoMedico());
        consulExtDto.setCodigoCie(consulExtDtoRes.getCodigoCie());
        consulExtDto.setDesDiagPrin(consulExtDtoRes.getDesDiagPrin());
        consulExtDto.setPrimeraVez(Integer.valueOf(consulExtDtoRes.getOcasionServicio()));
    }

    public void validaFecha() {
        if (this.fechaPf.getValue() != null
                && !this.fechaPf.getValue().toString().isEmpty()
                && !this.fechaPf.getValue().toString().contains("_")) {
            Date fecAux = (Date) fechaPf.getValue();
            if (fecAux.after(this.getFechaActual())) {
                validaFecha = false;
                foco = fechaPf.getClientId();
                mensajeErrorConParametros("me07_fecha_atencion_menor",
                        "Atenci\u00F3n");
            } else {
                String cveConstante = "PERIODO_LIBERACION";
                String fechaPeriodo = null;
                fechaPeriodo = consultaExternaService.buscarPeriodoLiberacion(cveConstante);
                SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaComp = null;
                try {
                    fechaComp = sdformat.parse(fechaPeriodo);
                } catch (ParseException e) {
                    logger.error("error al convertir la fecha" + e);
                    e.printStackTrace();
                }
                if (fecAux.before(fechaComp)) {
                    fechaPf.resetValue();
                    validaFecha = false;
                    foco = fechaPf.getClientId();
                    agregarError("me09_fecha_menor_liberacion");
                } else {
                    String cvePresupuestal = objetosSs.getUsuarioDetalleDto().getCvePresupuestal();
                    consulExtDto.setFecha((Date) fechaPf.getValue());
                    posicion = 2;
                    validaFecha = true;
                    foco = matriculaMedico.getClientId();
                    Calendar fecha = Calendar.getInstance();
                    fecha.setTime((Date) fechaPf.getValue());
                    Integer fechaDia = fecha.get(Calendar.DAY_OF_MONTH);
                    if (consultaExternaService.validaPeriodo(
                            fecha.get(Calendar.YEAR),
                            String.valueOf(fecha.get(Calendar.MONTH) + 1),
                            consulExtDto.getFecha(), cvePresupuestal, fechaDia)) {
                        posicion = 2;
                        validaFecha = true;
                        foco = matriculaMedico.getClientId();
                    } else {
                        validaFecha = false;
                        agregarError("me09_fuera_periodo");
                    }
                }
            }
        } else {
            foco = fechaPf.getClientId();
            validaFecha = false;
            mensajeErrorConParametros("mecx01_requerido", "la Fecha");
        }
    }

    public void validaEspecialidad(CatalogoDto catalogoDto) {
        if (this.especialidadAcPf.getValue() != null) {
            consulExtDto.setCveEspecialidad(catalogoDto.getClaveDes());
            consulExtDto.setEspecialidadSelc(catalogoDto.getClave().toString());
            consulExtDto.setNumTipoServicio(catalogoDto.getNumTipoServicio());
            if (consulExtDto.getNumTipoServicio().intValue() == EnumEspecialidades.URGENCIA.getClave()) {
                urgencia = false;
            } else {
                urgencia = true;
            }
            listadoConsultorio = catalogosService.llenaComboConsultorios(
                    objetosSs.getUsuarioDetalleDto().getCvePresupuestal(), consulExtDto.getNumTipoServicio());
            listaConsultorioStr = cargaListaString(listadoConsultorio);
            posicion = 5;
            foco = consultorioAcPf.getClientId();
            validaEspecialidad = true;
        } else {
            validaEspecialidad = false;
            foco = especialidadAcPf.getClientId();
            agregarError("me01_requerido");
        }
    }

    public List<String> completeTextEspecialidad(String query) {
        List<String> resultado = new ArrayList<String>();
        resultado = completeTextComponente(query, 4, listaEspecialidadStr);
        return resultado;
    }

    public void onItemSelectEspecialidad(SelectEvent event) {
        CatalogoDto objeto = null;
        objeto = itemSeleccionado(event, listaEspecialidadStr, especialidadAcPf, listadoEspecialidad);
        consultorioAcPf.resetValue();
        consulExtDto.setConsultorio(null);
        consultorioAcPf.setValue("");
        validaEspecialidad(objeto);
    }

    public void validaAutocEspecialidad() {
        CatalogoDto objeto = null;
        objeto = validaAutoComplete(especialidadAcPf, 4, listaEspecialidadStr, listadoEspecialidad);
        consultorioAcPf.resetValue();
        consulExtDto.setConsultorio(null);
        consultorioAcPf.setValue("");
        validaEspecialidad(objeto);
    }

    public void validaMatMedico() {
    	logger.info("Inicia validaMatMedico()");
        deshabilitadoMedS = false;
        deshabilitadoMedA = false;
        deshabilitadoMedR = false;
        deshabilitadoMed = true;
        consulExtDto.setNombreMedico(null);
        consulExtDto.setNombresMedi(null);
        consulExtDto.setaPaternoMedi(null);
        consulExtDto.setaMaternoMedi(null);
        consulExtDto.setMatriculaMedico(null);

        RequestContext context = RequestContext.getCurrentInstance();

        if (matriculaMedico.getValue().toString().length() > 5 && esNumero(matriculaMedico.getValue().toString())) {
            deshabilitadoMed = false;
            consulExtDto.setMatriculaMedico(matriculaMedico.getValue().toString());
            try {
                SiapDto responseSiap = accederService.consultarSiap(matriculaMedico.getValue().toString(),
                        objetosSs.getUsuarioDetalleDto().getCvePresupuestal().substring(0, 2));
                //SiapDto responseSiap=null;
                if (responseSiap != null) {

                    if (responseSiap.getEncontrado()) {
                        consulExtDto.setNombreMedico(responseSiap.getNombres());

                        if (responseSiap.getaPaterno() != null) {
                            consulExtDto.setNombreMedico(responseSiap.getNombres().concat(" ")
                                    .concat(responseSiap.getaPaterno()));
                        }
                        if (responseSiap.getaMaterno() != null) {
                            consulExtDto.setNombreMedico(responseSiap.getNombres().concat(" ")
                                    .concat(responseSiap.getaPaterno()).concat(" ").concat(responseSiap.getaMaterno()));
                        }


                        consulExtDto.setNombresMedi(responseSiap.getNombres());
                        consulExtDto.setaPaternoMedi(responseSiap.getaPaterno());
                        consulExtDto.setaMaternoMedi(responseSiap.getaMaterno());

                        this.nombreMedico.setValue(consulExtDto.getNombreMedico());
                        deshabilitadoMedS = true;

                    } else { // responde siap pero viene vacÃ­o

                        //Si siap viene vacÃ­o, aqui generamos la coleccion
                        //Verificamos si existe la matrÃ­cula
                        controlMedicoEncabezadoDto = consultaExternaService.buscar(matriculaMedico.getValue().toString());
                        if (controlMedicoEncabezadoDto != null) { //En caso que exista dentro de la colecciÃ³n
                            numIntento = controlMedicoEncabezadoDto.getNumIntento();
                            //Si estÃ¡ dada de alta pero su valor de nÃºmero intentos es cero
                            if (numIntento == 0) {
                                context.execute("PF('modalVerifiqueDialog').show();");
                                ControlMedicoEncabezadoDto controlMedicoEncabezadoDtoActualiza = new ControlMedicoEncabezadoDto();
                                controlMedicoEncabezadoDtoActualiza.setCveMatricula(matriculaMedico.getValue().toString());
                                controlMedicoEncabezadoDtoActualiza.setNumIntento(numIntento + 1);
                                consultaExternaService.actualizar(controlMedicoEncabezadoDtoActualiza);
                                this.matriculaMedico.setValue(null);
                                context.execute("document.getElementById('form:idMatricula').focus();");
                            } else if (numIntento == 1) { //Si estÃ¡ dada de alta pero su valor de nÃºmero de intentos es 1
                                context.execute("PF('modalConfirmeDialog').show();");
                                context.execute("document.getElementById('form:idMatricula').focus();");
                            }
                        } else {
                            // en caso de que no exista dentro de la colecciÃ³n
                            context.execute("PF('modalVerifiqueDialog').show();");
                            ControlMedicoEncabezadoDto controlMedicoEncabezadoDtInsert = new ControlMedicoEncabezadoDto();
                            controlMedicoEncabezadoDtInsert.setCveMatricula(matriculaMedico.getValue().toString());
                            controlMedicoEncabezadoDtInsert.setCvePresupuestal(objetosSs.getUsuarioDetalleDto().getCvePresupuestal());
                            controlMedicoEncabezadoDtInsert.setDesUnidadMedica(objetosSs.getUsuarioDetalleDto().getDesUnidadMedicaAct());
                            controlMedicoEncabezadoDtInsert.setNumIntento(1);
                            consultaExternaService.insertar(controlMedicoEncabezadoDtInsert);
                            this.matriculaMedico.setValue(null);
                            context.execute("document.getElementById('form:idMatricula').focus();");
                        }
                        this.nombreMedico.setValue(null);
                        deshabilitadoMedR = true;
                    }
                } else { // No responde el SIAP
                    consulExtDto.setNombresMedi("Servicio SIAP no disponible");
                    consulExtDto.setaPaternoMedi("Servicio SIAP no disponible");
                    consulExtDto.setaMaternoMedi("Servicio SIAP no disponible");
                    agregarError("msg_SIAP_no_disponible");
                    this.nombreMedico.setValue("Servicio SIAP no disponible");
                    deshabilitadoMedA = true;

                }
            } catch (Exception e) {
                e.printStackTrace();
                deshabilitadoMedA = true;
            }
            validaMatricula = true;
            posicion = 3;
            foco = tipoPrestador.getClientId();
        } else {
            foco = matriculaMedico.getClientId();
            validaMatricula = false;
            mensajeErrorConParametros("me18_valor_incorrecto", "Matr\u00EDcula");
        }
        logger.info("Termina validaMatMedico()");
    }

    public void validaMatMedicoConfirmar() {
        consulExtDto.setNombresMedi("******** No localizado en SIAP *******");
        consulExtDto.setaPaternoMedi("******** No localizado en SIAP *******");
        consulExtDto.setaMaternoMedi("******** No localizado en SIAP *******");
        this.nombreMedico.setValue("Matrícula no localizada en SIAP");
        ControlMedicoEncabezadoDto controlMedicoEncabezadoDtoActualiza = new ControlMedicoEncabezadoDto();
        controlMedicoEncabezadoDtoActualiza.setCveMatricula(matriculaMedico.getValue().toString());
        controlMedicoEncabezadoDtoActualiza.setNumIntento(0);
        consultaExternaService.actualizar(controlMedicoEncabezadoDtoActualiza);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("document.getElementById('form:prestador_focus').focus();");
    }

    public void validaMatMedicoCancelar() {
        this.nombreMedico.setValue(null);
        this.matriculaMedico.setValue(null);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("document.getElementById('form:idMatricula').focus();");
    }

    public void validaPrestador() {
        if (this.tipoPrestador != null) {
            posicion = 4;
            foco = especialidadAcPf.getClientId();
            validaTipoPrestado = true;
            consulExtDto.setTipoPrestador(this.tipoPrestador.getValue().toString());
        } else {
            validaTipoPrestado = false;
        }
    }

    public void validaHorasTrab() {
        if (this.horasTrabajadas != null && !this.horasTrabajadas.getValue().toString().isEmpty()) {
            if (ReglasNegocio.validaHorasTrabajadas(consulExtDto.getNumTipoServicio(),
                    this.horasTrabajadas.getValue().toString(), consulExtDto.getTurno())) {
                validaHoasTrab = true;
                posicion = 8;
                foco = consultasNoOtorgadas.getClientId();
                consulExtDto.setHorasTrabajadas(this.horasTrabajadas.getValue().toString());
            } else {
                validaHoasTrab = false;
                horasTrabajadas.resetValue();
                consulExtDto.setHorasTrabajadas(null);
                if (consulExtDto.getNumTipoServicio() == 4 || (consulExtDto.getTurno().equals(TURNO_NOCTURNO) && consulExtDto.getNumTipoServicio() != 5)) {
                    agregarError("me08_horas_rango1");

                } else {
                    if (consulExtDto.getNumTipoServicio() == 5 || consulExtDto.getNumTipoServicio() == 2 || consulExtDto.getNumTipoServicio() == 3) {
                        agregarError("me08_horas_rango");
                    }
                }
            }

        } else {
            mensajeErrorConParametros("mecx01_requerido", "Horas Trabajadas");
        }
    }


    public void validaConsultorio(CatalogoDto objeto) {
        if (this.consultorioAcPf != null && objeto != null) {
            posicion = 6;
            foco = turno.getClientId();
            validaConsultorio = true;
        } else {
            validaConsultorio = false;
        }
        if (objeto != null) {
            if (this.consultorioAcPf.getValue() != null && !this.consultorioAcPf.getValue().toString().isEmpty()) {
                consulExtDto.setConsultorio(objeto.getClaveDes());
            }
        }
    }
    
    public List<String> completeTextConsultorios(String query) {
        List<String> resultado = new ArrayList<String>();
        resultado = completeTextComponente(query, 3, listaConsultorioStr);
        return resultado;
    }

    public void onItemSelectConsultorios(SelectEvent event) {
        CatalogoDto objeto = null;
        objeto = itemSeleccionado(event, listaConsultorioStr, consultorioAcPf, listadoConsultorio);
        validaConsultorio(objeto);
    }

    public void validaAutocConsultorios() {
        CatalogoDto objeto = null;
        objeto = validaAutoComplete(consultorioAcPf, 3, listaConsultorioStr, listadoConsultorio);
        validaConsultorio(objeto);
    }

    public void validaTurno() {
        if (this.turno != null) {
            posicion = 7;
            foco = horasTrabajadas.getClientId();
            validaTurno = true;
        } else {
            validaTurno = false;
        }
        consulExtDto.setTurno(turno.getValue().toString());
    }

    public void validaConsultasNo() {
        validaConsulVsCitas = true;
        if (this.consultasNoOtorgadas != null && !this.consultasNoOtorgadas.getValue().toString().isEmpty()) {
            if (esNumero(this.consultasNoOtorgadas.getValue().toString())) {
                consulExtDto.setConsultasNoOtorgadas(Integer.parseInt(consultasNoOtorgadas.getValue().toString()));

                switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
                    case 2:// URGENCIA
                        posicion = 10;
                        foco = "idBtnAgregar";
                        desBotonAgregar = false;
                        break;

                    case 3:// URGENCIA_CIRUGIA
                    case 4:// PARAMEDICO
                        posicion = 9;
                        foco = citasNoCumplidas.getClientId();
                        break;

                    default:
                        posicion = 9;
                        foco = citasNoCumplidas.getClientId();
                        break;
                }
            } else {
                foco = consultasNoOtorgadas.getClientId();
                validaConsulVsCitas = false;
                mensajeErrorConParametros("me18_valor_incorrecto", "Consultas No otorgadas");
            }
        } else {
            validaConsulVsCitas = false;
            mensajeErrorConParametros("mecx01_requerido", "Consultas No otorgadas");
        }
    }

    public void validaConsulVsCitas() {
        if (this.citasNoCumplidas.getValue() != null) {

            if (this.consultasNoOtorgadas != null && !this.consultasNoOtorgadas.getValue().toString().isEmpty()) {
                if (esNumero(this.consultasNoOtorgadas.getValue().toString())) {
                    if (ReglasNegocio.validaConsulVsCitas(Integer.parseInt(consultasNoOtorgadas.getValue().toString()),
                            Integer.parseInt(citasNoCumplidas.getValue().toString()))) {
                        consulExtDto.setCitasNoCumplidas(Integer.parseInt(citasNoCumplidas.getValue().toString()));
                        validaConsulVsCitas = true;
                        posicion = 10;
                        if (validaFecha && validaMatricula && validaEspecialidad && validaConsultorio && validaTipoPrestado
                                && validaTurno && validaHoasTrab && validaConsulVsCitas) {
                            foco = "idBtnAgregar";
                            desBotonAgregar = false;
                        } else {
                            desBotonAgregar = true;
                        }

                    } else {
                        validaConsulVsCitas = false;
                        desBotonAgregar = true;
                        agregarError("me10_val_cit_incorrecto");
                    }
                } else {
                    foco = citasNoCumplidas.getClientId();
                    validaConsulVsCitas = false;
                    mensajeErrorConParametros("me18_valor_incorrecto", "Citas no cumplidas");
                }
            } else {
                validaConsulVsCitas = false;
                mensajeErrorConParametros("mecx01_requerido", "Citas no cumplidas");
            }
        } else {
            validaConsulVsCitas = false;
            desBotonAgregar = true;
            agregarError("me10_val_cit_incorrecto");
        }
    }

    public void validaNss() {
        if (this.nss.getValue().toString().length() == 10) {
            foco = agMeDerechoPf.getClientId();
            consulExtDto.setNss(nss.getValue().toString());
            if (validaCamposDeRepetidos()) {
                logger.debug("---REGSITAR REPETIDO, LIMPIANDO CAMPOS");

            }
        } else {
            deshabilitado = false;
            colorNss = "FFFFFF";
            foco = nss.getClientId();
            consulExtDto.setNss(null);
            mensajeErrorConParametros("me18_valor_incorrecto", "NSS");
        }
    }

    public void validaAgMedDerecho() {
        if (this.getAgMeDerechoPf().getValue() != null && !this.getAgMeDerechoPf().getValue().toString().isEmpty()) {
            foco = this.getAgMeSexoPf().getClientId();
            validaAgregadoMedico();
        } else {
            foco = this.getAgMeDerechoPf().getClientId();
        }
    }

    public void validaAgMedSexo() {
        if (this.getAgMeSexoPf().getValue() != null && !this.getAgMeSexoPf().getValue().toString().isEmpty()) {
            if (consulExtDto.getCveEspecialidad().equals("A600")) {
                if (agMeSexoPf.getValue().toString().toUpperCase().equals("M")) {
                    nss.resetValue();
                    agMeDerechoPf.resetValue();
                    agMeSexoPf.setValue("F");
                    foco = nss.getClientId();
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sexo no válido para la especialidad.", "Sexo no válido para la especialidad."));
                } else {
                    foco = this.getAgMeAnioPf().getClientId();
                    validaAgregadoMedico();
                }
            } else {
                foco = this.getAgMeAnioPf().getClientId();
                validaAgregadoMedico();
            }

        } else {
            foco = this.getAgMeSexoPf().getClientId();

        }
    }

    public void validaAgMedAnio() {
        if (this.getAgMeAnioPf().getValue() != null && !this.getAgMeAnioPf().getValue().toString().isEmpty()) {
            String annos = this.getAgMeAnioPf().getValue().toString();

            Calendar fecha = Calendar.getInstance();
            Integer anioActual = fecha.get(Calendar.YEAR);

            if (Integer.parseInt(annos) > anioActual) {
                this.getAgMeAnioPf().setValue(null);
                foco = this.getAgMeAnioPf().getClientId();
                agregarError("me139_anio_fuera_de_rango");
            } else {
                if (annos.length() < 4) {
                    // mensajeErrorConParametros("me18_valor_incorrecto", "A�o");
                    foco = this.getAgMeAnioPf().getClientId();
                } else {
                    this.getAgMeAnioPf().setValue(annos);
                    foco = this.getAgMeRegAcPf().getClientId();
                    validaAgregadoMedico();
                }
            }

        } else {
            foco = this.getAgMeAnioPf().getClientId();
            mensajeErrorConParametros("mecx01_requerido", "Año");

        }
    }

    public void validaAgMedReg(CatalogoDto catalogo) {
        if (catalogo != null && !catalogo.toString().isEmpty()) {

            validaAgregadoMedico();
            getAgMeRegAcPf().setStyle("");
        } 
        //else {
            //componenteNoValido(getAgMeRegAcPf());
        //}
    }

    public List<String> completeTextRegimen(String query) {
        List<String> resultado = new ArrayList<String>();
        resultado = completeTextComponente(query, 2, listaRegimenStr);
        return resultado;
    }

    public void onItemSelectRegimen(SelectEvent event) {
        CatalogoDto objeto = null;
        objeto = itemSeleccionado(event, listaRegimenStr, agMeRegAcPf, listadoAgreMed);
        validaAgMedReg(objeto);
    }

    public void validaAutocRegimen() {
        CatalogoDto objeto = null;
        objeto = validaAutoComplete(agMeRegAcPf, 2, listaRegimenStr, listadoAgreMed);
        validaAgMedReg(objeto);
    }

    public void validaAgregadoMedico() {
        if (this.getAgMeDerechoPf().getValue() != null && !this.getAgMeDerechoPf().getValue().toString().isEmpty()
                && this.getAgMeSexoPf().getValue() != null && !this.getAgMeSexoPf().getValue().toString().isEmpty()
                && this.getAgMeAnioPf().getValue() != null && !this.getAgMeAnioPf().getValue().toString().isEmpty()
                && this.getAgMeRegAcPf().getValue() != null && !this.getAgMeRegAcPf().getValue().toString().isEmpty()) {

            validaAgregado();

        }
    }

    public void validaAgregado() {
    	logger.info("Inicia validaAgregado()");
        verMetodoPlanFamiliar = Boolean.FALSE;
        String cadena = "";
        boolean correcto = true;
        deshabilitadoAgre = false;
        deshabilitadoAgreA = false;
        deshabilitadoAgreR = false;
        dialogEdad = Boolean.FALSE;
        boolean conecto = true;
        edad.setValue(null);
        muestraRiesgoDeTrabajo();
        limpiaDiagnosticos();
        SicEspecialidad especialidad = consultaExternaService.buscarEspecialidad(consulExtDto.getCveEspecialidad());
        setEdadEnSemanas(null);

        if (agMeDerechoPf != null && !this.agMeDerechoPf.getValue().toString().isEmpty()) {
            if (consulExtDto.getAgregadoMedico() == null) {
                consulExtDto.setAgregadoMedico(agMeDerechoPf.getValue().toString());
            } else {
                cadena = agMeDerechoPf.getValue().toString().concat(consulExtDto.getAgregadoMedico().substring(1));
                consulExtDto.setAgregadoMedico(cadena);
            }
            if (agMeSexoPf != null && !this.agMeSexoPf.getValue().toString().isEmpty()) {
                // Regla: RN27 <Pacientes para Urgencias Toco - cirugía>
                if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA_CIRUGIA
                        .getClave()
                        || consulExtDto.getCveEspecialidad()
                        .equals(String.valueOf(EnumEspecialidades.GINECOLOGIA.getClave()))
                        || consulExtDto.getCveEspecialidad()
                        .equals(String.valueOf(EnumEspecialidades.OBSTETRICIA.getClave()))) {
                    agMeSexoPf.setValue("F");
                }
                if (consulExtDto.getAgregadoMedico().length() == 1) {
                    cadena = consulExtDto.getAgregadoMedico().concat(agMeSexoPf.getValue().toString().toUpperCase());
                    consulExtDto.setAgregadoMedico(cadena);
                } else {
                    cadena = consulExtDto.getAgregadoMedico().substring(0, 1)
                            .concat(agMeSexoPf.getValue().toString().toUpperCase())
                            .concat(consulExtDto.getAgregadoMedico().substring(2));
                    consulExtDto.setAgregadoMedico(cadena);
                }
                limpiaDiagnosticos();

                if (agMeAnioPf != null && !this.agMeAnioPf.getValue().toString().isEmpty()) {
                    logger.info("01");//***
                    if (agMeAnioPf.getValue().toString().length() == 4) {
                        logger.info("02");//***
                        if (consulExtDto.getAgregadoMedico().length() == 2) {
                            cadena = consulExtDto.getAgregadoMedico().concat(this.agMeAnioPf.getValue().toString());
                            consulExtDto.setAgregadoMedico(cadena);
                        } else {
                            cadena = consulExtDto.getAgregadoMedico().substring(0, 2)
                                    .concat(this.agMeAnioPf.getValue().toString())
                                    .concat(consulExtDto.getAgregadoMedico().substring(6));
                            consulExtDto.setAgregadoMedico(cadena);
                        }

                        // valida COMBO METODO PLANIFICACION FAMILIAR
                        int edad = calcularEdadAnnos(this.agMeAnioPf.getValue().toString(), consulExtDto.getFecha());
                        muestraMetodoPlanificacionFamiliar(edad);
                        /*Regla: RN27 <Intervalo de edad para  Urgencias Toco - cirugia>*/
                        logger.info("03 \n" +
                                "consulExtDto.getEspecialidadSelc: " + consulExtDto.getEspecialidadSelc() +
                                "especialidad.getNumEdadMinima: " + especialidad.getNumEdadMinima() +
                                "ReglasNegocio.validaEdadUrgenciasToco: " + ReglasNegocio.validaEdadUrgenciasToco(consulExtDto.getEspecialidadSelc(), especialidad.getNumEdadMinima(), +
                                especialidad.getNumEdadMaxima(), edad));//***
                        if (ReglasNegocio.validaEdadUrgenciasToco(consulExtDto.getEspecialidadSelc(), especialidad.getNumEdadMinima(),
                                especialidad.getNumEdadMaxima(), edad)) {

                            limpiaAcceder();
                            deshabilitadoAgre = false;
                            color = "FFFFFF";
                            foco = agMeAnioPf.getClientId();
                            agregarError("me70_edad_toco");
                        } else {
                            logger.info("04 agMeRegAcPf: " + agMeRegAcPf);//***
                            if (agMeRegAcPf != null) {
                                logger.info("05");//***
                                if (consulExtDto.getAgregadoMedico().length() == 8) {
                                    consulExtDto.setAgregadoMedico(consulExtDto.getAgregadoMedico().substring(0, 6));
                                    consulExtDto.setAgregadoMedico(
                                            consulExtDto.getAgregadoMedico().concat(agMeRegAcPf.getValue().toString()));
                                } else {
                                    consulExtDto.setAgregadoMedico(
                                            consulExtDto.getAgregadoMedico().concat(agMeRegAcPf.getValue().toString()));
                                }
                                // Regla: RN09 <Agregado médico para población
                                // abierta>
                                if (Integer.parseInt(consulExtDto.getAgregadoMedico().substring(0, 1)) == 0) {

                                    if (!agMeRegAcPf.getValue().toString().equals("ND")) {
                                        correcto = false;
                                    }

                                } else {
                                    muestraDiasIncapacidad();
                                }

                                if (correcto) {
                                    if (validaCamposDeRepetidos()) {
                                        logger.debug("---REGSITAR REPETIDO, LIMPIANDO CAMPOS");
                                        correcto = false;

                                    }
                                }
                                logger.info("06 correcto: " + correcto + "Agregadomedico: " + consulExtDto.getAgregadoMedico());//***
                                if (correcto) {

                                    if (consulExtDto.getAgregadoMedico() != "") {
                                        color = "6CEE38";
                                        List<AccederDto> listaAccederDto = null;
                                        AccederDto response = null;
                                        Calendar fechaActual = Calendar.getInstance();
                                        fechaActual.setTime(consulExtDto.getFecha());
                                        Integer annis = fechaActual.get(Calendar.YEAR)
                                                - Integer.parseInt(this.agMeAnioPf.getValue().toString());
                                        try {// ACCEDER
                                            logger.info("Agregado "+consulExtDto.getAgregadoMedico()+" NSS "+consulExtDto.getNss()+" Buscando en acceder");
                                            listaAccederDto = accederService.getInfo(consulExtDto.getNss(),
                                                    "24",
                                                    consulExtDto.getAgregadoMedico());// 8373550125
                                            listaAccederDto=filtraListaBeneficiarios(listaAccederDto);//
                                            if (listaAccederDto.size() == 1) {
                                                response = listaAccederDto.get(0);
                                            } else if (listaAccederDto.size() > 1) {
                                                //Se abremodal

                                                if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA_CIRUGIA
                                                        .getClave()
                                                        || consulExtDto.getCveEspecialidad()
                                                        .equals(String.valueOf(EnumEspecialidades.GINECOLOGIA.getClave()))
                                                        || consulExtDto.getCveEspecialidad()
                                                        .equals(String.valueOf(EnumEspecialidades.OBSTETRICIA.getClave()))) {
                                                    listaBeneficiarios = filtraListaSoloMujeres(listaAccederDto);
                                                }else{
                                                    listaBeneficiarios = listaAccederDto;
                                                }

                                                RequestContext.getCurrentInstance().execute("PF('dlgPacientes').show();");

                                                //response=listaAccederDto.get(Integer.parseInt(String.valueOf(getNumBeneficiario())));
                                                logger.info("Se abre modal " + listaBeneficiarios.size());
                                                return;
                                            }
                                        } catch (Exception e) {
                                            deshabilitado = false;
                                            deshabilitadoA = true;
                                            deshabilitadoR = false;
                                            deshabilitadoAgre = false;
                                            deshabilitadoAgreA = true;
                                            deshabilitadoAgreR = false;
                                            deshabilitadoNom = false;
                                            deshabilitadoNomA = true;
                                            deshabilitadoNomR = false;
                                            logger.info(e.getMessage());
                                            dialogEdad = Boolean.TRUE;
                                            if (annis <= 1) {
                                                dialogEdad = Boolean.TRUE;
                                                RequestContext context = RequestContext.getCurrentInstance();
                                                context.execute("PF('dialogEdad').show();");
                                                sem = Boolean.TRUE;
                                                foco = this.edad.getClientId();
                                                this.edad.resetValue();
                                                consulExtDto.setEdad(null);
                                            } else {
                                                dialogEdad = Boolean.FALSE;
                                                this.edad.setValue(annis);
                                                consulExtDto.setEdad(annis);
                                                sem = Boolean.FALSE;
                                            }
                                        }
//										if (response != null && !response.isConecta()) {
//												conecto=false;
//												response=null;
//											}
                                        if (response != null) {

                                            if (response.getEncontrado()) {
                                                //if(true){
                                                // RN 132 Cuando hay respuesta de
                                                // ACCEDER
                                                setEdadEnSemanas(calculoSemanasEdadAcceder(response.getFecNacimiento()));
                                                if ((getEdadEnSemanas() < especialidad.getNumEdadMinSemana())
                                                        || (getEdadEnSemanas() > especialidad.getNumEdadMaximaSemanas())) {
                                                    resetCamposEdadEspecialdad();
                                                } else {

                                                    deshabilitado = true;
                                                    deshabilitadoA = false;
                                                    deshabilitadoR = false;
                                                    deshabilitadoAgre = true;
                                                    deshabilitadoAgreA = false;
                                                    deshabilitadoAgreR = false;
                                                    deshabilitadoNom = true;
                                                    deshabilitadoNomA = false;
                                                    deshabilitadoNomR = false;
                                                    colorNss = "6CEE38";
                                                    comparaAcceder(consulExtDto, response);
                                                    datosDerecho = true;
                                                    deshabilitadoNom = true;
                                                    if (urgencia) {
                                                        foco = primeraVez.getClientId();
                                                        campo = 1;
                                                    } else {
                                                        // foco = "primeraVez";
                                                        // campo = 2;
                                                        foco = cveTipoUrgencia.getClientId();
                                                        campo = 3;
                                                    }
                                                    this.edad.setValue(annis);
                                                    consulExtDto.setEdad(annis);
                                                }
                                            } else {
                                                // RN 132 Cuando NO hay respuesta de
                                                // ACCEDER se toma el año del
                                                // agregado médico y por default 01
                                                // de Enero
                                                setEdadEnSemanas(calculoSemanasEdad("01/01/" + agMeAnioPf.getValue().toString()));
                                                if ((getEdadEnSemanas() < especialidad.getNumEdadMinSemana())
                                                        || (getEdadEnSemanas() > especialidad.getNumEdadMaximaSemanas())) {
                                                    resetCamposEdadEspecialdad();
                                                } else {
                                                    deshabilitado = false;
                                                    deshabilitadoA = false;
                                                    deshabilitadoR = true;
                                                    deshabilitadoAgre = false;
                                                    deshabilitadoAgreA = false;
                                                    deshabilitadoAgreR = true;
                                                    deshabilitadoNom = false;
                                                    deshabilitadoNomA = false;
                                                    deshabilitadoNomR = true;
                                                    limpiaAcceder();
                                                    datosDerecho = false;
                                                    foco = nombre.getClientId();
                                                    fechaNac = true;
                                                    if (annis <= 1) {
                                                        dialogEdad = Boolean.TRUE;
                                                        RequestContext context = RequestContext.getCurrentInstance();
                                                        context.execute("PF('dialogEdad').show();");
                                                        foco = this.edad.getClientId();
                                                        sem = Boolean.TRUE;

                                                        this.edad.resetValue();
                                                        consulExtDto.setEdad(null);
                                                    } else {
                                                        dialogEdad = Boolean.FALSE;
                                                        this.edad.setValue(annis);
                                                        consulExtDto.setEdad(annis);
                                                        sem = Boolean.FALSE;
                                                    }
                                                }
                                                consulExtDto.setStpUltConsultaVig(new Date());
                                            }

                                        } else {
                                            datosUniAds = true;
                                            consulExtDto.setStpUltConsultaVig(new Date());

                                            // RN 132 Cuando NO hay respuesta de
                                            // ACCEDER se toma el año del
                                            // agregado médico y por default 01
                                            // de Enero
                                            setEdadEnSemanas(calculoSemanasEdad("01/01/" + agMeAnioPf.getValue().toString()));
                                            if ((getEdadEnSemanas() < especialidad.getNumEdadMinSemana())
                                                    || (getEdadEnSemanas() > especialidad.getNumEdadMaximaSemanas())) {
                                                resetCamposEdadEspecialdad();
                                            } else {
                                                deshabilitado = false;
                                                deshabilitadoA = true;
                                                deshabilitadoR = false;
                                                deshabilitadoAgre = false;
                                                deshabilitadoAgreA = true;
                                                deshabilitadoAgreR = false;
                                                deshabilitadoNom = false;
                                                deshabilitadoNomA = true;
                                                deshabilitadoNomR = false;
                                                limpiaAcceder();
                                                datosDerecho = false;
                                                foco = nombre.getClientId();
                                                fechaNac = true;
                                                if (annis <= 1) {
                                                    dialogEdad = Boolean.TRUE;
                                                    RequestContext context = RequestContext.getCurrentInstance();
                                                    context.execute("PF('dialogEdad').show();");
                                                    foco = this.edad.getClientId();
                                                    sem = Boolean.TRUE;

                                                    this.edad.resetValue();
                                                    consulExtDto.setEdad(null);
                                                } else {
                                                    dialogEdad = Boolean.FALSE;
                                                    this.edad.setValue(annis);
                                                    consulExtDto.setEdad(annis);
                                                    sem = Boolean.FALSE;
                                                }
                                            }


                                        }
                                    } else {
                                        deshabilitadoAgre = false;
                                        color = "FFFFFF";
                                    }


                                } else {
                                    if (!validaCamposDeRepetidos()) {
                                        limpiaAcceder();
                                        deshabilitadoAgre = false;
                                        color = "FFFFFF";
                                        foco = agMeDerechoPf.getClientId();
                                        mensajeErrorConParametros("me18_valor_incorrecto", "Agregdo Medico");
                                    }

                                }
                            } else {
                                limpiaAcceder();
                                deshabilitadoAgre = false;
                                color = "FFFFFF";
                                foco = agMeRegAcPf.getClientId();
                                mensajeErrorConParametros("me18_valor_incorrecto", "Agregdo Medico");
                            }
                        }
                    } else {
                        limpiaAcceder();
                        deshabilitadoAgre = false;
                        color = "FFFFFF";
                        foco = agMeAnioPf.getClientId();
                        mensajeErrorConParametros("me18_valor_incorrecto", "Agregdo Medico");
                    }
                } else {
                    if (regimen != null) {
                        limpiaAcceder();
                        deshabilitadoAgre = false;
                        color = "FFFFFF";
                        foco = agMeAnioPf.getClientId();
                        mensajeErrorConParametros("me18_valor_incorrecto", "Agregdo Medico");
                    } else {
                        foco = agMeAnioPf.getClientId();
                    }
                }
            } else {
                if (anno != null && regimen != null) {
                    limpiaAcceder();
                    deshabilitadoAgre = false;
                    color = "FFFFFF";
                    foco = agMeSexoPf.getClientId();
                    mensajeErrorConParametros("me18_valor_incorrecto", "Agregdo Medico");
                } else {
                    foco = agMeSexoPf.getClientId();
                }
            }
        } else {
            limpiaAcceder();
            deshabilitadoAgre = false;
            color = "FFFFFF";
            foco = agMeDerechoPf.getClientId();
            mensajeErrorConParametros("me18_valor_incorrecto", "Agregdo Medico");
        }
        Collection<String> componentes = new ArrayList<String>();
        componentes.add("form:cextAtencion");
        componentes.add("formDlgNegocio:dlgEdad");
        componentes.add("form:cextDiag");
        componentes.add("form:cextAtencion2");
//		RequestContext.getCurrentInstance().update("form:cextAtencion formDlgNegocio:dlgEdad");
        RequestContext.getCurrentInstance().update(componentes);
        //oncomplete="scriptDesbloquear();scripts_cextAtencion();PF('blockUIWidget').unblock();scripts_cextDiag();scripts_cextAtencion2();"
        RequestContext.getCurrentInstance().execute("scriptDesbloquear();scripts_cextAtencion();PF('blockUIWidget').unblock();scripts_cextDiag();scripts_cextAtencion2();");

//		RequestContext.getCurrentInstance().update("form:cextDiag");
//		RequestContext.getCurrentInstance().update("form:cextAtencion2");
        //RequestContext.getCurrentInstance().update("form:cextAtencion, formDlgNegocio:dlgEdad, form:cextDiag,form:cextAtencion2");

        //
        logger.info("Termina validaAgregado()");

    }

    private void muestraDiasIncapacidad() {
        if (Integer.parseInt(consulExtDto.getAgregadoMedico().substring(0, 1)) == 1) {// Regla:
            // RN16
            // <Días
            // de
            // incapacidad>
            if (!agMeRegAcPf.getValue().toString().equals("OR")) {
                diasInca = false;
                consulExtDto.setDiasIncapacidad(null);
                diasIncapacidad.resetValue();
            } else {
                diasInca = true;
            }
        } else {
            diasInca = false;
            consulExtDto.setDiasIncapacidad(null);
            diasIncapacidad.resetValue();
        }
    }

    private void muestraMetodoPlanificacionFamiliar(int edad) {
        if (consulExtDto.getCveEspecialidad().startsWith("50")
                || consulExtDto.getCveEspecialidad().equals("A600")
                || consulExtDto.getCveEspecialidad().equals("6200")
                || consulExtDto.getCveEspecialidad().equals("6300")
                || consulExtDto.getCveEspecialidad().equals("6400")
                || consulExtDto.getCveEspecialidad().equals("6600")
                || consulExtDto.getCveEspecialidad().equals("6900")
                || consulExtDto.getCveEspecialidad().equals("7000")
                || consulExtDto.getCveEspecialidad().equals("9800")) {

            this.verMetodoPlanFamiliar = Boolean.FALSE;
        } else {
            this.verMetodoPlanFamiliar = Boolean.TRUE;
            listadoMetPPF = catalogosService.llenaComboMetPPFXSexoXEdad(
                    agMeSexoPf.getValue().toString().toUpperCase().equals("M") ? 1 : 2, edad);
            if (!listadoMetPPF.isEmpty()) {
                this.verMetodoPlanFamiliar = Boolean.TRUE;
            } else {
                this.verMetodoPlanFamiliar = Boolean.FALSE;
            }
            listaMetPpfStr = cargaListaString(listadoMetPPF);
            metodoPf = true;
            if (listadoMetPPF.isEmpty()) {
                listadoMetPPF = new ArrayList<CatalogoDto>();
                listaMetPpfStr = cargaListaString(listadoMetPPF);
                metodoPf = false;
            }
        }
    }

    /**
     * Oculta o muestra el Riesgo de trabajo dependiendo de la RN y el agregado médico
     */
    private void muestraRiesgoDeTrabajo() {
        if (agMeDerechoPf.getValue().toString().equals("1") && agMeRegAcPf.getValue().toString().toUpperCase().equals("OR")) {
            setMuestraRiesgoTrabajo(true);
        } else {
            setMuestraRiesgoTrabajo(false);
        }
        if (consulExtDto.getNumTipoServicio().equals(EnumEspecialidades.PARAMEDICO.getClave())) {
            setMuestraRiesgoTrabajo(false);
        }
    }

    private void resetCamposEdadEspecialdad() {
        nss.resetValue();
        agMeDerechoPf.resetValue();
        agMeSexoPf.resetValue();
        agMeAnioPf.resetValue();
        agMeRegAcPf.resetValue();
        foco = nss.getClientId();
        color = "FFFFFF";
        RequestContext.getCurrentInstance().update("form");
        agregarError("me135_edad_especialidad");
    }

    private void limpiaDiagnosticos() {
        this.metodoPpfAcPf.resetValue();
        this.cantidadMpf.setValue(null);
        this.diagnosticoPrincipal.setValue("");
        this.desDiagPrin.setValue("");
        this.cveInforAdicPrin.setValue("");
        this.cveTipoPrin.setValue("");
        this.fecCirugiaPrin.setValue(null);
        consulExtDto.setDiagAcc(0);
        this.diagnosticoAdicional.setValue("");
        this.desDiagAdic.setValue("");
        this.primeraVezDiagAdic.setValue(null);
        this.cveInforAdicAdic.setValue("");
        this.cveTipoAdic.setValue("");
        this.fecCirugiaAdic.setValue(null);
        this.diagnosticoComplemento.setValue("");
        this.desDiagComp.setValue("");
        this.primeraVezDiagComp.setValue(null);
        this.cveInforAdicComp.setValue("");
        this.cveTipoComp.setValue("");
        this.fecCirugiaComp.setValue(null);
        this.procedimientoPrincipal.setValue("");
        this.desProcPrin.setValue("");
        this.procedimientoAdicional.setValue("");
        this.desProcAdic.setValue("");
        this.procedimientoComplemento.setValue("");
        this.desProcComp.setValue("");
        this.cveMorfologia.setValue("");
        this.setDesMorfologia("");
    }

    public void validaFechaNac() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            if (edad != null && !this.edad.getValue().toString().isEmpty()) {
                if (Integer.parseInt(edad.getValue().toString()) > 0
                        && Integer.parseInt(edad.getValue().toString()) <= 103) {

                    // valido
                    datosDerecho = true;
                    deshabilitadoNom = true;
                    datosDerecho = false;

                    consulExtDto.setEdad(Integer.parseInt(edad.getValue().toString()));
                    if (sem) {
                        if (Integer.parseInt(edad.getValue().toString()) > 51) {
                            consulExtDto.setEdad(1);
                        }
                    }

                    RequestContext context = RequestContext.getCurrentInstance();
                    context.execute("PF('dialogEdad').hide();");
                } else {
                    // invalido fuera de rago
                    consulExtDto.setEdad(null);
                    agregarError("me138_rangoEdad");
                }
            } else {
                // invalido reuqerido
                consulExtDto.setEdad(null);
                mensajeErrorConParametros("mecx01_requerido", "Edad");
            }
        } else {
            foco = agMeDerechoPf.getClientId();
            this.edad.resetValue();
            consulExtDto.setEdad(null);

            agMeDerechoPf.resetValue();
            agMeSexoPf.resetValue();
            agMeAnioPf.resetValue();
            agMeRegAcPf.resetValue();

            consulExtDto.setAgregadoMedico(null);
            // RequestContext.getCurrentInstance().update(");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogEdad').hide();");
        }
    }

    private void comparaAcceder(ConsultaExternaDto consulExtDto, AccederDto response) {
        this.nombre.resetValue();
        this.apePaterno.resetValue();
        this.apeMaterno.resetValue();
        this.nombre.setValue(response.getNombre());
        this.apePaterno.setValue(response.getApePaterno());
        this.apeMaterno.setValue(response.getApeMaterno());
        logger.info("Response Agregado Medico " +response.getAgregadoMedico());

        consulExtDto.setUnidadAdscripcion(response.getUnidadAdscripcion());
        consulExtDto.setNombreUnidadMed(response.getDescUnidadMed());

        consulExtDto.setAgregadoMedico(response.getAgregadoMedico());//

        consulExtDto.setNombre(response.getNombre());
        consulExtDto.setApePaterno(response.getApePaterno());
        consulExtDto.setApeMaterno(response.getApeMaterno());

        consulExtDto.setIndVigente(response.getIndVigente());
        consulExtDto.setIndPacEncontrado(response.getIndPacEncontrado());
        consulExtDto.setCveCurp(response.getCveCurp());
        consulExtDto.setStpUltConsultaVig(response.getStpUltConsultaVig());
        consulExtDto.setCveIdee(response.getCveIdee());

        Date fechaNac = null;
        try {
            fechaNac = new SimpleDateFormat("yyyy/MM/dd").parse(response.getFecNacimiento());
        } catch (ParseException e) {
            logger.info(e.getMessage());
        }

        consulExtDto.setFecNacimiento(fechaNac);
        consulExtDto.setVigencia(response.isVigencia());
    }

    private void limpiaAcceder() {
        this.nombre.setValue(null);
        this.apePaterno.setValue(null);
        this.apeMaterno.setValue(null);
        apMat = false;

        consulExtDto.setUnidadAdscripcion(null);
        consulExtDto.setNombreUnidadMed(null);
        consulExtDto.setFecNacimiento(null);
        consulExtDto.setVigencia(false);


        consulExtDto.setIndPacEncontrado(0);
        consulExtDto.setIndVigente(0);
//		consulExtDto.setStpUltConsultaVig(null);
        consulExtDto.setCveCurp(null);
        consulExtDto.setCveIdee(null);
        valNombreCom = "";
    }

    public void validaPriVez() {
		/*switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
		// case 2:// URGENCIA
		// foco = cveTipoUrgencia.getClientId();
		// campo = 3;
		// break;
		// case 4:// PARAMEDICO
		// foco = visitas.getClientId();
		// campo = 6;
		// break;
		// case 3:// URGENCIA_CIRUGIA
		// foco = pase.getClientId();
		// campo = 4;
		// break;
		//
		// default:
		// foco = pase.getClientId();
		// campo = 4;
		// break;
		// }
		case 2:// URGENCIA
			foco = cveTipoUrgencia.getClientId();
			campo = 3;
			break;
		// case 4:// PARAMEDICO
		// foco = citado.getClientId();
		// campo = 6;
		// break;
		// case 3:// URGENCIA_CIRUGIA
		// foco = pase.getClientId();
		// campo = 4;
		// break;

		default:
			foco = citado.getClientId();
			campo = 4;
			break;
		}*/


        if (validaDiferenteEmpty(this.getPrimeraVez())) {
            consulExtDto.setPrimeraVez(Integer.parseInt(primeraVez.getValue().toString()));
            validaFocoAfterAtencion(primeraVez.getClientId());
        } else {
            foco = this.getPrimeraVez().getClientId();
            consulExtDto.setPrimeraVez(null);
            mensajeErrorConParametros("mecx01_requerido", "Primera vez");
        }
    }

    public void validaCitado() {
		/*switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
		// case 2:// URGENCIA
		// foco = cveTipoUrgencia.getClientId();
		// campo = 3;
		// break;
		case 4:// PARAMEDICO
			foco = visitas.getClientId();
			campo = 6;
			break;
		// case 3:// URGENCIA_CIRUGIA
		// foco = pase.getClientId();
		// campo = 4;
		// break;

		// case n://URGENCIATOCO
		// foco = recetas.getClientId();
		// campo = 4;
		// break;

		default:
			foco = pase.getClientId();
			campo = 4;
			break;
		}*/
        if (validaDiferenteEmpty(this.getCitado())) {
            consulExtDto.setCitado(Integer.parseInt(citado.getValue().toString()));
            validaFocoAfterAtencion(citado.getClientId());
        } else {
            foco = this.getCitado().getClientId();
            consulExtDto.setCitado(null);
            mensajeErrorConParametros("mecx01_requerido", "Citado");
        }

    }

    public void validaTipoUrgencia() {
        foco = pase.getClientId();
        campo = 4;
        if (validaDiferenteEmpty(this.getCveTipoUrgencia())) {
            consulExtDto.setCveTipoUrgencia(Integer.parseInt(cveTipoUrgencia.getValue().toString()));
        } else {
            consulExtDto.setCveTipoUrgencia(null);
        }


    }

    public void validaPase() {
        if (pase.getValue() != null && !pase.getValue().toString().isEmpty()) {
            foco = recetas.getClientId();
            campo = 5;
            consulExtDto.setPase(Integer.parseInt(pase.getValue().toString()));
        } else {
            foco = pase.getClientId();

            consulExtDto.setPase(null);
            mensajeErrorConParametros("mecx01_requerido", "Pase");
        }

    }

    public void validaVisita() {
		/*switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
		case 4:// PARAMEDICO

			if (this.getAgMeDerechoPf().getValue().toString().equals("1")
					&& this.getAgMeRegAcPf().getValue().toString().equals("OR")) {
				foco = diasIncapacidad.getClientId();
				campo = 7;
			} else {
				foco = diagnosticoPrincipal.getClientId();
				campo = 11;// Diagnostico Principal
			}
			break;

		default:
			if (this.getAgMeDerechoPf().getValue().toString().equals("1")
					&& this.getAgMeRegAcPf().getValue().toString().equals("OR")) {
				foco = diasIncapacidad.getClientId();
				campo = 7;
			} else {
				foco = metodoPpfAcPf.getClientId();
				campo = 8;
			}
			if(!this.getAgMeDerechoPf().getValue().toString().equals("1") && this.getAgMeSexoPf().getValue().toString().toUpperCase().equals("M")){
				foco = diagnosticoPrincipal.getClientId();
				campo = 8;
			}
			break;
		}*/

        if (validaDiferenteEmpty(this.getVisitas())) {
            consulExtDto.setVisitas(Integer.parseInt(visitas.getValue().toString()));
            validaFocoAfterAtencion(visitas.getClientId());
        } else {
            foco = this.getVisitas().getClientId();
            consulExtDto.setVisitas(null);
            mensajeErrorConParametros("mecx01_requerido", "Visitas");
        }


    }

    public void validaNumRecetas() {
        if (this.recetas.getValue() != null && !this.recetas.getValue().toString().isEmpty()) {
            if (Integer.parseInt(this.recetas.getValue().toString()) > 10) {
                validaNumRecetas = false;
                agregarError("me11_num_max_recetas");
                foco = recetas.getClientId();
                campo = 5;
            } else {
                validaNumRecetas = true;
				/*foco = visitas.getClientId();
				switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
				case 2:// URGENCIA
				case 3:// URGENCIA_CIRUGIA
					foco = diasIncapacidad.getClientId();
					campo = 7;
					if (!this.diasInca) {
						foco = metodoPpfAcPf.getClientId();
					}
					if (!agMeDerechoPf.getValue().toString().equals("1") ||
							!agMeRegAcPf.getValue().toString().toUpperCase().equals("OR")) {
						foco=diagnosticoPrincipal.getClientId();
					}
					break;

				default:

					campo = 6;
					break;
				}*/
                validaFocoAfterAtencion(recetas.getClientId());
            }

            consulExtDto.setRecetas(Integer.parseInt(recetas.getValue().toString()));
        } else {

			/*if (consulExtDto.getEspecialidadSelc() != null && Integer.parseInt(consulExtDto.getEspecialidadSelc()) != 2
					&& Integer.parseInt(consulExtDto.getEspecialidadSelc()) != 3) {
				foco = visitas.getClientId();
			} else {
				foco = diasIncapacidad.getClientId();
			}
			if (!agMeDerechoPf.getValue().toString().equals("1") ||
					!agMeRegAcPf.getValue().toString().toUpperCase().equals("OR")) {
				foco=diagnosticoPrincipal.getClientId();
			}*/
            consulExtDto.setRecetas(null);
            validaFocoAfterAtencion(recetas.getClientId());
        }

    }

    public void validaDiasIncap() {
    	logger.info("Inicia validaDiasIncap()");
        boolean validoDiasIncapacidad = true;
        if (this.diasIncapacidad.getValue() != null && !this.diasIncapacidad.getValue().toString().isEmpty()) {
            if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.PARAMEDICO.getClave()) {
                foco = diagnosticoPrincipal.getClientId();
                campo = 11;
            } else if (consulExtDto.getCveEspecialidad()
                    .equals(String.valueOf(EnumEspecialidades.GINECOLOGIA.getClave()))
                    || consulExtDto.getCveEspecialidad()
                    .equals(String.valueOf(EnumEspecialidades.OBSTETRICIA.getClave()))) {
                if (Integer.parseInt(this.diasIncapacidad.getValue().toString()) > 90) {
                    validaDiasInca = false;
                    agregarError("me13_dias_men90");
                    foco = diasIncapacidad.getClientId();
                    validoDiasIncapacidad = false;
                    campo = 7;
                } else {
                    validaDiasInca = true;
                    foco = metodoPpfAcPf.getClientId();
                    campo = 8;
                    if (!verMetodoPlanFamiliar) {
                        foco = diagnosticoPrincipal.getClientId();
                    }
                }
            } else {
                if (Integer.parseInt(this.diasIncapacidad.getValue().toString()) > 28) {
                    validaDiasInca = false;
                    agregarError("me12_dias_men28");
                    foco = diasIncapacidad.getClientId();
                    validoDiasIncapacidad = false;
                    campo = 7;
                } else {
                    validaDiasInca = true;
                    if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA_CIRUGIA
                            .getClave() || (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA
                            .getClave())) {
                        foco = diagnosticoPrincipal.getClientId();
                        campo = 11;
                    } else {
                        if (metodoPf) {
                            if (metodoPpfAcPf.getValue() != null) {
                                foco = metodoPpfAcPf.getClientId();
                                campo = 8;
                            } else {
                                foco = diagnosticoPrincipal.getClientId();
                                campo = 11;
                            }

                        } else {
                            foco = diagnosticoPrincipal.getClientId();
                            campo = 11;
                        }
                    }
                }
            }
            consulExtDto.setDiasIncapacidad(Integer.parseInt(diasIncapacidad.getValue().toString()));
        } else {
            if (metodoPf) {
                foco = metodoPpfAcPf.getClientId();
                campo = 8;
                if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.PARAMEDICO.getClave()
                        || Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA_CIRUGIA
                        .getClave()
                    //|| Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA.getClave()
                ) {
                    foco = diagnosticoPrincipal.getClientId();
                    campo = 11;
                }
            } else {
                foco = diagnosticoPrincipal.getClientId();
                campo = 11;
            }
        }
        if (validoDiasIncapacidad) {
            validaFocoAfterAtencion(diasIncapacidad.getClientId());
        }
    	logger.info("Termina validaDiasIncap()");
    }

    public void validaMetodoPpf(CatalogoDto catalogoDto) {
        dialogCantidad = Boolean.FALSE;
        if (this.metodoPpfAcPf.getValue() != null && !this.metodoPpfAcPf.getValue().toString().isEmpty()
                && catalogoDto != null) {
            Integer cantidad = consultaExternaService.buscarCantidad(catalogoDto.getClave());

            if (cantidad > 0) {
                metodoPfCan = true;
                // foco = "cantMetodoPpf";
                campo = 31;
                dialogCantidad = Boolean.TRUE;
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('dialogCantidad').show();");
                this.cantidadMpf.setValue(null);
            } else {
                metodoPfCan = false;
                foco = diagnosticoPrincipal.getClientId();
                campo = 11;// Diagnostico Principal
            }
            consulExtDto.setMetodoPpf(catalogoDto.getClave());
        } else {
            foco = diagnosticoPrincipal.getClientId();
            consulExtDto.setMetodoPpf(null);
        }
    }

    public List<String> completeTextMetodoPpf(String query) {
        List<String> resultado = new ArrayList<String>();
        resultado = completeTextComponente(query, 2, listaMetPpfStr);
        return resultado;
    }

    public void onItemSelectMetodoPpf(SelectEvent event) {
        CatalogoDto objeto = null;
        objeto = itemSeleccionado(event, listaMetPpfStr, metodoPpfAcPf, listadoMetPPF);
        validaMetodoPpf(objeto);

    }

    public void validaAutocMetodoPpf() {

        CatalogoDto objeto = null;
        objeto = validaAutoComplete(metodoPpfAcPf, 2, listaMetPpfStr, listadoMetPPF);
        validaMetodoPpf(objeto);
    }

    public void validaCantMetodoPpf() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            String CERO = "0";
            Integer cantidad = consultaExternaService.buscarCantidad(consulExtDto.getMetodoPpf());
            if (this.cantidadMpf.getValue() == null || this.cantidadMpf.getValue().toString().isEmpty()) {
                mensajeErrorConParametros("mecx01_requerido", "Cantidad");
                dialogCantidad = Boolean.TRUE;
                foco = cantidadMpf.getClientId();
            } else if (Integer.parseInt(this.cantidadMpf.getValue().toString()) > cantidad
                    || this.cantidadMpf.getValue().toString().equals(CERO)) {
                agregarError("msg_cantidad_no_valida");
                dialogCantidad = Boolean.TRUE;
                foco = cantidadMpf.getClientId();
            } else {
                foco = "formDlgNegocio:btnAcepCon";
                consulExtDto.setCantidadMpf(Integer.parseInt(cantidadMpf.getValue().toString()));
                dialogCantidad = Boolean.FALSE;
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('dialogCantidad').hide();");
            }
        } else {
            //Tecla "ESC"
            dialogCantidad = Boolean.FALSE;

            foco = metodoPpfAcPf.getClientId();
            consulExtDto.setCantidadMpf(null);
            consulExtDto.setMetodoPpf(null);
            this.metodoPpfAcPf.resetValue();

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogCantidad').hide();");
        }

    }

    public void validaAccidentes() {
//		switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
//		case 2:// URGENCIA
//			foco = "form:btnAgregarAten";
//			campo = 28;
//			break;
//		case 3:// URGENCIA_CIRUGIA
//			foco = riesgosTrabajo.getClientId();
//			campo = 10;
//			break;
//
//		default:
//			foco = riesgosTrabajo.getClientId();
//			campo = 10;
//			break;
//		}


        if (validaDiferenteEmpty(this.getAccidentesLesiones())) {
            consulExtDto.setAccidentesLesiones(Integer.parseInt(accidentesLesiones.getValue().toString()));
        } else {
            consulExtDto.setAccidentesLesiones(null);
        }
        validaFocoAccidentesLesiones();

    }

    public void validaRiesgos() {
        foco = "form:btnAgregarAten";
        if (riesgosTrabajo.getValue() != null && !riesgosTrabajo.getValue().toString().isEmpty()) {
            consulExtDto.setRiesgosTrabajo(Integer.parseInt(riesgosTrabajo.getValue().toString()));
        } else {
            consulExtDto.setRiesgosTrabajo(null);
        }
        RequestContext context = RequestContext.getCurrentInstance();
        if (paraEdicion) {
            context.execute("document.getElementById('form:btnGuardarEdit').focus();");
        } else {
            context.execute("document.getElementById('form:btnAgregarAten').focus();");
        }

    }

    public void validaFisrtName() {
        if (this.getNombre().getValue() != null && !this.getNombre().getValue().toString().isEmpty()) {

            foco = apePaterno.getClientId();
            validaNombreCompleto();
        } else {
            foco = nombre.getClientId();
        }
    }

    public void validaApePaterno() {
        if (this.getApePaterno().getValue() != null && !this.getApePaterno().getValue().toString().isEmpty()) {

            foco = apeMaterno.getClientId();
            validaNombreCompleto();
        } else {
            foco = apePaterno.getClientId();
        }
    }

    public void validapeMaterno() {
        apMat = true;
        validaNombreCompleto();
        if (urgencia) {
            foco = primeraVez.getClientId();
            campo = 1;
        } else {
            foco = cveTipoUrgencia.getClientId();
            campo = 2;
        }
    }

    public void validaNombreCompleto() {
        if (this.getNombre().getValue() != null && !this.getNombre().getValue().toString().isEmpty()
                && this.getApePaterno().getValue() != null && !this.getApePaterno().getValue().toString().isEmpty()
                && ((this.getApeMaterno().getValue() != null && !this.getApeMaterno().getValue().toString().isEmpty())
                || apMat)) {

            validaNombre();
        }
    }

    public void validaNombre() {
        // deshabilitadoNom = true;
        datosDerecho = true;
        if (urgencia) {
            foco = citado.getClientId();
            campo = 1;
        } else {
            foco = cveTipoUrgencia.getClientId();
            campo = 2;
        }
        consulExtDto.setNombre(nombre.getValue().toString().trim());
        consulExtDto.setApePaterno(apePaterno.getValue().toString().trim());
        if (this.getApeMaterno().getValue() != null && !this.getApeMaterno().getValue().toString().isEmpty()) {
            consulExtDto.setApeMaterno(apeMaterno.getValue().toString().trim());
        }
    }

    public void validaDiagPrincipal() {
    	logger.info("Inicia validaDiagPrincipal()");
        boolean diagnosticoPrincipalValido = true;
        int edad = 0;
        String desDiagnostico = null;
        boolean isTumor;
        setMorfologiaVisible(Boolean.FALSE);
        SicCie10DTO sicCie10DTO = null;


        if (this.diagnosticoPrincipal.getValue() != null && !this.diagnosticoPrincipal.getValue().toString().isEmpty()) {

            if (!this.diagnosticoPrincipal.getValue().toString().toUpperCase().equals(consulExtDto.getDiagnosticoPrincipal())) {
                resetBorrarInfoAdicionalPrin();
            }
            consulExtDto.setDiagnosticoPrincipal(this.diagnosticoPrincipal.getValue().toString().toUpperCase());

            if (validaCamposDeRepetidos()) {
                logger.debug("---REGSITAR REPETIDO, LIMPIANDO CAMPOS");

            } else {
                logger.info("Campos no repetido");
                if (consulExtDto.getFecNacimiento() != null) {
                    edad = calculoSemanasEdad(new SimpleDateFormat("dd/MM/yyyy").format(consulExtDto.getFecNacimiento()));

                    sicCie10DTO = consultaExternaService.buscarDiagnostico(
                            this.diagnosticoPrincipal.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), Boolean.TRUE);
                    if (sicCie10DTO != null) {
                        desDiagnostico = sicCie10DTO.getDesCie10();
                    }
                } else {

                    edad = Integer.parseInt(this.edad.getValue().toString());
                    sicCie10DTO = consultaExternaService.buscarDiagnostico(
                            this.diagnosticoPrincipal.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), sem);
                    if (sicCie10DTO != null) {
                        desDiagnostico = sicCie10DTO.getDesCie10();
                    }
                }

                isTumor = consultaExternaService.isTumor(consulExtDto.getDiagnosticoPrincipal());
                logger.info("diagnostico "+desDiagnostico);
                if (desDiagnostico == null) {
                    foco = diagnosticoPrincipal.getClientId();
                    campo = 11;// Diagnostico Principal
                    consulExtDto.setDesDiagPrin("");
                    diagnosticoPrincipal.resetValue();
                    desDiagPrin.resetValue();
                    diagnosticoPrincipalValido = false;
                    agregarError("me14_clave_no_existe");
                } else {
                    this.desDiagPrin.setValue(desDiagnostico);
                    if (desDiagnostico.equals("X")) {
                        foco = diagnosticoPrincipal.getClientId();
                        campo = 11;// Diagnostico Principal
                        diagnosticoPrincipalValido = false;
                        agregarError("me17_vlave_no_sexo");
                    } else if (desDiagnostico.equals("N")) {
                        foco = diagnosticoPrincipal.getClientId();
                        campo = 11;// Diagnostico Principal
                        consulExtDto.setDesDiagPrin("");
                        diagnosticoPrincipalValido = false;
                        agregarError("me16_clave_no_edad");
                    } else if (isTumor) {
                        if (consulExtDto.getCveEspecialidad().startsWith("50")
                                || consulExtDto.getCveEspecialidad().equals("A600")
                                || consulExtDto.getCveEspecialidad().equals("6200")
                                || consulExtDto.getCveEspecialidad().equals("6300")
                                || consulExtDto.getCveEspecialidad().equals("6400")
                                || consulExtDto.getCveEspecialidad().equals("6600")
                                || consulExtDto.getCveEspecialidad().equals("6900")
                                || consulExtDto.getCveEspecialidad().equals("7000")
                                || consulExtDto.getCveEspecialidad().equals("9800")) {
                            foco = diagnosticoAdicional.getClientId();
                            diagnosticoAdicional.setDisabled(Boolean.FALSE);
                            primeraVezDiagAdic.setDisabled(Boolean.FALSE);
                        } else {
                            this.desDiagPrin.setValue(desDiagnostico);
                            RequestContext context = RequestContext.getCurrentInstance();
                            context.execute("PF('dialogMorfologia').show();");
                            cveMorfologia.resetValue();
                        }

                    } else /*
                     * RN 22 Indicador diagnostico principal consulta externa
                     * Diagnostico relacionado a violencia Diagnostico
                     * erradicado
                     */
                        if (sicCie10DTO.getIndPrincipalConsExterna() != 1 || isDiagnosticoViolencia(sicCie10DTO.getCveCie10())) {
                            foco = diagnosticoPrincipal.getClientId();
                            consulExtDto.setDesDiagPrin("");
                            diagnosticoPrincipalValido = false;
                            agregarError("me19_clave_no_permitida");
                            // diagnosticoPrincipal.resetValue();
                            // desDiagPrin.resetValue();
                        } else if (sicCie10DTO.getIndErrad() == 1) {
                            foco = diagnosticoPrincipal.getClientId();
                            consulExtDto.setDesDiagPrin("");
                            diagnosticoPrincipalValido = false;
                            agregarError("me140_erradicado");
                            // diagnosticoPrincipal.resetValue();
                            // desDiagPrin.resetValue();
                        } else if (sicCie10DTO.getIndNotifInmediata() == 1 || sicCie10DTO.getIndNotifObligatoria() == 1) {
                            foco = "formDlgNegocio:siEpidemia";
                            RequestContext context = RequestContext.getCurrentInstance();
                            context.execute("PF('dialogNotifInmedOblig').show();");
                        } else {
                            // VALIDA RANGO VIH B20 & B24 DIAGNOSTICO PRINCIPAL
                            String rangoN = "N";
                            String rangoX = "X";
//					if(desDiagnostico.equals("N") || !desDiagnostico.equals("X")){
                            if (rangoN.equals(desDiagnostico) || !rangoX.equals(desDiagnostico)) {
                                this.validaVIH(1);
                                this.validaTipoEmbarazo(1);
                            }
                            diagnosticoAdicional.setDisabled(Boolean.FALSE);
                            primeraVezDiagAdic.setDisabled(Boolean.FALSE);
                            foco = diagnosticoAdicional.getClientId();
                            campo = 15;
                        }
                    switch (sicCie10DTO.getCveCie10().toUpperCase().substring(0, 3)) {
                        case "A04":
                        case "A08":
                        case "A09":
                            foco = cveTipoPrin.getClientId();
                            campo = 13;
                            diagPrin = 3;
                            Calendar fechaActual = Calendar.getInstance();
                            fechaActual.setTime(consulExtDto.getFecha());
                            Integer annis = fechaActual.get(Calendar.YEAR)
                                    - Integer.parseInt(this.agMeAnioPf.getValue().toString());
                            if (annis < 5) {
                                dialogDiarreaPrin = Boolean.TRUE;
                                cveTipoPrin.resetValue();
                                consulExtDto.setCveTipoPrin(null);
                                RequestContext context = RequestContext.getCurrentInstance();
                                context.execute("PF('dialogTipDia').show();");
                            } else {
                                diagnosticoAdicional.setDisabled(Boolean.FALSE);
                                primeraVezDiagAdic.setDisabled(Boolean.FALSE);
                                foco = diagnosticoAdicional.getClientId();
                                campo = 15;
                            }
                            break;
//					default:
//						diagnosticoAdicional.setDisabled(Boolean.FALSE);
//						primeraVezDiagAdic.setDisabled(Boolean.FALSE);
//						foco = diagnosticoAdicional.getClientId();
//						campo = 15;
//						break;
                    }
                    this.desDiagPrin.setValue(desDiagnostico);
                    consulExtDto.setDesDiagPrin(desDiagPrin.getValue().toString());

                    // ultimo campo requerido y se activan los botones
                    logger.info("Validando Boton \n Agregado "+consulExtDto.getAgregadoMedico());
                    logger.info("Nombre "+this.nombre);
                    logger.info("Apellido Paterno "+ this.apePaterno);
                    logger.info("NSS "+nss.getValue());
                    if (nss.getValue() != null && !nss.getValue().toString().isEmpty()
                            && consulExtDto.getAgregadoMedico() != null && !consulExtDto.getAgregadoMedico().isEmpty()
                            && this.nombre.getValue() != null && !this.nombre.getValue().toString().isEmpty()
                            && this.apePaterno.getValue() != null && !this.apePaterno.getValue().toString().isEmpty()
                    ) {
                        try {
                            logger.info("especialidad " + consulExtDto.getEspecialidadSelc());
                            logger.info("primera vez " + this.primeraVez.getValue());
                            logger.info("Tipo urgencia clave " + this.cveTipoUrgencia.getValue());
                            logger.info("pase " + this.pase.getValue().toString().isEmpty());
                            logger.info("citado " + this.citado.getValue());
                            logger.info("visitas " + this.visitas.getValue());
                        } catch (Exception e){
                            logger.info(e);
                        }
                        if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA.getClave()
                                && this.primeraVez.getValue() != null && !this.primeraVez.getValue().toString().isEmpty()
                                && this.cveTipoUrgencia.getValue() != null
                                && !this.cveTipoUrgencia.getValue().toString().isEmpty() && this.pase.getValue() != null
                                && !this.pase.getValue().toString().isEmpty()) {
                            campo = 28;
                        } else if ((Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.PARAMEDICO
                                .getClave()
                                || consulExtDto.getCveEspecialidad()
                                .equals(String.valueOf(EnumEspecialidades.GINECOLOGIA.getClave()))
                                || consulExtDto.getCveEspecialidad()
                                .equals(String.valueOf(EnumEspecialidades.OBSTETRICIA.getClave())))
                                && this.citado.getValue() != null && !this.citado.getValue().toString().isEmpty()
                                && this.primeraVez.getValue() != null && !this.primeraVez.getValue().toString().isEmpty()
                                && this.visitas.getValue() != null && !this.visitas.getValue().toString().isEmpty()) {
                            campo = 28;
                        } else if (Integer.parseInt(
                                consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA_CIRUGIA.getClave()
                                && this.citado.getValue() != null && !this.citado.getValue().toString().isEmpty()
                                && this.primeraVez.getValue() != null && !this.primeraVez.getValue().toString().isEmpty()) {
                            campo = 28;
                        } else if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.OTROS
                                .getClave() && this.citado.getValue() != null
                                && !this.citado.getValue().toString().isEmpty() && this.primeraVez.getValue() != null
                                && !this.primeraVez.getValue().toString().isEmpty() && this.visitas.getValue() != null
                                && !this.visitas.getValue().toString().isEmpty()) {
                            campo = 28;
                        } else {
                            campo = 11;// Diagnostico Principal
                        }
                    }
                }

            }


            if (!diagnosticoPrincipalValido) {
                resetDiagPrincipal();
            }
        } else {
            resetDiagPrincipal();
            mensajeErrorConParametros("mecx01_requerido", "Diagn\u00F3stico Principal");
        }
        logger.info("Termina validaDiagPrincipal()");
    }

    public void validaMorfologia() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            if (cveMorfologia != null && !this.cveMorfologia.getValue().toString().isEmpty()) {

                SicCie10Morfologia morfologia = consultaExternaService
                        .buscarMorfologiaPorId(cveMorfologia.getValue().toString());
                if (morfologia != null) {
                    // Si es valida la morfologia
                    foco = diagnosticoComplemento.getClientId();
                    consulExtDto.setMorfologia(morfologia.getCveCie10Morfologia());

                    this.setDesMorfologia("M " + morfologia.getCveCie10Morfologia() + " " + morfologia.getDesCie10Morfologia());
                    morfologiaVisible = Boolean.TRUE;
                    diagnosticoAdicional.setDisabled(Boolean.FALSE);
                    primeraVezDiagAdic.setDisabled(Boolean.FALSE);
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.execute("PF('dialogMorfologia').hide();");
                } else {
                    foco = cveMorfologia.getClientId();
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.execute("foco='formDlgNegocio:idCveMorfologia';");
                    agregarError("me14_clave_no_existe");
                    //RequestContext context = RequestContext.getCurrentInstance();
                    //context.execute("PF('dialogMorfologia').show();");
                }

            } else {
                foco = cveMorfologia.getClientId();
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("foco='formDlgNegocio:idCveMorfologia';");
                // invalido reuqerido
                mensajeErrorConParametros("mecx01_requerido", "Morfología");
                //RequestContext context = RequestContext.getCurrentInstance();
                //context.execute("PF('dialogMorfologia').show();");
            }
        } else {
            // tecla ESC
            foco = diagnosticoPrincipal.getClientId();
            consulExtDto.setDiagnosticoPrincipal(null);
            consulExtDto.setDesDiagPrin(null);
            consulExtDto.setMorfologia(null);
            cveMorfologia.resetValue();
            this.getDiagnosticoPrincipal().resetValue();
            this.getDesDiagPrin().resetValue();
            this.setDesMorfologia("");


            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogMorfologia').hide();");
        }

    }

    public void validaCirugiaPrin() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = diagnosticoAdicional.getClientId();
            diagnosticoAdicional.setDisabled(Boolean.FALSE);
            primeraVezDiagAdic.setDisabled(Boolean.FALSE);
            consulExtDto.setFecCirugiaPrin((Date) fecCirugiaPrin.getValue());

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogFecCirugia').hide();");
        } else {
            this.diagnosticoAdicional.resetValue();
            consulExtDto.setDiagnosticoAdicional(null);
            consulExtDto.setDesDiagAdic(null);
            fecCirugiaPrin.resetValue();

            consulExtDto.setFecCirugiaPrin(null);

            foco = agMeDerechoPf.getClientId();
            consulExtDto.setAgregadoMedico(null);
            // RequestContext.getCurrentInstance().update(");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogEdad').hide();");
        }

    }

    public void validaTipoPrin() {
        foco = "fromDialog:btnAcepTip";
    }

    public void siguienteTipPri() {
        foco = procedimientoPrincipal.getClientId();
    }

    public void validaCirugiaAdic() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = primeraVezDiagAdic.getClientId();
            // diagnosticoComplemento.setDisabled(Boolean.FALSE);
            // primeraVezDiagComp.setDisabled(Boolean.FALSE);
            consulExtDto.setFecCirugiaAdic((Date) fecCirugiaAdic.getValue());
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogFecCirugiaAdic').hide();");
        } else {
            this.diagnosticoAdicional.resetValue();
            this.primeraVezDiagAdic.resetValue();
            consulExtDto.setDiagnosticoAdicional(null);
            consulExtDto.setDesDiagAdic(null);
            consulExtDto.setPrimeraVezDiagAdic(null);
            consulExtDto.setFecCirugiaAdic(null);
            fecCirugiaAdic.resetValue();

            foco = diagnosticoAdicional.getClientId();
            // RequestContext.getCurrentInstance().update(");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogFecCirugiaAdic').hide();");
        }
    }

    public void validaCirugiaCom() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = primeraVezDiagComp.getClientId();
            // diagnosticoComplemento.setDisabled(Boolean.FALSE);
            // primeraVezDiagComp.setDisabled(Boolean.FALSE);
            consulExtDto.setFecCirugiaComp((Date) fecCirugiaComp.getValue());
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogFecCirugiaCom').hide();");
        } else {
            // Si tecla ESC
            this.diagnosticoComplemento.resetValue();
            this.primeraVezDiagComp.resetValue();
            consulExtDto.setDiagnosticoComplemento(null);
            consulExtDto.setDesDiagComp(null);
            consulExtDto.setPrimeraVezDiagComp(null);
            consulExtDto.setFecCirugiaComp(null);
            fecCirugiaComp.resetValue();

            foco = diagnosticoComplemento.getClientId();
            // RequestContext.getCurrentInstance().update(");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogFecCirugiaCom').hide();");
        }
    }

    public void validaInfoAdicPrin() {

        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = diagnosticoAdicional.getClientId();
            diagnosticoAdicional.setDisabled(Boolean.FALSE);
            primeraVezDiagAdic.setDisabled(Boolean.FALSE);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogTipDia').hide();");
            consulExtDto.setCveTipoPrin(cveTipoPrin.getValue().toString().toUpperCase());
        } else {
            this.diagnosticoPrincipal.resetValue();
            consulExtDto.setDiagnosticoPrincipal(null);
            cveTipoPrin.resetValue();
            consulExtDto.setCveTipoPrin(null);
            foco = diagnosticoPrincipal.getClientId();
            // RequestContext.getCurrentInstance().update(");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogTipDia').hide();");

        }
    }

    public void validaProcPrin() {
        SicCie9DTO sicCie9DTO = null;
        if (this.procedimientoPrincipal != null && !this.procedimientoPrincipal.getValue().toString().isEmpty()
                && this.procedimientoPrincipal.getValue().toString().length() > 0) {

            int edad = 0;
            String desProcedimiento = null;
            if (consulExtDto.getFecNacimiento() != null) {
                edad = calcularEdadAnnos(new SimpleDateFormat("dd/MM/yyyy").format(consulExtDto.getFecNacimiento()),
                        consulExtDto.getFecha());
                sicCie9DTO = consultaExternaService.buscarProcedimiento(
                        this.procedimientoPrincipal.getValue().toString().toUpperCase(), edad,
                        this.agMeSexoPf.getValue().toString(), Boolean.TRUE);
                if (sicCie9DTO != null) {
                    desProcedimiento = sicCie9DTO.getDesCie9();
                }
            } else {
                edad = consulExtDto.getEdad();
                sicCie9DTO = consultaExternaService.buscarProcedimiento(
                        this.procedimientoPrincipal.getValue().toString().toUpperCase(), edad,
                        this.agMeSexoPf.getValue().toString(), ann);
                if (sicCie9DTO != null) {
                    desProcedimiento = sicCie9DTO.getDesCie9();
                }
            }

            if (desProcedimiento == null) {
                //TODO PROC PRINCIPAL
                resetProcPrincipal();
                agregarError("me14_clave_no_existe");
            } else {
                if (desProcedimiento.equals("X")) {
                    foco = procedimientoPrincipal.getClientId();
                    // campo = 15;
                    consulExtDto.setDesProcPrin("");
                    agregarError("me17_vlave_no_sexo");
                } else if (desProcedimiento.equals("N")) {
                    foco = procedimientoPrincipal.getClientId();
                    // campo = 15;
                    consulExtDto.setDesProcPrin("");
                    agregarError("me16_clave_no_edad");
                } else {
                    procedimientoAdicional.setDisabled(Boolean.FALSE);
                    foco = procedimientoAdicional.getClientId();
                    // campo = 16;
                    this.desProcPrin.setValue(desProcedimiento);
                    consulExtDto.setProcedimientoPrincipal(procedimientoPrincipal.getValue().toString());
                    consulExtDto.setDesProcPrin(desProcedimiento);
                }
            }
        } else {
            // foco = procedimientoAdicional.getClientId();

            // foco = procedimientoComplemento.getClientId();
//			switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
//			case 2:// URGENCIA
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					//foco = "form:btnAgregarAten";
//					if (agMeDerechoPf.getValue().toString().equals("1") || agMeRegAcPf.getValue().toString().toUpperCase().equals("OR")){
//						foco=riesgosTrabajo.getClientId();
//					}
//					// campo = 28;
//				}
//				break;
//			case 3:// URGENCIA_CIRUGIA
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					foco = riesgosTrabajo.getClientId();
//					// campo = 10;
//				}
//				break;
//			case 4:// PARAMEDICO
//				if (agMeDerechoPf.getValue().toString().equals("1") || agMeRegAcPf.getValue().toString().toUpperCase().equals("OR")){
//					foco=riesgosTrabajo.getClientId();
//				}
//				// campo = 28;
//				break;
//			default:
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					foco = riesgosTrabajo.getClientId();
//					// campo = 10;
//				}
//				break;
//			}
//
            validaFocoAfterProced();
            consulExtDto.setDesProcPrin("");
            consulExtDto.setProcedimientoPrincipal("");
            this.desProcPrin.setValue("");
            // campo = 16;
        }
    }

    public void validaDiagAdicional() {
    	logger.info("Inicia validaDiagAdicional()");
        SicCie10DTO sicCie10DTO = null;

        boolean esValidoDiagAdicional = true;
        if (this.diagnosticoAdicional != null && !this.diagnosticoAdicional.getValue().toString().isEmpty()
                && this.diagnosticoAdicional.getValue().toString().length() > 0) {
            if (!this.diagnosticoAdicional.getValue().toString().toUpperCase().equals(consulExtDto.getDiagnosticoAdicional())) {
                resetBorrarInfoAdicionalAdic();
            }
            consulExtDto.setDiagnosticoAdicional(diagnosticoAdicional.getValue().toString());
            if (!this.diagnosticoAdicional.getValue().toString().toUpperCase()
                    .equals(this.diagnosticoPrincipal.getValue().toString().toUpperCase())) {

//				if (diagnosticos(this.diagnosticoPrincipal.getValue().toString().toUpperCase())) {
//					String desDiagnostico = consultaExternaService
//							.buscarDiagMorfologia(this.diagnosticoAdicional.getValue().toString().toUpperCase());
//					if (desDiagnostico == null) {
//						foco = diagnosticoAdicional.getClientId();
//						// campo = 16;
//						consulExtDto.setDesDiagAdic("");
//						esValidoDiagAdicional=false;
//						agregarError("me14_clave_no_existe");
//					} else {
//
//						foco = primeraVezDiagAdic.getClientId();
//						// campo = 17;
//						this.desDiagAdic.setValue(desDiagnostico);
//						// VALIDA RANGO VIH B20 & B24 DIAGNOSTICO ADICIONAL
////						this.validaVIH(2);
////						this.validaTipoEmbarazo(2);
//					}
//				} else {
                int edad = 0;
                String desDiagnostico = null;
                if (consulExtDto.getFecNacimiento() != null) {
                    edad = calculoSemanasEdad(
                            new SimpleDateFormat("dd/MM/yyyy").format(consulExtDto.getFecNacimiento()));

                    sicCie10DTO = consultaExternaService.buscarDiagnostico(
                            this.diagnosticoAdicional.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), Boolean.TRUE);
                    if (sicCie10DTO != null) {
                        desDiagnostico = sicCie10DTO.getDesCie10();
                    }
                } else {
                    edad = consulExtDto.getEdad();
                    sicCie10DTO = consultaExternaService.buscarDiagnostico(
                            this.diagnosticoAdicional.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), sem);
                    if (sicCie10DTO != null) {
                        desDiagnostico = sicCie10DTO.getDesCie10();
                    }
                }

                if (desDiagnostico == null) {
                    foco = diagnosticoAdicional.getClientId();
                    // campo = 16;
                    consulExtDto.setDesDiagAdic("");
                    esValidoDiagAdicional = false;
                    agregarError("me14_clave_no_existe");
                } else {
                    if (desDiagnostico.equals("X")) {
                        foco = diagnosticoAdicional.getClientId();
                        // campo = 16;
                        consulExtDto.setDesDiagAdic("");
                        esValidoDiagAdicional = false;
                        agregarError("me17_vlave_no_sexo");
                    } else if (desDiagnostico.equals("N")) {
                        foco = diagnosticoAdicional.getClientId();
                        // campo = 16;
                        consulExtDto.setDesDiagAdic("");
                        esValidoDiagAdicional = false;
                        agregarError("me16_clave_no_edad");
                    } else {

                        foco = primeraVezDiagAdic.getClientId();
                        // campo = 17;
                        this.desDiagAdic.setValue(desDiagnostico);
                        switch (this.diagnosticoAdicional.getValue().toString().toUpperCase().substring(0, 3)) {
                            case "U58":
                                if (this.diagnosticoAdicional.getValue().toString().toUpperCase().equals("U580")
                                        || this.diagnosticoAdicional.getValue().toString().toUpperCase()
                                        .equals("U581")) {
                                    diagAdi = 2;
                                    dialogFecCirugiaAdic = Boolean.TRUE;
                                    RequestContext context = RequestContext.getCurrentInstance();
                                    context.execute("PF('dialogFecCirugiaAdic').show();");
                                } else {
                                    diagAdi = 0;
                                }
                                break;
                            default:
                                break;
                        }

                        if (activaAccidentes(this.diagnosticoAdicional.getValue().toString().toUpperCase())) {
                            diagAcc = 1;
                        } else {
                            diagAcc = 0;
                        }
                        // VALIDA RANGO VIH B20 & B24 DIAGNOSTICO ADICIONAL
//							this.validaVIH(2);
//							this.validaTipoEmbarazo(2);
                    }
                }

                if (sicCie10DTO != null) {
                    consulExtDto.setDesDiagAdic(desDiagAdic.getValue().toString());
                    if ((sicCie10DTO.getIndNotifInmediata() == 1 || sicCie10DTO.getIndNotifObligatoria() == 1)
                            && sicCie10DTO.getIndErrad() != 1) {
                        foco = "formDlgNegocio:siEpidemiaAdi";
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.execute("PF('dialogNotifInmedObligAdi').show();");
                    } else {
                        // VALIDA RANGO VIH B20 & B24 DIAGNOSTICO PRINCIPAL
                        String rangoN = "N";
                        String rangoX = "X";
//							if (!desDiagnostico.equals("N") && !desDiagnostico.equals("X")){
                        if (!rangoN.equals(desDiagnostico) && !rangoX.equals(desDiagnostico)) {
                            this.validaVIH(2);
                            this.validaTipoEmbarazo(2);
                        }

                    }
                    if (sicCie10DTO.getIndErrad() == 1) {
                        resetDiagAdicional();
                        esValidoDiagAdicional = false;
                        agregarError("me140_erradicado");
                    }
                } else {
                    esValidoDiagAdicional = false;
                }

//				}
                if (!esValidoDiagAdicional) {
                    resetDiagAdicional();
                }
            } else {
                resetDiagAdicional();
                // campo = 16;
                agregarError("me15_reg_repetido");

            }


        } else {
            if (procedimientoPrincipal.isDisabled()) {
                procedimientoPrincipal.setDisabled(Boolean.FALSE);
            }
            foco = procedimientoPrincipal.getClientId();
            consulExtDto.setDiagnosticoAdicional("");
            consulExtDto.setDesDiagAdic("");
            this.desDiagAdic.setValue("");
            primeraVezDiagAdic.resetValue();
            consulExtDto.setPrimeraVezDiagAdic(null);
            if (diagAcc <= 1) {
                diagAcc = 0;
            }
            // campo = 21;
        }
    	logger.info("Termina validaDiagAdicional()");
    }

    private boolean diagnosticos(String diagPrincipal) {
        boolean resultado = false;
        EnumDiagMorfologiasTumor[] var = EnumDiagMorfologiasTumor.values();
        int i = 0;
        while (i < var.length) {
            if (var[i].getCodigo().equals(diagPrincipal)) {
                resultado = true;
                break;
            } else {
                i++;
            }
        }

        return resultado;
    }

    public void validaPriVezAdicional() {
        // switch (diagAdi) {
        // case 1:
        // foco = "infoAdiAdi";
        // campo = 18;
        // break;
        // case 2:
        // foco = "fechaAdic";
        // campo = 20;
        // break;
        // case 3:
        // foco = "tipoAdic";
        // campo = 19;
        // break;
        // default:


        // campo = 21;
        // break;
        // }


        if (validaDiferenteEmpty(this.getPrimeraVezDiagAdic())) {
            consulExtDto.setPrimeraVezDiagAdic(Integer.parseInt(primeraVezDiagAdic.getValue().toString()));
            foco = diagnosticoComplemento.getClientId();
            diagnosticoComplemento.setDisabled(Boolean.FALSE);
            primeraVezDiagComp.setDisabled(Boolean.FALSE);
        } else {
            foco = this.getPrimeraVezDiagAdic().getClientId();
            consulExtDto.setPrimeraVezDiagAdic(null);
            mensajeErrorConParametros("mecx01_requerido", "1a. vez");
        }

    }

    public void validaInfoAdicAdic() {
        foco = "fromDialog:btnAcepTipAdic";
        // campo = 21;
    }

    public void validaProcAdi() {
        SicCie9DTO sicCie9DTO = null;
        String desProcedimiento = null;
        if (this.procedimientoAdicional != null && !this.procedimientoAdicional.getValue().toString().isEmpty()
                && procedimientoAdicional.getValue().toString().length() > 0) {
            if (!procedimientoAdicional.getValue().toString().toUpperCase()
                    .equals(procedimientoPrincipal.getValue().toString().toUpperCase())) {
                int edad = 0;
                if (consulExtDto.getFecNacimiento() != null) {
                    edad = calcularEdadAnnos(new SimpleDateFormat("dd/MM/yyyy").format(consulExtDto.getFecNacimiento()),
                            consulExtDto.getFecha());

                    sicCie9DTO = consultaExternaService.buscarProcedimiento(
                            this.procedimientoAdicional.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), Boolean.TRUE);
                    if (sicCie9DTO != null) {
                        desProcedimiento = sicCie9DTO.getDesCie9();
                    }
                } else {
                    edad = consulExtDto.getEdad();
                    sicCie9DTO = consultaExternaService.buscarProcedimiento(
                            this.procedimientoAdicional.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), ann);
                    if (sicCie9DTO != null) {
                        desProcedimiento = sicCie9DTO.getDesCie9();
                    }
                }

                if (desProcedimiento == null) {
                    resetProcAdicional();
                    //TODO PROC ADICIONAL
                    agregarError("me14_clave_no_existe");
                } else {
                    if (desProcedimiento.equals("X")) {
                        foco = procedimientoAdicional.getClientId();
                        // campo = 21;
                        consulExtDto.setDesProcAdic("");
                        agregarError("me17_vlave_no_sexo");
                    } else if (desProcedimiento.equals("N")) {
                        foco = procedimientoAdicional.getClientId();
                        // campo = 21;
                        consulExtDto.setDesProcAdic("");
                        agregarError("me16_clave_no_edad");
                    } else {
                        procedimientoComplemento.setDisabled(Boolean.FALSE);
                        foco = procedimientoComplemento.getClientId();
                        // campo = 22;
                        this.desProcAdic.setValue(desProcedimiento);
                        consulExtDto.setProcedimientoAdicional(procedimientoAdicional.getValue().toString());
                        consulExtDto.setDesProcAdic(desProcedimiento);
                    }
                }
            } else {
                foco = procedimientoAdicional.getClientId();
                // campo = 21;
                consulExtDto.setDesProcAdic("");
                consulExtDto.setProcedimientoAdicional("");
                procedimientoAdicional.resetValue();
                desProcAdic.resetValue();
                agregarError("me15_reg_repetido");
            }
        } else {
            // foco = procedimientoComplemento.getClientId();
//			switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
//			case 2:// URGENCIA
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					foco = "form:btnAgregarAten";
//					// campo = 28;
//				}
//				break;
//			case 3:// URGENCIA_CIRUGIA
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					foco = riesgosTrabajo.getClientId();
//					// campo = 10;
//				}
//				break;
//			case 4:// PARAMEDICO
//				foco = "form:btnAgregarAten";
//				// campo = 28;
//				break;
//			default:
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					foco = riesgosTrabajo.getClientId();
//					// campo = 10;
//				}
//				break;
//			}
            validaFocoAfterProced();

            consulExtDto.setDesProcAdic("");
            this.desProcAdic.setValue("");
            consulExtDto.setProcedimientoAdicional("");
            // campo = 22;
        }
    }

    public void validaDiagComp() {
    	logger.info("Inicia validaDiagComp()");

        SicCie10DTO sicCie10DTO = null;

        if (diagnosticoComplemento != null && diagnosticoComplemento.getValue().toString().length() > 0) {

            if (!this.diagnosticoComplemento.getValue().toString().toUpperCase().equals(consulExtDto.getDiagnosticoComplemento())) {
                resetBorrarInfoAdicionalComp();
            }

            consulExtDto.setDiagnosticoComplemento(diagnosticoComplemento.getValue().toString());

            if (!diagnosticoComplemento.getValue().toString().toUpperCase()
                    .equals(diagnosticoAdicional.getValue().toString().toUpperCase())
                    && !diagnosticoComplemento.getValue().toString().toUpperCase()
                    .equals(diagnosticoPrincipal.getValue().toString().toUpperCase())) {

                int edad = 0;
                String desDiagnostico = null;
                if (consulExtDto.getFecNacimiento() != null) {
                    edad = calculoSemanasEdad(
                            new SimpleDateFormat("dd/MM/yyyy").format(consulExtDto.getFecNacimiento()));

                    sicCie10DTO = consultaExternaService.buscarDiagnostico(
                            diagnosticoComplemento.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), Boolean.TRUE);
                    if (sicCie10DTO != null) {
                        desDiagnostico = sicCie10DTO.getDesCie10();
                    }
                } else {
                    edad = consulExtDto.getEdad();
                    sicCie10DTO = consultaExternaService.buscarDiagnostico(
                            diagnosticoComplemento.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), sem);
                    if (sicCie10DTO != null) {
                        desDiagnostico = sicCie10DTO.getDesCie10();
                    }
                }

                if (desDiagnostico == null) {
                    foco = diagnosticoComplemento.getClientId();
                    // campo = 22;
                    diagnosticoComplemento.resetValue();
                    consulExtDto.setDesDiagComp("");
                    agregarError("me14_clave_no_existe");
                } else {
                    if (desDiagnostico.equals("X")) {
                        foco = diagnosticoComplemento.getClientId();
                        // campo = 22;
                        consulExtDto.setDesDiagComp("");
                        agregarError("me17_vlave_no_sexo");
                    } else if (desDiagnostico.equals("N")) {
                        foco = diagnosticoComplemento.getClientId();
                        // campo = 22;
                        consulExtDto.setDesDiagComp("");
                        agregarError("me16_clave_no_edad");
                    } else {
                        // campo = 23;
                        foco = primeraVezDiagComp.getClientId();
                        consulExtDto.setDesDiagComp(desDiagnostico);

                        this.desDiagComp.setValue(desDiagnostico);
                        switch (diagnosticoComplemento.getValue().toString().toUpperCase().substring(0, 3)) {
                            case "U58":
                                if (diagnosticoComplemento.getValue().toString().toUpperCase().equals("U580")
                                        || diagnosticoComplemento.getValue().toString().toUpperCase().equals("U581")) {
                                    diagCom = 2;
                                    dialogFecCirugiaCom = Boolean.TRUE;
                                    RequestContext context = RequestContext.getCurrentInstance();
                                    context.execute("PF('dialogFecCirugiaCom').show();");
                                } else {
                                    diagCom = 0;
                                }
                                break;
                            default:
                                // campo = 23;
                                foco = primeraVezDiagComp.getClientId();
                                break;
                        }
                        if (diagAcc == 0) {
                            if (activaAccidentes(diagnosticoComplemento.getValue().toString().toUpperCase())) {
                                diagAcc = 2;
                            } else {
                                diagAcc = 0;
                            }
                        }
                        consulExtDto.setDesDiagComp(desDiagComp.getValue().toString());
                        if ((sicCie10DTO.getIndNotifInmediata() == 1 || sicCie10DTO.getIndNotifObligatoria() == 1)
                                && sicCie10DTO.getIndErrad() != 1) {
                            foco = "formDlgNegocio:siEpidemiaCom";
                            RequestContext context = RequestContext.getCurrentInstance();
                            context.execute("PF('dialogNotifInmedObligCom').show();");
                        } else {
                            // VALIDA RANGO VIH B20 & B24 DIAGNOSTICO PRINCIPAL
                            this.validaVIH(3);
                            this.validaTipoEmbarazo(3);
                        }
                        if (sicCie10DTO.getIndErrad() == 1) {
                            resetDiagComplementario();
                            agregarError("me140_erradicado");
                        }
                    }
                }
            } else {
                resetDiagComplementario();
                // campo = 22;
                agregarError("me15_reg_repetido");
            }
        } else {
            resetDiagComplementario();
            if (procedimientoPrincipal.isDisabled()) {
                procedimientoPrincipal.setDisabled(Boolean.FALSE);
            }
            foco = procedimientoPrincipal.getClientId();
            consulExtDto.setDesDiagComp("");
            this.desDiagComp.setValue("");
            consulExtDto.setDiagnosticoComplemento("");
            primeraVezDiagComp.resetValue();
            consulExtDto.setPrimeraVezDiagComp(null);
            if (diagAcc == 0 || diagAcc == 2) {
                diagAcc = 0;
            }
            // campo = 27;
        }
    	logger.info("Termina validaDiagComp()");
    }

    /**
     * @param diagnostico
     * @return
     */
    private boolean activaAccidentes(String diagnostico) {
        boolean resultado = false;
        if (diagnostico.substring(0, 1).equals("V") || diagnostico.substring(0, 1).equals("W")
                || diagnostico.substring(0, 1).equals("X") || diagnostico.substring(0, 1).equals("Y")) {
            resultado = true;
        }
        return resultado;
    }

    public void validaPriVezComp() {
        // switch (diagCom) {
        // case 1:
        // foco = "infoAdiComp";
        // campo = 24;
        // break;
        // case 2:
        // foco = "fechaComp";
        // campo = 26;
        // break;
        // case 3:
        // foco = "tipoComp";
        // campo = 25;
        // break;
        // default:

        if (validaDiferenteEmpty(this.getPrimeraVezDiagComp())) {
            consulExtDto.setPrimeraVezDiagComp(Integer.parseInt(primeraVezDiagComp.getValue().toString()));
            foco = procedimientoPrincipal.getClientId();
            if (procedimientoPrincipal.isDisabled()) {
                procedimientoPrincipal.setDisabled(Boolean.FALSE);
            }
        } else {
            foco = this.getPrimeraVezDiagComp().getClientId();
            consulExtDto.setPrimeraVezDiagComp(null);
            mensajeErrorConParametros("mecx01_requerido", "1a. vez");
        }

    }

    public void validaInfoAdicComp() {
        foco = "fromDialog:btnAcepTipCom";
        // campo = 27;
    }

    public void validaProcComp() {
        SicCie9DTO sicCie9DTO = null;
        if (procedimientoComplemento != null && procedimientoComplemento.getValue().toString().length() > 0) {
            if (!procedimientoComplemento.getValue().toString().toUpperCase()
                    .equals(procedimientoAdicional.getValue().toString().toUpperCase())
                    && !procedimientoComplemento.getValue().toString().toUpperCase()
                    .equals(procedimientoPrincipal.getValue().toString().toUpperCase())) {

                int edad = 0;
                String desProcedimiento = null;
                String cveProcComplemento = null;
                if (consulExtDto.getFecNacimiento() != null) {
                    edad = calcularEdadAnnos(new SimpleDateFormat("dd/MM/yyyy").format(consulExtDto.getFecNacimiento()),
                            consulExtDto.getFecha());
                    sicCie9DTO = consultaExternaService.buscarProcedimiento(
                            this.procedimientoComplemento.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), Boolean.TRUE);
                    if (sicCie9DTO != null) {
                        desProcedimiento = sicCie9DTO.getDesCie9();
                        cveProcComplemento = sicCie9DTO.getCveCie9();
                    }

                } else {
                    edad = consulExtDto.getEdad();
                    sicCie9DTO = consultaExternaService.buscarProcedimiento(
                            this.procedimientoComplemento.getValue().toString().toUpperCase(), edad,
                            this.agMeSexoPf.getValue().toString(), ann);
                    if (sicCie9DTO != null) {
                        desProcedimiento = sicCie9DTO.getDesCie9();
                        cveProcComplemento = sicCie9DTO.getCveCie9();

                    }
                }

                if (desProcedimiento == null) {
                    // TODO PROC COMPLEMENTARIO
                    resetProcComplementario();
                    agregarError("me14_clave_no_existe");
                } else {
                    if (desProcedimiento.equals("X")) {
                        foco = procedimientoComplemento.getClientId();
                        // campo = 27;
                        consulExtDto.setDesProcComp("");
                        agregarError("me17_vlave_no_sexo");
                    } else if (desProcedimiento.equals("N")) {
                        foco = procedimientoComplemento.getClientId();
                        // campo = 27;
                        consulExtDto.setDesProcComp("");
                        agregarError("me16_clave_no_edad");
                    } else {
                        desProcComp.setValue(desProcedimiento);
                        procedimientoComplemento.setValue(cveProcComplemento);
                        validaFocoAfterProced();
                        consulExtDto.setDesProcComp(desProcComp.getValue().toString());
                        consulExtDto.setProcedimientoComplemento(procedimientoComplemento.getValue().toString());
                    }
                }
            } else {
                foco = procedimientoComplemento.getClientId();
                // campo = 27;
                consulExtDto.setDesProcComp("");
                consulExtDto.setProcedimientoComplemento("");
                procedimientoComplemento.resetValue();
                desProcComp.resetValue();
                agregarError("me15_reg_repetido");
            }
        } else {
//			switch (Integer.parseInt(consulExtDto.getEspecialidadSelc())) {
//			case 2:// URGENCIA
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					foco = "form:btnAgregarAten";
//					// campo = 28;
//				}
//				break;
//			case 3:// URGENCIA_CIRUGIA
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					foco = riesgosTrabajo.getClientId();
//					// campo = 10;
//				}
//				break;
//			case 4:// PARAMEDICO
//				foco = "form:btnAgregarAten";
//				// campo = 28;
//				break;
//			default:
//				if (diagAcc > 0) {
//					foco = accidentesLesiones.getClientId();
//					// campo = 9;
//				} else {
//					foco = riesgosTrabajo.getClientId();
//					// campo = 10;
//				}
//				break;
//			}
            validaFocoAfterProced();
            consulExtDto.setDesProcComp("");
            desProcComp.setValue("");
            consulExtDto.setProcedimientoComplemento("");
        }
    }

    public Boolean validarConsecutivoConsulta() {
        Integer consecutivoActual = consultaExternaService.validarConsecutivoConsulta(consulExtDto);
        if (consulExtDto.getNumConsulta() <= consecutivoActual) {
            consecutivoActual++;
            contador = consecutivoActual;
            consulExtDto.setNumConsulta(contador);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogoNumConsultaRepetido').show();");
            return false;
        } else {
            return true;
        }
    }

    /**
     * insertar
     *
     * @return
     */
    public String insertarFormato4306() {
    	logger.info("Inicia insertarFormato4306()");
        try {
            validaCamposDeRepetidos();
            if (!esRepetido) {
                if (validaRequeridosAtencionDto()) {
                    validaRamaAseguramiento(this.agMeRegAcPf.getValue().toString(), this.agMeDerechoPf.getValue().toString(),
                            this.riesgosTrabajo.getValue() != null ? Integer.valueOf(this.riesgosTrabajo.getValue().toString()) : 1,
                            diagnosticoPrincipal.getValue().toString());
                    atenion = true;

                    // consulExtDto.setTipoDocumento(2);//1-> Encabezado 2-> Atencion
                    if (validaNumRecetas) {
                        if (validaDiasInca) {
                            if (!edicion) {
                                if (validarConsecutivoConsulta()) {
                                    consulExtDto.setDiagAcc(diagAcc);
                                    consulExtDto.setStpCaptura(new Date());
                                    consulExtDto.setEdad(Integer.parseInt(this.edad.getValue().toString()));
                                    if (recetas.getValue() == null || recetas.getValue().toString().isEmpty()) {
                                        consulExtDto.setRecetas(0);
                                    }
                                    if (diasIncapacidad.getValue() == null || diasIncapacidad.getValue().toString().isEmpty()) {
                                        consulExtDto.setDiasIncapacidad(0);
                                    }
                                    if (consultaExternaService.actualizar(consulExtDto)) {
                                        contador = consultaExternaService.buscar(consulExtDto, objetosSs.getUsuarioDetalleDto());
                                        consulExtDto.setNumConsulta(contador);
                                        limpiarCampos();
                                        agregarInfo("mg02_datos_almacenados");
                                    } else {
                                        agregarError("mg02_error_datos_almacenados");
                                    }

                                }


                            } else {
                                validaDiagEdicion();
                                consulExtDto.setFecCaptura(getFechaActual());
                                consulExtDto.setStpActualiza(getFechaActual());
                                consulExtDto.setDiagAcc(diagAcc);
                                consulExtDto.setEdad(Integer.parseInt(this.edad.getValue().toString()));
                                consultaExternaService.actEdicion(consulExtDto);
                                limpiarCampos();
                                consulExtDto.setNumConsulta(0);
                                objetosSs.setBusquedaCex(Boolean.TRUE);
                                agregarInfo("mg02_datos_almacenados");


                                return EnumNavegacion.CONSULTA_EXTERNA_BUSQ_ED.getValor();
                            }

                            //						objetosSs.setColorHeader(EnumColores.CONSULTA_EXTERNA.getValor());
                            //
                            //						return EnumNavegacion.CONSULTA_EXTERNA_BUSQ_ED.getValor();
                        } else {
                            agregarError("me12_dias_men28");
                        }
                    } else {
                        agregarError("me11_num_max_recetas");
                    }


                } else {
                    //agregarError("mg_requeridos_save");
                    agregarError("mg02_error_datos_almacenados");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            agregarError("mg02_error_datos_almacenados");
        }
        logger.info("Termina insertarFormato4306()");
        return null;
    }

    public String buscarEncabezado() {
    	logger.info("Inicia buscarEncabezado()");
        if (validaFecha) {
            if (validaMatricula) {
                if (validaEspecialidad) {
                    if (validaConsultorio) {
                        if (validaTipoPrestado) {
                            if (validaTurno) {
                                if (validaHoasTrab) {
                                    if (validaConsulVsCitas) {
                                        // atenion = true;
                                        try {
                                            // consulExtDto.setTipoDocumento(1);//1->
                                            // Encabezado 2->
                                            // Atencion
                                            consulExtDto.setFecCaptura(getFechaActual());
                                            contador = consultaExternaService.buscar(consulExtDto,
                                                    objetosSs.getUsuarioDetalleDto());
                                            if (contador > 0) {
                                                // dialogExiste = Boolean.FALSE;
                                                RequestContext context = RequestContext.getCurrentInstance();
                                                context.execute("PF('dialogExiste').show();");
                                            } else {
                                                contador = 1;
                                                atenion = true;
                                                listadoAgreMed = catalogosService.llenaComboAgreMed();
                                                listaRegimenStr = cargaListaString(listadoAgreMed);
                                                listadoPases = catalogosService.llenaComboPases();
                                                listadoAccLesiones = catalogosService.llenaComboAccLesiones();
                                                listadoRiesgos = catalogosService.llenaComboRiesgos();
                                                listadoTipUrgencia = catalogosService.llenaComboTipoUrgencia();
                                                consulExtDto.setNumConsulta(contador);
                                                foco = "form:idNss";
                                                metodoPf = true;
                                                muestraCampos();
                                            }
                                            // Regla: RN27 <Pacientes para
                                            // Urgencias Toco - cirugía>
                                            if (Integer.parseInt(consulExtDto
                                                    .getEspecialidadSelc()) == EnumEspecialidades.URGENCIA_CIRUGIA
                                                    .getClave()
                                                    || consulExtDto.getCveEspecialidad()
                                                    .equals(String
                                                            .valueOf(EnumEspecialidades.GINECOLOGIA.getClave()))
                                                    || consulExtDto.getCveEspecialidad().equals(String
                                                    .valueOf(EnumEspecialidades.OBSTETRICIA.getClave()))) {
                                                this.agMeSexoPf.setValue("F");
                                            }

                                            if (Integer.parseInt(
                                                    consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA
                                                    .getClave()) {
                                                consulExtDto.setPrimeraVez(1);
                                                primeraVez.setValue(1);
                                                pvez = true;
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        agregarError("me10_val_cit_incorrecto");
                                    }
                                } else {
                                    agregarError("me08_horas_rango");
                                }
                            } else {
                                mensajeErrorConParametros("mecx01_requerido", "el Turno");
                            }
                        } else {
                            mensajeErrorConParametros("mecx01_requerido", "el Tipo de Prestador");
                        }
                    } else {
                        mensajeErrorConParametros("mecx01_requerido", "el Consultorio");
                    }
                } else {
                    mensajeErrorConParametros("mecx01_requerido", "la Especialidad");
                }
            } else {
                mensajeErrorConParametros("mecx01_requerido", "la Matricula");
            }
        } else {
            mensajeErrorConParametros("mecx01_requerido", "la Fecha");
        }
        logger.info("Termina buscarEncabezado()");
        return EnumNavegacion.CONSULTA_EXTERNA.getValor();
    }

    public String iniCaptura() {
        // dialogExiste = Boolean.FALSE;
        // dialogCantidad = Boolean.FALSE;
        // dialogEdad = Boolean.FALSE;
        atenion = true;
        metodoPf = true;
        foco = null;
        foco = "form:idNss";
        listadoAgreMed = catalogosService.llenaComboAgreMed();
        listadoPases = catalogosService.llenaComboPases();
        listadoAccLesiones = catalogosService.llenaComboAccLesiones();
        listadoRiesgos = catalogosService.llenaComboRiesgos();
        listadoTipUrgencia = catalogosService.llenaComboTipoUrgencia();
        consulExtDto.setNumConsulta(contador);
        listaRegimenStr = cargaListaString(listadoAgreMed);
        muestraCampos();

        return EnumNavegacion.CONSULTA_EXTERNA.getValor();
    }

    public String limpiarCampos() {
        color = "FFFFFF";
        deshabilitado = false;
        colorNss = "FFFFFF";
        derecho = null;
        sexo = null;
        anno = null;
        regimen = null;
        deshabilitadoAgre = false;
        atenion = true;
        campo = 0;
        foco = nss.getClientId();
        metodoPf = true;
        valNombreCom = "";
        deshabilitadoNom = false;
        metodoPfCan = false;
        diagPrin = 0;
        diagAdi = 0;
        diagCom = 0;
        diagAcc = 0;
        desMorfologia = "";

        consulExtDto.setNss(null);
        consulExtDto.setAgregadoMedico(null);
        consulExtDto.setNombre(null);
        consulExtDto.setApePaterno(null);
        consulExtDto.setApeMaterno(null);
        apMat = false;
        consulExtDto.setFecNacimiento(null);
        consulExtDto.setEdad(null);
        consulExtDto.setUnidadAdscripcion(null);
        consulExtDto.setNombreUnidadMed(null);

        if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA.getClave()) {
            consulExtDto.setPrimeraVez(1);
        } else {
            consulExtDto.setPrimeraVez(null);
        }

        consulExtDto.setCitado(null);
        consulExtDto.setPase(null);
        consulExtDto.setRecetas(null);
        consulExtDto.setDiasIncapacidad(null);
        consulExtDto.setVisitas(null);
        consulExtDto.setMetodoPpf(null);
        consulExtDto.setCantidadMpf(null);
        consulExtDto.setCveTipoUrgencia(null);
        consulExtDto.setAccidentesLesiones(null);
        consulExtDto.setRiesgosTrabajo(null);
        consulExtDto.setDiagnosticoPrincipal(null);
        consulExtDto.setDesDiagPrin(null);
        consulExtDto.setProcedimientoPrincipal(null);
        consulExtDto.setDesProcPrin(null);
        consulExtDto.setDiagnosticoAdicional(null);
        consulExtDto.setDesDiagAdic(null);
        consulExtDto.setPrimeraVezDiagAdic(null);
        consulExtDto.setProcedimientoAdicional(null);
        consulExtDto.setDesProcAdic(null);
        consulExtDto.setDiagnosticoComplemento(null);
        consulExtDto.setDesDiagComp(null);
        consulExtDto.setPrimeraVezDiagComp(null);
        consulExtDto.setProcedimientoComplemento(null);
        consulExtDto.setDesProcComp(null);
        consulExtDto.setActiPerParamedico(null);
        consulExtDto.setDiagnosticoPrincipal(null);
        consulExtDto.setDesDiagPrin(null);

        resetBorrarInfoAdicionalPrin();
        resetBorrarInfoAdicionalAdic();
        resetBorrarInfoAdicionalComp();


        dialogCantidad = Boolean.FALSE;
        dialogEdad = Boolean.FALSE;
        dialogFecCirugiaPrin = Boolean.FALSE;
        dialogDiarreaPrin = Boolean.FALSE;
        dialogFecCirugiaAdic = Boolean.FALSE;
        dialogDiarreaAdic = Boolean.FALSE;
        dialogFecCirugiaCom = Boolean.FALSE;
        dialogDiarreaCom = Boolean.FALSE;

        deshabilitado = false;
        deshabilitadoA = false;
        deshabilitadoR = true;
        deshabilitadoAgre = false;
        deshabilitadoAgreA = false;
        deshabilitadoAgreR = true;
        deshabilitadoNom = false;
        deshabilitadoNomA = false;
        deshabilitadoNomR = true;
        esRepetido = Boolean.FALSE;
        limpiarBinding();
        return null;
    }

    public void limpiarBinding() {
        nss.resetValue();
        agMeDerechoPf.resetValue();
        agMeSexoPf.resetValue();
        agMeAnioPf.resetValue();
        agMeRegAcPf.resetValue();
        getAgMeRegAcPf().setStyle("");
        nombre.resetValue();
        apePaterno.resetValue();
        apeMaterno.resetValue();
        edad.resetValue();
        if (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA.getClave()) {
            primeraVez.setValue(1);
        } else {
            primeraVez.resetValue();
        }
        citado.resetValue();
        pase.resetValue();
        recetas.resetValue();
        diasIncapacidad.resetValue();
        visitas.resetValue();
        metodoPpfAcPf.resetValue();
        metodoPpfAcPf.resetValue();
        cantidadMpf.resetValue();
        accidentesLesiones.resetValue();
        riesgosTrabajo.resetValue();
        cveTipoUrgencia.resetValue();
        diagnosticoPrincipal.resetValue();
        desDiagPrin.resetValue();
        cveInforAdicPrin.resetValue();
        cveTipoPrin.resetValue();
        fecCirugiaPrin.resetValue();
        diagnosticoAdicional.resetValue();
        desDiagAdic.resetValue();
        primeraVezDiagAdic.resetValue();
        cveInforAdicAdic.resetValue();
        cveTipoAdic.resetValue();
        fecCirugiaAdic.resetValue();
        diagnosticoComplemento.resetValue();
        desDiagComp.resetValue();
        primeraVezDiagComp.resetValue();
        cveInforAdicComp.resetValue();
        cveTipoComp.resetValue();
        fecCirugiaComp.resetValue();
        procedimientoPrincipal.resetValue();
        desProcPrin.resetValue();
        procedimientoAdicional.resetValue();
        desProcAdic.resetValue();
        procedimientoComplemento.resetValue();
        desProcComp.resetValue();
        cveMorfologia.resetValue();
        cveMorfologia.resetValue();
        desMorfologia = "";
        setMorfologiaVisible(Boolean.FALSE);

        diagnosticoAdicional.setDisabled(true);
        primeraVezDiagAdic.setDisabled(true);
        diagnosticoComplemento.setDisabled(true);
        primeraVezDiagComp.setDisabled(true);
        procedimientoAdicional.setDisabled(true);
        procedimientoComplemento.setDisabled(true);
        this.setApareceRiesgoTrabajo(true);
        this.getRiesgosTrabajo().setDisabled(false);

    }

    public String limpiarEncabezado() {

        consulExtDto.setFecha(null);
        consulExtDto.setMatriculaMedico(null);
        consulExtDto.setNombreMedico(null);
        consulExtDto.setEspecialidad(null);
        consulExtDto.setTipoPrestador(null);
        consulExtDto.setConsultorio(null);
        consulExtDto.setHorasTrabajadas(null);
        consulExtDto.setTurno(null);
        consulExtDto.setConsultasNoOtorgadas(null);
        consulExtDto.setCitasNoCumplidas(null);
        posicion = 1;
        foco = fechaPf.getClientId();
        dialogExiste = Boolean.FALSE;
        deshabilitadoMedS = false;
        deshabilitadoMedA = false;
        // deshabilitadoMedR = false;
        // deshabilitadoMed = true;
        desBotonAgregar = true;

        fechaPf.resetValue();
        matriculaMedico.resetValue();
        nombreMedico.resetValue();
        especialidadAcPf.resetValue();
        tipoPrestador.resetValue();
        consultorioAcPf.resetValue();
        horasTrabajadas.resetValue();
        turno.resetValue();
        consultasNoOtorgadas.resetValue();
        citasNoCumplidas.resetValue();

        return null;
    }

    public String siguiente() {
        foco = diagnosticoPrincipal.getClientId();
        campo = 11;// Diagnostico Principal
        dialogCantidad = Boolean.FALSE;
        return null;
    }

    public String guardarNo() {
        dialogCantidad = Boolean.FALSE;
        dialogExiste = Boolean.FALSE;
        dialogCantidad = Boolean.FALSE;
        dialogEdad = Boolean.FALSE;
        dialogFecCirugiaPrin = Boolean.FALSE;
        dialogDiarreaPrin = Boolean.FALSE;
        dialogFecCirugiaAdic = Boolean.FALSE;
        dialogDiarreaAdic = Boolean.FALSE;
        dialogFecCirugiaCom = Boolean.FALSE;
        dialogDiarreaCom = Boolean.FALSE;
        foco = nss.getClientId();
        return null;
    }

    public String siguienteEdad() {
        foco = nombre.getClientId();
        return null;
    }

    public void mandaMensaje() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                .get("parametro");
        switch (parametro) {
            case "fechaInv":
                agregarError("formato_fechaInvalido");
                break;
            case "reqInfoAdicional":
                mensajeErrorConParametros("mecx01_requerido", "Informaci\u00F3n adicional");
                break;
            case "reqCantidad":
                mensajeErrorConParametros("mecx01_requerido", "Cantidad");
                break;
            case "reqEdad":
                mensajeErrorConParametros("mecx01_requerido", "Edad");
                break;
            case "rangoEdad":
                agregarError("me138_rangoEdad");
                break;


        }
    }

    public boolean isDiagnosticoViolencia(String cveDiagnostico) {
        if (cveDiagnostico.toUpperCase().startsWith("V") || cveDiagnostico.toUpperCase().startsWith("W")
                || cveDiagnostico.toUpperCase().startsWith("X") || cveDiagnostico.toUpperCase().startsWith("Y")) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * valida epidemia diagnostico adicional
     */
    public void siEpidemia() {
        if (!validaVIH(1)) {
            this.validaTipoEmbarazo(1);
        }
    }

    /**
     * valida epidemia diagnostico adicional
     */
    public void siEpidemiaAdicional() {
        if (!validaVIH(2)) {
            if (!validaTipoEmbarazo(2)) {
                foco = primeraVezDiagAdic.getClientId();
            }
        }

    }

    /**
     * valida epidemia diagnostico adicional
     */
    public void siEpidemiaComplementario() {
        if (!validaVIH(3)) {
            if (validaTipoEmbarazo(3)) {
                foco = primeraVezDiagComp.getClientId();
            }
        }

    }

    public void validaFocoAfterProced() {
        boolean focoListo = false;
        if (!focoListo && ((Integer.parseInt(consulExtDto.getEspecialidadSelc()) != 4 && diagAcc != 0) && !accidentesLesiones.isDisabled())) {
            foco = accidentesLesiones.getClientId();
            focoListo = true;
        }
        if (!focoListo && (muestraRiesgoTrabajo && !riesgosTrabajo.isDisabled())) {
            foco = riesgosTrabajo.getClientId();
            focoListo = true;
        }
        if (!focoListo) {
            foco = "form:btnGuardarEdit";
            RequestContext context = RequestContext.getCurrentInstance();
            if (paraEdicion) {
                context.execute("document.getElementById('form:btnGuardarEdit').focus();");
            } else {
                context.execute("document.getElementById('form:btnAgregarAten').focus();");
            }
        }

    }

    public void validaFocoAccidentesLesiones() {
        boolean focoListo = false;
        if (!focoListo && (muestraRiesgoTrabajo && !riesgosTrabajo.isDisabled())) {
            foco = riesgosTrabajo.getClientId();
            focoListo = true;
        }
        if (!focoListo) {
            foco = "form:btnGuardarEdit";
            RequestContext context = RequestContext.getCurrentInstance();
            if (paraEdicion) {
                context.execute("document.getElementById('form:btnGuardarEdit').focus();");
            } else {
                context.execute("document.getElementById('form:btnAgregarAten').focus();");
            }
        }

    }


    public void validaFocoAfterAtencion(String esEste) {

        boolean focoListo = false;
        //ESTA EN primeraVez
        if (esEste.equals(primeraVez.getClientId()) && !focoListo) {
            //Tipo Urgencia
            if (!focoListo && focoTipoUrgencia()) {
                foco = cveTipoUrgencia.getClientId();
                focoListo = true;
            }
            //citado
            if (!focoListo && focoCitado()) {
                foco = citado.getClientId();
                focoListo = true;
            }

        }
        //ESTA EN citado
        if (esEste.equals(citado.getClientId()) && !focoListo) {
            //pase
            if (!focoListo && focoPase()) {
                foco = pase.getClientId();
                focoListo = true;
            }
            //vista
            if (!focoListo && focoVisita()) {
                foco = visitas.getClientId();
                focoListo = true;
            }
        }
        //ESTA EN recetas
        if (esEste.equals(recetas.getClientId()) && !focoListo) {
            //visita
            if (!focoListo && focoVisita()) {
                foco = visitas.getClientId();
                focoListo = true;
            }
            //dias incapacidad
            if (!focoListo && focoDiasIncapacidad()) {
                foco = diasIncapacidad.getClientId();
                focoListo = true;
            }
            //diagnostico principal

        }
        //ESTA EN Visitas
        if (esEste.equals(visitas.getClientId()) && !focoListo) {
            //dias incapacidad
            if (!focoListo && focoDiasIncapacidad()) {
                foco = diasIncapacidad.getClientId();
                focoListo = true;
            }
            //metodo ppf
            if (!focoListo && focoMetodoPpf()) {
                foco = metodoPpfAcPf.getClientId();
                focoListo = true;
            }
        }
        //ESTA EN Dias incapacidad
        if (esEste.equals(diasIncapacidad.getClientId()) && !focoListo) {
            //metodo ppf
            if (!focoListo && focoMetodoPpf()) {
                foco = metodoPpfAcPf.getClientId();
                focoListo = true;
            }
        }
        if (!focoListo) {
            foco = diagnosticoPrincipal.getClientId();
        }

    }

    private boolean focoTipoUrgencia() {
        return (muestraTipoUrgencia && !cveTipoUrgencia.isDisabled());
    }

    private boolean focoCitado() {
        return (muestraCitado && !citado.isDisabled());
    }

    private boolean focoPase() {
        return (muestraPase && !pase.isDisabled());
    }

    private boolean focoVisita() {
        return (muestraVisita && !visitas.isDisabled());
    }

    private boolean focoDiasIncapacidad() {

        String derechoF = "";
//		sexo = consulExtDto.getAgregadoMedico().substring(1, 2);
//		anno = consulExtDto.getAgregadoMedico().substring(2, 6);
        String regimenF = "";
        if (consulExtDto.getAgregadoMedico() != null) {
            derechoF = consulExtDto.getAgregadoMedico().substring(0, 1);
            regimenF = consulExtDto.getAgregadoMedico().substring(6);

            if (derechoF.equals("1") && regimenF.toUpperCase().equals("OR")) {
                return ((Integer.parseInt(consulExtDto.getEspecialidadSelc()) != 4) && (this.isDiasInca()) && !diasIncapacidad.isDisabled());
            }
        } else if (agMeDerechoPf != null && agMeRegAcPf != null) {
            if (agMeDerechoPf.getValue().toString().equals("1") && agMeRegAcPf.getValue().toString().toUpperCase().equals("OR")) {
                return ((Integer.parseInt(consulExtDto.getEspecialidadSelc()) != 4) && (this.isDiasInca()) && !diasIncapacidad.isDisabled());
            }
        }
        return false;

    }

    private boolean focoMetodoPpf() {
        int anios = 0;
        if (consulExtDto.getEdad() != null) {
            anios = consulExtDto.getEdad();
        } else if (agMeAnioPf != null) {
            anios = calcularEdadAnnos(agMeAnioPf.getValue().toString(), consulExtDto.getFecCaptura());
        }
        return verMetodoPlanFamiliar && anios >= 10 && anios <= 59 && !metodoPpfAcPf.isDisabled();
    }

    private void validaDiagEdicion() {
        if (this.diagnosticoPrincipal.getValue() != null && !this.diagnosticoPrincipal.getValue().toString().isEmpty()) {
            if (this.cveInforAdicPrin.getValue() != null && !this.cveInforAdicPrin.getValue().toString().isEmpty()) {
                if (this.cveTipoInformacionAdicDiagP == EnumTipoInformacionAdicional.MORFOLOGIA.getValor()
                        && consulExtDto.getMorfologia() == null) {
                    consulExtDto.setMorfologia(this.cveInforAdicPrin.getValue().toString());
                }
                if (this.cveTipoInformacionAdicDiagP == EnumTipoInformacionAdicional.EMBARAZO.getValor()
                        && consulExtDto.getCveTipoEmbarazo() == null) {
                    consulExtDto.setCveTipoEmbarazo(Integer.valueOf(this.cveInforAdicPrin.getValue().toString()));
                }
                if (this.cveTipoInformacionAdicDiagP == EnumTipoInformacionAdicional.VIH.getValor()
                        && consulExtDto.getCveCD4Principal() == null) {
                    consulExtDto.setCveCD4Principal(Integer.valueOf(this.cveInforAdicPrin.getValue().toString()));
                }
            }
        }
        if (this.diagnosticoAdicional.getValue() != null && !this.diagnosticoAdicional.getValue().toString().isEmpty()) {
            if (this.cveInforAdicAdic.getValue() != null && !this.cveInforAdicAdic.getValue().toString().isEmpty()) {
                if (this.cveTipoInformacionAdicDiagA == EnumTipoInformacionAdicional.EMBARAZO.getValor()
                        && consulExtDto.getCveTipoEmbarazoAdicional() == null) {
                    consulExtDto.setCveTipoEmbarazoAdicional(Integer.valueOf(this.cveInforAdicAdic.getValue().toString()));
                }
                if (this.cveTipoInformacionAdicDiagA == EnumTipoInformacionAdicional.VIH.getValor()
                        && consulExtDto.getCveCD4Adicional() == null) {
                    consulExtDto.setCveCD4Adicional(Integer.valueOf(this.cveInforAdicAdic.getValue().toString()));
                }

            }
        }
        if (this.diagnosticoComplemento.getValue() != null && !this.diagnosticoComplemento.getValue().toString().isEmpty()) {
            if (this.cveInforAdicComp.getValue() != null && !this.cveInforAdicComp.getValue().toString().isEmpty()) {
                if (this.cveTipoInformacionAdicDiagC == EnumTipoInformacionAdicional.EMBARAZO.getValor()
                        && consulExtDto.getCveTipoEmbarazoComplementario() == null) {
                    consulExtDto.setCveTipoEmbarazoComplementario(Integer.valueOf(this.cveInforAdicComp.getValue().toString()));
                }
                if (this.cveTipoInformacionAdicDiagC == EnumTipoInformacionAdicional.VIH.getValor()
                        && consulExtDto.getCveCD4Compl() == null) {
                    consulExtDto.setCveCD4Compl(Integer.valueOf(this.cveInforAdicComp.getValue().toString()));
                }
            }
        }
    }


    /**
     * @return the consulExtDto
     */
    public ConsultaExternaDto getConsulExtDto() {
        return consulExtDto;
    }

    /**
     * @param consulExtDto the consulExtDto to set
     */
    public void setConsulExtDto(ConsultaExternaDto consulExtDto) {
        this.consulExtDto = consulExtDto;
    }

    /**
     * @return the catalogosService
     */
    public CatalogosService getCatalogosService() {
        return catalogosService;
    }

    /**
     * @param catalogosService the catalogosService to set
     */
    public void setCatalogosService(CatalogosService catalogosService) {
        this.catalogosService = catalogosService;
    }

    /**
     * @return the consultaExternaService
     */
    public ConsultaExternaService getConsultaExternaService() {
        return consultaExternaService;
    }

    /**
     * @param consultaExternaService the consultaExternaService to set
     */
    public void setConsultaExternaService(ConsultaExternaService consultaExternaService) {
        this.consultaExternaService = consultaExternaService;
    }

    /**
     * @return the listadoEspecialidad
     */
    public List<CatalogoDto> getListadoEspecialidad() {
        return listadoEspecialidad;
    }

    /**
     * @param listadoEspecialidad the listadoEspecialidad to set
     */
    public void setListadoEspecialidad(List<CatalogoDto> listadoEspecialidad) {
        this.listadoEspecialidad = listadoEspecialidad;
    }

    /**
     * @return the listadoTurnos
     */
    public List<CatalogoDto> getListadoTurnos() {
        return listadoTurnos;
    }

    /**
     * @param listadoTurnos the listadoTurnos to set
     */
    public void setListadoTurnos(List<CatalogoDto> listadoTurnos) {
        this.listadoTurnos = listadoTurnos;
    }

    /**
     * @return the listadoTipoPrestador
     */
    public List<CatalogoDto> getListadoTipoPrestador() {
        return listadoTipoPrestador;
    }

    /**
     * @param listadoTipoPrestador the listadoTipoPrestador to set
     */
    public void setListadoTipoPrestador(List<CatalogoDto> listadoTipoPrestador) {
        this.listadoTipoPrestador = listadoTipoPrestador;
    }

    /**
     * @return the listadoAgreMed
     */
    public List<CatalogoDto> getListadoAgreMed() {
        return listadoAgreMed;
    }

    /**
     * @param listadoAgreMed the listadoAgreMed to set
     */
    public void setListadoAgreMed(List<CatalogoDto> listadoAgreMed) {
        this.listadoAgreMed = listadoAgreMed;
    }

    /**
     * @return the listadoPases
     */
    public List<CatalogoDto> getListadoPases() {
        return listadoPases;
    }

    /**
     * @param listadoPases the listadoPases to set
     */
    public void setListadoPases(List<CatalogoDto> listadoPases) {
        this.listadoPases = listadoPases;
    }

    /**
     * @return the listadoMetPPF
     */
    public List<CatalogoDto> getListadoMetPPF() {
        return listadoMetPPF;
    }

    /**
     * @param listadoMetPPF the listadoMetPPF to set
     */
    public void setListadoMetPPF(List<CatalogoDto> listadoMetPPF) {
        this.listadoMetPPF = listadoMetPPF;
    }

    /**
     * @return the listadoAccLesiones
     */
    public List<CatalogoDto> getListadoAccLesiones() {
        return listadoAccLesiones;
    }

    /**
     * @param listadoAccLesiones the listadoAccLesiones to set
     */
    public void setListadoAccLesiones(List<CatalogoDto> listadoAccLesiones) {
        this.listadoAccLesiones = listadoAccLesiones;
    }

    /**
     * @return the listadoRiesgos
     */
    public List<CatalogoDto> getListadoRiesgos() {
        return listadoRiesgos;
    }

    /**
     * @param listadoRiesgos the listadoRiesgos to set
     */
    public void setListadoRiesgos(List<CatalogoDto> listadoRiesgos) {
        this.listadoRiesgos = listadoRiesgos;
    }

    /**
     * @return the deshabilitado
     */
    public boolean isDeshabilitado() {
        return deshabilitado;
    }

    /**
     * @param deshabilitado the deshabilitado to set
     */
    public void setDeshabilitado(boolean deshabilitado) {
        this.deshabilitado = deshabilitado;
    }

    /**
     * @return the deshabilitadoAgre
     */
    public boolean isDeshabilitadoAgre() {
        return deshabilitadoAgre;
    }

    /**
     * @param deshabilitadoAgre the deshabilitadoAgre to set
     */
    public void setDeshabilitadoAgre(boolean deshabilitadoAgre) {
        this.deshabilitadoAgre = deshabilitadoAgre;
    }

    /**
     * @return the deshabilitadoNom
     */
    public boolean isDeshabilitadoNom() {
        return deshabilitadoNom;
    }

    /**
     * @param deshabilitadoNom the deshabilitadoNom to set
     */
    public void setDeshabilitadoNom(boolean deshabilitadoNom) {
        this.deshabilitadoNom = deshabilitadoNom;
    }

    /**
     * @return the datosDerecho
     */
    public boolean isDatosDerecho() {
        return datosDerecho;
    }

    /**
     * @param datosDerecho the datosDerecho to set
     */
    public void setDatosDerecho(boolean datosDerecho) {
        this.datosDerecho = datosDerecho;
    }

    /**
     * @return the datosUniAds
     */
    public boolean isDatosUniAds() {
        return datosUniAds;
    }

    /**
     * @param datosUniAds the datosUniAds to set
     */
    public void setDatosUniAds(boolean datosUniAds) {
        this.datosUniAds = datosUniAds;
    }

    /**
     * @return the atenion
     */
    public boolean isAtenion() {
        return atenion;
    }

    /**
     * @param atenion the atenion to set
     */
    public void setAtenion(boolean atenion) {
        this.atenion = atenion;
    }

    /**
     * @return the validaMatricula
     */
    public boolean isValidaMatricula() {
        return validaMatricula;
    }

    /**
     * @param validaMatricula the validaMatricula to set
     */
    public void setValidaMatricula(boolean validaMatricula) {
        this.validaMatricula = validaMatricula;
    }

    /**
     * @return the validaHoasTrab
     */
    public boolean isValidaHoasTrab() {
        return validaHoasTrab;
    }

    /**
     * @param validaHoasTrab the validaHoasTrab to set
     */
    public void setValidaHoasTrab(boolean validaHoasTrab) {
        this.validaHoasTrab = validaHoasTrab;
    }

    /**
     * @return the validaConsulVsCitas
     */
    public boolean isValidaConsulVsCitas() {
        return validaConsulVsCitas;
    }

    /**
     * @param validaConsulVsCitas the validaConsulVsCitas to set
     */
    public void setValidaConsulVsCitas(boolean validaConsulVsCitas) {
        this.validaConsulVsCitas = validaConsulVsCitas;
    }

    /**
     * @return the contador
     */
    public Integer getContador() {
        return contador;
    }

    /**
     * @param contador the contador to set
     */
    public void setContador(Integer contador) {
        this.contador = contador;
    }

    /**
     * @return the auxConsultasNo
     */
    public Integer getAuxConsultasNo() {
        return auxConsultasNo;
    }

    /**
     * @param auxConsultasNo the auxConsultasNo to set
     */
    public void setAuxConsultasNo(Integer auxConsultasNo) {
        this.auxConsultasNo = auxConsultasNo;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the colorNss
     */
    public String getColorNss() {
        return colorNss;
    }

    /**
     * @param colorNss the colorNss to set
     */
    public void setColorNss(String colorNss) {
        this.colorNss = colorNss;
    }

    /**
     * @return the derecho
     */
    public String getDerecho() {
        return derecho;
    }

    /**
     * @param derecho the derecho to set
     */
    public void setDerecho(String derecho) {
        this.derecho = derecho;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the anno
     */
    public String getAnno() {
        return anno;
    }

    /**
     * @param anno the anno to set
     */
    public void setAnno(String anno) {
        this.anno = anno;
    }

    /**
     * @return the regimen
     */
    public String getRegimen() {
        return regimen;
    }

    /**
     * @param regimen the regimen to set
     */
    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    /**
     * @return the valNombreCom
     */
    public String getValNombreCom() {
        return valNombreCom;
    }

    /**
     * @param valNombreCom the valNombreCom to set
     */
    public void setValNombreCom(String valNombreCom) {
        this.valNombreCom = valNombreCom;
    }

    public ObjetosEnSesionBean getObjetosSs() {
        return objetosSs;
    }

    public void setObjetosSs(ObjetosEnSesionBean objetosSs) {
        this.objetosSs = objetosSs;
    }

    /**
     * @return the listadoTipUrgencia
     */
    public List<CatalogoDto> getListadoTipUrgencia() {
        return listadoTipUrgencia;
    }

    /**
     * @param listadoTipUrgencia the listadoTipUrgencia to set
     */
    public void setListadoTipUrgencia(List<CatalogoDto> listadoTipUrgencia) {
        this.listadoTipUrgencia = listadoTipUrgencia;
    }

    /**
     * @return the validaFecha
     */
    public boolean isValidaFecha() {
        return validaFecha;
    }

    /**
     * @param validaFecha the validaFecha to set
     */
    public void setValidaFecha(boolean validaFecha) {
        this.validaFecha = validaFecha;
    }

    public boolean isValidaNumRecetas() {
        return validaNumRecetas;
    }

    public void setValidaNumRecetas(boolean validaNumRecetas) {
        this.validaNumRecetas = validaNumRecetas;
    }

    public boolean isValidaDiasInca() {
        return validaDiasInca;
    }

    public void setValidaDiasInca(boolean validaDiasInca) {
        this.validaDiasInca = validaDiasInca;
    }

    public boolean isUrgencia() {
        return urgencia;
    }

    public void setUrgencia(boolean urgencia) {
        this.urgencia = urgencia;
    }

    public boolean isDiasInca() {
        return diasInca;
    }

    public void setDiasInca(boolean diasInca) {
        this.diasInca = diasInca;
    }

    /**
     * @return the posicion
     */
    public Integer getPosicion() {
        return posicion;
    }

    /**
     * @param posicion the posicion to set
     */
    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    /**
     * @return the foco
     */
    public String getFoco() {
        return foco;
    }

    /**
     * @param foco the foco to set
     */
    public void setFoco(String foco) {
        this.foco = foco;
    }

    /**
     * @return the campo
     */
    public Integer getCampo() {
        return campo;
    }

    /**
     * @param campo the campo to set
     */
    public void setCampo(Integer campo) {
        this.campo = campo;
    }

    /**
     * @return the lista1Vez
     */
    public List<CatalogoDto> getLista1Vez() {
        return lista1Vez;
    }

    /**
     * @param lista1Vez the lista1Vez to set
     */
    public void setLista1Vez(List<CatalogoDto> lista1Vez) {
        this.lista1Vez = lista1Vez;
    }

    /**
     * @return the listaInfoAdicional
     */
    public List<SdInformacionAdicional> getListaInfoAdicional() {
        return listaInfoAdicional;
    }

    /**
     * @param listaInfoAdicional the listaInfoAdicional to set
     */
    public void setListaInfoAdicional(List<SdInformacionAdicional> listaInfoAdicional) {
        this.listaInfoAdicional = listaInfoAdicional;
    }

    /**
     * @return the listaTipoDiarrea
     */
    public List<SdTipoDiarrea> getListaTipoDiarrea() {
        return listaTipoDiarrea;
    }

    /**
     * @param listaTipoDiarrea the listaTipoDiarrea to set
     */
    public void setListaTipoDiarrea(List<SdTipoDiarrea> listaTipoDiarrea) {
        this.listaTipoDiarrea = listaTipoDiarrea;
    }

    /**
     * @return the accederService
     */
    public AccederWsService getAccederService() {
        return accederService;
    }

    /**
     * @param accederService the accederService to set
     */
    public void setAccederService(AccederWsService accederService) {
        this.accederService = accederService;
    }

    /**
     * @return the metodoPf
     */
    public boolean isMetodoPf() {
        return metodoPf;
    }

    /**
     * @param metodoPf the metodoPf to set
     */
    public void setMetodoPf(boolean metodoPf) {
        this.metodoPf = metodoPf;
    }

    /**
     * @return the metodoPfCan
     */
    public boolean isMetodoPfCan() {
        return metodoPfCan;
    }

    /**
     * @param metodoPfCan the metodoPfCan to set
     */
    public void setMetodoPfCan(boolean metodoPfCan) {
        this.metodoPfCan = metodoPfCan;
    }

    /**
     * @return the diagPrin
     */
    public Integer getDiagPrin() {
        return diagPrin;
    }

    /**
     * @param diagPrin the diagPrin to set
     */
    public void setDiagPrin(Integer diagPrin) {
        this.diagPrin = diagPrin;
    }

    /**
     * @return the diagAdi
     */
    public Integer getDiagAdi() {
        return diagAdi;
    }

    /**
     * @param diagAdi the diagAdi to set
     */
    public void setDiagAdi(Integer diagAdi) {
        this.diagAdi = diagAdi;
    }

    /**
     * @return the diagCom
     */
    public Integer getDiagCom() {
        return diagCom;
    }

    /**
     * @param diagCom the diagCom to set
     */
    public void setDiagCom(Integer diagCom) {
        this.diagCom = diagCom;
    }

    /**
     * @return the diagAcc
     */
    public Integer getDiagAcc() {
        return diagAcc;
    }

    /**
     * @param diagAcc the diagAcc to set
     */
    public void setDiagAcc(Integer diagAcc) {
        this.diagAcc = diagAcc;
    }

    /**
     * @return the fechaNac
     */
    public boolean isFechaNac() {
        return fechaNac;
    }

    /**
     * @param fechaNac the fechaNac to set
     */
    public void setFechaNac(boolean fechaNac) {
        this.fechaNac = fechaNac;
    }

    /**
     * @return the edicion
     */
    public boolean isEdicion() {
        return edicion;
    }

    /**
     * @param edicion the edicion to set
     */
    public void setEdicion(boolean edicion) {
        this.edicion = edicion;
    }

    /**
     * @return the editable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param editable the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    /**
     * @return the deshabilitadoA
     */
    public boolean isDeshabilitadoA() {
        return deshabilitadoA;
    }

    /**
     * @param deshabilitadoA the deshabilitadoA to set
     */
    public void setDeshabilitadoA(boolean deshabilitadoA) {
        this.deshabilitadoA = deshabilitadoA;
    }

    /**
     * @return the deshabilitadoR
     */
    public boolean isDeshabilitadoR() {
        return deshabilitadoR;
    }

    /**
     * @param deshabilitadoR the deshabilitadoR to set
     */
    public void setDeshabilitadoR(boolean deshabilitadoR) {
        this.deshabilitadoR = deshabilitadoR;
    }

    /**
     * @return the pvez
     */
    public boolean isPvez() {
        return pvez;
    }

    /**
     * @param pvez the pvez to set
     */
    public void setPvez(boolean pvez) {
        this.pvez = pvez;
    }

    /**
     * @return the deshabilitadoAgreA
     */
    public boolean isDeshabilitadoAgreA() {
        return deshabilitadoAgreA;
    }

    /**
     * @param deshabilitadoAgreA the deshabilitadoAgreA to set
     */
    public void setDeshabilitadoAgreA(boolean deshabilitadoAgreA) {
        this.deshabilitadoAgreA = deshabilitadoAgreA;
    }

    /**
     * @return the deshabilitadoAgreR
     */
    public boolean isDeshabilitadoAgreR() {
        return deshabilitadoAgreR;
    }

    /**
     * @param deshabilitadoAgreR the deshabilitadoAgreR to set
     */
    public void setDeshabilitadoAgreR(boolean deshabilitadoAgreR) {
        this.deshabilitadoAgreR = deshabilitadoAgreR;
    }

    /**
     * @return the deshabilitadoNomA
     */
    public boolean isDeshabilitadoNomA() {
        return deshabilitadoNomA;
    }

    /**
     * @param deshabilitadoNomA the deshabilitadoNomA to set
     */
    public void setDeshabilitadoNomA(boolean deshabilitadoNomA) {
        this.deshabilitadoNomA = deshabilitadoNomA;
    }

    /**
     * @return the deshabilitadoNomR
     */
    public boolean isDeshabilitadoNomR() {
        return deshabilitadoNomR;
    }

    /**
     * @param deshabilitadoNomR the deshabilitadoNomR to set
     */
    public void setDeshabilitadoNomR(boolean deshabilitadoNomR) {
        this.deshabilitadoNomR = deshabilitadoNomR;
    }

    /**
     * @return the deshabilitadoMedS
     */
    public boolean isDeshabilitadoMedS() {
        return deshabilitadoMedS;
    }

    /**
     * @param deshabilitadoMedS the deshabilitadoMedS to set
     */
    public void setDeshabilitadoMedS(boolean deshabilitadoMedS) {
        this.deshabilitadoMedS = deshabilitadoMedS;
    }

    /**
     * @return the deshabilitadoMedA
     */
    public boolean isDeshabilitadoMedA() {
        return deshabilitadoMedA;
    }

    /**
     * @param deshabilitadoMedA the deshabilitadoMedA to set
     */
    public void setDeshabilitadoMedA(boolean deshabilitadoMedA) {
        this.deshabilitadoMedA = deshabilitadoMedA;
    }

    /**
     * @return the deshabilitadoMedR
     */
    public boolean isDeshabilitadoMedR() {
        return deshabilitadoMedR;
    }

    /**
     * @param deshabilitadoMedR the deshabilitadoMedR to set
     */
    public void setDeshabilitadoMedR(boolean deshabilitadoMedR) {
        this.deshabilitadoMedR = deshabilitadoMedR;
    }

    /**
     * @return the deshabilitadoMed
     */
    public boolean isDeshabilitadoMed() {
        return deshabilitadoMed;
    }

    /**
     * @param deshabilitadoMed the deshabilitadoMed to set
     */
    public void setDeshabilitadoMed(boolean deshabilitadoMed) {
        this.deshabilitadoMed = deshabilitadoMed;
    }

    /**
     * @return the validaEspecialidad
     */
    public boolean isValidaEspecialidad() {
        return validaEspecialidad;
    }

    /**
     * @param validaEspecialidad the validaEspecialidad to set
     */
    public void setValidaEspecialidad(boolean validaEspecialidad) {
        this.validaEspecialidad = validaEspecialidad;
    }

    /**
     * @return the validaTipoPrestado
     */
    public boolean isValidaTipoPrestado() {
        return validaTipoPrestado;
    }

    /**
     * @param validaTipoPrestado the validaTipoPrestado to set
     */
    public void setValidaTipoPrestado(boolean validaTipoPrestado) {
        this.validaTipoPrestado = validaTipoPrestado;
    }

    /**
     * @return the validaConsultorio
     */
    public boolean isValidaConsultorio() {
        return validaConsultorio;
    }

    /**
     * @param validaConsultorio the validaConsultorio to set
     */
    public void setValidaConsultorio(boolean validaConsultorio) {
        this.validaConsultorio = validaConsultorio;
    }

    /**
     * @return the validaTurno
     */
    public boolean isValidaTurno() {
        return validaTurno;
    }

    /**
     * @param validaTurno the validaTurno to set
     */
    public void setValidaTurno(boolean validaTurno) {
        this.validaTurno = validaTurno;
    }

    /**
     * @return the desBotonAgregar
     */
    public boolean isDesBotonAgregar() {
        return desBotonAgregar;
    }

    /**
     * @param desBotonAgregar the desBotonAgregar to set
     */
    public void setDesBotonAgregar(boolean desBotonAgregar) {
        this.desBotonAgregar = desBotonAgregar;
    }

    /**
     * @return the dialogFecCirugiaPrin
     */
    public Boolean getDialogFecCirugiaPrin() {
        return dialogFecCirugiaPrin;
    }

    /**
     * @param dialogFecCirugiaPrin the dialogFecCirugiaPrin to set
     */
    public void setDialogFecCirugiaPrin(Boolean dialogFecCirugiaPrin) {
        this.dialogFecCirugiaPrin = dialogFecCirugiaPrin;
    }

    /**
     * @return the dialogDiarreaPrin
     */
    public Boolean getDialogDiarreaPrin() {
        return dialogDiarreaPrin;
    }

    /**
     * @param dialogDiarreaPrin the dialogDiarreaPrin to set
     */
    public void setDialogDiarreaPrin(Boolean dialogDiarreaPrin) {
        this.dialogDiarreaPrin = dialogDiarreaPrin;
    }

    /**
     * @return the dialogFecCirugiaAdic
     */
    public Boolean getDialogFecCirugiaAdic() {
        return dialogFecCirugiaAdic;
    }

    /**
     * @param dialogFecCirugiaAdic the dialogFecCirugiaAdic to set
     */
    public void setDialogFecCirugiaAdic(Boolean dialogFecCirugiaAdic) {
        this.dialogFecCirugiaAdic = dialogFecCirugiaAdic;
    }

    /**
     * @return the dialogDiarreaAdic
     */
    public Boolean getDialogDiarreaAdic() {
        return dialogDiarreaAdic;
    }

    /**
     * @param dialogDiarreaAdic the dialogDiarreaAdic to set
     */
    public void setDialogDiarreaAdic(Boolean dialogDiarreaAdic) {
        this.dialogDiarreaAdic = dialogDiarreaAdic;
    }

    /**
     * @return the dialogFecCirugiaCom
     */
    public Boolean getDialogFecCirugiaCom() {
        return dialogFecCirugiaCom;
    }

    /**
     * @param dialogFecCirugiaCom the dialogFecCirugiaCom to set
     */
    public void setDialogFecCirugiaCom(Boolean dialogFecCirugiaCom) {
        this.dialogFecCirugiaCom = dialogFecCirugiaCom;
    }

    /**
     * @return the dialogDiarreaCom
     */
    public Boolean getDialogDiarreaCom() {
        return dialogDiarreaCom;
    }

    /**
     * @param dialogDiarreaCom the dialogDiarreaCom to set
     */
    public void setDialogDiarreaCom(Boolean dialogDiarreaCom) {
        this.dialogDiarreaCom = dialogDiarreaCom;
    }

    /**
     * @return the sem
     */
    public boolean isSem() {
        return sem;
    }

    /**
     * @param sem the sem to set
     */
    public void setSem(boolean sem) {
        this.sem = sem;
    }

    /**
     * @return the ann
     */
    public boolean isAnn() {
        return ann;
    }

    /**
     * @param ann the ann to set
     */
    public void setAnn(boolean ann) {
        this.ann = ann;
    }

    /**
     * @return the listadoConsultorio
     */
    public List<CatalogoDto> getListadoConsultorio() {
        return listadoConsultorio;
    }

    /**
     * @param listadoConsultorio the listadoConsultorio to set
     */
    public void setListadoConsultorio(List<CatalogoDto> listadoConsultorio) {
        this.listadoConsultorio = listadoConsultorio;
    }

    /**
     * @return the fechaPf
     */
    public InputMask getFechaPf() {
        return fechaPf;
    }

    /**
     * @param fechaPf the fechaPf to set
     */
    public void setFechaPf(InputMask fechaPf) {
        this.fechaPf = fechaPf;
    }

    /**
     * @return the matriculaMedico
     */
    public InputText getMatriculaMedico() {
        return matriculaMedico;
    }

    /**
     * @param matriculaMedico the matriculaMedico to set
     */
    public void setMatriculaMedico(InputText matriculaMedico) {
        this.matriculaMedico = matriculaMedico;
    }

    /**
     * @return the nombreMedico
     */
    public InputText getNombreMedico() {
        return nombreMedico;
    }

    /**
     * @param nombreMedico the nombreMedico to set
     */
    public void setNombreMedico(InputText nombreMedico) {
        this.nombreMedico = nombreMedico;
    }

    /**
     * @return the tipoPrestador
     */
    public SelectOneMenu getTipoPrestador() {
        return tipoPrestador;
    }

    /**
     * @param tipoPrestador the tipoPrestador to set
     */
    public void setTipoPrestador(SelectOneMenu tipoPrestador) {
        this.tipoPrestador = tipoPrestador;
    }

    /**
     * @return the horasTrabajadas
     */
    public InputMask getHorasTrabajadas() {
        return horasTrabajadas;
    }

    /**
     * @param horasTrabajadas the horasTrabajadas to set
     */
    public void setHorasTrabajadas(InputMask horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    /**
     * @return the turno
     */
    public SelectOneMenu getTurno() {
        return turno;
    }

    /**
     * @param turno the turno to set
     */
    public void setTurno(SelectOneMenu turno) {
        this.turno = turno;
    }

    /**
     * @return the consultasNoOtorgadas
     */
    public InputText getConsultasNoOtorgadas() {
        return consultasNoOtorgadas;
    }

    /**
     * @param consultasNoOtorgadas the consultasNoOtorgadas to set
     */
    public void setConsultasNoOtorgadas(InputText consultasNoOtorgadas) {
        this.consultasNoOtorgadas = consultasNoOtorgadas;
    }

    /**
     * @return the citasNoCumplidas
     */
    public InputText getCitasNoCumplidas() {
        return citasNoCumplidas;
    }

    /**
     * @param citasNoCumplidas the citasNoCumplidas to set
     */
    public void setCitasNoCumplidas(InputText citasNoCumplidas) {
        this.citasNoCumplidas = citasNoCumplidas;
    }

    /**
     * @return the nss
     */
    public InputText getNss() {
        return nss;
    }

    /**
     * @param nss the nss to set
     */
    public void setNss(InputText nss) {
        this.nss = nss;
    }

    /**
     * @return the agMeDerechoPf
     */
    public InputText getAgMeDerechoPf() {
        return agMeDerechoPf;
    }

    /**
     * @param agMeDerechoPf the agMeDerechoPf to set
     */
    public void setAgMeDerechoPf(InputText agMeDerechoPf) {
        this.agMeDerechoPf = agMeDerechoPf;
    }

    /**
     * @return the agMeSexoPf
     */
    public InputText getAgMeSexoPf() {
        return agMeSexoPf;
    }

    /**
     * @param agMeSexoPf the agMeSexoPf to set
     */
    public void setAgMeSexoPf(InputText agMeSexoPf) {
        this.agMeSexoPf = agMeSexoPf;
    }

    /**
     * @return the agMeAnioPf
     */
    public InputText getAgMeAnioPf() {
        return agMeAnioPf;
    }

    /**
     * @param agMeAnioPf the agMeAnioPf to set
     */
    public void setAgMeAnioPf(InputText agMeAnioPf) {
        this.agMeAnioPf = agMeAnioPf;
    }

    /**
     * @return the nombre
     */
    public InputText getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(InputText nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apePaterno
     */
    public InputText getApePaterno() {
        return apePaterno;
    }

    /**
     * @param apePaterno the apePaterno to set
     */
    public void setApePaterno(InputText apePaterno) {
        this.apePaterno = apePaterno;
    }

    /**
     * @return the apeMaterno
     */
    public InputText getApeMaterno() {
        return apeMaterno;
    }

    /**
     * @param apeMaterno the apeMaterno to set
     */
    public void setApeMaterno(InputText apeMaterno) {
        this.apeMaterno = apeMaterno;
    }

    /**
     * @return the edad
     */
    public InputText getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(InputText edad) {
        this.edad = edad;
    }

    /**
     * @return the primeraVez
     */
    public SelectOneMenu getPrimeraVez() {
        return primeraVez;
    }

    /**
     * @param primeraVez the primeraVez to set
     */
    public void setPrimeraVez(SelectOneMenu primeraVez) {
        this.primeraVez = primeraVez;
    }

    /**
     * @return the citado
     */
    public SelectOneMenu getCitado() {
        return citado;
    }

    /**
     * @param citado the citado to set
     */
    public void setCitado(SelectOneMenu citado) {
        this.citado = citado;
    }

    /**
     * @return the pase
     */
    public SelectOneMenu getPase() {
        return pase;
    }

    /**
     * @param pase the pase to set
     */
    public void setPase(SelectOneMenu pase) {
        this.pase = pase;
    }

    /**
     * @return the recetas
     */
    public InputText getRecetas() {
        return recetas;
    }

    /**
     * @param recetas the recetas to set
     */
    public void setRecetas(InputText recetas) {
        this.recetas = recetas;
    }

    /**
     * @return the diasIncapacidad
     */
    public InputText getDiasIncapacidad() {
        return diasIncapacidad;
    }

    /**
     * @param diasIncapacidad the diasIncapacidad to set
     */
    public void setDiasIncapacidad(InputText diasIncapacidad) {
        this.diasIncapacidad = diasIncapacidad;
    }

    /**
     * @return the visitas
     */
    public SelectOneMenu getVisitas() {
        return visitas;
    }

    /**
     * @param visitas the visitas to set
     */
    public void setVisitas(SelectOneMenu visitas) {
        this.visitas = visitas;
    }

    public AutoComplete getAgMeRegAcPf() {
        return agMeRegAcPf;
    }

    public void setAgMeRegAcPf(AutoComplete agMeRegAcPf) {
        this.agMeRegAcPf = agMeRegAcPf;
    }

    public AutoComplete getMetodoPpfAcPf() {
        return metodoPpfAcPf;
    }

    public void setMetodoPpfAcPf(AutoComplete metodoPpfAcPf) {
        this.metodoPpfAcPf = metodoPpfAcPf;
    }

    /**
     * @return the accidentesLesiones
     */
    public SelectOneMenu getAccidentesLesiones() {
        return accidentesLesiones;
    }

    /**
     * @param accidentesLesiones the accidentesLesiones to set
     */
    public void setAccidentesLesiones(SelectOneMenu accidentesLesiones) {
        this.accidentesLesiones = accidentesLesiones;
    }

    /**
     * @return the riesgosTrabajo
     */
    public SelectOneMenu getRiesgosTrabajo() {
        return riesgosTrabajo;
    }

    /**
     * @param riesgosTrabajo the riesgosTrabajo to set
     */
    public void setRiesgosTrabajo(SelectOneMenu riesgosTrabajo) {
        this.riesgosTrabajo = riesgosTrabajo;
    }

    /**
     * @return the cveTipoUrgencia
     */
    public SelectOneMenu getCveTipoUrgencia() {
        return cveTipoUrgencia;
    }

    /**
     * @param cveTipoUrgencia the cveTipoUrgencia to set
     */
    public void setCveTipoUrgencia(SelectOneMenu cveTipoUrgencia) {
        this.cveTipoUrgencia = cveTipoUrgencia;
    }

    /**
     * @return the diagnosticoPrincipal
     */
    public InputText getDiagnosticoPrincipal() {
        return diagnosticoPrincipal;
    }

    /**
     * @param diagnosticoPrincipal the diagnosticoPrincipal to set
     */
    public void setDiagnosticoPrincipal(InputText diagnosticoPrincipal) {
        this.diagnosticoPrincipal = diagnosticoPrincipal;
    }

    /**
     * @return the desDiagPrin
     */
    public InputText getDesDiagPrin() {
        return desDiagPrin;
    }

    /**
     * @param desDiagPrin the desDiagPrin to set
     */
    public void setDesDiagPrin(InputText desDiagPrin) {
        this.desDiagPrin = desDiagPrin;
    }

    /**
     * @return the cveInforAdicPrin
     */
    public SelectOneMenu getCveInforAdicPrin() {
        return cveInforAdicPrin;
    }

    /**
     * @param cveInforAdicPrin the cveInforAdicPrin to set
     */
    public void setCveInforAdicPrin(SelectOneMenu cveInforAdicPrin) {
        this.cveInforAdicPrin = cveInforAdicPrin;
    }

    /**
     * @return the cveTipoPrin
     */
    public InputText getCveTipoPrin() {
        return cveTipoPrin;
    }

    /**
     * @param cveTipoPrin the cveTipoPrin to set
     */
    public void setCveTipoPrin(InputText cveTipoPrin) {
        this.cveTipoPrin = cveTipoPrin;
    }

    /**
     * @return the fecCirugiaPrin
     */
    public InputMask getFecCirugiaPrin() {
        return fecCirugiaPrin;
    }

    /**
     * @param fecCirugiaPrin the fecCirugiaPrin to set
     */
    public void setFecCirugiaPrin(InputMask fecCirugiaPrin) {
        this.fecCirugiaPrin = fecCirugiaPrin;
    }

    /**
     * @return the diagnosticoAdicional
     */
    public InputText getDiagnosticoAdicional() {
        return diagnosticoAdicional;
    }

    /**
     * @param diagnosticoAdicional the diagnosticoAdicional to set
     */
    public void setDiagnosticoAdicional(InputText diagnosticoAdicional) {
        this.diagnosticoAdicional = diagnosticoAdicional;
    }

    /**
     * @return the desDiagAdic
     */
    public InputText getDesDiagAdic() {
        return desDiagAdic;
    }

    /**
     * @param desDiagAdic the desDiagAdic to set
     */
    public void setDesDiagAdic(InputText desDiagAdic) {
        this.desDiagAdic = desDiagAdic;
    }

    /**
     * @return the primeraVezDiagAdic
     */
    public SelectOneMenu getPrimeraVezDiagAdic() {
        return primeraVezDiagAdic;
    }

    /**
     * @param primeraVezDiagAdic the primeraVezDiagAdic to set
     */
    public void setPrimeraVezDiagAdic(SelectOneMenu primeraVezDiagAdic) {
        this.primeraVezDiagAdic = primeraVezDiagAdic;
    }

    /**
     * @return the cveInforAdicAdic
     */
    public SelectOneMenu getCveInforAdicAdic() {
        return cveInforAdicAdic;
    }

    /**
     * @param cveInforAdicAdic the cveInforAdicAdic to set
     */
    public void setCveInforAdicAdic(SelectOneMenu cveInforAdicAdic) {
        this.cveInforAdicAdic = cveInforAdicAdic;
    }

    /**
     * @return the cveTipoAdic
     */
    public SelectOneMenu getCveTipoAdic() {
        return cveTipoAdic;
    }

    /**
     * @param cveTipoAdic the cveTipoAdic to set
     */
    public void setCveTipoAdic(SelectOneMenu cveTipoAdic) {
        this.cveTipoAdic = cveTipoAdic;
    }

    /**
     * @return the fecCirugiaAdic
     */
    public InputMask getFecCirugiaAdic() {
        return fecCirugiaAdic;
    }

    /**
     * @param fecCirugiaAdic the fecCirugiaAdic to set
     */
    public void setFecCirugiaAdic(InputMask fecCirugiaAdic) {
        this.fecCirugiaAdic = fecCirugiaAdic;
    }

    /**
     * @return the diagnosticoComplemento
     */
    public InputText getDiagnosticoComplemento() {
        return diagnosticoComplemento;
    }

    /**
     * @param diagnosticoComplemento the diagnosticoComplemento to set
     */
    public void setDiagnosticoComplemento(InputText diagnosticoComplemento) {
        this.diagnosticoComplemento = diagnosticoComplemento;
    }

    /**
     * @return the desDiagComp
     */
    public InputText getDesDiagComp() {
        return desDiagComp;
    }

    /**
     * @param desDiagComp the desDiagComp to set
     */
    public void setDesDiagComp(InputText desDiagComp) {
        this.desDiagComp = desDiagComp;
    }

    /**
     * @return the primeraVezDiagComp
     */
    public SelectOneMenu getPrimeraVezDiagComp() {
        return primeraVezDiagComp;
    }

    /**
     * @param primeraVezDiagComp the primeraVezDiagComp to set
     */
    public void setPrimeraVezDiagComp(SelectOneMenu primeraVezDiagComp) {
        this.primeraVezDiagComp = primeraVezDiagComp;
    }

    /**
     * @return the cveInforAdicComp
     */
    public SelectOneMenu getCveInforAdicComp() {
        return cveInforAdicComp;
    }

    /**
     * @param cveInforAdicComp the cveInforAdicComp to set
     */
    public void setCveInforAdicComp(SelectOneMenu cveInforAdicComp) {
        this.cveInforAdicComp = cveInforAdicComp;
    }

    /**
     * @return the cveTipoComp
     */
    public SelectOneMenu getCveTipoComp() {
        return cveTipoComp;
    }

    /**
     * @param cveTipoComp the cveTipoComp to set
     */
    public void setCveTipoComp(SelectOneMenu cveTipoComp) {
        this.cveTipoComp = cveTipoComp;
    }

    /**
     * @return the fecCirugiaComp
     */
    public InputMask getFecCirugiaComp() {
        return fecCirugiaComp;
    }

    /**
     * @param fecCirugiaComp the fecCirugiaComp to set
     */
    public void setFecCirugiaComp(InputMask fecCirugiaComp) {
        this.fecCirugiaComp = fecCirugiaComp;
    }

    /**
     * @return the procedimientoPrincipal
     */
    public InputText getProcedimientoPrincipal() {
        return procedimientoPrincipal;
    }

    /**
     * @param procedimientoPrincipal the procedimientoPrincipal to set
     */
    public void setProcedimientoPrincipal(InputText procedimientoPrincipal) {
        this.procedimientoPrincipal = procedimientoPrincipal;
    }

    /**
     * @return the desProcPrin
     */
    public InputText getDesProcPrin() {
        return desProcPrin;
    }

    /**
     * @param desProcPrin the desProcPrin to set
     */
    public void setDesProcPrin(InputText desProcPrin) {
        this.desProcPrin = desProcPrin;
    }

    /**
     * @return the procedimientoAdicional
     */
    public InputText getProcedimientoAdicional() {
        return procedimientoAdicional;
    }

    /**
     * @param procedimientoAdicional the procedimientoAdicional to set
     */
    public void setProcedimientoAdicional(InputText procedimientoAdicional) {
        this.procedimientoAdicional = procedimientoAdicional;
    }

    /**
     * @return the desProcAdic
     */
    public InputText getDesProcAdic() {
        return desProcAdic;
    }

    /**
     * @param desProcAdic the desProcAdic to set
     */
    public void setDesProcAdic(InputText desProcAdic) {
        this.desProcAdic = desProcAdic;
    }

    /**
     * @return the procedimientoComplemento
     */
    public InputText getProcedimientoComplemento() {
        return procedimientoComplemento;
    }

    /**
     * @param procedimientoComplemento the procedimientoComplemento to set
     */
    public void setProcedimientoComplemento(InputText procedimientoComplemento) {
        this.procedimientoComplemento = procedimientoComplemento;
    }

    /**
     * @return the desProcComp
     */
    public InputText getDesProcComp() {
        return desProcComp;
    }

    /**
     * @param desProcComp the desProcComp to set
     */
    public void setDesProcComp(InputText desProcComp) {
        this.desProcComp = desProcComp;
    }

    public AutoComplete getEspecialidadAcPf() {
        return especialidadAcPf;
    }

    public void setEspecialidadAcPf(AutoComplete especialidadAcPf) {
        this.especialidadAcPf = especialidadAcPf;
    }

    public AutoComplete getConsultorioAcPf() {
        return consultorioAcPf;
    }

    public void setConsultorioAcPf(AutoComplete consultorioAcPf) {
        this.consultorioAcPf = consultorioAcPf;
    }

    /**
     * @return the dialogExiste
     */
    public Boolean getDialogExiste() {
        return dialogExiste;
    }

    /**
     * @param dialogExiste the dialogExiste to set
     */
    public void setDialogExiste(Boolean dialogExiste) {
        this.dialogExiste = dialogExiste;
    }

    /**
     * @return the dialogCantidad
     */
    public Boolean getDialogCantidad() {
        return dialogCantidad;
    }

    /**
     * @param dialogCantidad the dialogCantidad to set
     */
    public void setDialogCantidad(Boolean dialogCantidad) {
        this.dialogCantidad = dialogCantidad;
    }

    /**
     * @return the dialogEdad
     */
    public Boolean getDialogEdad() {
        return dialogEdad;
    }

    /**
     * @param dialogEdad the dialogEdad to set
     */
    public void setDialogEdad(Boolean dialogEdad) {
        this.dialogEdad = dialogEdad;
    }

    public List<String> getListaEspecialidadStr() {
        return listaEspecialidadStr;
    }

    public void setListaEspecialidadStr(List<String> listaEspecialidadStr) {
        this.listaEspecialidadStr = listaEspecialidadStr;
    }

    public List<String> getListaConsultorioStr() {
        return listaConsultorioStr;
    }

    public void setListaConsultorioStr(List<String> listaConsultorioStr) {
        this.listaConsultorioStr = listaConsultorioStr;
    }

    public List<String> getListaRegimenStr() {
        return listaRegimenStr;
    }

    public void setListaRegimenStr(List<String> listaRegimenStr) {
        this.listaRegimenStr = listaRegimenStr;
    }

    public List<String> getListaMetPpfStr() {
        return listaMetPpfStr;
    }

    public void setListaMetPpfStr(List<String> listaMetPpfStr) {
        this.listaMetPpfStr = listaMetPpfStr;
    }

    /**
     * @return the editableDiag
     */
    public boolean isEditableDiag() {
        return editableDiag;
    }

    /**
     * @param editableDiag the editableDiag to set
     */
    public void setEditableDiag(boolean editableDiag) {
        this.editableDiag = editableDiag;
    }

    /**
     * @return the apMat
     */
    public boolean isApMat() {
        return apMat;
    }

    /**
     * @param apMat the apMat to set
     */
    public void setApMat(boolean apMat) {
        this.apMat = apMat;
    }

    public Boolean getParaEdicion() {
        return paraEdicion;
    }

    public void setParaEdicion(Boolean paraEdicion) {
        this.paraEdicion = paraEdicion;
    }

    /**
     * Metodo que coloca en el objeto ConsultaExternaDto la clave y descripcion
     * de la rama de aseguramiento
     *
     * @param regimenAseguramiento
     * @param calidad
     * @param riesgoTrabajo
     */
    public void validaRamaAseguramiento(String regimenAseguramiento, String calidad, Integer riesgoTrabajo,
                                        String cveDiagnostico) {
        // agMeRegAcPf -regimenAseguramiento
        // agMeDerechoPf - calidad
        // validacion con riesgo de trabajo
        boolean tieneSupEmbarazo = false;
        for (String cveDiagEmbarazo : listaSupervicionEmbarazo) {
            if (cveDiagEmbarazo.equals(cveDiagnostico)) {
                tieneSupEmbarazo = true;
                break;
            }
        }
        if (regimenAseguramiento.equals("ND")) {
            consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.NO_DERECHOHABIENTE.getClave());
            consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.NO_DERECHOHABIENTE.getValor());
        }
        if (regimenAseguramiento.equals("SF")) {
            consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.SEGURO_FAMILIAR.getClave());
            consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.SEGURO_FAMILIAR.getValor());
        }
        if (regimenAseguramiento.equals("SA") || regimenAseguramiento.equals("ES")) {
            consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.OTROS.getClave());
            consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.OTROS.getValor());
        }
        if (regimenAseguramiento.equals("PE") || regimenAseguramiento.equals("OR")) {
            if (calidad.equals("5") || calidad.equals("6")) {
                consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.PENSIONADO.getClave());
                consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.PENSIONADO.getValor());
            }
        }
        if (regimenAseguramiento.equals("OR")) {
            if (calidad.equals("2") || calidad.equals("3") || calidad.equals("4")) {
                if (tieneSupEmbarazo) {
                    consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.MATERNIDAD.getClave());
                    consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.MATERNIDAD.getValor());
                } else {
                    consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.ENFERMEDAD_GENERAL.getClave());
                    consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.ENFERMEDAD_GENERAL.getValor());
                }
            }
            if (calidad.equals("1")) {
                if (riesgoTrabajo != null) {
                    if (tieneSupEmbarazo && riesgoTrabajo.equals(1)) {
                        consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.MATERNIDAD.getClave());
                        consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.MATERNIDAD.getValor());
                    }
                    if (!tieneSupEmbarazo && riesgoTrabajo.equals(1)) {
                        consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.ENFERMEDAD_GENERAL.getClave());
                        consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.ENFERMEDAD_GENERAL.getValor());
                    }
                    if (riesgoTrabajo.equals(2)) {
                        consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.RIESGO_DE_TRABAJO_CONFIRMADO.getClave());
                        consulExtDto
                                .setDescRamaAseguramiento(EnumRamaAseguramiento.RIESGO_DE_TRABAJO_CONFIRMADO.getValor());
                    }
                    if (riesgoTrabajo.equals(3)) {
                        consulExtDto.setCveRamaAseguramiento(EnumRamaAseguramiento.PROBABLE_RIESGO_DE_TRABAJO.getClave());
                        consulExtDto.setDescRamaAseguramiento(EnumRamaAseguramiento.PROBABLE_RIESGO_DE_TRABAJO.getValor());
                    }
                }
            }
        }
    }// end validaRamaAseguramiento

    public List<String> getListaSupervicionEmbarazo() {
        return listaSupervicionEmbarazo;
    }

    public void setListaSupervicionEmbarazo(List<String> listaSupervicionEmbarazo) {
        this.listaSupervicionEmbarazo = listaSupervicionEmbarazo;
    }

    public List<String> getListaParamedicas() {
        return listaParamedicas;
    }

    public void setListaParamedicas(List<String> listaParamedicas) {
        this.listaParamedicas = listaParamedicas;
    }

    public boolean isVerMetodoPlanFamiliar() {
        return verMetodoPlanFamiliar;
    }

    public void setVerMetodoPlanFamiliar(boolean verMetodoPlanFamiliar) {
        this.verMetodoPlanFamiliar = verMetodoPlanFamiliar;
    }

    public List<SicInformacionAdicionalVIH> getListaInfAdicionalVIH() {
        return listaInfAdicionalVIH;
    }

    public void setListaInfAdicionalVIH(List<SicInformacionAdicionalVIH> listaInfAdicionalVIH) {
        this.listaInfAdicionalVIH = listaInfAdicionalVIH;
    }

    // RN26 VALIDACION VIH-SIDA
    public boolean validaVIH(int idDiagnostico) {
        RequestContext contextVHI = RequestContext.getCurrentInstance();
        boolean verDialogoVIH = Boolean.FALSE;
        // listaInfAdicionalVIH = consultaExternaService.buscarTodosInfAdiVIH();
        cveCD4PBinding.resetValue();
        cveCD4ABinding.resetValue();
        cveCD4CBinding.resetValue();
        if (idDiagnostico == 1) {
            if (consulExtDto.getDiagnosticoPrincipal().startsWith("B20")
                    || consulExtDto.getDiagnosticoPrincipal().startsWith("B21")
                    || consulExtDto.getDiagnosticoPrincipal().startsWith("B22")
                    || consulExtDto.getDiagnosticoPrincipal().startsWith("B23")
                    || consulExtDto.getDiagnosticoPrincipal().startsWith("B24")) {
                foco = cveCD4PBinding.getClientId();
                verDialogoVIH = Boolean.TRUE;
                contextVHI.execute("PF('dialogCD4P').show();");
            } else {
                foco = diagnosticoAdicional.getClientId();
                diagnosticoAdicional.setDisabled(Boolean.FALSE);
                primeraVezDiagAdic.setDisabled(Boolean.FALSE);
            }
        }
        if (idDiagnostico == 2) {
            if (consulExtDto.getDiagnosticoAdicional().startsWith("B20")
                    || consulExtDto.getDiagnosticoAdicional().startsWith("B21")
                    || consulExtDto.getDiagnosticoAdicional().startsWith("B22")
                    || consulExtDto.getDiagnosticoAdicional().startsWith("B23")
                    || consulExtDto.getDiagnosticoAdicional().startsWith("B24")) {
                foco = cveCD4ABinding.getClientId();
                verDialogoVIH = Boolean.TRUE;
                contextVHI.execute("PF('dialogCD4A').show();");
            } else {
//				foco = diagnosticoAdicional.getClientId();
                foco = primeraVezDiagAdic.getClientId();
            }
        }
        if (idDiagnostico == 3) {
            if (consulExtDto.getDiagnosticoComplemento().startsWith("B20")
                    || consulExtDto.getDiagnosticoComplemento().startsWith("B21")
                    || consulExtDto.getDiagnosticoComplemento().startsWith("B22")
                    || consulExtDto.getDiagnosticoComplemento().startsWith("B23")
                    || consulExtDto.getDiagnosticoComplemento().startsWith("B24")) {
                foco = cveCD4CBinding.getClientId();
                verDialogoVIH = Boolean.TRUE;
                contextVHI.execute("PF('dialogCD4C').show();");
            } else {
//				foco = diagnosticoComplemento.getClientId();
                foco = primeraVezDiagComp.getClientId();
            }
        }
        return verDialogoVIH;
    }

    // RN 26 INFORMACION ADICIONAL - TIPO DE EMBARAZO
    public boolean validaTipoEmbarazo(int idDiagnostico) {
        RequestContext contextEmb = RequestContext.getCurrentInstance();
        boolean verDialogoEmbarazo = Boolean.FALSE;
        if (idDiagnostico == 1) {
            if (consulExtDto.getDiagnosticoPrincipal().startsWith("Z33")
                    || consulExtDto.getDiagnosticoPrincipal().startsWith("Z34")
                    || consulExtDto.getDiagnosticoPrincipal().startsWith("Z35")) {
                foco = cveTipoEmbarazo.getClientId();
                cveTipoEmbarazo.resetValue();
                verDialogoEmbarazo = Boolean.TRUE;
                contextEmb.execute("PF('dialogTipEmbarazo').show();");
            } else {
                foco = diagnosticoAdicional.getClientId();
                diagnosticoAdicional.setDisabled(Boolean.FALSE);
                primeraVezDiagAdic.setDisabled(Boolean.FALSE);
            }
        }
        if (idDiagnostico == 2) {

            if (consulExtDto.getDiagnosticoAdicional().startsWith("Z33")
                    || consulExtDto.getDiagnosticoAdicional().startsWith("Z34")
                    || consulExtDto.getDiagnosticoAdicional().startsWith("Z35")) {
                foco = cveTipoEmbarazoAdicional.getClientId();
                cveTipoEmbarazoAdicional.resetValue();
                verDialogoEmbarazo = Boolean.TRUE;
                contextEmb.execute("PF('dialogTipEmbarazoAdicional').show();");
            } else {
//				foco = diagnosticoAdicional.getClientId();
                foco = primeraVezDiagAdic.getClientId();
            }
        }
        if (idDiagnostico == 3) {

            if (consulExtDto.getDiagnosticoComplemento().startsWith("Z33")
                    || consulExtDto.getDiagnosticoComplemento().startsWith("Z34")
                    || consulExtDto.getDiagnosticoComplemento().startsWith("Z35")) {
                foco = cveTipoEmbarazoComplementario.getClientId();
                cveTipoEmbarazoComplementario.resetValue();
                verDialogoEmbarazo = Boolean.TRUE;
                contextEmb.execute("PF('dialogTipEmbarazoComplementario').show();");
            } else {
                foco = primeraVezDiagComp.getClientId();
            }
        }
        return verDialogoEmbarazo;
    }

    public void validacd4P() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            //se ingreso valor
            foco = diagnosticoAdicional.getClientId();
            diagnosticoAdicional.setDisabled(Boolean.FALSE);
            primeraVezDiagAdic.setDisabled(Boolean.FALSE);
            consulExtDto.setCveCD4Principal(Integer.valueOf(this.cveCD4PBinding.getValue().toString()));
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogCD4P').hide();");
        } else {
            //Tecla "ESC"
            resetDiagPrincipal();

            consulExtDto.setCveCD4Principal(null);
            cveCD4PBinding.resetValue();

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogCD4P').hide();");
        }

    }

    public void validacd4A() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = primeraVezDiagAdic.getClientId();

            consulExtDto.setCveCD4Adicional(Integer.valueOf(this.cveCD4ABinding.getValue().toString()));
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogCD4A').hide();");

        } else {
            //Tecla ESC
            resetDiagAdicional();

            consulExtDto.setCveCD4Adicional(null);
            cveCD4ABinding.resetValue();

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogCD4A').hide();");
        }

    }

    public void validacd4C() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = primeraVezDiagComp.getClientId();

            consulExtDto.setCveCD4Compl(Integer.valueOf(this.cveCD4CBinding.getValue().toString()));
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogCD4C').hide();");

        } else {
            //Tecla ESC
            resetDiagComplementario();

            consulExtDto.setCveCD4Compl(null);
            cveCD4CBinding.resetValue();

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogCD4C').hide();");
        }
    }

    public void validaTipoEmbarazoPrincipal() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = diagnosticoAdicional.getClientId();
            diagnosticoAdicional.setDisabled(Boolean.FALSE);
            primeraVezDiagAdic.setDisabled(Boolean.FALSE);
            consulExtDto.setCveTipoEmbarazo(Integer.valueOf(this.cveTipoEmbarazo.getValue().toString()));
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogTipEmbarazo').hide();");

        } else {
            //Tecla ESC
            resetDiagPrincipal();

            consulExtDto.setCveTipoEmbarazo(null);
            cveTipoEmbarazo.resetValue();

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogTipEmbarazo').hide();");
        }

    }

    public void validaTipoEmbarazoAdicional() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = primeraVezDiagAdic.getClientId();

            consulExtDto.setCveTipoEmbarazoAdicional(Integer.valueOf(this.cveTipoEmbarazoAdicional.getValue().toString()));
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogTipEmbarazoAdicional').hide();");

        } else {
            //Tecla ESC
            resetDiagAdicional();

            consulExtDto.setCveTipoEmbarazoAdicional(null);
            cveTipoEmbarazoAdicional.resetValue();

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogTipEmbarazoAdicional').hide();");

        }
    }

    public void validaTipoEmbarazoComplementario() {
        String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
        if (parametro == null) {
            foco = primeraVezDiagComp.getClientId();

            consulExtDto.setCveTipoEmbarazoComplementario(
                    Integer.valueOf(this.cveTipoEmbarazoComplementario.getValue().toString()));
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogTipEmbarazoComplementario').hide();");

        } else {
            //Tecla ESC
            resetDiagComplementario();

            consulExtDto.setCveTipoEmbarazoComplementario(null);
            cveTipoEmbarazoComplementario.resetValue();

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dialogTipEmbarazoComplementario').hide();");

        }

    }

    public void resetDiagPrincipal() {
        foco = diagnosticoPrincipal.getClientId();
        consulExtDto.setDiagnosticoPrincipal(null);
        consulExtDto.setDesDiagPrin("");
        diagnosticoPrincipal.resetValue();
        desDiagPrin.resetValue();
        resetBorrarInfoAdicionalPrin();
//		diagnosticoAdicional.setDisabled(Boolean.TRUE);
//		primeraVezDiagAdic.setDisabled(Boolean.TRUE);
    }

    public void resetDiagAdicional() {
        foco = diagnosticoAdicional.getClientId();
        consulExtDto.setDiagnosticoAdicional(null);
        consulExtDto.setDesDiagAdic("");
        diagnosticoAdicional.resetValue();
        desDiagAdic.resetValue();
        consulExtDto.setPrimeraVezDiagAdic(0);
        primeraVezDiagAdic.resetValue();
        resetBorrarInfoAdicionalAdic();
//		diagnosticoComplemento.setDisabled(Boolean.TRUE);
//		primeraVezDiagComp.setDisabled(Boolean.TRUE);
    }

    public void resetDiagComplementario() {
        foco = diagnosticoComplemento.getClientId();
        consulExtDto.setDiagnosticoComplemento(null);
        consulExtDto.setDesDiagComp(null);
        this.getDiagnosticoComplemento().resetValue();
        this.getDesDiagComp().resetValue();
        consulExtDto.setPrimeraVezDiagComp(0);
        primeraVezDiagComp.resetValue();
        resetBorrarInfoAdicionalComp();
    }


    //RESET PROCEDIMIENTOS
    public void resetProcPrincipal() {
        foco = procedimientoPrincipal.getClientId();
        consulExtDto.setProcedimientoPrincipal("");
        consulExtDto.setDesProcPrin("");
        getProcedimientoPrincipal().resetValue();
        getDesProcPrin().resetValue();
        procedimientoAdicional.setDisabled(Boolean.TRUE);
        procedimientoComplemento.setDisabled(Boolean.TRUE);
    }

    public void resetProcAdicional() {
        foco = procedimientoAdicional.getClientId();
        consulExtDto.setProcedimientoAdicional("");
        consulExtDto.setDesProcAdic("");
        procedimientoAdicional.resetValue();
        desProcAdic.resetValue();
        procedimientoComplemento.setDisabled(Boolean.TRUE);
    }

    public void resetProcComplementario() {
        foco = procedimientoComplemento.getClientId();
        consulExtDto.setProcedimientoComplemento("");
        consulExtDto.setDesProcComp("");
        procedimientoComplemento.resetValue();
        desProcComp.resetValue();
    }

    private void muestraCampos() {
        muestraCitado = (Integer.parseInt(consulExtDto.getEspecialidadSelc()) != EnumEspecialidades.URGENCIA.getClave());
        muestraPase = Integer.parseInt(consulExtDto.getEspecialidadSelc()) != EnumEspecialidades.PARAMEDICO.getClave();
        muestraRecetas = Integer.parseInt(consulExtDto.getEspecialidadSelc()) != EnumEspecialidades.PARAMEDICO.getClave();
        muestraVisita = (Integer.parseInt(consulExtDto.getEspecialidadSelc()) != EnumEspecialidades.URGENCIA.getClave()) && (Integer.parseInt(consulExtDto.getEspecialidadSelc()) != EnumEspecialidades.URGENCIA_CIRUGIA.getClave());
        muestraTipoUrgencia = (Integer.parseInt(consulExtDto.getEspecialidadSelc()) == EnumEspecialidades.URGENCIA.getClave());
        if (validaDiferenteEmpty(this.diagnosticoAdicional)) {
            if (activaAccidentes(this.diagnosticoAdicional.getValue().toString().toUpperCase())) {
                diagAcc = 1;
            } else {
                diagAcc = 0;
            }
        }


    }

    private boolean validaRequeridosAtencionDto() {
        boolean valido = true;
        String elFoco = "";

        //Riesgo de trabajo se caqmbia al inicio de forma temporal por bug encontrado
        if (muestraRiesgoTrabajo) {
            Integer tipoServ = consulExtDto.getNumTipoServicio() == null ? 0 : consulExtDto.getNumTipoServicio();
            if (tipoServ == EnumEspecialidades.URGENCIA.getClave()
                    || tipoServ == EnumEspecialidades.URGENCIA_CIRUGIA.getClave()) {
                valido = true;
            } else {
                if (!editableRiesgoTrabajo) {
                    if (riesgosTrabajo.getValue() == null || riesgosTrabajo.getValue().toString().isEmpty()) {
                        valido = false;
                        if (elFoco.isEmpty()) {
                            elFoco = this.getRiesgosTrabajo().getClientId();
                        }
                        mensajeErrorConParametros("mecx01_requerido", "Riesgo de Trabajo");
                    }
                }
            }
        }

        if (consulExtDto.getNss() == null || consulExtDto.getNss().isEmpty() || !validaDiferenteEmpty(this.getNss())) {
            valido = false;
            if (elFoco.isEmpty()) {
                elFoco = this.getNss().getClientId();
            }
            mensajeErrorConParametros("mecx01_requerido", "Nss");
        }
        if (consulExtDto.getAgregadoMedico() == null || consulExtDto.getAgregadoMedico().isEmpty() || !validaRequeridosAgregadoBinding()) {
            valido = false;
            if (elFoco.isEmpty()) {
                elFoco = this.getAgMeDerechoPf().getClientId();
            }
            mensajeErrorConParametros("mecx01_requerido", "Agregado M\u00E9dico");
        }
        if (consulExtDto.getNombre() == null || consulExtDto.getNombre().isEmpty() || !validaDiferenteEmpty(this.getNombre())) {
            valido = false;
            if (elFoco.isEmpty()) {
                elFoco = this.getNombre().getClientId();
            }
            mensajeErrorConParametros("mecx01_requerido", "Nombre");
        }
        if (consulExtDto.getApePaterno() == null || consulExtDto.getApePaterno().isEmpty() || !validaDiferenteEmpty(this.getApePaterno())) {
            valido = false;
            if (elFoco.isEmpty()) {
                elFoco = this.getApePaterno().getClientId();
            }
            mensajeErrorConParametros("mecx01_requerido", "Apellido Paterno");
        }

        //COMBOS Y DINAMICOS
        if (consulExtDto.getPrimeraVez() == null || !validaDiferenteEmpty(this.getPrimeraVez())) {
            valido = false;
            if (elFoco.isEmpty()) {
                elFoco = this.getPrimeraVez().getClientId();
            }
            mensajeErrorConParametros("mecx01_requerido", "Primera vez");
        }
        if (muestraCitado) {
            if (consulExtDto.getCitado() == null || !validaDiferenteEmpty(this.getCitado())) {
                valido = false;
                if (elFoco.isEmpty()) {
                    elFoco = this.getCitado().getClientId();
                }
                mensajeErrorConParametros("mecx01_requerido", "Citado");
            }
        }
        if (muestraPase) {
            if (consulExtDto.getPase() == null || !validaDiferenteEmpty(this.getPase())) {
                valido = false;
                if (elFoco.isEmpty()) {
                    elFoco = this.getPase().getClientId();
                }
                mensajeErrorConParametros("mecx01_requerido", "Pase");
            }
        }
        if (muestraVisita) {
            if (consulExtDto.getVisitas() == null || !validaDiferenteEmpty(this.getVisitas())) {
                valido = false;
                if (elFoco.isEmpty()) {
                    elFoco = this.getVisitas().getClientId();
                }
                mensajeErrorConParametros("mecx01_requerido", "Visitas");
            }
        }
        //Diagnóstico principal
        if (consulExtDto.getDesDiagPrin() == null || consulExtDto.getDesDiagPrin().isEmpty() || !validaDiferenteEmpty(this.getDiagnosticoPrincipal())) {
            valido = false;
            if (elFoco.isEmpty()) {
                elFoco = this.getDiagnosticoPrincipal().getClientId();
            }
            mensajeErrorConParametros("mecx01_requerido", "Diagn\u00F3stico Principal");
        }

        if (!elFoco.isEmpty()) {
            foco = elFoco;
        }
        return valido;
    }

    private boolean validaRequeridosAgregadoBinding() {
        boolean valido = true;
        if (!validaDiferenteEmpty(this.getAgMeDerechoPf())) {
            valido = false;
        }
        if (!validaDiferenteEmpty(this.getAgMeSexoPf())) {
            valido = false;
        }
        if (!validaDiferenteEmpty(this.getAgMeAnioPf())) {
            valido = false;
        }
        if (!validaDiferenteEmpty(this.getAgMeRegAcPf())) {
            valido = false;
        }
        return valido;
    }


    private void resetBorrarInfoAdicionalPrin() {
        consulExtDto.setCveInforAdicPrin(null);
        consulExtDto.setFecCirugiaPrin(null);
        consulExtDto.setCveTipoPrin(null);
        consulExtDto.setMorfologia(null);
        consulExtDto.setCveTipoEmbarazo(null);
        consulExtDto.setCveCD4Principal(null);
        cveMorfologia.resetValue();
        cveCD4PBinding.resetValue();
        cveTipoEmbarazo.resetValue();
        fecCirugiaPrin.resetValue();
        cveTipoPrin.resetValue();
    }

    private void resetBorrarInfoAdicionalAdic() {
        consulExtDto.setCveInforAdicAdic(null);
        consulExtDto.setFecCirugiaAdic(null);
        consulExtDto.setCveTipoAdic(null);
        consulExtDto.setCveTipoEmbarazoAdicional(null);
        consulExtDto.setCveCD4Adicional(null);
        cveCD4ABinding.resetValue();
        cveTipoEmbarazoAdicional.resetValue();
        fecCirugiaAdic.resetValue();
        cveTipoAdic.resetValue();
    }

    private void resetBorrarInfoAdicionalComp() {
        consulExtDto.setCveInforAdicComp(null);
        consulExtDto.setFecCirugiaComp(null);
        consulExtDto.setCveTipoComp(null);
        consulExtDto.setCveTipoEmbarazoComplementario(null);
        consulExtDto.setCveCD4Compl(null);
        cveCD4CBinding.resetValue();
        cveTipoEmbarazoComplementario.resetValue();
        fecCirugiaComp.resetValue();
        cveTipoComp.resetValue();

    }

    private Boolean validaCamposDeRepetidos() {

        if (consulExtDto != null) {
            if (consulExtDto.getId() != null && consulExtDto.getNss() != null && consulExtDto.getAgregadoMedico() != null) {
                if (consulExtDto.getCveEspecialidad() != null && (
                        consulExtDto.getCveEspecialidad().startsWith("50")
                                || consulExtDto.getCveEspecialidad().equals(CVE_TOCO_CIRUGIA)
                )) {
                    //urgencias
                    if (consulExtDto.getDiagnosticoPrincipal() != null) {
                        if (paraEdicion) {
                            if (!dxPOriginal.equals(consulExtDto.getDiagnosticoPrincipal())) {
                                esRepetido = consultaExternaService.buscarAtencionRepetida(consulExtDto.getNss(), consulExtDto.getAgregadoMedico(), consulExtDto.getDiagnosticoPrincipal(), consulExtDto.getId());
                            }
                        } else {
                            esRepetido = consultaExternaService.buscarAtencionRepetida(consulExtDto.getNss(), consulExtDto.getAgregadoMedico(), consulExtDto.getDiagnosticoPrincipal(), consulExtDto.getId());
                        }

                    }
                } else {
                    if (!paraEdicion) {
                        //especialidades
                        esRepetido = consultaExternaService.buscarAtencionRepetida(consulExtDto.getNss(), consulExtDto.getAgregadoMedico(), null, consulExtDto.getId());
                    }
                }
            }
        }
        if (esRepetido) {
            RequestContext context = RequestContext.getCurrentInstance();
            if (paraEdicion) {
                context.execute("PF('dialogRegistroRepetidoEdit').show();");
            } else {
                context.execute("PF('dialogRegistroRepetido').show();");
            }
        }
        return esRepetido;
    }
    @NonNull
    private List<AccederDto> filtraListaSoloMujeres(List<AccederDto> lista){
        logger.info("Se filtran las mujeres de la lista");
        List<AccederDto> ret= new ArrayList<>();
        for (AccederDto a:lista) {
            logger.info("Pasa por "+a.getNombre()+" "+a.getAgregadoMedico()+" en lista");
            if(a.getAgregadoMedico().substring(1, 2).equals("F")){
                ret.add(a);
                logger.info("Se añade "+a.getNombre()+" "+a.getAgregadoMedico()+" a la lista");
            }
        }
        return ret;
    }

    public void beneficiarioSeleccionado() {
    	logger.info("Inicia beneficiarioSeleccionado()");
        validarNumeroBeneficiario();
        if (listaBeneficiarios.size() < Integer.parseInt(numBeneficiario.getValue().toString())
                && Integer.parseInt(numBeneficiario.getValue().toString()) < 0) {
            return;
        }
        if (Integer.parseInt(numBeneficiario.getValue().toString()) == 0) {
            Calendar fechaActual = Calendar.getInstance();
            Integer annis = fechaActual.get(Calendar.YEAR)
                    - Integer.parseInt(this.agMeAnioPf.getValue().toString());
            if (annis <= 1) {
                dialogEdad = Boolean.TRUE;
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('dialogEdad').show();");
                sem = Boolean.TRUE;
                foco = this.edad.getClientId();
                this.edad.resetValue();
                consulExtDto.setEdad(null);
            } else {
                dialogEdad = Boolean.FALSE;
                this.edad.setValue(annis);
                consulExtDto.setEdad(annis);
                sem = Boolean.FALSE;
            }

            SicEspecialidad especialidad = consultaExternaService.buscarEspecialidad(consulExtDto.getCveEspecialidad());
            datosUniAds = true;
            consulExtDto.setStpUltConsultaVig(new Date());

            // RN 132 Cuando NO hay respuesta de
            // ACCEDER se toma el año del
            // agregado médico y por default 01
            // de Enero
            setEdadEnSemanas(calculoSemanasEdad("01/01/" + agMeAnioPf.getValue().toString()));
            if ((getEdadEnSemanas() < especialidad.getNumEdadMinSemana())
                    || (getEdadEnSemanas() > especialidad.getNumEdadMaximaSemanas())) {
                resetCamposEdadEspecialdad();
            } else {
                deshabilitado = false;
                deshabilitadoA = true;
                deshabilitadoR = false;
                deshabilitadoAgre = false;
                deshabilitadoAgreA = true;
                deshabilitadoAgreR = false;
                deshabilitadoNom = false;
                deshabilitadoNomA = true;
                deshabilitadoNomR = false;
                limpiaAcceder();
                datosDerecho = false;
                foco = nombre.getClientId();
                fechaNac = true;
                if (annis <= 1) {
                    dialogEdad = Boolean.TRUE;
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.execute("PF('dialogEdad').show();");
                    foco = this.edad.getClientId();
                    sem = Boolean.TRUE;

                    this.edad.resetValue();
                    consulExtDto.setEdad(null);
                } else {
                    dialogEdad = Boolean.FALSE;
                    this.edad.setValue(annis);
                    consulExtDto.setEdad(annis);
                    logger.info("Se hace seteo de la edad: " + consulExtDto.getEdad());
                    sem = Boolean.FALSE;
                }
            }

        } else {
            beneficiario = listaBeneficiarios.get(Integer.parseInt(numBeneficiario.getValue().toString()) - 1);


            String agregadoMedicoDerecho, agregadoMedicoAnio, agregadoMedicoSexo, agregadoMedicoReg;
            agregadoMedicoDerecho = beneficiario.getAgregadoMedico().substring(0, 1);
            agregadoMedicoSexo = beneficiario.getAgregadoMedico().substring(1, 2);
            agregadoMedicoAnio = beneficiario.getAgregadoMedico().substring(2, 6);

            agregadoMedicoReg = beneficiario.getAgregadoMedico().substring(6, 8);

            agMeRegAcPf.setDisabled(false);
            agMeRegAcPf.resetValue();
            agMeRegAcPf.setValue("");


            agMeAnioPf.setValue(agregadoMedicoAnio);

            agMeRegAcPf.setValue(agregadoMedicoReg);
            agMeSexoPf.setValue(agregadoMedicoSexo);

            agMeDerechoPf.setValue(agregadoMedicoDerecho);


            logger.info(this.beneficiario.getNombre() + " " + this.beneficiario.getApeMaterno() + " " + this.beneficiario.getApePaterno());
            logger.info("Agregado Medico " + beneficiario.getAgregadoMedico() + " AgregadoRegAcPF " + agMeRegAcPf.getValue().toString());

            if (beneficiario != null) {
                //RequestContext.getCurrentInstance().execute("PF('form:agregado').search('');");
                SicEspecialidad especialidad = consultaExternaService.buscarEspecialidad(consulExtDto.getCveEspecialidad());
                Calendar fechaActual = Calendar.getInstance();
                fechaActual.setTime(consulExtDto.getFecha());
                Integer annis = fechaActual.get(Calendar.YEAR)
                        - Integer.parseInt(this.agMeAnioPf.getValue().toString());
                logger.info("Se encontro beneficiario " + beneficiario.getCveIdee() + " edad: " + annis);
                if (beneficiario.getEncontrado()) {
                    logger.info("Encontrado = true");
                    //if(true){
                    // RN 132 Cuando hay respuesta de
                    // ACCEDER
                    setEdadEnSemanas(calculoSemanasEdadAcceder(beneficiario.getFecNacimiento()));
                    if ((getEdadEnSemanas() < especialidad.getNumEdadMinSemana())
                            || (getEdadEnSemanas() > especialidad.getNumEdadMaximaSemanas())) {
                        logger.info("Se ejecuta Reset de campos edad especialidad");
                        resetCamposEdadEspecialdad();
                        logger.info("Agregado Medico despues de LIMPIAR" + beneficiario.getAgregadoMedico() + " AgregadoRegAcPF " + agMeRegAcPf.getValue().toString());
                    } else {
                        logger.info("NO se ejecuta el reset de campos edad especialidad");

                        deshabilitado = true;
                        deshabilitadoA = false;
                        deshabilitadoR = false;
                        deshabilitadoAgre = true;
                        deshabilitadoAgreA = false;
                        deshabilitadoAgreR = false;
                        deshabilitadoNom = true;
                        deshabilitadoNomA = false;
                        deshabilitadoNomR = false;
                        colorNss = "6CEE38";

                        comparaAcceder(consulExtDto, beneficiario);


                        datosDerecho = true;
                        deshabilitadoNom = true;
                        if (urgencia) {
                            foco = primeraVez.getClientId();
                            campo = 1;
                        } else {
                            // foco = "primeraVez";
                            // campo = 2;
                            foco = cveTipoUrgencia.getClientId();
                            campo = 3;
                        }
                        this.edad.setValue(annis);
                        consulExtDto.setEdad(annis);
                        logger.info("Se hace seteo de la edad: " + consulExtDto.getEdad());
                    }
                }
				/*
				Collection<String> componentes = new ArrayList<String>();
				componentes.add("form:cextAtencion");
				componentes.add("formDlgNegocio:dlgEdad");
				componentes.add("form:cextDiag");
				componentes.add("form:cextAtencion2");

				RequestContext.getCurrentInstance().update(componentes);

				RequestContext.getCurrentInstance().execute("scriptDesbloquear();scripts_cextAtencion();PF('blockUIWidget').unblock();scripts_cextDiag();scripts_cextAtencion2();");
				*/
                // valida COMBO METODO PLANIFICACION FAMILIAR
                int edad = calcularEdadAnnos(this.agMeAnioPf.getValue().toString(), consulExtDto.getFecha());
                muestraMetodoPlanificacionFamiliar(edad);
            }
        }
        Collection<String> componentes = new ArrayList<String>();
        componentes.add("form:cextAtencion");
        componentes.add("formDlgNegocio:dlgEdad");
        componentes.add("form:cextDiag");
        componentes.add("form:cextAtencion2");
        componentes.add("form:agregado");


        RequestContext.getCurrentInstance().update(componentes);

        RequestContext.getCurrentInstance().execute("scriptDesbloquear();scripts_cextAtencion();PF('blockUIWidget').unblock();scripts_cextDiag();scripts_cextAtencion2();");
        //RequestContext.getCurrentInstance().execute("scriptDesbloquear();PF('blockUIWidget').unblock();");
        muestraRiesgoDeTrabajo();
        muestraDiasIncapacidad();
        logger.info("Termina beneficiarioSeleccionado()");
    }

    @NonNull
    private List<AccederDto> filtraListaBeneficiarios(List<AccederDto> ListaBeneficiarios){

        List <AccederDto> gemelos=new ArrayList<>();

        for (AccederDto beneficiario
                :ListaBeneficiarios) {
           if (beneficiario.getAgregadoMedico().equals(consulExtDto.getAgregadoMedico())){
               gemelos.add(beneficiario);
           }
        }
        if (!gemelos.isEmpty()) {
            return gemelos;
        } else {return ListaBeneficiarios;}
    }
    private void validarNumeroBeneficiario() {
        logger.info("Valida Numero :" + Integer.parseInt(numBeneficiario.getValue().toString()));
        if (Integer.parseInt(numBeneficiario.getValue().toString()) == 0) {
            datosDerecho = false;
            logger.info("Valida Numero : " + datosDerecho);
        }
        logger.info("Termina validarNumero");

    }


    public InputText getCveCD4PBinding() {
        return cveCD4PBinding;
    }

    public void setCveCD4PBinding(InputText cveCD4PBinding) {
        this.cveCD4PBinding = cveCD4PBinding;
    }

    public InputText getCveCD4ABinding() {
        return cveCD4ABinding;
    }

    public void setCveCD4ABinding(InputText cveCD4ABinding) {
        this.cveCD4ABinding = cveCD4ABinding;
    }

    public InputText getCveCD4CBinding() {
        return cveCD4CBinding;
    }

    public void setCveCD4CBinding(InputText cveCD4CBinding) {
        this.cveCD4CBinding = cveCD4CBinding;
    }

    public InputMask getCveMorfologia() {
        return cveMorfologia;
    }

    public void setCveMorfologia(InputMask cveMorfologia) {
        this.cveMorfologia = cveMorfologia;
    }

    public InputText getCveTipoEmbarazo() {
        return cveTipoEmbarazo;
    }

    public void setCveTipoEmbarazo(InputText cveTipoEmbarazo) {
        this.cveTipoEmbarazo = cveTipoEmbarazo;
    }

    public InputText getCveTipoEmbarazoAdicional() {
        return cveTipoEmbarazoAdicional;
    }

    public void setCveTipoEmbarazoAdicional(InputText cveTipoEmbarazoAdicional) {
        this.cveTipoEmbarazoAdicional = cveTipoEmbarazoAdicional;
    }

    public InputText getCveTipoEmbarazoComplementario() {
        return cveTipoEmbarazoComplementario;
    }

    public void setCveTipoEmbarazoComplementario(
            InputText cveTipoEmbarazoComplementario) {
        this.cveTipoEmbarazoComplementario = cveTipoEmbarazoComplementario;
    }

    public String getDesMorfologia() {
        return desMorfologia;
    }

    public void setDesMorfologia(String desMorfologia) {
        this.desMorfologia = desMorfologia;
    }


    /**
     * @return the editableRiesgoTrabajo
     */
    public boolean isEditableRiesgoTrabajo() {
        return editableRiesgoTrabajo;
    }

    /**
     * @param editableRiesgoTrabajo the editableRiesgoTrabajo to set
     */
    public void setEditableRiesgoTrabajo(boolean editableRiesgoTrabajo) {
        this.editableRiesgoTrabajo = editableRiesgoTrabajo;
    }


    public InputText getCantidadMpf() {
        return cantidadMpf;
    }

    public void setCantidadMpf(InputText cantidadMpf) {
        this.cantidadMpf = cantidadMpf;
    }

    public boolean isApareceRiesgoTrabajo() {
        return apareceRiesgoTrabajo;
    }

    public void setApareceRiesgoTrabajo(boolean apareceRiesgoTrabajo) {
        this.apareceRiesgoTrabajo = apareceRiesgoTrabajo;
    }


    public Boolean getMorfologiaVisible() {
        return morfologiaVisible;
    }

    public void setMorfologiaVisible(Boolean morfologiaVisible) {
        this.morfologiaVisible = morfologiaVisible;
    }

    public Integer getEdadEnSemanas() {
        return edadEnSemanas;
    }

    public void setEdadEnSemanas(Integer edadEnSemanas) {
        this.edadEnSemanas = edadEnSemanas;
    }

    public boolean getMuestraRiesgoTrabajo() {
        return muestraRiesgoTrabajo;
    }

    public void setMuestraRiesgoTrabajo(boolean muestraRiesgoTrabajo) {
        this.muestraRiesgoTrabajo = muestraRiesgoTrabajo;
    }

    public boolean isMuestraTipoUrgencia() {
        return muestraTipoUrgencia;
    }

    public void setMuestraTipoUrgencia(boolean muestraTipoUrgencia) {
        this.muestraTipoUrgencia = muestraTipoUrgencia;
    }

    public boolean isMuestraCitado() {
        return muestraCitado;
    }

    public void setMuestraCitado(boolean muestraCitado) {
        this.muestraCitado = muestraCitado;
    }

    public boolean isMuestraPase() {
        return muestraPase;
    }

    public void setMuestraPase(boolean muestraPase) {
        this.muestraPase = muestraPase;
    }

    public boolean isMuestraRecetas() {
        return muestraRecetas;
    }

    public void setMuestraRecetas(boolean muestraRecetas) {
        this.muestraRecetas = muestraRecetas;
    }

    public boolean isMuestraVisita() {
        return muestraVisita;
    }

    public void setMuestraVisita(boolean muestraVisita) {
        this.muestraVisita = muestraVisita;
    }

    /**
     * @return the cveTipoInformacionAdicDiagP
     */
    public Integer getCveTipoInformacionAdicDiagP() {
        return cveTipoInformacionAdicDiagP;
    }

    /**
     * @param cveTipoInformacionAdicDiagP the cveTipoInformacionAdicDiagP to set
     */
    public void setCveTipoInformacionAdicDiagP(Integer cveTipoInformacionAdicDiagP) {
        this.cveTipoInformacionAdicDiagP = cveTipoInformacionAdicDiagP;
    }

    /**
     * @return the cveTipoInformacionAdicDiagA
     */
    public Integer getCveTipoInformacionAdicDiagA() {
        return cveTipoInformacionAdicDiagA;
    }

    /**
     * @param cveTipoInformacionAdicDiagA the cveTipoInformacionAdicDiagA to set
     */
    public void setCveTipoInformacionAdicDiagA(Integer cveTipoInformacionAdicDiagA) {
        this.cveTipoInformacionAdicDiagA = cveTipoInformacionAdicDiagA;
    }

    /**
     * @return the cveTipoInformacionAdicDiagC
     */
    public Integer getCveTipoInformacionAdicDiagC() {
        return cveTipoInformacionAdicDiagC;
    }

    /**
     * @param cveTipoInformacionAdicDiagC the cveTipoInformacionAdicDiagC to set
     */
    public void setCveTipoInformacionAdicDiagC(Integer cveTipoInformacionAdicDiagC) {
        this.cveTipoInformacionAdicDiagC = cveTipoInformacionAdicDiagC;
    }

    public String getDxPOriginal() {
        return dxPOriginal;
    }

    public void setDxPOriginal(String dxPOriginal) {
        this.dxPOriginal = dxPOriginal;
    }

    public Boolean getEsRepetido() {
        return esRepetido;
    }

    public void setEsRepetido(Boolean esRepetido) {
        this.esRepetido = esRepetido;
    }

    public List<AccederDto> getListaBeneficiarios() {
        return listaBeneficiarios;
    }

    public void setListaBeneficiarios(List<AccederDto> listaBeneficiarios) {
        this.listaBeneficiarios = listaBeneficiarios;
    }

    public InputText getNumBeneficiario() {
        return numBeneficiario;
    }

    public void setNumBeneficiario(InputText numBeneficiario) {
        this.numBeneficiario = numBeneficiario;
    }

    public AccederDto getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(AccederDto beneficiario) {
        this.beneficiario = beneficiario;
    }

}