package mx.gob.imss.simo.comun.dto;

public class DelegacionesUmaesDto {

	private String id;
	private String cveDelegacionUmae;
	private String nombreDelegacionUmae;

	public String getCveDelegacionUmae() {
		return cveDelegacionUmae;
	}

	public void setCveDelegacionUmae(String cveDelegacionUmae) {
		this.cveDelegacionUmae = cveDelegacionUmae;
	}

	public String getNombreDelegacionUmae() {
		return nombreDelegacionUmae;
	}

	public void setNombreDelegacionUmae(String nombreDelegacionUmae) {
		this.nombreDelegacionUmae = nombreDelegacionUmae;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DelegacionesUmaesDto [id=" + id + ", cveDelegacionUmae="
				+ cveDelegacionUmae + ", nombreDelegacionUmae="
				+ nombreDelegacionUmae + "]";
	}

}
