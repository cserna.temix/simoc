package mx.gob.imss.simo.consultaexterna.dto;

import java.util.Date;

public class ConsultaExternaDtoCE {

	private String idCapturista;
	private String idPerfil;
	private String idDelegacion;
	private String idUnidadMedica;

	private Date fecNacimiento;
	private Integer edad;
	private String nombresMedi;
	private String aPaternoMedi;
	private String aMaternoMedi;
	private boolean vigencia;
	private String ocasionServicio;

	private String id;
	private Integer numConsulta;
	private Date fecha;
	private String matriculaMedico;
	private String nombreMedico;
	private String cveEspecialidad;
	private String especialidad;
	private String especialidadSelc;
	private String desEspecialidad;
	private String tipoPrestador;
	private String desTipoPrestador;
	private String consultorio;
	private String horasTrabajadas;
	private String turno;
	private String desTurno;
	private Integer consultasNoOtorgadas;
	private Integer citasNoCumplidas;
	private String nss;
	private String agregadoMedico;
	private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private String unidadAdscripcion;
	private String nombreUnidadMed;
	private String horaCita;
	private Integer primeraVez;
	private Integer citado;
	private Integer pase;
	private Integer recetas;
	private Integer diasIncapacidad;
	private Integer visitas;
	private Integer metodoPpf;
	private Integer cantidadMpf;
	private Integer accidentesLesiones;
	private Integer riesgosTrabajo;
	private Integer actiPerParamedico;
	private Integer tipoDocumento;
	private String codigoCie;
	private Integer cveTipoUrgencia;

	private String diagnosticoPrincipal;
	private String desDiagPrin;
	private String cveInforAdicPrin;
	private String cveTipoPrin;
	private Date fecCirugiaPrin;

	private Integer diagAcc;

	private String diagnosticoAdicional;
	private String desDiagAdic;
	private Integer primeraVezDiagAdic;
	private String cveInforAdicAdic;
	private String cveTipoAdic;
	private Date fecCirugiaAdic;

	private String diagnosticoComplemento;
	private String desDiagComp;
	private Integer primeraVezDiagComp;
	private String cveInforAdicComp;
	private String cveTipoComp;
	private Date fecCirugiaComp;

	private String procedimientoPrincipal;
	private String desProcPrin;
	private String procedimientoAdicional;
	private String desProcAdic;
	private String procedimientoComplemento;
	private String desProcComp;

	private Date fecCaptura;
	private String morfologia;

	private Boolean editable = Boolean.TRUE;
	private Boolean consultar = Boolean.TRUE;

	private int cveRamaAseguramiento;
	private String descRamaAseguramiento;
	private Integer cveCD4Principal;
	private Integer cveCD4Adicional;
	private Integer cveCD4Compl;
	private Integer numTipoServicio;
	private Integer cveTipoEmbarazo;
	private Integer cveTipoEmbarazoAdicional;
	private Integer cveTipoEmbarazoComplementario;
	
	private String cveCurp;
	private Integer indVigente;
	private Integer indPacEncontrado;
	private Date stpUltConsultaVig;
	private String cveIdee;
	private Integer cveTipoInfoAdDiagPrin;
	private Integer cveTipoInfoAdDiagAdic;
	private Integer cveTipoInfoAdDiagCompl;
	
	public ConsultaExternaDtoCE() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConsultaExternaDtoCE(Date fecha, String matriculaMedico, String consultorio, String horasTrabajadas,
			Integer consultasNoOtorgadas, Integer citasNoCumplidas) {
		this.fecha = fecha;
		this.matriculaMedico = matriculaMedico;
		this.consultorio = consultorio;
		this.horasTrabajadas = horasTrabajadas;
		this.consultasNoOtorgadas = consultasNoOtorgadas;
		this.citasNoCumplidas = citasNoCumplidas;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the especialidadSelc
	 */
	public String getEspecialidadSelc() {
		return especialidadSelc;
	}

	/**
	 * @param especialidadSelc
	 *            the especialidadSelc to set
	 */
	public void setEspecialidadSelc(String especialidadSelc) {
		this.especialidadSelc = especialidadSelc;
	}

	/**
	 * @return the cveEspecialidad
	 */
	public String getCveEspecialidad() {
		return cveEspecialidad;
	}

	/**
	 * @param cveEspecialidad
	 *            the cveEspecialidad to set
	 */
	public void setCveEspecialidad(String cveEspecialidad) {
		this.cveEspecialidad = cveEspecialidad;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the matriculaMedico
	 */
	public String getMatriculaMedico() {
		return matriculaMedico;
	}

	/**
	 * @param matriculaMedico
	 *            the matriculaMedico to set
	 */
	public void setMatriculaMedico(String matriculaMedico) {
		this.matriculaMedico = matriculaMedico;
	}

	/**
	 * @return the nombreMedico
	 */
	public String getNombreMedico() {
		return nombreMedico;
	}

	/**
	 * @param nombreMedico
	 *            the nombreMedico to set
	 */
	public void setNombreMedico(String nombreMedico) {
		this.nombreMedico = nombreMedico;
	}

	/**
	 * @return the especialidad
	 */
	public String getEspecialidad() {
		return especialidad;
	}

	/**
	 * @param especialidad
	 *            the especialidad to set
	 */
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	/**
	 * @return the consultorio
	 */
	public String getConsultorio() {
		return consultorio;
	}

	/**
	 * @param consultorio
	 *            the consultorio to set
	 */
	public void setConsultorio(String consultorio) {
		this.consultorio = consultorio;
	}

	/**
	 * @return the horasTrabajadas
	 */
	public String getHorasTrabajadas() {
		return horasTrabajadas;
	}

	/**
	 * @param horasTrabajadas
	 *            the horasTrabajadas to set
	 */
	public void setHorasTrabajadas(String horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	/**
	 * @return the turno
	 */
	public String getTurno() {
		return turno;
	}

	/**
	 * @param turno
	 *            the turno to set
	 */
	public void setTurno(String turno) {
		this.turno = turno;
	}

	/**
	 * @return the consultasNoOtorgadas
	 */
	public Integer getConsultasNoOtorgadas() {
		return consultasNoOtorgadas;
	}

	/**
	 * @param consultasNoOtorgadas
	 *            the consultasNoOtorgadas to set
	 */
	public void setConsultasNoOtorgadas(Integer consultasNoOtorgadas) {
		this.consultasNoOtorgadas = consultasNoOtorgadas;
	}

	/**
	 * @return the citasNoCumplidas
	 */
	public Integer getCitasNoCumplidas() {
		return citasNoCumplidas;
	}

	/**
	 * @param citasNoCumplidas
	 *            the citasNoCumplidas to set
	 */
	public void setCitasNoCumplidas(Integer citasNoCumplidas) {
		this.citasNoCumplidas = citasNoCumplidas;
	}

	/**
	 * @return the nss
	 */
	public String getNss() {
		return nss;
	}

	/**
	 * @param nss
	 *            the nss to set
	 */
	public void setNss(String nss) {
		this.nss = nss;
	}

	/**
	 * @return the agregadoMedico
	 */
	public String getAgregadoMedico() {
		return agregadoMedico;
	}

	/**
	 * @param agregadoMedico
	 *            the agregadoMedico to set
	 */
	public void setAgregadoMedico(String agregadoMedico) {
		this.agregadoMedico = agregadoMedico;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apePaterno
	 */
	public String getApePaterno() {
		return apePaterno;
	}

	/**
	 * @param apePaterno
	 *            the apePaterno to set
	 */
	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	/**
	 * @return the apeMaterno
	 */
	public String getApeMaterno() {
		return apeMaterno;
	}

	/**
	 * @param apeMaterno
	 *            the apeMaterno to set
	 */
	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	/**
	 * @return the unidadAdscripcion
	 */
	public String getUnidadAdscripcion() {
		return unidadAdscripcion;
	}

	/**
	 * @param unidadAdscripcion
	 *            the unidadAdscripcion to set
	 */
	public void setUnidadAdscripcion(String unidadAdscripcion) {
		this.unidadAdscripcion = unidadAdscripcion;
	}

	/**
	 * @return the nombreUnidadMed
	 */
	public String getNombreUnidadMed() {
		return nombreUnidadMed;
	}

	/**
	 * @param nombreUnidadMed
	 *            the nombreUnidadMed to set
	 */
	public void setNombreUnidadMed(String nombreUnidadMed) {
		this.nombreUnidadMed = nombreUnidadMed;
	}

	/**
	 * @return the horaCita
	 */
	public String getHoraCita() {
		return horaCita;
	}

	/**
	 * @param horaCita
	 *            the horaCita to set
	 */
	public void setHoraCita(String horaCita) {
		this.horaCita = horaCita;
	}

	/**
	 * @return the primeraVez
	 */
	public Integer getPrimeraVez() {
		return primeraVez;
	}

	/**
	 * @param primeraVez
	 *            the primeraVez to set
	 */
	public void setPrimeraVez(Integer primeraVez) {
		this.primeraVez = primeraVez;
	}

	/**
	 * @return the citado
	 */
	public Integer getCitado() {
		return citado;
	}

	/**
	 * @param citado
	 *            the citado to set
	 */
	public void setCitado(Integer citado) {
		this.citado = citado;
	}

	/**
	 * @return the pase
	 */
	public Integer getPase() {
		return pase;
	}

	/**
	 * @param pase
	 *            the pase to set
	 */
	public void setPase(Integer pase) {
		this.pase = pase;
	}

	/**
	 * @return the recetas
	 */
	public Integer getRecetas() {
		return recetas;
	}

	/**
	 * @param recetas
	 *            the recetas to set
	 */
	public void setRecetas(Integer recetas) {
		this.recetas = recetas;
	}

	/**
	 * @return the diasIncapacidad
	 */
	public Integer getDiasIncapacidad() {
		return diasIncapacidad;
	}

	/**
	 * @param diasIncapacidad
	 *            the diasIncapacidad to set
	 */
	public void setDiasIncapacidad(Integer diasIncapacidad) {
		this.diasIncapacidad = diasIncapacidad;
	}

	/**
	 * @return the visitas
	 */
	public Integer getVisitas() {
		return visitas;
	}

	/**
	 * @param visitas
	 *            the visitas to set
	 */
	public void setVisitas(Integer visitas) {
		this.visitas = visitas;
	}

	/**
	 * @return the metodoPpf
	 */
	public Integer getMetodoPpf() {
		return metodoPpf;
	}

	/**
	 * @param metodoPpf
	 *            the metodoPpf to set
	 */
	public void setMetodoPpf(Integer metodoPpf) {
		this.metodoPpf = metodoPpf;
	}

	/**
	 * @return the cantidadMpf
	 */
	public Integer getCantidadMpf() {
		return cantidadMpf;
	}

	/**
	 * @param cantidadMpf
	 *            the cantidadMpf to set
	 */
	public void setCantidadMpf(Integer cantidadMpf) {
		this.cantidadMpf = cantidadMpf;
	}

	/**
	 * @return the accidentesLesiones
	 */
	public Integer getAccidentesLesiones() {
		return accidentesLesiones;
	}

	/**
	 * @param accidentesLesiones
	 *            the accidentesLesiones to set
	 */
	public void setAccidentesLesiones(Integer accidentesLesiones) {
		this.accidentesLesiones = accidentesLesiones;
	}

	/**
	 * @return the riesgosTrabajo
	 */
	public Integer getRiesgosTrabajo() {
		return riesgosTrabajo;
	}

	/**
	 * @param riesgosTrabajo
	 *            the riesgosTrabajo to set
	 */
	public void setRiesgosTrabajo(Integer riesgosTrabajo) {
		this.riesgosTrabajo = riesgosTrabajo;
	}

	/**
	 * @return the diagnosticoPrincipal
	 */
	public String getDiagnosticoPrincipal() {
		return diagnosticoPrincipal;
	}

	/**
	 * @param diagnosticoPrincipal
	 *            the diagnosticoPrincipal to set
	 */
	public void setDiagnosticoPrincipal(String diagnosticoPrincipal) {
		this.diagnosticoPrincipal = diagnosticoPrincipal;
	}

	/**
	 * @return the desDiagPrin
	 */
	public String getDesDiagPrin() {
		return desDiagPrin;
	}

	/**
	 * @param desDiagPrin
	 *            the desDiagPrin to set
	 */
	public void setDesDiagPrin(String desDiagPrin) {
		this.desDiagPrin = desDiagPrin;
	}

	/**
	 * @return the procedimientoPrincipal
	 */
	public String getProcedimientoPrincipal() {
		return procedimientoPrincipal;
	}

	/**
	 * @param procedimientoPrincipal
	 *            the procedimientoPrincipal to set
	 */
	public void setProcedimientoPrincipal(String procedimientoPrincipal) {
		this.procedimientoPrincipal = procedimientoPrincipal;
	}

	/**
	 * @return the desProcPrin
	 */
	public String getDesProcPrin() {
		return desProcPrin;
	}

	/**
	 * @param desProcPrin
	 *            the desProcPrin to set
	 */
	public void setDesProcPrin(String desProcPrin) {
		this.desProcPrin = desProcPrin;
	}

	/**
	 * @return the diagnosticoAdicional
	 */
	public String getDiagnosticoAdicional() {
		return diagnosticoAdicional;
	}

	/**
	 * @param diagnosticoAdicional
	 *            the diagnosticoAdicional to set
	 */
	public void setDiagnosticoAdicional(String diagnosticoAdicional) {
		this.diagnosticoAdicional = diagnosticoAdicional;
	}

	/**
	 * @return the desDiagAdic
	 */
	public String getDesDiagAdic() {
		return desDiagAdic;
	}

	/**
	 * @param desDiagAdic
	 *            the desDiagAdic to set
	 */
	public void setDesDiagAdic(String desDiagAdic) {
		this.desDiagAdic = desDiagAdic;
	}

	/**
	 * @return the primeraVezDiagAdic
	 */
	public Integer getPrimeraVezDiagAdic() {
		return primeraVezDiagAdic;
	}

	/**
	 * @param primeraVezDiagAdic
	 *            the primeraVezDiagAdic to set
	 */
	public void setPrimeraVezDiagAdic(Integer primeraVezDiagAdic) {
		this.primeraVezDiagAdic = primeraVezDiagAdic;
	}

	/**
	 * @return the procedimientoAdicional
	 */
	public String getProcedimientoAdicional() {
		return procedimientoAdicional;
	}

	/**
	 * @param procedimientoAdicional
	 *            the procedimientoAdicional to set
	 */
	public void setProcedimientoAdicional(String procedimientoAdicional) {
		this.procedimientoAdicional = procedimientoAdicional;
	}

	/**
	 * @return the desProcAdic
	 */
	public String getDesProcAdic() {
		return desProcAdic;
	}

	/**
	 * @param desProcAdic
	 *            the desProcAdic to set
	 */
	public void setDesProcAdic(String desProcAdic) {
		this.desProcAdic = desProcAdic;
	}

	/**
	 * @return the diagnosticoComplemento
	 */
	public String getDiagnosticoComplemento() {
		return diagnosticoComplemento;
	}

	/**
	 * @param diagnosticoComplemento
	 *            the diagnosticoComplemento to set
	 */
	public void setDiagnosticoComplemento(String diagnosticoComplemento) {
		this.diagnosticoComplemento = diagnosticoComplemento;
	}

	/**
	 * @return the desDiagComp
	 */
	public String getDesDiagComp() {
		return desDiagComp;
	}

	/**
	 * @param desDiagComp
	 *            the desDiagComp to set
	 */
	public void setDesDiagComp(String desDiagComp) {
		this.desDiagComp = desDiagComp;
	}

	/**
	 * @return the primeraVezDiagComp
	 */
	public Integer getPrimeraVezDiagComp() {
		return primeraVezDiagComp;
	}

	/**
	 * @param primeraVezDiagComp
	 *            the primeraVezDiagComp to set
	 */
	public void setPrimeraVezDiagComp(Integer primeraVezDiagComp) {
		this.primeraVezDiagComp = primeraVezDiagComp;
	}

	/**
	 * @return the procedimientoComplemento
	 */
	public String getProcedimientoComplemento() {
		return procedimientoComplemento;
	}

	/**
	 * @param procedimientoComplemento
	 *            the procedimientoComplemento to set
	 */
	public void setProcedimientoComplemento(String procedimientoComplemento) {
		this.procedimientoComplemento = procedimientoComplemento;
	}

	/**
	 * @return the desProcComp
	 */
	public String getDesProcComp() {
		return desProcComp;
	}

	/**
	 * @param desProcComp
	 *            the desProcComp to set
	 */
	public void setDesProcComp(String desProcComp) {
		this.desProcComp = desProcComp;
	}

	/**
	 * @return the actiPerParamedico
	 */
	public Integer getActiPerParamedico() {
		return actiPerParamedico;
	}

	/**
	 * @param actiPerParamedico
	 *            the actiPerParamedico to set
	 */
	public void setActiPerParamedico(Integer actiPerParamedico) {
		this.actiPerParamedico = actiPerParamedico;
	}

	/**
	 * @return the tipoPrestador
	 */
	public String getTipoPrestador() {
		return tipoPrestador;
	}

	/**
	 * @param tipoPrestador
	 *            the tipoPrestador to set
	 */
	public void setTipoPrestador(String tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}

	/**
	 * @return the tipoDocumento
	 */
	public Integer getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento
	 *            the tipoDocumento to set
	 */
	public void setTipoDocumento(Integer tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the codigoCie
	 */
	public String getCodigoCie() {
		return codigoCie;
	}

	/**
	 * @param codigoCie
	 *            the codigoCie to set
	 */
	public void setCodigoCie(String codigoCie) {
		this.codigoCie = codigoCie;
	}

	/**
	 * @return the numConsulta
	 */
	public Integer getNumConsulta() {
		return numConsulta;
	}

	/**
	 * @param numConsulta
	 *            the numConsulta to set
	 */
	public void setNumConsulta(Integer numConsulta) {
		this.numConsulta = numConsulta;
	}

	/**
	 * @return the cveTipoUrgencia
	 */
	public Integer getCveTipoUrgencia() {
		return cveTipoUrgencia;
	}

	/**
	 * @param cveTipoUrgencia
	 *            the cveTipoUrgencia to set
	 */
	public void setCveTipoUrgencia(Integer cveTipoUrgencia) {
		this.cveTipoUrgencia = cveTipoUrgencia;
	}

	/**
	 * @return the cveInforAdicPrin
	 */
	public String getCveInforAdicPrin() {
		return cveInforAdicPrin;
	}

	/**
	 * @param cveInforAdicPrin
	 *            the cveInforAdicPrin to set
	 */
	public void setCveInforAdicPrin(String cveInforAdicPrin) {
		this.cveInforAdicPrin = cveInforAdicPrin;
	}

	/**
	 * @return the cveTipoPrin
	 */
	public String getCveTipoPrin() {
		return cveTipoPrin;
	}

	/**
	 * @param cveTipoPrin
	 *            the cveTipoPrin to set
	 */
	public void setCveTipoPrin(String cveTipoPrin) {
		this.cveTipoPrin = cveTipoPrin;
	}

	/**
	 * @return the fecCirugiaPrin
	 */
	public Date getFecCirugiaPrin() {
		return fecCirugiaPrin;
	}

	/**
	 * @param fecCirugiaPrin
	 *            the fecCirugiaPrin to set
	 */
	public void setFecCirugiaPrin(Date fecCirugiaPrin) {
		this.fecCirugiaPrin = fecCirugiaPrin;
	}

	/**
	 * @return the cveInforAdicAdic
	 */
	public String getCveInforAdicAdic() {
		return cveInforAdicAdic;
	}

	/**
	 * @param cveInforAdicAdic
	 *            the cveInforAdicAdic to set
	 */
	public void setCveInforAdicAdic(String cveInforAdicAdic) {
		this.cveInforAdicAdic = cveInforAdicAdic;
	}

	/**
	 * @return the cveTipoAdic
	 */
	public String getCveTipoAdic() {
		return cveTipoAdic;
	}

	/**
	 * @param cveTipoAdic
	 *            the cveTipoAdic to set
	 */
	public void setCveTipoAdic(String cveTipoAdic) {
		this.cveTipoAdic = cveTipoAdic;
	}

	/**
	 * @return the fecCirugiaAdic
	 */
	public Date getFecCirugiaAdic() {
		return fecCirugiaAdic;
	}

	/**
	 * @param fecCirugiaAdic
	 *            the fecCirugiaAdic to set
	 */
	public void setFecCirugiaAdic(Date fecCirugiaAdic) {
		this.fecCirugiaAdic = fecCirugiaAdic;
	}

	/**
	 * @return the cveInforAdicComp
	 */
	public String getCveInforAdicComp() {
		return cveInforAdicComp;
	}

	/**
	 * @param cveInforAdicComp
	 *            the cveInforAdicComp to set
	 */
	public void setCveInforAdicComp(String cveInforAdicComp) {
		this.cveInforAdicComp = cveInforAdicComp;
	}

	/**
	 * @return the cveTipoComp
	 */
	public String getCveTipoComp() {
		return cveTipoComp;
	}

	/**
	 * @param cveTipoComp
	 *            the cveTipoComp to set
	 */
	public void setCveTipoComp(String cveTipoComp) {
		this.cveTipoComp = cveTipoComp;
	}

	/**
	 * @return the fecCirugiaComp
	 */
	public Date getFecCirugiaComp() {
		return fecCirugiaComp;
	}

	/**
	 * @param fecCirugiaComp
	 *            the fecCirugiaComp to set
	 */
	public void setFecCirugiaComp(Date fecCirugiaComp) {
		this.fecCirugiaComp = fecCirugiaComp;
	}

	/**
	 * @return the fecCaptura
	 */
	public Date getFecCaptura() {
		return fecCaptura;
	}

	/**
	 * @param fecCaptura
	 *            the fecCaptura to set
	 */
	public void setFecCaptura(Date fecCaptura) {
		this.fecCaptura = fecCaptura;
	}

	/**
	 * @return the idCapturista
	 */
	public String getIdCapturista() {
		return idCapturista;
	}

	/**
	 * @param idCapturista
	 *            the idCapturista to set
	 */
	public void setIdCapturista(String idCapturista) {
		this.idCapturista = idCapturista;
	}

	/**
	 * @return the idPerfil
	 */
	public String getIdPerfil() {
		return idPerfil;
	}

	/**
	 * @param idPerfil
	 *            the idPerfil to set
	 */
	public void setIdPerfil(String idPerfil) {
		this.idPerfil = idPerfil;
	}

	/**
	 * @return the idDelegacion
	 */
	public String getIdDelegacion() {
		return idDelegacion;
	}

	/**
	 * @param idDelegacion
	 *            the idDelegacion to set
	 */
	public void setIdDelegacion(String idDelegacion) {
		this.idDelegacion = idDelegacion;
	}

	/**
	 * @return the idUnidadMedica
	 */
	public String getIdUnidadMedica() {
		return idUnidadMedica;
	}

	/**
	 * @param idUnidadMedica
	 *            the idUnidadMedica to set
	 */
	public void setIdUnidadMedica(String idUnidadMedica) {
		this.idUnidadMedica = idUnidadMedica;
	}

	/**
	 * @return the diagAcc
	 */
	public Integer getDiagAcc() {
		return diagAcc;
	}

	/**
	 * @param diagAcc
	 *            the diagAcc to set
	 */
	public void setDiagAcc(Integer diagAcc) {
		this.diagAcc = diagAcc;
	}

	/**
	 * @return the fecNacimiento
	 */
	public Date getFecNacimiento() {
		return fecNacimiento;
	}

	/**
	 * @param fecNacimiento
	 *            the fecNacimiento to set
	 */
	public void setFecNacimiento(Date fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

	/**
	 * @return the nombresMedi
	 */
	public String getNombresMedi() {
		return nombresMedi;
	}

	/**
	 * @param nombresMedi
	 *            the nombresMedi to set
	 */
	public void setNombresMedi(String nombresMedi) {
		this.nombresMedi = nombresMedi;
	}

	/**
	 * @return the aPaternoMedi
	 */
	public String getaPaternoMedi() {
		return aPaternoMedi;
	}

	/**
	 * @param aPaternoMedi
	 *            the aPaternoMedi to set
	 */
	public void setaPaternoMedi(String aPaternoMedi) {
		this.aPaternoMedi = aPaternoMedi;
	}

	/**
	 * @return the aMaternoMedi
	 */
	public String getaMaternoMedi() {
		return aMaternoMedi;
	}

	/**
	 * @param aMaternoMedi
	 *            the aMaternoMedi to set
	 */
	public void setaMaternoMedi(String aMaternoMedi) {
		this.aMaternoMedi = aMaternoMedi;
	}

	/**
	 * @return the vigencia
	 */
	public boolean isVigencia() {
		return vigencia;
	}

	/**
	 * @param vigencia
	 *            the vigencia to set
	 */
	public void setVigencia(boolean vigencia) {
		this.vigencia = vigencia;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the desEspecialidad
	 */
	public String getDesEspecialidad() {
		return desEspecialidad;
	}

	/**
	 * @param desEspecialidad
	 *            the desEspecialidad to set
	 */
	public void setDesEspecialidad(String desEspecialidad) {
		this.desEspecialidad = desEspecialidad;
	}

	/**
	 * @return the ocasionServicio
	 */
	public String getOcasionServicio() {
		return ocasionServicio;
	}

	/**
	 * @param ocasionServicio
	 *            the ocasionServicio to set
	 */
	public void setOcasionServicio(String ocasionServicio) {
		this.ocasionServicio = ocasionServicio;
	}

	/**
	 * @return the desTipoPrestador
	 */
	public String getDesTipoPrestador() {
		return desTipoPrestador;
	}

	/**
	 * @param desTipoPrestador
	 *            the desTipoPrestador to set
	 */
	public void setDesTipoPrestador(String desTipoPrestador) {
		this.desTipoPrestador = desTipoPrestador;
	}

	/**
	 * @return the desTurno
	 */
	public String getDesTurno() {
		return desTurno;
	}

	/**
	 * @param desTurno
	 *            the desTurno to set
	 */
	public void setDesTurno(String desTurno) {
		this.desTurno = desTurno;
	}

	/**
	 * @return the edad
	 */
	public Integer getEdad() {
		return edad;
	}

	/**
	 * @param edad
	 *            the edad to set
	 */
	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	/**
	 * @return the consultar
	 */
	public Boolean getConsultar() {
		return consultar;
	}

	/**
	 * @param consultar
	 *            the consultar to set
	 */
	public void setConsultar(Boolean consultar) {
		this.consultar = consultar;
	}

	public int getCveRamaAseguramiento() {
		return cveRamaAseguramiento;
	}

	public void setCveRamaAseguramiento(int cveRamaAseguramiento) {
		this.cveRamaAseguramiento = cveRamaAseguramiento;
	}

	public String getDescRamaAseguramiento() {
		return descRamaAseguramiento;
	}

	public void setDescRamaAseguramiento(String descRamaAseguramiento) {
		this.descRamaAseguramiento = descRamaAseguramiento;
	}

	public String getMorfologia() {
		return morfologia;
	}

	public void setMorfologia(String morfologia) {
		this.morfologia = morfologia;
	}

	/**
	 * @return the numTipoServicio
	 */
	public Integer getNumTipoServicio() {
		return numTipoServicio;
	}

	/**
	 * @param numTipoServicio the numTipoServicio to set
	 */
	public void setNumTipoServicio(Integer numTipoServicio) {
		this.numTipoServicio = numTipoServicio;
	}

	public Integer getCveCD4Principal() {
		return cveCD4Principal;
	}

	public void setCveCD4Principal(Integer cveCD4Principal) {
		this.cveCD4Principal = cveCD4Principal;
	}

	public Integer getCveCD4Adicional() {
		return cveCD4Adicional;
	}

	public void setCveCD4Adicional(Integer cveCD4Adicional) {
		this.cveCD4Adicional = cveCD4Adicional;
	}

	public Integer getCveCD4Compl() {
		return cveCD4Compl;
	}

	public void setCveCD4Compl(Integer cveCD4Compl) {
		this.cveCD4Compl = cveCD4Compl;
	}

	public Integer getCveTipoEmbarazo() {
		return cveTipoEmbarazo;
	}

	public void setCveTipoEmbarazo(Integer cveTipoEmbarazo) {
		this.cveTipoEmbarazo = cveTipoEmbarazo;
	}

	public Integer getCveTipoEmbarazoAdicional() {
		return cveTipoEmbarazoAdicional;
	}

	public void setCveTipoEmbarazoAdicional(Integer cveTipoEmbarazoAdicional) {
		this.cveTipoEmbarazoAdicional = cveTipoEmbarazoAdicional;
	}

	public Integer getCveTipoEmbarazoComplementario() {
		return cveTipoEmbarazoComplementario;
	}

	public void setCveTipoEmbarazoComplementario(
			Integer cveTipoEmbarazoComplementario) {
		this.cveTipoEmbarazoComplementario = cveTipoEmbarazoComplementario;
	}

	public String getCveCurp() {
		return cveCurp;
	}

	public void setCveCurp(String cveCurp) {
		this.cveCurp = cveCurp;
	}

	public Integer getIndVigente() {
		return indVigente;
	}

	public void setIndVigente(Integer indVigente) {
		this.indVigente = indVigente;
	}

	public Integer getIndPacEncontrado() {
		return indPacEncontrado;
	}

	public void setIndPacEncontrado(Integer indPacEncontrado) {
		this.indPacEncontrado = indPacEncontrado;
	}

	public Date getStpUltConsultaVig() {
		return stpUltConsultaVig;
	}

	public void setStpUltConsultaVig(Date stpUltConsultaVig) {
		this.stpUltConsultaVig = stpUltConsultaVig;
	}

	public String getCveIdee() {
		return cveIdee;
	}

	public void setCveIdee(String cveIdee) {
		this.cveIdee = cveIdee;
	}

	/**
	 * @return the cveTipoInfoAdDiagPrin
	 */
	public Integer getCveTipoInfoAdDiagPrin() {
		return cveTipoInfoAdDiagPrin;
	}

	/**
	 * @param cveTipoInfoAdDiagPrin the cveTipoInfoAdDiagPrin to set
	 */
	public void setCveTipoInfoAdDiagPrin(Integer cveTipoInfoAdDiagPrin) {
		this.cveTipoInfoAdDiagPrin = cveTipoInfoAdDiagPrin;
	}

	/**
	 * @return the cveTipoInfoAdDiagAdic
	 */
	public Integer getCveTipoInfoAdDiagAdic() {
		return cveTipoInfoAdDiagAdic;
	}

	/**
	 * @param cveTipoInfoAdDiagAdic the cveTipoInfoAdDiagAdic to set
	 */
	public void setCveTipoInfoAdDiagAdic(Integer cveTipoInfoAdDiagAdic) {
		this.cveTipoInfoAdDiagAdic = cveTipoInfoAdDiagAdic;
	}

	/**
	 * @return the cveTipoInfoAdDiagCompl
	 */
	public Integer getCveTipoInfoAdDiagCompl() {
		return cveTipoInfoAdDiagCompl;
	}

	/**
	 * @param cveTipoInfoAdDiagCompl the cveTipoInfoAdDiagCompl to set
	 */
	public void setCveTipoInfoAdDiagCompl(Integer cveTipoInfoAdDiagCompl) {
		this.cveTipoInfoAdDiagCompl = cveTipoInfoAdDiagCompl;
	}

	
}
