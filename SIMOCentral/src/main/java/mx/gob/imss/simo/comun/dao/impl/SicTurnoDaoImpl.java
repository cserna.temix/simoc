/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTurnoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTurno;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicTurnoDao")
public class SicTurnoDaoImpl implements SicTurnoDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicTurno> burcarTodos() {
		// SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTurno> listTurno = new ArrayList<SicTurno>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listTurno = dsEsp.createQuery(SicTurno.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listTurno;
	}

	@Override
	public List<SicTurno> burcarPorId(Integer cveTurno) {
		// SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTurno> listTurno = new ArrayList<SicTurno>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listTurno = dsEsp.createQuery(SicTurno.class).field("cveTurno")
					.equal(cveTurno).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listTurno;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
