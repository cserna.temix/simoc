/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.mapping.Mapper;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicPeriodosImssDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.comun.entity.negocio.SitPeriodoOperacion;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoCaptura;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;

/**
 * @author francisco.rodriguez
 * 
 */
@Repository("sicPeriodosImssDao")
public class SicPeriodosImssDaoImpl implements SicPeriodosImssDao {

	final static Logger logger = Logger.getLogger(SicPeriodosImssDaoImpl.class.getName());
	
//	@Autowired
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicPeriodosImssDao#burcarPorPeriodo(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public SicPeriodosImss burcarPorPeriodo(Integer numAnioPeriodo,
			String numMesPeriodo) {
	//SimoDatastore simoDatastore = new SimoDatastore();
		SicPeriodosImss sicPeriodosImss = new SicPeriodosImss();
		try {

			Datastore ds =  MongoManager.INSTANCE.getDatastore();

			sicPeriodosImss = ds.createQuery(SicPeriodosImss.class)
					.field("numAnioPeriodo").equal(numAnioPeriodo)
					.field("numMesPeriodo").equal(numMesPeriodo).get();
			
//			logger.info(ds.createQuery(SicPeriodosImss.class)
//					.field("numAnioPeriodo").equal(numAnioPeriodo)
//					.field("numMesPeriodo").equal(numMesPeriodo).toString());
			
			

		} catch (Exception e) {
			e.getMessage();
		}
		return sicPeriodosImss;
	}
	
	public List<SicPeriodosImss> buscarCataoloPeriodoImss(){
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicPeriodosImss> lstCatalogoPeriodoImss = new ArrayList<SicPeriodosImss>();
		try{
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();
			
			lstCatalogoPeriodoImss = dsEsp.createQuery(SicPeriodosImss.class).asList();
			
		}catch(Exception ex){
			ex.getMessage();
		}
		return lstCatalogoPeriodoImss;
	}
	
	
	public void actualizarPeriodoActualIMSS(SicPeriodosImss periodoIMSS, String cvePresupuestal, String unidadMedica, String modulo){
        //SimoDatastore simoDatastore = new SimoDatastore();
        try{
        	SitPeriodoOperacion sitPeriodoOperacion = burcarPorPeriodoOperacion(periodoIMSS.getCvePeriodo(), cvePresupuestal);
        	Datastore dstore = MongoManager.INSTANCE.getDatastore();
        	if(sitPeriodoOperacion != null){
        		if(sitPeriodoOperacion.getIndCierre() == 0){
        			//Actualiza
        			Query<SitPeriodoOperacion> updateQuery = dstore.createQuery(SitPeriodoOperacion.class).field(Mapper.ID_KEY).equal(sitPeriodoOperacion.getId());
        			UpdateOperations<SitPeriodoOperacion> ops = dstore.createUpdateOperations(SitPeriodoOperacion.class).set("fecCierre", new Date()).set("indCierre", 1);
        			dstore.update(updateQuery, ops);
        		}
        	} else {
        		//Inserta
        		SitPeriodoOperacion stPeriodo = new SitPeriodoOperacion();
        		
        		stPeriodo.setCvePeriodo(periodoIMSS.getCvePeriodo());
        		List<SdTipoCaptura> sdTipoCaptura = new ArrayList<SdTipoCaptura>();
        		sdTipoCaptura.add(new SdTipoCaptura(1, modulo));
        		stPeriodo.setSdTipoCaptura(sdTipoCaptura);
        		List<SdUnidadMedica> sdUnidadMedica = new ArrayList<SdUnidadMedica>();
        		sdUnidadMedica.add(new SdUnidadMedica(cvePresupuestal, unidadMedica));
        		stPeriodo.setSdUnidadMedica(sdUnidadMedica);
        		stPeriodo.setIndCierre(1);
        		stPeriodo.setFecCierre(new Date());
        		
        		dstore.save(stPeriodo);
        	}
        }catch (Exception e) {
               e.getMessage();
               e.printStackTrace();                    
        }            
  }
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SitPeriodoOperacion#burcarPorPeriodo(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public SitPeriodoOperacion burcarPorPeriodoOperacion(Integer periodoOperacion,
			String cvePresupuestal) {
	//SimoDatastore simoDatastore = new SimoDatastore();
	SitPeriodoOperacion sitPeriodoOperacion = new SitPeriodoOperacion();
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();

			sitPeriodoOperacion = ds.createQuery(SitPeriodoOperacion.class)
					.field("cvePeriodo").equal(periodoOperacion)
					.field("sdTipoCaptura.cveTipoCaptura").equal(1)
					.field("sdUnidadMedica.cvePresupuestal").equal(cvePresupuestal).get();
			
//			logger.info(ds.createQuery(SitPeriodoOperacion.class)
//					.field("cvePeriodo").equal(periodoOperacion)
//					.field("sdUnidadMedica.cvePresupuestal").equal(cvePresupuestal).toString());
			
			

		} catch (Exception e) {
			e.getMessage();
		}
		return sitPeriodoOperacion;
	}


//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}
}
