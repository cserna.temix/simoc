/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_ALTA")
public class SicAlta {

	@Id
	private ObjectId id;
	private Integer cveAlta;
	private String desAlta;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveAlta
	 */
	public Integer getCveAlta() {
		return cveAlta;
	}

	/**
	 * @param cveAlta
	 *            the cveAlta to set
	 */
	public void setCveAlta(Integer cveAlta) {
		this.cveAlta = cveAlta;
	}

	/**
	 * @return the desAlta
	 */
	public String getDesAlta() {
		return desAlta;
	}

	/**
	 * @param desAlta
	 *            the desAlta to set
	 */
	public void setDesAlta(String desAlta) {
		this.desAlta = desAlta;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
