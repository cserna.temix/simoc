/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoRegistro;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicTipoRegistroDao {

	/**
	 * @return
	 */
	List<SicTipoRegistro> burcarTodos();

	/**
	 * @param cveTipoRegistro
	 * @return
	 */
	List<SicTipoRegistro> burcarPorId(Integer cveTipoRegistro);
}
