/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_SEXO")
public class SicSexo {

	@Id
	private ObjectId id;
	private Integer cveSexo;
	private String desSexo;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveSexo
	 */
	public Integer getCveSexo() {
		return cveSexo;
	}

	/**
	 * @param cveSexo
	 *            the cveSexo to set
	 */
	public void setCveSexo(Integer cveSexo) {
		this.cveSexo = cveSexo;
	}

	/**
	 * @return the desSexo
	 */
	public String getDesSexo() {
		return desSexo;
	}

	/**
	 * @param desSexo
	 *            the desSexo to set
	 */
	public void setDesSexo(String desSexo) {
		this.desSexo = desSexo;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
