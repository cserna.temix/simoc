/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.aggregation.AggregationPipeline;
import org.mongodb.morphia.aggregation.Sort;
import org.mongodb.morphia.mapping.Mapper;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import org.springframework.stereotype.Repository;

import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.entity.catalogos.SicConstantes;
import mx.gob.imss.simo.comun.entity.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;
import mx.gob.imss.simo.comun.entity.negocio.SitControlCapturaCe;
import mx.gob.imss.simo.comun.entity.negocio.SitControlMedicoEncabezado;
import mx.gob.imss.simo.consultaexterna.dao.ConsultaExternaDao;
import mx.gob.imss.simo.consultaexterna.pojo.SdCapturista;
import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdFormato;
import mx.gob.imss.simo.consultaexterna.pojo.SdPaciente;
import mx.gob.imss.simo.consultaexterna.presentacion.EditarAtencionBean;

/**
 * @author francisco.rrios
 */
@Repository("consultaExternaDao")
public class ConsultaExternaDaoImpl implements ConsultaExternaDao {

    Logger logger = Logger.getLogger(ConsultaExternaDaoImpl.class);

    private final static int ESTATUS_CONSULTA_ENVIADA = 1;
    private final static int ESTATUS_CONSULTA_NO_ENVIADA = 0;

    // @Autowired
    // private ConnectionMongo connectionMongo;

    @Override
    public void insertar(SitConsultaExterna consultaExterna) {

        // SimoDatastore simoDatastore = new SimoDatastore();
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();
            ds.save(consultaExterna);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean actualizar(SitConsultaExterna consutaExterna) {

        Boolean actualizado = Boolean.FALSE;
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();

            Query<SitConsultaExterna> updateQuery = ds.createQuery(SitConsultaExterna.class).field(Mapper.ID_KEY)
                    .equal(consutaExterna.getId());

            UpdateOperations<SitConsultaExterna> ops = ds.createUpdateOperations(SitConsultaExterna.class)
                    .set("sdEncabezado", consutaExterna.getSdEncabezado());

            UpdateResults consutaExternaUr = ds.update(updateQuery, ops);
            if (consutaExternaUr.getUpdatedExisting() || consutaExternaUr.getInsertedCount() > 0) {
                actualizado = Boolean.TRUE;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return actualizado;
    }

    @Override
    public SitConsultaExterna buscar(Date fecha, String matriculaMedico, String idEspecialidad, Integer idTurno) {

        // //SimoDatastore simoDatastore = new SimoDatastore();
        SitConsultaExterna consultaExterna = null;
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();
            // logger.info(fecha.toString());
            String fechaAux = new SimpleDateFormat("dd/MM/yyyy").format(fecha);
            // logger.info(fechaAux);
            Date fechaFormato = new SimpleDateFormat("dd/MM/yyyy").parse(fechaAux);
            // logger.info(fechaFormato.toString());
            consultaExterna = ds.createQuery(SitConsultaExterna.class)
                    .filter("sdEncabezado.fecAltaEncabezado", fechaFormato)
                    .filter("sdEncabezado.sdMedico.cveMatricula", matriculaMedico)
                    .filter("sdEncabezado.sdEspecialidad.cveEspecialidad", idEspecialidad)
                    .filter("sdEncabezado.sdTurno.cveTurno", idTurno).get();
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }
        return consultaExterna;
    }

    /*
     * (non-Javadoc)
     * @see
     * mx.gob.imss.simo.consultaexterna.dao.ConsultaExternaDao#buscarAtenciones
     * (java.util.Date, java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.Integer)
     */
    @Override
    public List<SdEncabezado> buscarAtenciones(Date fecha, String matriculaMedico, String nss, String idEspecialidad,
            String agregadoMedico, String codigoCie, Integer primeraVez, String cvePresupuestal) {

        // SimoDatastore simoDatastore = new SimoDatastore();
        List<SdEncabezado> listAtenciones = new ArrayList<SdEncabezado>();
        List<SitConsultaExterna> listConsultaExterna = null;
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();

            String fechaAux = new SimpleDateFormat("dd/MM/yyyy").format(fecha);
            Date fechaFormato = new SimpleDateFormat("dd/MM/yyyy").parse(fechaAux);

            Query<SitConsultaExterna> query = ds.createQuery(SitConsultaExterna.class)
                    .filter("sdEncabezado.fecAltaEncabezado", fechaFormato);

            query.filter("sdUnidadMedica.cvePresupuestal", cvePresupuestal);

            if (matriculaMedico != null) {
                query.filter("sdEncabezado.sdMedico.cveMatricula", matriculaMedico);
            }
            if (nss != null) {
                query.filter("sdEncabezado.sdFormato.sdPaciente.numNss", nss);
            }
            if (idEspecialidad != null) {
                query.filter("sdEncabezado.sdEspecialidad.cveEspecialidad", idEspecialidad);
            }
            if (agregadoMedico != null) {
                query.filter("sdEncabezado.sdFormato.sdPaciente.refAgregadoMedico", agregadoMedico);
            }
            if (codigoCie != null) {
                query.filter("sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoPrincipal.cveCie10", codigoCie);
            }
            if (primeraVez != null && primeraVez < 2) {
                query.filter("sdEncabezado.sdFormato.ind1EraVez", primeraVez);
            }

            // logger.info(query.toString());
            listConsultaExterna = query.asList();
            for (SitConsultaExterna consul : listConsultaExterna) {
                listAtenciones.add(consul.getSdEncabezado().get(0));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listAtenciones;
    }

    public List<SitConsultaExterna> buscarConsultasExternas(List<String> listaEspecialidades,
            List<String> catalogoEpidemiologico,

            String unidad) {

        // SimoDatastore simoDatastore = new SimoDatastore();
        List<SitConsultaExterna> listConsultaExterna = new ArrayList<SitConsultaExterna>();
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SitConsultaExterna> query = dsEsp.createQuery(SitConsultaExterna.class);

            query.and(query.criteria("sdEncabezado.sdEspecialidad.cveEspecialidad").notIn(listaEspecialidades));
            query.and(query.criteria("sdEncabezado.sdFormato.numEstadoFormato").equal(ESTATUS_CONSULTA_NO_ENVIADA));
            query.and(query.criteria("sdUnidadMedica.cvePresupuestal").equal(unidad));
            query.or(
                    query.criteria("sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoPrincipal.cveCie10")
                            .in(catalogoEpidemiologico),
                    query.criteria("sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoAdicional.cveCie10")
                            .in(catalogoEpidemiologico),
                    query.criteria("sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoComplementario.cveCie10")
                            .in(catalogoEpidemiologico));

            return query.asList();

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }

        return listConsultaExterna;
    }

    public void actualizarConsultasExternas(List<String> listaEspecialidades, List<String> catalogoEpidemiologico,
            String unidad, List<SitConsultaExterna> listaConsultasExternas) {

        // SimoDatastore simoDatastore = new SimoDatastore();
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            DBObject especialidad = new BasicDBObject("sdEncabezado.sdEspecialidad.cveEspecialidad",
                    new BasicDBObject("$nin", listaEspecialidades));
            BasicDBObject diagPpal = new BasicDBObject(
                    "sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoPrincipal.cveCie10",
                    new BasicDBObject("$in", catalogoEpidemiologico));
            BasicDBObject diagAdicional = new BasicDBObject(
                    "sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoAdicional.cveCie10",
                    new BasicDBObject("$in", catalogoEpidemiologico));
            BasicDBObject diagComplementario = new BasicDBObject(
                    "sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoComplementario.cveCie10",
                    new BasicDBObject("$in", catalogoEpidemiologico));

            BasicDBList listaDiagnosticos = new BasicDBList();
            listaDiagnosticos.add(diagPpal);
            listaDiagnosticos.add(diagAdicional);
            listaDiagnosticos.add(diagComplementario);
            DBObject orDiagnosticos = new BasicDBObject("$or", listaDiagnosticos);

            DBObject condicionUnidad = new BasicDBObject("sdUnidadMedica.cvePresupuestal", unidad);

            BasicDBList listaCondiciones = new BasicDBList();
            listaCondiciones.add(especialidad);
            listaCondiciones.add(orDiagnosticos);
            listaCondiciones.add(condicionUnidad);
            DBObject andCondiciones = new BasicDBObject("$and", listaCondiciones);

            // Variables del ciclo
            int indice;
            DBObject update;
            Query<SitConsultaExterna> query;
            WriteResult resultado;
            boolean upsert = false;
            boolean multi = false;
            ObjectId idConsultaExterna;

            for (SitConsultaExterna sitConsultaExterna : listaConsultasExternas) {

                idConsultaExterna = sitConsultaExterna.getId();

                SitConsultaExterna consultaExterna = dsEsp.createQuery(SitConsultaExterna.class)
                        .filter("_id", idConsultaExterna).get();

                for (SdFormato formato : sitConsultaExterna.getSdEncabezado().get(0).getSdFormato()) {
                    indice = formato.getNumConsecutivo();

                    update = new BasicDBObject("$set",
                            new BasicDBObject("sdEncabezado.0.sdFormato." + (indice - 1) + ".numEstadoFormato",
                                    ESTATUS_CONSULTA_ENVIADA));

                    // update = new BasicDBObject();

                    query = dsEsp.createQuery(SitConsultaExterna.class);

                    resultado = query.getCollection().update(andCondiciones, update, upsert, multi);

                    // update.put("sdEncabezado.0.sdFormato." + (indice - 1) + ".numEstadoFormato",
                    // ESTATUS_CONSULTA_ENVIADA);

                    // logger.info("update exitoso: " + resultado.getN());
                    //
                    // logger.info("Formato: Consecutivo: " + formato.getNumConsecutivo());

                    // DBCollection collection = dsEsp.getCollection(SitConsultaExterna.class);
                    //
                    // collection.update(andCondiciones, update);
                }
            }

            // DBObject update = new BasicDBObject("$set", new
            // BasicDBObject("sdEncabezado.0.sdFormato.0.numEstadoFormato", ESTATUS_CONSULTA_ENVIADA));
            //
            // BasicDBObject update = new BasicDBObject();
            // update.put("sdEncabezado.0.sdFormato.0.numEstadoFormato", ESTATUS_CONSULTA_ENVIADA);
            //
            // DBCollection collection = dsEsp.getCollection(SitConsultaExterna.class);
            //
            // collection.update(andCondiciones, update);

            // Query<SitConsultaExterna> query = dsEsp.createQuery(SitConsultaExterna.class);
            // WriteResult resultado = query.getCollection().update(andCondiciones, update, true, true);
            // logger.info("update exitoso: " + resultado.getN());

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    public void actualizarAtenciones(List<SitConsultaExterna> listaConsultasExternas) {

        // SimoDatastore simoDatastore = new SimoDatastore();
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            int count = 0;
            for (SitConsultaExterna sitConsultaExterna : listaConsultasExternas) {

                Query<SitConsultaExterna> updateQuery = dsEsp.createQuery(SitConsultaExterna.class).field(Mapper.ID_KEY)
                        .equal(sitConsultaExterna.getId());

                UpdateOperations<SitConsultaExterna> update = dsEsp.createUpdateOperations(SitConsultaExterna.class)
                        .set("sdEncabezado", sitConsultaExterna.getSdEncabezado());

                dsEsp.update(updateQuery, update);
                count++;
            }
            // logger.info("se actualizaron " + count + " atenciones");
        } catch (Exception e) {
            logger.info("Fallo al actualizar las atencions: " + e.getMessage());
        }
    }

    public List<String> buscarUnidadesMedicasConConsultasExternas(List<String> listaEspecialidades,
            List<String> catalogoEpidemiologico) {

        // SimoDatastore simoDatastore = new SimoDatastore();
        List<String> listaUnidades = new ArrayList<String>();

        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            DBObject especialidad = new BasicDBObject("sdEncabezado.sdEspecialidad.cveEspecialidad",
                    new BasicDBObject("$nin", listaEspecialidades));
            BasicDBObject diagPpal = new BasicDBObject(
                    "sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoPrincipal.cveCie10",
                    new BasicDBObject("$in", catalogoEpidemiologico));
            BasicDBObject diagAdicional = new BasicDBObject(
                    "sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoAdicional.cveCie10",
                    new BasicDBObject("$in", catalogoEpidemiologico));
            BasicDBObject diagComplementario = new BasicDBObject(
                    "sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoComplementario.cveCie10",
                    new BasicDBObject("$in", catalogoEpidemiologico));

            BasicDBList listaDiagnosticos = new BasicDBList();
            listaDiagnosticos.add(diagPpal);
            listaDiagnosticos.add(diagAdicional);
            listaDiagnosticos.add(diagComplementario);
            DBObject orDiagnosticos = new BasicDBObject("$or", listaDiagnosticos);

            DBObject condicionEstatus = new BasicDBObject("sdEncabezado.sdFormato.numEstadoFormato",
                    ESTATUS_CONSULTA_NO_ENVIADA);

            BasicDBList listaCondiciones = new BasicDBList();
            listaCondiciones.add(especialidad);
            listaCondiciones.add(orDiagnosticos);
            listaCondiciones.add(condicionEstatus);
            DBObject andCondiciones = new BasicDBObject("$and", listaCondiciones);

            Query<SitConsultaExterna> query = dsEsp.createQuery(SitConsultaExterna.class);

            listaUnidades = query.getCollection().distinct("sdUnidadMedica.cvePresupuestal", andCondiciones);

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }

        return listaUnidades;
    }

    public List<SitConsultaExterna> buscarParametrosReporteCierreMes(Date fechaInicialPeriodoAnterior,
            Date fechaFinPeriodoAnterior, String clavePresupuestal) {

        // SimoDatastore simoDatastore = new SimoDatastore();
        List<SitConsultaExterna> lstparametrosReporte = new ArrayList<SitConsultaExterna>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaFinPeriodoAnterior);
        calendar.add(Calendar.DATE, 1);
        Date fehcaFinMasUnDia = calendar.getTime();
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
            Iterable<SitConsultaExterna> iterator = dsEsp.createQuery(SitConsultaExterna.class)
                    .filter("sdEncabezado.fecAltaEncabezado >=", fechaInicialPeriodoAnterior)
                    .filter("sdEncabezado.fecAltaEncabezado <", fehcaFinMasUnDia)
                    .filter("sdUnidadMedica.cvePresupuestal", clavePresupuestal).field("sdEncabezado.sdEspecialidad")
                    .exists().fetch();
            for (SitConsultaExterna sitConsultaExterna : iterator) {
                lstparametrosReporte.add(sitConsultaExterna);
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return lstparametrosReporte;
    }

    public List<String> buscarParametrosGenerarReporte(String fechaInicio, String fechaFin) {

        try {

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return null;
    }

    public String obtenerUrlConstante(String value) {

        String constante = "";
        // SimoDatastore simoDatastore = new SimoDatastore();
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();
            constante = ds.createQuery(SicConstantes.class).filter("cveConstante", value).get().getRefValor();
        } catch (Exception e) {
            constante = "No se encontró el valor deseado";
        }
        return constante;
    }

    public void actualizaConstantes(String bandera, String cveConstante) {

        // SimoDatastore simoDatastore = new SimoDatastore();
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();

            Query<SicConstantes> updateQuery = ds.createQuery(SicConstantes.class).field("cveConstante")
                    .equal(cveConstante);

            UpdateOperations<SicConstantes> ops = ds.createUpdateOperations(SicConstantes.class).set("refValor",
                    bandera);

            ds.update(updateQuery, ops);

        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public Boolean buscarAtencionRepetida(String cveNss, String cveAgMedico, String cveDxP, String idCE) {

        Boolean esRepetido = Boolean.TRUE;
        Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

        Query<SitConsultaExterna> q = dsEsp.createQuery(SitConsultaExterna.class);
        q.filter("_id", new ObjectId(idCE));
        AggregationPipeline aggr = dsEsp.createAggregation(SitConsultaExterna.class);
        aggr.match(q);
        aggr.unwind("sdEncabezado");
        aggr.unwind("sdEncabezado.sdFormato");

        if (cveDxP != null && !cveDxP.isEmpty()) {
            // Búsqueda de urgencias
            aggr.match(dsEsp.createQuery(SitConsultaExterna.class)
                    .filter("sdEncabezado.sdFormato.sdPaciente.numNss", cveNss.toString())
                    .filter("sdEncabezado.sdFormato.sdPaciente.refAgregadoMedico", cveAgMedico)
                    .filter("sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoPrincipal.cveCie10", cveDxP));
        } else {
            aggr.match(dsEsp.createQuery(SitConsultaExterna.class)
                    .filter("sdEncabezado.sdFormato.sdPaciente.numNss", cveNss.toString())
                    .filter("sdEncabezado.sdFormato.sdPaciente.refAgregadoMedico", cveAgMedico));
        }

        Iterator<SitConsultaExterna> aggregate = aggr.aggregate(SitConsultaExterna.class,
                AggregationOptions.builder().build());
        if (aggregate == null || !aggregate.hasNext()) {
            esRepetido = Boolean.FALSE;
        } else {
            logger.debug("----REGISTRO REPETIDO");
            esRepetido = Boolean.TRUE;
        }
        while (aggregate.hasNext()) {
            SitConsultaExterna sce = aggregate.next();
            for (SdCapturista sc : sce.getSdCapturista()) {
                logger.debug("-------SC:" + sc.getRefNombre());
            }
            for (SdEncabezado se : sce.getSdEncabezado()) {
                logger.debug("-------se:" + se.getSdFormato());
                for (SdFormato sf : se.getSdFormato()) {
                    logger.debug("-------sf:" + sf.getSdPaciente());
                    for (SdPaciente sp : sf.getSdPaciente()) {
                        logger.debug("-------spnss:" + sp.getNumNss());
                        logger.debug("-------spagmed:" + sp.getRefAgregadoMedico());
                    }
                }
            }
        }

        return esRepetido;
    }

    @Override
    public SitControlCapturaCe actualizarControlCapturaCE(SitControlCapturaCe controlCapturaCe) {

        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();

            Query<SitControlCapturaCe> updateQuery = ds.createQuery(SitControlCapturaCe.class)
                    .filter("sdUnidadMedica.cvePresupuestal", controlCapturaCe.getSdUnidadMedica().getCvePresupuestal())
                    .filter("cvePeriodo", controlCapturaCe.getCvePeriodo());

            UpdateOperations<SitControlCapturaCe> ops = ds.createUpdateOperations(SitControlCapturaCe.class)
                    .set("indControl", 1).set("fecActualizaControl", new Date())
                    .set("sdUnidadMedica.desUnidadMedica", controlCapturaCe.getSdUnidadMedica().getDesUnidadMedica());

            controlCapturaCe = ds.findAndModify(updateQuery, ops, false, true);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return controlCapturaCe;
    }

    @Override
    public SicPeriodosImss obtenerPeriodImssPorFecha(Date fecha) {

        SicPeriodosImss periodo = null;
        Iterator<SicPeriodosImss> sicperiodos = null;
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

            Query<SicPeriodosImss> q = dsEsp.createQuery(SicPeriodosImss.class);
            // MATCH
            q.criteria("fecInicial").lessThanOrEq(fecha);
            q.criteria("fecFinal").greaterThanOrEq(fecha);
            AggregationPipeline aggr = dsEsp.createAggregation(SicPeriodosImss.class);
            aggr.match(q);
            aggr.sort(Sort.descending("cvePeriodo"));
            aggr.limit(1);

            sicperiodos = aggr.aggregate(SicPeriodosImss.class, AggregationOptions.builder().build());
            if (sicperiodos.hasNext()) {
                periodo = sicperiodos.next();
            }
        } catch (Exception e) {
            e.getMessage();
        }

        return periodo;

    }

	@Override
	public List<SitConsultaExterna> buscarAtencionesParaActualiza(Date fecha, String matriculaMedico, String nss,
			String idEspecialidad, String agregadoMedico, String codigoCie, Integer primeraVez,
			String cvePresupuestal) {

        List<SitConsultaExterna> listConsultaExterna = null;
        try {
            Datastore ds = MongoManager.INSTANCE.getDatastore();

            String fechaAux = new SimpleDateFormat("dd/MM/yyyy").format(fecha);
            Date fechaFormato = new SimpleDateFormat("dd/MM/yyyy").parse(fechaAux);

            Query<SitConsultaExterna> query = ds.createQuery(SitConsultaExterna.class)
                    .filter("sdEncabezado.fecAltaEncabezado", fechaFormato);

            query.filter("sdUnidadMedica.cvePresupuestal", cvePresupuestal);

            if (matriculaMedico != null) {
                query.filter("sdEncabezado.sdMedico.cveMatricula", matriculaMedico);
            }
            if (nss != null) {
                query.filter("sdEncabezado.sdFormato.sdPaciente.numNss", nss);
            }
            if (idEspecialidad != null) {
                query.filter("sdEncabezado.sdEspecialidad.cveEspecialidad", idEspecialidad);
            }
            if (agregadoMedico != null) {
                query.filter("sdEncabezado.sdFormato.sdPaciente.refAgregadoMedico", agregadoMedico);
            }
            if (codigoCie != null) {
                query.filter("sdEncabezado.sdFormato.sdDiagnosticos.sdDiagnosticoPrincipal.cveCie10", codigoCie);
            }
            if (primeraVez != null && primeraVez < 2) {
                query.filter("sdEncabezado.sdFormato.ind1EraVez", primeraVez);
            }

            // logger.info(query.toString());
            listConsultaExterna = query.asList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listConsultaExterna;
	}

	@Override
	public Boolean actualizaNombreMedico(SitConsultaExterna sitConsultaExterna) {
		Boolean actualizado= Boolean.FALSE;
		
        try {
            Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
                Query<SitConsultaExterna> updateQuery = dsEsp.createQuery(SitConsultaExterna.class).field(Mapper.ID_KEY)
                        .equal(sitConsultaExterna.getId());

                UpdateOperations<SitConsultaExterna> update = dsEsp.createUpdateOperations(SitConsultaExterna.class)
                        .set("sdEncabezado", sitConsultaExterna.getSdEncabezado());

                
                UpdateResults controlMedicoRes=dsEsp.update(updateQuery, update); 
                if (controlMedicoRes.getUpdatedExisting() || controlMedicoRes.getInsertedCount() > 0) {
                    actualizado = Boolean.TRUE;
                }  

                return actualizado;       

        } catch (Exception e) {
            logger.info("Fallo al actualizar las atencions: " + e.getMessage());
            return actualizado;
        }
	}

}
