package mx.gob.imss.simo.consultaexterna.dao;

import java.util.Date;
import java.util.List;

import org.mongodb.morphia.query.UpdateResults;

import mx.gob.imss.simo.comun.entity.catalogos.SicCie10VigilanciaEpi;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;
import mx.gob.imss.simo.comun.entity.negocio.SitControlCapturaCe;
import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.model.PeriodoOperacion;

/**
 * @author francisco.rrios
 * 
 */
public interface ConsultaExternaDao {

	/**
	 * Insertar consultaExterna
	 * 
	 * @param encabezado
	 */
	void insertar(SitConsultaExterna consultaExterna);

	/**
	 * Agrega un Atencion
	 * 
	 * @param consutaExterna
	 * @return 
	 * 
	 */
	Boolean actualizar(SitConsultaExterna consutaExterna);

	/**
	 * Busca si existe un encabezado anterior y el número de Atencion
	 * 
	 * @param fecha
	 * @param matriculaMedico
	 * @param idEspecialidad
	 * @param idTurno
	 * @return
	 */
	SitConsultaExterna buscar(Date fecha, String matriculaMedico,
			String idEspecialidad, Integer idTurno);

	/**
	 * @param fecha
	 * @param matriculaMedico
	 * @param nss
	 * @param idEspecialidad
	 * @param agregadoMedico
	 * @param codigoCie
	 * @param primeraVez
	 * @return
	 */
	List<SdEncabezado> buscarAtenciones(Date fecha, String matriculaMedico,
			String nss, String idEspecialidad, String agregadoMedico,
			String codigoCie, Integer primeraVez, String cvePresupuestal);
	
	List<SitConsultaExterna> buscarConsultasExternas(List<String> listaEspecialidades, List<String> catalogoEpidemiologico, String unidad);
	List<String> buscarUnidadesMedicasConConsultasExternas(List<String> listaEspecialidades, List<String> catalogoEpidemiologico);
	void actualizarConsultasExternas(List<String> listaEspecialidades, List<String> catalogoEpidemiologico,
			String unidad, List<SitConsultaExterna> listaConsultasExternas);
	
	List<SitConsultaExterna> buscarParametrosReporteCierreMes(Date fechaInicialPeriodoAnterior, Date fechaFinPeriodoAnterior, String clavePresupuestal);
	List<String> buscarParametrosGenerarReporte(String fechaInicio, String fechaFin);
	void actualizaConstantes(String bandera, String cveConstante);
	String obtenerUrlConstante(String value);
	
	 void actualizarAtenciones(List<SitConsultaExterna> listaConsultasExternas);
	 /**
	  * Busca si una atención es repetida, de acuerdo a ciertos parametrios
	  * 
	  * @param cveNss
	  * @param cveAgMedico
	  * @param cveDxP
	  * @param idCE
	  * @return
	  */
	 Boolean buscarAtencionRepetida (String cveNss, String cveAgMedico, String cveDxP, String idCE);
	 
	 /**
	  * Se realiza la actualización o inserción de un registro en la coleccion SIT_CONTROL_CAPTURA_CE
	  * cada vez que alguien inserta un registro o lo actualiza
	  * @param controlCapturaCe
	  * @return 
	  */
	 SitControlCapturaCe actualizarControlCapturaCE(SitControlCapturaCe controlCapturaCe);
	 
	 /**
	  * Se obtiene un periodo IMSS de acuerdo a una fecha dada
	  * @param fechaEncabezado
	  * @return
	  */
	 SicPeriodosImss obtenerPeriodImssPorFecha(Date fechaEncabezado);
	 
	/* Boolean actualizaNombreMedico(String nombreMedico, String apPaterno, String apMaterno, Date fecha, String matriculaMedico, String nss, String idEspecialidad,
	            String agregadoMedico, String codigoCie, Integer primeraVez, String cvePresupuestal); */
	 
	 Boolean actualizaNombreMedico(SitConsultaExterna sitConsultaExterna); 
	 
		List<SitConsultaExterna> buscarAtencionesParaActualiza(Date fecha, String matriculaMedico,
				String nss, String idEspecialidad, String agregadoMedico,
				String codigoCie, Integer primeraVez, String cvePresupuestal);
	 
}
