package mx.gob.imss.simo.comun.service.impl;

import mx.gob.imss.simo.comun.dao.SitPersonalOperativoDao;
import mx.gob.imss.simo.comun.service.SitPersonalOperativoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("sitPersonalOperativoService")
public class SitPersonalOperativoServiceImpl implements
		SitPersonalOperativoService {

	@Autowired
	private SitPersonalOperativoDao personalOperativoDao;

	@Override
	public void updateIntento(String activeDirectoryAccount, int intento) {
		personalOperativoDao.updateIntento(activeDirectoryAccount, intento);
	}

	public void setPersonalOperativoDao(
			SitPersonalOperativoDao personalOperativoDao) {
		this.personalOperativoDao = personalOperativoDao;
	}

	@Override
	public int getIntento(String activeDirectoryAccount) {
		return personalOperativoDao.getIntento(activeDirectoryAccount);
	}

	@Override
	public void updateBloqueo(String activeDirectoryAccount, int bloqueo) {
		personalOperativoDao.updateBloqueo(activeDirectoryAccount, bloqueo);
	}

	@Override
	public int getBloqueo(String activeDirectoryAccount) {
		return personalOperativoDao.getBloqueo(activeDirectoryAccount);
	}

	@Override
	public void updateDesbloqueo(int indDesloqueo) {
		personalOperativoDao.updateDesbloqueo(indDesloqueo);
	}
	
}
