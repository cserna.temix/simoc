/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_TIPO_PARTO")
public class SicTipoParto {

	@Id
	private ObjectId id;
	private Integer cveTipoParto;
	private String desTipoParto;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTipoParto
	 */
	public Integer getCveTipoParto() {
		return cveTipoParto;
	}

	/**
	 * @param cveTipoParto
	 *            the cveTipoParto to set
	 */
	public void setCveTipoParto(Integer cveTipoParto) {
		this.cveTipoParto = cveTipoParto;
	}

	/**
	 * @return the desTipoParto
	 */
	public String getDesTipoParto() {
		return desTipoParto;
	}

	/**
	 * @param desTipoParto
	 *            the desTipoParto to set
	 */
	public void setDesTipoParto(String desTipoParto) {
		this.desTipoParto = desTipoParto;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
