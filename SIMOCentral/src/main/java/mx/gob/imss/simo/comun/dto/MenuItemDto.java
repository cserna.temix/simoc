package mx.gob.imss.simo.comun.dto;

import java.util.List;

public class MenuItemDto extends MenuDto {

	// De Administracion
	private List<MenuItemDto> sdUsuarios;
	private List<MenuItemDto> sdMonitoreo;

	// De Catalogos
	private List<MenuItemDto> sdUnidadesMedicas;

	// de sdHospitalizacion
	private List<MenuItemDto> sdIngreso4306;
	private List<MenuItemDto> sdEgresos;

	// De sdReportes
	private List<MenuItemDto> sdConsultaExterna;
	private List<MenuItemDto> sdPrincipalesMotivosConsulta;
	private List<MenuItemDto> sdCausaEgresos;
	private List<MenuItemDto> sdCausaEgresosHospitalarios;
	private List<MenuItemDto> sdInformeQuirofano;
	private List<MenuItemDto> sdInformeCortaEstancia;
	private List<MenuItemDto> sdInformeServicioUrgencias;

	private List<MenuItemDto> sdUtilerias;

	private List<MenuItemDto> sdBuscadores;

	public List<MenuItemDto> getSdMonitoreo() {
		return sdMonitoreo;
	}

	public void setSdMonitoreo(List<MenuItemDto> sdMonitoreo) {
		this.sdMonitoreo = sdMonitoreo;
	}

	public List<MenuItemDto> getSdIngreso4306() {
		return sdIngreso4306;
	}

	public void setSdIngreso4306(List<MenuItemDto> sdIngreso4306) {
		this.sdIngreso4306 = sdIngreso4306;
	}

	public List<MenuItemDto> getSdEgresos() {
		return sdEgresos;
	}

	public void setSdEgresos(List<MenuItemDto> sdEgresos) {
		this.sdEgresos = sdEgresos;
	}

	public List<MenuItemDto> getSdConsultaExterna() {
		return sdConsultaExterna;
	}

	public void setSdConsultaExterna(List<MenuItemDto> sdConsultaExterna) {
		this.sdConsultaExterna = sdConsultaExterna;
	}

	public List<MenuItemDto> getSdPrincipalesMotivosConsulta() {
		return sdPrincipalesMotivosConsulta;
	}

	public void setSdPrincipalesMotivosConsulta(
			List<MenuItemDto> sdPrincipalesMotivosConsulta) {
		this.sdPrincipalesMotivosConsulta = sdPrincipalesMotivosConsulta;
	}

	public List<MenuItemDto> getSdCausaEgresos() {
		return sdCausaEgresos;
	}

	public void setSdCausaEgresos(List<MenuItemDto> sdCausaEgresos) {
		this.sdCausaEgresos = sdCausaEgresos;
	}

	public List<MenuItemDto> getSdCausaEgresosHospitalarios() {
		return sdCausaEgresosHospitalarios;
	}

	public void setSdCausaEgresosHospitalarios(
			List<MenuItemDto> sdCausaEgresosHospitalarios) {
		this.sdCausaEgresosHospitalarios = sdCausaEgresosHospitalarios;
	}

	public List<MenuItemDto> getSdInformeQuirofano() {
		return sdInformeQuirofano;
	}

	public void setSdInformeQuirofano(List<MenuItemDto> sdInformeQuirofano) {
		this.sdInformeQuirofano = sdInformeQuirofano;
	}

	public List<MenuItemDto> getSdInformeCortaEstancia() {
		return sdInformeCortaEstancia;
	}

	public void setSdInformeCortaEstancia(
			List<MenuItemDto> sdInformeCortaEstancia) {
		this.sdInformeCortaEstancia = sdInformeCortaEstancia;
	}

	public List<MenuItemDto> getSdInformeServicioUrgencias() {
		return sdInformeServicioUrgencias;
	}

	public void setSdInformeServicioUrgencias(
			List<MenuItemDto> sdInformeServicioUrgencias) {
		this.sdInformeServicioUrgencias = sdInformeServicioUrgencias;
	}

	public List<MenuItemDto> getSdUtilerias() {
		return sdUtilerias;
	}

	public void setSdUtilerias(List<MenuItemDto> sdUtilerias) {
		this.sdUtilerias = sdUtilerias;
	}

	public List<MenuItemDto> getSdBuscadores() {
		return sdBuscadores;
	}

	public void setSdBuscadores(List<MenuItemDto> sdBuscadores) {
		this.sdBuscadores = sdBuscadores;
	}

	public List<MenuItemDto> getSdUsuarios() {
		return sdUsuarios;
	}

	public void setSdUsuarios(List<MenuItemDto> sdUsuarios) {
		this.sdUsuarios = sdUsuarios;
	}

	public List<MenuItemDto> getSdUnidadesMedicas() {
		return sdUnidadesMedicas;
	}

	public void setSdUnidadesMedicas(List<MenuItemDto> sdUnidadesMedicas) {
		this.sdUnidadesMedicas = sdUnidadesMedicas;
	}

	@Override
	public String toString() {
		return "MenuItemDto [sdUsuarios=" + sdUsuarios + ", sdMonitoreo="
				+ sdMonitoreo + ", sdUnidadesMedicas=" + sdUnidadesMedicas
				+ ", sdIngreso4306=" + sdIngreso4306 + ", sdEgresos="
				+ sdEgresos + ", sdConsultaExterna=" + sdConsultaExterna
				+ ", sdPrincipalesMotivosConsulta="
				+ sdPrincipalesMotivosConsulta + ", sdCausaEgresos="
				+ sdCausaEgresos + ", sdCausaEgresosHospitalarios="
				+ sdCausaEgresosHospitalarios + ", sdInformeQuirofano="
				+ sdInformeQuirofano + ", sdInformeCortaEstancia="
				+ sdInformeCortaEstancia + ", sdInformeServicioUrgencias="
				+ sdInformeServicioUrgencias + ", sdUtilerias=" + sdUtilerias
				+ ", sdBuscadores=" + sdBuscadores + "]";
	}
}
