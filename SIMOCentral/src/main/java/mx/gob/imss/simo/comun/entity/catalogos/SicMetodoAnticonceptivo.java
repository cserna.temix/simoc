/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_METODO_ANTICONCEPTIVO")
public class SicMetodoAnticonceptivo {

	@Id
	private ObjectId id;
	private Integer cveMetodoAnticonceptivo;
	private String desMetodoAnticonceptivo;
	private Integer cveSexo;
	private Integer canMax1eraVez;
	private Integer canMaxSubsecuente;
	private Integer numEdadInicial;
	private Integer numEdadFinal;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveMetodoAnticonceptivo
	 */
	public Integer getCveMetodoAnticonceptivo() {
		return cveMetodoAnticonceptivo;
	}

	/**
	 * @param cveMetodoAnticonceptivo
	 *            the cveMetodoAnticonceptivo to set
	 */
	public void setCveMetodoAnticonceptivo(Integer cveMetodoAnticonceptivo) {
		this.cveMetodoAnticonceptivo = cveMetodoAnticonceptivo;
	}

	/**
	 * @return the desMetodoAnticonceptivo
	 */
	public String getDesMetodoAnticonceptivo() {
		return desMetodoAnticonceptivo;
	}

	/**
	 * @param desMetodoAnticonceptivo
	 *            the desMetodoAnticonceptivo to set
	 */
	public void setDesMetodoAnticonceptivo(String desMetodoAnticonceptivo) {
		this.desMetodoAnticonceptivo = desMetodoAnticonceptivo;
	}

	/**
	 * @return the cveSexo
	 */
	public Integer getCveSexo() {
		return cveSexo;
	}

	/**
	 * @param cveSexo
	 *            the cveSexo to set
	 */
	public void setCveSexo(Integer cveSexo) {
		this.cveSexo = cveSexo;
	}

	/**
	 * @return the canMax1eraVez
	 */
	public Integer getCanMax1eraVez() {
		return canMax1eraVez;
	}

	/**
	 * @param canMax1eraVez
	 *            the canMax1eraVez to set
	 */
	public void setCanMax1eraVez(Integer canMax1eraVez) {
		this.canMax1eraVez = canMax1eraVez;
	}

	/**
	 * @return the canMaxSubsecuente
	 */
	public Integer getCanMaxSubsecuente() {
		return canMaxSubsecuente;
	}

	/**
	 * @param canMaxSubsecuente
	 *            the canMaxSubsecuente to set
	 */
	public void setCanMaxSubsecuente(Integer canMaxSubsecuente) {
		this.canMaxSubsecuente = canMaxSubsecuente;
	}

	/**
	 * @return the numEdadInicial
	 */
	public Integer getNumEdadInicial() {
		return numEdadInicial;
	}

	/**
	 * @param numEdadInicial
	 *            the numEdadInicial to set
	 */
	public void setNumEdadInicial(Integer numEdadInicial) {
		this.numEdadInicial = numEdadInicial;
	}

	/**
	 * @return the numEdadFinal
	 */
	public Integer getNumEdadFinal() {
		return numEdadFinal;
	}

	/**
	 * @param numEdadFinal
	 *            the numEdadFinal to set
	 */
	public void setNumEdadFinal(Integer numEdadFinal) {
		this.numEdadFinal = numEdadFinal;
	}

}
