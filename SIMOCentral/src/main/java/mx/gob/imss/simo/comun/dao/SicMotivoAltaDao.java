/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicMotivoAlta;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicMotivoAltaDao {

	/**
	 * @return
	 */
	List<SicMotivoAlta> burcarTodos();

	/**
	 * @param cveMotivoAlta
	 * @return
	 */
	List<SicMotivoAlta> burcarPorId(Integer cveMotivoAlta);
}
