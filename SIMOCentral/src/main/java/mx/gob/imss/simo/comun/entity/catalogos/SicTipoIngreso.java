/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Frank
 * 
 */
@Entity("SIC_TIPO_INGRESO")
public class SicTipoIngreso {

	@Id
	private ObjectId id;
	private Integer cveTipoIngreso;
	private String desTipoIngreso;
	private String desLugarAtencion;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTipoIngreso
	 */
	public Integer getCveTipoIngreso() {
		return cveTipoIngreso;
	}

	/**
	 * @param cveTipoIngreso
	 *            the cveTipoIngreso to set
	 */
	public void setCveTipoIngreso(Integer cveTipoIngreso) {
		this.cveTipoIngreso = cveTipoIngreso;
	}

	/**
	 * @return the desTipoIngreso
	 */
	public String getDesTipoIngreso() {
		return desTipoIngreso;
	}

	/**
	 * @param desTipoIngreso
	 *            the desTipoIngreso to set
	 */
	public void setDesTipoIngreso(String desTipoIngreso) {
		this.desTipoIngreso = desTipoIngreso;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the desLugarAtencion
	 */
	public String getDesLugarAtencion() {
		return desLugarAtencion;
	}

	/**
	 * @param desLugarAtencion the desLugarAtencion to set
	 */
	public void setDesLugarAtencion(String desLugarAtencion) {
		this.desLugarAtencion = desLugarAtencion;
	}

}
