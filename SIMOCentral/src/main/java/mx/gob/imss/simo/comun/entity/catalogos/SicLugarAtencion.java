/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_LUGAR_ATENCION")
public class SicLugarAtencion {

	@Id
	private ObjectId id;
	private Integer cveLugarAtencion;
	private String desLugarAtencion;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveLugarAtencion
	 */
	public Integer getCveLugarAtencion() {
		return cveLugarAtencion;
	}

	/**
	 * @param cveLugarAtencion
	 *            the cveLugarAtencion to set
	 */
	public void setCveLugarAtencion(Integer cveLugarAtencion) {
		this.cveLugarAtencion = cveLugarAtencion;
	}

	/**
	 * @return the desLugarAtencion
	 */
	public String getDesLugarAtencion() {
		return desLugarAtencion;
	}

	/**
	 * @param desLugarAtencion
	 *            the desLugarAtencion to set
	 */
	public void setDesLugarAtencion(String desLugarAtencion) {
		this.desLugarAtencion = desLugarAtencion;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
