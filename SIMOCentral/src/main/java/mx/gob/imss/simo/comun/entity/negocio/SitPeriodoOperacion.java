package mx.gob.imss.simo.comun.entity.negocio;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import mx.gob.imss.simo.consultaexterna.pojo.SdTipoCaptura;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;

@Entity("SIT_PERIODO_OPERACION")
public class SitPeriodoOperacion {
	
	@Id
	private ObjectId id;
	private Integer cvePeriodo;
	private List<SdTipoCaptura> sdTipoCaptura;
	private List<SdUnidadMedica> sdUnidadMedica;
	private Integer indCierre;
	private Date fecCierre;
	
	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cvePeriodo
	 */
	public Integer getCvePeriodo() {
		return cvePeriodo;
	}

	/**
	 * @param cvePeriodo the cvePeriodo to set
	 */
	public void setCvePeriodo(Integer cvePeriodo) {
		this.cvePeriodo = cvePeriodo;
	}

	/**
	 * @return the sdTipoCaptura
	 */
	public List<SdTipoCaptura> getSdTipoCaptura() {
		return sdTipoCaptura;
	}

	/**
	 * @param sdTipoCaptura the sdTipoCaptura to set
	 */
	public void setSdTipoCaptura(List<SdTipoCaptura> sdTipoCaptura) {
		this.sdTipoCaptura = sdTipoCaptura;
	}

	/**
	 * @return the sdUnidadMedica
	 */
	public List<SdUnidadMedica> getSdUnidadMedica() {
		return sdUnidadMedica;
	}

	/**
	 * @param sdUnidadMedica the sdUnidadMedica to set
	 */
	public void setSdUnidadMedica(List<SdUnidadMedica> sdUnidadMedica) {
		this.sdUnidadMedica = sdUnidadMedica;
	}

	/**
	 * @return the indCierre
	 */
	public Integer getIndCierre() {
		return indCierre;
	}

	/**
	 * @param indCierre the indCierre to set
	 */
	public void setIndCierre(Integer indCierre) {
		this.indCierre = indCierre;
	}

	/**
	 * @return the fecCierre
	 */
	public Date getFecCierre() {
		return fecCierre;
	}

	/**
	 * @param fecCierre the fecCierre to set
	 */
	public void setFecCierre(Date fecCierre) {
		this.fecCierre = fecCierre;
	}

}
