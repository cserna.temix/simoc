/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_PROCEDENCIA")
public class SicProcedencia {

	@Id
	private ObjectId id;
	private Integer cveProcedencia;
	private String desProcedencia;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveProcedencia
	 */
	public Integer getCveProcedencia() {
		return cveProcedencia;
	}

	/**
	 * @param cveProcedencia
	 *            the cveProcedencia to set
	 */
	public void setCveProcedencia(Integer cveProcedencia) {
		this.cveProcedencia = cveProcedencia;
	}

	/**
	 * @return the desProcedencia
	 */
	public String getDesProcedencia() {
		return desProcedencia;
	}

	/**
	 * @param desProcedencia
	 *            the desProcedencia to set
	 */
	public void setDesProcedencia(String desProcedencia) {
		this.desProcedencia = desProcedencia;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}