/**
 * 
 */
package mx.gob.imss.simo.comun.core;

import java.util.List;

import mx.gob.imss.simo.comun.enums.EnumEspecialidades;
/**
 * Clase encargada de Realizar las Reglas de negocio
 * 
 * @author francisco.rrios
 * 
 */
public class ReglasNegocio {
	
	//Turno 
	private static final String  TURNO_NOCTURNO = "3";
	/**
	 * Regla: RN04 <Servicios paramédicos>
	 * 
	 * @param servicio
	 * @return
	 */
	public static boolean validaSerParamedicos(String servicio) {
		boolean valida = true;
		if (servicio != "62" && servicio != "63" && servicio != "64"
				&& servicio != "66" && servicio != "69" && servicio != "70"
				&& servicio != "98") {
			valida = false;

		}
		return valida;
	}

	/**
	 * Regla: RN10 <Conformación de Matrícula, del prestador de servicio>
	 * 
	 * @param matricula
	 * @return
	 */
	public static boolean validaMatricula(String matricula) {
		boolean valida = true;
		Integer validaro;

		try {			
			validaro = Integer.parseInt(matricula.substring(0, 2));
			while (matricula.length() < 10) {
				String cadAux = "0";
				cadAux = cadAux.concat(matricula);
				matricula = cadAux;
			}
			if (validaro == 99) {
				Integer delegacion = Integer
						.parseInt(matricula.substring(6, 8));
				if (delegacion > 38 || delegacion < 1) {
					valida = false;
				}

			} else {
				Integer digiVerificador = Integer.parseInt(matricula
						.substring(9));

				Integer pos2 = Integer.parseInt(matricula.substring(8, 9));
				Integer pos3 = Integer.parseInt(matricula.substring(7, 8));
				Integer pos4 = Integer.parseInt(matricula.substring(6, 7));
				Integer pos5 = Integer.parseInt(matricula.substring(5, 6));
				Integer pos6 = Integer.parseInt(matricula.substring(4, 5));
				Integer pos7 = Integer.parseInt(matricula.substring(3, 4));
				Integer pos8 = Integer.parseInt(matricula.substring(2, 3));

				Integer resultado = (pos2 * 2) + (pos3 * 3) + (pos4 * 4)
						+ (pos5 * 5) + (pos6 * 6) + (pos7 * 7) + (pos8 * 8);

				Integer modulo = resultado % 11;

				if (digiVerificador != modulo) {
					valida = false;
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return valida;
	}

	/**
	 * Regla: RN12 <Horas trabajadas>
	 * 
	 * @param horasTrab
	 * @return
	 */
	public static boolean validaHorasTrabajadas(Integer numTipoServicio, String horasTrab, String turno) {
		boolean valida = false;
		Integer horas = Integer.parseInt(horasTrab.substring(0, 2));
		Integer minutos = Integer.parseInt(horasTrab.substring(3));
		
		
			
		
		if(numTipoServicio != null && turno != null  && horasTrab != null){
			if (numTipoServicio == 4  || (turno.equals(TURNO_NOCTURNO) && numTipoServicio != 5 )){
				if(horas >= 1 && horas<= 11 && minutos <= 59){
					valida = true;
				}	
			    if(horas == 11 && minutos > 00){
			    	valida = false;
			    }
			}else{
				if(numTipoServicio == 5 || numTipoServicio == 2  || numTipoServicio == 3 ){
					if(horas >= 1 && horas <= 6 && minutos <= 59){
						valida=true;
					}
				    if(horas == 6 && minutos > 30){
		               valida=false;
		            }
			     }
			
					
			
			}
			
		}
		return valida;
	}


	/**
	 * Regla: RN13 <Citas no cumplidas> y RN14 <Consulta no otorgadas>
	 * 
	 * @param ConsultasNoOtorgadas
	 * @return
	 */
	public static boolean validaConsulVsCitas(Integer consultasNoOtorgadas,
			Integer citasNoAtendidas) {
		boolean valida = true;

		if (consultasNoOtorgadas < citasNoAtendidas) {
			valida = false;
		}
		return valida;
	}

	/**
	 * Regla: RN20 <Clave CIE10>
	 * 
	 * @param diagnostico
	 * @param diagPermitidos
	 * @param tipo
	 * @return
	 */
	public static boolean validaCie10(String diagnostico,
			List<String> diagPermitidos, int posicion) {
		boolean valida = false;
		int i = 0;
		if(diagPermitidos.size() > 0){
			while (diagPermitidos.get(i) != null) {
				if (diagnostico == diagPermitidos.get(i)) {
					valida = true;
					posicion = i;
					break;
				}
				i++;
			}
		}
		return valida;
	}
	/*Regla: RN27 <Intervalo de edad para  Urgencias Toco - cirugía>*/
	/**
	 * Metodo que sirve para validar la edad de Urgencias Toco-cirugia
	 * @param especialidadSelc - el tipo de servicio
	 * @param numEdadMinima- edad minima de la especialidad
	 * @param numEdadMaxima- edad maxima de la especialidad
	 * @param edad- definida por el usuario
	 * @return booleano
	 */
	public static boolean validaEdadUrgenciasToco(String especialidadSelc, Integer numEdadMinima, Integer numEdadMaxima, int edad){
		boolean valida = false;
		
		
		if(edad < numEdadMinima  || edad > numEdadMaxima && (Integer.parseInt(especialidadSelc)== 
				EnumEspecialidades.URGENCIA_CIRUGIA.getClave())){			
				valida = true;									
					}
				
		return valida;
		
	}
	


}