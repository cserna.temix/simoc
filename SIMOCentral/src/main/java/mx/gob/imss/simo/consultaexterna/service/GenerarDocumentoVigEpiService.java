package mx.gob.imss.simo.consultaexterna.service;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;

public interface GenerarDocumentoVigEpiService {
	
	public List<SicEspecialidad> burcarcvEspecialidad();

}
