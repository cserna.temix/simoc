package mx.gob.imss.simo.consultaexterna.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.entity.catalogos.SicSemanaVigilaciaEpi;
import mx.gob.imss.simo.consultaexterna.dao.CatalogoSemanaVigilanciaEpiDao;

@Repository("catalogoSemanaVigilanciaEpiDao")
public class CatalogoSemanaVigilanciaEpiDaoImpl implements CatalogoSemanaVigilanciaEpiDao{
	
//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicSemanaVigilaciaEpi> burcarCatalogoSemanaVigiEpi() {
		// SimoDatastore simoDatastore = new SimoDatastore();
		List<SicSemanaVigilaciaEpi>listaCatalogoSemanaEpi = new ArrayList<SicSemanaVigilaciaEpi>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listaCatalogoSemanaEpi = dsEsp.createQuery(SicSemanaVigilaciaEpi.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
		return listaCatalogoSemanaEpi;
	}

}
