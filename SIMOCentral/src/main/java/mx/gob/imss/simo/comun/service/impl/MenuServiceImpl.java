package mx.gob.imss.simo.comun.service.impl;

import java.util.ArrayList;
import java.util.List;

import mx.gob.imss.simo.comun.dao.MenuDao;
import mx.gob.imss.simo.comun.dto.MenuDto;
import mx.gob.imss.simo.comun.dto.MenuItemDto;
import mx.gob.imss.simo.comun.service.MenuService;

import org.apache.log4j.Logger;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

	final static Logger logger = Logger.getLogger(MenuServiceImpl.class);

	@Autowired
	MenuDao menuDao;

	@Override
	public List<MenuDto> getMenu(String rol) {
		return menuDao.getMenu(rol);
	}

	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
	}

	public MenuModel createMenu(String perfil) {

		MenuModel model;

		List<MenuDto> menu = getMenu(perfil);

//		logger.info("Creando menu para el rol: " + perfil);
//
//		logger.info("~~~The MENU~~~ " + menu.toString());

		String nivel1 = "ui-icon-bullet";
		String salirIcon = "ui-icon-home";

		model = new DefaultMenuModel();

		DefaultSubMenu rootMenu = new DefaultSubMenu();
		List<MenuElement> menu0 = new ArrayList<MenuElement>();

		for (MenuDto men : menu) {
			//logger.info("Agregando menu: " + men.getDesMenuAplicativo());

			DefaultSubMenu submenu0 = new DefaultSubMenu(
					men.getDesMenuAplicativo(), nivel1);

			submenu0.setStyleClass("imagenMenuBar");
			submenu0.setStyleClass("menuContenedores");
			submenu0.setId("menu_p");

			if (men.getDesMenuAplicativo().equals("SALIR")) {
				DefaultMenuItem salir = new DefaultMenuItem(
						men.getDesMenuAplicativo(), salirIcon);
				salir.setCommand(men.getRefAccion());
				menu0.add(salir);
			} else {

				// Submenu Administracion
				if (men.getSdAdministracion() != null) {
					for (MenuItemDto adm : men.getSdAdministracion()) {
						DefaultSubMenu item = new DefaultSubMenu(
								adm.getDesMenuAplicativo());
						if (adm.getSdUsuarios() != null) {
							for (MenuItemDto admItem : adm.getSdUsuarios()) {
								DefaultMenuItem adminSubItem = new DefaultMenuItem(
										admItem.getDesMenuAplicativo());
								adminSubItem.setCommand(admItem.getRefAccion());
								item.addElement(adminSubItem);
							}
							submenu0.addElement(item);
						}

						if (adm.getSdMonitoreo() != null) {
							for (MenuItemDto admItem : adm.getSdMonitoreo()) {
								DefaultMenuItem subMoitoreo = new DefaultMenuItem(
										admItem.getDesMenuAplicativo());
								item.addElement(subMoitoreo);
								subMoitoreo.setCommand(admItem.getRefAccion());
							}
							submenu0.addElement(item);
						}
					}
				}

				// Submenu Catalogos
				if (men.getSdCatalogos() != null) {
					for (MenuItemDto cat : men.getSdCatalogos()) {
						DefaultSubMenu item = new DefaultSubMenu(
								cat.getDesMenuAplicativo());
						DefaultMenuItem subItem = new DefaultMenuItem(
								cat.getDesMenuAplicativo());
						if (cat.getSdUnidadesMedicas() != null) {
							for (MenuItemDto unidItem : cat
									.getSdUnidadesMedicas()) {
								DefaultMenuItem unidSubItem = new DefaultMenuItem(
										unidItem.getDesMenuAplicativo());
								unidSubItem.setCommand(unidItem.getRefAccion());
								item.addElement(unidSubItem);
							}
							submenu0.addElement(item);
						} else
							submenu0.addElement(subItem);
					}
				}

				// Submenu Consulta Externa
				if (men.getSdConsultaExterna() != null) {
					for (MenuItemDto cons : men.getSdConsultaExterna()) {
						DefaultMenuItem consItem = new DefaultMenuItem(
								cons.getDesMenuAplicativo());
						consItem.setCommand(cons.getRefAccion());
						submenu0.addElement(consItem);
					}
				}

				// Submenu Hospitalizcion
				if (men.getSdHospitalizacion() != null) {
					for (MenuItemDto hosp : men.getSdHospitalizacion()) {
						DefaultMenuItem hospItem = new DefaultMenuItem(
								hosp.getDesMenuAplicativo());
						hospItem.setCommand(hosp.getRefAccion());
						DefaultSubMenu hospSub = new DefaultSubMenu(
								hosp.getDesMenuAplicativo());
						DefaultSubMenu hospSubEgreso = new DefaultSubMenu(
								hosp.getDesMenuAplicativo());
						if (hosp.getSdIngreso4306() != null) {
							for (MenuItemDto hospSubItem : hosp
									.getSdIngreso4306()) {
								DefaultMenuItem hospSubItemIngreso = new DefaultMenuItem(
										hospSubItem.getDesMenuAplicativo());
								hospSubItemIngreso.setCommand(hospSubItem
										.getRefAccion());
								hospSub.addElement(hospSubItemIngreso);
							}
							submenu0.addElement(hospSub);
						} else

						if (hosp.getSdEgresos() != null) {
							for (MenuItemDto hospSubItem : hosp.getSdEgresos()) {
								DefaultMenuItem hospSubItemEgreso = new DefaultMenuItem(
										hospSubItem.getDesMenuAplicativo());
								hospSubItemEgreso.setCommand(hospSubItem
										.getRefAccion());
								hospSubEgreso.addElement(hospSubItemEgreso);
							}
							submenu0.addElement(hospSubEgreso);
						} else {
							submenu0.addElement(hospItem);
						}
					}
				}

				// Submenu Reportes
				if (men.getSdReportes() != null) {
					for (MenuItemDto rep : men.getSdReportes()) {
						DefaultSubMenu repSub = new DefaultSubMenu(
								rep.getDesMenuAplicativo());
						DefaultMenuItem repItem = new DefaultMenuItem(
								rep.getDesMenuAplicativo());
						repItem.setCommand(rep.getRefAccion());

						if (rep.getSdConsultaExterna() != null) {
							for (MenuItemDto consItem : rep
									.getSdConsultaExterna()) {
								DefaultSubMenu consExtSub = new DefaultSubMenu(
										consItem.getDesMenuAplicativo());
								DefaultMenuItem consExtItem = new DefaultMenuItem(
										consItem.getDesMenuAplicativo());
								consExtItem.setCommand(consItem.getRefAccion());
								if (consItem.getSdPrincipalesMotivosConsulta() != null) {

									for (MenuItemDto motivo : consItem
											.getSdPrincipalesMotivosConsulta()) {
										DefaultMenuItem motivoItem = new DefaultMenuItem(
												motivo.getDesMenuAplicativo());
										motivoItem.setCommand(motivo
												.getRefAccion());

										consExtSub.addElement(motivoItem);
									}
									repSub.addElement(consExtSub);
								} else {
									repSub.addElement(consExtItem);
								}
							}
							submenu0.addElement(repSub);
						} else if (rep.getSdHospitalizacion() != null) {
							for (MenuItemDto hospItem : rep
									.getSdHospitalizacion()) {
								DefaultSubMenu hospSubMenu = new DefaultSubMenu(
										hospItem.getDesMenuAplicativo());
								DefaultMenuItem hospSubItem = new DefaultMenuItem(
										hospItem.getDesMenuAplicativo());
								hospSubItem.setCommand(hospItem.getRefAccion());

								if (hospItem.getSdCausaEgresos() != null) {
									for (MenuItemDto egreso : hospItem
											.getSdCausaEgresos()) {
										DefaultSubMenu egresoSubMenu = new DefaultSubMenu(
												egreso.getDesMenuAplicativo());
										DefaultMenuItem egresoSubItem = new DefaultMenuItem(
												egreso.getDesMenuAplicativo());
										egresoSubItem.setCommand(egreso
												.getRefAccion());
										if (egreso
												.getSdCausaEgresosHospitalarios() != null) {
											for (MenuItemDto causaEgreso : egreso
													.getSdCausaEgresosHospitalarios()) {
												DefaultMenuItem causaEgresoItem = new DefaultMenuItem(
														causaEgreso
																.getDesMenuAplicativo());
												causaEgresoItem
														.setCommand(causaEgreso
																.getRefAccion());
												egresoSubMenu
														.addElement(causaEgresoItem);
											}
											hospSubMenu
													.addElement(egresoSubMenu);
										} else {
											hospSubMenu
													.addElement(egresoSubItem);
										}
									}
									repSub.addElement(hospSubMenu);
								} else if (hospItem.getSdInformeQuirofano() != null) {
									for (MenuItemDto informeQ : hospItem
											.getSdInformeQuirofano()) {
										DefaultMenuItem informeQItem = new DefaultMenuItem(
												informeQ.getDesMenuAplicativo());
										informeQItem.setCommand(informeQ
												.getRefAccion());
										hospSubMenu.addElement(informeQItem);
									}
									repSub.addElement(hospSubMenu);
								} else if (hospItem.getSdInformeCortaEstancia() != null) {
									for (MenuItemDto informeCortaEst : hospItem
											.getSdInformeCortaEstancia()) {
										DefaultMenuItem informeCortaEstItem = new DefaultMenuItem(
												informeCortaEst
														.getDesMenuAplicativo());
										informeCortaEstItem
												.setCommand(informeCortaEst
														.getRefAccion());
										hospSubMenu
												.addElement(informeCortaEstItem);
									}
									repSub.addElement(hospSubMenu);
								} else if (hospItem
										.getSdInformeServicioUrgencias() != null) {
									for (MenuItemDto informeServUrg : hospItem
											.getSdInformeServicioUrgencias()) {
										DefaultMenuItem informeServUrgItem = new DefaultMenuItem(
												informeServUrg
														.getDesMenuAplicativo());
										informeServUrgItem
												.setCommand(informeServUrg
														.getRefAccion());
										hospSubMenu
												.addElement(informeServUrgItem);
									}
									repSub.addElement(hospSubMenu);
								} else {
									repSub.addElement(hospSubItem);
								}
							}
							submenu0.addElement(repSub);
						} else {
							submenu0.addElement(repItem);
						}
					}
				}

				// Submenu Enlaces
				if (men.getSdEnlace() != null) {
					for (MenuItemDto enlace : men.getSdEnlace()) {
						DefaultMenuItem enlaceItem = new DefaultMenuItem(
								enlace.getDesMenuAplicativo());
						enlaceItem.setCommand(enlace.getRefAccion());
						submenu0.addElement(enlaceItem);
					}
				}

				// Submenu Utilerias
				if (men.getSdUtilerias() != null) {
					for (MenuItemDto utileria : men.getSdUtilerias()) {
						DefaultSubMenu utileriaSub = new DefaultSubMenu(
								utileria.getDesMenuAplicativo());
						if (utileria.getSdBuscadores() != null) {
							for (MenuItemDto utileriaItem : utileria
									.getSdBuscadores()) {
								DefaultMenuItem utilSubItem = new DefaultMenuItem(
										utileriaItem.getDesMenuAplicativo());
								utilSubItem.setCommand(utileriaItem
										.getRefAccion());
								utileriaSub.addElement(utilSubItem);
							}
						}
						submenu0.addElement(utileriaSub);
					}
				}

				// Submenu Interfaces
				if (men.getSdInterfaces() != null) {
					for (MenuItemDto interf : men.getSdInterfaces()) {
						DefaultMenuItem interfItem = new DefaultMenuItem(
								interf.getDesMenuAplicativo());
						interfItem.setCommand(interf.getRefAccion());
						submenu0.addElement(interfItem);
					}
				}
				menu0.add(submenu0);
			}
		}
		rootMenu.setElements(menu0);
		model.addElement(rootMenu);
		return model;
	}
}
