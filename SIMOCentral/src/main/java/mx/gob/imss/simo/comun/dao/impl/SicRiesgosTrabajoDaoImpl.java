/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicRiesgosTrabajoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicRiesgoTrabajo;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicRiesgosTrabajoDao")
public class SicRiesgosTrabajoDaoImpl implements SicRiesgosTrabajoDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicRiesgoTrabajo> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicRiesgoTrabajo> listRiesgosTrabajo = new ArrayList<SicRiesgoTrabajo>();
		try {

			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listRiesgosTrabajo = dsEsp.createQuery(SicRiesgoTrabajo.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listRiesgosTrabajo;
	}

	@Override
	public List<SicRiesgoTrabajo> burcarPorId(Integer cveRiesgoTrabajo) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicRiesgoTrabajo> listRiesgosTrabajo = new ArrayList<SicRiesgoTrabajo>();
		try {

			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listRiesgosTrabajo = dsEsp.createQuery(SicRiesgoTrabajo.class)
					.field("cveRiesgoTrabajo").equal(cveRiesgoTrabajo).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listRiesgosTrabajo;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
