/**
 * 
 */
package mx.gob.imss.simo.comun.enums;

/**
 * @author francisco.rrios
 *
 */
public enum EnumDiagMorfologiasTumor {

	DIA_TUM_1("C000"),
	DIA_TUM_2("C001"),
	DIA_TUM_3("C002"),
	DIA_TUM_4("C003"),
	DIA_TUM_5("C004"),
	DIA_TUM_6("C005"),
	DIA_TUM_7("C006"),
	DIA_TUM_8("C008"),
	DIA_TUM_9("C009"),
	DIA_TUM_10("C00X"),
	DIA_TUM_11("C01X"),
	DIA_TUM_12("C020"),
	DIA_TUM_13("C021"),
	DIA_TUM_14("C022"),
	DIA_TUM_15("C023"),
	DIA_TUM_16("C024"),
	DIA_TUM_17("C028"),
	DIA_TUM_18("C029"),
	DIA_TUM_19("C02X"),
	DIA_TUM_20("C030"),
	DIA_TUM_21("C031"),
	DIA_TUM_22("C039"),
	DIA_TUM_23("C03X"),
	DIA_TUM_24("C040"),
	DIA_TUM_25("C041"),
	DIA_TUM_26("C048"),
	DIA_TUM_27("C049"),
	DIA_TUM_28("C04X"),
	DIA_TUM_29("C050"),
	DIA_TUM_30("C051"),
	DIA_TUM_31("C052"),
	DIA_TUM_32("C058"),
	DIA_TUM_33("C059"),
	DIA_TUM_34("C05X"),
	DIA_TUM_35("C060"),
	DIA_TUM_36("C061"),
	DIA_TUM_37("C062"),
	DIA_TUM_38("C068"),
	DIA_TUM_39("C069"),
	DIA_TUM_40("C06X"),
	DIA_TUM_41("C07X"),
	DIA_TUM_42("C080"),
	DIA_TUM_43("C081"),
	DIA_TUM_44("C088"),
	DIA_TUM_45("C089"),
	DIA_TUM_46("C08X"),
	DIA_TUM_47("C090"),
	DIA_TUM_48("C091"),
	DIA_TUM_49("C098"),
	DIA_TUM_50("C099"),
	DIA_TUM_51("C09X"),
	DIA_TUM_52("C100"),
	DIA_TUM_53("C101"),
	DIA_TUM_54("C102"),
	DIA_TUM_55("C103"),
	DIA_TUM_56("C104"),
	DIA_TUM_57("C108"),
	DIA_TUM_58("C109"),
	DIA_TUM_59("C10X"),
	DIA_TUM_60("C110"),
	DIA_TUM_61("C111"),
	DIA_TUM_62("C112"),
	DIA_TUM_63("C113"),
	DIA_TUM_64("C118"),
	DIA_TUM_65("C119"),
	DIA_TUM_66("C11X"),
	DIA_TUM_67("C12X"),
	DIA_TUM_68("C130"),
	DIA_TUM_69("C131"),
	DIA_TUM_70("C132"),
	DIA_TUM_71("C138"),
	DIA_TUM_72("C139"),
	DIA_TUM_73("C13X"),
	DIA_TUM_74("C140"),
	DIA_TUM_75("C142"),
	DIA_TUM_76("C148"),
	DIA_TUM_77("C14X"),
	DIA_TUM_78("C150"),
	DIA_TUM_79("C151"),
	DIA_TUM_80("C152"),
	DIA_TUM_81("C153"),
	DIA_TUM_82("C154"),
	DIA_TUM_83("C155"),
	DIA_TUM_84("C158"),
	DIA_TUM_85("C159"),
	DIA_TUM_86("C15X"),
	DIA_TUM_87("C160"),
	DIA_TUM_88("C161"),
	DIA_TUM_89("C162"),
	DIA_TUM_90("C163"),
	DIA_TUM_91("C164"),
	DIA_TUM_92("C165"),
	DIA_TUM_93("C166"),
	DIA_TUM_94("C168"),
	DIA_TUM_95("C169"),
	DIA_TUM_96("C16X"),
	DIA_TUM_97("C170"),
	DIA_TUM_98("C171"),
	DIA_TUM_99("C172"),
	DIA_TUM_100("C173"),
	DIA_TUM_101("C178"),
	DIA_TUM_102("C179"),
	DIA_TUM_103("C17X"),
	DIA_TUM_104("C180"),
	DIA_TUM_105("C181"),
	DIA_TUM_106("C182"),
	DIA_TUM_107("C183"),
	DIA_TUM_108("C184"),
	DIA_TUM_109("C185"),
	DIA_TUM_110("C186"),
	DIA_TUM_111("C187"),
	DIA_TUM_112("C188"),
	DIA_TUM_113("C189"),
	DIA_TUM_114("C18X"),
	DIA_TUM_115("C19X"),
	DIA_TUM_116("C20X"),
	DIA_TUM_117("C210"),
	DIA_TUM_118("C211"),
	DIA_TUM_119("C212"),
	DIA_TUM_120("C218"),
	DIA_TUM_121("C21X"),
	DIA_TUM_122("C220"),
	DIA_TUM_123("C221"),
	DIA_TUM_124("C222"),
	DIA_TUM_125("C223"),
	DIA_TUM_126("C224"),
	DIA_TUM_127("C227"),
	DIA_TUM_128("C229"),
	DIA_TUM_129("C22X"),
	DIA_TUM_130("C23X"),
	DIA_TUM_131("C240"),
	DIA_TUM_132("C241"),
	DIA_TUM_133("C248"),
	DIA_TUM_134("C249"),
	DIA_TUM_135("C24X"),
	DIA_TUM_136("C250"),
	DIA_TUM_137("C251"),
	DIA_TUM_138("C252"),
	DIA_TUM_139("C253"),
	DIA_TUM_140("C254"),
	DIA_TUM_141("C257"),
	DIA_TUM_142("C258"),
	DIA_TUM_143("C259"),
	DIA_TUM_144("C25X"),
	DIA_TUM_145("C260"),
	DIA_TUM_146("C261"),
	DIA_TUM_147("C268"),
	DIA_TUM_148("C269"),
	DIA_TUM_149("C26X"),
	DIA_TUM_150("C300"),
	DIA_TUM_151("C301"),
	DIA_TUM_152("C30X"),
	DIA_TUM_153("C310"),
	DIA_TUM_154("C311"),
	DIA_TUM_155("C312"),
	DIA_TUM_156("C313"),
	DIA_TUM_157("C318"),
	DIA_TUM_158("C319"),
	DIA_TUM_159("C31X"),
	DIA_TUM_160("C320"),
	DIA_TUM_161("C321"),
	DIA_TUM_162("C322"),
	DIA_TUM_163("C323"),
	DIA_TUM_164("C328"),
	DIA_TUM_165("C329"),
	DIA_TUM_166("C32X"),
	DIA_TUM_167("C33X"),
	DIA_TUM_168("C340"),
	DIA_TUM_169("C341"),
	DIA_TUM_170("C342"),
	DIA_TUM_171("C343"),
	DIA_TUM_172("C348"),
	DIA_TUM_173("C349"),
	DIA_TUM_174("C34X"),
	DIA_TUM_175("C37X"),
	DIA_TUM_176("C380"),
	DIA_TUM_177("C381"),
	DIA_TUM_178("C382"),
	DIA_TUM_179("C383"),
	DIA_TUM_180("C384"),
	DIA_TUM_181("C388"),
	DIA_TUM_182("C38X"),
	DIA_TUM_183("C390"),
	DIA_TUM_184("C398"),
	DIA_TUM_185("C399"),
	DIA_TUM_186("C39X"),
	DIA_TUM_187("C400"),
	DIA_TUM_188("C401"),
	DIA_TUM_189("C402"),
	DIA_TUM_190("C403"),
	DIA_TUM_191("C408"),
	DIA_TUM_192("C409"),
	DIA_TUM_193("C40X"),
	DIA_TUM_194("C410"),
	DIA_TUM_195("C411"),
	DIA_TUM_196("C412"),
	DIA_TUM_197("C413"),
	DIA_TUM_198("C414"),
	DIA_TUM_199("C418"),
	DIA_TUM_200("C419"),
	DIA_TUM_201("C41X"),
	DIA_TUM_202("C430"),
	DIA_TUM_203("C431"),
	DIA_TUM_204("C432"),
	DIA_TUM_205("C433"),
	DIA_TUM_206("C434"),
	DIA_TUM_207("C435"),
	DIA_TUM_208("C436"),
	DIA_TUM_209("C437"),
	DIA_TUM_210("C438"),
	DIA_TUM_211("C439"),
	DIA_TUM_212("C43X"),
	DIA_TUM_213("C440"),
	DIA_TUM_214("C441"),
	DIA_TUM_215("C442"),
	DIA_TUM_216("C443"),
	DIA_TUM_217("C444"),
	DIA_TUM_218("C445"),
	DIA_TUM_219("C446"),
	DIA_TUM_220("C447"),
	DIA_TUM_221("C448"),
	DIA_TUM_222("C449"),
	DIA_TUM_223("C44X"),
	DIA_TUM_224("C450"),
	DIA_TUM_225("C451"),
	DIA_TUM_226("C452"),
	DIA_TUM_227("C457"),
	DIA_TUM_228("C459"),
	DIA_TUM_229("C45X"),
	DIA_TUM_230("C460"),
	DIA_TUM_231("C461"),
	DIA_TUM_232("C462"),
	DIA_TUM_233("C463"),
	DIA_TUM_234("C467"),
	DIA_TUM_235("C468"),
	DIA_TUM_236("C469"),
	DIA_TUM_237("C46X"),
	DIA_TUM_238("C470"),
	DIA_TUM_239("C471"),
	DIA_TUM_240("C472"),
	DIA_TUM_241("C473"),
	DIA_TUM_242("C474"),
	DIA_TUM_243("C475"),
	DIA_TUM_244("C476"),
	DIA_TUM_245("C478"),
	DIA_TUM_246("C479"),
	DIA_TUM_247("C47X"),
	DIA_TUM_248("C480"),
	DIA_TUM_249("C481"),
	DIA_TUM_250("C482"),
	DIA_TUM_251("C488"),
	DIA_TUM_252("C48X"),
	DIA_TUM_253("C490"),
	DIA_TUM_254("C491"),
	DIA_TUM_255("C492"),
	DIA_TUM_256("C493"),
	DIA_TUM_257("C494"),
	DIA_TUM_258("C495"),
	DIA_TUM_259("C496"),
	DIA_TUM_260("C498"),
	DIA_TUM_261("C499"),
	DIA_TUM_262("C49X"),
	DIA_TUM_263("C500"),
	DIA_TUM_264("C501"),
	DIA_TUM_265("C502"),
	DIA_TUM_266("C503"),
	DIA_TUM_267("C504"),
	DIA_TUM_268("C505"),
	DIA_TUM_269("C506"),
	DIA_TUM_270("C508"),
	DIA_TUM_271("C509"),
	DIA_TUM_272("C50X"),
	DIA_TUM_273("C510"),
	DIA_TUM_274("C511"),
	DIA_TUM_275("C512"),
	DIA_TUM_276("C518"),
	DIA_TUM_277("C519"),
	DIA_TUM_278("C51X"),
	DIA_TUM_279("C52X"),
	DIA_TUM_280("C530"),
	DIA_TUM_281("C531"),
	DIA_TUM_282("C538"),
	DIA_TUM_283("C539"),
	DIA_TUM_284("C53X"),
	DIA_TUM_285("C540"),
	DIA_TUM_286("C541"),
	DIA_TUM_287("C542"),
	DIA_TUM_288("C543"),
	DIA_TUM_289("C548"),
	DIA_TUM_290("C549"),
	DIA_TUM_291("C54X"),
	DIA_TUM_292("C55X"),
	DIA_TUM_293("C56X"),
	DIA_TUM_294("C570"),
	DIA_TUM_295("C571"),
	DIA_TUM_296("C572"),
	DIA_TUM_297("C573"),
	DIA_TUM_298("C574"),
	DIA_TUM_299("C577"),
	DIA_TUM_300("C578"),
	DIA_TUM_301("C579"),
	DIA_TUM_302("C57X"),
	DIA_TUM_303("C58X"),
	DIA_TUM_304("C600"),
	DIA_TUM_305("C601"),
	DIA_TUM_306("C602"),
	DIA_TUM_307("C608"),
	DIA_TUM_308("C609"),
	DIA_TUM_309("C60X"),
	DIA_TUM_310("C61X"),
	DIA_TUM_311("C620"),
	DIA_TUM_312("C621"),
	DIA_TUM_313("C629"),
	DIA_TUM_314("C62X"),
	DIA_TUM_315("C630"),
	DIA_TUM_316("C631"),
	DIA_TUM_317("C632"),
	DIA_TUM_318("C637"),
	DIA_TUM_319("C638"),
	DIA_TUM_320("C639"),
	DIA_TUM_321("C63X"),
	DIA_TUM_322("C64X"),
	DIA_TUM_323("C65X"),
	DIA_TUM_324("C66X"),
	DIA_TUM_325("C670"),
	DIA_TUM_326("C671"),
	DIA_TUM_327("C672"),
	DIA_TUM_328("C673"),
	DIA_TUM_329("C674"),
	DIA_TUM_330("C675"),
	DIA_TUM_331("C676"),
	DIA_TUM_332("C677"),
	DIA_TUM_333("C678"),
	DIA_TUM_334("C679"),
	DIA_TUM_335("C67X"),
	DIA_TUM_336("C680"),
	DIA_TUM_337("C681"),
	DIA_TUM_338("C688"),
	DIA_TUM_339("C689"),
	DIA_TUM_340("C68X"),
	DIA_TUM_341("C690"),
	DIA_TUM_342("C691"),
	DIA_TUM_343("C692"),
	DIA_TUM_344("C693"),
	DIA_TUM_345("C694"),
	DIA_TUM_346("C695"),
	DIA_TUM_347("C696"),
	DIA_TUM_348("C698"),
	DIA_TUM_349("C699"),
	DIA_TUM_350("C69X"),
	DIA_TUM_351("C700"),
	DIA_TUM_352("C701"),
	DIA_TUM_353("C709"),
	DIA_TUM_354("C70X"),
	DIA_TUM_355("C710"),
	DIA_TUM_356("C711"),
	DIA_TUM_357("C712"),
	DIA_TUM_358("C713"),
	DIA_TUM_359("C714"),
	DIA_TUM_360("C715"),
	DIA_TUM_361("C716"),
	DIA_TUM_362("C717"),
	DIA_TUM_363("C718"),
	DIA_TUM_364("C719"),
	DIA_TUM_365("C71X"),
	DIA_TUM_366("C720"),
	DIA_TUM_367("C721"),
	DIA_TUM_368("C722"),
	DIA_TUM_369("C723"),
	DIA_TUM_370("C724"),
	DIA_TUM_371("C725"),
	DIA_TUM_372("C728"),
	DIA_TUM_373("C729"),
	DIA_TUM_374("C72X"),
	DIA_TUM_375("C73X"),
	DIA_TUM_376("C740"),
	DIA_TUM_377("C741"),
	DIA_TUM_378("C749"),
	DIA_TUM_379("C74X"),
	DIA_TUM_380("C750"),
	DIA_TUM_381("C751"),
	DIA_TUM_382("C752"),
	DIA_TUM_383("C753"),
	DIA_TUM_384("C754"),
	DIA_TUM_385("C755"),
	DIA_TUM_386("C758"),
	DIA_TUM_387("C759"),
	DIA_TUM_388("C75X"),
	DIA_TUM_389("C760"),
	DIA_TUM_390("C761"),
	DIA_TUM_391("C762"),
	DIA_TUM_392("C763"),
	DIA_TUM_393("C764"),
	DIA_TUM_394("C765"),
	DIA_TUM_395("C767"),
	DIA_TUM_396("C768"),
	DIA_TUM_397("C76X"),
	DIA_TUM_398("C770"),
	DIA_TUM_399("C771"),
	DIA_TUM_400("C772"),
	DIA_TUM_401("C773"),
	DIA_TUM_402("C774"),
	DIA_TUM_403("C775"),
	DIA_TUM_404("C778"),
	DIA_TUM_405("C779"),
	DIA_TUM_406("C77X"),
	DIA_TUM_407("C780"),
	DIA_TUM_408("C781"),
	DIA_TUM_409("C782"),
	DIA_TUM_410("C783"),
	DIA_TUM_411("C784"),
	DIA_TUM_412("C785"),
	DIA_TUM_413("C786"),
	DIA_TUM_414("C787"),
	DIA_TUM_415("C788"),
	DIA_TUM_416("C78X"),
	DIA_TUM_417("C790"),
	DIA_TUM_418("C791"),
	DIA_TUM_419("C792"),
	DIA_TUM_420("C793"),
	DIA_TUM_421("C794"),
	DIA_TUM_422("C795"),
	DIA_TUM_423("C796"),
	DIA_TUM_424("C797"),
	DIA_TUM_425("C798"),
	DIA_TUM_426("C799"),
	DIA_TUM_427("C79X"),
	DIA_TUM_428("C800"),
	DIA_TUM_429("C809"),
	DIA_TUM_430("C80X"),
	DIA_TUM_431("C80X"),
	DIA_TUM_432("C810"),
	DIA_TUM_433("C811"),
	DIA_TUM_434("C812"),
	DIA_TUM_435("C813"),
	DIA_TUM_436("C814"),
	DIA_TUM_437("C817"),
	DIA_TUM_438("C819"),
	DIA_TUM_439("C81X"),
	DIA_TUM_440("C820"),
	DIA_TUM_441("C821"),
	DIA_TUM_442("C822"),
	DIA_TUM_443("C823"),
	DIA_TUM_444("C824"),
	DIA_TUM_445("C825"),
	DIA_TUM_446("C826"),
	DIA_TUM_447("C827"),
	DIA_TUM_448("C829"),
	DIA_TUM_449("C82X"),
	DIA_TUM_450("C830"),
	DIA_TUM_451("C831"),
	DIA_TUM_452("C832"),
	DIA_TUM_453("C833"),
	DIA_TUM_454("C834"),
	DIA_TUM_455("C835"),
	DIA_TUM_456("C836"),
	DIA_TUM_457("C837"),
	DIA_TUM_458("C838"),
	DIA_TUM_459("C839"),
	DIA_TUM_460("C83X"),
	DIA_TUM_461("C840"),
	DIA_TUM_462("C841"),
	DIA_TUM_463("C842"),
	DIA_TUM_464("C843"),
	DIA_TUM_465("C844"),
	DIA_TUM_466("C845"),
	DIA_TUM_467("C846"),
	DIA_TUM_468("C847"),
	DIA_TUM_469("C848"),
	DIA_TUM_470("C849"),
	DIA_TUM_471("C84X"),
	DIA_TUM_472("C850"),
	DIA_TUM_473("C851"),
	DIA_TUM_474("C852"),
	DIA_TUM_475("C857"),
	DIA_TUM_476("C859"),
	DIA_TUM_477("C85X"),
	DIA_TUM_478("C860"),
	DIA_TUM_479("C861"),
	DIA_TUM_480("C862"),
	DIA_TUM_481("C863"),
	DIA_TUM_482("C864"),
	DIA_TUM_483("C865"),
	DIA_TUM_484("C866"),
	DIA_TUM_485("C86X"),
	DIA_TUM_486("C880"),
	DIA_TUM_487("C881"),
	DIA_TUM_488("C882"),
	DIA_TUM_489("C883"),
	DIA_TUM_490("C884"),
	DIA_TUM_491("C887"),
	DIA_TUM_492("C889"),
	DIA_TUM_493("C88X"),
	DIA_TUM_494("C900"),
	DIA_TUM_495("C901"),
	DIA_TUM_496("C902"),
	DIA_TUM_497("C903"),
	DIA_TUM_498("C90X"),
	DIA_TUM_499("C910"),
	DIA_TUM_500("C911"),
	DIA_TUM_501("C912"),
	DIA_TUM_502("C913"),
	DIA_TUM_503("C914"),
	DIA_TUM_504("C915"),
	DIA_TUM_505("C916"),
	DIA_TUM_506("C917"),
	DIA_TUM_507("C918"),
	DIA_TUM_508("C919"),
	DIA_TUM_509("C91X"),
	DIA_TUM_510("C920"),
	DIA_TUM_511("C921"),
	DIA_TUM_512("C922"),
	DIA_TUM_513("C923"),
	DIA_TUM_514("C924"),
	DIA_TUM_515("C925"),
	DIA_TUM_516("C926"),
	DIA_TUM_517("C927"),
	DIA_TUM_518("C928"),
	DIA_TUM_519("C929"),
	DIA_TUM_520("C92X"),
	DIA_TUM_521("C930"),
	DIA_TUM_522("C931"),
	DIA_TUM_523("C932"),
	DIA_TUM_524("C933"),
	DIA_TUM_525("C937"),
	DIA_TUM_526("C939"),
	DIA_TUM_527("C93X"),
	DIA_TUM_528("C940"),
	DIA_TUM_529("C941"),
	DIA_TUM_530("C942"),
	DIA_TUM_531("C943"),
	DIA_TUM_532("C944"),
	DIA_TUM_533("C945"),
	DIA_TUM_534("C946"),
	DIA_TUM_535("C947"),
	DIA_TUM_536("C94X"),
	DIA_TUM_537("C950"),
	DIA_TUM_538("C951"),
	DIA_TUM_539("C952"),
	DIA_TUM_540("C957"),
	DIA_TUM_541("C959"),
	DIA_TUM_542("C95X"),
	DIA_TUM_543("C960"),
	DIA_TUM_544("C961"),
	DIA_TUM_545("C962"),
	DIA_TUM_546("C963"),
	DIA_TUM_547("C964"),
	DIA_TUM_548("C965"),
	DIA_TUM_549("C966"),
	DIA_TUM_550("C967"),
	DIA_TUM_551("C968"),
	DIA_TUM_552("C969"),
	DIA_TUM_553("C96X"),
	DIA_TUM_554("C97X"),
	DIA_TUM_555("D000"),
	DIA_TUM_556("D001"),
	DIA_TUM_557("D002"),
	DIA_TUM_558("D00X"),
	DIA_TUM_559("D010"),
	DIA_TUM_560("D011"),
	DIA_TUM_561("D012"),
	DIA_TUM_562("D013"),
	DIA_TUM_563("D014"),
	DIA_TUM_564("D015"),
	DIA_TUM_565("D017"),
	DIA_TUM_566("D019"),
	DIA_TUM_567("D01X"),
	DIA_TUM_568("D020"),
	DIA_TUM_569("D021"),
	DIA_TUM_570("D022"),
	DIA_TUM_571("D023"),
	DIA_TUM_572("D024"),
	DIA_TUM_573("D02X"),
	DIA_TUM_574("D030"),
	DIA_TUM_575("D031"),
	DIA_TUM_576("D032"),
	DIA_TUM_577("D033"),
	DIA_TUM_578("D034"),
	DIA_TUM_579("D035"),
	DIA_TUM_580("D036"),
	DIA_TUM_581("D037"),
	DIA_TUM_582("D038"),
	DIA_TUM_583("D039"),
	DIA_TUM_584("D03X"),
	DIA_TUM_585("D040"),
	DIA_TUM_586("D041"),
	DIA_TUM_587("D042"),
	DIA_TUM_588("D043"),
	DIA_TUM_589("D044"),
	DIA_TUM_590("D045"),
	DIA_TUM_591("D046"),
	DIA_TUM_592("D047"),
	DIA_TUM_593("D048"),
	DIA_TUM_594("D049"),
	DIA_TUM_595("D04X"),
	DIA_TUM_596("D050"),
	DIA_TUM_597("D051"),
	DIA_TUM_598("D057"),
	DIA_TUM_599("D059"),
	DIA_TUM_600("D05X"),
	DIA_TUM_601("D060"),
	DIA_TUM_602("D061"),
	DIA_TUM_603("D067"),
	DIA_TUM_604("D069"),
	DIA_TUM_605("D06X"),
	DIA_TUM_606("D070"),
	DIA_TUM_607("D071"),
	DIA_TUM_608("D072"),
	DIA_TUM_609("D073"),
	DIA_TUM_610("D074"),
	DIA_TUM_611("D075"),
	DIA_TUM_612("D076"),
	DIA_TUM_613("D07X"),
	DIA_TUM_614("D090"),
	DIA_TUM_615("D091"),
	DIA_TUM_616("D092"),
	DIA_TUM_617("D093"),
	DIA_TUM_618("D097"),
	DIA_TUM_619("D099"),
	DIA_TUM_620("D09X"),
	DIA_TUM_621("D100"),
	DIA_TUM_622("D101"),
	DIA_TUM_623("D102"),
	DIA_TUM_624("D103"),
	DIA_TUM_625("D104"),
	DIA_TUM_626("D105"),
	DIA_TUM_627("D106"),
	DIA_TUM_628("D107"),
	DIA_TUM_629("D109"),
	DIA_TUM_630("D10X"),
	DIA_TUM_631("D110"),
	DIA_TUM_632("D117"),
	DIA_TUM_633("D119"),
	DIA_TUM_634("D11X"),
	DIA_TUM_635("D120"),
	DIA_TUM_636("D121"),
	DIA_TUM_637("D122"),
	DIA_TUM_638("D123"),
	DIA_TUM_639("D124"),
	DIA_TUM_640("D125"),
	DIA_TUM_641("D126"),
	DIA_TUM_642("D127"),
	DIA_TUM_643("D128"),
	DIA_TUM_644("D129"),
	DIA_TUM_645("D12X"),
	DIA_TUM_646("D130"),
	DIA_TUM_647("D131"),
	DIA_TUM_648("D132"),
	DIA_TUM_649("D133"),
	DIA_TUM_650("D134"),
	DIA_TUM_651("D135"),
	DIA_TUM_652("D136"),
	DIA_TUM_653("D137"),
	DIA_TUM_654("D139"),
	DIA_TUM_655("D13X"),
	DIA_TUM_656("D140"),
	DIA_TUM_657("D141"),
	DIA_TUM_658("D142"),
	DIA_TUM_659("D143"),
	DIA_TUM_660("D144"),
	DIA_TUM_661("D14X"),
	DIA_TUM_662("D150"),
	DIA_TUM_663("D151"),
	DIA_TUM_664("D152"),
	DIA_TUM_665("D157"),
	DIA_TUM_666("D159"),
	DIA_TUM_667("D15X"),
	DIA_TUM_668("D160"),
	DIA_TUM_669("D161"),
	DIA_TUM_670("D162"),
	DIA_TUM_671("D163"),
	DIA_TUM_672("D164"),
	DIA_TUM_673("D165"),
	DIA_TUM_674("D166"),
	DIA_TUM_675("D167"),
	DIA_TUM_676("D168"),
	DIA_TUM_677("D169"),
	DIA_TUM_678("D16X"),
	DIA_TUM_679("D170"),
	DIA_TUM_680("D171"),
	DIA_TUM_681("D172"),
	DIA_TUM_682("D173"),
	DIA_TUM_683("D174"),
	DIA_TUM_684("D175"),
	DIA_TUM_685("D176"),
	DIA_TUM_686("D177"),
	DIA_TUM_687("D179"),
	DIA_TUM_688("D17X"),
	DIA_TUM_689("D180"),
	DIA_TUM_690("D181"),
	DIA_TUM_691("D18X"),
	DIA_TUM_692("D190"),
	DIA_TUM_693("D191"),
	DIA_TUM_694("D197"),
	DIA_TUM_695("D199"),
	DIA_TUM_696("D19X"),
	DIA_TUM_697("D200"),
	DIA_TUM_698("D201"),
	DIA_TUM_699("D20X"),
	DIA_TUM_700("D210"),
	DIA_TUM_701("D211"),
	DIA_TUM_702("D212"),
	DIA_TUM_703("D213"),
	DIA_TUM_704("D214"),
	DIA_TUM_705("D215"),
	DIA_TUM_706("D216"),
	DIA_TUM_707("D219"),
	DIA_TUM_708("D21X"),
	DIA_TUM_709("D220"),
	DIA_TUM_710("D221"),
	DIA_TUM_711("D222"),
	DIA_TUM_712("D223"),
	DIA_TUM_713("D224"),
	DIA_TUM_714("D225"),
	DIA_TUM_715("D226"),
	DIA_TUM_716("D227"),
	DIA_TUM_717("D229"),
	DIA_TUM_718("D22X"),
	DIA_TUM_719("D230"),
	DIA_TUM_720("D231"),
	DIA_TUM_721("D232"),
	DIA_TUM_722("D233"),
	DIA_TUM_723("D234"),
	DIA_TUM_724("D235"),
	DIA_TUM_725("D236"),
	DIA_TUM_726("D237"),
	DIA_TUM_727("D239"),
	DIA_TUM_728("D23X"),
	DIA_TUM_729("D24X"),
	DIA_TUM_730("D250"),
	DIA_TUM_731("D251"),
	DIA_TUM_732("D252"),
	DIA_TUM_733("D259"),
	DIA_TUM_734("D25X"),
	DIA_TUM_735("D260"),
	DIA_TUM_736("D261"),
	DIA_TUM_737("D267"),
	DIA_TUM_738("D269"),
	DIA_TUM_739("D26X"),
	DIA_TUM_740("D27X"),
	DIA_TUM_741("D280"),
	DIA_TUM_742("D281"),
	DIA_TUM_743("D282"),
	DIA_TUM_744("D287"),
	DIA_TUM_745("D289"),
	DIA_TUM_746("D28X"),
	DIA_TUM_747("D290"),
	DIA_TUM_748("D291"),
	DIA_TUM_749("D292"),
	DIA_TUM_750("D293"),
	DIA_TUM_751("D294"),
	DIA_TUM_752("D297"),
	DIA_TUM_753("D299"),
	DIA_TUM_754("D29X"),
	DIA_TUM_755("D300"),
	DIA_TUM_756("D301"),
	DIA_TUM_757("D302"),
	DIA_TUM_758("D303"),
	DIA_TUM_759("D304"),
	DIA_TUM_760("D307"),
	DIA_TUM_761("D309"),
	DIA_TUM_762("D30X"),
	DIA_TUM_763("D310"),
	DIA_TUM_764("D311"),
	DIA_TUM_765("D312"),
	DIA_TUM_766("D313"),
	DIA_TUM_767("D314"),
	DIA_TUM_768("D315"),
	DIA_TUM_769("D316"),
	DIA_TUM_770("D319"),
	DIA_TUM_771("D31X"),
	DIA_TUM_772("D320"),
	DIA_TUM_773("D321"),
	DIA_TUM_774("D329"),
	DIA_TUM_775("D32X"),
	DIA_TUM_776("D330"),
	DIA_TUM_777("D331"),
	DIA_TUM_778("D332"),
	DIA_TUM_779("D333"),
	DIA_TUM_780("D334"),
	DIA_TUM_781("D337"),
	DIA_TUM_782("D339"),
	DIA_TUM_783("D33X"),
	DIA_TUM_784("D34X"),
	DIA_TUM_785("D350"),
	DIA_TUM_786("D351"),
	DIA_TUM_787("D352"),
	DIA_TUM_788("D353"),
	DIA_TUM_789("D354"),
	DIA_TUM_790("D355"),
	DIA_TUM_791("D356"),
	DIA_TUM_792("D357"),
	DIA_TUM_793("D358"),
	DIA_TUM_794("D359"),
	DIA_TUM_795("D35X"),
	DIA_TUM_796("D360"),
	DIA_TUM_797("D361"),
	DIA_TUM_798("D367"),
	DIA_TUM_799("D369"),
	DIA_TUM_800("D36X"),
	DIA_TUM_801("D370"),
	DIA_TUM_802("D371"),
	DIA_TUM_803("D372"),
	DIA_TUM_804("D373"),
	DIA_TUM_805("D374"),
	DIA_TUM_806("D375"),
	DIA_TUM_807("D376"),
	DIA_TUM_808("D377"),
	DIA_TUM_809("D379"),
	DIA_TUM_810("D37X"),
	DIA_TUM_811("D380"),
	DIA_TUM_812("D381"),
	DIA_TUM_813("D382"),
	DIA_TUM_814("D383"),
	DIA_TUM_815("D384"),
	DIA_TUM_816("D385"),
	DIA_TUM_817("D386"),
	DIA_TUM_818("D38X"),
	DIA_TUM_819("D390"),
	DIA_TUM_820("D391"),
	DIA_TUM_821("D392"),
	DIA_TUM_822("D397"),
	DIA_TUM_823("D399"),
	DIA_TUM_824("D39X"),
	DIA_TUM_825("D400"),
	DIA_TUM_826("D401"),
	DIA_TUM_827("D407"),
	DIA_TUM_828("D409"),
	DIA_TUM_829("D40X"),
	DIA_TUM_830("D410"),
	DIA_TUM_831("D411"),
	DIA_TUM_832("D412"),
	DIA_TUM_833("D413"),
	DIA_TUM_834("D414"),
	DIA_TUM_835("D417"),
	DIA_TUM_836("D419"),
	DIA_TUM_837("D41X"),
	DIA_TUM_838("D420"),
	DIA_TUM_839("D421"),
	DIA_TUM_840("D429"),
	DIA_TUM_841("D42X"),
	DIA_TUM_842("D430"),
	DIA_TUM_843("D431"),
	DIA_TUM_844("D432"),
	DIA_TUM_845("D433"),
	DIA_TUM_846("D434"),
	DIA_TUM_847("D437"),
	DIA_TUM_848("D439"),
	DIA_TUM_849("D43X"),
	DIA_TUM_850("D440"),
	DIA_TUM_851("D441"),
	DIA_TUM_852("D442"),
	DIA_TUM_853("D443"),
	DIA_TUM_854("D444"),
	DIA_TUM_855("D445"),
	DIA_TUM_856("D446"),
	DIA_TUM_857("D447"),
	DIA_TUM_858("D448"),
	DIA_TUM_859("D449"),
	DIA_TUM_860("D44X"),
	DIA_TUM_861("D45X"),
	DIA_TUM_862("D460"),
	DIA_TUM_863("D461"),
	DIA_TUM_864("D462"),
	DIA_TUM_865("D463"),
	DIA_TUM_866("D464"),
	DIA_TUM_867("D465"),
	DIA_TUM_868("D466"),
	DIA_TUM_869("D467"),
	DIA_TUM_870("D469"),
	DIA_TUM_871("D46X"),
	DIA_TUM_872("D470"),
	DIA_TUM_873("D471"),
	DIA_TUM_874("D472"),
	DIA_TUM_875("D473"),
	DIA_TUM_876("D474"),
	DIA_TUM_877("D475"),
	DIA_TUM_878("D477"),
	DIA_TUM_879("D479"),
	DIA_TUM_880("D47X"),
	DIA_TUM_881("D480"),
	DIA_TUM_882("D481"),
	DIA_TUM_883("D482"),
	DIA_TUM_884("D483"),
	DIA_TUM_885("D484"),
	DIA_TUM_886("D485"),
	DIA_TUM_887("D486"),
	DIA_TUM_888("D487"),
	DIA_TUM_889("D489"),
	DIA_TUM_890("D48X");

	private String codigo;

	EnumDiagMorfologiasTumor(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}	
}
