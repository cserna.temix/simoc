/**
 * 
 */
package mx.gob.imss.simo.comun.enums;

/**
 * @author janiel.mb
 *
 */
public enum EnumTipoInformacionAdicional {


	MORFOLOGIA(1, "MORFOLOGIA"),  
	EMBARAZO(2, "EMBARAZO"), 
	VIH(3, "VIH");
	
	
	private Integer valor;
	private String descripcion;

	private EnumTipoInformacionAdicional(Integer valor, String descripcion) {
		this.valor = valor;
		this.descripcion=descripcion;
	}

	public Integer getValor() {
		return valor;
	}
	public String getDescripcion() {
		return descripcion;
	}
}