package mx.gob.imss.simo.consultaexterna.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.comun.service.SitPersonalOperativoService;

@Service("desbloquearCuentasActiveServiceImpl")
public class DesbloquearCuentasActiveServiceImpl {

	//private static final Log logger = LogFactory.getLog(DesbloquearCuentasActiveServiceImpl.class.getName());

	@Autowired
	private SitPersonalOperativoService personalOperativoService;
	

	public void desbloqueoUsuarios() {
		//logger.info("DesbloquearCuentasActiveServiceImpl - actualizando el desbloqueo de usuarios");
		
		personalOperativoService.updateDesbloqueo(0);
	}

	public SitPersonalOperativoService getPersonalOperativoService() {
		return personalOperativoService;
	}


	public void setPersonalOperativoService(SitPersonalOperativoService personalOperativoService) {
		this.personalOperativoService = personalOperativoService;
	}

		
}