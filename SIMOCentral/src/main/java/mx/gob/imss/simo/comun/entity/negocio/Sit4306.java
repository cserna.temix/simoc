/**
 * 
 */
package mx.gob.imss.simo.comun.entity.negocio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicAccidentesLesiones;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicRiesgoTrabajo;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoPrestador;
import mx.gob.imss.simo.comun.entity.catalogos.SicTurno;
import mx.gob.imss.simo.consultaexterna.pojo.SdMetodoAnticonceptivo;
import mx.gob.imss.simo.consultaexterna.pojo.SdPaseServicio;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Frank
 * 
 */
@Entity("SIT_4306")
public class Sit4306 {

	@Id
	private ObjectId id;
	private Date fecha;
	private String matriculaMedico;
	private String consultorio;
	private String horasTrabajadas;
	private Integer consultasNoOtorgadas;
	private Integer citasNoCumplidas;
	private List<SicEspecialidad> especialidad = new ArrayList<SicEspecialidad>();
	private List<SicTipoPrestador> tipoPrestador = new ArrayList<SicTipoPrestador>();
	private List<SicTurno> turno = new ArrayList<SicTurno>();

	private List<Sit4306> formatos = new ArrayList<Sit4306>();

	private String nss;
	private String agregadoMedico;
	private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private String unidadAdscripcion;
	private String nombreUnidadMed;
	private String horaCita;

	private List<SdPaseServicio> pase = new ArrayList<SdPaseServicio>();
	private List<SdMetodoAnticonceptivo> metodoPpf = new ArrayList<SdMetodoAnticonceptivo>();
	private List<SicAccidentesLesiones> accidentesLesiones = new ArrayList<SicAccidentesLesiones>();
	private List<SicRiesgoTrabajo> riesgosTrabajo = new ArrayList<SicRiesgoTrabajo>();

	private String recetas;
	private Integer diasIncapacidad;
	private String diagnosticoPrincipal;
	private String desDiagPrin;
	private Boolean primeraVezDiagPrin;
	private String procedimientoPrincipal;
	private String desProcPrin;
	private String diagnosticoAdicional;
	private String desDiagAdic;
	private Boolean primeraVezDiagAdic;
	private String procedimientoAdicional;
	private String desProcAdic;
	private String diagnosticoComplemento;
	private String desDiagComp;
	private Boolean primeraVezDiagComp;
	private String procedimientoComplemento;
	private String desProcComp;

	public Sit4306() {
		super();
	}

	public Sit4306(Date fecha, String matriculaMedico, String consultorio,
			String horasTrabajadas, Integer consultasNoOtorgadas,
			Integer citasNoCumplidas) {
		super();
		this.fecha = fecha;
		this.matriculaMedico = matriculaMedico;
		this.consultorio = consultorio;
		this.horasTrabajadas = horasTrabajadas;
		this.consultasNoOtorgadas = consultasNoOtorgadas;
		this.citasNoCumplidas = citasNoCumplidas;
	}

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the matriculaMedico
	 */
	public String getMatriculaMedico() {
		return matriculaMedico;
	}

	/**
	 * @param matriculaMedico
	 *            the matriculaMedico to set
	 */
	public void setMatriculaMedico(String matriculaMedico) {
		this.matriculaMedico = matriculaMedico;
	}

	/**
	 * @return the consultorio
	 */
	public String getConsultorio() {
		return consultorio;
	}

	/**
	 * @param consultorio
	 *            the consultorio to set
	 */
	public void setConsultorio(String consultorio) {
		this.consultorio = consultorio;
	}

	/**
	 * @return the horasTrabajadas
	 */
	public String getHorasTrabajadas() {
		return horasTrabajadas;
	}

	/**
	 * @param horasTrabajadas
	 *            the horasTrabajadas to set
	 */
	public void setHorasTrabajadas(String horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	/**
	 * @return the consultasNoOtorgadas
	 */
	public Integer getConsultasNoOtorgadas() {
		return consultasNoOtorgadas;
	}

	/**
	 * @param consultasNoOtorgadas
	 *            the consultasNoOtorgadas to set
	 */
	public void setConsultasNoOtorgadas(Integer consultasNoOtorgadas) {
		this.consultasNoOtorgadas = consultasNoOtorgadas;
	}

	/**
	 * @return the citasNoCumplidas
	 */
	public Integer getCitasNoCumplidas() {
		return citasNoCumplidas;
	}

	/**
	 * @param citasNoCumplidas
	 *            the citasNoCumplidas to set
	 */
	public void setCitasNoCumplidas(Integer citasNoCumplidas) {
		this.citasNoCumplidas = citasNoCumplidas;
	}

	/**
	 * @return the especialidad
	 */
	public List<SicEspecialidad> getEspecialidad() {
		return especialidad;
	}

	/**
	 * @param especialidad
	 *            the especialidad to set
	 */
	public void setEspecialidad(List<SicEspecialidad> especialidad) {
		this.especialidad = especialidad;
	}

	/**
	 * @return the tipoPrestador
	 */
	public List<SicTipoPrestador> getTipoPrestador() {
		return tipoPrestador;
	}

	/**
	 * @param tipoPrestador
	 *            the tipoPrestador to set
	 */
	public void setTipoPrestador(List<SicTipoPrestador> tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}

	/**
	 * @return the turno
	 */
	public List<SicTurno> getTurno() {
		return turno;
	}

	/**
	 * @param turno
	 *            the turno to set
	 */
	public void setTurno(List<SicTurno> turno) {
		this.turno = turno;
	}

	/**
	 * @return the formatos
	 */
	public List<Sit4306> getFormatos() {
		return formatos;
	}

	/**
	 * @param formatos
	 *            the formatos to set
	 */
	public void setFormatos(List<Sit4306> formatos) {
		this.formatos = formatos;
	}

	/**
	 * @return the nss
	 */
	public String getNss() {
		return nss;
	}

	/**
	 * @param nss
	 *            the nss to set
	 */
	public void setNss(String nss) {
		this.nss = nss;
	}

	/**
	 * @return the agregadoMedico
	 */
	public String getAgregadoMedico() {
		return agregadoMedico;
	}

	/**
	 * @param agregadoMedico
	 *            the agregadoMedico to set
	 */
	public void setAgregadoMedico(String agregadoMedico) {
		this.agregadoMedico = agregadoMedico;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apePaterno
	 */
	public String getApePaterno() {
		return apePaterno;
	}

	/**
	 * @param apePaterno
	 *            the apePaterno to set
	 */
	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	/**
	 * @return the apeMaterno
	 */
	public String getApeMaterno() {
		return apeMaterno;
	}

	/**
	 * @param apeMaterno
	 *            the apeMaterno to set
	 */
	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	/**
	 * @return the unidadAdscripcion
	 */
	public String getUnidadAdscripcion() {
		return unidadAdscripcion;
	}

	/**
	 * @param unidadAdscripcion
	 *            the unidadAdscripcion to set
	 */
	public void setUnidadAdscripcion(String unidadAdscripcion) {
		this.unidadAdscripcion = unidadAdscripcion;
	}

	/**
	 * @return the nombreUnidadMed
	 */
	public String getNombreUnidadMed() {
		return nombreUnidadMed;
	}

	/**
	 * @param nombreUnidadMed
	 *            the nombreUnidadMed to set
	 */
	public void setNombreUnidadMed(String nombreUnidadMed) {
		this.nombreUnidadMed = nombreUnidadMed;
	}

	/**
	 * @return the horaCita
	 */
	public String getHoraCita() {
		return horaCita;
	}

	/**
	 * @param horaCita
	 *            the horaCita to set
	 */
	public void setHoraCita(String horaCita) {
		this.horaCita = horaCita;
	}

	

	/**
	 * @return the pase
	 */
	public List<SdPaseServicio> getPase() {
		return pase;
	}

	/**
	 * @param pase the pase to set
	 */
	public void setPase(List<SdPaseServicio> pase) {
		this.pase = pase;
	}

	/**
	 * @return the metodoPpf
	 */
	public List<SdMetodoAnticonceptivo> getMetodoPpf() {
		return metodoPpf;
	}

	/**
	 * @param metodoPpf
	 *            the metodoPpf to set
	 */
	public void setMetodoPpf(List<SdMetodoAnticonceptivo> metodoPpf) {
		this.metodoPpf = metodoPpf;
	}

	/**
	 * @return the accidentesLesiones
	 */
	public List<SicAccidentesLesiones> getAccidentesLesiones() {
		return accidentesLesiones;
	}

	/**
	 * @param accidentesLesiones
	 *            the accidentesLesiones to set
	 */
	public void setAccidentesLesiones(
			List<SicAccidentesLesiones> accidentesLesiones) {
		this.accidentesLesiones = accidentesLesiones;
	}

	/**
	 * @return the riesgosTrabajo
	 */
	public List<SicRiesgoTrabajo> getRiesgosTrabajo() {
		return riesgosTrabajo;
	}

	/**
	 * @param riesgosTrabajo
	 *            the riesgosTrabajo to set
	 */
	public void setRiesgosTrabajo(List<SicRiesgoTrabajo> riesgosTrabajo) {
		this.riesgosTrabajo = riesgosTrabajo;
	}

	

	/**
	 * @return the recetas
	 */
	public String getRecetas() {
		return recetas;
	}

	/**
	 * @param recetas
	 *            the recetas to set
	 */
	public void setRecetas(String recetas) {
		this.recetas = recetas;
	}

	/**
	 * @return the diasIncapacidad
	 */
	public Integer getDiasIncapacidad() {
		return diasIncapacidad;
	}

	/**
	 * @param diasIncapacidad
	 *            the diasIncapacidad to set
	 */
	public void setDiasIncapacidad(Integer diasIncapacidad) {
		this.diasIncapacidad = diasIncapacidad;
	}

	/**
	 * @return the diagnosticoPrincipal
	 */
	public String getDiagnosticoPrincipal() {
		return diagnosticoPrincipal;
	}

	/**
	 * @param diagnosticoPrincipal
	 *            the diagnosticoPrincipal to set
	 */
	public void setDiagnosticoPrincipal(String diagnosticoPrincipal) {
		this.diagnosticoPrincipal = diagnosticoPrincipal;
	}

	/**
	 * @return the desDiagPrin
	 */
	public String getDesDiagPrin() {
		return desDiagPrin;
	}

	/**
	 * @param desDiagPrin
	 *            the desDiagPrin to set
	 */
	public void setDesDiagPrin(String desDiagPrin) {
		this.desDiagPrin = desDiagPrin;
	}

	/**
	 * @return the primeraVezDiagPrin
	 */
	public Boolean getPrimeraVezDiagPrin() {
		return primeraVezDiagPrin;
	}

	/**
	 * @param primeraVezDiagPrin
	 *            the primeraVezDiagPrin to set
	 */
	public void setPrimeraVezDiagPrin(Boolean primeraVezDiagPrin) {
		this.primeraVezDiagPrin = primeraVezDiagPrin;
	}

	/**
	 * @return the procedimientoPrincipal
	 */
	public String getProcedimientoPrincipal() {
		return procedimientoPrincipal;
	}

	/**
	 * @param procedimientoPrincipal
	 *            the procedimientoPrincipal to set
	 */
	public void setProcedimientoPrincipal(String procedimientoPrincipal) {
		this.procedimientoPrincipal = procedimientoPrincipal;
	}

	/**
	 * @return the desProcPrin
	 */
	public String getDesProcPrin() {
		return desProcPrin;
	}

	/**
	 * @param desProcPrin
	 *            the desProcPrin to set
	 */
	public void setDesProcPrin(String desProcPrin) {
		this.desProcPrin = desProcPrin;
	}

	/**
	 * @return the diagnosticoAdicional
	 */
	public String getDiagnosticoAdicional() {
		return diagnosticoAdicional;
	}

	/**
	 * @param diagnosticoAdicional
	 *            the diagnosticoAdicional to set
	 */
	public void setDiagnosticoAdicional(String diagnosticoAdicional) {
		this.diagnosticoAdicional = diagnosticoAdicional;
	}

	/**
	 * @return the desDiagAdic
	 */
	public String getDesDiagAdic() {
		return desDiagAdic;
	}

	/**
	 * @param desDiagAdic
	 *            the desDiagAdic to set
	 */
	public void setDesDiagAdic(String desDiagAdic) {
		this.desDiagAdic = desDiagAdic;
	}

	/**
	 * @return the primeraVezDiagAdic
	 */
	public Boolean getPrimeraVezDiagAdic() {
		return primeraVezDiagAdic;
	}

	/**
	 * @param primeraVezDiagAdic
	 *            the primeraVezDiagAdic to set
	 */
	public void setPrimeraVezDiagAdic(Boolean primeraVezDiagAdic) {
		this.primeraVezDiagAdic = primeraVezDiagAdic;
	}

	/**
	 * @return the procedimientoAdicional
	 */
	public String getProcedimientoAdicional() {
		return procedimientoAdicional;
	}

	/**
	 * @param procedimientoAdicional
	 *            the procedimientoAdicional to set
	 */
	public void setProcedimientoAdicional(String procedimientoAdicional) {
		this.procedimientoAdicional = procedimientoAdicional;
	}

	/**
	 * @return the desProcAdic
	 */
	public String getDesProcAdic() {
		return desProcAdic;
	}

	/**
	 * @param desProcAdic
	 *            the desProcAdic to set
	 */
	public void setDesProcAdic(String desProcAdic) {
		this.desProcAdic = desProcAdic;
	}

	/**
	 * @return the diagnosticoComplemento
	 */
	public String getDiagnosticoComplemento() {
		return diagnosticoComplemento;
	}

	/**
	 * @param diagnosticoComplemento
	 *            the diagnosticoComplemento to set
	 */
	public void setDiagnosticoComplemento(String diagnosticoComplemento) {
		this.diagnosticoComplemento = diagnosticoComplemento;
	}

	/**
	 * @return the desDiagComp
	 */
	public String getDesDiagComp() {
		return desDiagComp;
	}

	/**
	 * @param desDiagComp
	 *            the desDiagComp to set
	 */
	public void setDesDiagComp(String desDiagComp) {
		this.desDiagComp = desDiagComp;
	}

	/**
	 * @return the primeraVezDiagComp
	 */
	public Boolean getPrimeraVezDiagComp() {
		return primeraVezDiagComp;
	}

	/**
	 * @param primeraVezDiagComp
	 *            the primeraVezDiagComp to set
	 */
	public void setPrimeraVezDiagComp(Boolean primeraVezDiagComp) {
		this.primeraVezDiagComp = primeraVezDiagComp;
	}

	/**
	 * @return the procedimientoComplemento
	 */
	public String getProcedimientoComplemento() {
		return procedimientoComplemento;
	}

	/**
	 * @param procedimientoComplemento
	 *            the procedimientoComplemento to set
	 */
	public void setProcedimientoComplemento(String procedimientoComplemento) {
		this.procedimientoComplemento = procedimientoComplemento;
	}

	/**
	 * @return the desProcComp
	 */
	public String getDesProcComp() {
		return desProcComp;
	}

	/**
	 * @param desProcComp
	 *            the desProcComp to set
	 */
	public void setDesProcComp(String desProcComp) {
		this.desProcComp = desProcComp;
	}

}
