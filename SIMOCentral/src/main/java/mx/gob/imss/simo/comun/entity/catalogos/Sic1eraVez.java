/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_1ERA_VEZ")
public class Sic1eraVez {
	
	@Id
	private ObjectId id;
	private Integer cve1eraVez;
	private String des1eraVez;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cve1eraVez
	 */
	public Integer getCve1eraVez() {
		return cve1eraVez;
	}

	/**
	 * @param cve1eraVez
	 *            the cve1eraVez to set
	 */
	public void setCve1eraVez(Integer cve1eraVez) {
		this.cve1eraVez = cve1eraVez;
	}

	/**
	 * @return the des1eraVez
	 */
	public String getDes1eraVez() {
		return des1eraVez;
	}

	/**
	 * @param des1eraVez
	 *            the des1eraVez to set
	 */
	public void setDes1eraVez(String des1eraVez) {
		this.des1eraVez = des1eraVez;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
