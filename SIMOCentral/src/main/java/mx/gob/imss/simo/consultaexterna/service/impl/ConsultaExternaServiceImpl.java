package mx.gob.imss.simo.consultaexterna.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.mongodb.morphia.query.UpdateResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.comun.core.AssemblerDtoEntity;
import mx.gob.imss.simo.comun.core.MapperEntityToDto;
import mx.gob.imss.simo.comun.dao.SicAccidentesLesionesDao;
import mx.gob.imss.simo.comun.dao.SicCie10Dao;
import mx.gob.imss.simo.comun.dao.SicCie10MorfologiaDao;
import mx.gob.imss.simo.comun.dao.SicCie9Dao;
import mx.gob.imss.simo.comun.dao.SicConsultorioDao;
import mx.gob.imss.simo.comun.dao.SicDelegacionImssDao;
import mx.gob.imss.simo.comun.dao.SicEspecialidadDao;
import mx.gob.imss.simo.comun.dao.SicEspecialidadUnidadDao;
import mx.gob.imss.simo.comun.dao.SicInformacionAdicionalDao;
import mx.gob.imss.simo.comun.dao.SicInformacionAdicionalEmbarazoDao;
import mx.gob.imss.simo.comun.dao.SicInformacionAdicionalVIHDao;
import mx.gob.imss.simo.comun.dao.SicLugarAtencionDao;
import mx.gob.imss.simo.comun.dao.SicMedicoDao;
import mx.gob.imss.simo.comun.dao.SicMetodoAnticonceptivoDao;
import mx.gob.imss.simo.comun.dao.SicPaseServicioDao;
import mx.gob.imss.simo.comun.dao.SicPeriodosImssDao;
import mx.gob.imss.simo.comun.dao.SicRiesgosTrabajoDao;
import mx.gob.imss.simo.comun.dao.SicTipoDiarreaDao;
import mx.gob.imss.simo.comun.dao.SicTipoPrestadorDao;
import mx.gob.imss.simo.comun.dao.SicTipoUrgenciaDao;
import mx.gob.imss.simo.comun.dao.SicTurnoDao;
import mx.gob.imss.simo.comun.dao.SicUnidadAdscripcionDao;
import mx.gob.imss.simo.comun.dao.SitPacienteDao;
import mx.gob.imss.simo.comun.dto.SicCie10DTO;
import mx.gob.imss.simo.comun.dto.SicCie9DTO;
import mx.gob.imss.simo.comun.entity.catalogos.SicAccidentesLesiones;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie10;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie10Morfologia;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie9;
import mx.gob.imss.simo.comun.entity.catalogos.SicConsultorio;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidadUnidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicionalEmbarazo;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicionalVIH;
import mx.gob.imss.simo.comun.entity.catalogos.SicMetodoAnticonceptivo;
import mx.gob.imss.simo.comun.entity.catalogos.SicPaseServicio;
import mx.gob.imss.simo.comun.entity.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.comun.entity.catalogos.SicRiesgoTrabajo;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoDiarrea;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoPrestador;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoUrgencia;
import mx.gob.imss.simo.comun.entity.catalogos.SicTurno;
import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;
import mx.gob.imss.simo.comun.entity.negocio.SitControlCapturaCe;
import mx.gob.imss.simo.comun.entity.negocio.SitControlMedicoEncabezado;
import mx.gob.imss.simo.comun.entity.negocio.SitPeriodoOperacion;
import mx.gob.imss.simo.comun.enums.EnumRoles;
import mx.gob.imss.simo.comun.enums.EnumTipoInformacionAdicional;
import mx.gob.imss.simo.consultaexterna.dao.ConsultaExternaDao;
import mx.gob.imss.simo.consultaexterna.dao.ControlMedicoEncabezadoDao;
import mx.gob.imss.simo.consultaexterna.dao.impl.ConsultaExternaDaoImpl;
import mx.gob.imss.simo.consultaexterna.dto.ControlMedicoEncabezadoDto;
import mx.gob.imss.simo.consultaexterna.pojo.SdAccidentesLesiones;
import mx.gob.imss.simo.consultaexterna.pojo.SdCapturista;
import mx.gob.imss.simo.consultaexterna.pojo.SdConsultorio;
import mx.gob.imss.simo.consultaexterna.pojo.SdDelegacion;
import mx.gob.imss.simo.consultaexterna.pojo.SdDiagnosticoAdicional;
import mx.gob.imss.simo.consultaexterna.pojo.SdDiagnosticoComplementario;
import mx.gob.imss.simo.consultaexterna.pojo.SdDiagnosticoPrincipal;
import mx.gob.imss.simo.consultaexterna.pojo.SdDiagnosticos;
import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdEspecialidades;
import mx.gob.imss.simo.consultaexterna.pojo.SdFormato;
import mx.gob.imss.simo.consultaexterna.pojo.SdInformacionAdicional;
import mx.gob.imss.simo.consultaexterna.pojo.SdMedico;
import mx.gob.imss.simo.consultaexterna.pojo.SdMetodoAnticonceptivo;
import mx.gob.imss.simo.consultaexterna.pojo.SdPaciente;
import mx.gob.imss.simo.consultaexterna.pojo.SdPaseServicio;
import mx.gob.imss.simo.consultaexterna.pojo.SdPerfil;
import mx.gob.imss.simo.consultaexterna.pojo.SdProcedimientoAdicional;
import mx.gob.imss.simo.consultaexterna.pojo.SdProcedimientoComplementario;
import mx.gob.imss.simo.consultaexterna.pojo.SdProcedimientoPrincipal;
import mx.gob.imss.simo.consultaexterna.pojo.SdProcedimientos;
import mx.gob.imss.simo.consultaexterna.pojo.SdRamaAseguramiento;
import mx.gob.imss.simo.consultaexterna.pojo.SdRiesgoTrabajo;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoDiarrea;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoPrestador;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoUrgencia;
import mx.gob.imss.simo.consultaexterna.pojo.SdTurno;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadAdscripcion;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;
import mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService;
import mx.gob.imss.simo.model.ConsultaExternaDto;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.model.UsuarioDetalleDto;

/**
 * @author francisco.rrios
 * 
 */
@Service("consultaExternaService")
public class ConsultaExternaServiceImpl implements ConsultaExternaService {

	final static Logger logger = Logger
			.getLogger(ConsultaExternaServiceImpl.class.getName());
	
	@Autowired
	private ControlMedicoEncabezadoDao controlMedicoEncabezadoDao;
	@Autowired
	private ConsultaExternaDao consultaExternaDao;
	@Autowired
	private SicEspecialidadUnidadDao sicEspecialidadUnidadDao;
	@Autowired
	private SicTipoPrestadorDao sicTipoPrestadorDao;
	@Autowired
	private SicTurnoDao sicTurnoDao;
	@Autowired
	private SicDelegacionImssDao sicDelegacionImssDao;
	@Autowired
	private SicPaseServicioDao SicPaseServicioDao;
	@Autowired
	private SicMetodoAnticonceptivoDao sicMetodoAnticonceptivoDao;
	@Autowired
	private SicAccidentesLesionesDao sicAccidentesLesionesDao;
	@Autowired
	private SicRiesgosTrabajoDao sicRiesgosTrabajoDao;
	@Autowired
	private SitPacienteDao sitPacienteDao;
	@Autowired
	private SicUnidadAdscripcionDao sicUnidadAdscripcionDao;
	@Autowired
	private SicTipoUrgenciaDao SicTipoUrgenciaDao;
	@Autowired
	private SicCie10Dao sicCie10Dao;
	@Autowired
	private SicCie10MorfologiaDao sicCie10MorfologiaDao;
	@Autowired
	private SicCie9Dao SicCie9Dao;
	@Autowired
	private SicMedicoDao sicMedicoDao;
	@Autowired
	private SicInformacionAdicionalDao sicInformacionAdicionalDao;
	@Autowired
	private SicLugarAtencionDao sicLugarAtencionDao;
	@Autowired
	private SicTipoDiarreaDao sicTipoDiarreaDao;
	@Autowired
	private SicPeriodosImssDao sicPeriodosImssDao;
	@Autowired
	private SicConsultorioDao sicConsultorioDao;
	@Autowired
	private SicEspecialidadDao sicEspecialidadDao;
	@Autowired
	private SicInformacionAdicionalVIHDao sicInformacionAdicionalVIHDao;
	@Autowired
	private SicInformacionAdicionalEmbarazoDao sicInformacionAdicionalEmbarazoDao;

	@Override
	public void insertar(ConsultaExternaDto consulExtDto,
			UsuarioDetalleDto usuarioDetalleDto) {
		try {
			SitConsultaExterna consultaExterna = new SitConsultaExterna();
			consultaExterna.setFecCaptura(consulExtDto.getFecCaptura());
			cargaConsulExterna(consultaExterna, usuarioDetalleDto);

			SdEncabezado encabezado = (SdEncabezado) AssemblerDtoEntity
					.convertirDtoToEntidad(consulExtDto, SdEncabezado.class);

			consultaExterna.setSdEncabezado(cargaEncabezado(encabezado,
					consulExtDto, usuarioDetalleDto.getCvePresupuestal()));

			consultaExternaDao.insertar(consultaExterna);
			
			
			consulExtDto.setId(consultaExterna.getId().toString());
			consulExtDto.setEspecialidad(consultaExterna.getSdEncabezado()
					.get(0).getSdEspecialidad().get(0).getNumTipoServicio()
					.toString());
			consulExtDto.setDesEspecialidad(consultaExterna
					.getSdEncabezado()
					.get(0)
					.getSdEspecialidad()
					.get(0)
					.getCveEspecialidad()
					.toString()
					.concat(" ")
					.concat(consultaExterna.getSdEncabezado().get(0)
							.getSdEspecialidad().get(0).getDesEspecialidad()));
			consulExtDto.setTipoPrestador(consultaExterna.getSdEncabezado()
					.get(0).getSdTipoPrestador().get(0).getCveTipoPrestador()
					.toString());
			consulExtDto.setDesTipoPrestador(consultaExterna.getSdEncabezado()
					.get(0).getSdTipoPrestador().get(0).getDesTipoPrestador());
			consulExtDto.setTurno(consultaExterna.getSdEncabezado().get(0)
					.getSdTurno().get(0).getCveTurno().toString());
			consulExtDto.setDesTurno(consultaExterna.getSdEncabezado().get(0)
					.getSdTurno().get(0).getDesTurno());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void cargaConsulExterna(SitConsultaExterna consultaExterna,
			UsuarioDetalleDto usuarioDetalleDto) {

		List<SdCapturista> cap = new ArrayList<SdCapturista>();
		cap.add(new SdCapturista(usuarioDetalleDto.getAppPaterno(),
				usuarioDetalleDto.getAppMaterno(), usuarioDetalleDto
						.getNombre()));
		consultaExterna.setSdCapturista(cap);

		List<SdPerfil> per = new ArrayList<SdPerfil>();
		per.add(new SdPerfil(usuarioDetalleDto.getCvePerfil(),
				usuarioDetalleDto.getPerfil()));
		consultaExterna.setSdPerfil(per);

		List<SdDelegacion> del = new ArrayList<SdDelegacion>();
		del.add(new SdDelegacion(usuarioDetalleDto.getCveDelegacionUmae(),
				usuarioDetalleDto.getDesDelegacionAct()));
		consultaExterna.setSdDelegacion(del);

		List<SdUnidadMedica> uni = new ArrayList<SdUnidadMedica>();
		uni.add(new SdUnidadMedica(usuarioDetalleDto.getCvePresupuestal(),
				usuarioDetalleDto.getDesUnidadMedicaAct()));
		consultaExterna.setSdUnidadMedica(uni);

	}

	@SuppressWarnings("unchecked")
	private List<SdEncabezado> cargaEncabezado(SdEncabezado encabezado,
			ConsultaExternaDto consulExtDto, String cvePresupuestal) {
		List<SdEncabezado> listEncabezado = new ArrayList<SdEncabezado>();
		List<SicEspecialidadUnidad> listEspecialidad = sicEspecialidadUnidadDao
				.burcarPorIdUnidadMed(consulExtDto.getCveEspecialidad(),
						cvePresupuestal);
		List<SicTipoPrestador> listTipoPrestador = sicTipoPrestadorDao
				.burcarPorId(Integer.parseInt(consulExtDto.getTipoPrestador()));
		List<SicTurno> listTurno = sicTurnoDao.burcarPorId(Integer
				.parseInt(consulExtDto.getTurno()));
		List<SicConsultorio> listConsultorio = new ArrayList<SicConsultorio>();
		SicConsultorio consultorio = new SicConsultorio();
		consultorio.setCveConsultorio(consulExtDto.getConsultorio());
		listConsultorio.add(consultorio);

		try {

			List<SdMedico> listMedico = new ArrayList<SdMedico>();
			SdMedico medico = new SdMedico();
			medico.setCveMatricula(consulExtDto.getMatriculaMedico());
			medico.setRefNombre(consulExtDto.getNombresMedi());
			medico.setRefApellidoPaterno(consulExtDto.getaPaternoMedi());
			medico.setRefApellidoMaterno(consulExtDto.getaMaternoMedi());
			listMedico.add(medico);
			encabezado.setSdMedico(listMedico);
			List<SicEspecialidadUnidad> listEspecialidadUno = new ArrayList<SicEspecialidadUnidad>();
            if (listEspecialidad.size() > 0) {
                listEspecialidadUno.add(listEspecialidad.get(0));
            }
            encabezado.setSdEspecialidad((List<SdEspecialidades>) AssemblerDtoEntity
                    .convertirListEntidadToDto(listEspecialidadUno, SdEspecialidades.class));


			encabezado
					.setSdTipoPrestador((List<SdTipoPrestador>) AssemblerDtoEntity
							.convertirListEntidadToDto(listTipoPrestador,
									SdTipoPrestador.class));
			encabezado.setSdTurno((List<SdTurno>) AssemblerDtoEntity
					.convertirListEntidadToDto(listTurno, SdTurno.class));
			encabezado
					.setSdConsultorio((List<SdConsultorio>) AssemblerDtoEntity
							.convertirListEntidadToDto(listConsultorio,
									SdConsultorio.class));
			listEncabezado.add(encabezado);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return listEncabezado;
	}

	@Override
	public Boolean actualizar(ConsultaExternaDto consulExtDto) {
		Boolean actualizo=Boolean.FALSE;
		List<SdFormato> listFormatos = new ArrayList<SdFormato>();
		List<SdEncabezado> listEncabezado = new ArrayList<SdEncabezado>();
		SdFormato formato = null;
		try {
			SitConsultaExterna consultaExterna = consultaExternaDao.buscar(
					consulExtDto.getFecha(), consulExtDto.getMatriculaMedico(),
					consulExtDto.getCveEspecialidad(),
					Integer.parseInt(consulExtDto.getTurno()));

			listEncabezado = consultaExterna.getSdEncabezado();
			listFormatos = consultaExterna.getSdEncabezado().get(0)
					.getSdFormato();

			formato = cargaActualizacion(consulExtDto, consultaExterna);

			if(formato!=null && formato.getSdDiagnosticos()!=null && formato.getSdDiagnosticos().size()!=0 && formato.getSdDiagnosticos().get(0)!=null
					&& formato.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal()!=null
					&& formato.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0)!=null
					&& formato.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0).getCveCie10()!=null
					&& !formato.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0).getCveCie10().equals("")) {
			
				listFormatos.add(formato);
				listEncabezado.get(0).setSdFormato(listFormatos);
				consultaExterna.setSdEncabezado(listEncabezado);

				actualizo=Boolean.TRUE;
				actualizarControlCapturaCe(consultaExternaDao.actualizar(consultaExterna),consultaExterna);
			}
		} catch (Exception e) {
			actualizo=Boolean.FALSE;
			logger.info("Error al guardar:"+e);
		}
		return actualizo;
	}

	
	
	
	@Override
	public void actEdicion(ConsultaExternaDto consulExtDto) {
		List<SdFormato> listFormatos = new ArrayList<SdFormato>();
		List<SdEncabezado> listEncabezado = new ArrayList<SdEncabezado>();
		SdFormato formato = null;
		try {
			SitConsultaExterna consultaExterna = consultaExternaDao.buscar(
					consulExtDto.getFecha(), consulExtDto.getMatriculaMedico(),
					consulExtDto.getCveEspecialidad(),
					Integer.parseInt(consulExtDto.getTurno()));

			listEncabezado = consultaExterna.getSdEncabezado();
			listFormatos = consultaExterna.getSdEncabezado().get(0)
					.getSdFormato();

			formato = cargaActualizacion(consulExtDto, consultaExterna);

			for (int index = 0; index < listFormatos.size(); index++) {
				SdFormato atencion = listFormatos.get(index);
				if (atencion.getNumConsecutivo().equals(
						consulExtDto.getNumConsulta())) {
					listFormatos.set(index, formato);
					break;
				}
			}
			listEncabezado.get(0).setSdFormato(listFormatos);
			consultaExterna.setSdEncabezado(listEncabezado);

			actualizarControlCapturaCe(consultaExternaDao.actualizar(consultaExterna),consultaExterna);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private SdFormato cargaActualizacion(ConsultaExternaDto consulExtDto,
			SitConsultaExterna consultaExterna) {
		SdFormato formato = null;
		try {
			formato = (SdFormato) AssemblerDtoEntity.convertirDtoToEntidad(
					consulExtDto, SdFormato.class);

			// coloca el valor de la rama de aseguramiento
			SdRamaAseguramiento sdRamaAseguramiento = new SdRamaAseguramiento();
			sdRamaAseguramiento.setCveRamaAseguramiento(consulExtDto
					.getCveRamaAseguramiento());
			sdRamaAseguramiento.setDesRamaAseguramiento(consulExtDto
					.getDescRamaAseguramiento());

			//coloca el valor del NumEstadoFormato en cero para su actualizacion desde el proceso batch
			formato.setNumEstadoFormato(0);
			formato.setSdRamaAseguramiento(sdRamaAseguramiento);

			formato.setSdPaciente(cargaPaciente(consulExtDto));
			if(consulExtDto.getStpCaptura()==null){
				consulExtDto.setStpCaptura(new Date());				
			}
			formato.setStpCaptura(consulExtDto.getStpCaptura());
			formato.setStpActualiza(consulExtDto.getStpActualiza());
			
			if (consulExtDto.getPase() != null) {
				List<SicPaseServicio> pase = SicPaseServicioDao
						.burcarPorId(consulExtDto.getPase());
				formato.setSdPaseServicio((List<SdPaseServicio>) AssemblerDtoEntity
						.convertirListEntidadToDto(pase, SdPaseServicio.class));
			}
			if (consulExtDto.getMetodoPpf() != null) {
				List<SicMetodoAnticonceptivo> listMetodoPpf = sicMetodoAnticonceptivoDao
						.burcarPorId(consulExtDto.getMetodoPpf());
				List<SdMetodoAnticonceptivo> aux = (List<SdMetodoAnticonceptivo>) AssemblerDtoEntity
						.convertirListEntidadToDto(listMetodoPpf,
								SdMetodoAnticonceptivo.class);
				aux.get(0).setCanMetodoAnticonceptivo(
						consulExtDto.getCantidadMpf());
				formato.setSdMetodoAnticonceptivo(aux);
			}
			if (consulExtDto.getRiesgosTrabajo() != null) {
				List<SicRiesgoTrabajo> riesgosTrabajo = sicRiesgosTrabajoDao
						.burcarPorId(consulExtDto.getRiesgosTrabajo());
				formato.setSdRiesgoTrabajo((List<SdRiesgoTrabajo>) AssemblerDtoEntity
						.convertirListEntidadToDto(riesgosTrabajo,
								SdRiesgoTrabajo.class));
			}
			if (consulExtDto.getCveTipoUrgencia() != null) {
				List<SicTipoUrgencia> tipoUrgencia = SicTipoUrgenciaDao
						.burcarPorId(consulExtDto.getCveTipoUrgencia());
				formato.setSdTipoUrgencia((List<SdTipoUrgencia>) AssemblerDtoEntity
						.convertirListEntidadToDto(tipoUrgencia,
								SdTipoUrgencia.class));
			}
			// ******************Inicio Diagnostico****************
			int index = 0;
			if (consultaExterna.getSdEncabezado().get(index).getSdFormato()
					.size() > 0) {
				List<SdDiagnosticos> sdDiagnosticos = consultaExterna
						.getSdEncabezado().get(index).getSdFormato().get(index)
						.getSdDiagnosticos();
				
				if(consulExtDto.getDiagnosticoPrincipal()!=null && consulExtDto.getDiagnosticoPrincipal().length()>0) {				
				
					if (sdDiagnosticos!=null && sdDiagnosticos.size()>0){ 

						if (consulExtDto.getDiagnosticoPrincipal().equals(
								sdDiagnosticos.get(index).getSdDiagnosticoPrincipal()
										.get(index).getCveCie10())) {
							if (consulExtDto.getCveInforAdicPrin() == null) {
								List<SdInformacionAdicional> sdInformacionAdicional = sdDiagnosticos
										.get(index).getSdDiagnosticoPrincipal()
										.get(index).getSdInformacionAdicional();
								consulExtDto.setCveInforAdicPrin(sdInformacionAdicional
										.size() == 0 ? null : sdInformacionAdicional
										.get(index).getCveInformacionAdicional());
							}
							if (consulExtDto.getCveTipoPrin() == null) {
								List<SdTipoDiarrea> sdTipoDiarrea = sdDiagnosticos
										.get(index).getSdDiagnosticoPrincipal()
										.get(index).getSdTipoDiarrea();
								consulExtDto
										.setCveTipoPrin(sdTipoDiarrea.size() == 0 ? null
												: sdTipoDiarrea.get(index)
														.getCveTipoDiarrea());
							}
							if (consulExtDto.getFecCirugiaPrin() == null) {
								Date fecCirugia = sdDiagnosticos.get(index)
										.getSdDiagnosticoPrincipal().get(index)
										.getFecCirugia();
								consulExtDto
										.setFecCirugiaPrin(fecCirugia == null ? null
												: fecCirugia);
							}
						}

						if (sdDiagnosticos.get(index).getSdDiagnosticoComplementario()
								.size() > 0 && consulExtDto.getDiagnosticoComplemento() !=null
								&& consulExtDto.getDiagnosticoComplemento().equals(
										sdDiagnosticos.get(index)
												.getSdDiagnosticoComplementario()
												.get(index).getCveCie10())) {
							if (consulExtDto.getCveInforAdicComp() == null) {
								List<SdInformacionAdicional> sdInformacionAdicional = sdDiagnosticos
										.get(index).getSdDiagnosticoComplementario()
										.get(index).getSdInformacionAdicional();
								consulExtDto.setCveInforAdicComp(sdInformacionAdicional
										.size() == 0 ? null : sdInformacionAdicional
										.get(index).getCveInformacionAdicional());
							}
							if (consulExtDto.getCveTipoComp() == null) {
								List<SdTipoDiarrea> sdTipoDiarrea = sdDiagnosticos
										.get(index).getSdDiagnosticoComplementario()
										.get(index).getSdTipoDiarrea();
								consulExtDto
										.setCveTipoComp(sdTipoDiarrea.size() == 0 ? null
												: sdTipoDiarrea.get(index)
														.getCveTipoDiarrea());
							}
							if (consulExtDto.getFecCirugiaComp() == null) {
								Date fecCirugia = sdDiagnosticos.get(index)
										.getSdDiagnosticoComplementario().get(index)
										.getFecCirugia();
								consulExtDto
										.setFecCirugiaComp(fecCirugia == null ? null
												: fecCirugia);
							}
						}

						if (sdDiagnosticos.get(index).getSdDiagnosticoAdicional()
								.size() > 0 && consulExtDto.getDiagnosticoAdicional()!=null
								&& consulExtDto.getDiagnosticoAdicional().equals(
										sdDiagnosticos.get(index)
												.getSdDiagnosticoAdicional().get(index)
												.getCveCie10())) {
							if (consulExtDto.getCveInforAdicAdic() == null) {
								List<SdInformacionAdicional> sdInformacionAdicional = sdDiagnosticos
										.get(index).getSdDiagnosticoAdicional()
										.get(index).getSdInformacionAdicional();
								consulExtDto.setCveInforAdicAdic(sdInformacionAdicional
										.size() == 0 ? null : sdInformacionAdicional
										.get(index).getCveInformacionAdicional());
							}
							if (consulExtDto.getCveTipoAdic() == null) {
								List<SdTipoDiarrea> sdTipoDiarrea = sdDiagnosticos
										.get(index).getSdDiagnosticoAdicional()
										.get(index).getSdTipoDiarrea();
								consulExtDto
										.setCveTipoAdic(sdTipoDiarrea.size() == 0 ? null
												: sdTipoDiarrea.get(index)
														.getCveTipoDiarrea());
							}
							if (consulExtDto.getFecCirugiaAdic() == null) {
								Date fecCirugia = sdDiagnosticos.get(index)
										.getSdDiagnosticoAdicional().get(index)
										.getFecCirugia();
								consulExtDto
										.setFecCirugiaAdic(fecCirugia == null ? null
												: fecCirugia);
							}
						}
					}
				}else {
					return null;
				}
			}

			formato.setSdDiagnosticos(cargaDiagnosticos(consulExtDto));
			// ******************Fin Diagnostico*******************

			// ******************Inicio Procedimiento****************
			formato.setSdProcedimientos(cargaProcedimientos(consulExtDto));
			// ******************Fin Procedimiento****************
		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();

		}
		return formato;
	}

	private List<SdProcedimientos> cargaProcedimientos(ConsultaExternaDto consulExtDto) {
		List<SdProcedimientos> listProcedimientos = new ArrayList<SdProcedimientos>();
		try {
			SdProcedimientos sdProcedimientos = null;
			if (consulExtDto.getProcedimientoPrincipal() != null
					&& consulExtDto.getProcedimientoPrincipal().length() > 0) {
				sdProcedimientos = new SdProcedimientos();
				SicCie9 cie9MC = SicCie9Dao.buscarPorId(consulExtDto.getProcedimientoPrincipal());
				List<SdProcedimientoPrincipal> principal = new ArrayList<SdProcedimientoPrincipal>();
				SdProcedimientoPrincipal procPrin = new SdProcedimientoPrincipal(cie9MC.getCveCie9(),
						cie9MC.getDesCie9());
				principal.add(procPrin);
				sdProcedimientos.setSdProcedimientoPrincipal(principal);
				// listProcedimientos.add(sdProcedimientos);
			}
			if (consulExtDto.getProcedimientoComplemento() != null
					&& consulExtDto.getProcedimientoComplemento().length() > 0) {
				SicCie9 cie9MC = SicCie9Dao.buscarPorId(consulExtDto.getProcedimientoComplemento());
				List<SdProcedimientoComplementario> complementario = new ArrayList<SdProcedimientoComplementario>();
				SdProcedimientoComplementario procComp = new SdProcedimientoComplementario(cie9MC.getCveCie9(),
						cie9MC.getDesCie9());
				complementario.add(procComp);
				if (sdProcedimientos != null) {
					sdProcedimientos
							.setSdProcedimientoComplementario(complementario);
				} else {
					sdProcedimientos = new SdProcedimientos();
					sdProcedimientos
							.setSdProcedimientoComplementario(complementario);
				}
				// listProcedimientos.add(sdProcedimientos);
			}
			if (consulExtDto.getProcedimientoAdicional() != null
					&& consulExtDto.getProcedimientoAdicional().length() > 0) {
				SicCie9 cie9MC = SicCie9Dao.buscarPorId(consulExtDto.getProcedimientoAdicional());
				List<SdProcedimientoAdicional> adicional = new ArrayList<SdProcedimientoAdicional>();
				SdProcedimientoAdicional procAdic = new SdProcedimientoAdicional(cie9MC.getCveCie9(),
						cie9MC.getDesCie9());
				adicional.add(procAdic);
				if (sdProcedimientos != null) {
					sdProcedimientos.setSdProcedimientoAdicional(adicional);
				} else {
					sdProcedimientos = new SdProcedimientos();
					sdProcedimientos.setSdProcedimientoAdicional(adicional);
				}
			}
			if (sdProcedimientos != null) {
				listProcedimientos.add(sdProcedimientos);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		return listProcedimientos;
	}

	@SuppressWarnings("unchecked")
	private List<SdDiagnosticos> cargaDiagnosticos(
			ConsultaExternaDto consulExtDto) {
		List<SdDiagnosticos> diagnosticos = new ArrayList<SdDiagnosticos>();
		try {
			SdDiagnosticos sdDiagnosticos = null;
			if (consulExtDto.getDiagnosticoPrincipal() != null) {
				sdDiagnosticos = new SdDiagnosticos();
				SicCie10 cie10 = sicCie10Dao.buscarPorId(consulExtDto.getDiagnosticoPrincipal());
				List<SdDiagnosticoPrincipal> principal = new ArrayList<SdDiagnosticoPrincipal>();
				SdDiagnosticoPrincipal diaPrincipal = new SdDiagnosticoPrincipal(cie10.getCveCie10(),
						cie10.getDesCie10());
				if (null != consulExtDto.getMorfologia()) {
					SicCie10Morfologia morf = buscarMorfologiaPorId(consulExtDto
							.getMorfologia());
					List<SdInformacionAdicional> infAd = new ArrayList<SdInformacionAdicional>();
					SdInformacionAdicional morfologia = new SdInformacionAdicional();
					morfologia.setCveInformacionAdicional(morf
							.getCveCie10Morfologia());
					morfologia.setDesInformacionAdicional(morf
							.getDesCie10Morfologia());
					morfologia
							.setCveTipoInformacionAdicional(EnumTipoInformacionAdicional.MORFOLOGIA
									.getValor());
					infAd.add(morfologia);
					diaPrincipal.setSdInformacionAdicional(infAd);
				}
				if (null != consulExtDto.getCveCD4Principal()) {
					SicInformacionAdicionalVIH infoAdicional = sicInformacionAdicionalVIHDao
							.burcarVIHPorId(consulExtDto.getCveCD4Principal());
					List<SdInformacionAdicional> infVIH = new ArrayList<SdInformacionAdicional>();
					SdInformacionAdicional cd4VIH = new SdInformacionAdicional();
					cd4VIH.setCveInformacionAdicional(infoAdicional
							.getCveInfoAdicionalVIH().toString());
					cd4VIH.setDesInformacionAdicional(infoAdicional
							.getDesInfoAdicionalVIH());
					cd4VIH.setCveTipoInformacionAdicional(EnumTipoInformacionAdicional.VIH
							.getValor()); // VIH
					infVIH.add(cd4VIH);
					diaPrincipal.setSdInformacionAdicional(infVIH);
				}
				if (null != consulExtDto.getCveTipoEmbarazo()) {
					SicInformacionAdicionalEmbarazo infoAdicionalEmbarazo = sicInformacionAdicionalEmbarazoDao
							.burcarEmbarazoPorId(consulExtDto
									.getCveTipoEmbarazo());
					List<SdInformacionAdicional> infAEmbarazo = new ArrayList<SdInformacionAdicional>();
					SdInformacionAdicional infA = new SdInformacionAdicional();
					infA.setCveInformacionAdicional(infoAdicionalEmbarazo
							.getCveInfoAdicionalEmb().toString());
					infA.setDesInformacionAdicional(infoAdicionalEmbarazo
							.getDesInfoAdicionalEmb().toString());
					infA.setCveTipoInformacionAdicional(EnumTipoInformacionAdicional.EMBARAZO
							.getValor());
					infAEmbarazo.add(infA);
					diaPrincipal.setSdInformacionAdicional(infAEmbarazo);
				}
				if (consulExtDto.getCveTipoPrin() != null) {// diarrea
					List<SicTipoDiarrea> listTipoDiarrea = sicTipoDiarreaDao
							.burcarPorId(consulExtDto.getCveTipoPrin());
					diaPrincipal
							.setSdTipoDiarrea((List<SdTipoDiarrea>) AssemblerDtoEntity
									.convertirListEntidadToDto(listTipoDiarrea,
											SdTipoDiarrea.class));
				}
				if (consulExtDto.getFecCirugiaPrin() != null) {
					diaPrincipal
							.setFecCirugia(consulExtDto.getFecCirugiaPrin());
				}
				diaPrincipal.setInd1EraVez(consulExtDto.getPrimeraVez());
				principal.add(diaPrincipal);
				sdDiagnosticos.setSdDiagnosticoPrincipal(principal);
				// diagnosticos.add(sdDiagnosticos);
			}

			// DIAGNOSTICO COMPLEMENTARIO

			if (consulExtDto.getDiagnosticoComplemento() != null
					&& consulExtDto.getDiagnosticoComplemento().length() > 0) {
				List<SdDiagnosticoComplementario> complementario = new ArrayList<SdDiagnosticoComplementario>();
				SicCie10 cie10 = sicCie10Dao.buscarPorId(consulExtDto.getDiagnosticoComplemento().toUpperCase());

				SdDiagnosticoComplementario diaComplementario = new SdDiagnosticoComplementario(cie10.getCveCie10(),
						cie10.getDesCie10(), consulExtDto.getPrimeraVezDiagComp());

				if (null != consulExtDto.getCveCD4Compl()) {
					SicInformacionAdicionalVIH infoAdicionalc = sicInformacionAdicionalVIHDao
							.burcarVIHPorId(consulExtDto.getCveCD4Compl());
					List<SdInformacionAdicional> infVIHc = new ArrayList<SdInformacionAdicional>();
					SdInformacionAdicional cd4VIHc = new SdInformacionAdicional();
					cd4VIHc.setCveInformacionAdicional(infoAdicionalc
							.getCveInfoAdicionalVIH().toString());
					cd4VIHc.setDesInformacionAdicional(infoAdicionalc
							.getDesInfoAdicionalVIH());
					cd4VIHc.setCveTipoInformacionAdicional(EnumTipoInformacionAdicional.VIH
							.getValor()); // VIH
					infVIHc.add(cd4VIHc);
					diaComplementario.setSdInformacionAdicional(infVIHc);
				}
				if (null != consulExtDto.getCveTipoEmbarazoComplementario()) {
					SicInformacionAdicionalEmbarazo infoAdicionalEmbarazoC = sicInformacionAdicionalEmbarazoDao
							.burcarEmbarazoPorId(consulExtDto
									.getCveTipoEmbarazoComplementario());
					List<SdInformacionAdicional> infAEmbarazoC = new ArrayList<SdInformacionAdicional>();
					SdInformacionAdicional infA = new SdInformacionAdicional();
					infA.setCveInformacionAdicional(infoAdicionalEmbarazoC
							.getCveInfoAdicionalEmb().toString());
					infA.setDesInformacionAdicional(infoAdicionalEmbarazoC
							.getDesInfoAdicionalEmb().toString());
					infA.setCveTipoInformacionAdicional(EnumTipoInformacionAdicional.EMBARAZO
							.getValor());
					infAEmbarazoC.add(infA);
					diaComplementario.setSdInformacionAdicional(infAEmbarazoC);
				}
				if (consulExtDto.getCveTipoComp() != null) {// diarrea
					List<SicTipoDiarrea> listTipoDiarrea = sicTipoDiarreaDao
							.burcarPorId(consulExtDto.getCveTipoComp());
					diaComplementario
							.setSdTipoDiarrea((List<SdTipoDiarrea>) AssemblerDtoEntity
									.convertirListEntidadToDto(listTipoDiarrea,
											SdTipoDiarrea.class));
				}
				if (consulExtDto.getFecCirugiaComp() != null) {
					diaComplementario.setFecCirugia(consulExtDto
							.getFecCirugiaComp());
				}
				if (consulExtDto.getAccidentesLesiones() != null
						&& consulExtDto.getDiagAcc() == 2) {
					List<SicAccidentesLesiones> accidentesLesiones = sicAccidentesLesionesDao
							.burcarPorId(consulExtDto.getAccidentesLesiones());
					List<SdAccidentesLesiones> accidentes = new ArrayList<SdAccidentesLesiones>();
					SdAccidentesLesiones procAdic = new SdAccidentesLesiones(
							accidentesLesiones.get(0).getCveAccidenteLesion(),
							accidentesLesiones.get(0).getDesAccidenteLesion());
					accidentes.add(procAdic);

					diaComplementario.setSdAccidentesLesiones(accidentes);
				}
				complementario.add(diaComplementario);
				sdDiagnosticos.setSdDiagnosticoComplementario(complementario);
				// diagnosticos.add(sdDiagnosticos);
			}

			// DIAGNOSTICO ADICIONAL

			if (consulExtDto.getDiagnosticoAdicional() != null && consulExtDto.getDiagnosticoAdicional().length() > 0) {
				List<SdDiagnosticoAdicional> adicional = new ArrayList<SdDiagnosticoAdicional>();
				SdDiagnosticoAdicional diaAdicional = null;
				SicCie10 cie10 = sicCie10Dao.buscarPorId(consulExtDto.getDiagnosticoAdicional());
				if (cie10 == null) {
					List<SicCie10Morfologia> listCie10Morfologia = sicCie10MorfologiaDao
							.burcarPorVigentes(consulExtDto.getDiagnosticoAdicional().toUpperCase());
					diaAdicional = new SdDiagnosticoAdicional(listCie10Morfologia.get(0).getCveCie10Morfologia(),
							listCie10Morfologia.get(0).getDesCie10Morfologia(), consulExtDto.getPrimeraVezDiagAdic());
				} else {
					diaAdicional = new SdDiagnosticoAdicional(cie10.getCveCie10(), cie10.getDesCie10(),
							consulExtDto.getPrimeraVezDiagAdic());
				}

				if (null != consulExtDto.getCveCD4Adicional()) {
					SicInformacionAdicionalVIH infoAdicionala = sicInformacionAdicionalVIHDao
							.burcarVIHPorId(consulExtDto.getCveCD4Adicional());
					List<SdInformacionAdicional> infVIHa = new ArrayList<SdInformacionAdicional>();
					SdInformacionAdicional cd4VIHa = new SdInformacionAdicional();
					cd4VIHa.setCveInformacionAdicional(infoAdicionala
							.getCveInfoAdicionalVIH().toString());
					cd4VIHa.setDesInformacionAdicional(infoAdicionala
							.getDesInfoAdicionalVIH());
					cd4VIHa.setCveTipoInformacionAdicional(EnumTipoInformacionAdicional.VIH
							.getValor()); // VIH
					infVIHa.add(cd4VIHa);
					diaAdicional.setSdInformacionAdicional(infVIHa);
				}
				if (null != consulExtDto.getCveTipoEmbarazoAdicional()) {
					SicInformacionAdicionalEmbarazo infoAdicionalEmbarazoA = sicInformacionAdicionalEmbarazoDao
							.burcarEmbarazoPorId(consulExtDto
									.getCveTipoEmbarazoAdicional());
					List<SdInformacionAdicional> infAEmbarazoA = new ArrayList<SdInformacionAdicional>();
					SdInformacionAdicional infA = new SdInformacionAdicional();
					infA.setCveInformacionAdicional(infoAdicionalEmbarazoA
							.getCveInfoAdicionalEmb().toString());
					infA.setDesInformacionAdicional(infoAdicionalEmbarazoA
							.getDesInfoAdicionalEmb().toString());
					infA.setCveTipoInformacionAdicional(EnumTipoInformacionAdicional.EMBARAZO
							.getValor());
					infAEmbarazoA.add(infA);
					diaAdicional.setSdInformacionAdicional(infAEmbarazoA);
				}
				if (consulExtDto.getCveTipoAdic() != null) {// diarrea
					List<SicTipoDiarrea> listTipoDiarrea = sicTipoDiarreaDao
							.burcarPorId(consulExtDto.getCveTipoAdic());
					diaAdicional
							.setSdTipoDiarrea((List<SdTipoDiarrea>) AssemblerDtoEntity
									.convertirListEntidadToDto(listTipoDiarrea,
											SdTipoDiarrea.class));
				}
				if (consulExtDto.getFecCirugiaAdic() != null) {
					diaAdicional
							.setFecCirugia(consulExtDto.getFecCirugiaAdic());
				}
				if (consulExtDto.getAccidentesLesiones() != null
						&& consulExtDto.getDiagAcc() == 1) {
					List<SicAccidentesLesiones> accidentesLesiones = sicAccidentesLesionesDao
							.burcarPorId(consulExtDto.getAccidentesLesiones());
					List<SdAccidentesLesiones> accidentes = new ArrayList<SdAccidentesLesiones>();
					SdAccidentesLesiones procAdic = new SdAccidentesLesiones(
							accidentesLesiones.get(0).getCveAccidenteLesion(),
							accidentesLesiones.get(0).getDesAccidenteLesion());
					accidentes.add(procAdic);

					diaAdicional.setSdAccidentesLesiones(accidentes);
				}
				adicional.add(diaAdicional);
				sdDiagnosticos.setSdDiagnosticoAdicional(adicional);
			}
			diagnosticos.add(sdDiagnosticos);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		return diagnosticos;
	}
	private List<SdPaciente> cargaPaciente(ConsultaExternaDto consulExtDto) {
		List<SdPaciente> pasientes = new ArrayList<SdPaciente>();
		try {
			SdPaciente paciente = new SdPaciente();
			paciente.setNumNss(consulExtDto.getNss());
			paciente.setRefAgregadoMedico(consulExtDto.getAgregadoMedico());
			paciente.setRefNombre(consulExtDto.getNombre());
			paciente.setRefApellidoPaterno(consulExtDto.getApePaterno());
			paciente.setRefApellidoMaterno(consulExtDto.getApeMaterno());
			paciente.setFecNacimiento(consulExtDto.getFecNacimiento());

			paciente.setNumEdad(consulExtDto.getEdad());
			paciente.setCveCurp(consulExtDto.getCveCurp());
			paciente.setIndVigente(consulExtDto.getIndVigente());
			paciente.setIndPacEncontrado(consulExtDto.getIndPacEncontrado());
			paciente.setStpUltConsultaVig(consulExtDto.getStpUltConsultaVig());
			paciente.setCveIdee(consulExtDto.getCveIdee());
			
			if (consulExtDto.getUnidadAdscripcion() != null) {
				List<SdUnidadAdscripcion> uniAdscripcion = new ArrayList<SdUnidadAdscripcion>();
				SdUnidadAdscripcion unidad = new SdUnidadAdscripcion();

				unidad.setCvePresupuestal(consulExtDto.getUnidadAdscripcion());
				unidad.setDesUnidadMedica(consulExtDto.getNombreUnidadMed());
				uniAdscripcion.add(unidad);
				paciente.setSdUnidadAdscripcion(uniAdscripcion);
			}

			pasientes.add(paciente);

		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		return pasientes;
	}
	
	public Integer validarConsecutivoConsulta(ConsultaExternaDto consulExtDto){
		
		Integer numAtenciones = 0;
		List<SdEncabezado> sdEncabezado = null;
		
		SitConsultaExterna consultaExterna = consultaExternaDao.buscar(
				consulExtDto.getFecha(), consulExtDto.getMatriculaMedico(),
				consulExtDto.getCveEspecialidad(),
				Integer.parseInt(consulExtDto.getTurno()));
		
		if (consultaExterna != null) {
			sdEncabezado = consultaExterna.getSdEncabezado();
		}
		if (sdEncabezado != null) {
			if (sdEncabezado.get(0).getSdFormato() != null) {
				numAtenciones = sdEncabezado.get(0).getSdFormato().size();
			} else {
				numAtenciones = 0;
			}
		}
		
		return numAtenciones;		
	}

	@Override
	public Integer buscar(ConsultaExternaDto consulExtDto,
			UsuarioDetalleDto usuarioDetalleDto) {
		Integer numAtenciones = 0;
		List<SdEncabezado> sdEncabezado = null;
		try {
			SitConsultaExterna consultaExterna = consultaExternaDao.buscar(
					consulExtDto.getFecha(), consulExtDto.getMatriculaMedico(),
					consulExtDto.getCveEspecialidad(),
					Integer.parseInt(consulExtDto.getTurno()));

			if (consultaExterna != null) {
				sdEncabezado = consultaExterna.getSdEncabezado();
			}

			// RN03 <Asignar numero de consulta>
			if (sdEncabezado != null) {
				if (sdEncabezado.get(0).getSdFormato() != null) {
					numAtenciones = sdEncabezado.get(0).getSdFormato().size();
					numAtenciones++;
				} else {
					numAtenciones = 1;
				}
				consulExtDto.setId(consultaExterna.getId().toString());
				consulExtDto.setEspecialidad(sdEncabezado
						.get(0)
						.getSdEspecialidad()
						.get(0)
						.getNumTipoServicio()
						.toString()
						.concat(".")
						.concat(sdEncabezado.get(0).getSdEspecialidad().get(0)
								.getCveEspecialidad()));
				consulExtDto.setCveEspecialidad(sdEncabezado.get(0)
						.getSdEspecialidad().get(0).getCveEspecialidad());
				consulExtDto.setEspecialidadSelc(sdEncabezado.get(0)
						.getSdEspecialidad().get(0).getNumTipoServicio()
						.toString());
				consulExtDto.setDesEspecialidad(sdEncabezado
						.get(0)
						.getSdEspecialidad()
						.get(0)
						.getCveEspecialidad()
						.toString()
						.concat(" ")
						.concat(sdEncabezado.get(0).getSdEspecialidad().get(0)
								.getDesEspecialidad()));
				consulExtDto.setTipoPrestador(sdEncabezado.get(0)
						.getSdTipoPrestador().get(0).getCveTipoPrestador()
						.toString());
				consulExtDto.setDesTipoPrestador(sdEncabezado.get(0)
						.getSdTipoPrestador().get(0).getDesTipoPrestador());
				consulExtDto.setTurno(sdEncabezado.get(0).getSdTurno().get(0)
						.getCveTurno().toString());
				consulExtDto.setDesTurno(sdEncabezado.get(0).getSdTurno()
						.get(0).getDesTurno());
			} else {
				insertar(consulExtDto, usuarioDetalleDto);
				// numAtenciones = 1;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return numAtenciones;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService#
	 * cargarNombreUnidad
	 * (mx.gob.imss.simo.consultaexterna.dto.ConsultaExternaDto)
	 */
	@Override
	public void cargarNombreUnidad(ConsultaExternaDto consulExtDto) {
		// Busca por medio de un WS el nombre correspondiente al Nss y Agregado;
		// asi como la Unidad Adscripto

		consulExtDto.setNombre("Jose");
		consulExtDto.setApePaterno("Perez");
		consulExtDto.setApeMaterno("Gomez");

		consulExtDto.setUnidadAdscripcion("35");
		consulExtDto.setNombreUnidadMed("Feliz Descanso");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService#
	 * buscarEncabezadoCap
	 * (mx.gob.imss.simo.consultaexterna.dto.ConsultaExternaDto)
	 */
	@Override
	public void buscarEncabezadoCap(ConsultaExternaDto consulExtDto) {
		try {
			SitConsultaExterna consultaExterna = consultaExternaDao.buscar(
					consulExtDto.getFecha(), consulExtDto.getMatriculaMedico(),
					consulExtDto.getEspecialidad(),
					Integer.parseInt(consulExtDto.getTurno()));

			if (consultaExterna != null) {
				consulExtDto = (ConsultaExternaDto) AssemblerDtoEntity
						.convertirEntidadToDto(consultaExterna,
								ConsultaExternaDto.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService#
	 * buscarAtenciones(mx.gob.imss.simo.consultaexterna.dto.ConsultaExternaDto)
	 */
	@Override
	public List<ConsultaExternaDto> buscarAtenciones(
			ConsultaExternaDto consulExtDto, UsuarioDetalleDto usuarioDetalleDto) {
		List<ConsultaExternaDto> listResul = new ArrayList<ConsultaExternaDto>();
		try {
			List<SdEncabezado> listEncabezado = consultaExternaDao
					.buscarAtenciones(consulExtDto.getFecha(),
							consulExtDto.getMatriculaMedico(),
							consulExtDto.getNss(),
							consulExtDto.getEspecialidad(),
							consulExtDto.getAgregadoMedico(),
							consulExtDto.getCodigoCie(),
							consulExtDto.getPrimeraVez(),
							usuarioDetalleDto.getCvePresupuestal());

			// listResul = (List<ConsultaExternaDto>) AssemblerDtoEntity
			// .convertirListEntidadToDto(listEncabezado,
			// ConsultaExternaDto.class);
			for (SdEncabezado enca : listEncabezado) {
				List<SdFormato> listSdFormato = enca.getSdFormato();
				for (int i = 0; i < listSdFormato.size(); i++) {
					SdFormato sdFor = listSdFormato.get(i);
					boolean bandera = true;
					if (consulExtDto.getNss() != null) {
						if (!consulExtDto.getNss().equals(
								sdFor.getSdPaciente().get(0).getNumNss())) {
							bandera = false;
						}
					}
					if (consulExtDto.getAgregadoMedico() != null) {
						if (!consulExtDto.getAgregadoMedico().equals(
								sdFor.getSdPaciente().get(0)
										.getRefAgregadoMedico())) {
							bandera = false;
						}
					}
					if (consulExtDto.getCodigoCie() != null) {
						if (!consulExtDto.getCodigoCie().equals(
								sdFor.getSdDiagnosticos().get(0)
										.getSdDiagnosticoPrincipal().get(0)
										.getCveCie10())) {
							bandera = false;
						}
					}
					if (consulExtDto.getPrimeraVez() != null) {
						if (consulExtDto.getPrimeraVez() < 2) {
							if (!consulExtDto.getPrimeraVez().equals(
									sdFor.getInd1EraVez())) {
								bandera = false;
							}

						}
					}

					if (bandera) {
						ConsultaExternaDto oDto = (ConsultaExternaDto) AssemblerDtoEntity
								.convertirEntidadToDto(sdFor,
										ConsultaExternaDto.class);
						oDto.setEspecialidad(enca.getSdEspecialidad().get(0)
								.getCveEspecialidad().toString());
						oDto.setDesEspecialidad(enca.getSdEspecialidad().get(0)
								.getDesEspecialidad());
						oDto.setMatriculaMedico(enca.getSdMedico().get(0)
								.getCveMatricula());
						oDto.setTurno(enca.getSdTurno().get(0).getCveTurno()
								.toString());
						oDto.setFecha(enca.getFecAltaEncabezado());
						oDto.setPrimeraVez(enca.getSdFormato().get(0).getInd1EraVez());
						oDto.getSdEncabezado().add(enca);
						
		//FIXME:AGRP se comenta la vista de consulta hasta que se definan correctamente los componentes de la pantalla  

						if (usuarioDetalleDto
								.tieneRole(EnumRoles.ROLE_ADMIN_CENTRAL
										.getDescripcion())) {
							oDto.setEditable(Boolean.TRUE);
							oDto.setConsultar(Boolean.FALSE);
						
						} else if (usuarioDetalleDto
								.tieneRole(EnumRoles.ROLE_ADMIN_DELEG
										.getDescripcion())) {
							// 1 mes
		/*					double tiempo = calculaTiempo(
									enca.getFecAltaEncabezado(), 2);
							if (tiempo <= 1) { */
								oDto.setEditable(Boolean.TRUE);
								oDto.setConsultar(Boolean.FALSE);
		/*					} else {
								oDto.setEditable(Boolean.FALSE);
								oDto.setConsultar(Boolean.TRUE);
							} */
						} else if (usuarioDetalleDto
								.tieneRole(EnumRoles.ROLE_ARIMAC_JEFE
										.getDescripcion())) {
							// 1 sem
		/*					double tiempo = calculaTiempo(
									enca.getFecAltaEncabezado(), 1);
							if (tiempo <= 1) {*/
								oDto.setEditable(Boolean.TRUE);
								oDto.setConsultar(Boolean.FALSE);
		/*					} else {
								oDto.setEditable(Boolean.FALSE);
								oDto.setConsultar(Boolean.TRUE);
							}*/
						} 
						listResul.add(oDto);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listResul;
	}

	@Override
	public SicCie10DTO buscarDiagnostico(String cveDiagnostico, int edad, String sexo, boolean edadSem) {
		int numsexo = sexo.toUpperCase().equals("M") ? 1 : 2;
		SicCie10 cie10 = sicCie10Dao.buscarPorId(cveDiagnostico);
		SicCie10DTO dtoDiagnostico = null;

		try {
			if (cie10 != null) {
				dtoDiagnostico = (SicCie10DTO) MapperEntityToDto.conversion(cie10, SicCie10DTO.class);

				if (cie10.getCveSexo().equals(numsexo) || cie10.getCveSexo().equals(0)) {
					if (edadSem) {
						if (edad < cie10.getNumEdadInferiorSemanas() || edad > cie10.getNumEdadSuperiorSemanas()) {
							dtoDiagnostico.setDesCie10("N");
						}
					} else {
						if (edad < cie10.getNumEdadInferiorAnios() || edad > cie10.getNumEdadSuperiorAnios()) {
							dtoDiagnostico.setDesCie10("N");
						}
					}
				} else {
					dtoDiagnostico.setDesCie10("X");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dtoDiagnostico;
	}
	
	@Override
	public SicCie10DTO buscarDiagnostico(String cveDiagnostico) {
		SicCie10 cie10 = sicCie10Dao.buscarPorId(cveDiagnostico);
		SicCie10DTO dtoDiagnostico = null;
		try {
			if (cie10 != null) {
				dtoDiagnostico = (SicCie10DTO) MapperEntityToDto.conversion(cie10, SicCie10DTO.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dtoDiagnostico;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService#
	 * buscarProcedimiento(java.lang.String, java.lang.Integer,
	 * java.lang.String)
	 */
	@Override
	public SicCie9DTO buscarProcedimiento(String cveProcedimiento, int edad, String sexo, boolean edadAnn) {
		int numsexo = sexo.toUpperCase().equals("M") ? 1 : 2;
		SicCie9 cie9 = SicCie9Dao.buscarPorId(cveProcedimiento);
		SicCie9DTO dtoProcedimiento = null;

		try {
			if (cie9 != null) {
				dtoProcedimiento = (SicCie9DTO) MapperEntityToDto.conversion(cie9, SicCie9DTO.class);
				if (cie9.getCveSexo().equals(numsexo) || cie9.getCveSexo().equals(0)) {
					if (edadAnn) {
						if (edad < cie9.getNumEdadInferiorAnios() || edad > cie9.getNumEdadSuperiorAnios()) {
							dtoProcedimiento.setDesCie9("N");
						}
					} else {
						if (edad < cie9.getNumEdadInferiorSemanas() || edad > cie9.getNumEdadSuperiorSemanas()) {
							dtoProcedimiento.setDesCie9("N");
						}
					}
				} else {
					dtoProcedimiento.setDesCie9("X");
				}
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dtoProcedimiento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService#
	 * buscarCantidad(java.lang.Integer)
	 */
	@Override
	public Integer buscarCantidad(Integer cveMetodoPPF) {
		Integer cantidad = 0;
		// TODO:AGRP Verificar como se recibe la cantidad
		try {
			SicMetodoAnticonceptivo metodoPpf = sicMetodoAnticonceptivoDao
					.burcarPorId(cveMetodoPPF).get(0);
			if (metodoPpf != null) {
				/*
				 * if (cveMetodoPPF % 2 == 0) { cantidad =
				 * metodoPpf.getCanMaxSubsecuente(); } else { cantidad =
				 * metodoPpf.getCanMax1eraVez(); }
				 */
				cantidad = metodoPpf.getCanMaxSubsecuente();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cantidad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService#
	 * buscarDiagMorfologia(java.lang.String)
	 */
	@Override
	public String buscarDiagMorfologia(String cveDiagnostico) {		
		String desDiagnostico = null;
		try {
//			if (listCie10Morfologia.size() > 0) {
//				desDiagnostico = listCie10Morfologia.get(0)
//						.getDesCie10Morfologia();
//			}
			desDiagnostico = sicCie10Dao.buscarPorRangoPorId(cveDiagnostico.toUpperCase(), "C00", "D48X").getDesCie10();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return desDiagnostico;
	}

	@Override
	public SicCie10Morfologia buscarMorfologiaPorId(String cveMorfologia) {
		return sicCie10MorfologiaDao.buscarPorId(cveMorfologia);
	}

	@Override
	public Boolean isTumor(String cveDiagnostico) {
		if (null != sicCie10Dao.buscarPorRangoPorId(
				cveDiagnostico.toUpperCase(), "C00", "D48X")) {
			return true;
		} else
			return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService#
	 * buscarAtenEdicion
	 * (mx.gob.imss.simo.consultaexterna.dto.ConsultaExternaDto)
	 */
	@Override
	public ConsultaExternaDto buscarAtenEdicion(ConsultaExternaDto consulExtDto) {
		ConsultaExternaDto oDto = null;
		try {
			SitConsultaExterna consultaExterna = consultaExternaDao.buscar(
					consulExtDto.getFecha(), consulExtDto.getMatriculaMedico(),
					consulExtDto.getEspecialidad(),
					Integer.parseInt(consulExtDto.getTurno()));

			if (consultaExterna != null) {
				// TODO CARGAR LOS VALORES DEL ENCABEZADO CONTRA EL DTO
				oDto = (ConsultaExternaDto) AssemblerDtoEntity
						.convertirEntidadToDto(consultaExterna,
								ConsultaExternaDto.class);
				// Encabezado
				oDto.setId(consultaExterna.getId().toString());
				oDto.setEspecialidadSelc(consultaExterna.getSdEncabezado()
						.get(0).getSdEspecialidad().get(0).getNumTipoServicio()
						.toString());
				oDto.setCveEspecialidad(consultaExterna.getSdEncabezado()
						.get(0).getSdEspecialidad().get(0).getCveEspecialidad());
				oDto.setDesEspecialidad(consultaExterna
						.getSdEncabezado()
						.get(0)
						.getSdEspecialidad()
						.get(0)
						.getCveEspecialidad()
						.toString()
						.concat(" ")
						.concat(consultaExterna.getSdEncabezado().get(0)
								.getSdEspecialidad().get(0)
								.getDesEspecialidad()));
				oDto.setTipoPrestador(consultaExterna.getSdEncabezado().get(0)
						.getSdTipoPrestador().get(0).getCveTipoPrestador()
						.toString());
				oDto.setDesTipoPrestador(consultaExterna.getSdEncabezado()
						.get(0).getSdTipoPrestador().get(0)
						.getDesTipoPrestador());
				oDto.setMatriculaMedico(consultaExterna.getSdEncabezado()
						.get(0).getSdMedico().get(0).getCveMatricula());
				if (consultaExterna.getSdEncabezado().get(0).getSdMedico()
						.get(0).getRefNombre() != null ) {				
					if(consultaExterna.getSdEncabezado().get(0).getSdMedico()
							.get(0).getRefApellidoMaterno() != null){
						oDto.setNombreMedico(consultaExterna
								.getSdEncabezado()
								.get(0)
								.getSdMedico()
								.get(0)
								.getRefNombre()
								.concat(" ")
								.concat(consultaExterna.getSdEncabezado().get(0)
										.getSdMedico().get(0)
										.getRefApellidoPaterno())
								.concat(" ")
								.concat(consultaExterna.getSdEncabezado().get(0)
										.getSdMedico().get(0)
										.getRefApellidoMaterno()));
					}else{
							oDto.setNombreMedico(consultaExterna
									.getSdEncabezado()
									.get(0)
									.getSdMedico()
									.get(0)
									.getRefNombre()
									.concat(" ")
									.concat(consultaExterna.getSdEncabezado().get(0)
											.getSdMedico().get(0)
											.getRefApellidoPaterno()));
					}
				}
				oDto.setTurno(consultaExterna.getSdEncabezado().get(0)
						.getSdTurno().get(0).getCveTurno().toString());
				oDto.setDesTurno(consultaExterna.getSdEncabezado().get(0)
						.getSdTurno().get(0).getDesTurno());

				oDto.setConsultar(consulExtDto.getConsultar());
				oDto.setEditable(consulExtDto.getEditable());
				if (consultaExterna.getSdEncabezado().get(0).getSdFormato()
						.size() > 0) {
					for (SdFormato atencion : consultaExterna.getSdEncabezado()
							.get(0).getSdFormato()) {
						if (atencion.getNumConsecutivo().equals(
								consulExtDto.getNumConsulta())) {
							// Paciente
							oDto.setNumConsulta(atencion.getNumConsecutivo());
							oDto.setNss(atencion.getSdPaciente().get(0)
									.getNumNss());
							oDto.setAgregadoMedico(atencion.getSdPaciente()
									.get(0).getRefAgregadoMedico());
							oDto.setNombre(atencion.getSdPaciente().get(0)
									.getRefNombre());
							oDto.setApePaterno(atencion.getSdPaciente().get(0)
									.getRefApellidoPaterno());
							oDto.setApeMaterno(atencion.getSdPaciente().get(0)
									.getRefApellidoMaterno());
							oDto.setFecNacimiento(atencion.getSdPaciente()
									.get(0).getFecNacimiento());
							oDto.setEdad(atencion.getSdPaciente().get(0)
									.getNumEdad());
							if (atencion.getSdPaciente().get(0)
									.getSdUnidadAdscripcion().size() > 0) {
								oDto.setUnidadAdscripcion(atencion
										.getSdPaciente().get(0)
										.getSdUnidadAdscripcion().get(0)
										.getCvePresupuestal());
								oDto.setNombreUnidadMed(atencion
										.getSdPaciente().get(0)
										.getSdUnidadAdscripcion().get(0)
										.getDesUnidadMedica());
							}
							// Atencion
							oDto.setStpCaptura(atencion.getStpCaptura());
							oDto.setCitado(atencion.getIndCitado());
							oDto.setPrimeraVez(atencion.getInd1EraVez());
							oDto.setRecetas(atencion.getNumRecetas());
							oDto.setDiasIncapacidad(atencion
									.getNumDiasIncapacidad());
							oDto.setVisitas(atencion.getIndVisita());
							if (atencion.getSdTipoUrgencia().size() > 0) {
								oDto.setCveTipoUrgencia(atencion
										.getSdTipoUrgencia().get(0)
										.getCveTipoUrgencia());
							}
							if (atencion.getSdPaseServicio().size() > 0) {
								oDto.setPase(atencion.getSdPaseServicio()
										.get(0).getCvePaseServicio());
							}
							if (atencion.getSdMetodoAnticonceptivo().size() > 0) {
								oDto.setMetodoPpf(atencion
										.getSdMetodoAnticonceptivo().get(0)
										.getCveMetodoAnticonceptivo());
								if (atencion.getSdMetodoAnticonceptivo().get(0)
										.getCanMetodoAnticonceptivo() != null) {
									if (atencion.getSdMetodoAnticonceptivo()
											.get(0)
											.getCanMetodoAnticonceptivo() > 0) {
										oDto.setCantidadMpf(atencion
												.getSdMetodoAnticonceptivo()
												.get(0)
												.getCanMetodoAnticonceptivo());
									}
								}
							}
							if (atencion.getSdRiesgoTrabajo().size() > 0) {
								oDto.setRiesgosTrabajo(atencion
										.getSdRiesgoTrabajo().get(0)
										.getCveRiesgoTrabajo());
							}
							// Diagnosticos
							if (atencion.getSdDiagnosticos().size() > 0) {
								cargaDiagnosticosDto(oDto, atencion);
							}
							// Procedimientos
							if (atencion.getSdProcedimientos().size() > 0) {
								cargaProcedimientosDto(oDto, atencion);
							}

							// rama aseguramiento
							oDto.setCveRamaAseguramiento(atencion
									.getSdRamaAseguramiento()
									.getCveRamaAseguramiento());
							oDto.setDescRamaAseguramiento(atencion
									.getSdRamaAseguramiento()
									.getDesRamaAseguramiento());
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return oDto;
	}

	private void cargaDiagnosticosDto(ConsultaExternaDto consulExtDto,
			SdFormato atencion) {
		try {
			if (atencion.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal()
					.size() > 0) {
				consulExtDto.setDiagnosticoPrincipal(atencion
						.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal()
						.get(0).getCveCie10());
				consulExtDto.setDesDiagPrin(atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoPrincipal().get(0).getDesCie10());

				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoPrincipal().get(0)
						.getSdInformacionAdicional().size() > 0) {
					consulExtDto.setCveInforAdicPrin(atencion
							.getSdDiagnosticos().get(0)
							.getSdDiagnosticoPrincipal().get(0)
							.getSdInformacionAdicional().get(0)
							.getCveInformacionAdicional());
					consulExtDto.setCveTipoInfoAdDiagPrin(atencion
							.getSdDiagnosticos().get(0)
							.getSdDiagnosticoPrincipal().get(0)
							.getSdInformacionAdicional().get(0)
							.getCveTipoInformacionAdicional());
				}
				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoPrincipal().get(0).getSdTipoDiarrea()
						.size() > 0) {
					consulExtDto.setCveTipoPrin(atencion.getSdDiagnosticos()
							.get(0).getSdDiagnosticoPrincipal().get(0)
							.getSdTipoDiarrea().get(0).getCveTipoDiarrea());
				}
				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoPrincipal().get(0).getFecCirugia() != null) {
					consulExtDto.setFecCirugiaPrin(atencion.getSdDiagnosticos()
							.get(0).getSdDiagnosticoPrincipal().get(0)
							.getFecCirugia());
				}
			}
			if (atencion.getSdDiagnosticos().get(0)
					.getSdDiagnosticoComplementario().size() > 0) {
				consulExtDto.setDiagnosticoComplemento(atencion
						.getSdDiagnosticos().get(0)
						.getSdDiagnosticoComplementario().get(0).getCveCie10());
				consulExtDto.setDesDiagComp(atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoComplementario().get(0).getDesCie10());
				consulExtDto.setPrimeraVezDiagComp(atencion.getSdDiagnosticos()
						.get(0).getSdDiagnosticoComplementario().get(0)
						.getInd1EraVez());

				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoComplementario().get(0)
						.getSdInformacionAdicional().size() > 0) {
					consulExtDto.setCveInforAdicComp(atencion
							.getSdDiagnosticos().get(0)
							.getSdDiagnosticoComplementario().get(0)
							.getSdInformacionAdicional().get(0)
							.getCveInformacionAdicional());
					consulExtDto.setCveTipoInfoAdDiagCompl(atencion
							.getSdDiagnosticos().get(0)
							.getSdDiagnosticoComplementario().get(0)
							.getSdInformacionAdicional().get(0)
							.getCveTipoInformacionAdicional());
				}
				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoComplementario().get(0)
						.getSdTipoDiarrea().size() > 0) {
					consulExtDto.setCveTipoComp(atencion.getSdDiagnosticos()
							.get(0).getSdDiagnosticoComplementario().get(0)
							.getSdTipoDiarrea().get(0).getCveTipoDiarrea());
				}
				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoComplementario().get(0)
						.getFecCirugia() != null) {
					consulExtDto.setFecCirugiaComp(atencion.getSdDiagnosticos()
							.get(0).getSdDiagnosticoComplementario().get(0)
							.getFecCirugia());
				}
				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoComplementario().get(0)
						.getSdAccidentesLesiones().size() > 0) {
					consulExtDto.setAccidentesLesiones(atencion
							.getSdDiagnosticos().get(0)
							.getSdDiagnosticoComplementario().get(0)
							.getSdAccidentesLesiones().get(0)
							.getCveAccidenteLesion());
				}
			}
			if (atencion.getSdDiagnosticos().get(0).getSdDiagnosticoAdicional()
					.size() > 0) {
				consulExtDto.setDiagnosticoAdicional(atencion
						.getSdDiagnosticos().get(0).getSdDiagnosticoAdicional()
						.get(0).getCveCie10());
				consulExtDto.setDesDiagAdic(atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoAdicional().get(0).getDesCie10());
				consulExtDto.setPrimeraVezDiagAdic(atencion.getSdDiagnosticos()
						.get(0).getSdDiagnosticoAdicional().get(0)
						.getInd1EraVez());

				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoAdicional().get(0)
						.getSdInformacionAdicional().size() > 0) {
					consulExtDto.setCveInforAdicAdic(atencion
							.getSdDiagnosticos().get(0)
							.getSdDiagnosticoAdicional().get(0)
							.getSdInformacionAdicional().get(0)
							.getCveInformacionAdicional());
					consulExtDto.setCveTipoInfoAdDiagAdic(atencion
							.getSdDiagnosticos().get(0)
							.getSdDiagnosticoAdicional().get(0)
							.getSdInformacionAdicional().get(0)
							.getCveTipoInformacionAdicional());
				}
				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoAdicional().get(0).getSdTipoDiarrea()
						.size() > 0) {
					consulExtDto.setCveTipoAdic(atencion.getSdDiagnosticos()
							.get(0).getSdDiagnosticoAdicional().get(0)
							.getSdTipoDiarrea().get(0).getCveTipoDiarrea());
				}
				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoAdicional().get(0).getFecCirugia() != null) {
					consulExtDto.setFecCirugiaAdic(atencion.getSdDiagnosticos()
							.get(0).getSdDiagnosticoAdicional().get(0)
							.getFecCirugia());
				}
				if (atencion.getSdDiagnosticos().get(0)
						.getSdDiagnosticoAdicional().get(0)
						.getSdAccidentesLesiones().size() > 0) {
					consulExtDto.setAccidentesLesiones(atencion
							.getSdDiagnosticos().get(0)
							.getSdDiagnosticoAdicional().get(0)
							.getSdAccidentesLesiones().get(0)
							.getCveAccidenteLesion());
				}
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
	}

	private void cargaProcedimientosDto(ConsultaExternaDto consulExtDto,
			SdFormato atencion) {
		try {
			if (atencion.getSdProcedimientos().get(0)
					.getSdProcedimientoPrincipal().size() > 0) {
				consulExtDto.setProcedimientoPrincipal(atencion
						.getSdProcedimientos().get(0)
						.getSdProcedimientoPrincipal().get(0).getCveCie9mc());
				consulExtDto.setDesProcPrin(atencion.getSdProcedimientos()
						.get(0).getSdProcedimientoPrincipal().get(0)
						.getDesCie9mc());
			}
			if (atencion.getSdProcedimientos().get(0)
					.getSdProcedimientoComplementario().size() > 0) {
				consulExtDto.setProcedimientoComplemento(atencion
						.getSdProcedimientos().get(0)
						.getSdProcedimientoComplementario().get(0)
						.getCveCie9mc());
				consulExtDto.setDesProcComp(atencion.getSdProcedimientos()
						.get(0).getSdProcedimientoComplementario().get(0)
						.getDesCie9mc());
			}
			if (atencion.getSdProcedimientos().get(0)
					.getSdProcedimientoAdicional().size() > 0) {
				consulExtDto.setProcedimientoAdicional(atencion
						.getSdProcedimientos().get(0)
						.getSdProcedimientoAdicional().get(0).getCveCie9mc());
				consulExtDto.setDesProcAdic(atencion.getSdProcedimientos()
						.get(0).getSdProcedimientoAdicional().get(0)
						.getDesCie9mc());
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService#validaPeriodo
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public boolean validaPeriodo(Integer numAnioPeriodo, String numMesPeriodo,
			Date fechaCap, String cvePresupuestal, Integer fechaDia) {
		boolean resultado = false;
		Integer diaFinalPeriodo = 25;
		Integer mesDiciembre = 12;
		Integer mesInt = Integer.parseInt(numMesPeriodo);
		if(fechaDia > diaFinalPeriodo){
			if(mesInt == mesDiciembre){
				mesInt = 1;
				numAnioPeriodo += 1;
			} else {
				mesInt += 1;
			}
			numMesPeriodo = String.valueOf(mesInt);
		}
		while (numMesPeriodo.length() < 2) {
			String cadAux = "0";
			cadAux = cadAux.concat(numMesPeriodo);
			numMesPeriodo = cadAux;
		}
		try {
						
			String periodoOperacion = String.valueOf(numAnioPeriodo).concat(numMesPeriodo);
			
			SicPeriodosImss periodo = sicPeriodosImssDao.burcarPorPeriodo(
					numAnioPeriodo, numMesPeriodo);

			if (periodo != null) {
					SitPeriodoOperacion sitPeriodoOperacion = sicPeriodosImssDao
							.burcarPorPeriodoOperacion(Integer.parseInt(periodoOperacion), cvePresupuestal);
						if(sitPeriodoOperacion == null || sitPeriodoOperacion.getIndCierre() == 0){
							resultado = true;
						}
			}
		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return resultado;
	}

	private static double calculaTiempo(Date fechaAtencion, int tiempoSelec) {
		double tiempo = 0;
		Calendar fechaReg = Calendar.getInstance();
		Calendar fechaActual = Calendar.getInstance();
		fechaReg.setTime(fechaAtencion);

		if (tiempoSelec == 1) { // Semanas
			double diff = fechaActual.getTime().getTime()
					- fechaReg.getTime().getTime();
			double dias = diff / 86400000l;
			tiempo = (double) (dias / 7);
		} else if (tiempoSelec == 2) {// Meses
			double yy = fechaReg.get(Calendar.YEAR);
			double mm = fechaReg.get(Calendar.MONTH) + 1;
			double dd = fechaReg.get(Calendar.DATE);

			double gdate = fechaActual.get(Calendar.DATE);
			double gmonth = fechaActual.get(Calendar.MONTH) + 1;
			double gyear = fechaActual.get(Calendar.YEAR);

			double age = gyear - yy;

			if (mm > gmonth && age > 0) {
				age = age - 1;
				tiempo = 12;
			}

			tiempo += age * 12;

			if (gdate < dd) {
				gmonth--;
			}

			tiempo += gmonth - mm;
		} else if (tiempoSelec == 3) {// Anos
			int yy = fechaReg.get(Calendar.YEAR);
			int mm = fechaReg.get(Calendar.MONTH) + 1;

			int gmonth = fechaActual.get(Calendar.MONTH) + 1;
			int gyear = fechaActual.get(Calendar.YEAR);

			tiempo = gyear - yy;

			if (mm > gmonth && tiempo > 0) {
				tiempo = tiempo - 1;
			}
		}
		return tiempo;
	}
	
	@Override
	public void actualizarControlCapturaCe(Boolean consutaExternaUr, SitConsultaExterna consultaExterna) {
		if (consutaExternaUr) {
			//AQUI se debe de insertar en la clase de control
			SitControlCapturaCe controlCapturaCE=new SitControlCapturaCe();
			SdUnidadMedica unidad=new SdUnidadMedica();
			unidad.setCvePresupuestal(consultaExterna.getSdUnidadMedica().get(0).getCvePresupuestal());
			unidad.setDesUnidadMedica(consultaExterna.getSdUnidadMedica().get(0).getDesUnidadMedica());
			controlCapturaCE.setSdUnidadMedica(unidad);
			SicPeriodosImss periodo=consultaExternaDao.obtenerPeriodImssPorFecha(consultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado());
			controlCapturaCE.setFecActualizaControl(new Date());
			controlCapturaCE.setIndControl(1);
			controlCapturaCE.setCvePeriodo(periodo.getCvePeriodo());
			consultaExternaDao.actualizarControlCapturaCE(controlCapturaCE);
			
	    }
	}
	
	public String buscarPeriodoLiberacion(String cveConstante) {		
		String refValor = null;
		try {
			refValor = consultaExternaDao.obtenerUrlConstante(cveConstante);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return refValor;
	}

	@Override
	public SicEspecialidad buscarEspecialidad(String cveEspecialidad) {
		return sicEspecialidadDao.buscarPorId(cveEspecialidad);
	}

	/**
	 * @return the consultaExternaDao
	 */
	public ConsultaExternaDao getConsultaExternaDao() {
		return consultaExternaDao;
	}

	/**
	 * @param consultaExternaDao
	 *            the consultaExternaDao to set
	 */
	public void setConsultaExternaDao(ConsultaExternaDao consultaExternaDao) {
		this.consultaExternaDao = consultaExternaDao;
	}

	/**
	 * @return the sicEspecialidadUnidadDao
	 */
	public SicEspecialidadUnidadDao getSicEspecialidadUnidadDao() {
		return sicEspecialidadUnidadDao;
	}

	/**
	 * @param sicEspecialidadUnidadDao
	 *            the sicEspecialidadUnidadDao to set
	 */
	public void setSicEspecialidadUnidadDao(
			SicEspecialidadUnidadDao sicEspecialidadUnidadDao) {
		this.sicEspecialidadUnidadDao = sicEspecialidadUnidadDao;
	}

	/**
	 * @return the sicConsultorioDao
	 */
	public SicConsultorioDao getSicConsultorioDao() {
		return sicConsultorioDao;
	}

	/**
	 * @param sicConsultorioDao
	 *            the sicConsultorioDao to set
	 */
	public void setSicConsultorioDao(SicConsultorioDao sicConsultorioDao) {
		this.sicConsultorioDao = sicConsultorioDao;
	}

	/**
	 * @return the sicTipoPrestadorDao
	 */
	public SicTipoPrestadorDao getSicTipoPrestadorDao() {
		return sicTipoPrestadorDao;
	}

	/**
	 * @param sicTipoPrestadorDao
	 *            the sicTipoPrestadorDao to set
	 */
	public void setSicTipoPrestadorDao(SicTipoPrestadorDao sicTipoPrestadorDao) {
		this.sicTipoPrestadorDao = sicTipoPrestadorDao;
	}

	/**
	 * @return the sicTurnoDao
	 */
	public SicTurnoDao getSicTurnoDao() {
		return sicTurnoDao;
	}

	/**
	 * @param sicTurnoDao
	 *            the sicTurnoDao to set
	 */
	public void setSicTurnoDao(SicTurnoDao sicTurnoDao) {
		this.sicTurnoDao = sicTurnoDao;
	}

	/**
	 * @return the SicPaseServicioDao
	 */
	public SicPaseServicioDao getSicPaseServicioDao() {
		return SicPaseServicioDao;
	}

	/**
	 * @param SicPaseServicioDao
	 *            the SicPaseServicioDao to set
	 */
	public void setSicPaseServicioDao(SicPaseServicioDao SicPaseServicioDao) {
		this.SicPaseServicioDao = SicPaseServicioDao;
	}

	/**
	 * @return the SicMetodoAnticonceptivoDao
	 */
	public SicMetodoAnticonceptivoDao getSicMetodoAnticonceptivoDao() {
		return sicMetodoAnticonceptivoDao;
	}

	/**
	 * @param SicMetodoAnticonceptivoDao
	 *            the SicMetodoAnticonceptivoDao to set
	 */
	public void setSicMetodoAnticonceptivoDao(
			SicMetodoAnticonceptivoDao sicMetodoAnticonceptivoDao) {
		this.sicMetodoAnticonceptivoDao = sicMetodoAnticonceptivoDao;
	}

	/**
	 * @return the sicAccidentesLesionesDao
	 */
	public SicAccidentesLesionesDao getSicAccidentesLesionesDao() {
		return sicAccidentesLesionesDao;
	}

	/**
	 * @param sicAccidentesLesionesDao
	 *            the sicAccidentesLesionesDao to set
	 */
	public void setSicAccidentesLesionesDao(
			SicAccidentesLesionesDao sicAccidentesLesionesDao) {
		this.sicAccidentesLesionesDao = sicAccidentesLesionesDao;
	}

	/**
	 * @return the sicRiesgosTrabajoDao
	 */
	public SicRiesgosTrabajoDao getSicRiesgosTrabajoDao() {
		return sicRiesgosTrabajoDao;
	}

	/**
	 * @param sicRiesgosTrabajoDao
	 *            the sicRiesgosTrabajoDao to set
	 */
	public void setSicRiesgosTrabajoDao(
			SicRiesgosTrabajoDao sicRiesgosTrabajoDao) {
		this.sicRiesgosTrabajoDao = sicRiesgosTrabajoDao;
	}

	/**
	 * @return the sitPacienteDao
	 */
	public SitPacienteDao getSitPacienteDao() {
		return sitPacienteDao;
	}

	/**
	 * @param sitPacienteDao
	 *            the sitPacienteDao to set
	 */
	public void setSitPacienteDao(SitPacienteDao sitPacienteDao) {
		this.sitPacienteDao = sitPacienteDao;
	}

	/**
	 * @return the SicTipoUrgenciaDao
	 */
	public SicTipoUrgenciaDao getSicTipoUrgenciaDao() {
		return SicTipoUrgenciaDao;
	}

	/**
	 * @param SicTipoUrgenciaDao
	 *            the SicTipoUrgenciaDao to set
	 */
	public void setSicTipoUrgenciaDao(SicTipoUrgenciaDao SicTipoUrgenciaDao) {
		this.SicTipoUrgenciaDao = SicTipoUrgenciaDao;
	}

	/**
	 * @return the sicCie10Dao
	 */
	public SicCie10Dao getSicCie10Dao() {
		return sicCie10Dao;
	}

	/**
	 * @param sicCie10Dao
	 *            the sicCie10Dao to set
	 */
	public void setSicCie10Dao(SicCie10Dao sicCie10Dao) {
		this.sicCie10Dao = sicCie10Dao;
	}

	/**
	 * @return the SicCie9Dao
	 */
	public SicCie9Dao getSicCie9Dao() {
		return SicCie9Dao;
	}

	/**
	 * @param SicCie9Dao
	 *            the SicCie9Dao to set
	 */
	public void setSicCie9Dao(SicCie9Dao SicCie9Dao) {
		this.SicCie9Dao = SicCie9Dao;
	}

	/**
	 * @return the sicMedicoDao
	 */
	public SicMedicoDao getSicMedicoDao() {
		return sicMedicoDao;
	}

	/**
	 * @param sicMedicoDao
	 *            the sicMedicoDao to set
	 */
	public void setSicMedicoDao(SicMedicoDao sicMedicoDao) {
		this.sicMedicoDao = sicMedicoDao;
	}

	/**
	 * @return the sicUnidadAdscripcionDao
	 */
	public SicUnidadAdscripcionDao getSicUnidadAdscripcionDao() {
		return sicUnidadAdscripcionDao;
	}

	/**
	 * @param sicUnidadAdscripcionDao
	 *            the sicUnidadAdscripcionDao to set
	 */
	public void setSicUnidadAdscripcionDao(
			SicUnidadAdscripcionDao sicUnidadAdscripcionDao) {
		this.sicUnidadAdscripcionDao = sicUnidadAdscripcionDao;
	}

	/**
	 * @return the sicDelegacionImssDao
	 */
	public SicDelegacionImssDao getSicDelegacionImssDao() {
		return sicDelegacionImssDao;
	}

	/**
	 * @param sicDelegacionImssDao
	 *            the sicDelegacionImssDao to set
	 */
	public void setSicDelegacionImssDao(
			SicDelegacionImssDao sicDelegacionImssDao) {
		this.sicDelegacionImssDao = sicDelegacionImssDao;
	}

	/**
	 * @return the sicInformacionAdicionalDao
	 */
	public SicInformacionAdicionalDao getSicInformacionAdicionalDao() {
		return sicInformacionAdicionalDao;
	}

	/**
	 * @param sicInformacionAdicionalDao
	 *            the sicInformacionAdicionalDao to set
	 */
	public void setSicInformacionAdicionalDao(
			SicInformacionAdicionalDao sicInformacionAdicionalDao) {
		this.sicInformacionAdicionalDao = sicInformacionAdicionalDao;
	}

	/**
	 * @return the sicLugarAtencionDao
	 */
	public SicLugarAtencionDao getSicLugarAtencionDao() {
		return sicLugarAtencionDao;
	}

	/**
	 * @param sicLugarAtencionDao
	 *            the sicLugarAtencionDao to set
	 */
	public void setSicLugarAtencionDao(SicLugarAtencionDao sicLugarAtencionDao) {
		this.sicLugarAtencionDao = sicLugarAtencionDao;
	}

	/**
	 * @return the sicTipoDiarreaDao
	 */
	public SicTipoDiarreaDao getSicTipoDiarreaDao() {
		return sicTipoDiarreaDao;
	}

	/**
	 * @param sicTipoDiarreaDao
	 *            the sicTipoDiarreaDao to set
	 */
	public void setSicTipoDiarreaDao(SicTipoDiarreaDao sicTipoDiarreaDao) {
		this.sicTipoDiarreaDao = sicTipoDiarreaDao;
	}

	/**
	 * @return the sicCie10MorfologiaDao
	 */
	public SicCie10MorfologiaDao getSicCie10MorfologiaDao() {
		return sicCie10MorfologiaDao;
	}

	/**
	 * @param sicCie10MorfologiaDao
	 *            the sicCie10MorfologiaDao to set
	 */
	public void setSicCie10MorfologiaDao(
			SicCie10MorfologiaDao sicCie10MorfologiaDao) {
		this.sicCie10MorfologiaDao = sicCie10MorfologiaDao;
	}

	/**
	 * @return the sicPeriodosImssDao
	 */
	public SicPeriodosImssDao getSicPeriodosImssDao() {
		return sicPeriodosImssDao;
	}

	/**
	 * @param sicPeriodosImssDao
	 *            the sicPeriodosImssDao to set
	 */
	public void setSicPeriodosImssDao(SicPeriodosImssDao sicPeriodosImssDao) {
		this.sicPeriodosImssDao = sicPeriodosImssDao;
	}

	public SicEspecialidadDao getSicEspecialidadDao() {
		return sicEspecialidadDao;
	}

	public void setSicEspecialidadDao(SicEspecialidadDao sicEspecialidadDao) {
		this.sicEspecialidadDao = sicEspecialidadDao;
	}

	public SicInformacionAdicionalVIHDao getSicInformacionAdicionalVIHDao() {
		return sicInformacionAdicionalVIHDao;
	}

	public void setSicInformacionAdicionalVIHDao(
			SicInformacionAdicionalVIHDao sicInformacionAdicionalVIHDao) {
		this.sicInformacionAdicionalVIHDao = sicInformacionAdicionalVIHDao;
	}

	/**
	 * metodo para obtener todos los valores de la coleccion
	 * SIC_INFORMACION_ADICIONAL_VIH
	 */
	public List<SicInformacionAdicionalVIH> buscarTodosInfAdiVIH() {
		return sicInformacionAdicionalVIHDao.burcarTodos();
	}

	public SicInformacionAdicionalEmbarazoDao getSicInformacionAdicionalEmbarazoDao() {
		return sicInformacionAdicionalEmbarazoDao;
	}

	public void setSicInformacionAdicionalEmbarazoDao(
			SicInformacionAdicionalEmbarazoDao sicInformacionAdicionalEmbarazoDao) {
		this.sicInformacionAdicionalEmbarazoDao = sicInformacionAdicionalEmbarazoDao;
	}


	public Boolean buscarAtencionRepetida(String cveNss, String cveAgMedico, String cveDxP, String idCE) {
		return consultaExternaDao.buscarAtencionRepetida(cveNss, cveAgMedico, cveDxP, idCE);
	}
	
	@Override
	public void insertar(ControlMedicoEncabezadoDto controlMedicoDto) {
		// TODO Auto-generated method stub
		try {
			SitControlMedicoEncabezado sitControlMedico = new SitControlMedicoEncabezado();
			sitControlMedico.setCveMatricula(controlMedicoDto.getCveMatricula());
			sitControlMedico.setNumIntento(controlMedicoDto.getNumIntento());
			
			SdUnidadMedica unidadMedica= new SdUnidadMedica(controlMedicoDto.getCvePresupuestal(), controlMedicoDto.getDesUnidadMedica());
			List<SdUnidadMedica> sdUnidadMedicalist = new ArrayList<SdUnidadMedica>(); // Se genera el array list
			sdUnidadMedicalist.add(unidadMedica);
			sitControlMedico.setSdUnidadMedica(sdUnidadMedicalist);
			controlMedicoEncabezadoDao.insertar(sitControlMedico);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Boolean actualizar(ControlMedicoEncabezadoDto controlMedicoDto) {
		try {
			SitControlMedicoEncabezado sitControlMedicoActualizar= new SitControlMedicoEncabezado();
			sitControlMedicoActualizar.setCveMatricula(controlMedicoDto.getCveMatricula());
			sitControlMedicoActualizar.setNumIntento(controlMedicoDto.getNumIntento());
			
			SdUnidadMedica unidadMedicaActualiza= new SdUnidadMedica(controlMedicoDto.getCvePresupuestal(), controlMedicoDto.getDesUnidadMedica());
			List<SdUnidadMedica> sdUnidadMedicalist = new ArrayList<SdUnidadMedica>(); // Se genera el array list
			sdUnidadMedicalist.add(unidadMedicaActualiza);
			sitControlMedicoActualizar.setSdUnidadMedica(sdUnidadMedicalist);
			controlMedicoEncabezadoDao.actualizar(sitControlMedicoActualizar);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public ControlMedicoEncabezadoDto buscar(String matriculaMedico) {
		ControlMedicoEncabezadoDto controlDto=null;
		try {
			SdUnidadMedica unidadMedica= new SdUnidadMedica();
			SitControlMedicoEncabezado sitControlMedicoBusqueda=controlMedicoEncabezadoDao.buscar(matriculaMedico);
			unidadMedica=sitControlMedicoBusqueda.getSdUnidadMedica().get(0);
			String cvePresupuestal=unidadMedica.getCvePresupuestal();
			String nombreUnidad=unidadMedica.getDesUnidadMedica();
			controlDto=new ControlMedicoEncabezadoDto(
					cvePresupuestal, 
					nombreUnidad, 
					sitControlMedicoBusqueda.getCveMatricula(), 
					sitControlMedicoBusqueda.getNumIntento());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return controlDto;
	}
	@Override	
	public Boolean actualizaNombreMedico(ConsultaExternaDto consultActualizaDto) {
		Boolean respuesta;
		try {
			ObjectId objId = new ObjectId(consultActualizaDto.getId());
			SitConsultaExterna sitConsultaExterna = new SitConsultaExterna();
			sitConsultaExterna.setId(objId);
			sitConsultaExterna.setSdEncabezado(consultActualizaDto.getSdEncabezado());

			respuesta=consultaExternaDao.actualizaNombreMedico(sitConsultaExterna);
			if(respuesta) {
				SicPeriodosImss periodo = consultaExternaDao.obtenerPeriodImssPorFecha(sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado());
				SdUnidadMedica unidad=new SdUnidadMedica();
				SitControlCapturaCe sitControlCapturaCE= new SitControlCapturaCe();
				
				unidad.setCvePresupuestal(consultActualizaDto.getSdUnidadMedica().get(0).getCvePresupuestal());
				unidad.setDesUnidadMedica(consultActualizaDto.getSdUnidadMedica().get(0).getDesUnidadMedica());
				sitControlCapturaCE.setSdUnidadMedica(unidad);
				
				sitControlCapturaCE.setFecActualizaControl(new Date());
				sitControlCapturaCE.setIndControl(1);
				sitControlCapturaCE.setCvePeriodo(periodo.getCvePeriodo());
				consultaExternaDao.actualizarControlCapturaCE(sitControlCapturaCE);
				
			}
			
			return Boolean.TRUE;
		} catch (Exception e) {
            e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	@Override
	public List<ConsultaExternaDto> buscarAtencionesParaActualiza(ConsultaExternaDto consulExtDto,
			UsuarioDetalleDto usuarioDetalleDto) {

		List<ConsultaExternaDto> listResul = new ArrayList<ConsultaExternaDto>();
		try {
			List<SitConsultaExterna> listConsultaExterna = consultaExternaDao
					.buscarAtencionesParaActualiza(consulExtDto.getFecha(),
							consulExtDto.getMatriculaMedico(),
							consulExtDto.getNss(),
							consulExtDto.getEspecialidad(),
							consulExtDto.getAgregadoMedico(),
							consulExtDto.getCodigoCie(),
							consulExtDto.getPrimeraVez(),
							usuarioDetalleDto.getCvePresupuestal());

			for (SitConsultaExterna sitConsultaExternaEnca : listConsultaExterna) {
				List<SdFormato> listSdFormato = sitConsultaExternaEnca.getSdEncabezado().get(0).getSdFormato();
				for (int i = 0; i < listSdFormato.size(); i++) {
					SdFormato sdFor = listSdFormato.get(i);
					boolean bandera = true;
					if (consulExtDto.getNss() != null) {
						if (!consulExtDto.getNss().equals(
								sdFor.getSdPaciente().get(0).getNumNss())) {
							bandera = false;
						}
					}
					if (consulExtDto.getAgregadoMedico() != null) {
						if (!consulExtDto.getAgregadoMedico().equals(
								sdFor.getSdPaciente().get(0)
										.getRefAgregadoMedico())) {
							bandera = false;
						}
					}
					if (consulExtDto.getCodigoCie() != null) {
						if (!consulExtDto.getCodigoCie().equals(
								sdFor.getSdDiagnosticos().get(0)
										.getSdDiagnosticoPrincipal().get(0)
										.getCveCie10())) {
							bandera = false;
						}
					}
					if (consulExtDto.getPrimeraVez() != null) {
						if (consulExtDto.getPrimeraVez() < 2) {
							if (!consulExtDto.getPrimeraVez().equals(
									sdFor.getInd1EraVez())) {
								bandera = false;
							}

						}
					}

					if (bandera) {
						ConsultaExternaDto oDto = (ConsultaExternaDto) AssemblerDtoEntity
								.convertirEntidadToDto(sdFor,
										ConsultaExternaDto.class);
						oDto.setEspecialidad(sitConsultaExternaEnca.getSdEncabezado().get(0).getSdEspecialidad().get(0)
								.getCveEspecialidad().toString());
						oDto.setDesEspecialidad(sitConsultaExternaEnca.getSdEncabezado().get(0).getSdEspecialidad().get(0)
								.getDesEspecialidad());
						oDto.setMatriculaMedico(sitConsultaExternaEnca.getSdEncabezado().get(0).getSdMedico().get(0)
								.getCveMatricula());
						oDto.setTurno(sitConsultaExternaEnca.getSdEncabezado().get(0).getSdTurno().get(0).getCveTurno()
								.toString());
						oDto.setFecha(sitConsultaExternaEnca.getSdEncabezado().get(0).getFecAltaEncabezado());
						oDto.setPrimeraVez(sitConsultaExternaEnca.getSdEncabezado().get(0).getSdFormato().get(0).getInd1EraVez());
						oDto.setId(sitConsultaExternaEnca.getId().toString());
						oDto.setSdEncabezado(sitConsultaExternaEnca.getSdEncabezado());
						oDto.setSdUnidadMedica(sitConsultaExternaEnca.getSdUnidadMedica());
						if (usuarioDetalleDto
								.tieneRole(EnumRoles.ROLE_ADMIN_CENTRAL
										.getDescripcion())) {
							oDto.setEditable(Boolean.TRUE);
							oDto.setConsultar(Boolean.FALSE);
						
						} else if (usuarioDetalleDto
								.tieneRole(EnumRoles.ROLE_ADMIN_DELEG
										.getDescripcion())) {
								oDto.setEditable(Boolean.TRUE);
								oDto.setConsultar(Boolean.FALSE);
						} else if (usuarioDetalleDto
								.tieneRole(EnumRoles.ROLE_ARIMAC_JEFE
										.getDescripcion())) {
								oDto.setEditable(Boolean.TRUE);
								oDto.setConsultar(Boolean.FALSE);
						} 
						listResul.add(oDto);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listResul;
		
		
		
	}
	
	


}
