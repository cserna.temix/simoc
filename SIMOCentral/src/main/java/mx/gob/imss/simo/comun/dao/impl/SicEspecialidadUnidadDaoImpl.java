/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicEspecialidadUnidadDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidadUnidad;

/**
 * @author francisco.rodriguez
 * 
 */
@Component("sicEspecialidadUnidadDao")
public class SicEspecialidadUnidadDaoImpl implements SicEspecialidadUnidadDao {
//
//	@Autowired
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.comun.dao.SicEspecialidadUnidadDao#burcarTodos()
	 */
	@Override
	public List<SicEspecialidadUnidad> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicEspecialidadUnidad> listEspecialidad = new ArrayList<SicEspecialidadUnidad>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listEspecialidad = dsEsp.createQuery(SicEspecialidadUnidad.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listEspecialidad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicEspecialidadUnidadDao#burcarPorId(java.
	 * lang.String)
	 */
	@Override
	public List<SicEspecialidadUnidad> burcarPorId(String cveEspecialidad) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicEspecialidadUnidad> listEspecialidad = new ArrayList<SicEspecialidadUnidad>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listEspecialidad = dsEsp.createQuery(SicEspecialidadUnidad.class)
					.field("cveEspecialidad").equal(cveEspecialidad).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listEspecialidad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicEspecialidadUnidadDao#burcarPorUnidadMed
	 * (java.lang.String)
	 */
	@Override
	public List<SicEspecialidadUnidad> burcarPorUnidadMed(String cvePresupuestal, String claveModuloAtencion) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicEspecialidadUnidad> listEspecialidad = new ArrayList<SicEspecialidadUnidad>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listEspecialidad = dsEsp.createQuery(SicEspecialidadUnidad.class)					
					.field("cvePresupuestal").equal(cvePresupuestal)
					.field(claveModuloAtencion).equal(1).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listEspecialidad;
	}

	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicEspecialidadUnidadDao#burcarPorIdUnidadMed(java.lang.String, java.lang.String)
	 */
	@Override
	public List<SicEspecialidadUnidad> burcarPorIdUnidadMed(
			String cveEspecialidad, String cvePresupuestal) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicEspecialidadUnidad> listEspecialidad = new ArrayList<SicEspecialidadUnidad>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listEspecialidad = dsEsp.createQuery(SicEspecialidadUnidad.class)
					.field("cveEspecialidad").equal(cveEspecialidad)
					.field("cvePresupuestal").equal(cvePresupuestal).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listEspecialidad;
	}

	@Override
	public List<SicEspecialidadUnidad> buscarPorClaveProsupuestal(String cvePresupuestal) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicEspecialidadUnidad> listEspecialidad = new ArrayList<SicEspecialidadUnidad>();
				try{
					Datastore datastore = MongoManager.INSTANCE.getDatastore();
							listEspecialidad = datastore.createQuery(SicEspecialidadUnidad.class).order("desEspecialidad")
									.field("cvePresupuestal").equal(cvePresupuestal).asList();
				}catch (Exception e){
					e.getMessage();
				}
		return listEspecialidad;
	}


}
