package mx.gob.imss.simo.consultaexterna.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import mx.gob.imss.simo.comun.dao.SicCie10VigilanciaEpiDao;
import mx.gob.imss.simo.comun.dao.SicEspecialidadDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicSemanaVigilaciaEpi;
import mx.gob.imss.simo.comun.entity.negocio.SitBitacoraEnvio;
import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;
import mx.gob.imss.simo.consultaexterna.dao.BitacoraEnvioDao;
import mx.gob.imss.simo.consultaexterna.dao.CatalogoSemanaVigilanciaEpiDao;
import mx.gob.imss.simo.consultaexterna.dao.ConsultaExternaDao;
import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdFormato;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;

@Service("generarDocumentoVigEpiServiceImpl")
public class GenerarDocumentoVigEpiServiceImpl {

	@Value("${ftp.server}")
	private String server;
	@Value("${ftp.port}")
	private int port;
	@Value("${ftp.user}")
	private String user;
	@Value("${ftp.password}")
	private String pass;
	@Value("${ruta.local.archivos}")
	private String rutaLocalArchivos;
	@Value("${ftp.timeout}")
	private int timeout;

	private static final Log logger = LogFactory.getLog(GenerarDocumentoVigEpiServiceImpl.class.getName());

	private final static int ESTATUS_CONSULTA_ENVIADA = 1;

	@Autowired
	private SicEspecialidadDao sicEspecialidadDao;
	@Autowired
	private SicCie10VigilanciaEpiDao cie10VigilanciaEpiDao;
	@Autowired
	private ConsultaExternaDao consultaExternaDao;
	@Autowired
	private BitacoraEnvioDao bitacoraEnvioDao;
	@Autowired
	private CatalogoSemanaVigilanciaEpiDao catalogoSemanaVigilanciaEpiDao;

	final String CLAVE = "CE_VIGEPI";
	final String ACTIVO = "ACTIVO";
	final String INACTIVO = "INACTIVO";

	public void generarArchivosUnidadesMedicas() {

		//logger.info("Vigilancia epidemiologica - Inicio del proceso");

		List<SitBitacoraEnvio> bitacora = bitacoraEnvioDao.consultarBitacora();

		//logger.info("Verificando si el proceso puede ejecutarse");

		if (consultaExternaDao.obtenerUrlConstante(CLAVE).equals(ACTIVO) && !validarFechaBitacora(bitacora)) {

			logger.info(
					"VigilanciaEp-No hay ejecuciones en progreso y tampoco hay ejecuciones concluidas en el dia por lo que se inicia la ejecucion");

			consultaExternaDao.actualizaConstantes(INACTIVO, CLAVE);

			logger.info("VigilanciaEp-Se bloquea la ejecucion para evitar otras simultaneas");

			List<String> listaEspecialidades = obtenerEspecialidades();

			List<String> catalogoEpidemiologico = obtenerCatalogoVigEpi();

			List<SicSemanaVigilaciaEpi> catalogoSemanaEpi = obtenerCatalogoSemanaEpi();

			List<String> unidades = this.consultaExternaDao
					.buscarUnidadesMedicasConConsultasExternas(listaEspecialidades, catalogoEpidemiologico);
		
			List<SitConsultaExterna> listaConsultasExternas;
			List<File> archivoOrigen;
			String resultadoOperacion;
			int resultadoEnvio = 0;

			for (final String unidad : unidades) {
				listaConsultasExternas = obtenerConsultasExternas(listaEspecialidades, catalogoEpidemiologico, unidad);

				logger.info(
						"VigilanciaEp-Total de consultas externas de la unidad: " + unidad + ": " + listaConsultasExternas.size());

				try {
					archivoOrigen = this.crearArchivos(listaConsultasExternas, unidad, catalogoSemanaEpi);

					resultadoEnvio = this.enviarArchivoSFTP(archivoOrigen);

					if (resultadoEnvio == 1) {
						actualizarConsultaExterna(listaConsultasExternas);

						resultadoOperacion = "";
					} else {
						resultadoOperacion = "El mensaje de la excepcion devuelta por el metodo enviarArchivoSFTP()";
					}
				} catch (IOException e) {
					resultadoOperacion = e.getMessage();
				} catch (JSchException e) {
					resultadoOperacion = e.getMessage();
				} catch (SftpException e) {
					resultadoOperacion = e.getMessage();
				}

				registrarBitacora(
						unidad, ((SdUnidadMedica) ((SitConsultaExterna) listaConsultasExternas.get(0))
								.getSdUnidadMedica().get(0)).getDesUnidadMedica(),
						listaConsultasExternas.size(), resultadoOperacion , resultadoEnvio);
			}

			logger.info("Se desbloquea la ejecucion");

			consultaExternaDao.actualizaConstantes(ACTIVO, CLAVE);
		} else {
			logger.info(
					"Hay ejecuciones en progreso o hay ejecuciones concluidas en el dia por lo que se impide la ejecucion del proceso");
		}

		logger.info("Vigilancia epidemiologica - Fin del proceso");
	}

	private void registrarBitacora(final String clavePresupuestalUnidadMedica, final String descripcionUnidadMedica,
			final int totalRegistrosProcesados, final String resultadoOperacion, final int resultadoEnvio) {
		//logger.info("Antes de registrar el evento en la bitacora");

		final SitBitacoraEnvio bitacoraEnvio = new SitBitacoraEnvio();

		bitacoraEnvio.setCvePresupuestal(clavePresupuestalUnidadMedica);
		bitacoraEnvio.setDesUnidadMedica(descripcionUnidadMedica);
		bitacoraEnvio.setStpCreaArchivo(new Date());
		bitacoraEnvio.setNumRegistros(totalRegistrosProcesados);
		bitacoraEnvio.setRefRespServidor(resultadoOperacion);
		bitacoraEnvio.setIndEnvio(resultadoEnvio);
		bitacoraEnvio.setStpEnvio(1 == resultadoEnvio ? new Date() : null);

		this.bitacoraEnvioDao.insertar(bitacoraEnvio);

		//logger.info("Evento registrado en la bitacora: " + bitacoraEnvio);
	}

	private boolean validarFechaBitacora(List<SitBitacoraEnvio> fechaBitacora) {
		Date fechaInicio = new Date();
		String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(fechaInicio);

		for (SitBitacoraEnvio sitBitacoraEnvio : fechaBitacora) {
			String fechaEnvio = new SimpleDateFormat("dd/MM/yyyy").format(sitBitacoraEnvio.getStpEnvio());
			if (fechaEnvio.equals(fechaActual)) {
				return true;
			}

		}

		return false;
	}

	private List<File> crearArchivos(List<SitConsultaExterna> listaConsultasExternas, String unidad,
			List<SicSemanaVigilaciaEpi> catalogoSemanaEpi) throws IOException {
		//logger.info("iniciando la creacion del archivo");
		// Escribir las consultas en sus respectivos archivos
		String extrarSexo;
		String sexo;
		File archivoEpi = null;
		List<File> archivos = new ArrayList<File>();
		FileWriter write;
		BufferedWriter bw = null;
		PrintWriter wr = null;
		String fechaAux = "";
		boolean flag = false;

		for (SitConsultaExterna sitConsultaExterna : listaConsultasExternas) {
			if (tieneFormato(sitConsultaExterna.getSdEncabezado())) {
				int cont = 0;
				for (SdFormato formato : sitConsultaExterna.getSdEncabezado().get(0).getSdFormato()) {

					String fechaCaptura = new SimpleDateFormat("ddMMyyyy").format(sitConsultaExterna.getFecCaptura());
					if (!fechaCaptura.equals(fechaAux)) {
						if (flag) {
							archivos.add(archivoEpi);
							wr.close();
							bw.close();
						}
						flag = true;
						fechaAux = fechaCaptura;
						 archivoEpi = new
						 File(System.getProperty("user.home")+System.getProperty("file.separator")+fechaAux+
						 unidad +2+".txt");
//						archivoEpi = new File("C:/epi/" + fechaAux + unidad + 2 + ".txt");
						write = new FileWriter(archivoEpi);
						bw = new BufferedWriter(write);
						wr = new PrintWriter(bw);
					}
					//logger.debug("       el id es : " + sitConsultaExterna.getId());
					if (sitConsultaExterna.getSdUnidadMedica().get(0).getCvePresupuestal() != null) {
						wr.write(sitConsultaExterna.getSdUnidadMedica().get(0).getCvePresupuestal() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (sitConsultaExterna.getSdUnidadMedica().get(0).getDesUnidadMedica() != null) {
						wr.write(sitConsultaExterna.getSdUnidadMedica().get(0).getDesUnidadMedica() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado().toString() != null) {
						wr.write(new SimpleDateFormat("dd/MM/yyyy")
								.format(sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado()) + "|");
					} else {
						wr.write(" " + "|");
					}
					if (sitConsultaExterna.getFecCaptura() != null) {
						String mesEpidemiologica = " ";
						String semanaEpidemiologica = " ";
						for (SicSemanaVigilaciaEpi sicSemanaVigilaciaEpi : catalogoSemanaEpi) {
							if ((sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado()
									.after(sicSemanaVigilaciaEpi.getFecInicial()))
									&& (sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado()
											.before(sicSemanaVigilaciaEpi.getFecFinal()))) {
								mesEpidemiologica = sicSemanaVigilaciaEpi.getNumMesPeriodo();
								semanaEpidemiologica = sicSemanaVigilaciaEpi.getNumSemanaEPI().toString();
								wr.write(mesEpidemiologica + "|");
								wr.write(semanaEpidemiologica + "|");
								break;
							} else if ((sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado()
									.getDay() == sicSemanaVigilaciaEpi.getFecInicial().getDay())
									&& (sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado()
											.getMonth() == sicSemanaVigilaciaEpi.getFecInicial().getMonth())
									&& (sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado()
											.getYear() == sicSemanaVigilaciaEpi.getFecInicial().getYear())) {
								mesEpidemiologica = sicSemanaVigilaciaEpi.getNumMesPeriodo();
								semanaEpidemiologica = sicSemanaVigilaciaEpi.getNumSemanaEPI().toString();
								wr.write(mesEpidemiologica + "|");
								wr.write(semanaEpidemiologica + "|");
								break;
							} else if ((sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado()
									.getDay() == sicSemanaVigilaciaEpi.getFecFinal().getDay())
									&& (sitConsultaExterna.getSdEncabezado().get(0).getFecAltaEncabezado()
											.getMonth() == sicSemanaVigilaciaEpi.getFecFinal().getMonth())
									&& (sitConsultaExterna.getFecCaptura().getYear() == sicSemanaVigilaciaEpi
											.getFecFinal().getYear())) {
								mesEpidemiologica = sicSemanaVigilaciaEpi.getNumMesPeriodo();
								semanaEpidemiologica = sicSemanaVigilaciaEpi.getNumSemanaEPI().toString();
								wr.write(mesEpidemiologica + "|");
								wr.write(semanaEpidemiologica + "|");
								break;
							}
						}
					} else {
						wr.write(" |");
					}

					if (sitConsultaExterna.getSdEncabezado().get(0).getSdEspecialidad().get(0)
							.getCveEspecialidad() != null) {
						wr.write(sitConsultaExterna.getSdEncabezado().get(0).getSdEspecialidad().get(0)
								.getCveEspecialidad() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (sitConsultaExterna.getSdEncabezado().get(0).getSdEspecialidad().get(0)
							.getDesEspecialidad() != null) {
						wr.write(sitConsultaExterna.getSdEncabezado().get(0).getSdEspecialidad().get(0)
								.getDesEspecialidad() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (formato.getSdPaciente().get(0).getNumNss() != null) {
						String digitoVerificador = this
								.calcularDigitoVerificador(formato.getSdPaciente().get(0).getNumNss());
						wr.write(formato.getSdPaciente().get(0).getNumNss() + digitoVerificador + "|");
					} else {
						wr.write(" " + "|");
					}
					if (formato.getSdPaciente().get(0).getRefAgregadoMedico() != null) {
						wr.write(formato.getSdPaciente().get(0).getRefAgregadoMedico() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (formato.getSdPaciente().get(0).getRefApellidoPaterno() != null) {
						wr.write(formato.getSdPaciente().get(0).getRefApellidoPaterno() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (formato.getSdPaciente().get(0).getRefApellidoMaterno() != null) {
						wr.write(formato.getSdPaciente().get(0).getRefApellidoMaterno() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (formato.getSdPaciente().get(0).getRefNombre() != null) {
						wr.write(formato.getSdPaciente().get(0).getRefNombre() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (formato.getSdPaciente().get(0).getRefAgregadoMedico() != null) {
						extrarSexo = formato.getSdPaciente().get(0).getRefAgregadoMedico();
						sexo = extrarSexo.substring(1, 2);
						wr.write(sexo + "|");
					} else {
						wr.write(" " + "|");
					}
					// edad.meses
					if (formato.getSdPaciente().get(0).getFecNacimiento() != null) {
						wr.write(obtenerEdadConMeses(formato.getSdPaciente().get(0).getFecNacimiento()) + "|");
					} else {
						if (formato.getSdPaciente().get(0).getNumEdad() != null) {
							wr.write(formato.getSdPaciente().get(0).getNumEdad() + "|");
						} else {
							wr.write(" " + "|");
						}
					}
					if (sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().get(0).getCveMatricula() != null) {
						wr.write(sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().get(0).getCveMatricula()
								+ "|");
					} else {
						wr.write(" " + "|");
					}
					if (!sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().isEmpty()) {
						if (sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().get(0).getRefNombre() != null
								&& sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().get(0)
										.getRefApellidoMaterno() != null
								&& sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().get(0)
										.getRefApellidoPaterno() != null) {

							String nombre = sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().get(0)
									.getRefNombre();
							String apMaterno = sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().get(0)
									.getRefApellidoMaterno();
							String apPaterno = sitConsultaExterna.getSdEncabezado().get(0).getSdMedico().get(0)
									.getRefApellidoPaterno();

							String nombreCompleto = nombre + " " + apMaterno + " " + apPaterno;
							wr.write(nombreCompleto + "|");
						} else {
							wr.write(" " + "|");
						}
					} else {
						wr.write(" " + "|");
					}
					if (!sitConsultaExterna.getSdEncabezado().get(0).getSdConsultorio().isEmpty()) {
						wr.write(sitConsultaExterna.getSdEncabezado().get(0).getSdConsultorio().get(0)
								.getCveConsultorio() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (sitConsultaExterna.getSdEncabezado().get(0).getSdTurno().get(0).getCveTurno() != null) {
						wr.write(sitConsultaExterna.getSdEncabezado().get(0).getSdTurno().get(0).getCveTurno() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (sitConsultaExterna.getSdEncabezado().get(0).getSdTurno().get(0).getDesTurno() != null) {
						wr.write(sitConsultaExterna.getSdEncabezado().get(0).getSdTurno().get(0).getDesTurno() + "|");
					} else {
						wr.write(" " + "|");
					}
					if (formato.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0).getCveCie10() != null) {
						wr.write(formato.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0).getCveCie10()
								+ "|");
					} else {
						wr.write(" " + "|");
					}
					if (formato.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0).getDesCie10() != null) {
						wr.write(formato.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0).getDesCie10()
								+ "|");
					} else {
						wr.write(" " + "|");
					}
					// OcaServ
					if (formato.getInd1EraVez() != null) {
						wr.write(formato.getInd1EraVez() + "|");
					} else {
						wr.write(" " + "|");
					}
					// idee
					if (formato.getSdPaciente().get(0).getCveIdee() != null) {
						wr.write(formato.getSdPaciente().get(0).getCveIdee() + "|");
					} else {
						wr.write(" " + "|");
					}
					// curp
					if (formato.getSdPaciente().get(0).getCveCurp() != null) {
						wr.write(formato.getSdPaciente().get(0).getCveCurp() + "|");
					} else {
						wr.write(" " + "|");
					}
					// SistemaFuente
					wr.write("1" + "|");
					// CodigoCIEModificado
					wr.write(" " + "|");
					// OcasionServicioModificado
					wr.write(" " + "|");
					// StatusVigilanciaEpidemiologica
					wr.write(" " + "|");
					// FechaVigilancia EpidemiologicaRevisionyEntrega
					wr.write(" " + "|");
					// FechaEN
					wr.write(" " + "|");
					wr.write("\n");

					sitConsultaExterna.getSdEncabezado().get(0).getSdFormato().get(cont)
							.setNumEstadoFormato(ESTATUS_CONSULTA_ENVIADA);
					cont++;					
				}
			}else{
				logger.info("La consulta " + sitConsultaExterna.getId() + " no tiene alguna atencion");
			}
		}
		archivos.add(archivoEpi);
		wr.close();
		bw.close();

		//logger.info("saliendo de la creacion del archivo exitoso");

		return archivos;
	}

	public String calcularDigitoVerificador(String nss) {

		//logger.debug("entrando al metodo para calcular digito verificador " + nss);
		int primero = Integer.parseInt(nss.substring(0, 1)) * 1;
		String primeroModificado = Integer.toString(primero);
		int resultado = 0;
		if (primeroModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(primeroModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(primeroModificado.substring(1, 2));
			resultado = primeroModificado1 + primeroModificado2;
		} else {
			resultado = primero;
		}
		int segundo = Integer.parseInt(nss.substring(1, 2)) * 2;
		String segundoModificado = Integer.toString(segundo);
		int resultado2 = 0;
		if (segundoModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(segundoModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(segundoModificado.substring(1, 2));
			resultado2 = primeroModificado1 + primeroModificado2;
		} else {
			resultado2 = segundo;
		}
		int tercero = Integer.parseInt(nss.substring(2, 3)) * 1;
		String terceroModificado = Integer.toString(tercero);
		int resultado3 = 0;
		if (terceroModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(terceroModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(terceroModificado.substring(1, 2));
			resultado3 = primeroModificado1 + primeroModificado2;
		} else {
			resultado3 = tercero;
		}
		int cuarto = Integer.parseInt(nss.substring(3, 4)) * 2;
		String cuartoModificado = Integer.toString(cuarto);
		int resultado4 = 0;
		if (cuartoModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(cuartoModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(cuartoModificado.substring(1, 2));
			resultado4 = primeroModificado1 + primeroModificado2;
		} else {
			resultado4 = cuarto;
		}
		int quinto = Integer.parseInt(nss.substring(4, 5)) * 1;
		String quintoModificado = Integer.toString(quinto);
		int resultado5 = 0;
		if (quintoModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(quintoModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(quintoModificado.substring(1, 2));
			resultado5 = primeroModificado1 + primeroModificado2;
		} else {
			resultado5 = quinto;
		}
		int sexto = Integer.parseInt(nss.substring(5, 6)) * 2;
		String sextoModificado = Integer.toString(sexto);
		int resultado6 = 0;
		if (sextoModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(sextoModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(sextoModificado.substring(1, 2));
			resultado6 = primeroModificado1 + primeroModificado2;
		} else {
			resultado6 = sexto;
		}
		int septimo = Integer.parseInt(nss.substring(6, 7)) * 1;
		String septimoModificado = Integer.toString(septimo);
		int resultado7 = 0;
		if (septimoModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(septimoModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(septimoModificado.substring(1, 2));
			resultado7 = primeroModificado1 + primeroModificado2;
		} else {
			resultado7 = septimo;
		}
		int octavo = Integer.parseInt(nss.substring(7, 8)) * 2;
		String octavoModificado = Integer.toString(octavo);
		int resultado8 = 0;
		if (octavoModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(octavoModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(octavoModificado.substring(1, 2));
			resultado8 = primeroModificado1 + primeroModificado2;
		} else {
			resultado8 = octavo;
		}
		int noveno = Integer.parseInt(nss.substring(8, 9)) * 1;
		String novenoModificado = Integer.toString(noveno);
		int resultado9 = 0;
		if (novenoModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(novenoModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(novenoModificado.substring(1, 2));
			resultado9 = primeroModificado1 + primeroModificado2;
		} else {
			resultado9 = noveno;
		}
		int decimo = Integer.parseInt(nss.substring(9, 10)) * 2;
		String decimoModificado = Integer.toString(decimo);
		int resultado10 = 0;
		if (decimoModificado.length() > 1) {
			int primeroModificado1 = Integer.parseInt(decimoModificado.substring(0, 1));
			int primeroModificado2 = Integer.parseInt(decimoModificado.substring(1, 2));
			resultado10 = primeroModificado1 + primeroModificado2;
		} else {
			resultado10 = decimo;
		}
		int subTotalDV = resultado + resultado2 + resultado3 + resultado4 + resultado5 + resultado6 + resultado7
				+ resultado8 + resultado9 + resultado10;

		int dv = 0;
		for (int i = 99; i > dv; i--) {
			String cadena = Integer.toString(subTotalDV);
			String aux = "";
			if (cadena.length() > 1) {
				aux = cadena.substring(1, 2);
			} else {
				aux = cadena;
			}
			if (aux.equals("0")) {
				break;
			}
			dv++;
			subTotalDV++;
		}
		String total = Integer.toString(dv);
		//logger.debug("Saliendo del metodo para calcular digito verificador");
		return total;
	}

	public List<SitConsultaExterna> obtenerConsultasExternas(List<String> listaEspecialidades,
			List<String> catalogoEpidemiologico, String unidad) {

		List<SitConsultaExterna> unidadMedica = this.consultaExternaDao.buscarConsultasExternas(listaEspecialidades,
				catalogoEpidemiologico, unidad);
		return unidadMedica;
	}

	public void actualizarConsultaExterna(List<SitConsultaExterna> listaConsultasExternas) {

		// this.consultaExternaDao.actualizarConsultasExternas(listaEspecialidades,
		// catalogoEpidemiologico, unidad, listaConsultasExternas);
		this.consultaExternaDao.actualizarAtenciones(listaConsultasExternas);
	}

	public List<String> obtenerCatalogoVigEpi() {

		List<String> catalogoVigEpi = this.cie10VigilanciaEpiDao.obtenerCatalogoVigEpi();
		return catalogoVigEpi;
	}

	public List<SicSemanaVigilaciaEpi> obtenerCatalogoSemanaEpi() {

		List<SicSemanaVigilaciaEpi> listacatalogoSemanaEpi = this.catalogoSemanaVigilanciaEpiDao
				.burcarCatalogoSemanaVigiEpi();
		return listacatalogoSemanaEpi;
	}

	public List<String> obtenerEspecialidades() {

		List<String> especialidad = this.sicEspecialidadDao.burcarcvEspecialidad();
		return especialidad;
	}

	public int enviarArchivoSFTP(List<File> archivos) throws JSchException, FileNotFoundException, SftpException {

		int resultado = 0;

		//logger.debug("Inicia envio por ftp del archivo ");
		JSch sftp = new JSch();

		Session session = null;

		String rutaArchivoOrigen = "";
		String rutaArchivoDestino = "";

		ChannelSftp channelSftp = null;

		try {
			session = sftp.getSession(this.user, this.server, this.port);
			session.setPassword(this.pass);

			Properties prop = new Properties();
			prop.put("StrictHostKeyChecking", "no");
			session.setConfig(prop);

			//logger.debug("Iniciando sesion: " + this.user + " ******** " + this.server);

			session.connect(this.timeout);

			channelSftp = (ChannelSftp) session.openChannel("sftp");
			channelSftp.connect();

			for (File archivo : archivos) {

				rutaArchivoOrigen = archivo.getPath();
				rutaArchivoDestino = archivo.getName();

				File firstLocalFile = new File(rutaArchivoOrigen);

				InputStream inputStream = new FileInputStream(firstLocalFile);

				channelSftp.put(inputStream, rutaArchivoDestino);
			}

			resultado = 1;
		} finally {
			try {
				if(session != null){
					if (session.isConnected()) {
						session.disconnect();
					}
				}
				if(channelSftp != null){
					if (channelSftp.isConnected()) {
						channelSftp.disconnect();
					}
				}
			} catch (Exception e) {
				logger.error("Error al intentar enviar el archivo: ", e);
			}
		}

		//logger.info("Archivo enviado al ftp " + this.rutaLocalArchivos);

		return resultado;
	}

	public String obtenerEdadConMeses(Date fecha) {

		String edadConMeses = "";
		String fecNa = new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		String fechaInicio = fecNa;

		String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

		String[] aFechaIng = fechaInicio.split("/");
		Integer diaInicio = Integer.valueOf(Integer.parseInt(aFechaIng[0]));
		Integer mesInicio = Integer.valueOf(Integer.parseInt(aFechaIng[1]));
		Integer anioInicio = Integer.valueOf(Integer.parseInt(aFechaIng[2]));

		String[] aFecha = fechaActual.split("/");
		Integer diaActual = Integer.valueOf(Integer.parseInt(aFecha[0]));
		Integer mesActual = Integer.valueOf(Integer.parseInt(aFecha[1]));
		Integer anioActual = Integer.valueOf(Integer.parseInt(aFecha[2]));

		int b = 0;
		int dias = 0;
		int mes = 0;
		int anios = 0;
		int meses = 0;
		mes = mesInicio.intValue() - 1;
		if (mes == 2) {
			if ((anioActual.intValue() % 4 == 0)
					&& ((anioActual.intValue() % 100 != 0) || (anioActual.intValue() % 400 == 0))) {
				b = 29;
			} else {
				b = 28;
			}
		} else if (mes <= 7) {
			if (mes == 0) {
				b = 31;
			} else if (mes % 2 == 0) {
				b = 30;
			} else {
				b = 31;
			}
		} else if (mes > 7) {
			if (mes % 2 == 0) {
				b = 31;
			} else {
				b = 30;
			}
		}
		if ((anioInicio.intValue() <= anioActual.intValue())
				&& ((anioInicio != anioActual) || (mesInicio.intValue() <= mesActual.intValue()))
				&& ((anioInicio != anioActual) || (mesInicio != mesActual)
						|| (diaInicio.intValue() <= diaActual.intValue()))) {
			if (mesInicio.intValue() <= mesActual.intValue()) {
				anios = anioActual.intValue() - anioInicio.intValue();
				if (diaInicio.intValue() <= diaActual.intValue()) {
					meses = mesActual.intValue() - mesInicio.intValue();
					dias = b - (diaInicio.intValue() - diaActual.intValue());
				} else {
					if (mesActual == mesInicio) {
						anios -= 1;
					}
					meses = (mesActual.intValue() - mesInicio.intValue() - 1 + 12) % 12;
					dias = b - (diaInicio.intValue() - diaActual.intValue());
				}
			} else {
				anios = anioActual.intValue() - anioInicio.intValue() - 1;
				if (diaInicio.intValue() > diaActual.intValue()) {
					meses = mesActual.intValue() - mesInicio.intValue() - 1 + 12;
					dias = b - (diaInicio.intValue() - diaActual.intValue());
				} else {
					meses = mesActual.intValue() - mesInicio.intValue() + 12;
					dias = diaActual.intValue() - diaInicio.intValue();
				}
			}
		}
		if(String.valueOf(meses).length()>1){
			return edadConMeses = String.valueOf(anios) + "." + String.valueOf(meses);

		}else{
			return	edadConMeses = String.valueOf(anios) + "." + "0" +String.valueOf(meses);

		}
	}

	private boolean tieneFormato(List<SdEncabezado> sdEncabezado) {
		if (sdEncabezado.get(0).getSdFormato() != null && !sdEncabezado.get(0).getSdFormato().isEmpty()) {
			return true;
		}
		return false;
	}
}