/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoIngreso;

/**
 * @author francisco.rrios
 * 
 */
public interface SicTipoIngresoDao {

	/**
	 * @return
	 */
	List<SicTipoIngreso> burcarTodos();

	/**
	 * @param cveTipoIngreso
	 * @return
	 */
	List<SicTipoIngreso> burcarPorId(Integer cveTipoIngreso);
}
