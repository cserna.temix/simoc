/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_TIPO_EGRESO")
public class SicTipoEgreso {

	@Id
	private ObjectId id;
	private Integer cveTipoEgreso;
	private String desTipoEgreso;
	private String desLugarAtencion;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTipoEgreso
	 */
	public Integer getCveTipoEgreso() {
		return cveTipoEgreso;
	}

	/**
	 * @param cveTipoEgreso
	 *            the cveTipoEgreso to set
	 */
	public void setCveTipoEgreso(Integer cveTipoEgreso) {
		this.cveTipoEgreso = cveTipoEgreso;
	}

	/**
	 * @return the desTipoEgreso
	 */
	public String getDesTipoEgreso() {
		return desTipoEgreso;
	}

	/**
	 * @param desTipoEgreso
	 *            the desTipoEgreso to set
	 */
	public void setDesTipoEgreso(String desTipoEgreso) {
		this.desTipoEgreso = desTipoEgreso;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the desLugarAtencion
	 */
	public String getDesLugarAtencion() {
		return desLugarAtencion;
	}

	/**
	 * @param desLugarAtencion the desLugarAtencion to set
	 */
	public void setDesLugarAtencion(String desLugarAtencion) {
		this.desLugarAtencion = desLugarAtencion;
	}

}
