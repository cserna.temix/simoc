/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SitUnidadMedicaDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.negocio.SitUnidadMedica;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sitUnidadMedicaDao")
public class SitUnidadMedicaDaoImpl implements SitUnidadMedicaDao {
//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SitUnidadMedica> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SitUnidadMedica> listUnidadMedica = new ArrayList<SitUnidadMedica>();
		try {
			//connectionMongo = new ConnectionMongo();
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listUnidadMedica = dsEsp.createQuery(SitUnidadMedica.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//			connectionMongo.cerrarConexion();
//		}

		return listUnidadMedica;
	}

	@Override
	public List<SitUnidadMedica> burcarPorId(String idUnidadMedica) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SitUnidadMedica> listUnidadMedica = new ArrayList<SitUnidadMedica>();
		try {
			//connectionMongo = new ConnectionMongo();
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listUnidadMedica = dsEsp.createQuery(SitUnidadMedica.class)
					.field("cvePresupuestal").equal(idUnidadMedica).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//			connectionMongo.cerrarConexion();
//		}

		return listUnidadMedica;
	}

	@Override
	public List<SitUnidadMedica> burcarPorCveDelegacion(String cveDelegacion) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SitUnidadMedica> listUnidadMedica = new ArrayList<SitUnidadMedica>();
		try {
			//connectionMongo = new ConnectionMongo();
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listUnidadMedica = dsEsp.createQuery(SitUnidadMedica.class)
					.field("cveDelegacion").equal(cveDelegacion).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//			connectionMongo.cerrarConexion();
//		}

		return listUnidadMedica;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
