/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;

/**
 * @author francisco.rrios
 * 
 */
public interface SicEspecialidadDao {

	/**
	 * @return
	 */
	List<SicEspecialidad> burcarTodos();

	/**
	 * @param cveEspecialidad
	 * @return
	 */
	List<SicEspecialidad> burcarPorId(String cveEspecialidad);
	
	/**
	 * Metodo que realiza la busqueda de una especialidad de acuerdo a su clave
	 * @param cveEspecialidad
	 * @return SicEspecialidad- Especialidad con sus atributos
	 */
	SicEspecialidad buscarPorId(String cveEspecialidad);	
	List<String> burcarcvEspecialidad();
	List<SicEspecialidad> burcarEspecialidadesPorCategoria(Integer numCategoria);
	List<SicEspecialidad> burcarEspecialidadesPorCategoriaOrdenAlfabetico(Integer numCategoria);	 
	
}
