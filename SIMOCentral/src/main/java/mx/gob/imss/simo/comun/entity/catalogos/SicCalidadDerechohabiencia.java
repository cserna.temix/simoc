/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_CALIDAD_DERECHOHABIENCIA")
public class SicCalidadDerechohabiencia {

	@Id
	private ObjectId id;
	private Integer cveCalidadDerechohabiencia;
	private String desCalidadDerechohabiencia;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveCalidadDerechohabiencia
	 */
	public Integer getCveCalidadDerechohabiencia() {
		return cveCalidadDerechohabiencia;
	}

	/**
	 * @param cveCalidadDerechohabiencia
	 *            the cveCalidadDerechohabiencia to set
	 */
	public void setCveCalidadDerechohabiencia(Integer cveCalidadDerechohabiencia) {
		this.cveCalidadDerechohabiencia = cveCalidadDerechohabiencia;
	}

	/**
	 * @return the desCalidadDerechohabiencia
	 */
	public String getDesCalidadDerechohabiencia() {
		return desCalidadDerechohabiencia;
	}

	/**
	 * @param desCalidadDerechohabiencia
	 *            the desCalidadDerechohabiencia to set
	 */
	public void setDesCalidadDerechohabiencia(String desCalidadDerechohabiencia) {
		this.desCalidadDerechohabiencia = desCalidadDerechohabiencia;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
