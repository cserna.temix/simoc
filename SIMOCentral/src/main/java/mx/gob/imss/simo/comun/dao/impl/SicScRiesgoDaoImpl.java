/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicScRiesgoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicScRiesgo;

/**
 * @author francisco.rodriguez
 *
 */
@Repository("sicScRiesgoDao")
public class SicScRiesgoDaoImpl implements SicScRiesgoDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicScRiesgoDao#burcarTodos()
	 */
	@Override
	public List<SicScRiesgo> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicScRiesgo> listScRiesgo = new ArrayList<SicScRiesgo>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listScRiesgo = dsEsp.createQuery(SicScRiesgo.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listScRiesgo;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicScRiesgoDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicScRiesgo> burcarPorId(Integer cveRiesgo) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicScRiesgo> listScRiesgo = new ArrayList<SicScRiesgo>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listScRiesgo = dsEsp.createQuery(SicScRiesgo.class)
					.field("cveRiesgo").equal(cveRiesgo).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listScRiesgo;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
