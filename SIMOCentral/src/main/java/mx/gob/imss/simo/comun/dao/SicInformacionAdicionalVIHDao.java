/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicionalVIH;

/**
 * @author francisco.rrios
 * 
 */
public interface SicInformacionAdicionalVIHDao {

	/**
	 * @return
	 */
	List<SicInformacionAdicionalVIH> burcarTodos();

	/**
	 * @return
	 */
	SicInformacionAdicionalVIH burcarVIHPorId(Integer cveCD4);
}
