package mx.gob.imss.simo.consultaexterna.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.negocio.SitBitacoraEnvio;

public interface BitacoraEnvioDao {

	void insertar(SitBitacoraEnvio bitacoraEnvio);
	
	List<SitBitacoraEnvio> consultarBitacora();
}
