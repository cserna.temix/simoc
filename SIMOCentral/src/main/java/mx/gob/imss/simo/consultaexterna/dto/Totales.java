package mx.gob.imss.simo.consultaexterna.dto;

public enum Totales {
	
	UNIDAD("TOTAL DE LA UNIDAD"),
	ESPECIALIDADES("MED. DE ESPECIALIDADES"),
	URGENCIAS("MEDICINA DE URGENCIAS");
	
	private String clave;
	
	private Totales(String clave) {
		this.clave = clave;
	}
	
	public String getClave() {
		return clave;
	}

}