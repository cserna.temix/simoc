/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rodriguez
 * 
 */
@Entity("SIC_ESPECIALIDAD_UNIDAD")
public class SicEspecialidadUnidad {

	@Id
	private ObjectId id;
	private String cveEspecialidad;
	private String desEspecialidad;
	private String cvePresupuestal;
	private Integer indConsultaExterna;
	private Integer indHospital;
	private Integer indPediatria;
	private Integer indCirugia;
	private Integer indEstatus;
	private Date fecAlta;
	private Date fecBaja;
	private Integer numTipoServicio;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveEspecialidad
	 */
	public String getCveEspecialidad() {
		return cveEspecialidad;
	}

	/**
	 * @param cveEspecialidad
	 *            the cveEspecialidad to set
	 */
	public void setCveEspecialidad(String cveEspecialidad) {
		this.cveEspecialidad = cveEspecialidad;
	}

	/**
	 * @return the desEspecialidad
	 */
	public String getDesEspecialidad() {
		return desEspecialidad;
	}

	/**
	 * @param desEspecialidad
	 *            the desEspecialidad to set
	 */
	public void setDesEspecialidad(String desEspecialidad) {
		this.desEspecialidad = desEspecialidad;
	}

	/**
	 * @return the cvePresupuestal
	 */
	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	/**
	 * @param cvePresupuestal
	 *            the cvePresupuestal to set
	 */
	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	/**
	 * @return the indConsultaExterna
	 */
	public Integer getIndConsultaExterna() {
		return indConsultaExterna;
	}

	/**
	 * @param indConsultaExterna
	 *            the indConsultaExterna to set
	 */
	public void setIndConsultaExterna(Integer indConsultaExterna) {
		this.indConsultaExterna = indConsultaExterna;
	}

	/**
	 * @return the indHospital
	 */
	public Integer getIndHospital() {
		return indHospital;
	}

	/**
	 * @param indHospital
	 *            the indHospital to set
	 */
	public void setIndHospital(Integer indHospital) {
		this.indHospital = indHospital;
	}

	/**
	 * @return the indPediatria
	 */
	public Integer getIndPediatria() {
		return indPediatria;
	}

	/**
	 * @param indPediatria
	 *            the indPediatria to set
	 */
	public void setIndPediatria(Integer indPediatria) {
		this.indPediatria = indPediatria;
	}

	/**
	 * @return the indCirugia
	 */
	public Integer getIndCirugia() {
		return indCirugia;
	}

	/**
	 * @param indCirugia
	 *            the indCirugia to set
	 */
	public void setIndCirugia(Integer indCirugia) {
		this.indCirugia = indCirugia;
	}

	/**
	 * @return the indEstatus
	 */
	public Integer getIndEstatus() {
		return indEstatus;
	}

	/**
	 * @param indEstatus
	 *            the indEstatus to set
	 */
	public void setIndEstatus(Integer indEstatus) {
		this.indEstatus = indEstatus;
	}

	/**
	 * @return the fecAlta
	 */
	public Date getFecAlta() {
		return fecAlta;
	}

	/**
	 * @param fecAlta
	 *            the fecAlta to set
	 */
	public void setFecAlta(Date fecAlta) {
		this.fecAlta = fecAlta;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the numTipoServicio
	 */
	public Integer getNumTipoServicio() {
		return numTipoServicio;
	}

	/**
	 * @param numTipoServicio
	 *            the numTipoServicio to set
	 */
	public void setNumTipoServicio(Integer numTipoServicio) {
		this.numTipoServicio = numTipoServicio;
	}

}
