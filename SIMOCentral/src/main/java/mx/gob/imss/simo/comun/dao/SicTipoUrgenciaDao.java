/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoUrgencia;

/**
 * @author francisco.rrios
 * 
 */
public interface SicTipoUrgenciaDao {

	/**
	 * @return
	 */
	List<SicTipoUrgencia> burcarTodos();

	/**
	 * @param cveTipoUrgencia
	 * @return
	 */
	List<SicTipoUrgencia> burcarPorId(Integer cveTipoUrgencia);
}
