/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_MOTIVO_ALTA")
public class SicMotivoAlta {

	@Id
	private ObjectId id;
	private Integer cveMotivoAlta;
	private String desMotivoAlta;
	private String desTipoEgreso;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveMotivoAlta
	 */
	public Integer getCveMotivoAlta() {
		return cveMotivoAlta;
	}

	/**
	 * @param cveMotivoAlta
	 *            the cveMotivoAlta to set
	 */
	public void setCveMotivoAlta(Integer cveMotivoAlta) {
		this.cveMotivoAlta = cveMotivoAlta;
	}

	/**
	 * @return the desMotivoAlta
	 */
	public String getDesMotivoAlta() {
		return desMotivoAlta;
	}

	/**
	 * @param desMotivoAlta
	 *            the desMotivoAlta to set
	 */
	public void setDesMotivoAlta(String desMotivoAlta) {
		this.desMotivoAlta = desMotivoAlta;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the desTipoEgreso
	 */
	public String getDesTipoEgreso() {
		return desTipoEgreso;
	}

	/**
	 * @param desTipoEgreso the desTipoEgreso to set
	 */
	public void setDesTipoEgreso(String desTipoEgreso) {
		this.desTipoEgreso = desTipoEgreso;
	}

}
