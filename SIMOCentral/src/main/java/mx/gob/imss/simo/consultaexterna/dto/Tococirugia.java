package mx.gob.imss.simo.consultaexterna.dto;

public enum Tococirugia {

	TOCOCIRUGIA("TOCOCIRUG\u00CDA");

	private String clave;

	private Tococirugia(String clave) {
		this.clave = clave;
	}

	public String getClave() {
		return clave;
	}

}