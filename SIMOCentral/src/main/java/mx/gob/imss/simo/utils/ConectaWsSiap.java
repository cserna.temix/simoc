/**
 * 
 */
package mx.gob.imss.simo.utils;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mongodb.morphia.Datastore;
import org.w3c.dom.Element;

import mx.gob.imss.didt.cia.interoper.servicios.endpoint.ObtenerServicioResponse;
import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dto.SiapDto;
import mx.gob.imss.simo.comun.entity.catalogos.SicConstantes;
import mx.gob.imss.simo.master.client.ws.SimoMasterClientWS;
import mx.gob.imss.simo.siap.jaxb.ConsultaSimo;
import mx.gob.imss.simo.siap.jaxb.ConsultaSimoResponse;

/**
 * @author janiel.mb
 *
 */
public class ConectaWsSiap extends Thread{

	final static Log logger = LogFactory.getLog(ConectaWsSiap.class.getName());
	
	private String matricula;
	private String delegacion;
	private String simo_osb;
	private SiapDto siapDto = null;
//	private ConnectionMongo connectionMongo;
	
	
	public ConectaWsSiap(String matricula, String delegacion, String simo_osb){
//		this.connectionMongo=connectionMongo;
		this.matricula=matricula;
		this.delegacion=delegacion;
		this.simo_osb=simo_osb;
	}
	
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

	public void run(){
		
			
		// Request SIAP
				ConsultaSimo requestSiap = new ConsultaSimo();
				requestSiap.setMatricula(matricula); // 99092041
				requestSiap.setDelegacion(delegacion); // 09

				ObtenerServicioResponse responseMaster;
				try {				
					responseMaster = SimoMasterClientWS.obtenerServicio(requestSiap, "ConsultarSiap", "version", this.obtenerUrlConstante(simo_osb));			

					ConsultaSimoResponse csr = (ConsultaSimoResponse) SimoMasterClientWS
							.unmarshallObject((Element) responseMaster.getEndPointCadisOut().getMensaje(),ConsultaSimoResponse.class);
					if (csr.getConsultaSimoResult().getNewDataSet().getQry().getNUM() == 0) {
						siapDto = new SiapDto();
						siapDto.setaMaterno(csr.getConsultaSimoResult().getNewDataSet().getQry().getAMATERNO());
						siapDto.setaPaterno(csr.getConsultaSimoResult().getNewDataSet().getQry().getAPATERNO());
						siapDto.setNombres(csr.getConsultaSimoResult().getNewDataSet().getQry().getNOMBRES());
						siapDto.setTipoContratacion(csr.getConsultaSimoResult().getNewDataSet().getQry().getTIPOCONTRATACION());
						siapDto.setDelegacion(String.valueOf(csr.getConsultaSimoResult().getNewDataSet().getQry().getDELEGACION()));
						siapDto.setMatricula(String.valueOf(csr.getConsultaSimoResult().getNewDataSet().getQry().getMATRICULA()));
						siapDto.setEncontrado(Boolean.TRUE);

					}else{						
						siapDto = new SiapDto();
						siapDto.setEncontrado(Boolean.FALSE);
						
					}
				} catch (JAXBException e) {
					logger.info(e.getMessage());
				}catch(MalformedURLException me) {
					logger.info(me.getMessage());
				}catch (UnsupportedEncodingException ue) {
					logger.info(ue.getMessage());
				}
			
		}

	private String obtenerUrlConstante(String value){
		String url = "";
//		SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();			
			url = ds.createQuery(SicConstantes.class).filter("cveConstante", value).get().getRefValor();			
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return url;
	}
	
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getDelegacion() {
		return delegacion;
	}

	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}

	public SiapDto getSiapDto() {
		return siapDto;
	}

	public void setSiapDto(SiapDto siapDto) {
		this.siapDto = siapDto;
	}

	public String getSimo_osb() {
		return simo_osb;
	}

	public void setSimo_osb(String simo_osb) {
		this.simo_osb = simo_osb;
	}
	
	
	
}
