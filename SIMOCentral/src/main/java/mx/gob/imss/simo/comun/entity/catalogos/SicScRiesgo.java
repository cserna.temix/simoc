/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rodriguez
 *
 */
@Entity("SIC_SC_RIESGO")
public class SicScRiesgo {

	@Id
	private ObjectId id;
	private Integer cveRiesgo;
	private String desRiesgo;
	private Date fecBaja;
	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}
	/**
	 * @return the cveRiesgo
	 */
	public Integer getCveRiesgo() {
		return cveRiesgo;
	}
	/**
	 * @param cveRiesgo the cveRiesgo to set
	 */
	public void setCveRiesgo(Integer cveRiesgo) {
		this.cveRiesgo = cveRiesgo;
	}
	/**
	 * @return the desRiesgo
	 */
	public String getDesRiesgo() {
		return desRiesgo;
	}
	/**
	 * @param desRiesgo the desRiesgo to set
	 */
	public void setDesRiesgo(String desRiesgo) {
		this.desRiesgo = desRiesgo;
	}
	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}
	/**
	 * @param fecBaja the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}
	
	
}
