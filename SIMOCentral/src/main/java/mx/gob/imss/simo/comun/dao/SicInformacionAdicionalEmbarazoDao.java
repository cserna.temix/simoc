/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicionalEmbarazo;

/**
 * @author francisco.rrios
 * 
 */
public interface SicInformacionAdicionalEmbarazoDao {

	/**
	 * @return
	 */
	List<SicInformacionAdicionalEmbarazo> burcarTodos();

	/**
	 * @return
	 */
	SicInformacionAdicionalEmbarazo burcarEmbarazoPorId(Integer cveTipoEmbarazo);
}
