/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicConsultorio;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicConsultorioDao {

	/**
	 * @return
	 */
	List<SicConsultorio> burcarTodos();

	/**
	 * @param cveConsultorio
	 * @return
	 */
	List<SicConsultorio> burcarPorId(String cveConsultorio);
	

	/**
	 * @param cvePresupuestal
	 * @param cveEspecialidad
	 * @return
	 */
	List<SicConsultorio> burcarPorUnidadEsp(String cvePresupuestal, Integer tipoServicio);
	
	/**
	 * @param cveConsultorio
	 * @param cvePresupuestal
	 * @param cveEspecialidad
	 * @return
	 */
	List<SicConsultorio> burcarPorIdUnidadEsp(String cveConsultorio, String cvePresupuestal, String cveEspecialidad);
}
