/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rodriguez
 *
 */
@Entity("SIC_TIPO_REGISTRO")
public class SicTipoRegistro {

	@Id
	private ObjectId id;
	private Integer cveTipoRegistro;
	private String desTipoRegistro;
	private Date fecBaja;
	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}
	/**
	 * @return the cveTipoRegistro
	 */
	public Integer getCveTipoRegistro() {
		return cveTipoRegistro;
	}
	/**
	 * @param cveTipoRegistro the cveTipoRegistro to set
	 */
	public void setCveTipoRegistro(Integer cveTipoRegistro) {
		this.cveTipoRegistro = cveTipoRegistro;
	}
	/**
	 * @return the desTipoRegistro
	 */
	public String getDesTipoRegistro() {
		return desTipoRegistro;
	}
	/**
	 * @param desTipoRegistro the desTipoRegistro to set
	 */
	public void setDesTipoRegistro(String desTipoRegistro) {
		this.desTipoRegistro = desTipoRegistro;
	}
	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}
	/**
	 * @param fecBaja the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}
	
	
}
