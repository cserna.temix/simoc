/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicMedico;

/**
 * @author francisco.rrios
 *
 */
public interface SicMedicoDao {
	/**
	 * @return
	 */
	List<SicMedico> burcarTodos();

	/**
	 * @param cveMatricula
	 * @return
	 */
	List<SicMedico> burcarPorId(String cveMatricula);
}
