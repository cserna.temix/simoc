/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidadUnidad;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicEspecialidadUnidadDao {

	/**
	 * @return
	 */
	List<SicEspecialidadUnidad> burcarTodos();

	/**
	 * @param cveEspecialidad
	 * @return
	 */
	List<SicEspecialidadUnidad> burcarPorId(String cveEspecialidad);
	
	
	/**
	 * @param cvePresupuestal
	 * @return
	 */
	List<SicEspecialidadUnidad> burcarPorUnidadMed(String cvePresupuestal, String claveModuloAtencion);
	
	/**
	 * @param cveEspecialidad
	 * @param cvePresupuestal
	 * @return
	 */
	List<SicEspecialidadUnidad> burcarPorIdUnidadMed(String cveEspecialidad, String cvePresupuestal);
	/**
	 * 
	 * @param cvePresupuestal
	 * @return
	 */
	List<SicEspecialidadUnidad> buscarPorClaveProsupuestal(String cvePresupuestal);
}
