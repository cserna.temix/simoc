/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicUnidadAdscripcionDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicUnidadAdscripcion;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicUnidadAdscripcionDao")
public class SicUnidadAdscripcionDaoImpl implements SicUnidadAdscripcionDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/**
	 * 
	 */
	public SicUnidadAdscripcionDaoImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<SicUnidadAdscripcion> burcarTodos() {
		// SimoDatastore simoDatastore = new SimoDatastore();
		List<SicUnidadAdscripcion> listUnidadAdscripcion = new ArrayList<SicUnidadAdscripcion>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			
			listUnidadAdscripcion = dsEsp.createQuery(
					SicUnidadAdscripcion.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listUnidadAdscripcion;
	}

	@Override
	public List<SicUnidadAdscripcion> burcarPorId(String cvePresupuestal) {
		// SimoDatastore simoDatastore = new SimoDatastore();
		List<SicUnidadAdscripcion> listUnidadAdscripcion = new ArrayList<SicUnidadAdscripcion>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listUnidadAdscripcion = dsEsp
					.createQuery(SicUnidadAdscripcion.class)
					.field("cvePresupuestal").equal(cvePresupuestal).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listUnidadAdscripcion;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
