/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

/**
 * @author francisco.rrios
 * 
 */
public class SdInformacionAdicional {

	private String cveInformacionAdicional;
	private String desInformacionAdicional;
	private Integer cveTipoInformacionAdicional;
	/**
	 * @return the cveInformacionAdicional
	 */
	public String getCveInformacionAdicional() {
		return cveInformacionAdicional;
	}

	/**
	 * @param cveInformacionAdicional
	 *            the cveInformacionAdicional to set
	 */
	public void setCveInformacionAdicional(String cveInformacionAdicional) {
		this.cveInformacionAdicional = cveInformacionAdicional;
	}

	/**
	 * @return the desInformacionAdicional
	 */
	public String getDesInformacionAdicional() {
		return desInformacionAdicional;
	}

	/**
	 * @param desInformacionAdicional
	 *            the desInformacionAdicional to set
	 */
	public void setDesInformacionAdicional(String desInformacionAdicional) {
		this.desInformacionAdicional = desInformacionAdicional;
	}

	public Integer getCveTipoInformacionAdicional() {
		return cveTipoInformacionAdicional;
	}

	public void setCveTipoInformacionAdicional(Integer cveTipoInformacionAdicional) {
		this.cveTipoInformacionAdicional = cveTipoInformacionAdicional;
	}

}
