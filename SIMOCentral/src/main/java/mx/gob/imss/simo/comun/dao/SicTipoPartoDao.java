/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoParto;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicTipoPartoDao {

	/**
	 * @return
	 */
	List<SicTipoParto> burcarTodos();

	/**
	 * @param cveTipoParto
	 * @return
	 */
	List<SicTipoParto> burcarPorId(Integer cveTipoParto);
}
