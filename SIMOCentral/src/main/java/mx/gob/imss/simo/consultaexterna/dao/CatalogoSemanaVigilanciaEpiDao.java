package mx.gob.imss.simo.consultaexterna.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicSemanaVigilaciaEpi;

public interface CatalogoSemanaVigilanciaEpiDao {
	
	List<SicSemanaVigilaciaEpi> burcarCatalogoSemanaVigiEpi();

}
