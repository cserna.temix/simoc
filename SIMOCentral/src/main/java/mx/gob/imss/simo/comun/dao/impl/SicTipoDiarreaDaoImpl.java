/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoDiarreaDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoDiarrea;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicTipoDiarreaDao")
public class SicTipoDiarreaDaoImpl implements SicTipoDiarreaDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.comun.dao.SicTipoDiarreaDao#burcarTodos()
	 */
	@Override
	public List<SicTipoDiarrea> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoDiarrea> listTipoDiarrea = new ArrayList<SicTipoDiarrea>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoDiarrea = dsEsp.createQuery(SicTipoDiarrea.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listTipoDiarrea;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicTipoDiarreaDao#burcarPorId(java.lang.Integer
	 * )
	 */
	@Override
	public List<SicTipoDiarrea> burcarPorId(String cveTipoDiarrea) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoDiarrea> listTipoDiarrea = new ArrayList<SicTipoDiarrea>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoDiarrea = dsEsp.createQuery(SicTipoDiarrea.class)
					.field("cveTipoDiarrea").equal(cveTipoDiarrea).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoDiarrea;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
