package mx.gob.imss.simo.consultaexterna.service;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;

public interface DesbloquearCuentasActiveService {
	
	public List<SicEspecialidad> burcarcvEspecialidad();

}
