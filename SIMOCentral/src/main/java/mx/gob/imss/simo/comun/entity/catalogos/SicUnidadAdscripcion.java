/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_UNIDAD_ADSCRIPCION")
public class SicUnidadAdscripcion {

	@Id
	private ObjectId id;
	private Integer cvePresupuestal;
	private String desUnidadMedica;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cvePresupuestal
	 */
	public Integer getCvePresupuestal() {
		return cvePresupuestal;
	}

	/**
	 * @param cvePresupuestal
	 *            the cvePresupuestal to set
	 */
	public void setCvePresupuestal(Integer cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	/**
	 * @return the desUnidadMedica
	 */
	public String getDesUnidadMedica() {
		return desUnidadMedica;
	}

	/**
	 * @param desUnidadMedica
	 *            the desUnidadMedica to set
	 */
	public void setDesUnidadMedica(String desUnidadMedica) {
		this.desUnidadMedica = desUnidadMedica;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
