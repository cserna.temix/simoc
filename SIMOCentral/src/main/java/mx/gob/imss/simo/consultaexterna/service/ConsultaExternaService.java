package mx.gob.imss.simo.consultaexterna.service;

import java.util.Date;
import java.util.List;

import org.mongodb.morphia.query.UpdateResults;

import mx.gob.imss.simo.comun.dto.SicCie10DTO;
import mx.gob.imss.simo.comun.dto.SicCie9DTO;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie10Morfologia;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicionalVIH;
import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;
import mx.gob.imss.simo.consultaexterna.dto.ControlMedicoEncabezadoDto;
import mx.gob.imss.simo.model.ConsultaExternaDto;
import mx.gob.imss.simo.model.UsuarioDetalleDto;

/**
 * @author francisco.rrios
 * 
 */
public interface ConsultaExternaService {

	/**
	 * Insertar Registro
	 * 
	 * @param consulExtDto
	 */
	void insertar(ConsultaExternaDto consulExtDto, UsuarioDetalleDto usuarioDetalleDto);

	/**
	 * Agrega un detalle
	 * 
	 * @param consulExtDto
	 * @return 
	 */
	Boolean actualizar(ConsultaExternaDto consulExtDto);

	/**
	 * Modifica una atencion
	 * 
	 * @param consulExtDto
	 */
	void actEdicion(ConsultaExternaDto consulExtDto);

	/**
	 * Busca si existe un encabezado anterior y el número de Atencion
	 * 
	 * @param consulExtDto
	 * @param usuarioDetalleDto
	 * @return
	 */
	Integer buscar(ConsultaExternaDto consulExtDto, UsuarioDetalleDto usuarioDetalleDto);

	/**
	 * Agrega el nombre y la Unidad Adscripta correspondiente al Nss y
	 * Agregadomedico
	 * 
	 * @param consulExtDto
	 */
	void cargarNombreUnidad(ConsultaExternaDto consulExtDto);

	/**
	 * @param consulExtDto
	 */
	void buscarEncabezadoCap(ConsultaExternaDto consulExtDto);

	/**
	 * @param consulExtDto
	 * @return
	 */
	List<ConsultaExternaDto> buscarAtenciones(ConsultaExternaDto consulExtDto, UsuarioDetalleDto usuarioDetalleDto);

	/**
	 * @param cveDiagnostico
	 * @param edad
	 * @param sexo
	 * @param edadSem
	 * @return
	 */
	SicCie10DTO buscarDiagnostico(String cveDiagnostico, int edad, String sexo, boolean edadSem);
	
	/**
	 * @param cveProcedimiento
	 * @param edad
	 * @param sexo
	 * @param edadAnn
	 * @return
	 */
	SicCie9DTO buscarProcedimiento(String cveProcedimiento, int edad, String sexo, boolean edadAnn);

	/**
	 * @param cveMetodoPPF
	 * @return
	 */
	Integer buscarCantidad(Integer cveMetodoPPF);

	/**
	 * @param cveDiagnostico
	 * @return
	 */
	String buscarDiagMorfologia(String cveDiagnostico);

	Boolean isTumor(String cveDiagnostico);
	
	SicCie10Morfologia buscarMorfologiaPorId(String cveMorfologia);
	
	SicEspecialidad buscarEspecialidad(String cveEspecialidad);
	
	SicCie10DTO buscarDiagnostico(String cveDiagnostico);

	/**
	 * @param consulExtDto
	 * @return
	 */
	ConsultaExternaDto buscarAtenEdicion(ConsultaExternaDto consulExtDto);

	/**
	 * @param numAnioPeriodo
	 * @param numMesPeriodo
	 * @param fechaCap
	 * @return
	 */
	boolean validaPeriodo(Integer numAnioPeriodo, String numMesPeriodo, Date fechaCap, String cvePresupuestal, Integer fechaDia);
	
	List<SicInformacionAdicionalVIH> buscarTodosInfAdiVIH();
	
	/**
	 *  Servicio que manda a llamar una búsqueda de una atención repetida.
	 * @param cveNss Nss del paciente
	 * @param cveAgMedico Agregado médico del Paciente
	 * @param cveDxP Diagnóstico Principal del Paciente
	 * @param idCE id del objeto en BD
	 * @return
	 */
	Boolean buscarAtencionRepetida(String cveNss, String cveAgMedico, String cveDxP, String idCE);

	/**
	 * Método que realiza la inserción de un registro en SIT_CONTROL_CAPTURA_CE cuando una atencióin es
	 * creada o actualizada
	 * 
	 * @param actualizado El resultado si se realizo el update
	 * @param consultaExterna Valores  para llenar el registro
	 */
	void actualizarControlCapturaCe(Boolean actualizado, SitConsultaExterna consultaExterna);
	
	/**
	 * Método que valida si en número consecutivo de la atención que se quiere guardar, no se 
	 * encuentra duplicado
	 *  
	 * @param consulExtDto
	 */
	Integer validarConsecutivoConsulta(ConsultaExternaDto consulExtDto);
	
	String buscarPeriodoLiberacion(String cveConstante);
	
	void insertar(ControlMedicoEncabezadoDto controlMedicoDto);

	Boolean actualizar(ControlMedicoEncabezadoDto controlMedicoDto);

	ControlMedicoEncabezadoDto buscar(String matriculaMedico);
	
	Boolean actualizaNombreMedico(ConsultaExternaDto consultActualizaDto);
	List<ConsultaExternaDto> buscarAtencionesParaActualiza(ConsultaExternaDto consulExtDto, UsuarioDetalleDto usuarioDetalleDto);


}
