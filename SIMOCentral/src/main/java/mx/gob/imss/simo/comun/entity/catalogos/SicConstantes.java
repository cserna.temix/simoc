package mx.gob.imss.simo.comun.entity.catalogos;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author S@m
 * 
 */
@Entity("SIC_CONSTANTES")
public class SicConstantes {

	@Id
	private ObjectId id;
	private String cveConstante;
	private String refValor;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getCveConstante() {
		return cveConstante;
	}

	public void setCveConstante(String cveConstante) {
		this.cveConstante = cveConstante;
	}

	public String getRefValor() {
		return refValor;
	}

	public void setRefValor(String refValor) {
		this.refValor = refValor;
	}
}
