/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicCie10Dao;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie10;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicCie10Dao")
public class SicCie10DaoImpl implements SicCie10Dao {

	final static Logger logger = Logger.getLogger(SicCie10DaoImpl.class.getName());

//	@Autowired
//	private ConnectionMongo connectionMongo;

	public SicCie10DaoImpl() {
	}

	@Override
	public List<SicCie10> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicCie10> listCie10 = new ArrayList<SicCie10>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listCie10 = dsEsp.createQuery(SicCie10.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listCie10;
	}

	@Override
	public SicCie10 buscarPorId(String cveCie10) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		SicCie10 cie10Item = null;
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SicCie10> query = ds.createQuery(SicCie10.class);
			query.and(query.criteria("cveCie10").equal(cveCie10), query.criteria("indVigente").equal(1));
			cie10Item = query.get();

		} catch (Exception e) {
			e.getMessage();
		}
		return cie10Item;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicCie10Dao#burcarEdadSexo(java.lang.Integer,
	 * java.lang.String)
	 */
	@Override
	public List<SicCie10> burcarEdadSexo(String cveDiagnostico, int edad, Integer sexo, boolean edadSem) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicCie10> listCie10 = new ArrayList<SicCie10>();
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SicCie10> query = ds.createQuery(SicCie10.class);
			query.field("cveCie10").equal(cveDiagnostico);
			if (edadSem) {
				query.filter("numEdadInferiorSemanas <=", edad);
				query.filter("numEdadSuperiorSemanas >=", edad);
			} else {
				query.filter("numEdadInferiroAnios <=", edad);
				query.filter("numEdadSuperiorAnios >=", edad);
			}
			query.or(query.criteria("indSexo").equal(sexo), query.criteria("indSexo").equal(0));
			//logger.info(query.toString());
			listCie10 = query.asList();

		} catch (Exception e) {
			logger.info("ERROR: " + e);
		}

		return listCie10;
	}

	@Override
	public SicCie10 buscarPorRangoPorId(String cveCie10, String rangoInicio, String rangoFin) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		SicCie10 cie10Item = null;
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();
			Query<SicCie10> query = ds.createQuery(SicCie10.class);
			query.and(query.criteria("cveCie10").greaterThanOrEq(rangoInicio),
					query.criteria("cveCie10").lessThanOrEq(rangoFin), query.criteria("cveCie10").equal(cveCie10),
					query.criteria("indVigente").equal(1));
			cie10Item = query.get();
		} catch (Exception e) {
			e.getMessage();
		}
		return cie10Item;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
