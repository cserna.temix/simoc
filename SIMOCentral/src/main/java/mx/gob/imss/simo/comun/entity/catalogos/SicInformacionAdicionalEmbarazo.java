package mx.gob.imss.simo.comun.entity.catalogos;
 

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "SIC_INFORMACION_ADICIONAL_EMBARAZO")
public class SicInformacionAdicionalEmbarazo {

    @Id
  	private ObjectId _id;
  	private Integer cveInfoAdicionalEmb;
  	private String desInfoAdicionalEmb;
  	private Date fecBaja;
  	
	public SicInformacionAdicionalEmbarazo() {
	    }

	public org.bson.types.ObjectId get_id() {
		return _id;
	} 

	public void set_id(org.bson.types.ObjectId _id) {
		this._id = _id;
	}

	public java.lang.Integer getCveInfoAdicionalEmb() {
		return cveInfoAdicionalEmb;
	} 

	public void setCveInfoAdicionalEmb(java.lang.Integer cveInfoAdicionalEmb) {
		this.cveInfoAdicionalEmb = cveInfoAdicionalEmb;
	}

	public java.lang.String getDesInfoAdicionalEmb() {
		return desInfoAdicionalEmb;
	} 

	public void setDesInfoAdicionalEmb(java.lang.String desInfoAdicionalEmb) {
		this.desInfoAdicionalEmb = desInfoAdicionalEmb;
	}

	public Date getFecBaja() {
		return fecBaja;
	}

	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}