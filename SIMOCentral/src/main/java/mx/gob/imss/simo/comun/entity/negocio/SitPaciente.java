/**
 * 
 */
package mx.gob.imss.simo.comun.entity.negocio;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIT_PACIENTE")
public class SitPaciente {

	@Id
	private ObjectId id;
	private String refAgregadoMedico;
	private String refApellidoPaterno;
	private String refApellidoMaterno;
	private String refNombre;
	private String numNss;
	private Date fecNacimiento;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the refAgregadoMedico
	 */
	public String getRefAgregadoMedico() {
		return refAgregadoMedico;
	}

	/**
	 * @param refAgregadoMedico
	 *            the refAgregadoMedico to set
	 */
	public void setRefAgregadoMedico(String refAgregadoMedico) {
		this.refAgregadoMedico = refAgregadoMedico;
	}

	/**
	 * @return the refApellidoPaterno
	 */
	public String getRefApellidoPaterno() {
		return refApellidoPaterno;
	}

	/**
	 * @param refApellidoPaterno
	 *            the refApellidoPaterno to set
	 */
	public void setRefApellidoPaterno(String refApellidoPaterno) {
		this.refApellidoPaterno = refApellidoPaterno;
	}

	/**
	 * @return the refApellidoMaterno
	 */
	public String getRefApellidoMaterno() {
		return refApellidoMaterno;
	}

	/**
	 * @param refApellidoMaterno
	 *            the refApellidoMaterno to set
	 */
	public void setRefApellidoMaterno(String refApellidoMaterno) {
		this.refApellidoMaterno = refApellidoMaterno;
	}

	/**
	 * @return the refNombre
	 */
	public String getRefNombre() {
		return refNombre;
	}

	/**
	 * @param refNombre
	 *            the refNombre to set
	 */
	public void setRefNombre(String refNombre) {
		this.refNombre = refNombre;
	}

	/**
	 * @return the numNss
	 */
	public String getNumNss() {
		return numNss;
	}

	/**
	 * @param numNss
	 *            the numNss to set
	 */
	public void setNumNss(String numNss) {
		this.numNss = numNss;
	}

	/**
	 * @return the fecNacimiento
	 */
	public Date getFecNacimiento() {
		return fecNacimiento;
	}

	/**
	 * @param fecNacimiento
	 *            the fecNacimiento to set
	 */
	public void setFecNacimiento(Date fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

}
