/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicRiesgoTrabajo;

/**
 * @author francisco.rrios
 * 
 */
public interface SicRiesgosTrabajoDao {

	/**
	 * @return
	 */
	List<SicRiesgoTrabajo> burcarTodos();

	/**
	 * @param cveRiesgoTrabajo
	 * @return
	 */
	List<SicRiesgoTrabajo> burcarPorId(Integer cveRiesgoTrabajo);
}
