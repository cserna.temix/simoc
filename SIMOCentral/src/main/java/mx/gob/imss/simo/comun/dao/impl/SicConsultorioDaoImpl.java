/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicConsultorioDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicConsultorio;

/**
 * @author francisco.rodriguez
 * 
 */
@Component("sicConsultorioDao")
public class SicConsultorioDaoImpl implements SicConsultorioDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.comun.dao.SicConsultorioDao#burcarTodos()
	 */
	@Override
	public List<SicConsultorio> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicConsultorio> listConsultorio = new ArrayList<SicConsultorio>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listConsultorio = dsEsp.createQuery(SicConsultorio.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listConsultorio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicConsultorioDao#burcarPorId(java.lang.Integer
	 * )
	 */
	@Override
	public List<SicConsultorio> burcarPorId(String cveConsultorio) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicConsultorio> listConsultorio = new ArrayList<SicConsultorio>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listConsultorio = dsEsp.createQuery(SicConsultorio.class)
					.field("cveConsultorio").equal(cveConsultorio).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listConsultorio;
	}
	
	

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicConsultorioDao#burcarPorUnidadEsp(java.lang.String, java.lang.String)
	 */
	@Override
	public List<SicConsultorio> burcarPorUnidadEsp(String cvePresupuestal,
			Integer tipoServicio) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicConsultorio> listConsultorio = new ArrayList<SicConsultorio>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listConsultorio = dsEsp.createQuery(SicConsultorio.class)					
					.field("cvePresupuestal").equal(cvePresupuestal)
					.field("numTipoServicio").equal(tipoServicio).asList();
					

		} catch (Exception e) {
			e.getMessage();
		}
		return listConsultorio;
	}

	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicConsultorioDao#burcarPorIdUnidadEsp(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<SicConsultorio> burcarPorIdUnidadEsp(String cveConsultorio,
			String cvePresupuestal, String cveEspecialidad) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicConsultorio> listConsultorio = new ArrayList<SicConsultorio>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listConsultorio = dsEsp.createQuery(SicConsultorio.class)
					.field("cveConsultorio").equal(cveConsultorio)
					.field("cvePresupuestal").equal(cvePresupuestal)
					.field("cveEspecialidad").equal(cveEspecialidad).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listConsultorio;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
