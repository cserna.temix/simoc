/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicInformacionAdicionalVIHDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicionalVIH;

/**
 * @author sm
 * 
 */
@Repository("sicInformacionAdicionalVIHDao")
public class SicInformacionAdicionalVIHDaoImpl implements
		SicInformacionAdicionalVIHDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicInformacionAdicionalVIHDao#burcarTodos()
	 */
	@Override
	public List<SicInformacionAdicionalVIH> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicInformacionAdicionalVIH> listInformacionAdicionalVIH = new ArrayList<SicInformacionAdicionalVIH>();
		try {
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listInformacionAdicionalVIH = dsEsp.createQuery(
					SicInformacionAdicionalVIH.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listInformacionAdicionalVIH;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicInformacionAdicionalVIHDao#burcarPorId()
	 */
	@Override
	public SicInformacionAdicionalVIH burcarVIHPorId(Integer cveCD4) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		SicInformacionAdicionalVIH informacionAdicionalVIH = new SicInformacionAdicionalVIH();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
		
			informacionAdicionalVIH = ds.createQuery(SicInformacionAdicionalVIH.class).field("cveInfoAdicionalVIH").equal(cveCD4).get();

		} catch (Exception e) {
			e.getMessage();
		}
		return informacionAdicionalVIH;
	}
	
}
