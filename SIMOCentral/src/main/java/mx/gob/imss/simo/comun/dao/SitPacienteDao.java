/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.negocio.SitPaciente;

/**
 * @author francisco.rrios
 * 
 */
public interface SitPacienteDao {

	/**
	 * @return
	 */
	List<SitPaciente> burcarTodos();


	/**
	 * @param nss
	 * @param agregadoMedico
	 * @return
	 */
	List<SitPaciente> burcarPorId(String nss, String agregadoMedico);
}
