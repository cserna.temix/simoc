package mx.gob.imss.simo.utils;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.entity.catalogos.SicConstantes;

public class ActiveVigEpiServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(ActiveVigEpiServlet.class.getName());

	final String ACTIVO = "ACTIVO";
	final String CLAVE = "CE_VIGEPI";

	public void init() throws ServletException {
		logger.info("iniciando servlet");
		// SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
			Query<SicConstantes> updateQuery = ds.createQuery(SicConstantes.class).field("cveConstante").equal(CLAVE);
			UpdateOperations<SicConstantes> ops = ds.createUpdateOperations(SicConstantes.class).set("refValor",
					ACTIVO);
			ds.update(updateQuery, ops);
		} catch (Exception e) {
			e.getMessage();
		}
	}
}
