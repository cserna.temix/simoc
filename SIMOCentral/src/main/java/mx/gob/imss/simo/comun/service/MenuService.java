/**
 * 
 */
package mx.gob.imss.simo.comun.service;

import java.util.List;

import mx.gob.imss.simo.comun.dto.MenuDto;

import org.primefaces.model.menu.MenuModel;

public interface MenuService {

	List<MenuDto> getMenu(String rol);
	MenuModel createMenu(String perfil);
}
