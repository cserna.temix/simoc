/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.presentacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.SelectEvent;

import mx.gob.imss.simo.comun.core.AbstractBaseBean;
import mx.gob.imss.simo.comun.dto.CatalogoDto;
import mx.gob.imss.simo.comun.dto.SiapDto;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.enums.EnumModuloAtencion;
import mx.gob.imss.simo.comun.enums.EnumNavegacion;
import mx.gob.imss.simo.comun.service.CatalogosService;
import mx.gob.imss.simo.model.ConsultaExternaDto;
import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdMedico;
import mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService;
import mx.gob.imss.simo.model.ObjetosEnSesionBean;
import mx.gob.imss.simo.ws.service.AccederWsService;

/**
 * @author francisco.rrios
 * 
 */

@ManagedBean(name = "editarAtencionCExBean")
@ViewScoped
public class EditarAtencionBean extends AbstractBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(EditarAtencionBean.class.getName());
	
	@ManagedProperty("#{catalogosService}")
	private CatalogosService catalogosService;

	@ManagedProperty("#{consultaExternaService}")
	private ConsultaExternaService consultaExternaService;

	@ManagedProperty("#{objetosSs}")
	ObjetosEnSesionBean objetosSs;
	
	@ManagedProperty("#{accederService}")
	private AccederWsService accederService;


	
	private ConsultaExternaDto consulExtDto;
	private List<CatalogoDto> listadoEspecialidad;
	private List<String> listaEspecialidadAc;
	private AutoComplete especialidadAcPf;
	
	private List<CatalogoDto> listadoAgreMed;
	private List<String> listaRegimenAc;
	private AutoComplete regimenAcPf;
	
	private List<CatalogoDto> listadoPrimeraVez;
	private List<ConsultaExternaDto> listEditConExt;
	private List<ConsultaExternaDto> listEditConExtActualiza;
	private ConsultaExternaDto selConsultaExternaDto;
	private String derecho;
	private String sexo;
	private String anno;
	private String regimen;

	/**
	 * SGMP_F07_DST_F23_EspCU_SIMO_EditarAtnCE
	 */

	private InputMask fechaPf;
	private InputText noSsInPf;
	private InputText matMedInPf;
	private InputText matMednommedPf;
	private InputText agMeCalidadPf;
	private InputText agMeSexoPf;
	private InputText agMeAnioPf;
	//private SelectOneMenu agMeRegPf;
	private SelectOneMenu especialidadPf;
	private InputText codCie10Pf;
	private SelectOneMenu ocasServPf;

	
	private CommandButton salirBtPf;
	
	private final Integer MININO_CIE10 = 4;
	SiapDto responseSiap=null;
	
	private boolean deshabilitadoMedS;
	private boolean deshabilitadoMedA;
	private boolean deshabilitadoMedR;


	@PostConstruct
	private void inicio() {
		cargaInicial();
		listadoEspecialidad = catalogosService.llenaComboEspeUnidad(objetosSs
				.getUsuarioDetalleDto().getCvePresupuestal()
				, EnumModuloAtencion.CONSULTA_EXTERNA.getClave());
		listadoAgreMed = catalogosService.llenaComboAgreMed();
		cargaListadosAutocomplete();
		llenarPrimeraVez();
	}

	public void cargaListadosAutocomplete(){
		
		//listadoEspecialidad
		CatalogoDto catalogoEspe=new CatalogoDto();
        Iterator<CatalogoDto> it = listadoEspecialidad.iterator();
        while(it.hasNext()){
        	catalogoEspe = it.next();
        	listaEspecialidadAc.add(catalogoEspe.getDescripcion());
            
        }
        CatalogoDto catalogoRegimen=new CatalogoDto();
        Iterator<CatalogoDto> it_regimen = listadoAgreMed.iterator();
        while(it_regimen.hasNext()){
        	catalogoRegimen = it_regimen.next();
        	listaRegimenAc.add(catalogoRegimen.getDescripcion());
            
        }
	}
	
	public void cargaInicial() {
		consulExtDto = new ConsultaExternaDto();
		listadoEspecialidad = new ArrayList<CatalogoDto>();
		listaEspecialidadAc=new ArrayList<String>();
		especialidadAcPf=new AutoComplete();
		listadoAgreMed = new ArrayList<CatalogoDto>();
		listaRegimenAc=new ArrayList<String>();
		listadoPrimeraVez = new ArrayList<CatalogoDto>();

		// objetos primefaces
		fechaPf = new InputMask();
		noSsInPf = new InputText();
		matMedInPf = new InputText();
		matMednommedPf = new InputText();
		agMeCalidadPf = new InputText();
		agMeSexoPf = new InputText();
		agMeAnioPf = new InputText();
		//agMeRegPf = new SelectOneMenu();
		regimenAcPf=new AutoComplete();
		especialidadPf = new SelectOneMenu();
		codCie10Pf = new InputText();
		ocasServPf = new SelectOneMenu();

		salirBtPf = new CommandButton();

		//inhabilitarCampos();

		// caracteristicas de los inputs
		matMedInPf.setMaxlength(10);
		noSsInPf.setMaxlength(10);
		agMeCalidadPf.setMaxlength(1);
		agMeSexoPf.setMaxlength(1);
		agMeAnioPf.setMaxlength(4);
		codCie10Pf.setMaxlength(4);

		tieneFoco = "idFecha";
		salirBtPf.setDisabled(Boolean.FALSE);
		deshabilitadoMedS = false;
		deshabilitadoMedA = false;
		deshabilitadoMedR = true;
		//llenarResultados();
	}

	public void inhabilitarCampos() {
		fechaPf.setDisabled(Boolean.FALSE);
		noSsInPf.setDisabled(Boolean.TRUE);
		matMedInPf.setDisabled(Boolean.TRUE);
		matMednommedPf.setDisabled(Boolean.TRUE);
		agMeCalidadPf.setDisabled(Boolean.TRUE);
		agMeSexoPf.setDisabled(Boolean.TRUE);
		agMeAnioPf.setDisabled(Boolean.TRUE);
		//agMeRegPf.setDisabled(Boolean.TRUE);
		regimenAcPf.setDisabled(Boolean.TRUE);
		especialidadPf.setDisabled(Boolean.TRUE);
		codCie10Pf.setDisabled(Boolean.TRUE);
		ocasServPf.setDisabled(Boolean.TRUE);
	}
	
	public void llenarResultados(){
		logger.info("Llena resultado");
		if(objetosSs.getBusquedaCex()){
			//fechaPf=objetosSs.getDatosBusquedaDto().getFechaPf();
			if(objetosSs.getCeDatosBusqueda()!=null){				
				if(objetosSs.getCeDatosBusqueda().getFecha()!=null){
					this.getFechaPf().setValue((objetosSs.getCeDatosBusqueda().getFecha()));
					if(fechaPf!=null){			
						validaFecha();
					}	
				}				
			}
			
			consulExtDto=objetosSs.getCeDatosBusqueda();
			buscar();
		}
		objetosSs.setCeDatosBusqueda(null);
		objetosSs.setBusquedaCex(Boolean.FALSE);
		
		
	}

	public String buscar() {
		try {
			consulExtDto.setAgregadoMedico(null);
			listEditConExt = new ArrayList<ConsultaExternaDto>();
			if(validaNulosONoAgregadoMedico() || !esVaciaMatriculaMedica()){
				if (validaAgregado()) {
					consulExtDto.setAgregadoMedico(derecho.concat(sexo.toUpperCase())
							.concat(anno).concat(regimen));
					
				}
				consulExtDto.setFecha((Date) fechaPf.getValue());
				listEditConExt = consultaExternaService.buscarAtenciones(consulExtDto, objetosSs.getUsuarioDetalleDto());
				listEditConExtActualiza=consultaExternaService.buscarAtencionesParaActualiza(consulExtDto, objetosSs.getUsuarioDetalleDto());
				for(ConsultaExternaDto consultExternaListado: listEditConExtActualiza) {
					 if(consultExternaListado.getSdEncabezado().get(0).getSdMedico().get(0).getRefNombre().equals("Servicio SIAP no disponible")) {
						 responseSiap= accederService.consultarSiap(consultExternaListado.getMatriculaMedico(),
									objetosSs.getUsuarioDetalleDto().getCvePresupuestal().substring(0, 2));						
							if(responseSiap!=null) {
								
									ConsultaExternaDto consultActualizaDto=new ConsultaExternaDto();
									consultActualizaDto.setId(consultExternaListado.getId());
									consultActualizaDto.setSdEncabezado(consultExternaListado.getSdEncabezado());
									consultActualizaDto.setSdUnidadMedica(consultExternaListado.getSdUnidadMedica());
									consultActualizaDto.getSdEncabezado().get(0).getSdMedico().get(0).setRefNombre(responseSiap.getNombres());
									consultActualizaDto.getSdEncabezado().get(0).getSdMedico().get(0).setRefApellidoPaterno(responseSiap.getaPaterno());
									consultActualizaDto.getSdEncabezado().get(0).getSdMedico().get(0).setRefApellidoMaterno(responseSiap.getaMaterno());
									consultaExternaService.actualizaNombreMedico(consultActualizaDto);	
							}					 
					 }
					
				}	
			}else{	
				if(esVaciaMatriculaMedica()) {
					agregarError("me141_valor_incorrecto");					
				}
				mensajeErrorConParametros("me18_valor_incorrecto",
						"Agregdo Medico");
			}
	
			return EnumNavegacion.CONSULTA_EXTERNA_BUSQ_ED.getValor();
		
		}catch (Exception e) {
			e.printStackTrace();
			return EnumNavegacion.CONSULTA_EXTERNA_BUSQ_ED.getValor();
		}
	}
	
		
	
	public void llenarResultadoListener(ActionEvent event){
		ConsultaExternaDto consulExtDtoRes = (ConsultaExternaDto)event.getComponent().getAttributes().get("datoConsulta");	
		SicEspecialidad  espec = consultaExternaService.buscarEspecialidad(consulExtDtoRes.getEspecialidad());
		if(espec!=null){
			consulExtDtoRes.setNumTipoServicio(espec.getNumTipoServicio());
		}
//		llenarCexBusqueda();
		objetosSs.setCeDatosBusqueda(consulExtDto);
		objetosSs.setConsultaExternaDtoSesion(new ConsultaExternaDto());	
		objetosSs.setConsultaExternaDtoSesion(consulExtDtoRes);
		objetosSs.getConsultaExternaDtoSesion().setEditable(Boolean.TRUE);
		objetosSs.getConsultaExternaDtoSesion().setConsultar(Boolean.FALSE);
		
	}
	
	public void llenarResulListenerCon(ActionEvent event){
		ConsultaExternaDto consulExtDtoRes = (ConsultaExternaDto)event.getComponent().getAttributes().get("datoConsulta");		
		objetosSs.setConsultaExternaDtoSesion(new ConsultaExternaDto());	
		objetosSs.setConsultaExternaDtoSesion(consulExtDtoRes);
		objetosSs.getConsultaExternaDtoSesion().setEditable(Boolean.TRUE);
		objetosSs.getConsultaExternaDtoSesion().setConsultar(Boolean.TRUE);
	}

	private void llenarPrimeraVez() {
		CatalogoDto primeravez = new CatalogoDto();
		primeravez.setClave(1);
		primeravez.setDescripcion("Primera Vez");
		CatalogoDto subsecuente = new CatalogoDto();
		subsecuente.setClave(0);
		subsecuente.setDescripcion("Subsecuente");
		listadoPrimeraVez.add(primeravez);
		listadoPrimeraVez.add(subsecuente);

	}

	public void validaFecha() {
		if (this.fechaPf.getValue() != null
				&& !this.fechaPf.getValue().toString().isEmpty()
				&& !this.fechaPf.getValue().toString().contains("_")) {
			componenteValido(this.fechaPf, this.matMedInPf);
			consulExtDto.setFecha((Date) fechaPf.getValue());
		} else {
			tieneFoco=fechaPf.getClientId();
			componenteNoValido(this.fechaPf);
			consulExtDto.setFecha(null);
			//agregarError("me01_requerido", "cxbet_fecha");
		}
	}
	
	public boolean esVaciaMatriculaMedica() {
		if (this.matMedInPf.getValue() != null
				&& !this.matMedInPf.getValue().toString().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public void validaMatricula() {
		deshabilitadoMedA = false;
		deshabilitadoMedS = false;
		deshabilitadoMedR = false;
		if (this.getMatMedInPf().getValue().toString().length() > 0) {
			String matricula=this.getMatMedInPf().getValue().toString();
			
//			if (ReglasNegocio
//					.validaMatricula(consulExtDto.getMatriculaMedico())) {
			if (matricula.length() > 5) {
//				String matricula = this.getMatMedInPf().getValue().toString();
//				while (matricula.length() < 10) {
//					String cadAux = "0";
//					cadAux = cadAux.concat(matricula);
//					matricula = cadAux;
//				}
				consulExtDto.setMatriculaMedico(matricula);
				SiapDto responseSiap = accederService.consultarSiap(
						consulExtDto.getMatriculaMedico(), objetosSs.getUsuarioDetalleDto().getCveDelegacionUmae());
				if (responseSiap != null) {
					if(responseSiap.getEncontrado()){
						consulExtDto.setNombreMedico(responseSiap.getNombres()
								.concat(" ").concat(responseSiap.getaPaterno())
								.concat(" ").concat(responseSiap.getaMaterno()));
						consulExtDto.setNombresMedi(responseSiap.getNombres());
						consulExtDto.setaPaternoMedi(responseSiap.getaPaterno());
						consulExtDto.setaMaternoMedi(responseSiap.getaMaterno());
						this.getMatMednommedPf().setValue(
							consulExtDto.getNombreMedico());
						deshabilitadoMedS = true;
					} else {
						deshabilitadoMedR = true;
					}
				} else {
					deshabilitadoMedA = true;
				}
				this.getMatMedInPf().setValue(consulExtDto.getMatriculaMedico());
				// validaMatricula = true;
				// posicion = 3;
				// foco = "prestador";
				//componenteValido(this.getMatMedInPf(), this.getEspecialidadPf());
				componenteValido(this.getMatMedInPf(), this.getEspecialidadAcPf());
//				this.getMatMedInPf().setStyle("");
//				this.setUpdateFoco("");
			} else {
				componenteNoValido(this.getMatMedInPf());
				this.getMatMednommedPf().setValue("");
				mensajeErrorConParametros("me18_valor_incorrecto", "Matrícula");
			}
		} else {
			componenteValido(this.getMatMedInPf(),  this.getEspecialidadAcPf());
			this.getMatMednommedPf().setValue("");
			this.getMatMedInPf().setValue("");
			consulExtDto.setMatriculaMedico(null);
			
//			this.getMatMedInPf().setStyle("");
//			this.setUpdateFoco("");
		}
	}

		
	public List<String> completeTextRegimen(String query) { 
		List<String> resultado = new ArrayList<String>();
		resultado=completeTextComponente(query,2,listaRegimenAc);
		return resultado;
	}
	
	public void onItemSelectRegimen(SelectEvent event){
		CatalogoDto objeto=null;
		objeto=itemSeleccionado(event,listaRegimenAc,regimenAcPf,listadoAgreMed);
		if(objeto!=null){
			//componenteValido(this.getEspecialidadAcPf(), this.getNoSsInPf());
			validaAgMedReg();
		}else{
			//componenteNoValido(this.getEspecialidadAcPf());
		}
	}
		
	public void validaAutocRegimen() {
		CatalogoDto objeto=null;
		objeto=validaAutoComplete(regimenAcPf,2,listaRegimenAc,listadoAgreMed);
		if(objeto!=null){
			 validaAgMedReg();	
		}
		
	}
		
	//autocomplete especialidad
	
	public List<String> completeTextEspecialidad(String query) {
		List<String> resultado = new ArrayList<String>();
		resultado=completeTextComponente(query,4,listaEspecialidadAc);
		return resultado;
    }
	
	
	public void onItemSelectEspecialidad(SelectEvent event) {
		
		CatalogoDto objeto=null;
		
		objeto=itemSeleccionado(event,listaEspecialidadAc,especialidadAcPf,listadoEspecialidad);
		if(objeto!=null){
			componenteValido(this.getEspecialidadAcPf(), this.getNoSsInPf());
			consulExtDto.setEspecialidad(objeto.getClaveDes());
		}else{
			consulExtDto.setEspecialidad(null);
		}
    }
	
	public void validaAutocEspecialidad(){
		if(especialidadAcPf.getValue()!=null && !especialidadAcPf.getValue().toString().isEmpty()){
			CatalogoDto objeto=null;
			objeto=validaAutoComplete(especialidadAcPf,4,listaEspecialidadAc,listadoEspecialidad);
			if(objeto!=null){
				componenteValido(this.getEspecialidadAcPf(), this.getNoSsInPf());
				consulExtDto.setEspecialidad(objeto.getClaveDes());
			}else{
				//componenteValido(this.getEspecialidadAcPf(), this.getNoSsInPf());
				this.setTieneFoco(especialidadAcPf.getClientId());
				consulExtDto.setEspecialidad(null);
				
			}
		}else{
			componenteValido(this.getEspecialidadAcPf(), this.getNoSsInPf());
			consulExtDto.setEspecialidad(null);
		}
		
	}

	
	public void validaEspecialidad() {
		if (this.getEspecialidadPf().getValue() != null && !this.getEspecialidadPf().getValue().toString().isEmpty()) {
			consulExtDto.setEspecialidad(this.getEspecialidadPf().getValue()
					.toString());
			componenteValido(this.getEspecialidadPf(), this.getNoSsInPf());
//			getEspecialidadPf().setStyle("");
//			this.setUpdateFoco("");
		} else {
			consulExtDto.setEspecialidad(null);
			componenteValido(this.getEspecialidadPf(), this.getNoSsInPf());
			
//			getEspecialidadPf().setStyle("");
//			this.setUpdateFoco("");
		}
	}
	
	//

	public void validaNoSs() {
		if (this.getNoSsInPf().getValue() != null
				&& !this.getNoSsInPf().getValue().toString().isEmpty()) {
			consulExtDto.setNss(this.getNoSsInPf().getValue().toString());
			if (consulExtDto.getNss().length() != 10) {
				mensajeErrorConParametros("me18_valor_incorrecto", "NSS");
				//componenteNoValido(getNoSsInPf());
				consulExtDto.setNss("");
			} else {

				componenteValido(getNoSsInPf(), getAgMeCalidadPf());
				
//				this.getNoSsInPf().setStyle("");
//				this.setUpdateFoco("");
			}
		} else {
			componenteValido(getNoSsInPf(), getAgMeCalidadPf());
			consulExtDto.setNss(null);
//			this.getNoSsInPf().setStyle("");
//			this.setUpdateFoco("");
		}

	}
	
	public Boolean validaNulosONoAgregadoMedico(){
		Boolean validoNulos=Boolean.FALSE;
				
		Boolean calidadBool=Boolean.FALSE;
		if(this.getAgMeCalidadPf().getValue() != null
				&& !this.getAgMeCalidadPf().getValue().toString().isEmpty() ){
			calidadBool=Boolean.TRUE;
		}else{
			calidadBool=Boolean.FALSE;
		}
		Boolean sexoBool=Boolean.FALSE;
		if(this.getAgMeSexoPf().getValue() != null
				&& !this.getAgMeSexoPf().getValue().toString().isEmpty() ){
			sexoBool=Boolean.TRUE;
		}else{
			sexoBool=Boolean.FALSE;
		}
		Boolean anioBool=Boolean.FALSE;
		if(this.getAgMeAnioPf().getValue() != null
				&& !this.getAgMeAnioPf().getValue().toString().isEmpty()){
			anioBool=Boolean.TRUE;
		}else{
			anioBool=Boolean.FALSE;
		}
		Boolean regBool=Boolean.FALSE;
		if(this.getRegimenAcPf().getValue() != null
				&& !this.getRegimenAcPf().getValue().toString().isEmpty()){
			regBool=Boolean.TRUE;
		}else{
			regBool=Boolean.FALSE;
		}
		
		
		if(calidadBool && sexoBool && anioBool && regBool){
			validoNulos=Boolean.TRUE;
		}
		if(!calidadBool && !sexoBool && !anioBool && !regBool){
			validoNulos=Boolean.TRUE;
		}

		if(validoNulos){
			return Boolean.TRUE;
		}else{
			return Boolean.FALSE;
		}
		
		
		
		
		
	}

	public Boolean validaAgregado() {
		
		boolean correcto = true;
		if(this.getAgMeCalidadPf().getValue() != null
				&& !this.getAgMeCalidadPf().getValue().toString().isEmpty() && this.getAgMeSexoPf().getValue() != null
						&& !this.getAgMeSexoPf().getValue().toString().isEmpty()
						&& this.getAgMeSexoPf().getValue() != null
						&& !this.getAgMeSexoPf().getValue().toString().isEmpty()
						&& this.getAgMeAnioPf().getValue() != null
						&& !this.getAgMeAnioPf().getValue().toString().isEmpty()
						&& this.getRegimenAcPf().getValue() != null
						&& !this.getRegimenAcPf().getValue().toString().isEmpty()){
			derecho = this.getAgMeCalidadPf().getValue().toString();
			sexo = this.getAgMeSexoPf().getValue().toString();
			anno = this.getAgMeAnioPf().getValue().toString();
			regimen = this.getRegimenAcPf().getValue().toString();
			String cadena = "";
			

			if (derecho != null) {
				if (consulExtDto.getAgregadoMedico() == null) {
					consulExtDto.setAgregadoMedico(derecho);
				} else {
					cadena = derecho.concat(consulExtDto.getAgregadoMedico()
							.substring(1));
					consulExtDto.setAgregadoMedico(cadena);
				}
			}
			if (sexo != null) {
				if (consulExtDto.getAgregadoMedico().length() == 1) {
					cadena = consulExtDto.getAgregadoMedico().concat(
							sexo.toUpperCase());
					consulExtDto.setAgregadoMedico(cadena);
				} else {
					cadena = consulExtDto.getAgregadoMedico().substring(0, 1)
							.concat(sexo.toUpperCase())
							.concat(consulExtDto.getAgregadoMedico().substring(2));
					consulExtDto.setAgregadoMedico(cadena);
				}
			}
			if (anno != null) {
				if (consulExtDto.getAgregadoMedico().length() == 2) {
					cadena = consulExtDto.getAgregadoMedico().concat(anno);
					consulExtDto.setAgregadoMedico(cadena);
				} else {
					cadena = consulExtDto.getAgregadoMedico().substring(0, 2)
							.concat(anno)
							.concat(consulExtDto.getAgregadoMedico().substring(6));
					consulExtDto.setAgregadoMedico(cadena);
				}
			}
		}else{
			correcto=false;


		}
			
		
		if (regimen != null) {
			if (consulExtDto.getAgregadoMedico().length() == 8) {
				consulExtDto.setAgregadoMedico(consulExtDto.getAgregadoMedico()
						.substring(0, 6));
				consulExtDto.setAgregadoMedico(consulExtDto.getAgregadoMedico()
						.concat(regimen));
			} else {
				consulExtDto.setAgregadoMedico(consulExtDto.getAgregadoMedico()
						.concat(regimen));
			}
			// Regla: RN09 <Agregado mÃ©dico para poblaciÃ³n abierta>
			if (Integer.parseInt(consulExtDto.getAgregadoMedico().substring(0,
					1)) == 0) {
				if (!regimen.equals("ND")) {
					correcto = false;
				}

			} else if (Integer.parseInt(consulExtDto.getAgregadoMedico()
					.substring(0, 1)) == 1) {// Regla: RN16 <DÃ­as de
												// incapacidad>
												// if (!regimen.equals("OR")) {
				// diasInca = false;
				// } else {
				// diasInca = true;
				// }
			}
			// else {
			// diasInca = false;
			// }
			if (correcto) {
				if (consulExtDto.getAgregadoMedico() != "") {
					// deshabilitadoAgre = true;
					// color = "6CEE38";
					componenteValido(this.getRegimenAcPf(), this.getCodCie10Pf());
					this.getRegimenAcPf().setStyle("");
					//this.setUpdateFoco("");
					try {
						// consultaExternaService = (ConsultaExternaService)
						// obtenerService("consultaExternaService");
						consultaExternaService.cargarNombreUnidad(consulExtDto);

						// if (consulExtDto.getNombre() != null
						// && consulExtDto.getApePaterno() != null
						// && consulExtDto.getApeMaterno() != null) {
						// datosDerecho = true;
						// deshabilitadoNom = true;
						// } else {
						// datosDerecho = false;
						// deshabilitadoNom = false;
						// }

						// if (consulExtDto.getNombreUnidadMed() != null) {
						// datosUniAds = true;
						// } else {
						// datosUniAds = false;
						// }
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					// deshabilitadoAgre = false;
					// color = "FFFFFF";
					componenteValido(this.getRegimenAcPf(), this.getCodCie10Pf());
//					this.getAgMeRegPf().setStyle("");
//					this.setUpdateFoco("");
				}
			} else {
				mensajeErrorConParametros("me18_valor_incorrecto",
						"Agregdo Medico");

			}
		}
		return correcto;

	}

	public void validaAgMedCalidad() {
		if (this.getAgMeCalidadPf().getValue() != null
				&& !this.getAgMeCalidadPf().getValue().toString().isEmpty()) {
			componenteValido(this.getAgMeCalidadPf(), this.getAgMeSexoPf());
//			this.getAgMeCalidadPf().setStyle("");
//			this.setUpdateFoco("");
		} else {
			componenteValido(this.getAgMeCalidadPf(), this.getCodCie10Pf());
			this.setDerecho(null);
			limpiaAgMed();
//			this.getAgMeCalidadPf().setStyle("");
//			this.setUpdateFoco("");
		}

	}

	public void validaAgMedSexo() {
		if (this.getAgMeSexoPf().getValue() != null
				&& !this.getAgMeSexoPf().getValue().toString().isEmpty()) {
			componenteValido(this.getAgMeSexoPf(), this.getAgMeAnioPf());
//			this.setUpdateFoco("");
		} else {
			this.setSexo(null);
//			this.getAgMeSexoPf().setStyle("");

		}

	}

	public void validaAgMedAnio() {
		if (this.getAgMeAnioPf().getValue() != null
				&& !this.getAgMeAnioPf().getValue().toString().isEmpty()) {
			String annos = this.getAgMeAnioPf().getValue().toString();
			if (annos.length() < 4) {
				//componenteNoValido(getAgMeAnioPf());
//				this.getAgMeAnioPf().setStyle("");
				mensajeErrorConParametros("me18_valor_incorrecto", "Año");
			}else{
				this.getAgMeAnioPf().setValue(annos);
				componenteValido(this.getAgMeAnioPf(), this.getRegimenAcPf());
//				this.getAgMeAnioPf().setStyle("");
			}	
		} else {
			this.setAnno(null);
			//componenteNoValido(getAgMeAnioPf());
//			this.getAgMeAnioPf().setStyle("");
			
		}

	}

//	public void validaAgMedReg() {
//		if (this.getAgMeRegPf().getValue() != null
//				&& !this.getAgMeRegPf().getValue().toString().isEmpty()) {
//			validaAgregado();
//		} else {
//			this.setRegimen(null);
//			//componenteNoValido(getAgMeRegPf());
//			//this.getAgMeAnioPf().setStyle("");
//		}
//
//	}
	public void validaAgMedReg() {
		if (this.getRegimenAcPf().getValue() != null
				&& !this.getRegimenAcPf().getValue().toString().isEmpty()) {
			validaAgregado();
		} else {
			this.setRegimen(null);
			//componenteNoValido(getAgMeRegPf());
			//this.getAgMeAnioPf().setStyle("");
		}

	}

	public void limpiaAgMed(){
		this.setDerecho(null);
		this.setSexo(null);
		this.setRegimen(null);
		this.setAnno(null);
		agMeCalidadPf.resetValue();
		agMeSexoPf.resetValue();
		agMeAnioPf.resetValue();
		//agMeRegPf.resetValue();
		regimenAcPf.resetValue();
	}
	
	public void validaCIE10() {
		if (this.getCodCie10Pf().getValue() != null
				&& !this.getCodCie10Pf().getValue().toString().isEmpty()) {
			if(this.getCodCie10Pf().getValue().toString().length() < MININO_CIE10){
				this.getCodCie10Pf().setValue(null);
				agregarError("me14_clave_no_existe");
			} else {			
				consulExtDto.setCodigoCie(this.getCodCie10Pf().getValue()
						.toString());
				componenteValido(this.getCodCie10Pf(), this.getOcasServPf());				
			}
		} else {
			componenteValido(this.getCodCie10Pf(), this.getOcasServPf());
			consulExtDto.setCodigoCie(null);
//			this.getCodCie10Pf().setStyle("");
//			this.setUpdateFoco("");
		}

	}

	public void validaOcasServ() {
		//this.getOcasServPf().setDisabled(Boolean.TRUE);
		if(ocasServPf.getValue()!=null){
			consulExtDto.setPrimeraVez(Integer.parseInt(ocasServPf.getValue().toString()));
			this.getOcasServPf().setStyle("");
			this.setTieneFoco("id_btn_buscar");
		}else{
			this.getOcasServPf().setStyle("");
			this.setTieneFoco("id_btn_buscar");
			consulExtDto.setPrimeraVez(null);
		}
		

	}

	
	public void espec_validaRequerido(){
		if(especialidadPf.getValue()==null){
			componenteNoValidoSelectFocus(especialidadPf);
		}	
	}
	public void ocas_validaRequerido(){
		if(ocasServPf.getValue()==null){			
			componenteNoValidoSelectFocus(ocasServPf);
		}
	}
	



	public String cancelar() {
		limpiarCampos();
		
		return null;

	}

	public void limpiarCampos() {
		consulExtDto = null;
		
		consulExtDto = new ConsultaExternaDto();
		consulExtDto.setAgregadoMedico(null);
		this.setSexo(null);
		this.setDerecho(null);
		this.setAnno(null);
		this.setRegimen(null);
		fechaPf.resetValue();
		noSsInPf.resetValue();
		noSsInPf.setStyleClass("");
		this.noSsInPf.setValue("");
		matMedInPf.setStyle("");
		matMedInPf.resetValue();
		this.getMatMedInPf().setValue("");
		matMednommedPf.resetValue();
		agMeCalidadPf.resetValue();
		agMeSexoPf.resetValue();
		agMeAnioPf.resetValue();
		//agMeRegPf.resetValue();
		regimenAcPf.resetValue();
		especialidadAcPf.setValue(null);
		especialidadAcPf.resetValue();
		especialidadPf.resetValue();
		
		codCie10Pf.resetValue();
		ocasServPf.setValue("");
		tieneFoco = "idFecha";
//		accionBtPf.setDisabled(Boolean.TRUE);
//		cancelarBtPf.setDisabled(Boolean.TRUE);
		salirBtPf.setDisabled(Boolean.FALSE);
		listEditConExt = new ArrayList<ConsultaExternaDto>();
		// cargaInicial();

		
		
	}
	
	

	/**
	 * @return the catalogosService
	 */
	public CatalogosService getCatalogosService() {
		return catalogosService;
	}

	/**
	 * @param catalogosService
	 *            the catalogosService to set
	 */
	public void setCatalogosService(CatalogosService catalogosService) {
		this.catalogosService = catalogosService;
	}

	/**
	 * @return the consultaExternaService
	 */
	public ConsultaExternaService getConsultaExternaService() {
		return consultaExternaService;
	}

	/**
	 * @param consultaExternaService
	 *            the consultaExternaService to set
	 */
	public void setConsultaExternaService(
			ConsultaExternaService consultaExternaService) {
		this.consultaExternaService = consultaExternaService;
	}

	/**
	 * @return the objetosSs
	 */
	public ObjetosEnSesionBean getObjetosSs() {
		return objetosSs;
	}

	/**
	 * @param objetosSs
	 *            the objetosSs to set
	 */
	public void setObjetosSs(ObjetosEnSesionBean objetosSs) {
		this.objetosSs = objetosSs;
	}

	/**
	 * @return the consulExtDto
	 */
	public ConsultaExternaDto getConsulExtDto() {
		return consulExtDto;
	}

	/**
	 * @param consulExtDto
	 *            the consulExtDto to set
	 */
	public void setConsulExtDto(ConsultaExternaDto consulExtDto) {
		this.consulExtDto = consulExtDto;
	}

	/**
	 * @return the listadoEspecialidad
	 */
	public List<CatalogoDto> getListadoEspecialidad() {
		return listadoEspecialidad;
	}

	/**
	 * @param listadoEspecialidad
	 *            the listadoEspecialidad to set
	 */
	public void setListadoEspecialidad(List<CatalogoDto> listadoEspecialidad) {
		this.listadoEspecialidad = listadoEspecialidad;
	}

	/**
	 * @return the listadoAgreMed
	 */
	public List<CatalogoDto> getListadoAgreMed() {
		return listadoAgreMed;
	}

	/**
	 * @param listadoAgreMed
	 *            the listadoAgreMed to set
	 */
	public void setListadoAgreMed(List<CatalogoDto> listadoAgreMed) {
		this.listadoAgreMed = listadoAgreMed;
	}

	/**
	 * @return the derecho
	 */
	public String getDerecho() {
		return derecho;
	}

	/**
	 * @param derecho
	 *            the derecho to set
	 */
	public void setDerecho(String derecho) {
		this.derecho = derecho;
	}

	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}

	/**
	 * @param sexo
	 *            the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	/**
	 * @return the anno
	 */
	public String getAnno() {
		return anno;
	}

	/**
	 * @param anno
	 *            the anno to set
	 */
	public void setAnno(String anno) {
		this.anno = anno;
	}

	/**
	 * @return the regimen
	 */
	public String getRegimen() {
		return regimen;
	}

	/**
	 * @param regimen
	 *            the regimen to set
	 */
	public void setRegimen(String regimen) {
		this.regimen = regimen;
	}

	

	
	
	public List<ConsultaExternaDto> getListEditConExt() {
		return listEditConExt;
	}

	public void setListEditConExt(List<ConsultaExternaDto> listEditConExt) {
		this.listEditConExt = listEditConExt;
	}

	public InputMask getFechaPf() {
		return fechaPf;
	}

	public void setFechaPf(InputMask fechaPf) {
		this.fechaPf = fechaPf;
	}

	public InputText getNoSsInPf() {
		return noSsInPf;
	}

	public void setNoSsInPf(InputText noSsInPf) {
		this.noSsInPf = noSsInPf;
	}

	public InputText getMatMedInPf() {
		return matMedInPf;
	}

	public void setMatMedInPf(InputText matMedInPf) {
		this.matMedInPf = matMedInPf;
	}

	public InputText getMatMednommedPf() {
		return matMednommedPf;
	}

	public void setMatMednommedPf(InputText matMednommedPf) {
		this.matMednommedPf = matMednommedPf;
	}

	public InputText getAgMeCalidadPf() {
		return agMeCalidadPf;
	}

	public void setAgMeCalidadPf(InputText agMeCalidadPf) {
		this.agMeCalidadPf = agMeCalidadPf;
	}

	public InputText getAgMeSexoPf() {
		return agMeSexoPf;
	}

	public void setAgMeSexoPf(InputText agMeSexoPf) {
		this.agMeSexoPf = agMeSexoPf;
	}

	public InputText getAgMeAnioPf() {
		return agMeAnioPf;
	}

	public void setAgMeAnioPf(InputText agMeAnioPf) {
		this.agMeAnioPf = agMeAnioPf;
	}



	public SelectOneMenu getEspecialidadPf() {
		return especialidadPf;
	}

	public void setEspecialidadPf(SelectOneMenu especialidadPf) {
		this.especialidadPf = especialidadPf;
	}

	public InputText getCodCie10Pf() {
		return codCie10Pf;
	}

	public void setCodCie10Pf(InputText codCie10Pf) {
		this.codCie10Pf = codCie10Pf;
	}

	public SelectOneMenu getOcasServPf() {
		return ocasServPf;
	}

	public void setOcasServPf(SelectOneMenu ocasServPf) {
		this.ocasServPf = ocasServPf;
	}

	public CommandButton getSalirBtPf() {
		return salirBtPf;
	}

	public void setSalirBtPf(CommandButton salirBtPf) {
		this.salirBtPf = salirBtPf;
	}

	public List<CatalogoDto> getListadoPrimeraVez() {
		return listadoPrimeraVez;
	}

	public void setListadoPrimeraVez(List<CatalogoDto> listadoPrimeraVez) {
		this.listadoPrimeraVez = listadoPrimeraVez;
	}




	public ConsultaExternaDto getSelConsultaExternaDto() {
		return selConsultaExternaDto;
	}

	public void setSelConsultaExternaDto(
			ConsultaExternaDto selConsultaExternaDto) {
	}

	 
	/**
	 * @return the accederService
	 */
	public AccederWsService getAccederService() {
		return accederService;
	}

	/**
	 * @param accederService
	 *            the accederService to set
	 */
	public void setAccederService(AccederWsService accederService) {
		this.accederService = accederService;
	}
	public List<String> getListaEspecialidadAc() {
		return listaEspecialidadAc;
	}

	public void setListaEspecialidadAc(List<String> listaEspecialidadAc) {
		this.listaEspecialidadAc = listaEspecialidadAc;
	}

	public AutoComplete getEspecialidadAcPf() {
		return especialidadAcPf;
	}

	public void setEspecialidadAcPf(AutoComplete especialidadAcPf) {
		this.especialidadAcPf = especialidadAcPf;
	}

	public List<String> getListaRegimenAc() {
		return listaRegimenAc;
	}

	public void setListaRegimenAc(List<String> listaRegimenAc) {
		this.listaRegimenAc = listaRegimenAc;
	}

	public AutoComplete getRegimenAcPf() {
		return regimenAcPf;
	}

	public void setRegimenAcPf(AutoComplete regimenAcPf) {
		this.regimenAcPf = regimenAcPf;
	}

	public boolean isDeshabilitadoMedS() {
		return deshabilitadoMedS;
	}

	public void setDeshabilitadoMedS(boolean deshabilitadoMedS) {
		this.deshabilitadoMedS = deshabilitadoMedS;
	}

	public boolean isDeshabilitadoMedA() {
		return deshabilitadoMedA;
	}

	public void setDeshabilitadoMedA(boolean deshabilitadoMedA) {
		this.deshabilitadoMedA = deshabilitadoMedA;
	}

	public boolean isDeshabilitadoMedR() {
		return deshabilitadoMedR;
	}

	public void setDeshabilitadoMedR(boolean deshabilitadoMedR) {
		this.deshabilitadoMedR = deshabilitadoMedR;
	}
		
	
}
