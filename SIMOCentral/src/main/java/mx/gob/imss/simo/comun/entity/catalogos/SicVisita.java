/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_VISITA")
public class SicVisita {

	@Id
	private ObjectId id;
	private Integer cveVisita;
	private String desVisita;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveVisita
	 */
	public Integer getCveVisita() {
		return cveVisita;
	}

	/**
	 * @param cveVisita
	 *            the cveVisita to set
	 */
	public void setCveVisita(Integer cveVisita) {
		this.cveVisita = cveVisita;
	}

	/**
	 * @return the desVisita
	 */
	public String getDesVisita() {
		return desVisita;
	}

	/**
	 * @param desVisita
	 *            the desVisita to set
	 */
	public void setDesVisita(String desVisita) {
		this.desVisita = desVisita;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
