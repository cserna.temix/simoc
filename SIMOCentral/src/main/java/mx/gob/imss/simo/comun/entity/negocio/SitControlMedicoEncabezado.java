package mx.gob.imss.simo.comun.entity.negocio;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;

@Entity("SIT_CONTROL_MEDICO_ENCABEZADO")

public class SitControlMedicoEncabezado {

	@Id
	private ObjectId id;
	//private SdUnidadMedica sdUnidadMedica;
	private List<SdUnidadMedica> sdUnidadMedica = new ArrayList<>();
	private String cveMatricula;
	private Integer numIntento;
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
/*	public SdUnidadMedica getSdUnidadMedica() {
		return sdUnidadMedica;
	}
	public void setSdUnidadMedica(SdUnidadMedica sdUnidadMedica) {
		this.sdUnidadMedica = sdUnidadMedica;
	}*/
	
	public String getCveMatricula() {
		return cveMatricula;
	}
	public void setCveMatricula(String cveMatricula) {
		this.cveMatricula = cveMatricula;
	}
	public Integer getNumIntento() {
		return numIntento;
	}
	public void setNumIntento(Integer numIntento) {
		this.numIntento = numIntento;
	}
	
	public List<SdUnidadMedica> getSdUnidadMedica() {
		return sdUnidadMedica;
	}

	public void setSdUnidadMedica(List<SdUnidadMedica> sdUnidadMedica) {
		this.sdUnidadMedica = sdUnidadMedica;
	}
}
