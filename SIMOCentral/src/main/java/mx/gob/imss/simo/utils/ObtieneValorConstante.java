package mx.gob.imss.simo.utils;

import org.mongodb.morphia.Datastore;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.entity.catalogos.SicConstantes;

public class ObtieneValorConstante {

//	@Autowired
//	private ConnectionMongo connectionMongo;
//	
	public String obtenerUrlConstante(String value) {
		String constante = "";
		// SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
			constante = ds.createQuery(SicConstantes.class)
					.filter("cveConstante", value).get().getRefValor();
		} catch (Exception e) {
			constante = "No se encontró el valor deseado";
		}
		return constante;
	}
}
