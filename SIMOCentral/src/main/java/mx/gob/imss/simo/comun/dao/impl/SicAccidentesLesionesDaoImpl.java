/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicAccidentesLesionesDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicAccidentesLesiones;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicAccidentesLesionesDao")
public class SicAccidentesLesionesDaoImpl implements SicAccidentesLesionesDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicAccidentesLesiones> burcarTodos() {
//		SimoDatastore simoDatastore = new SimoDatastore();
		List<SicAccidentesLesiones> listAccidentesLesiones = new ArrayList<SicAccidentesLesiones>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listAccidentesLesiones = dsEsp.createQuery(
					SicAccidentesLesiones.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//			connectionMongo.cerrarConexion();
//		}

		return listAccidentesLesiones;
	}

	@Override
	public List<SicAccidentesLesiones> burcarPorId(Integer cveAccidentesLesiones) {
//		SimoDatastore simoDatastore = new SimoDatastore();
		List<SicAccidentesLesiones> listAccidentesLesiones = new ArrayList<SicAccidentesLesiones>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listAccidentesLesiones = dsEsp
					.createQuery(SicAccidentesLesiones.class)
					.field("cveAccidenteLesion").equal(cveAccidentesLesiones)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 

		return listAccidentesLesiones;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
