package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicCie10VigilanciaEpi;

public interface SicCie10VigilanciaEpiDao {
	
	List<String> obtenerCatalogoVigEpi();

}
