package mx.gob.imss.simo.consultaexterna.dto;

public enum Especiales {
	
	VIOLENCIAS("VIOLENCIAS"),
	MATERNO_INFANTIL("ENFERMERA MATERNO INFANTIL"),
	PSICOLOGIA("PSICOLOG\u00CDA"),
	PRENATAL("ENFERMERA PRENATAL DE HOSPITAL"),
	NUTRICION("NUTRICI\u00D3N Y DIET\u00C9TICA"),
	TRABAJO_SOCIAL("TRABAJO SOCIAL"),
	CRONICO_DEGENERATIVO("ENFERMERA CR\u00D3NICO-DEGENERATIVO"),
	OPTOMETRIA("OPTOMETR\u00CDA");
	
	private String clave;
	
	private Especiales(String clave) {
		this.clave = clave;
	}
	
	public String getClave() {
		return clave;
	}

}