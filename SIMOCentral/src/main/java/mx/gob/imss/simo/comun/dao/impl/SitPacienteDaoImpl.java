/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SitPacienteDao;
import mx.gob.imss.simo.comun.entity.negocio.SitPaciente;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sitPacienteDao")
public class SitPacienteDaoImpl implements SitPacienteDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/**
	 * 
	 */
	public SitPacienteDaoImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<SitPaciente> burcarTodos() {
		// SimoDatastore simoDatastore = new SimoDatastore();
		List<SitPaciente> listPaciente = new ArrayList<SitPaciente>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listPaciente = dsEsp.createQuery(SitPaciente.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listPaciente;
	}

	@Override
	public List<SitPaciente> burcarPorId(String nss, String agregadoMedico) {
		// SimoDatastore simoDatastore = new SimoDatastore();
		List<SitPaciente> listPaciente = new ArrayList<SitPaciente>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listPaciente = dsEsp.createQuery(SitPaciente.class).field("numNss")
					.equal(nss).field("refAgregadoMedico")
					.equal(agregadoMedico).asList();

		} catch (Exception e) {
			e.getMessage();
		}
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listPaciente;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
