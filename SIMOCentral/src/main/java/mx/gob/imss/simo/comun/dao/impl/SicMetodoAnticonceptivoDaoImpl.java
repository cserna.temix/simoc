/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicMetodoAnticonceptivoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicMetodoAnticonceptivo;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicMetodoAnticonceptivoDao")
public class SicMetodoAnticonceptivoDaoImpl implements
		SicMetodoAnticonceptivoDao {

	final static Logger logger = Logger
			.getLogger(SicMetodoAnticonceptivoDaoImpl.class.getName());

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicMetodoAnticonceptivo> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicMetodoAnticonceptivo> listMetodoPpf = new ArrayList<SicMetodoAnticonceptivo>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listMetodoPpf = dsEsp.createQuery(SicMetodoAnticonceptivo.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listMetodoPpf;
	}

	@Override
	public List<SicMetodoAnticonceptivo> burcarPorId(Integer cveMetodoPpf) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicMetodoAnticonceptivo> listMetodoPpf = new ArrayList<SicMetodoAnticonceptivo>();
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();

			listMetodoPpf = ds.createQuery(SicMetodoAnticonceptivo.class)
					.field("cveMetodoAnticonceptivo").equal(cveMetodoPpf)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listMetodoPpf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicMetodoAnticonceptivoDao#burcarXSexo(java
	 * .lang.Integer)
	 */
	@Override
	public List<SicMetodoAnticonceptivo> burcarXSexo(Integer sexo) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicMetodoAnticonceptivo> listMetodoPpf = new ArrayList<SicMetodoAnticonceptivo>();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SicMetodoAnticonceptivo> query = ds
					.createQuery(SicMetodoAnticonceptivo.class);
			query.or(query.criteria("cveSexo").equal(sexo),
					query.criteria("cveSexo").equal(0));
			listMetodoPpf = query.asList();

		} catch (Exception e) {
			logger.info("ERROR: " + e);
		}
		return listMetodoPpf;
	}

	@Override
	public List<SicMetodoAnticonceptivo> burcarXSexoXEdad(Integer sexo,
			Integer edad) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicMetodoAnticonceptivo> listMetodoPpf = new ArrayList<SicMetodoAnticonceptivo>();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SicMetodoAnticonceptivo> query = ds
					.createQuery(SicMetodoAnticonceptivo.class);
			query.and(query.or(query.criteria("cveSexo").equal(sexo),query.criteria("cveSexo").equal(0)), 
					query.criteria("numEdadFinal").greaterThanOrEq(edad), query.criteria("numEdadInicial").lessThanOrEq(edad));
			listMetodoPpf = query.asList();

		} catch (Exception e) {
			logger.info("ERROR: " + e);
		}
		return listMetodoPpf;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
