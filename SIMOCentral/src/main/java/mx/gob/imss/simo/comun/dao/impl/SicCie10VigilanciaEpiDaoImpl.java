package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicCie10VigilanciaEpiDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie10VigilanciaEpi;

@Repository("SicCie10VigilanciaEpiDao")
public class SicCie10VigilanciaEpiDaoImpl implements SicCie10VigilanciaEpiDao{
	
	final static Logger logger = Logger.getLogger(SicCie10VigilanciaEpiDaoImpl.class.getName());
	
//	@Autowired
//	private ConnectionMongo connectionMongo;
	
	public List<String> obtenerCatalogoVigEpi(){
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<String> listCatalogoVigEpi = new ArrayList<String>();
		try {
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			DBObject fields = new BasicDBObject("cveCie10VigEpi", 1);
			DBCursor cursor = dsEsp.getCollection(SicCie10VigilanciaEpi.class).find(null, fields);
			for (DBObject dbObject : cursor) {
				listCatalogoVigEpi.add((String) dbObject.get("cveCie10VigEpi"));
				//logger.info((String) dbObject.get("cveCie10VigEpi"));
			}

		} catch (Exception e) {
			e.getMessage();
		} 
		return listCatalogoVigEpi;
	}

}
