package mx.gob.imss.simo.consultaexterna.dto;

public class ControlMedicoEncabezadoDto {
	
	private String cvePresupuestal;
	private String desUnidadMedica;
	private String cveMatricula;
	private Integer numIntento;
	
	public ControlMedicoEncabezadoDto(String cvePresupuestal, String desUnidadMedica, String cveMatricula, Integer numIntento) {
		this.cvePresupuestal=cvePresupuestal;
		this.desUnidadMedica=desUnidadMedica;
		this.cveMatricula=cveMatricula;
		this.numIntento=numIntento;
	}
	
	public ControlMedicoEncabezadoDto() {

	}

	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	public String getDesUnidadMedica() {
		return desUnidadMedica;
	}

	public void setDesUnidadMedica(String desUnidadMedica) {
		this.desUnidadMedica = desUnidadMedica;
	}

	public String getCveMatricula() {
		return cveMatricula;
	}

	public void setCveMatricula(String cveMatricula) {
		this.cveMatricula = cveMatricula;
	}

	public Integer getNumIntento() {
		return numIntento;
	}

	public void setNumIntento(Integer numIntento) {
		this.numIntento = numIntento;
	}
	
}
