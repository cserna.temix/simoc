/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoEgreso;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicTipoEgresoDao {

	/**
	 * @return
	 */
	List<SicTipoEgreso> burcarTodos();

	/**
	 * @param cveTipoEgreso
	 * @return
	 */
	List<SicTipoEgreso> burcarPorId(Integer cveTipoEgreso);
}
