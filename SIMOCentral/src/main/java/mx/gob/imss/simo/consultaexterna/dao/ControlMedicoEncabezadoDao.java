package mx.gob.imss.simo.consultaexterna.dao;


import mx.gob.imss.simo.comun.entity.negocio.SitControlMedicoEncabezado;


public interface ControlMedicoEncabezadoDao {


	void insertar(SitControlMedicoEncabezado controlMedico);

	Boolean actualizar(SitControlMedicoEncabezado controlMedico);

	SitControlMedicoEncabezado buscar(String matriculaMedico);

}
