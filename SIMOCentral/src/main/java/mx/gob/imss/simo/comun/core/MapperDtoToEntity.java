/**
 * 
 */
package mx.gob.imss.simo.comun.core;

import java.util.Date;

import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdFormato;
import mx.gob.imss.simo.model.ConsultaExternaDto;

/**
 * @author francisco.rrios
 * 
 */
public class MapperDtoToEntity {

	public static Object conversion(Object dtoOrigen, Class<?> entidad)
			throws Exception {
		try {
			String dtoNombre = dtoOrigen.getClass().getSimpleName();

			switch (dtoNombre) {

			case "ConsultaExternaDto":
				if (entidad.getSimpleName().equals("SdEncabezado")) {
					ConsultaExternaDto dto = (ConsultaExternaDto) dtoOrigen;
					SdEncabezado ent = new SdEncabezado(dto.getFecha(),
							dto.getHorasTrabajadas(),
							dto.getConsultasNoOtorgadas(),
							dto.getCitasNoCumplidas());
					return ent;					
				} else if(entidad.getSimpleName().equals("SdFormato")){

					ConsultaExternaDto dto = (ConsultaExternaDto) dtoOrigen;
					SdFormato ent = new SdFormato();
						ent.setDesTipoFormato("4306");
						ent.setNumConsecutivo(dto.getNumConsulta());
						ent.setIndCitado(dto.getCitado());
						ent.setInd1EraVez(dto.getPrimeraVez());
						ent.setNumRecetas(dto.getRecetas());
						ent.setNumDiasIncapacidad(dto.getDiasIncapacidad());
						ent.setIndVisita(dto.getVisitas());
						ent.setStpCaptura(dto.getStpCaptura());
						ent.setStpActualiza(dto.getStpActualiza());
					return ent;
				
				}
				break;				
			}
		} catch (Exception e) {
			throw new Exception("Error al transformar Dto en Entidad", e);
		}
		return null;
	}	
	
}
