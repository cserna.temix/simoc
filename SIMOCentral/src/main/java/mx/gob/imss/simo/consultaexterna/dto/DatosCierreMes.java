//package com.jasperreports.ce;
package mx.gob.imss.simo.consultaexterna.dto;

public class DatosCierreMes {
	
	private String matricula;

	private String texto;
	private String numMed;
	private String horasTrab;
	private String diasTrab;
	private String cons;
	private String cons1V;
	private String consNO;
	private String promHoras;
	private String visitas;
	private String citas;
	private String citasCump;
	private String pasesEsp;
	private String pasesEspOtra;
	private String altasEsp;
	private String incapExp;
	private String incapDias;
	private String incapProm;
	private String recetasExp;
	private String recetasPorc;
	
	public DatosCierreMes() {
		this.texto = "";
		this.numMed = "";
		this.horasTrab = "";
		this.diasTrab = "";
		this.cons = "";
		this.cons1V = "";
		this.consNO = "";
		this.promHoras = "";
		this.visitas = "";
		this.citas = "";
		this.citasCump = "";
		this.pasesEsp = "";
		this.pasesEspOtra = "";
		this.altasEsp = "";
		this.incapExp = "";
		this.incapDias = "";
		this.incapProm = "";
		this.recetasExp = "";
		this.recetasPorc = "";
	}
	
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getNumMed() {
		return numMed;
	}

	public void setNumMed(String numMed) {
		this.numMed = numMed;
	}

	public String getHorasTrab() {
		return horasTrab;
	}

	public void setHorasTrab(String horasTrab) {
		this.horasTrab = horasTrab;
	}

	public String getDiasTrab() {
		return diasTrab;
	}

	public void setDiasTrab(String diasTrab) {
		this.diasTrab = diasTrab;
	}

	public String getCons() {
		return cons;
	}

	public void setCons(String cons) {
		this.cons = cons;
	}

	public String getCons1V() {
		return cons1V;
	}

	public void setCons1V(String cons1v) {
		cons1V = cons1v;
	}

	public String getConsNO() {
		return consNO;
	}

	public void setConsNO(String consNO) {
		this.consNO = consNO;
	}

	public String getPromHoras() {
		return promHoras;
	}

	public void setPromHoras(String promHoras) {
		this.promHoras = promHoras;
	}

	public String getVisitas() {
		return visitas;
	}

	public void setVisitas(String visitas) {
		this.visitas = visitas;
	}

	public String getCitas() {
		return citas;
	}

	public void setCitas(String citas) {
		this.citas = citas;
	}

	public String getCitasCump() {
		return citasCump;
	}

	public void setCitasCump(String citasCump) {
		this.citasCump = citasCump;
	}

	public String getPasesEsp() {
		return pasesEsp;
	}

	public void setPasesEsp(String pasesEsp) {
		this.pasesEsp = pasesEsp;
	}

	public String getPasesEspOtra() {
		return pasesEspOtra;
	}

	public void setPasesEspOtra(String pasesEspOtra) {
		this.pasesEspOtra = pasesEspOtra;
	}

	public String getAltasEsp() {
		return altasEsp;
	}

	public void setAltasEsp(String altasEsp) {
		this.altasEsp = altasEsp;
	}

	public String getIncapExp() {
		return incapExp;
	}

	public void setIncapExp(String incapExp) {
		this.incapExp = incapExp;
	}

	public String getIncapDias() {
		return incapDias;
	}

	public void setIncapDias(String incapDias) {
		this.incapDias = incapDias;
	}

	public String getIncapProm() {
		return incapProm;
	}

	public void setIncapProm(String incapProm) {
		this.incapProm = incapProm;
	}

	public String getRecetasExp() {
		return recetasExp;
	}

	public void setRecetasExp(String recetasExp) {
		this.recetasExp = recetasExp;
	}

	public String getRecetasPorc() {
		return recetasPorc;
	}

	public void setRecetasPorc(String recetasPorc) {
		this.recetasPorc = recetasPorc;
	}

}