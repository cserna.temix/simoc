/**
 * 
 */
package mx.gob.imss.simo.comun.entity.negocio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.consultaexterna.pojo.SdCapturista;
import mx.gob.imss.simo.consultaexterna.pojo.SdDelegacion;
import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdPerfil;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIT_CONSULTA_EXTERNA")
public class SitConsultaExterna {

	@Id
	private ObjectId id;
	private List<SdCapturista> sdCapturista = new ArrayList<>();
	private List<SdPerfil> sdPerfil = new ArrayList<>();
	private List<SdDelegacion> sdDelegacion = new ArrayList<>();
	private List<SdUnidadMedica> sdUnidadMedica = new ArrayList<>();
	private List<SdEncabezado> sdEncabezado = new ArrayList<>();
	private Date fecCaptura;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the sdCapturista
	 */
	public List<SdCapturista> getSdCapturista() {
		return sdCapturista;
	}

	/**
	 * @param sdCapturista
	 *            the sdCapturista to set
	 */
	public void setSdCapturista(List<SdCapturista> sdCapturista) {
		this.sdCapturista = sdCapturista;
	}

	/**
	 * @return the sdPerfil
	 */
	public List<SdPerfil> getSdPerfil() {
		return sdPerfil;
	}

	/**
	 * @param sdPerfil
	 *            the sdPerfil to set
	 */
	public void setSdPerfil(List<SdPerfil> sdPerfil) {
		this.sdPerfil = sdPerfil;
	}

	/**
	 * @return the sdDelegacion
	 */
	public List<SdDelegacion> getSdDelegacion() {
		return sdDelegacion;
	}

	/**
	 * @param sdDelegacion
	 *            the sdDelegacion to set
	 */
	public void setSdDelegacion(List<SdDelegacion> sdDelegacion) {
		this.sdDelegacion = sdDelegacion;
	}

	/**
	 * @return the sdUnidadMedica
	 */
	public List<SdUnidadMedica> getSdUnidadMedica() {
		return sdUnidadMedica;
	}

	/**
	 * @param sdUnidadMedica
	 *            the sdUnidadMedica to set
	 */
	public void setSdUnidadMedica(List<SdUnidadMedica> sdUnidadMedica) {
		this.sdUnidadMedica = sdUnidadMedica;
	}

	/**
	 * @return the sdEncabezado
	 */
	public List<SdEncabezado> getSdEncabezado() {
		return sdEncabezado;
	}

	/**
	 * @param sdEncabezado
	 *            the sdEncabezado to set
	 */
	public void setSdEncabezado(List<SdEncabezado> sdEncabezado) {
		this.sdEncabezado = sdEncabezado;
	}

	/**
	 * @return the fecCaptura
	 */
	public Date getFecCaptura() {
		return fecCaptura;
	}

	/**
	 * @param fecCaptura
	 *            the fecCaptura to set
	 */
	public void setFecCaptura(Date fecCaptura) {
		this.fecCaptura = fecCaptura;
	}

}
