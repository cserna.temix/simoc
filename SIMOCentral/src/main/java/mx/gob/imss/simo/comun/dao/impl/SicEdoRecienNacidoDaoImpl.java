/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicEdoRecienNacidoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicEdoRecienNacido;

/**
 * @author francisco.rodriguez
 *
 */
public class SicEdoRecienNacidoDaoImpl implements SicEdoRecienNacidoDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicEdoRecienNacidoDao#burcarTodos()
	 */
	@Override
	public List<SicEdoRecienNacido> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicEdoRecienNacido> listEdoRecienNacidoo = new ArrayList<SicEdoRecienNacido>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listEdoRecienNacidoo = dsEsp.createQuery(SicEdoRecienNacido.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listEdoRecienNacidoo;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicEdoRecienNacidoDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicEdoRecienNacido> burcarPorId(Integer cveEdoRecienNacido) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicEdoRecienNacido> listEdoRecienNacidoo = new ArrayList<SicEdoRecienNacido>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listEdoRecienNacidoo = dsEsp.createQuery(SicEdoRecienNacido.class)
					.field("cveEdoRecienNacido").equal(cveEdoRecienNacido).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listEdoRecienNacidoo;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
