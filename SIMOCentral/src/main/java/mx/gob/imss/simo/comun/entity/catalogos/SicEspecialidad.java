/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Frank
 * 
 */
@Entity("SIC_ESPECIALIDAD")
public class SicEspecialidad {

	@Id
	private ObjectId id;
	private Integer numTipoServicio;
	private String cveEspecialidad;
	private String desEspecialidad;
	private Date fecBaja;
	private String cvePresupuestal;
	
  	private Integer numDivision;
  	private Integer numUbicacion;
  	private Integer indQx;
  	private Integer indSexo;
  	private Integer numEdadMinima;
  	private Integer numEdadMinSemana;
  	private Integer numEdadMaxima;
  	private Integer numEdadMaximaSemanas;
  	private Integer indPediatria;
  	private String refAreaResponsable;
  	private String refSimoSias;
  	private Integer numSimoCentral;
  	private Integer numCategoria;
  	private Integer indMoviIntrahospitalario;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	
	/**
	 * @return the numTipoServicio
	 */
	public Integer getNumTipoServicio() {
		return numTipoServicio;
	}

	/**
	 * @param numTipoServicio the numTipoServicio to set
	 */
	public void setNumTipoServicio(Integer numTipoServicio) {
		this.numTipoServicio = numTipoServicio;
	}

	/**
	 * @return the cveEspecialidad
	 */
	public String getCveEspecialidad() {
		return cveEspecialidad;
	}

	/**
	 * @param cveEspecialidad
	 *            the cveEspecialidad to set
	 */
	public void setCveEspecialidad(String cveEspecialidad) {
		this.cveEspecialidad = cveEspecialidad;
	}

	/**
	 * @return the desEspecialidad
	 */
	public String getDesEspecialidad() {
		return desEspecialidad;
	}

	/**
	 * @param desEspecialidad
	 *            the desEspecialidad to set
	 */
	public void setDesEspecialidad(String desEspecialidad) {
		this.desEspecialidad = desEspecialidad;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the cvePresupuestal
	 */
	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	/**
	 * @param cvePresupuestal the cvePresupuestal to set
	 */
	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	public Integer getNumDivision() {
		return numDivision;
	}

	public void setNumDivision(Integer numDivision) {
		this.numDivision = numDivision;
	}

	public Integer getNumUbicacion() {
		return numUbicacion;
	}

	public void setNumUbicacion(Integer numUbicacion) {
		this.numUbicacion = numUbicacion;
	}

	public Integer getIndQx() {
		return indQx;
	}

	public void setIndQx(Integer indQx) {
		this.indQx = indQx;
	}

	public Integer getIndSexo() {
		return indSexo;
	}

	public void setIndSexo(Integer indSexo) {
		this.indSexo = indSexo;
	}

	public Integer getNumEdadMinima() {
		return numEdadMinima;
	}

	public void setNumEdadMinima(Integer numEdadMinima) {
		this.numEdadMinima = numEdadMinima;
	}

	public Integer getNumEdadMinSemana() {
		return numEdadMinSemana;
	}

	public void setNumEdadMinSemana(Integer numEdadMinSemana) {
		this.numEdadMinSemana = numEdadMinSemana;
	}

	public Integer getNumEdadMaxima() {
		return numEdadMaxima;
	}

	public void setNumEdadMaxima(Integer numEdadMaxima) {
		this.numEdadMaxima = numEdadMaxima;
	}

	public Integer getNumEdadMaximaSemanas() {
		return numEdadMaximaSemanas;
	}

	public void setNumEdadMaximaSemanas(Integer numEdadMaximaSemanas) {
		this.numEdadMaximaSemanas = numEdadMaximaSemanas;
	}

	public Integer getIndPediatria() {
		return indPediatria;
	}

	public void setIndPediatria(Integer indPediatria) {
		this.indPediatria = indPediatria;
	}

	public String getRefAreaResponsable() {
		return refAreaResponsable;
	}

	public void setRefAreaResponsable(String refAreaResponsable) {
		this.refAreaResponsable = refAreaResponsable;
	}

	public String getRefSimoSias() {
		return refSimoSias;
	}

	public void setRefSimoSias(String refSimoSias) {
		this.refSimoSias = refSimoSias;
	}

	public Integer getNumSimoCentral() {
		return numSimoCentral;
	}

	public void setNumSimoCentral(Integer numSimoCentral) {
		this.numSimoCentral = numSimoCentral;
	}

	public Integer getNumCategoria() {
		return numCategoria;
	}

	public void setNumCategoria(Integer numCategoria) {
		this.numCategoria = numCategoria;
	}

	public Integer getIndMoviIntrahospitalario() {
		return indMoviIntrahospitalario;
	}

	public void setIndMoviIntrahospitalario(Integer indMoviIntrahospitalario) {
		this.indMoviIntrahospitalario = indMoviIntrahospitalario;
	}

}
