package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("SIC_TIPO_CAPTURA")
public class SicTipoCaptura {
	
	@Id
	private ObjectId id;
	private Integer cveTipoCaptura;
	private String desTipoCaptura;
	private Date fecBaja;
	
	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTipoCaptura
	 */
	public Integer getCveTipoCaptura() {
		return cveTipoCaptura;
	}

	/**
	 * @param cveTipoCaptura the cveTipoCaptura to set
	 */
	public void setCveTipoCaptura(Integer cveTipoCaptura) {
		this.cveTipoCaptura = cveTipoCaptura;
	}

	/**
	 * @return the desTipoCaptura
	 */
	public String getDesTipoCaptura() {
		return desTipoCaptura;
	}

	/**
	 * @param desTipoCaptura the desTipoCaptura to set
	 */
	public void setDesTipoCaptura(String desTipoCaptura) {
		this.desTipoCaptura = desTipoCaptura;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}
	

}
