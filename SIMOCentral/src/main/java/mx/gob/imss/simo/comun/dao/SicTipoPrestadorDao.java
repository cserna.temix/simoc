/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoPrestador;

/**
 * @author francisco.rrios
 * 
 */
public interface SicTipoPrestadorDao {

	/**
	 * @return
	 */
	List<SicTipoPrestador> burcarTodos();

	/**
	 * @param cveTipoPrestador
	 * @return
	 */
	List<SicTipoPrestador> burcarPorId(Integer cveTipoPrestador);
}
