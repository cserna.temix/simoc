/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.comun.entity.negocio.SitPeriodoOperacion;

/**
 * @author francisco.rodriguez
 * 
 */
public interface SicPeriodosImssDao {

	SicPeriodosImss burcarPorPeriodo(Integer numAnioPeriodo,
			String numMesPeriodo);
	
	SitPeriodoOperacion burcarPorPeriodoOperacion(Integer periodoOperacion,
			String cvePresupuestal);
	
	List<SicPeriodosImss> buscarCataoloPeriodoImss();
	
	void actualizarPeriodoActualIMSS(SicPeriodosImss periodoIMSS, String cvePresupuestal, String unidadMedica, String modulo);
}
