/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.mapping.Mapper;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicAgregadoMedicoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicAgregadoMedico;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicAgregadoMedicoDao")
public class SicAgregadoMedicoDaoImpl implements SicAgregadoMedicoDao {
	
//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicAgregadoMedico> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicAgregadoMedico> listAgregadoMedico = new ArrayList<SicAgregadoMedico>();
		try {
		
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listAgregadoMedico = dsEsp.createQuery(SicAgregadoMedico.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 
		return listAgregadoMedico;
	}

	@Override
	public List<SicAgregadoMedico> burcarPorId(String idAgregadoMedico) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicAgregadoMedico> listAgregadoMedico = new ArrayList<SicAgregadoMedico>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listAgregadoMedico = dsEsp.createQuery(SicAgregadoMedico.class)
					.field(Mapper.ID_KEY).equal(new ObjectId(idAgregadoMedico))
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//			connectionMongo.cerrarConexion();
//		}

		return listAgregadoMedico;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
