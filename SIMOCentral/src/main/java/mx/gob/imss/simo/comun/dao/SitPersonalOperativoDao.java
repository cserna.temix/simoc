/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import mx.gob.imss.simo.comun.entity.negocio.SitPersonalOperativo;

public interface SitPersonalOperativoDao {

	SitPersonalOperativo loadByUsername(String username);

	void updateIntento(String activeDirectoryAccount, int intento);

	int getIntento(String activeDirectoryAccount);
	
	void updateBloqueo(String activeDirectoryAccount, int bloqueo);
	
	int getBloqueo(String activeDirectoryAccount);
	
	void updateDesbloqueo(int indDesbloqueo);
}
