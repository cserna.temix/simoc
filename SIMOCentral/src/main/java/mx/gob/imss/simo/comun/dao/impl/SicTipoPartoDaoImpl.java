/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoPartoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoParto;

/**
 * @author francisco.rodriguez
 *
 */
public class SicTipoPartoDaoImpl implements SicTipoPartoDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoPartoDao#burcarTodos()
	 */
	@Override
	public List<SicTipoParto> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoParto> listTipoParto = new ArrayList<SicTipoParto>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoParto = dsEsp.createQuery(SicTipoParto.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoParto;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoPartoDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicTipoParto> burcarPorId(Integer cveTipoParto) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoParto> listTipoParto = new ArrayList<SicTipoParto>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoParto = dsEsp.createQuery(SicTipoParto.class)
					.field("cveTipoParto").equal(cveTipoParto).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoParto;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
