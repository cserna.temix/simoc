/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoAtencionDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoAtencion;

/**
 * @author francisco.rodriguez
 *
 */
@Repository("sicTipoAtencionDao")
public class SicTipoAtencionDaoImpl implements SicTipoAtencionDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoAtencionDao#burcarTodos()
	 */
	@Override
	public List<SicTipoAtencion> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoAtencion> listTipoAtencion = new ArrayList<SicTipoAtencion>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoAtencion = dsEsp.createQuery(SicTipoAtencion.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoAtencion;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoAtencionDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicTipoAtencion> burcarPorId(Integer cveTipoAtencion) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoAtencion> listTipoAtencion = new ArrayList<SicTipoAtencion>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoAtencion = dsEsp.createQuery(SicTipoAtencion.class)
					.field("cveTipoAtencion").equal(cveTipoAtencion).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoAtencion;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
