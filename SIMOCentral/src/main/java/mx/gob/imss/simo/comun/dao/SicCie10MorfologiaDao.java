/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicCie10Morfologia;

/**
 * @author francisco.rrios
 *
 */
public interface SicCie10MorfologiaDao {

	/**
	 * @return
	 */
	List<SicCie10Morfologia> burcarTodos();

	/**
	 * @param cveCie10Morfologia
	 * @return
	 */
	List<SicCie10Morfologia> burcarPorId(String cveCie10Morfologia);

	SicCie10Morfologia buscarPorId(String cveCie10Morfologia);

	/**
	 * @param cveCie10Morfologia
	 * @return
	 */
	List<SicCie10Morfologia> burcarPorVigentes(String cveCie10Morfologia);
}
