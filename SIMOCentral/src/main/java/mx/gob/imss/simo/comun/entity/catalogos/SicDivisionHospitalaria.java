/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_DIVISION_HOSPITALARIA")
public class SicDivisionHospitalaria {

	@Id
	private ObjectId id;
	private Integer cveDivisionHospitalaria;
	private String desDivisionHospitalaria;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveDivisionHospitalaria
	 */
	public Integer getCveDivisionHospitalaria() {
		return cveDivisionHospitalaria;
	}

	/**
	 * @param cveDivisionHospitalaria
	 *            the cveDivisionHospitalaria to set
	 */
	public void setCveDivisionHospitalaria(Integer cveDivisionHospitalaria) {
		this.cveDivisionHospitalaria = cveDivisionHospitalaria;
	}

	/**
	 * @return the desDivisionHospitalaria
	 */
	public String getDesDivisionHospitalaria() {
		return desDivisionHospitalaria;
	}

	/**
	 * @param desDivisionHospitalaria
	 *            the desDivisionHospitalaria to set
	 */
	public void setDesDivisionHospitalaria(String desDivisionHospitalaria) {
		this.desDivisionHospitalaria = desDivisionHospitalaria;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
