//package mx.gob.imss.simo.comun.core;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Locale;
//import java.util.ResourceBundle;
//import java.util.logging.Logger;
//
//import javax.faces.bean.SessionScoped;
//
//import org.springframework.stereotype.Service;
//
//import com.mongodb.MongoClient;
//import com.mongodb.MongoCredential;
//import com.mongodb.ServerAddress;
//import com.mongodb.client.MongoDatabase;
//
//@Service("connectionMongo")
//@SessionScoped
//public class ConnectionMongo {
//
//	final static Logger logger = Logger.getLogger(ConnectionMongo.class.getName());
//	
//	private MongoDatabase conexion;
//	private MongoClient mongoClient;
//
//	private static final String IDIOMA = "idioma_locale";
//	private static final String PAIS = "pais_locale";
//	private static final String BUNDLE_LOCALE = "bundle.index.locale";
//	protected static final Locale LOCALE = new Locale(ResourceBundle.getBundle(
//			BUNDLE_LOCALE).getString(IDIOMA), ResourceBundle.getBundle(
//			BUNDLE_LOCALE).getString(PAIS));
//	
//	public ConnectionMongo() {
//		final String SERVER = obtenerMensajes().getString("database.host");
//		final String SERVER2 = obtenerMensajes().getString("database.host2");
//		final String SERVER3 = obtenerMensajes().getString("database.host3");
//		final int PORT = Integer.parseInt(obtenerMensajes().getString("database.port"));
//		final int PORT2 = Integer.parseInt(obtenerMensajes().getString("database.port2"));
//		final int PORT3 = Integer.parseInt(obtenerMensajes().getString("database.port3"));
//		final String USERNAME = obtenerMensajes().getString("database.user");
//		final String PASSWORD = obtenerMensajes().getString("database.password");
//		final String DATABASE = obtenerMensajes().getString("database.base");
//		try {
//			MongoCredential credential = MongoCredential.createCredential(
//					USERNAME, DATABASE, PASSWORD.toCharArray());
//			List<MongoCredential> credentials = new ArrayList<MongoCredential>();
//			credentials.add(credential);
//
//			mongoClient = new MongoClient(Arrays.asList(new ServerAddress(SERVER, PORT),
//						new ServerAddress(SERVER2, PORT2), new ServerAddress(SERVER3, PORT3)), credentials);
//
//			conexion = mongoClient.getDatabase(DATABASE);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//
//	
//	public void cerrarConexion() {
//		try {
//			mongoClient.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * Retorna un recurso referenciado a los mensajes del modulo de PostMortem
//	 * */
//	public static ResourceBundle obtenerMensajes() {
//		return ResourceBundle.getBundle("META-INF.database", LOCALE);
//	}
//
//	public MongoDatabase getConexion() {
//		return conexion;
//	}
//
//	public void setConexion(MongoDatabase conexion) {
//		this.conexion = conexion;
//	}
//
//	/**
//	 * @return the mongoClient
//	 */
//	public MongoClient getMongoClient() {
//		return mongoClient;
//	}
//
//	/**
//	 * @param mongoClient
//	 *            the mongoClient to set
//	 */
//	public void setMongoClient(MongoClient mongoClient) {
//		this.mongoClient = mongoClient;
//	}
//
//	@SuppressWarnings({ "resource", "unused" })
//	public static void main(String... args) {
//		final String SERVER = "11.254.7.152";
//		final int PORT = 27017;
//		final String USERNAME = "SIMO";
//		final String PASSWORD = "SIM2013";
//		final String DATABASE = "test";
//		try {
//			ServerAddress seeds = new ServerAddress(SERVER, PORT);
//			MongoCredential credential = MongoCredential.createCredential(
//					USERNAME, DATABASE, PASSWORD.toCharArray());
//			List<MongoCredential> credentials = new ArrayList<MongoCredential>();
//			credentials.add(credential);
//
//			MongoClient mongoClient = new MongoClient(seeds, credentials);
//			MongoDatabase conexionMongo = mongoClient.getDatabase(DATABASE);
//			logger.info("exito");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//}
