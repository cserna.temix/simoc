/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_PERFIL")
public class SicPerfil {

	@Id
	private ObjectId id;
	private Integer cvePerfil;
	private String desPerfil;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cvePerfil
	 */
	public Integer getCvePerfil() {
		return cvePerfil;
	}

	/**
	 * @param cvePerfil
	 *            the cvePerfil to set
	 */
	public void setCvePerfil(Integer cvePerfil) {
		this.cvePerfil = cvePerfil;
	}

	/**
	 * @return the desPerfil
	 */
	public String getDesPerfil() {
		return desPerfil;
	}

	/**
	 * @param desPerfil
	 *            the desPerfil to set
	 */
	public void setDesPerfil(String desPerfil) {
		this.desPerfil = desPerfil;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
