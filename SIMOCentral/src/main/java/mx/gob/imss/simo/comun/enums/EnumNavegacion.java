/**
 * 
 */
package mx.gob.imss.simo.comun.enums;

/**
 * @author francisco.rrios
 *
 */
public enum EnumNavegacion {

	
	INDEX("/index"),
	PORTADA("/pages/inicio"),
	//Consulta externa
	CONSULTA_EXTERNA("consultaExterna"),
	CONSULTA_EXTERNA_BUSQ_ED("consultaExternaEdicion"),
	//salir
	LOGOUT_SIMO("/logout.jsf"),
	CIERRE_MES("cierreMes");
	
	private String valor;
	
	/**
	 * 
	 * Constructor para la clase EnumNombreBeans.
	 * 
	 * @param clave
	 */
	EnumNavegacion(String valor) {
		this.valor = valor;
	}

	/**
	 * Retorna el valor del Enumerado.
	 * 
	 * @return
	 */
	public String getValor() {
		return valor;
	}
}
