package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.dto.MenuDto;

public interface MenuDao {
	
	List<MenuDto> getMenu(String rol);

}
