/**
 * 
 */
package mx.gob.imss.simo.comun.enums;

/**
 * @author francisco.rrios
 * 
 */
public enum EnumEspecialidades {

	GINECO_OBSTETRICIA(1, "Gineco Obstetricia"),
	// Se evaluan por separado 
	GINECOLOGIA(2100, "Ginecologia"),
	OBSTETRICIA(2400, "Obstetricia"),
	URGENCIA(2, "Urgencia"),
	URGENCIA_CIRUGIA(3,"Urgencias Toco - cirugía"),
	PARAMEDICO(4,"Paramedico"),
	OTROS(5,"Otros"),
	
	
	;

	private int clave;
	private String valor;

	EnumEspecialidades(int clave, String valor) {
		this.clave = clave;
		this.valor = valor;
	}

	/**
	 * @return the clave
	 */
	public int getClave() {
		return clave;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

}
