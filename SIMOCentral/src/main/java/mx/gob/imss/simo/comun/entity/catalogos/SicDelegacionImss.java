package mx.gob.imss.simo.comun.entity.catalogos;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Janiel
 * 
 */
@Entity("SIC_DELEGACION_IMSS")
public class SicDelegacionImss {

	@Id
	private ObjectId id;
	private String cveDelegacion;
	private String desDelegacion;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveDelegacion
	 */
	public String getCveDelegacion() {
		return cveDelegacion;
	}

	/**
	 * @param cveDelegacion
	 *            the cveDelegacion to set
	 */
	public void setCveDelegacion(String cveDelegacion) {
		this.cveDelegacion = cveDelegacion;
	}

	/**
	 * @return the desDelegacion
	 */
	public String getDesDelegacion() {
		return desDelegacion;
	}

	/**
	 * @param desDelegacion
	 *            the desDelegacion to set
	 */
	public void setDesDelegacion(String desDelegacion) {
		this.desDelegacion = desDelegacion;
	}

}
