/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_MEDICO")
public class SicMedico {

	@Id
	private ObjectId id;
	private String cveMatricula;
	private String refApellidoPaterno;
	private String refApellidoMaterno;
	private String refNombre;

	

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * 
	 */
	public SicMedico() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the cveMatricula
	 */
	public String getCveMatricula() {
		return cveMatricula;
	}

	/**
	 * @param cveMatricula
	 *            the cveMatricula to set
	 */
	public void setCveMatricula(String cveMatricula) {
		this.cveMatricula = cveMatricula;
	}

	/**
	 * @return the refApellidoPaterno
	 */
	public String getRefApellidoPaterno() {
		return refApellidoPaterno;
	}

	/**
	 * @param refApellidoPaterno
	 *            the refApellidoPaterno to set
	 */
	public void setRefApellidoPaterno(String refApellidoPaterno) {
		this.refApellidoPaterno = refApellidoPaterno;
	}

	/**
	 * @return the refApellidoMaterno
	 */
	public String getRefApellidoMaterno() {
		return refApellidoMaterno;
	}

	/**
	 * @param refApellidoMaterno
	 *            the refApellidoMaterno to set
	 */
	public void setRefApellidoMaterno(String refApellidoMaterno) {
		this.refApellidoMaterno = refApellidoMaterno;
	}

	/**
	 * @return the refNombre
	 */
	public String getRefNombre() {
		return refNombre;
	}

	/**
	 * @param refNombre
	 *            the refNombre to set
	 */
	public void setRefNombre(String refNombre) {
		this.refNombre = refNombre;
	}

}
