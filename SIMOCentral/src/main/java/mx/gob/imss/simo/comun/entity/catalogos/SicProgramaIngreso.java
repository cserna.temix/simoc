/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_PROGRAMA_INGRESO")
public class SicProgramaIngreso {

	@Id
	private ObjectId id;
	private Integer cveProgramaIngreso;
	private String desProgramaIngreso;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveProgramaIngreso
	 */
	public Integer getCveProgramaIngreso() {
		return cveProgramaIngreso;
	}

	/**
	 * @param cveProgramaIngreso
	 *            the cveProgramaIngreso to set
	 */
	public void setCveProgramaIngreso(Integer cveProgramaIngreso) {
		this.cveProgramaIngreso = cveProgramaIngreso;
	}

	/**
	 * @return the desProgramaIngreso
	 */
	public String getDesProgramaIngreso() {
		return desProgramaIngreso;
	}

	/**
	 * @param desProgramaIngreso
	 *            the desProgramaIngreso to set
	 */
	public void setDesProgramaIngreso(String desProgramaIngreso) {
		this.desProgramaIngreso = desProgramaIngreso;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
