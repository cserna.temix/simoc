package mx.gob.imss.simo.comun.service;

public interface SitPersonalOperativoService {

	void updateIntento(String activeDirectoryAccount, int intento);

	int getIntento(String activeDirectoryAccount);
	
	void updateBloqueo(String activeDirectoryAccount, int bloqueo);
	
	int getBloqueo(String activeDirectoryAccount);
	
	void updateDesbloqueo(int indDesbloqueo);
}
