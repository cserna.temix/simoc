package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("SIC_SEMANA_VIGILANCIA_EPI")
public class SicSemanaVigilaciaEpi {
	
	@Id
	private ObjectId id;
	private Integer numSemanaEPI;
	private Integer numAnioPeriodo;
	private String numMesPeriodo;
	private Date fecInicial;
	private Date fecFinal;
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public Integer getNumSemanaEPI() {
		return numSemanaEPI;
	}
	public void setNumSemanaEPI(Integer numSemanaEPI) {
		this.numSemanaEPI = numSemanaEPI;
	}
	public Integer getNumAnioPeriodo() {
		return numAnioPeriodo;
	}
	public void setNumAnioPeriodo(Integer numAnioPeriodo) {
		this.numAnioPeriodo = numAnioPeriodo;
	}
	public String getNumMesPeriodo() {
		return numMesPeriodo;
	}
	public void setNumMesPeriodo(String numMesPeriodo) {
		this.numMesPeriodo = numMesPeriodo;
	}
	public Date getFecInicial() {
		return fecInicial;
	}
	public void setFecInicial(Date fecInicial) {
		this.fecInicial = fecInicial;
	}
	public Date getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(Date fecFinal) {
		this.fecFinal = fecFinal;
	}
}