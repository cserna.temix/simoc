/**
 * 
 */
package mx.gob.imss.simo.comun.service;

import java.util.List;

import mx.gob.imss.simo.comun.dto.CatalogoDto;
import mx.gob.imss.simo.comun.dto.DelegacionesUmaesDto;
import mx.gob.imss.simo.comun.dto.UnidadMedicaDto;
import mx.gob.imss.simo.consultaexterna.pojo.SdInformacionAdicional;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoDiarrea;

/**
 * @author francisco.rrios
 * 
 */
public interface CatalogosService {

	/**
	 * llena el combo de especialidades
	 */
	List<CatalogoDto> llenaComboEspecialidades();
	
	/**
	 * llena el combo de especialidades por unidad medica
	 */
	List<CatalogoDto> llenaComboEspeUnidad(String cvePresupuestal, int claveModuloAtencion);

	/**
	 * llena el combo de Agregado Medico
	 */
	List<CatalogoDto> llenaComboAgreMed();

	/**
	 * llena el combo de Pases
	 */
	List<CatalogoDto> llenaComboPases();

	/**
	 * llena el combo de Metodo PPF
	 */
	List<CatalogoDto> llenaComboMetPPF();

	/**
	 * llena el combo de Accidentes y Lesiones
	 */
	List<CatalogoDto> llenaComboAccLesiones();

	/**
	 * llena el combo de Riesgos
	 */
	List<CatalogoDto> llenaComboRiesgos();

	/**
	 * llena el combo de Turnos
	 */
	List<CatalogoDto> llenaComboTurnos();

	/**
	 * llena el combo de Tipo de Ingreso
	 */
	List<CatalogoDto> llenaComboTipoIng();

	/**
	 * llena el combo de Camas
	 */
	List<CatalogoDto> llenaComboCamas();

	/**
	 * llena el combo de Procedencia
	 */
	List<CatalogoDto> llenaComboProcedencia();

	/**
	 * llena el combo de Procedencia
	 */
	List<CatalogoDto> llenaComboTipoPrestador();

	/**
	 * llena el combo de Delegacion
	 */
	List<DelegacionesUmaesDto> llenaComboDelegacion();

	/**
	 * llena el combo de Unidad Medica
	 */
	List<UnidadMedicaDto> llenaComboUnidadMedica(String cveDelegacion);

	/**
	 * llena el combo de Tipo Urgencia
	 */
	List<CatalogoDto> llenaComboTipoUrgencia();

	/**
	 * llena el combo de primera vez
	 */
	List<CatalogoDto> llenaCombo1Vez();

	/**
	 * llena el combo de Informacion Adicional
	 */
	List<SdInformacionAdicional> llenaComboInfoAdicional(Integer tipo);

	/**
	 * llena el combo de Tipo diarrea
	 */
	List<SdTipoDiarrea> llenaComboTipoDiarrea();
	
	/**
	 * llena el combo de Metodo PPF depende del Sexo
	 */
	List<CatalogoDto> llenaComboMetPPFXSexo(Integer sexo);
	
	/**
	 * llena el combo de Metodo PPF depende del Sexo
	 */
	List<CatalogoDto> llenaComboMetPPFXSexoXEdad(Integer sexo, Integer edad);
	
	/**
	 * llena el combo de Consultorios
	 */
	List<CatalogoDto> llenaComboConsultorios(String cvePresupuestal, Integer tipoServicio);
}
