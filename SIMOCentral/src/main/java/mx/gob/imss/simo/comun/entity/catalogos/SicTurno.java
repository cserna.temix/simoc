/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Frank
 * 
 */
@Entity("SIC_TURNO")
public class SicTurno {

	@Id
	private ObjectId id;
	private Integer cveTurno;
	private String desTurno;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTurno
	 */
	public Integer getCveTurno() {
		return cveTurno;
	}

	/**
	 * @param cveTurno
	 *            the cveTurno to set
	 */
	public void setCveTurno(Integer cveTurno) {
		this.cveTurno = cveTurno;
	}

	/**
	 * @return the desTurno
	 */
	public String getDesTurno() {
		return desTurno;
	}

	/**
	 * @param desTurno
	 *            the desTurno to set
	 */
	public void setDesTurno(String desTurno) {
		this.desTurno = desTurno;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
