/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoEgresoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoEgreso;

/**
 * @author francisco.rodriguez
 *
 */
@Repository("sicTipoEgresoDao")
public class SicTipoEgresoDaoImpl implements SicTipoEgresoDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoEgresoDao#burcarTodos()
	 */
	@Override
	public List<SicTipoEgreso> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoEgreso> listTipoEgreso = new ArrayList<SicTipoEgreso>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoEgreso = dsEsp.createQuery(SicTipoEgreso.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoEgreso;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoEgresoDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicTipoEgreso> burcarPorId(Integer cveTipoEgreso) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoEgreso> listTipoEgreso = new ArrayList<SicTipoEgreso>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoEgreso = dsEsp.createQuery(SicTipoEgreso.class)
					.field("cveTipoEgreso").equal(cveTipoEgreso).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoEgreso;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
