/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Frank
 * 
 */
@Entity("SIC_RIESGO_TRABAJO")
public class SicRiesgoTrabajo {

	@Id
	private ObjectId id;
	private Integer cveRiesgoTrabajo;
	private String desRiesgoTrabajo;
	private Integer cveSexo;
	private Integer numEdad;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveRiesgoTrabajo
	 */
	public Integer getCveRiesgoTrabajo() {
		return cveRiesgoTrabajo;
	}

	/**
	 * @param cveRiesgoTrabajo
	 *            the cveRiesgoTrabajo to set
	 */
	public void setCveRiesgoTrabajo(Integer cveRiesgoTrabajo) {
		this.cveRiesgoTrabajo = cveRiesgoTrabajo;
	}

	/**
	 * @return the desRiesgoTrabajo
	 */
	public String getDesRiesgoTrabajo() {
		return desRiesgoTrabajo;
	}

	/**
	 * @param desRiesgoTrabajo
	 *            the desRiesgoTrabajo to set
	 */
	public void setDesRiesgoTrabajo(String desRiesgoTrabajo) {
		this.desRiesgoTrabajo = desRiesgoTrabajo;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the cveSexo
	 */
	public Integer getCveSexo() {
		return cveSexo;
	}

	/**
	 * @param cveSexo
	 *            the cveSexo to set
	 */
	public void setCveSexo(Integer cveSexo) {
		this.cveSexo = cveSexo;
	}

	/**
	 * @return the numEdad
	 */
	public Integer getNumEdad() {
		return numEdad;
	}

	/**
	 * @param numEdad
	 *            the numEdad to set
	 */
	public void setNumEdad(Integer numEdad) {
		this.numEdad = numEdad;
	}

}
