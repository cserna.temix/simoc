package mx.gob.imss.simo.ws.service.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dto.AccederDto;
import mx.gob.imss.simo.comun.dto.SiapDto;
import mx.gob.imss.simo.comun.entity.negocio.SitUnidadMedica;
import mx.gob.imss.simo.utils.ConectaWsAcceder;
import mx.gob.imss.simo.utils.ConectaWsSiap;
import mx.gob.imss.simo.ws.service.AccederWsService;


@Service("accederService")
public class AccederWsServiceImpl implements AccederWsService {

	
	final static Logger logger = Logger.getLogger(AccederWsServiceImpl.class.getName());
	private static final String  SIMO_OSB = "SIMO_OSB";

	private final static Integer tiempoEsperaSiap=6;
	private final static Integer tiempoEsperaAcceder=3;
	private static final Integer CONSTANTE_INTEGER_ESPERA = 1;
	
	
//	@Autowired
//	private ConnectionMongo connectionMongo;

	/**
	 * Metodo que consulta el webservice Acceder unificado interfaz OSB endpoint
	 * /SIMO-ESB/Core/PSSimoESB
	 * 
	 */
	public List<AccederDto> getInfo(String nss, String cpid, String aMedico) {
		List<AccederDto> listaConsulExtDto = null;
		
		ConectaWsAcceder cliente= new ConectaWsAcceder(nss, cpid, aMedico, SIMO_OSB);
		long tiempoRespuesta = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + tiempoEsperaAcceder;
		
		cliente.start();
		
		while(tiempoRespuesta >= TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())){
			
			if(cliente.getListaConsulExtDto()!=null){
				break;
			}else {
				try {
					TimeUnit.SECONDS.sleep(CONSTANTE_INTEGER_ESPERA);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
			
		}
		
		cliente.interrupt();
		
		if(cliente.getListaConsulExtDto()!=null){
			listaConsulExtDto=cliente.getListaConsulExtDto();
			if(cliente.getListaConsulExtDto().size()==1){
				//logger.info("ACCEDER Conecta:"+cliente.getConsulExtDto().getEncontrado());
				cliente.getListaConsulExtDto().get(0).setDescUnidadMed(buscarDescUnidadMedica(cliente.getListaConsulExtDto().get(0).getDescUnidadMed()));
				//logger.info("registro ENCONTRADO VERDE");
			}else{
				//logger.info("ACCEDER Conecta:"+cliente.getConsulExtDto().getEncontrado());
				//logger.info("registro NO ENCONTRADO ROJO");
			}
		}else{
			logger.info("ACCEDER No se conecta AMARILLO");
		}
		
		return listaConsulExtDto;
	}
	/**
	 * Metodo que consulta el webservice SIAP interfaz OSB endpoint
	 * /SIMO-ESB/Core/PSSimoESB
	 * 
	 */
	public SiapDto consultarSiap(String matricula, String delegacion) {
		SiapDto siapDto = null;
		
		ConectaWsSiap cliente = new ConectaWsSiap(matricula, delegacion,SIMO_OSB);
		
		long tiempoRespuesta = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + tiempoEsperaSiap;
		
		cliente.start();
		while(tiempoRespuesta >= TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())){
			
			if(cliente.getSiapDto()!=null){
				break;
			}
			else {
				try {
					TimeUnit.SECONDS.sleep(CONSTANTE_INTEGER_ESPERA);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
			
		}
		cliente.interrupt();
		if(cliente.getSiapDto()!=null){
			siapDto=cliente.getSiapDto();
			if((cliente.getSiapDto().getEncontrado())){
				//logger.info("SIAP Conecta:"+cliente.getSiapDto().getEncontrado());
				//logger.info("registro ENCONTRADO VERDE");
			}else{
				//logger.info("SIAP Conecta:"+cliente.getSiapDto().getEncontrado());
				//logger.info("registro NO ENCONTRADO ROJO");
			}
		}else{
			logger.info("SIAP No se conecta AMARILLO");
		}
		
		
		return siapDto;
		
	}
	
	

	
	private String buscarDescUnidadMedica(String cvePresupuestal) {
		String descUnidadMedica = "";
		// SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
			SitUnidadMedica uni = ds.createQuery(SitUnidadMedica.class).filter("cvePresupuestal", cvePresupuestal).get();
			descUnidadMedica = uni.getDesUnidadMedica();
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return descUnidadMedica;
	}
	
//	
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}	
	
}
