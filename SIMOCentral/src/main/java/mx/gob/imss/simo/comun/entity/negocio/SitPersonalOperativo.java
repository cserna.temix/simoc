/**
 * 
 */
package mx.gob.imss.simo.comun.entity.negocio;

import java.util.Date;
import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicRol;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIT_PERSONAL_OPERATIVO")
public class SitPersonalOperativo {

	@Id
	private ObjectId id;
	private String refCuentaActiveD;
	private String cveMatricula;
	private String refNombre;
	private String refApellidoPaterno;
	private String refApellidoMaterno;
	private List<SicRol> sdRol;
	private List<SitUnidadMedica> sdUnidadMedica;

	private String refNumTelefono;
	private String refNumExtension;
	private String refEmail;
	private Boolean indActivo;
	private int indBloqueado;
	private Date fecBaja;
	private Integer caIntentos;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveMatricula
	 */
	public String getCveMatricula() {
		return cveMatricula;
	}

	/**
	 * @param cveMatricula
	 *            the cveMatricula to set
	 */
	public void setCveMatricula(String cveMatricula) {
		this.cveMatricula = cveMatricula;
	}

	/**
	 * @return the refNombre
	 */
	public String getRefNombre() {
		return refNombre;
	}

	/**
	 * @param refNombre
	 *            the refNombre to set
	 */
	public void setRefNombre(String refNombre) {
		this.refNombre = refNombre;
	}

	/**
	 * @return the refApellidoPaterno
	 */
	public String getRefApellidoPaterno() {
		return refApellidoPaterno;
	}

	/**
	 * @param refApellidoPaterno
	 *            the refApellidoPaterno to set
	 */
	public void setRefApellidoPaterno(String refApellidoPaterno) {
		this.refApellidoPaterno = refApellidoPaterno;
	}

	/**
	 * @return the refApellidoMaterno
	 */
	public String getRefApellidoMaterno() {
		return refApellidoMaterno;
	}

	/**
	 * @param refApellidoMaterno
	 *            the refApellidoMaterno to set
	 */
	public void setRefApellidoMaterno(String refApellidoMaterno) {
		this.refApellidoMaterno = refApellidoMaterno;
	}

	/**
	 * @return the refEmail
	 */
	public String getRefEmail() {
		return refEmail;
	}

	/**
	 * @param refEmail
	 *            the refEmail to set
	 */
	public void setRefEmail(String refEmail) {
		this.refEmail = refEmail;
	}

	/**
	 * @return the indActivo
	 */
	public Boolean getIndActivo() {
		return indActivo;
	}

	/**
	 * @param indActivo
	 *            the indActivo to set
	 */
	public void setIndActivo(Boolean indActivo) {
		this.indActivo = indActivo;
	}

	/**
	 * @return the indBloqueado
	 */
	public int getIndBloqueado() {
		return indBloqueado;
	}

	/**
	 * @param indBloqueado
	 *            the indBloqueado to set
	 */
	public void setIndBloqueado(int indBloqueado) {
		this.indBloqueado = indBloqueado;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the refNumTelefono
	 */
	public String getRefNumTelefono() {
		return refNumTelefono;
	}

	/**
	 * @param refNumTelefono
	 *            the refNumTelefono to set
	 */
	public void setRefNumTelefono(String refNumTelefono) {
		this.refNumTelefono = refNumTelefono;
	}

	/**
	 * @return the refNumExtension
	 */
	public String getRefNumExtension() {
		return refNumExtension;
	}

	/**
	 * @param refNumExtension
	 *            the refNumExtension to set
	 */
	public void setRefNumExtension(String refNumExtension) {
		this.refNumExtension = refNumExtension;
	}

	public String getRefCuentaActiveD() {
		return refCuentaActiveD;
	}

	public void setRefCuentaActiveD(String refCuentaActiveD) {
		this.refCuentaActiveD = refCuentaActiveD;
	}

	public List<SicRol> getSdRol() {
		return sdRol;
	}

	public void setSdRol(List<SicRol> sdRol) {
		this.sdRol = sdRol;
	}

	public List<SitUnidadMedica> getSdUnidadMedica() {
		return sdUnidadMedica;
	}

	public void setSdUnidadMedica(List<SitUnidadMedica> sdUnidadMedica) {
		this.sdUnidadMedica = sdUnidadMedica;
	}

	public Integer getCaIntentos() {
		return caIntentos;
	}

	public void setCaIntentos(Integer caIntentos) {
		this.caIntentos = caIntentos;
	}

}
