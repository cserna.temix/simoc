/**
 * 
 */
package mx.gob.imss.simo.comun.core;

import java.io.Serializable;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import mx.gob.imss.simo.comun.dto.CatalogoDto;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.SelectEvent;

/**
 * @author francisco.rrios
 * 
 */
public abstract class AbstractBaseBean implements Serializable {

	/**
	 * 
	 */
	final static Logger logger = Logger.getLogger(AbstractBaseBean.class.getName());
	
	private static final long serialVersionUID = 1L;
	private Date fechaActual;
	private static final String IDIOMA = "idioma_locale";
	private static final String PAIS = "pais_locale";
	private static final String BUNDLE_LOCALE = "bundle.index.locale";
	protected static final Locale LOCALE = new Locale(ResourceBundle.getBundle(
			BUNDLE_LOCALE).getString(IDIOMA), ResourceBundle.getBundle(
			BUNDLE_LOCALE).getString(PAIS));
	private static final String ES_NUMERO="^[0-9]+$";
	private static final String ES_ALFANUMERICO="^[a-zA-Z0-9]+$";
	private static final String ES_ALFA="^[a-zA-Z]+$";

	/***
	 * Para mensajes de error
	 */
	protected static String CSS_MSG_ERROR="border:1px solid #000000 !important;box-shadow: 0 0 10px 2px #ff0000 !important;border-radius: 4px;";
	protected static String MSG_ERROR_ID="growlMessageError";
	protected static String MSG_WARN_ID="growlMessageWarning";
	protected static String MSG_INFO_ID="growlMessageInfo";
	protected static String growlMessageT=MSG_ERROR_ID;
	protected String tieneFoco;

	
	
	/**
	 * @param nombreBean
	 * @return
	 */
	public Object obtenerService(String service) {
		//@SuppressWarnings("resource")
		//ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/applicationContext.xml");
		//return context.getBean(service);
		return null;
	}

	/**
	 * Retorna un recurso referenciado a los mensajes del modulo de PostMortem
	 * */
	public static ResourceBundle obtenerMensajes() {
		return ResourceBundle.getBundle("bundle.index.Message",
				LOCALE);
	}

	/**
	 * Agregar un mensaje de Informacion, desde un archivo definido como
	 * resource boundle en faces-config
	 * 
	 * @param keyMensaje
	 *            llave del mensaje
	 */
	public void agregarInfo(String keyMensaje) {
		String msg = obtenerMensajes().getString(keyMensaje);
		growlMessageT=MSG_INFO_ID;
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
	}

	/**
	 * Agregar un mensaje de Erorr, desde un archivo definido como resource
	 * boundle en faces-config
	 * 
	 * @param keyMensaje
	 *            llave del mensaje
	 */
	public static void agregarError(String keyMensaje) {
		String msg = obtenerMensajes().getString(keyMensaje);
		growlMessageT=MSG_ERROR_ID;
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
	}
	
	
	public static void agregarErrorString(String keyMensaje) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, keyMensaje, keyMensaje));
	}
	
	public static void agregarError(String keyMensaje, String keyMensajeComplemento) {
		String msg = obtenerMensajes().getString(keyMensaje);
		String msgCompl = obtenerMensajes().getString(keyMensajeComplemento);
		growlMessageT=MSG_ERROR_ID;
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg+msgCompl));
	}

	/**
	 * Agregar un mensaje de Fatal, desde un archivo definido como resource
	 * boundle en faces-config
	 * 
	 * @param keyMensaje
	 *            llave del mensaje
	 */
	public static void agregarFatal(String keyMensaje) {
		String msg = obtenerMensajes().getString(keyMensaje);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_FATAL, msg, msg));
	}

	/**
	 * Agregar un mensaje de Warn, desde un archivo definido como resource
	 * boundle en faces-config
	 * 
	 * @param keyMensaje
	 *            llave del mensaje
	 */
	public static void agregarWarn(String keyMensaje) {
		String msg = obtenerMensajes().getString(keyMensaje);
		growlMessageT=MSG_WARN_ID;
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, msg, msg));
	}
	
	
	/**
	 * Agregar un mensaje de con algun Parametro desde un archivo definido como resource
	 * boundle en faces-config
	 * @param keyMensaje
	 * @param arguments
	 */
	public static void mensajeErrorConParametros(String keyMensaje, Object ... arguments) {
		String msg = MessageFormat.format(obtenerMensajes().getString(keyMensaje), arguments);
		growlMessageT=MSG_ERROR_ID;
		 FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
	}
	
	/**
	 * Agregar un mensaje de con algun Parametro desde un archivo definido como resource
	 * boundle en faces-config
	 * @param keyMensaje
	 * @param keyMensaje argumento
	 */
	public static void mensajeErrorCon2MsjParametros(String keyMensaje, String argumentskeyMensaje) {
		String msg = MessageFormat.format(obtenerMensajes().getString(keyMensaje), obtenerMensajes().getString(argumentskeyMensaje));
		growlMessageT=MSG_ERROR_ID;
		 FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
	}

	/*
	 * Metodo a Eliminar
	 */
	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	/**
	 * Obtiene el valor del atributo fechaActual.
	 * 
	 * @return el valor de fechaActual
	 */
	public Date getFechaActual() {
		if (fechaActual == null) {
			fechaActual = new Date();
		}
		return fechaActual;
	}

	/**
	 * Cambia formato de fecha
	 * 
	 * @param fecha
	 * @return
	 */

	public String convertirFecha(Date fecha) {
		SimpleDateFormat dateUtil = new SimpleDateFormat("dd/MM/YYYY");
		String fechaAlterna = dateUtil.format(fecha);
		return fechaAlterna;
	}
	
	/**
	 * String a Date
	 * 
	 */
	public Date stringToDate(String strFecha){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");		
		Date fecha = null;
		try {

		fecha = formatoDelTexto.parse(strFecha);

		} catch (ParseException ex) {

		ex.printStackTrace();

		}
		return fecha;
	}

	/**
	 * 
	 * @param value
	 * @param isHora
	 * @return
	 */
	public String formatearTiempo(int value, boolean isHora) {
		if (isHora) {
			if (value > 12)
				value = value - 12;
			if (value == 0)
				return "12";
		}
		if (value != 0 && value <= 9)
			return "0" + String.valueOf(value);
		return String.valueOf(value);
	}

	//OK
	public static int calculoSemanasEdad(String fecNacimiento) {
		int semanasEdad = 0;
		try {
			Date fechaNac = new SimpleDateFormat("dd/MM/yyyy")
					.parse(fecNacimiento);
			Calendar fechaNacimiento = Calendar.getInstance();
			Calendar fechaActual = Calendar.getInstance();
			// Se asigna la fecha recibida a la fecha de nacimiento.
			fechaNacimiento.setTime(fechaNac);

			long diff = fechaActual.getTime().getTime() - fechaNacimiento.getTime().getTime();//milisegundos de vida

			//long sobra = diff % 86400000l;
			//diff -= sobra; //con esto ya tenemos puros dias
			long dias = diff / 86400000l ; //estos son los dias en total que llevas de vida
			semanasEdad = (int) (dias / 7);
		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return semanasEdad;
	}
	
	public static int calculoSemanasEdadAcceder(String fecNacimiento) {
		int semanasEdad = 0;
		try {
			Date fechaNac = new SimpleDateFormat("yyyy/MM/dd")
					.parse(fecNacimiento);
			Calendar fechaNacimiento = Calendar.getInstance();
			Calendar fechaActual = Calendar.getInstance();
			// Se asigna la fecha recibida a la fecha de nacimiento.
			fechaNacimiento.setTime(fechaNac);

			long diff = fechaActual.getTime().getTime() - fechaNacimiento.getTime().getTime();//milisegundos de vida

			//long sobra = diff % 86400000l;
			//diff -= sobra; //con esto ya tenemos puros dias
			long dias = diff / 86400000l ; //estos son los dias en total que llevas de vida
			semanasEdad = (int) (dias / 7);
						
		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return semanasEdad;
	}
	
	//ok
	public static int calculoMesesEdad(String fecNacimiento) {
		int mesesEdad = 0;
		try {
			Date fechaNac = new SimpleDateFormat("dd/MM/yyyy")
					.parse(fecNacimiento);
			Calendar fechaNacimiento = Calendar.getInstance();
			Calendar fechaActual = Calendar.getInstance();
			// Se asigna la fecha recibida a la fecha de nacimiento.
			fechaNacimiento.setTime(fechaNac);

			int yy = fechaNacimiento.get(Calendar.YEAR);
			int mm = fechaNacimiento.get(Calendar.MONTH) + 1;
			int dd = fechaNacimiento.get(Calendar.DATE);

			int gdate = fechaActual.get(Calendar.DATE);
			int gmonth = fechaActual.get(Calendar.MONTH) + 1;
			int gyear = fechaActual.get(Calendar.YEAR);
			
			Integer age = gyear - yy;

			if (mm > gmonth && age > 0) {
				age = age - 1;
				mesesEdad = 12;
			}

			mesesEdad += age*12;
			
			
			if (gdate < dd) {
				gmonth--;		        
		    }
//		    if (gmonth < mm) {		        
//		    	gmonth += 12;
//		    }		    
		    mesesEdad += gmonth-mm;

		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return mesesEdad;
	}
	
	/**
	 * calcula la edad en anios del paciente enviandole el anio de nacimiento
	 * @param fecNacimiento
	 * @param fechaCap
	 * @return
	 */
	public static int calcularEdadAnnos(String fecNacimiento, Date fechaCap) {
		Date fechaNac = null;
		try {
			if(fecNacimiento.length() == 4){
				fechaNac = new SimpleDateFormat("yyyy").parse(fecNacimiento);
			}else{
				fechaNac = new SimpleDateFormat("dd/MM/yyyy").parse(fecNacimiento);
			}			
		} catch (Exception ex) {
			logger.info("Error:" + ex);
		}

		Calendar fechaNacimiento = Calendar.getInstance();
		fechaNacimiento.setTime(fechaNac);
		// Se crea un objeto con la fecha actual
		Calendar fechaActual = Calendar.getInstance();
		fechaActual.setTime(fechaCap);
		
		int age = fechaActual.get(Calendar.YEAR)- fechaNacimiento.get(Calendar.YEAR);
	    int mes =fechaActual.get(Calendar.MONTH)- fechaNacimiento.get(Calendar.MONTH);
	    int dia = fechaActual.get(Calendar.DATE)- fechaNacimiento.get(Calendar.DATE);
		// Se restan la fecha actual y la fecha de nacimiento
	    
	    if(mes<0 || (mes==0 && dia<0)){
	    	age--;
        }

		return age;
	}

	// Este es el método calcularEdad que se manda a llamar en el main
	public static Integer calcularEdad(String fecha) {
		Date fechaNac = null;
		try {
			/**
			 * Se puede cambiar la mascara por el formato de la fecha que se
			 * quiera recibir, por ejemplo año mes día "yyyy-MM-dd" en este caso
			 * es día mes año
			 */
			fechaNac = new SimpleDateFormat("dd-MM-yyyy").parse(fecha);
		} catch (Exception ex) {
			logger.info("Error:" + ex);
		}
		Calendar fechaNacimiento = Calendar.getInstance();
		// Se crea un objeto con la fecha actual
		Calendar fechaActual = Calendar.getInstance();
		// Se asigna la fecha recibida a la fecha de nacimiento.
		fechaNacimiento.setTime(fechaNac);
		// Se restan la fecha actual y la fecha de nacimiento
		int anno = fechaActual.get(Calendar.YEAR)
				- fechaNacimiento.get(Calendar.YEAR);
		int mes = fechaActual.get(Calendar.MONTH)
				- fechaNacimiento.get(Calendar.MONTH);
		int dia = fechaActual.get(Calendar.DATE)
				- fechaNacimiento.get(Calendar.DATE);
		// Se ajusta el año dependiendo el mes y el día
		if (mes < 0 || (mes == 0 && dia < 0)) {
			anno--;
		}
		// Regresa la edad en base a la fecha de nacimiento
		return anno;
	}
	
	/**
	 * 
	 * @param name es un string que queremos determinar si tiene solo letras
	 * @return TRUE si el string son solo dígitos
	 */
	
	public boolean isNumero(String name) {
	    char[] chars = name.toCharArray();
	    Boolean es=Boolean.FALSE;
	    for (char c : chars) {
	        if(Character.isDigit(c)) {
	        	es=Boolean.TRUE;
	        }else{
	        	return Boolean.FALSE;
	        }
	    }

	    return es;
	}

	/**
	 * @param fechaActual
	 *            the fechaActual to set
	 */
	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	
	/**
	 * @param actual Componente evaluado
	 * @param siguiente Componente siguiente al evaluado
	 */
	
	
	/**
	 * itemSeleccionado-> Selecciona  el objeto en la lista de CatalogoDto de acuerdo al dato seleccionado en el
	 * 					dropdown del autocomplete. Se realiza la búsqueda sobre la lista
	 * @param event			-Evento de busqueda
	 * @param listaString	-Lista de Strings que muestra los resultados
	 * @param componnetePf	-Componente Autocomplete
	 * @param listaCatalogo	-Lista de objetos tipo CatalogoDto del que obtendrá un objeto
	 * @return CatalogoDto	-El objeto encontrado
	 */
		
		public CatalogoDto itemSeleccionado(SelectEvent event, List<String> listaString, 
				AutoComplete componnetePf, List<CatalogoDto> listaCatalogo){
			CatalogoDto objeto=null;
			int index=listaString.indexOf(componnetePf.getValue().toString());
			logger.info("index: "+ index);
			objeto=listaCatalogo.get(index);
			logger.info( "objeto: "+objeto.getDescripcion() );
			return objeto;
		}
		
		
		/**
		 * completeTextComponente-> Selecciona  el objeto en la lista de CatalogoDto de acuerdo al dato seleccionado en el
		 * 					dropdown del autocomplete. Se realiza la búsqueda sobre la lista
		 * @param query			-Datos ingresados en el componente autocomplete
		 * @param max			-Número máximo de datos para realizar la busqueda
		 * @param listaString	-La lista de strings sobre la cual buscara
		 * @return				-La lista de resultados de acuerdo al query
		 */
		public List<String> completeTextComponente(String query, Integer max, List<String> listaString) {
			List<String> resultado = new ArrayList<String>();
	        if(query.length()<max){
	        	 String cadena=null;
	             Iterator<String> it = listaString.iterator();
	             while(it.hasNext()){
	             	cadena = it.next();
	             	if(StringUtils.containsIgnoreCase(cadena, query)){
	             		resultado.add(cadena);
	             	}   
	             }
	             return resultado;
	        }else{
	        	return null;
	        }	       
	    }
		
		
		/**
		 *  validaAutoComplete->Se ejecuta cuando el usuario manda la acción llegando al número de caracteres requeridos para seleccionar un 
		 *  					elemento de la busqueda
		 * @param componente	-El componente autocomplete
		 * @param max			-Numero máximo para relizar la búsqueda
		 * @param listaString	-Lista sobre la cual busca
		 * @param listaCatalogo	-Lista de objetos tipo CatalogoDto del que obtendrá un objeto
		 * @return				-El objeto encontrado
		 */
		public CatalogoDto validaAutoComplete(AutoComplete componente,Integer max,List<String> listaString, List<CatalogoDto> listaCatalogo){
			CatalogoDto objeto=null;
			String textoIngresado=null;
			if(componente.getValue()!=null){
				textoIngresado=componente.getValue().toString();					
				if(textoIngresado!=null && !textoIngresado.isEmpty() && textoIngresado.length()>=max){
					List<String> resultado = new ArrayList<String>();
					String cadena;
			        Iterator<String> it = listaString.iterator();
			        while(it.hasNext()){
			        	cadena = it.next();
		             	if(StringUtils.containsIgnoreCase(cadena, textoIngresado)){
		             		resultado.add(cadena);
		             	}			                     
			        }				
		        	if(resultado.size()>=1){		           
		        		componente.setValue(resultado.get(0));
		        		int index=0;
						if(componente.getValue()!=null){
							index=listaString.indexOf(componente.getValue().toString());
							objeto=listaCatalogo.get(index);	
						}	
		            }else{
		            	componente.setValue(null);
		            }	
									
				}
			}
			return objeto;
		}
	
	public void componenteValido(InputText actual, InputText siguiente){
		//actual.setDisabled(Boolean.TRUE);
		//siguiente.setDisabled(Boolean.FALSE);
		this.setTieneFoco(siguiente.getClientId());
		actual.setStyle("");
	}
	

	public void componenteValido(InputText actual, SelectOneMenu siguiente){
		//actual.setDisabled(Boolean.TRUE);
		//siguiente.setDisabled(Boolean.FALSE);
		this.setTieneFoco(siguiente.getClientId());
		actual.setStyle("");
	}
	
	public void componenteValido(SelectOneMenu actual, InputText siguiente){
		//actual.setDisabled(Boolean.TRUE);
		//siguiente.setDisabled(Boolean.FALSE);
		this.setTieneFoco(siguiente.getClientId());
		actual.setStyle("");		
	}
	
	public void componenteValido(SelectOneMenu actual, CommandButton siguiente){
		//actual.setDisabled(Boolean.TRUE);
		//siguiente.setDisabled(Boolean.FALSE);
		this.setTieneFoco(siguiente.getClientId());
		actual.setStyle("");		
	}
	
	public void componenteValido(InputMask actual, SelectOneMenu siguiente){
		//actual.setDisabled(Boolean.TRUE);
		//siguiente.setDisabled(Boolean.FALSE);
		this.setTieneFoco(siguiente.getClientId());
		actual.setStyle("");		
	}
	
	public void componenteValido(InputMask actual, InputText siguiente){
		//actual.setDisabled(Boolean.TRUE);
		//siguiente.setDisabled(Boolean.FALSE);
		this.setTieneFoco(siguiente.getClientId());
		actual.setStyle("");		
	}
	public void componenteValido(AutoComplete actual, InputText siguiente){
		this.setTieneFoco(siguiente.getClientId());
		actual.setStyle("");		
	}
	public void componenteValido(InputText actual, AutoComplete siguiente){
		this.setTieneFoco(siguiente.getClientId());
		actual.setStyle("");
	}
	
	public void componenteNoValido(InputText actual){
		actual.setStyle(CSS_MSG_ERROR);
		this.setTieneFoco(actual.getClientId());
	}
	
	public void componenteNoValido(SelectOneMenu actual){
		actual.setStyle(CSS_MSG_ERROR);
		this.setTieneFoco(actual.getClientId());
	}
	
	public void componenteNoValidoSelectFocus(SelectOneMenu actual){
		actual.setStyle(CSS_MSG_ERROR);
		this.setTieneFoco(actual.getClientId());
	}
	
	public void componenteNoValido(InputMask actual){
		actual.setStyle(CSS_MSG_ERROR);
		this.setTieneFoco(actual.getClientId());
	}
	
	public void componenteNoValido(AutoComplete actual){
		actual.setStyle(CSS_MSG_ERROR);
		this.setTieneFoco(actual.getClientId());
	}
	
	
	public List<String> cargaListaString(List<CatalogoDto> listaDto){
		CatalogoDto catalogo=new CatalogoDto();
		List<String> listaString=new ArrayList<String>();
		Iterator<CatalogoDto> it = listaDto.iterator();
        while(it.hasNext()){
        	catalogo = it.next();
        	listaString.add(catalogo.getDescripcion());            
        }
        return listaString;
	}
	
	
	
	public String getGrowlMessageT() {
		return growlMessageT;
	}

	public void setGrowlMessageT(String growlMessageT) {
		this.growlMessageT = growlMessageT;
	}

	public String getTieneFoco() {
		return tieneFoco;
	}

	public void setTieneFoco(String tieneFoco) {
		this.tieneFoco = tieneFoco;
	}

	

	public boolean esNumero(String cadena){
		Pattern pattern = Pattern.compile(ES_NUMERO);
		Matcher matcher = pattern.matcher(cadena);
		return matcher.matches();
	}
	
	public boolean esAlfaNumerico(String cadena){
		Pattern pattern = Pattern.compile(ES_ALFANUMERICO);
		Matcher matcher = pattern.matcher(cadena);
		return matcher.matches();
	}
	
	public boolean esAlfabeto(String cadena){
		Pattern pattern = Pattern.compile(ES_ALFA);
		Matcher matcher = pattern.matcher(cadena);
		return matcher.matches();
	}
	
	public boolean validaDiferenteEmpty(InputText campo){
		return campo.getValue() != null && !campo.getValue().toString().isEmpty();	
	}
	public boolean validaDiferenteEmpty(InputMask campo){
		return campo.getValue() != null && !campo.getValue().toString().isEmpty();	
	}
	public boolean validaDiferenteEmpty(SelectOneMenu campo){
		return campo.getValue() != null && !campo.getValue().toString().isEmpty();	
	}
	public boolean validaDiferenteEmpty(AutoComplete campo){
		return campo.getValue() != null && !campo.getValue().toString().isEmpty();	
	}
	
	
}
