/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicInformacionAdicionalDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicional;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicInformacionAdicionalDao")
public class SicInformacionAdicionalDaoImpl implements
		SicInformacionAdicionalDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.comun.dao.SicInformacionAdicionalDao#burcarTodos()
	 */
	@Override
	public List<SicInformacionAdicional> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicInformacionAdicional> listInformacionAdicional = new ArrayList<SicInformacionAdicional>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listInformacionAdicional = dsEsp.createQuery(
					SicInformacionAdicional.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}


		return listInformacionAdicional;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicInformacionAdicionalDao#burcarPorId(java
	 * .lang.Integer)
	 */
	@Override
	public List<SicInformacionAdicional> burcarPorId(
			String cveInformacionAdicional) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicInformacionAdicional> listInformacionAdicional = new ArrayList<SicInformacionAdicional>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listInformacionAdicional = dsEsp
					.createQuery(SicInformacionAdicional.class)
					.field("cveInformacionAdicional")
					.equal(cveInformacionAdicional).asList();

		} catch (Exception e) {
			e.getMessage();
		}

		

		return listInformacionAdicional;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicInformacionAdicionalDao#burcarPorTipo(java.lang.Integer)
	 */
	@Override
	public List<SicInformacionAdicional> burcarPorTipo(Integer tipo) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicInformacionAdicional> listInformacionAdicional = new ArrayList<SicInformacionAdicional>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listInformacionAdicional = dsEsp
					.createQuery(SicInformacionAdicional.class)
					.field("indCveCie10")
					.equal(tipo).asList();

		} catch (Exception e) {
			e.getMessage();
		}

		

		return listInformacionAdicional;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
