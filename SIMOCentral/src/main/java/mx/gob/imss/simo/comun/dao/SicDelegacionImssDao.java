/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicDelegacionImss;

/**
 * @author francisco.rrios
 *
 */
public interface SicDelegacionImssDao {

	/**
	 * @return
	 */
	List<SicDelegacionImss> burcarTodos();

	/**
	 * @param idDelUMAES
	 * @return
	 */
	List<SicDelegacionImss> burcarPorId(String idDelUMAES);
}
