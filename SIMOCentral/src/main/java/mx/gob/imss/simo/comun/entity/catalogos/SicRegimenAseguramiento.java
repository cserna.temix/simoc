/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_REGIMEN_ASEGURAMIENTO")
public class SicRegimenAseguramiento {
	@Id
	private ObjectId id;
	private Integer numRegimenAseguramiento;
	private String cveRegimenAseguramiento;
	private String desRegimenAseguramiento;
	private Date fecBaja;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Integer getNumRegimenAseguramiento() {
		return numRegimenAseguramiento;
	}

	public void setNumRegimenAseguramiento(Integer numRegimenAseguramiento) {
		this.numRegimenAseguramiento = numRegimenAseguramiento;
	}

	public String getCveRegimenAseguramiento() {
		return cveRegimenAseguramiento;
	}

	public void setCveRegimenAseguramiento(String cveRegimenAseguramiento) {
		this.cveRegimenAseguramiento = cveRegimenAseguramiento;
	}

	public String getDesRegimenAseguramiento() {
		return desRegimenAseguramiento;
	}

	public void setDesRegimenAseguramiento(String desRegimenAseguramiento) {
		this.desRegimenAseguramiento = desRegimenAseguramiento;
	}

	public Date getFecBaja() {
		return fecBaja;
	}

	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
