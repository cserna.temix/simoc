/**
 * 
 */
package mx.gob.imss.simo.comun.service.impl;

import java.util.ArrayList;
import java.util.List;

import mx.gob.imss.simo.comun.core.AssemblerDtoEntity;
import mx.gob.imss.simo.comun.dao.Sic1eraVezDao;
import mx.gob.imss.simo.comun.dao.SicAccidentesLesionesDao;
import mx.gob.imss.simo.comun.dao.SicAgregadoMedicoDao;
import mx.gob.imss.simo.comun.dao.SicConsultorioDao;
import mx.gob.imss.simo.comun.dao.SicDelegacionImssDao;
import mx.gob.imss.simo.comun.dao.SicEspecialidadDao;
import mx.gob.imss.simo.comun.dao.SicEspecialidadUnidadDao;
import mx.gob.imss.simo.comun.dao.SicInformacionAdicionalDao;
import mx.gob.imss.simo.comun.dao.SicMetodoAnticonceptivoDao;
import mx.gob.imss.simo.comun.dao.SicPaseServicioDao;
import mx.gob.imss.simo.comun.dao.SicRiesgosTrabajoDao;
import mx.gob.imss.simo.comun.dao.SicTipoDiarreaDao;
import mx.gob.imss.simo.comun.dao.SicTipoIngresoDao;
import mx.gob.imss.simo.comun.dao.SicTipoPrestadorDao;
import mx.gob.imss.simo.comun.dao.SicTipoUrgenciaDao;
import mx.gob.imss.simo.comun.dao.SicTurnoDao;
import mx.gob.imss.simo.comun.dao.SitUnidadMedicaDao;
import mx.gob.imss.simo.comun.dto.CatalogoDto;
import mx.gob.imss.simo.comun.dto.DelegacionesUmaesDto;
import mx.gob.imss.simo.comun.dto.UnidadMedicaDto;
import mx.gob.imss.simo.comun.entity.catalogos.Sic1eraVez;
import mx.gob.imss.simo.comun.entity.catalogos.SicAccidentesLesiones;
import mx.gob.imss.simo.comun.entity.catalogos.SicAgregadoMedico;
import mx.gob.imss.simo.comun.entity.catalogos.SicConsultorio;
import mx.gob.imss.simo.comun.entity.catalogos.SicDelegacionImss;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidadUnidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicional;
import mx.gob.imss.simo.comun.entity.catalogos.SicMetodoAnticonceptivo;
import mx.gob.imss.simo.comun.entity.catalogos.SicPaseServicio;
import mx.gob.imss.simo.comun.entity.catalogos.SicRiesgoTrabajo;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoDiarrea;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoIngreso;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoPrestador;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoUrgencia;
import mx.gob.imss.simo.comun.entity.catalogos.SicTurno;
import mx.gob.imss.simo.comun.entity.negocio.SitUnidadMedica;
import mx.gob.imss.simo.comun.enums.EnumModuloAtencion;
import mx.gob.imss.simo.comun.service.CatalogosService;
import mx.gob.imss.simo.consultaexterna.pojo.SdInformacionAdicional;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoDiarrea;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author francisco.rrios
 * 
 */
@Service("catalogosService")
public class CatalogosServiceImpl implements CatalogosService {

	@Autowired
	private SicEspecialidadDao sicEspecialidadDao;
	@Autowired
	private SicEspecialidadUnidadDao sicEspecialidadUnidadDao;
	@Autowired
	private SicTipoPrestadorDao sicTipoPrestadorDao;
	@Autowired
	private SicTurnoDao sicTurnoDao;
	@Autowired
	private SicAccidentesLesionesDao sicAccidentesLesionesDao;
	@Autowired
	private SicAgregadoMedicoDao sicAgregadoMedicoDao;
	@Autowired
	private SicMetodoAnticonceptivoDao sicMetodoAnticonceptivoDao;
	@Autowired
	private SicPaseServicioDao sicPaseServicioDao;
	@Autowired
	private SicRiesgosTrabajoDao sicRiesgosTrabajoDao;
	@Autowired
	private SicTipoIngresoDao sicTipoIngresoDao;
	@Autowired
	private SicDelegacionImssDao sicDelegacionImssDao;
	@Autowired
	private SitUnidadMedicaDao sitUnidadMedicaDao;
	@Autowired
	private SicTipoUrgenciaDao sicTipoUrgenciaDao;
	@Autowired
	private SicTipoDiarreaDao sicTipoDiarreaDao;
	@Autowired
	private SicInformacionAdicionalDao sicInformacionAdicionalDao;
	@Autowired
	private Sic1eraVezDao sic1eraVezDao; 
	@Autowired
	private SicConsultorioDao sicConsultorioDao;

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboEspecialidades() {
		List<CatalogoDto> listadoEspecialidadRes = new ArrayList<CatalogoDto>();
		try {
			List<SicEspecialidad> listadoEspecialidad = sicEspecialidadDao.burcarTodos();

			listadoEspecialidadRes = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoEspecialidad,
							CatalogoDto.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoEspecialidadRes;

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboEspeUnidad(String cvePresupuestal, int claveModuloAtencion) {
		List<CatalogoDto> listadoEspecialidadRes = new ArrayList<CatalogoDto>();
		String MODULO;
		try {
			if(claveModuloAtencion == EnumModuloAtencion.CONSULTA_EXTERNA.getClave()){
				MODULO = "indConsultaExterna";
			} else if (claveModuloAtencion == EnumModuloAtencion.HOSPITALIZACION.getClave()){
				MODULO = "indHospital";
			} else if (claveModuloAtencion == EnumModuloAtencion.PEDIATRIA.getClave()){
				MODULO = "indPediatria";
			} else { //Cirugia
				MODULO = "indCirugia";
			}
			
			List<SicEspecialidadUnidad> listadoEspecialidad = sicEspecialidadUnidadDao.burcarPorUnidadMed(cvePresupuestal, MODULO);

			listadoEspecialidadRes = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoEspecialidad,
							CatalogoDto.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoEspecialidadRes;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboAgreMed() {
		List<CatalogoDto> listadoAgreMed = new ArrayList<CatalogoDto>();
		try {

			List<SicAgregadoMedico> listadoAgre = sicAgregadoMedicoDao
					.burcarTodos();

			listadoAgreMed = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoAgre, CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoAgreMed;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboPases() {
		List<CatalogoDto> listadoPasesRes = new ArrayList<CatalogoDto>();
		try {
			List<SicPaseServicio> listadoPases = sicPaseServicioDao
					.burcarTodos();

			listadoPasesRes = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoPases, CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoPasesRes;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboMetPPF() {
		List<CatalogoDto> listadoMetPPF = new ArrayList<CatalogoDto>();
		try {
			List<SicMetodoAnticonceptivo> listadoMetodoPpf = sicMetodoAnticonceptivoDao
					.burcarTodos();

			listadoMetPPF = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoMetodoPpf,
							CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoMetPPF;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboAccLesiones() {
		List<CatalogoDto> listadoAccLesiones = new ArrayList<CatalogoDto>();
		try {

			List<SicAccidentesLesiones> listadoAccLes = sicAccidentesLesionesDao
					.burcarTodos();

			listadoAccLesiones = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoAccLes, CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoAccLesiones;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboRiesgos() {
		List<CatalogoDto> listadoRiesgos = new ArrayList<CatalogoDto>();
		try {

			List<SicRiesgoTrabajo> listadoriesgoTra = sicRiesgosTrabajoDao
					.burcarTodos();

			listadoRiesgos = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoriesgoTra,
							CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoRiesgos;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboTurnos() {
		List<CatalogoDto> listadoTurnos = new ArrayList<CatalogoDto>();

		try {

			List<SicTurno> listadoTur = sicTurnoDao.burcarTodos();

			listadoTurnos = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoTur, CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoTurnos;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboTipoIng() {
		List<CatalogoDto> listadoTipoIng = new ArrayList<CatalogoDto>();
		try {
			List<SicTipoIngreso> listadoTipo = sicTipoIngresoDao.burcarTodos();

			listadoTipoIng = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoTipo, CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoTipoIng;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboCamas() {// /////////////////////////
		List<CatalogoDto> listadoTipoIng = new ArrayList<CatalogoDto>();
		try {
			List<SicTipoIngreso> listadoTipo = sicTipoIngresoDao.burcarTodos();

			listadoTipoIng = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoTipo, CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoTipoIng;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboProcedencia() {// ////////////////////////////
		List<CatalogoDto> listadoTipoIng = new ArrayList<CatalogoDto>();
		try {
			List<SicTipoIngreso> listadoTipo = sicTipoIngresoDao.burcarTodos();

			listadoTipoIng = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoTipo, CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoTipoIng;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.service.CatalogosService#llenaComboTipoPrestador
	 * (mx.gob.imss.simo.comun.core.Mongo)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboTipoPrestador() {
		List<CatalogoDto> listadoTipoPrestador = new ArrayList<CatalogoDto>();
		try {

			List<SicTipoPrestador> listadoTipoPres = sicTipoPrestadorDao
					.burcarTodos();

			listadoTipoPrestador = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoTipoPres,
							CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoTipoPrestador;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DelegacionesUmaesDto> llenaComboDelegacion() {
		List<DelegacionesUmaesDto> listadoDelegacion = new ArrayList<DelegacionesUmaesDto>();
		try {
			List<SicDelegacionImss> listadoDelUMAES = sicDelegacionImssDao
					.burcarTodos();

			listadoDelegacion = (List<DelegacionesUmaesDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoDelUMAES,
							DelegacionesUmaesDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoDelegacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UnidadMedicaDto> llenaComboUnidadMedica(String cveDelegacion) {
		List<UnidadMedicaDto> listadoUnidadM = new ArrayList<UnidadMedicaDto>();
		try {
			List<SitUnidadMedica> listUnidadMedica = sitUnidadMedicaDao
					.burcarPorCveDelegacion(cveDelegacion);

			listadoUnidadM = (List<UnidadMedicaDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listUnidadMedica,
							UnidadMedicaDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoUnidadM;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.service.CatalogosService#llenaComboTipoUrgencia()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboTipoUrgencia() {
		List<CatalogoDto> listadoTipoUrgencia = new ArrayList<CatalogoDto>();
		try {

			List<SicTipoUrgencia> listadoTipoUrg = sicTipoUrgenciaDao
					.burcarTodos();

			listadoTipoUrgencia = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoTipoUrg,
							CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoTipoUrgencia;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.comun.service.CatalogosService#llenaCombo1Vez()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaCombo1Vez() {
		List<CatalogoDto> listado1Vez = new ArrayList<CatalogoDto>();
		try {

			List<Sic1eraVez> lista1Vez = sic1eraVezDao.burcarTodos();

			listado1Vez = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(lista1Vez,
							CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listado1Vez;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.service.CatalogosService#llenaComboInfoAdicional()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SdInformacionAdicional> llenaComboInfoAdicional(Integer tipo) {
		List<SdInformacionAdicional> listadoInfoAdicional = new ArrayList<SdInformacionAdicional>();
		try {

			List<SicInformacionAdicional> listadoInfoAdic = sicInformacionAdicionalDao.burcarPorTipo(tipo);

			listadoInfoAdicional = (List<SdInformacionAdicional>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoInfoAdic,
							SdInformacionAdicional.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoInfoAdicional;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.service.CatalogosService#llenaComboTipoDiarrea()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SdTipoDiarrea> llenaComboTipoDiarrea() {
		List<SdTipoDiarrea> listadoTipoDiarrea = new ArrayList<SdTipoDiarrea>();
		try {

			List<SicTipoDiarrea> listadoTipoDia = sicTipoDiarreaDao
					.burcarTodos();

			listadoTipoDiarrea = (List<SdTipoDiarrea>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoTipoDia,
							SdTipoDiarrea.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoTipoDiarrea;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.service.CatalogosService#llenaComboMetPPFXSexo
	 * (java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboMetPPFXSexo(Integer sexo) {
		List<CatalogoDto> listadoMetPPF = new ArrayList<CatalogoDto>();
		try {
			List<SicMetodoAnticonceptivo> listadoMetodoPpf = sicMetodoAnticonceptivoDao
					.burcarXSexo(sexo);

			listadoMetPPF = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoMetodoPpf,
							CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoMetPPF;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboMetPPFXSexoXEdad(Integer sexo, Integer edad) {
		List<CatalogoDto> listadoMetPPF = new ArrayList<CatalogoDto>();
		try {
			List<SicMetodoAnticonceptivo> listadoMetodoPpf = sicMetodoAnticonceptivoDao
					.burcarXSexoXEdad(sexo, edad);

			listadoMetPPF = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadoMetodoPpf,
							CatalogoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadoMetPPF;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDto> llenaComboConsultorios(String cvePresupuestal, Integer tipoServicio) {
		List<CatalogoDto> listadosicConsultorioRes = new ArrayList<CatalogoDto>();
		try {
			List<SicConsultorio> listadosicConsultorio = sicConsultorioDao
					.burcarPorUnidadEsp(cvePresupuestal, tipoServicio);

			listadosicConsultorioRes = (List<CatalogoDto>) AssemblerDtoEntity
					.convertirListEntidadToDto(listadosicConsultorio,
							CatalogoDto.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listadosicConsultorioRes;

	}
	

	/**
	 * @return the sicTipoPrestadorDao
	 */
	public SicTipoPrestadorDao getSicTipoPrestadorDao() {
		return sicTipoPrestadorDao;
	}

	/**
	 * @param sicTipoPrestadorDao
	 *            the sicTipoPrestadorDao to set
	 */
	@Autowired
	public void setSicTipoPrestadorDao(SicTipoPrestadorDao sicTipoPrestadorDao) {
		this.sicTipoPrestadorDao = sicTipoPrestadorDao;
	}

	/**
	 * @return the sicTurnoDao
	 */
	public SicTurnoDao getSicTurnoDao() {
		return sicTurnoDao;
	}

	/**
	 * @param sicTurnoDao
	 *            the sicTurnoDao to set
	 */
	@Autowired
	public void setSicTurnoDao(SicTurnoDao sicTurnoDao) {
		this.sicTurnoDao = sicTurnoDao;
	}

	/**
	 * @return the sicAccidentesLesionesDao
	 */
	public SicAccidentesLesionesDao getSicAccidentesLesionesDao() {
		return sicAccidentesLesionesDao;
	}

	/**
	 * @param sicAccidentesLesionesDao
	 *            the sicAccidentesLesionesDao to set
	 */
	@Autowired
	public void setSicAccidentesLesionesDao(
			SicAccidentesLesionesDao sicAccidentesLesionesDao) {
		this.sicAccidentesLesionesDao = sicAccidentesLesionesDao;
	}

	/**
	 * @return the sicAgregadoMedicoDao
	 */
	public SicAgregadoMedicoDao getSicAgregadoMedicoDao() {
		return sicAgregadoMedicoDao;
	}

	/**
	 * @param sicAgregadoMedicoDao
	 *            the sicAgregadoMedicoDao to set
	 */
	@Autowired
	public void setSicAgregadoMedicoDao(
			SicAgregadoMedicoDao sicAgregadoMedicoDao) {
		this.sicAgregadoMedicoDao = sicAgregadoMedicoDao;
	}

	/**
	 * @return the SicMetodoAnticonceptivoDao
	 */
	public SicMetodoAnticonceptivoDao getSicMetodoAnticonceptivoDao() {
		return sicMetodoAnticonceptivoDao;
	}

	/**
	 * @param SicMetodoAnticonceptivoDao
	 *            the SicMetodoAnticonceptivoDao to set
	 */
	@Autowired
	public void setSicMetodoAnticonceptivoDao(
			SicMetodoAnticonceptivoDao sicMetodoAnticonceptivoDao) {
		this.sicMetodoAnticonceptivoDao = sicMetodoAnticonceptivoDao;
	}

	

	
	/**
	 * @return the sicRiesgosTrabajoDao
	 */
	public SicRiesgosTrabajoDao getSicRiesgosTrabajoDao() {
		return sicRiesgosTrabajoDao;
	}

	/**
	 * @param sicRiesgosTrabajoDao
	 *            the sicRiesgosTrabajoDao to set
	 */
	@Autowired
	public void setSicRiesgosTrabajoDao(
			SicRiesgosTrabajoDao sicRiesgosTrabajoDao) {
		this.sicRiesgosTrabajoDao = sicRiesgosTrabajoDao;
	}

	/**
	 * @return the sicTipoIngresoDao
	 */
	public SicTipoIngresoDao getSicTipoIngresoDao() {
		return sicTipoIngresoDao;
	}

	/**
	 * @param sicTipoIngresoDao
	 *            the sicTipoIngresoDao to set
	 */
	@Autowired
	public void setSicTipoIngresoDao(SicTipoIngresoDao sicTipoIngresoDao) {
		this.sicTipoIngresoDao = sicTipoIngresoDao;
	}

	
	/**
	 * @return the SicTipoUrgenciaDao
	 */
	public SicTipoUrgenciaDao getSicTipoUrgenciaDao() {
		return sicTipoUrgenciaDao;
	}

	/**
	 * @param SicTipoUrgenciaDao
	 *            the SicTipoUrgenciaDao to set
	 */
	@Autowired
	public void setSicTipoUrgenciaDao(SicTipoUrgenciaDao sicTipoUrgenciaDao) {
		this.sicTipoUrgenciaDao = sicTipoUrgenciaDao;
	}

	/**
	 * @return the sicTipoDiarreaDao
	 */
	public SicTipoDiarreaDao getSicTipoDiarreaDao() {
		return sicTipoDiarreaDao;
	}

	/**
	 * @param sicTipoDiarreaDao
	 *            the sicTipoDiarreaDao to set
	 */
	@Autowired
	public void setSicTipoDiarreaDao(SicTipoDiarreaDao sicTipoDiarreaDao) {
		this.sicTipoDiarreaDao = sicTipoDiarreaDao;
	}

	public SicEspecialidadDao getSicEspecialidadDao() {
		return sicEspecialidadDao;
	}
	@Autowired
	public void setSicEspecialidadDao(SicEspecialidadDao sicEspecialidadDao) {
		this.sicEspecialidadDao = sicEspecialidadDao;
	}

	public SicPaseServicioDao getSicPaseServicioDao() {
		return sicPaseServicioDao;
	}
	@Autowired
	public void setSicPaseServicioDao(SicPaseServicioDao sicPaseServicioDao) {
		this.sicPaseServicioDao = sicPaseServicioDao;
	}

	public SicDelegacionImssDao getSicDelegacionImssDao() {
		return sicDelegacionImssDao;
	}
	@Autowired
	public void setSicDelegacionImssDao(SicDelegacionImssDao sicDelegacionImssDao) {
		this.sicDelegacionImssDao = sicDelegacionImssDao;
	}
	
	public SitUnidadMedicaDao getSitUnidadMedicaDao() {
		return sitUnidadMedicaDao;
	}
	@Autowired
	public void setSitUnidadMedicaDao(SitUnidadMedicaDao sitUnidadMedicaDao) {
		this.sitUnidadMedicaDao = sitUnidadMedicaDao;
	}

	/**
	 * @return the sicInformacionAdicionalDao
	 */
	public SicInformacionAdicionalDao getSicInformacionAdicionalDao() {
		return sicInformacionAdicionalDao;
	}

	/**
	 * @param sicInformacionAdicionalDao the sicInformacionAdicionalDao to set
	 */
	@Autowired
	public void setSicInformacionAdicionalDao(
			SicInformacionAdicionalDao sicInformacionAdicionalDao) {
		this.sicInformacionAdicionalDao = sicInformacionAdicionalDao;
	}

	/**
	 * @return the sic1eraVezDao
	 */
	public Sic1eraVezDao getSic1eraVezDao() {
		return sic1eraVezDao;
	}

	/**
	 * @param sic1eraVezDao the sic1eraVezDao to set
	 */
	@Autowired
	public void setSic1eraVezDao(Sic1eraVezDao sic1eraVezDao) {
		this.sic1eraVezDao = sic1eraVezDao;
	}

	/**
	 * @return the sicConsultorioDao
	 */
	public SicConsultorioDao getSicConsultorioDao() {
		return sicConsultorioDao;
	}

	/**
	 * @param sicConsultorioDao the sicConsultorioDao to set
	 */
	public void setSicConsultorioDao(SicConsultorioDao sicConsultorioDao) {
		this.sicConsultorioDao = sicConsultorioDao;
	}

	/**
	 * @return the sicEspecialidadUnidadDao
	 */
	public SicEspecialidadUnidadDao getSicEspecialidadUnidadDao() {
		return sicEspecialidadUnidadDao;
	}

	/**
	 * @param sicEspecialidadUnidadDao the sicEspecialidadUnidadDao to set
	 */
	public void setSicEspecialidadUnidadDao(
			SicEspecialidadUnidadDao sicEspecialidadUnidadDao) {
		this.sicEspecialidadUnidadDao = sicEspecialidadUnidadDao;
	}

	
}
