/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rodriguez
 *
 */
@Entity("SIC_SERVICIO_CUNERO")
public class SicServicioCunero {

	@Id
	private ObjectId id;
	private Integer cveServicioCunero;
	private String desServicioCunero;
	private Date fecBaja;
	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}
	/**
	 * @return the cveServicioCunero
	 */
	public Integer getCveServicioCunero() {
		return cveServicioCunero;
	}
	/**
	 * @param cveServicioCunero the cveServicioCunero to set
	 */
	public void setCveServicioCunero(Integer cveServicioCunero) {
		this.cveServicioCunero = cveServicioCunero;
	}
	/**
	 * @return the desServicioCunero
	 */
	public String getDesServicioCunero() {
		return desServicioCunero;
	}
	/**
	 * @param desServicioCunero the desServicioCunero to set
	 */
	public void setDesServicioCunero(String desServicioCunero) {
		this.desServicioCunero = desServicioCunero;
	}
	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}
	/**
	 * @param fecBaja the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}
	
	
}
