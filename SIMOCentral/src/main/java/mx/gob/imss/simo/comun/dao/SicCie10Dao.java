/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicCie10;

/**
 * @author francisco.rrios
 *
 */
public interface SicCie10Dao {

	/**
	 * @return
	 */
	List<SicCie10> burcarTodos();

	SicCie10 buscarPorId(String cveCie10);
	
	SicCie10 buscarPorRangoPorId(String cveCie10, String rangoInicio, String rangoFin);
	

	/**
	 * @param cveDiagnostico
	 * @param edad
	 * @param sexo
	 * @param edadSem
	 * @return
	 */
	List<SicCie10> burcarEdadSexo(String cveDiagnostico, int edad, Integer sexo, boolean edadSem);
}
