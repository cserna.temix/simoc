/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicInformacionAdicionalEmbarazoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicionalEmbarazo;

/**
 * @author sm
 * 
 */
@Repository("sicInformacionAdicionalEmbarazoDao")
public class SicInformacionAdicionalEmbarazoDaoImpl implements
		SicInformacionAdicionalEmbarazoDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicInformacionAdicionalVIHDao#burcarTodos()
	 */
	@Override
	public List<SicInformacionAdicionalEmbarazo> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicInformacionAdicionalEmbarazo> listInformacionAdicionalEmbarazo = new ArrayList<SicInformacionAdicionalEmbarazo>();
		try {
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listInformacionAdicionalEmbarazo = dsEsp.createQuery(
					SicInformacionAdicionalEmbarazo.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listInformacionAdicionalEmbarazo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicInformacionAdicionalVIHDao#burcarPorId()
	 */
	@Override
	public SicInformacionAdicionalEmbarazo burcarEmbarazoPorId(Integer cveCD4) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		SicInformacionAdicionalEmbarazo informacionAdicionalEmbarazo = new SicInformacionAdicionalEmbarazo();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
		
			informacionAdicionalEmbarazo = ds.createQuery(SicInformacionAdicionalEmbarazo.class).field("cveInfoAdicionalEmb").equal(cveCD4).get();

		} catch (Exception e) {
			e.getMessage();
		}
		return informacionAdicionalEmbarazo;
	}
	
}
