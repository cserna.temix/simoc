/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_TIPO_URGENCIA")
public class SicTipoUrgencia {

	@Id
	private ObjectId id;
	private Integer cveTipoUrgencia;
	private String desTipoUrgencia;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * 
	 */
	public SicTipoUrgencia() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the cveTipoUrgencia
	 */
	public Integer getCveTipoUrgencia() {
		return cveTipoUrgencia;
	}

	/**
	 * @param cveTipoUrgencia
	 *            the cveTipoUrgencia to set
	 */
	public void setCveTipoUrgencia(Integer cveTipoUrgencia) {
		this.cveTipoUrgencia = cveTipoUrgencia;
	}

	/**
	 * @return the desTipoUrgencia
	 */
	public String getDesTipoUrgencia() {
		return desTipoUrgencia;
	}

	/**
	 * @param desTipoUrgencia
	 *            the desTipoUrgencia to set
	 */
	public void setDesTipoUrgencia(String desTipoUrgencia) {
		this.desTipoUrgencia = desTipoUrgencia;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
