/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoIngresoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoIngreso;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicTipoIngresoDao")
public class SicTipoIngresoDaoImpl implements SicTipoIngresoDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicTipoIngreso> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoIngreso> listTipoIngreso = new ArrayList<SicTipoIngreso>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoIngreso = dsEsp.createQuery(SicTipoIngreso.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listTipoIngreso;
	}

	@Override
	public List<SicTipoIngreso> burcarPorId(Integer cveTipoIngreso) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoIngreso> listTipoIngreso = new ArrayList<SicTipoIngreso>();
		try {

			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoIngreso = dsEsp.createQuery(SicTipoIngreso.class)
					.field("cveTipoIngreso").equal(cveTipoIngreso)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listTipoIngreso;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
