package mx.gob.imss.simo.comun.dto;

import java.util.Date;

import org.bson.types.ObjectId;

public class SicCie10DTO {

	private ObjectId id;
	private String cveCie10;
	private String desCie10;
	private Integer cveSexo;
	private Integer numEdadInferiorSemanas;
	private Integer numEdadInferiorAnios;
	private Integer numEdadSuperiorSemanas;
	private Integer numEdadSuperiorAnios;
	private Date fecBaja;
	private String refSexo;
	private String refLimiteEdadInferior;
	private String refLimiteEdadSuperior;
	private Integer indTriv;
	private Integer indErrad;
	private Integer indNotifInternacional;
	private Integer indNotifObligatoria;
	private Integer indNotifInmediata;
	private Integer indNotifObstetrica;
	private Integer indNoValidaBasicaDef;
	private Integer indNoValidaAfeccionHosp;
	private Integer indValidaMuerteFetal;
	private String refActualizacionCie10;
	private String refAnioModificacion;
	private Integer numAnioVigencia;
	private String refCatalago;
	private Integer indEnfCronica;
	private Integer numClaveAparato;
	private Integer indTransmisible;
	private Integer indPrincipalConsExterna;
	private Integer indNivel1;
	private Integer indNivel2;
	private Integer indNivel3;
	private Integer numRenglon;
	private String desEnfermedad;
	private Integer numFiltro;
	private Integer numCapituloCie10;
	private Integer numGrupoCie10;
	private Integer numSubGrupoCie10;
	private String refCategoriaCie10;
	private Integer indExclusion;
	private Integer numAgrupamiento;
	private Integer numCodigoConsecutivo;
	private Integer indVigente;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveCie10
	 */
	public String getCveCie10() {
		return cveCie10;
	}

	/**
	 * @param cveCie10
	 *            the cveCie10 to set
	 */
	public void setCveCie10(String cveCie10) {
		this.cveCie10 = cveCie10;
	}

	/**
	 * @return the desCie10
	 */
	public String getDesCie10() {
		return desCie10;
	}

	/**
	 * @param desCie10
	 *            the desCie10 to set
	 */
	public void setDesCie10(String desCie10) {
		this.desCie10 = desCie10;
	}

	public Integer getCveSexo() {
		return cveSexo;
	}

	public void setCveSexo(Integer cveSexo) {
		this.cveSexo = cveSexo;
	}

	/**
	 * @return the numEdadInferiorSemanas
	 */
	public Integer getNumEdadInferiorSemanas() {
		return numEdadInferiorSemanas;
	}

	/**
	 * @param numEdadInferiorSemanas
	 *            the numEdadInferiorSemanas to set
	 */
	public void setNumEdadInferiorSemanas(Integer numEdadInferiorSemanas) {
		this.numEdadInferiorSemanas = numEdadInferiorSemanas;
	}

	/**
	 * @return the numEdadSuperiorSemanas
	 */
	public Integer getNumEdadSuperiorSemanas() {
		return numEdadSuperiorSemanas;
	}

	/**
	 * @param numEdadSuperiorSemanas
	 *            the numEdadSuperiorSemanas to set
	 */
	public void setNumEdadSuperiorSemanas(Integer numEdadSuperiorSemanas) {
		this.numEdadSuperiorSemanas = numEdadSuperiorSemanas;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	public Integer getNumEdadInferiorAnios() {
		return numEdadInferiorAnios;
	}

	public void setNumEdadInferiorAnios(Integer numEdadInferiorAnios) {
		this.numEdadInferiorAnios = numEdadInferiorAnios;
	}

	/**
	 * @return the numEdadSuperiorAnios
	 */
	public Integer getNumEdadSuperiorAnios() {
		return numEdadSuperiorAnios;
	}

	/**
	 * @param numEdadSuperiorAnios
	 *            the numEdadSuperiorAnios to set
	 */
	public void setNumEdadSuperiorAnios(Integer numEdadSuperiorAnios) {
		this.numEdadSuperiorAnios = numEdadSuperiorAnios;
	}

	public String getRefSexo() {
		return refSexo;
	}

	public void setRefSexo(String refSexo) {
		this.refSexo = refSexo;
	}

	public String getRefLimiteEdadInferior() {
		return refLimiteEdadInferior;
	}

	public void setRefLimiteEdadInferior(String refLimiteEdadInferior) {
		this.refLimiteEdadInferior = refLimiteEdadInferior;
	}

	public String getRefLimiteEdadSuperior() {
		return refLimiteEdadSuperior;
	}

	public void setRefLimiteEdadSuperior(String refLimiteEdadSuperior) {
		this.refLimiteEdadSuperior = refLimiteEdadSuperior;
	}

	public Integer getIndTriv() {
		return indTriv;
	}

	public void setIndTriv(Integer indTriv) {
		this.indTriv = indTriv;
	}

	public Integer getIndErrad() {
		return indErrad;
	}

	public void setIndErrad(Integer indErrad) {
		this.indErrad = indErrad;
	}

	public Integer getIndNotifInternacional() {
		return indNotifInternacional;
	}

	public void setIndNotifInternacional(Integer indNotifInternacional) {
		this.indNotifInternacional = indNotifInternacional;
	}

	public Integer getIndNotifObligatoria() {
		return indNotifObligatoria;
	}

	public void setIndNotifObligatoria(Integer indNotifObligatoria) {
		this.indNotifObligatoria = indNotifObligatoria;
	}

	public Integer getIndNotifInmediata() {
		return indNotifInmediata;
	}

	public void setIndNotifInmediata(Integer indNotifInmediata) {
		this.indNotifInmediata = indNotifInmediata;
	}

	public Integer getIndNotifObstetrica() {
		return indNotifObstetrica;
	}

	public void setIndNotifObstetrica(Integer indNotifObstetrica) {
		this.indNotifObstetrica = indNotifObstetrica;
	}

	public Integer getIndNoValidaBasicaDef() {
		return indNoValidaBasicaDef;
	}

	public void setIndNoValidaBasicaDef(Integer indNoValidaBasicaDef) {
		this.indNoValidaBasicaDef = indNoValidaBasicaDef;
	}

	public Integer getIndNoValidaAfeccionHosp() {
		return indNoValidaAfeccionHosp;
	}

	public void setIndNoValidaAfeccionHosp(Integer indNoValidaAfeccionHosp) {
		this.indNoValidaAfeccionHosp = indNoValidaAfeccionHosp;
	}

	public Integer getIndValidaMuerteFetal() {
		return indValidaMuerteFetal;
	}

	public void setIndValidaMuerteFetal(Integer indValidaMuerteFetal) {
		this.indValidaMuerteFetal = indValidaMuerteFetal;
	}

	public String getRefActualizacionCie10() {
		return refActualizacionCie10;
	}

	public void setRefActualizacionCie10(String refActualizacionCie10) {
		this.refActualizacionCie10 = refActualizacionCie10;
	}

	public String getRefAnioModificacion() {
		return refAnioModificacion;
	}

	public void setRefAnioModificacion(String refAnioModificacion) {
		this.refAnioModificacion = refAnioModificacion;
	}

	public Integer getNumAnioVigencia() {
		return numAnioVigencia;
	}

	public void setNumAnioVigencia(Integer numAnioVigencia) {
		this.numAnioVigencia = numAnioVigencia;
	}

	public String getRefCatalago() {
		return refCatalago;
	}

	public void setRefCatalago(String refCatalago) {
		this.refCatalago = refCatalago;
	}

	public Integer getIndEnfCronica() {
		return indEnfCronica;
	}

	public void setIndEnfCronica(Integer indEnfCronica) {
		this.indEnfCronica = indEnfCronica;
	}

	public Integer getNumClaveAparato() {
		return numClaveAparato;
	}

	public void setNumClaveAparato(Integer numClaveAparato) {
		this.numClaveAparato = numClaveAparato;
	}

	public Integer getIndTransmisible() {
		return indTransmisible;
	}

	public void setIndTransmisible(Integer indTransmisible) {
		this.indTransmisible = indTransmisible;
	}

	public Integer getIndPrincipalConsExterna() {
		return indPrincipalConsExterna;
	}

	public void setIndPrincipalConsExterna(Integer indPrincipalConsExterna) {
		this.indPrincipalConsExterna = indPrincipalConsExterna;
	}

	public Integer getIndNivel1() {
		return indNivel1;
	}

	public void setIndNivel1(Integer indNivel1) {
		this.indNivel1 = indNivel1;
	}

	public Integer getIndNivel2() {
		return indNivel2;
	}

	public void setIndNivel2(Integer indNivel2) {
		this.indNivel2 = indNivel2;
	}

	public Integer getIndNivel3() {
		return indNivel3;
	}

	public void setIndNivel3(Integer indNivel3) {
		this.indNivel3 = indNivel3;
	}

	public Integer getNumRenglon() {
		return numRenglon;
	}

	public void setNumRenglon(Integer numRenglon) {
		this.numRenglon = numRenglon;
	}

	public String getDesEnfermedad() {
		return desEnfermedad;
	}

	public void setDesEnfermedad(String desEnfermedad) {
		this.desEnfermedad = desEnfermedad;
	}

	public Integer getNumFiltro() {
		return numFiltro;
	}

	public void setNumFiltro(Integer numFiltro) {
		this.numFiltro = numFiltro;
	}

	public Integer getNumCapituloCie10() {
		return numCapituloCie10;
	}

	public void setNumCapituloCie10(Integer numCapituloCie10) {
		this.numCapituloCie10 = numCapituloCie10;
	}

	public Integer getNumGrupoCie10() {
		return numGrupoCie10;
	}

	public void setNumGrupoCie10(Integer numGrupoCie10) {
		this.numGrupoCie10 = numGrupoCie10;
	}

	public Integer getNumSubGrupoCie10() {
		return numSubGrupoCie10;
	}

	public void setNumSubGrupoCie10(Integer numSubGrupoCie10) {
		this.numSubGrupoCie10 = numSubGrupoCie10;
	}

	public String getRefCategoriaCie10() {
		return refCategoriaCie10;
	}

	public void setRefCategoriaCie10(String refCategoriaCie10) {
		this.refCategoriaCie10 = refCategoriaCie10;
	}

	public Integer getIndExclusion() {
		return indExclusion;
	}

	public void setIndExclusion(Integer indExclusion) {
		this.indExclusion = indExclusion;
	}

	public Integer getNumAgrupamiento() {
		return numAgrupamiento;
	}

	public void setNumAgrupamiento(Integer numAgrupamiento) {
		this.numAgrupamiento = numAgrupamiento;
	}

	public Integer getNumCodigoConsecutivo() {
		return numCodigoConsecutivo;
	}

	public void setNumCodigoConsecutivo(Integer numCodigoConsecutivo) {
		this.numCodigoConsecutivo = numCodigoConsecutivo;
	}

	public Integer getIndVigente() {
		return indVigente;
	}

	public void setIndVigente(Integer indVigente) {
		this.indVigente = indVigente;
	}

}
