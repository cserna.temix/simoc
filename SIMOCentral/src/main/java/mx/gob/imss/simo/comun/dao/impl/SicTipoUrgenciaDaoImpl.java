/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoUrgenciaDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoUrgencia;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicTipoUrgenciaDao")
public class SicTipoUrgenciaDaoImpl implements SicTipoUrgenciaDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/**
	 * 
	 */
	public SicTipoUrgenciaDaoImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<SicTipoUrgencia> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoUrgencia> listTipoUrgencia = new ArrayList<SicTipoUrgencia>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoUrgencia = dsEsp.createQuery(SicTipoUrgencia.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listTipoUrgencia;
	}

	@Override
	public List<SicTipoUrgencia> burcarPorId(Integer cveTipoUrgencia) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoUrgencia> listTipoUrgencia = new ArrayList<SicTipoUrgencia>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoUrgencia = dsEsp.createQuery(SicTipoUrgencia.class)
					.field("cveTipoUrgencia").equal(cveTipoUrgencia).asList();

		} catch (Exception e) {
			e.getMessage();
		}
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listTipoUrgencia;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
