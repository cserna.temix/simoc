package mx.gob.imss.simo.comun.enums;

public enum EnumModuloAtencion {
	
	CONSULTA_EXTERNA (1, "Consulta Externa"),
	HOSPITALIZACION(2, "Hospitalizacion"),
	PEDIATRIA(3, "Pediatria"),
	CIRUGIA(4, "Cirugia")
	
	;
	
	
	
	private int clave;
	private String valor;
	
	private EnumModuloAtencion(int clave, String valor) {
		// TODO Auto-generated constructor stub
		this.clave = clave;
		this.valor = valor;
	}
	
	/**
	 * @return
	 */
	public int getClave() {
		return clave;
	}
	
	/**
	 * @return
	 */
	public String getValor() {
		return valor;
	}

}
