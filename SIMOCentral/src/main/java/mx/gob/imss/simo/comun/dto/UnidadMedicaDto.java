package mx.gob.imss.simo.comun.dto;

public class UnidadMedicaDto {

	private String id;
	private int indTitular;
	private String cvePresupuestal;
	private String desUnidadMedica;
	private String cveDelegacion;
	private String desDelegacion;
	private String cveUnidadPresupuestal;

	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	public int getIndTitular() {
		return indTitular;
	}

	public void setIndTitular(int indTitular) {
		this.indTitular = indTitular;
	}

	public String getCveUnidadPresupuestal() {
		return cveUnidadPresupuestal;
	}

	public void setCveUnidadPresupuestal(String cveUnidadPresupuestal) {
		this.cveUnidadPresupuestal = cveUnidadPresupuestal;
	}

	public String getDesUnidadMedica() {
		return desUnidadMedica;
	}

	public void setDesUnidadMedica(String desUnidadMedica) {
		this.desUnidadMedica = desUnidadMedica;
	}

	public String getCveDelegacion() {
		return cveDelegacion;
	}

	public void setCveDelegacion(String cveDelegacion) {
		this.cveDelegacion = cveDelegacion;
	}

	public String getDesDelegacion() {
		return desDelegacion;
	}

	public void setDesDelegacion(String desDelegacion) {
		this.desDelegacion = desDelegacion;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "UnidadMedicaDto [id=" + id + ", indTitular=" + indTitular
				+ ", cvePresupuestal=" + cvePresupuestal + ", desUnidadMedica="
				+ desUnidadMedica + ", cveDelegacion=" + cveDelegacion
				+ ", desDelegacion=" + desDelegacion
				+ ", cveUnidadPresupuestal=" + cveUnidadPresupuestal + "]";
	}

}
