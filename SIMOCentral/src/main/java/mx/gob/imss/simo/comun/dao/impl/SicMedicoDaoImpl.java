/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicMedicoDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicMedico;

/**
 * @author francisco.rrios
 *
 */
@Repository("sicMedicoDao")
public class SicMedicoDaoImpl implements SicMedicoDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicMedicoDao#burcarTodos()
	 */
	@Override
	public List<SicMedico> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicMedico> listMedico = new ArrayList<SicMedico>();
		try {
		
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listMedico = dsEsp.createQuery(SicMedico.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}
		return listMedico;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicMedicoDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicMedico> burcarPorId(String cveMatricula) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicMedico> listMedico = new ArrayList<SicMedico>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listMedico = dsEsp.createQuery(SicMedico.class)
					.field("cveMatricula").equal(cveMatricula).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listMedico;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
