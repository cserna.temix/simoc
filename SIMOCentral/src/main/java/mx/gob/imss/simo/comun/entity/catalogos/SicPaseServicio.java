/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_PASE_SERVICIO")
public class SicPaseServicio {

	@Id
	private ObjectId id;
	private Integer cvePaseServicio;
	private String desPaseServicio;
	private String desTipoServicio;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cvePaseServicio
	 */
	public Integer getCvePaseServicio() {
		return cvePaseServicio;
	}

	/**
	 * @param cvePaseServicio
	 *            the cvePaseServicio to set
	 */
	public void setCvePaseServicio(Integer cvePaseServicio) {
		this.cvePaseServicio = cvePaseServicio;
	}

	/**
	 * @return the desPaseServicio
	 */
	public String getDesPaseServicio() {
		return desPaseServicio;
	}

	/**
	 * @param desPaseServicio
	 *            the desPaseServicio to set
	 */
	public void setDesPaseServicio(String desPaseServicio) {
		this.desPaseServicio = desPaseServicio;
	}

	/**
	 * @return the desTipoServicio
	 */
	public String getDesTipoServicio() {
		return desTipoServicio;
	}

	/**
	 * @param desTipoServicio
	 *            the desTipoServicio to set
	 */
	public void setDesTipoServicio(String desTipoServicio) {
		this.desTipoServicio = desTipoServicio;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
