/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicAgregadoMedico;

/**
 * @author francisco.rrios
 * 
 */
public interface SicAgregadoMedicoDao {

	/**
	 * @return
	 */
	List<SicAgregadoMedico> burcarTodos();

	/**
	 * @param idAgregadoMedico
	 * @return
	 */
	List<SicAgregadoMedico> burcarPorId(String idAgregadoMedico);
}
