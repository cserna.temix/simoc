/**
 * nt
 */
package mx.gob.imss.simo.comun.core;

import java.util.Date;

import mx.gob.imss.simo.comun.dto.CatalogoDto;
import mx.gob.imss.simo.comun.dto.DelegacionesUmaesDto;
import mx.gob.imss.simo.comun.dto.SicCie10DTO;
import mx.gob.imss.simo.comun.dto.SicCie9DTO;
import mx.gob.imss.simo.comun.dto.UnidadMedicaDto;
import mx.gob.imss.simo.comun.entity.catalogos.Sic1eraVez;
import mx.gob.imss.simo.comun.entity.catalogos.SicAccidentesLesiones;
import mx.gob.imss.simo.comun.entity.catalogos.SicAgregadoMedico;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie10;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie9;
import mx.gob.imss.simo.comun.entity.catalogos.SicConsultorio;
import mx.gob.imss.simo.comun.entity.catalogos.SicDelegacionImss;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidadUnidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicInformacionAdicional;
import mx.gob.imss.simo.comun.entity.catalogos.SicLugarAtencion;
import mx.gob.imss.simo.comun.entity.catalogos.SicMedico;
import mx.gob.imss.simo.comun.entity.catalogos.SicMetodoAnticonceptivo;
import mx.gob.imss.simo.comun.entity.catalogos.SicPaseServicio;
import mx.gob.imss.simo.comun.entity.catalogos.SicRiesgoTrabajo;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoDiarrea;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoIngreso;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoPrestador;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoUrgencia;
import mx.gob.imss.simo.comun.entity.catalogos.SicTurno;
import mx.gob.imss.simo.comun.entity.catalogos.SicUnidadAdscripcion;
import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;
import mx.gob.imss.simo.comun.entity.negocio.SitPaciente;
import mx.gob.imss.simo.comun.entity.negocio.SitUnidadMedica;
import mx.gob.imss.simo.consultaexterna.pojo.Sd1eraVez;
import mx.gob.imss.simo.consultaexterna.pojo.SdConsultorio;
import mx.gob.imss.simo.consultaexterna.pojo.SdDelegacion;
import mx.gob.imss.simo.consultaexterna.pojo.SdEspecialidades;
import mx.gob.imss.simo.consultaexterna.pojo.SdFormato;
import mx.gob.imss.simo.consultaexterna.pojo.SdInformacionAdicional;
import mx.gob.imss.simo.consultaexterna.pojo.SdLugarAtencion;
import mx.gob.imss.simo.consultaexterna.pojo.SdMedico;
import mx.gob.imss.simo.consultaexterna.pojo.SdMetodoAnticonceptivo;
import mx.gob.imss.simo.consultaexterna.pojo.SdPaciente;
import mx.gob.imss.simo.consultaexterna.pojo.SdPaseServicio;
import mx.gob.imss.simo.consultaexterna.pojo.SdRiesgoTrabajo;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoDiarrea;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoPrestador;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoUrgencia;
import mx.gob.imss.simo.consultaexterna.pojo.SdTurno;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadAdscripcion;
import mx.gob.imss.simo.model.ConsultaExternaDto;

/**
 * @author Frank
 */
public class MapperEntityToDto {

    public static Object conversion(Object entidad, Class<?> dto) throws Exception {

        try {

            String entidadNombre = entidad.getClass().getSimpleName();

            switch (entidadNombre) {

                case "SicEspecialidad":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicEspecialidad ent = (SicEspecialidad) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getNumTipoServicio());
                        catDto.setClaveDes(ent.getCveEspecialidad());
                        catDto.setDescripcion(ent.getCveEspecialidad().concat(" ").concat(ent.getDesEspecialidad()));
                        return catDto;
                    } else if (dto.getSimpleName().equals("SdEspecialidades")) {
                        SicEspecialidad ent = (SicEspecialidad) entidad;
                        SdEspecialidades sd = new SdEspecialidades();
                        sd.setCveEspecialidad(ent.getCveEspecialidad());
                        sd.setDesEspecialidad(ent.getDesEspecialidad());
                        sd.setNumTipoServicio(ent.getNumTipoServicio());
                        return sd;
                    }
                    break;

                case "SicEspecialidadUnidad":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicEspecialidadUnidad ent = (SicEspecialidadUnidad) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getNumTipoServicio());
                        catDto.setClaveDes(ent.getCveEspecialidad());
                        catDto.setDescripcion(ent.getCveEspecialidad().concat(" ").concat(ent.getDesEspecialidad()));
                        catDto.setNumTipoServicio(ent.getNumTipoServicio());
                        return catDto;
                    } else if (dto.getSimpleName().equals("SdEspecialidades")) {
                        SicEspecialidadUnidad ent = (SicEspecialidadUnidad) entidad;
                        SdEspecialidades sd = new SdEspecialidades();
                        sd.setCveEspecialidad(ent.getCveEspecialidad());
                        sd.setDesEspecialidad(ent.getDesEspecialidad());
                        sd.setNumTipoServicio(ent.getNumTipoServicio());
                        return sd;
                    }
                    break;
                case "SicAccidentesLesiones":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicAccidentesLesiones ent = (SicAccidentesLesiones) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCveAccidenteLesion());
                        catDto.setDescripcion(
                                ent.getCveAccidenteLesion().toString().concat(" ").concat(ent.getDesAccidenteLesion()));
                        return catDto;
                    }
                    break;

                case "SicAgregadoMedico":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicAgregadoMedico ent = (SicAgregadoMedico) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCveRegimen());
                        catDto.setDescripcion(ent.getDesRegimen());

                        return catDto;
                    }
                    break;

                case "SicMetodoAnticonceptivo":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicMetodoAnticonceptivo ent = (SicMetodoAnticonceptivo) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCveMetodoAnticonceptivo());
                        catDto.setDescripcion(ent.getCveMetodoAnticonceptivo().toString().concat(" ")
                                .concat(ent.getDesMetodoAnticonceptivo()));

                        return catDto;
                    } else if (dto.getSimpleName().equals("SdMetodoAnticonceptivo")) {
                        SicMetodoAnticonceptivo ent = (SicMetodoAnticonceptivo) entidad;
                        SdMetodoAnticonceptivo sd = new SdMetodoAnticonceptivo(ent.getCveMetodoAnticonceptivo(),
                                ent.getCveMetodoAnticonceptivo().toString().concat(" ")
                                        .concat(ent.getDesMetodoAnticonceptivo()));
                        return sd;
                    }
                    break;

                case "SicPaseServicio":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicPaseServicio ent = (SicPaseServicio) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCvePaseServicio());
                        catDto.setDescripcion(
                                ent.getCvePaseServicio().toString().concat(" ").concat(ent.getDesPaseServicio()));

                        return catDto;
                    } else if (dto.getSimpleName().equals("SdPaseServicio")) {
                        SicPaseServicio ent = (SicPaseServicio) entidad;
                        SdPaseServicio sd = new SdPaseServicio();
                        sd.setCvePaseServicio(ent.getCvePaseServicio());
                        sd.setDesPaseServicio(ent.getDesPaseServicio());
                        return sd;
                    }
                    break;

                case "SicRiesgoTrabajo":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicRiesgoTrabajo ent = (SicRiesgoTrabajo) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCveRiesgoTrabajo());
                        catDto.setDescripcion(
                                ent.getCveRiesgoTrabajo().toString().concat(" ").concat(ent.getDesRiesgoTrabajo()));

                        return catDto;
                    } else if (dto.getSimpleName().equals("SdRiesgoTrabajo")) {
                        SicRiesgoTrabajo ent = (SicRiesgoTrabajo) entidad;
                        SdRiesgoTrabajo sd = new SdRiesgoTrabajo(ent.getCveRiesgoTrabajo(), ent.getDesRiesgoTrabajo());
                        return sd;
                    }
                    break;

                case "SicTipoIngreso":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicTipoIngreso ent = (SicTipoIngreso) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCveTipoIngreso());
                        catDto.setDescripcion(
                                ent.getCveTipoIngreso().toString().concat(" ").concat(ent.getDesTipoIngreso()));

                        return catDto;
                    }
                    break;

                case "SicTurno":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicTurno ent = (SicTurno) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCveTurno());
                        catDto.setDescripcion(ent.getCveTurno().toString().concat(" ").concat(ent.getDesTurno()));
                        return catDto;
                    } else if (dto.getSimpleName().equals("SdTurno")) {
                        SicTurno ent = (SicTurno) entidad;
                        SdTurno sd = new SdTurno();
                        sd.setCveTurno(ent.getCveTurno());
                        sd.setDesTurno(ent.getDesTurno());
                        return sd;
                    }
                    break;

                case "SicTipoUrgencia":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicTipoUrgencia ent = (SicTipoUrgencia) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCveTipoUrgencia());
                        catDto.setDescripcion(
                                ent.getCveTipoUrgencia().toString().concat(" ").concat(ent.getDesTipoUrgencia()));
                        return catDto;
                    } else if (dto.getSimpleName().equals("SdTipoUrgencia")) {
                        SicTipoUrgencia ent = (SicTipoUrgencia) entidad;
                        SdTipoUrgencia sd = new SdTipoUrgencia();
                        sd.setCveTipoUrgencia(ent.getCveTipoUrgencia());
                        sd.setDesTipoUrgencia(ent.getDesTipoUrgencia());
                        return sd;
                    }
                    break;

                case "SicDelegacionImss":
                    if (dto.getSimpleName().equals("DelegacionesUmaesDto")) {
                        SicDelegacionImss ent = (SicDelegacionImss) entidad;
                        DelegacionesUmaesDto oDto = new DelegacionesUmaesDto();
                        oDto.setId(ent.getId().toString());
                        oDto.setNombreDelegacionUmae(
                                ent.getCveDelegacion().toString().concat(" ").concat(ent.getDesDelegacion()));
                        oDto.setCveDelegacionUmae(ent.getCveDelegacion());
                        return oDto;
                    } else if (dto.getSimpleName().equals("SdDelegacion")) {
                        SicDelegacionImss ent = (SicDelegacionImss) entidad;
                        SdDelegacion sd = new SdDelegacion();
                        sd.setCveDelegacion(ent.getCveDelegacion());
                        sd.setDesDelegacion(ent.getDesDelegacion());
                        return sd;
                    }
                    break;

                case "SitUnidadMedica":
                    if (dto.getSimpleName().equals("UnidadMedicaDto")) {
                        SitUnidadMedica ent = (SitUnidadMedica) entidad;
                        UnidadMedicaDto oDto = new UnidadMedicaDto();
                        // oDto.setClave(ent.getId().toString());
                        oDto.setCveDelegacion(ent.getCveDelegacion());
                        oDto.setCvePresupuestal(ent.getCvePresupuestal());
                        oDto.setDesDelegacion(ent.getDesDelegacion());
                        oDto.setDesUnidadMedica(ent.getDesUnidadMedica());
                        oDto.setCveUnidadPresupuestal(ent.getCveUnidadPresupuestal());
                        oDto.setIndTitular(ent.getIndTitular());
                        return oDto;
                    }
                    break;

                case "SicTipoPrestador":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicTipoPrestador ent = (SicTipoPrestador) entidad;
                        CatalogoDto oDto = new CatalogoDto();
                        oDto.setClave(ent.getCveTipoPrestador());
                        oDto.setDescripcion(
                                ent.getCveTipoPrestador().toString().concat(" ").concat(ent.getDesTipoPrestador()));
                        return oDto;
                    } else if (dto.getSimpleName().equals("SdTipoPrestador")) {
                        SicTipoPrestador ent = (SicTipoPrestador) entidad;
                        SdTipoPrestador sd = new SdTipoPrestador();
                        sd.setCveTipoPrestador(ent.getCveTipoPrestador());
                        sd.setDesTipoPrestador(ent.getDesTipoPrestador());
                        return sd;
                    }
                    break;

                case "SitConsultaExterna":
                    if (dto.getSimpleName().equals("ConsultaExternaDto")) {
                        SitConsultaExterna ent = (SitConsultaExterna) entidad;
                        ConsultaExternaDto oDto = new ConsultaExternaDto(
                                ent.getSdEncabezado().get(0).getFecAltaEncabezado(),
                                ent.getSdEncabezado().get(0).getSdMedico().get(0).getCveMatricula(),
                                ent.getSdEncabezado().get(0).getSdConsultorio().get(0).getCveConsultorio(),
                                ent.getSdEncabezado().get(0).getNumHorasTrabajadas(),
                                ent.getSdEncabezado().get(0).getNumConsultasNoOtorgadas(),
                                ent.getSdEncabezado().get(0).getNumCitasNoCumplidas());
                        return oDto;
                    }
                    break;
                case "SdFormato":
                    if (dto.getSimpleName().equals("ConsultaExternaDto")) {
                        SdFormato ent = (SdFormato) entidad;
                        ConsultaExternaDto oDto = new ConsultaExternaDto();
                        // oDto.setFecha(ent.getStpCaptura());
                        oDto.setNumConsulta(ent.getNumConsecutivo());
                        oDto.setNss(ent.getSdPaciente().get(0).getNumNss());
                        // oDto.setCveEspecialidad(ent.getSdEspecialidad().get(0).getCveEspecialidad());
                        // oDto.setDesEspecialidad(ent.getSdEspecialidad().get(0).getDesEspecialidad());
                        // oDto.setMatriculaMedico(ent.getSdMedico().get(0).getCveMatricula());
                        oDto.setNombre(ent.getSdPaciente().get(0).getRefNombre().concat(" ")
                                .concat(ent.getSdPaciente().get(0).getRefApellidoPaterno()));
                        if (ent.getSdPaciente().get(0).getRefApellidoMaterno() != null) {
                            oDto.setNombre(oDto.getNombre().concat(" ")
                                    .concat(ent.getSdPaciente().get(0).getRefApellidoMaterno()));
                        }
                        oDto.setAgregadoMedico(ent.getSdPaciente().get(0).getRefAgregadoMedico());
                        if (ent.getSdDiagnosticos() != null && ent.getSdDiagnosticos().size() != 0) {
                            oDto.setCodigoCie(
                                    ent.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0).getCveCie10());
                            oDto.setDesDiagPrin(
                                    ent.getSdDiagnosticos().get(0).getSdDiagnosticoPrincipal().get(0).getDesCie10());
                        }

                        if (ent.getInd1EraVez() != null) {
                            oDto.setOcasionServicio(ent.getInd1EraVez() == 1 ? "Primera Vez" : "Subsecuente");
                        }
                        oDto.setStpActualiza(ent.getStpActualiza());
                        if (ent.getStpCaptura() == null) {
                            oDto.setStpCaptura(new Date());
                        }
                        oDto.setStpCaptura(ent.getStpCaptura());
                        return oDto;
                    }
                    break;
                case "SicCie10":
                    if (dto.getSimpleName().equals("SicCie10DTO")) {
                        SicCie10 ent = (SicCie10) entidad;
                        SicCie10DTO oDto = new SicCie10DTO();
                        oDto.setId(ent.getId());
                        oDto.setCveCie10(ent.getCveCie10());
                        oDto.setDesCie10(ent.getDesCie10());
                        oDto.setCveSexo(ent.getCveSexo());
                        oDto.setNumEdadInferiorSemanas(ent.getNumEdadInferiorSemanas());
                        oDto.setNumEdadInferiorAnios(ent.getNumEdadInferiorAnios());
                        oDto.setNumEdadSuperiorSemanas(ent.getNumEdadSuperiorSemanas());
                        oDto.setNumEdadSuperiorAnios(ent.getNumEdadSuperiorAnios());
                        oDto.setFecBaja(ent.getFecBaja());
                        oDto.setRefSexo(ent.getRefSexo());
                        oDto.setRefLimiteEdadInferior(ent.getRefLimiteEdadInferior());
                        oDto.setRefLimiteEdadSuperior(ent.getRefLimiteEdadSuperior());
                        oDto.setIndTriv(ent.getIndTriv());
                        oDto.setIndErrad(ent.getIndErrad());
                        oDto.setIndNotifInternacional(ent.getIndNotifInternacional());
                        oDto.setIndNotifObligatoria(ent.getIndNotifObligatoria());
                        oDto.setIndNotifInmediata(ent.getIndNotifInmediata());
                        oDto.setIndNotifObstetrica(ent.getIndNotifObstetrica());
                        oDto.setIndNoValidaBasicaDef(ent.getIndNoValidaBasicaDef());
                        oDto.setIndNoValidaAfeccionHosp(ent.getIndNoValidaAfeccionHosp());
                        oDto.setIndValidaMuerteFetal(ent.getIndValidaMuerteFetal());
                        oDto.setRefActualizacionCie10(ent.getRefActualizacionCie10());
                        oDto.setRefAnioModificacion(ent.getRefAnioModificacion());
                        oDto.setNumAnioVigencia(ent.getNumAnioVigencia());
                        oDto.setRefCatalago(ent.getRefCatalago());
                        oDto.setIndEnfCronica(ent.getIndEnfCronica());
                        oDto.setNumClaveAparato(ent.getNumClaveAparato());
                        oDto.setIndTransmisible(ent.getIndTransmisible());
                        oDto.setIndPrincipalConsExterna(ent.getIndPrincipalConsExterna());
                        oDto.setIndNivel1(ent.getIndNivel1());
                        oDto.setIndNivel2(ent.getIndNivel2());
                        oDto.setIndNivel3(ent.getIndNivel3());
                        oDto.setNumRenglon(ent.getNumRenglon());
                        oDto.setDesEnfermedad(ent.getDesEnfermedad());
                        oDto.setNumFiltro(ent.getNumFiltro());
                        oDto.setNumCapituloCie10(ent.getNumCapituloCie10());
                        oDto.setNumGrupoCie10(ent.getNumGrupoCie10());
                        oDto.setNumSubGrupoCie10(ent.getNumSubGrupoCie10());
                        oDto.setRefCategoriaCie10(ent.getRefCategoriaCie10());
                        oDto.setIndExclusion(ent.getIndExclusion());
                        oDto.setNumAgrupamiento(ent.getNumAgrupamiento());
                        oDto.setNumCodigoConsecutivo(ent.getNumCodigoConsecutivo());
                        oDto.setIndVigente(ent.getIndVigente());
                        return oDto;
                    }
                    break;
                case "SicCie9":
                    if (dto.getSimpleName().equals("SicCie9DTO")) {
                        SicCie9 ent = (SicCie9) entidad;
                        SicCie9DTO oDto = new SicCie9DTO();
                        oDto.setId(ent.getId());
                        oDto.setCveCie9(ent.getCveCie9());
                        oDto.setDesCie9(ent.getDesCie9());
                        oDto.setCveSexo(ent.getCveSexo());
                        oDto.setNumEdadInferiorAnios(ent.getNumEdadInferiorAnios());
                        oDto.setNumEdadInferiorSemanas(ent.getNumEdadInferiorSemanas());
                        oDto.setNumEdadSuperiorAnios(ent.getNumEdadSuperiorAnios());
                        oDto.setNumEdadSuperiorSemanas(ent.getNumEdadSuperiorSemanas());
                        oDto.setFecBaja(ent.getFecBaja());
                        oDto.setNumFiltro(ent.getNumFiltro());
                        oDto.setDesCie9M(ent.getDesCie9M());
                        oDto.setNumAniosVigencia(ent.getNumAniosVigencia());
                        oDto.setRefUso(ent.getRefUso());
                        oDto.setRefActualizacion(ent.getRefActualizacion());
                        oDto.setIndVigente(ent.getIndVigente());
                        oDto.setNumProcedencia(ent.getNumProcedencia());
                        oDto.setIndNivel1(ent.getIndNivel1());
                        oDto.setIndNivel2(ent.getIndNivel2());
                        oDto.setIndNivel3(ent.getIndNivel3());
                        oDto.setNumCapituloCie9(ent.getNumCapituloCie9());
                        oDto.setNumGrupoCie9(ent.getNumGrupoCie9());
                        oDto.setNumCodigoConsecutivo(ent.getNumCodigoConsecutivo());
                        oDto.setRefBilateralidad(ent.getRefBilateralidad());
                        return oDto;
                    }
                    break;

                case "SicMedico":
                    if (dto.getSimpleName().equals("SdMedico")) {
                        SicMedico ent = (SicMedico) entidad;
                        SdMedico sd = new SdMedico();
                        sd.setCveMatricula(ent.getCveMatricula());
                        sd.setRefNombre(ent.getRefNombre());
                        sd.setRefApellidoPaterno(ent.getRefApellidoPaterno());
                        sd.setRefApellidoMaterno(ent.getRefApellidoMaterno());
                        return sd;
                    }
                    break;

                case "SicLugarAtencion":
                    if (dto.getSimpleName().equals("SdLugarAtencion")) {
                        SicLugarAtencion ent = (SicLugarAtencion) entidad;
                        SdLugarAtencion sd = new SdLugarAtencion();
                        sd.setCveLugarAtencion(ent.getCveLugarAtencion());
                        sd.setDesLugarAtencion(ent.getDesLugarAtencion());
                        return sd;
                    }
                    break;

                case "SitPaciente":
                    if (dto.getSimpleName().equals("SdPaciente")) {
                        SitPaciente ent = (SitPaciente) entidad;
                        SdPaciente sd = new SdPaciente();
                        sd.setNumNss(ent.getNumNss());
                        sd.setRefAgregadoMedico(ent.getRefAgregadoMedico());
                        sd.setRefNombre(ent.getRefNombre());
                        sd.setRefApellidoPaterno(ent.getRefApellidoPaterno());
                        sd.setRefApellidoMaterno(ent.getRefApellidoMaterno());
                        return sd;
                    }
                    break;

                case "SicUnidadAdscripcion":
                    if (dto.getSimpleName().equals("SdUnidadAdscripcion")) {
                        SicUnidadAdscripcion ent = (SicUnidadAdscripcion) entidad;
                        SdUnidadAdscripcion sd = new SdUnidadAdscripcion();
                        sd.setCvePresupuestal(ent.getCvePresupuestal().toString());
                        sd.setDesUnidadMedica(ent.getDesUnidadMedica());
                        return sd;
                    }
                    break;

                case "SicInformacionAdicional":
                    if (dto.getSimpleName().equals("SdInformacionAdicional")) {
                        SicInformacionAdicional ent = (SicInformacionAdicional) entidad;
                        SdInformacionAdicional sd = new SdInformacionAdicional();
                        sd.setCveInformacionAdicional(ent.getCveInformacionAdicional());
                        sd.setDesInformacionAdicional(ent.getCveInformacionAdicional().toString().concat(" ")
                                .concat(ent.getDesInformacionAdicional()));
                        return sd;
                    }
                    break;

                case "SicTipoDiarrea":
                    if (dto.getSimpleName().equals("SdTipoDiarrea")) {
                        SicTipoDiarrea ent = (SicTipoDiarrea) entidad;
                        SdTipoDiarrea sd = new SdTipoDiarrea();
                        sd.setCveTipoDiarrea(ent.getCveTipoDiarrea());
                        sd.setDesTipoDiarrea(
                                ent.getCveTipoDiarrea().toString().concat(" ").concat(ent.getDesTipoDiarrea()));
                        return sd;
                    }
                    break;
                case "Sic1eraVez":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        Sic1eraVez ent = (Sic1eraVez) entidad;
                        CatalogoDto catDto = new CatalogoDto();
                        catDto.setClave(ent.getCve1eraVez());
                        catDto.setDescripcion(ent.getCve1eraVez().toString().concat(" ").concat(ent.getDes1eraVez()));
                        return catDto;
                    } else if (dto.getSimpleName().equals("Sd1eraVez")) {
                        Sic1eraVez ent = (Sic1eraVez) entidad;
                        Sd1eraVez sd = new Sd1eraVez();
                        sd.setCve1eraVez(ent.getCve1eraVez());
                        sd.setDes1eraVez(ent.getDes1eraVez());
                        return sd;
                    }
                    break;
                case "SicConsultorio":
                    if (dto.getSimpleName().equals("CatalogoDto")) {
                        SicConsultorio ent = (SicConsultorio) entidad;
                        CatalogoDto oDto = new CatalogoDto();
                        oDto.setClaveDes(ent.getCveConsultorio());
                        oDto.setDescripcion(ent.getCveConsultorio().concat(" ").concat(ent.getDesConsultorio()));
                        return oDto;
                    } else if (dto.getSimpleName().equals("SdConsultorio")) {
                        SicConsultorio ent = (SicConsultorio) entidad;
                        SdConsultorio sd = new SdConsultorio();
                        sd.setCveConsultorio(ent.getCveConsultorio());
                        sd.setDesConsultorio(ent.getDesConsultorio());
                        return sd;

                    }
            }
        } catch (Exception e) {
            throw new Exception("Error al transformar Entidad: " + entidad.getClass().getSimpleName() + " en Dto: "
                    + dto.getSimpleName(), e);
        }
        return null;
    }
}
