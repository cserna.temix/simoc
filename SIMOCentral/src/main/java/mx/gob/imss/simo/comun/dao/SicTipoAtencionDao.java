/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoAtencion;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicTipoAtencionDao {

	/**
	 * @return
	 */
	List<SicTipoAtencion> burcarTodos();

	/**
	 * @param cveTipoAtencion
	 * @return
	 */
	List<SicTipoAtencion> burcarPorId(Integer cveTipoAtencion);
}
