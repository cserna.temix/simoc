/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Frank
 * 
 */
@Entity("SIC_AGREGADO_MEDICO")
public class SicAgregadoMedico {

	@Id
	private ObjectId id;
	private Integer cveRegimen;
	private String desRegimen;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveRegimen
	 */
	public Integer getCveRegimen() {
		return cveRegimen;
	}

	/**
	 * @param cveRegimen
	 *            the cveRegimen to set
	 */
	public void setCveRegimen(Integer cveRegimen) {
		this.cveRegimen = cveRegimen;
	}

	/**
	 * @return the desRegimen
	 */
	public String getDesRegimen() {
		return desRegimen;
	}

	/**
	 * @param desRegimen
	 *            the desRegimen to set
	 */
	public void setDesRegimen(String desRegimen) {
		this.desRegimen = desRegimen;
	}

}
