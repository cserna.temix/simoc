/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoRegistroDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoRegistro;

/**
 * @author francisco.rodriguez
 *
 */
@Repository("sicTipoRegistroDao")
public class SicTipoRegistroDaoImpl implements SicTipoRegistroDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoRegistroDao#burcarTodos()
	 */
	@Override
	public List<SicTipoRegistro> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoRegistro> listTipoRegistro = new ArrayList<SicTipoRegistro>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoRegistro = dsEsp.createQuery(SicTipoRegistro.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoRegistro;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicTipoRegistroDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicTipoRegistro> burcarPorId(Integer cveTipoRegistro) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoRegistro> listTipoRegistro = new ArrayList<SicTipoRegistro>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoRegistro = dsEsp.createQuery(SicTipoRegistro.class)
					.field("cveTipoRegistro").equal(cveTipoRegistro).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listTipoRegistro;
	}
//
//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
