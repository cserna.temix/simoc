package mx.gob.imss.simo.consultaexterna.dto;

import java.util.Date;

import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;

public class DatosBusquedaDto {

	private Date fecha;
	private InputMask fechaPf;
	private InputText noSsInPf;
	private InputText matMedInPf;
	private InputText matMednommedPf;
	private InputText agMeCalidadPf;
	private InputText agMeSexoPf;
	private InputText agMeAnioPf;
	private SelectOneMenu especialidadPf;
	private InputText codCie10Pf;
	private SelectOneMenu ocasServPf;
	
	
	
	
	public DatosBusquedaDto() {
		super();
	}
	public InputMask getFechaPf() {
		return fechaPf;
	}
	public void setFechaPf(InputMask fechaPf) {
		this.fechaPf = fechaPf;
	}
	public InputText getNoSsInPf() {
		return noSsInPf;
	}
	public void setNoSsInPf(InputText noSsInPf) {
		this.noSsInPf = noSsInPf;
	}
	public InputText getMatMedInPf() {
		return matMedInPf;
	}
	public void setMatMedInPf(InputText matMedInPf) {
		this.matMedInPf = matMedInPf;
	}
	public InputText getMatMednommedPf() {
		return matMednommedPf;
	}
	public void setMatMednommedPf(InputText matMednommedPf) {
		this.matMednommedPf = matMednommedPf;
	}
	public InputText getAgMeCalidadPf() {
		return agMeCalidadPf;
	}
	public void setAgMeCalidadPf(InputText agMeCalidadPf) {
		this.agMeCalidadPf = agMeCalidadPf;
	}
	public InputText getAgMeSexoPf() {
		return agMeSexoPf;
	}
	public void setAgMeSexoPf(InputText agMeSexoPf) {
		this.agMeSexoPf = agMeSexoPf;
	}
	public InputText getAgMeAnioPf() {
		return agMeAnioPf;
	}
	public void setAgMeAnioPf(InputText agMeAnioPf) {
		this.agMeAnioPf = agMeAnioPf;
	}
	public SelectOneMenu getEspecialidadPf() {
		return especialidadPf;
	}
	public void setEspecialidadPf(SelectOneMenu especialidadPf) {
		this.especialidadPf = especialidadPf;
	}
	public InputText getCodCie10Pf() {
		return codCie10Pf;
	}
	public void setCodCie10Pf(InputText codCie10Pf) {
		this.codCie10Pf = codCie10Pf;
	}
	public SelectOneMenu getOcasServPf() {
		return ocasServPf;
	}
	public void setOcasServPf(SelectOneMenu ocasServPf) {
		this.ocasServPf = ocasServPf;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
}
