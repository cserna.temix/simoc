/**
 * 
 */
package mx.gob.imss.simo.comun.core;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Frank
 * 
 */
public class AssemblerDtoEntity {

	public static <D, S> Object convertirDtoToEntidad(Object dtoOrigen,
			Class<?> entidadDestino) throws Exception {
		return MapperDtoToEntity.conversion(dtoOrigen, entidadDestino);
	}

	public static <D, S> Object convertirEntidadToDto(Object entidadOrigen,
			Class<?> dtoDestino) throws Exception {
		return MapperEntityToDto.conversion(entidadOrigen, dtoDestino);
	}

	public static <D, S> List<?> convertirListEntidadToDto(
			List<?> listEntidadOrigen, Class<?> dtoDestino) throws Exception {
		List<Object> listaRes = new ArrayList<Object>();
		for (int i = 0; i < listEntidadOrigen.size(); i++) {
			listaRes.add(convertirEntidadToDto(listEntidadOrigen.get(i),
					dtoDestino));
		}
		return listaRes;
	}
}
