/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicPaseServicio;

/**
 * @author francisco.rrios
 * 
 */
public interface SicPaseServicioDao {

	/**
	 * @return
	 */
	List<SicPaseServicio> burcarTodos();

	/**
	 * @param cvePaseServicio
	 * @return
	 */
	List<SicPaseServicio> burcarPorId(Integer cvePaseServicio);
}
