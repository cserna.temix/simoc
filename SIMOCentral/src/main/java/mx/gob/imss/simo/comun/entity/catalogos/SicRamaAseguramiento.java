/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_RAMA_ASEGURAMIENTO")
public class SicRamaAseguramiento {

	@Id
	private ObjectId id;
	private Integer cveRamaAseguramiento;
	private String desRamaAseguramiento;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveRamaAseguramiento
	 */
	public Integer getCveRamaAseguramiento() {
		return cveRamaAseguramiento;
	}

	/**
	 * @param cveRamaAseguramiento
	 *            the cveRamaAseguramiento to set
	 */
	public void setCveRamaAseguramiento(Integer cveRamaAseguramiento) {
		this.cveRamaAseguramiento = cveRamaAseguramiento;
	}

	/**
	 * @return the desRamaAseguramiento
	 */
	public String getDesRamaAseguramiento() {
		return desRamaAseguramiento;
	}

	/**
	 * @param desRamaAseguramiento
	 *            the desRamaAseguramiento to set
	 */
	public void setDesRamaAseguramiento(String desRamaAseguramiento) {
		this.desRamaAseguramiento = desRamaAseguramiento;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
