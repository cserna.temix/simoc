/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicUnidadAdscripcion;

/**
 * @author francisco.rrios
 *
 */
public interface SicUnidadAdscripcionDao {

	/**
	 * @return
	 */
	List<SicUnidadAdscripcion> burcarTodos();

	/**
	 * @param cvePresupuestal
	 * @return
	 */
	List<SicUnidadAdscripcion> burcarPorId(String cvePresupuestal);
}
