package mx.gob.imss.simo.consultaexterna.dto;

import java.util.ArrayList;
import java.util.List;

public class DatosReporteParteI {
	
	String nombre = "";
	String matricula = "";
	String especialidad = "";
	String nombreMedico = "";
	int totalNumMedicos = 0;
	String totalNumHorasTrab = "0:0";
	int totalDiasTrab = 0;
	int totalConsultas = 0;
	int totalConsultasPrimeraVez = 0;
	int totalConsultasNoOtorgadas = 0;
	double promedioHorasAtencionesMedicas = 0.0;
	int totalNumeroVisitas = 0;
	int totalCitas = 0;
	int totalCitasCumplidas = 0;
	int totalPaseEspecialista = 0;
	int totalPaseEspecialistaOtraUnidad = 0;
	int totalAltasEspecialista = 0;
	int totalIncapacidadesExpedidas = 0;
	int totalIncapacidadesDias = 0;
	double promedioDiasAmparadosPorIncapacidad = 0.0;
	int totalRecetasExpedidas = 0;
	double porcentajeRecetasExpedidas = 0.0;
	
	List<String> medicos = new ArrayList<String>();
	List<String> especialidadMatricula = new ArrayList<String>();
	
		
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getNombreMedico() {
		return nombreMedico;
	}
	public void setNombreMedico(String nombreMedico) {
		this.nombreMedico = nombreMedico;
	}
	public int getTotalNumMedicos() {
		return totalNumMedicos;
	}
	public void setTotalNumMedicos(int totalNumMedicos) {
		this.totalNumMedicos = totalNumMedicos;
	}
	public String getTotalNumHorasTrab() {
		return totalNumHorasTrab;
	}
	public void setTotalNumHorasTrab(String totalNumHorasTrab) {
		this.totalNumHorasTrab = totalNumHorasTrab;
	}
	public int getTotalDiasTrab() {
		return totalDiasTrab;
	}
	public void setTotalDiasTrab(int totalDiasTrab) {
		this.totalDiasTrab = totalDiasTrab;
	}
	public int getTotalConsultas() {
		return totalConsultas;
	}
	public void setTotalConsultas(int totalConsultas) {
		this.totalConsultas = totalConsultas;
	}
	public int getTotalConsultasPrimeraVez() {
		return totalConsultasPrimeraVez;
	}
	public void setTotalConsultasPrimeraVez(int totalConsultasPrimeraVez) {
		this.totalConsultasPrimeraVez = totalConsultasPrimeraVez;
	}
	public int getTotalConsultasNoOtorgadas() {
		return totalConsultasNoOtorgadas;
	}
	public void setTotalConsultasNoOtorgadas(int totalConsultasNoOtorgadas) {
		this.totalConsultasNoOtorgadas = totalConsultasNoOtorgadas;
	}
	public double getPromedioHorasAtencionesMedicas() {
		return promedioHorasAtencionesMedicas;
	}
	public void setPromedioHorasAtencionesMedicas(double promedioHorasAtencionesMedicas) {
		this.promedioHorasAtencionesMedicas = promedioHorasAtencionesMedicas;
	}
	public int getTotalNumeroVisitas() {
		return totalNumeroVisitas;
	}
	public void setTotalNumeroVisitas(int totalNumeroVisitas) {
		this.totalNumeroVisitas = totalNumeroVisitas;
	}
	public int getTotalCitas() {
		return totalCitas;
	}
	public void setTotalCitas(int totalCitas) {
		this.totalCitas = totalCitas;
	}
	public int getTotalCitasCumplidas() {
		return totalCitasCumplidas;
	}
	public void setTotalCitasCumplidas(int totalCitasCumplidas) {
		this.totalCitasCumplidas = totalCitasCumplidas;
	}
	public int getTotalPaseEspecialista() {
		return totalPaseEspecialista;
	}
	public void setTotalPaseEspecialista(int totalPaseEspecialista) {
		this.totalPaseEspecialista = totalPaseEspecialista;
	}
	public int getTotalPaseEspecialistaOtraUnidad() {
		return totalPaseEspecialistaOtraUnidad;
	}
	public void setTotalPaseEspecialistaOtraUnidad(int totalPaseEspecialistaOtraUnidad) {
		this.totalPaseEspecialistaOtraUnidad = totalPaseEspecialistaOtraUnidad;
	}
	public int getTotalAltasEspecialista() {
		return totalAltasEspecialista;
	}
	public void setTotalAltasEspecialista(int totalAltasEspecialista) {
		this.totalAltasEspecialista = totalAltasEspecialista;
	}
	public int getTotalIncapacidadesExpedidas() {
		return totalIncapacidadesExpedidas;
	}
	public void setTotalIncapacidadesExpedidas(int totalIncapacidadesExpedidas) {
		this.totalIncapacidadesExpedidas = totalIncapacidadesExpedidas;
	}
	public int getTotalIncapacidadesDias() {
		return totalIncapacidadesDias;
	}
	public void setTotalIncapacidadesDias(int totalIncapacidadesDias) {
		this.totalIncapacidadesDias = totalIncapacidadesDias;
	}
	public double getPromedioDiasAmparadosPorIncapacidad() {
		return promedioDiasAmparadosPorIncapacidad;
	}
	public void setPromedioDiasAmparadosPorIncapacidad(double promedioDiasAmparadosPorIncapacidad) {
		this.promedioDiasAmparadosPorIncapacidad = promedioDiasAmparadosPorIncapacidad;
	}
	public int getTotalRecetasExpedidas() {
		return totalRecetasExpedidas;
	}
	public void setTotalRecetasExpedidas(int totalRecetasExpedidas) {
		this.totalRecetasExpedidas = totalRecetasExpedidas;
	}
	public double getPorcentajeRecetasExpedidas() {
		return porcentajeRecetasExpedidas;
	}
	public void setPorcentajeRecetasExpedidas(double porcentajeRecetasExpedidas) {
		this.porcentajeRecetasExpedidas = porcentajeRecetasExpedidas;
	}
	public List<String> getMedicos() {
		return medicos;
	}
	public void setMedicos(List<String> medicos) {
		this.medicos = medicos;
	}
	public List<String> getEspecialidadMatricula() {
		return especialidadMatricula;
	}
	public void setEspecialidadMatricula(List<String> especialidadMatricula) {
		this.especialidadMatricula = especialidadMatricula;
	}
	
	
	
}