/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoDiarrea;

/**
 * @author francisco.rrios
 * 
 */
public interface SicTipoDiarreaDao {

	/**
	 * @return
	 */
	List<SicTipoDiarrea> burcarTodos();

	/**
	 * @param cveTipoDiarrea
	 * @return
	 */
	List<SicTipoDiarrea> burcarPorId(String cveTipoDiarrea);
}
