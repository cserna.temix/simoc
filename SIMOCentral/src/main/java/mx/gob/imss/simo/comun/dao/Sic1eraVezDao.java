/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.Sic1eraVez;

/**
 * @author francisco.rrios
 * 
 */
public interface Sic1eraVezDao {

	/**
	 * @return
	 */
	List<Sic1eraVez> burcarTodos();

	/**
	 * @param cve1eraVez
	 * @return
	 */
	List<Sic1eraVez> burcarPorId(Integer cve1eraVez);

}
