package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_TIPO_ATENCION")
public class SicTipoAtencion {

	@Id
	private ObjectId id;
	private Integer cveTipoAtencion;
	private String desTipoAtencion;
	private String desLugarAtencion;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTipoAtencion
	 */
	public Integer getCveTipoAtencion() {
		return cveTipoAtencion;
	}

	/**
	 * @param cveTipoAtencion
	 *            the cveTipoAtencion to set
	 */
	public void setCveTipoAtencion(Integer cveTipoAtencion) {
		this.cveTipoAtencion = cveTipoAtencion;
	}

	/**
	 * @return the desTipoAtencion
	 */
	public String getDesTipoAtencion() {
		return desTipoAtencion;
	}

	/**
	 * @param desTipoAtencion
	 *            the desTipoAtencion to set
	 */
	public void setDesTipoAtencion(String desTipoAtencion) {
		this.desTipoAtencion = desTipoAtencion;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the desLugarAtencion
	 */
	public String getDesLugarAtencion() {
		return desLugarAtencion;
	}

	/**
	 * @param desLugarAtencion the desLugarAtencion to set
	 */
	public void setDesLugarAtencion(String desLugarAtencion) {
		this.desLugarAtencion = desLugarAtencion;
	}

}
