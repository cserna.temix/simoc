/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicMotivoAltaDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicMotivoAlta;

/**
 * @author francisco.rodriguez
 *
 */
public class SicMotivoAltaDaoImpl implements SicMotivoAltaDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicMotivoAltaDao#burcarTodos()
	 */
	@Override
	public List<SicMotivoAlta> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicMotivoAlta> listMotivoAlta = new ArrayList<SicMotivoAlta>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listMotivoAlta = dsEsp.createQuery(SicMotivoAlta.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listMotivoAlta;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicMotivoAltaDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicMotivoAlta> burcarPorId(Integer cveMotivoAlta) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicMotivoAlta> listMotivoAlta = new ArrayList<SicMotivoAlta>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listMotivoAlta = dsEsp.createQuery(SicMotivoAlta.class)
					.field("cveMotivoAlta").equal(cveMotivoAlta).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listMotivoAlta;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
