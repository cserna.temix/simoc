package mx.gob.imss.simo.comun.dto;

import java.util.Date;

import org.bson.types.ObjectId;

public class SicCie9DTO {

	private ObjectId id;
	private String cveCie9;
	private String desCie9;
	private Integer cveSexo;
	private Integer numEdadInferiorAnios;
	private Integer numEdadInferiorSemanas;
	private Integer numEdadSuperiorAnios;
	private Integer numEdadSuperiorSemanas;
	private Date fecBaja;

	private Integer numFiltro;
	private String desCie9M;
	private Integer numAniosVigencia;
	private String refUso;
	private String refActualizacion;
	private Integer indVigente;
	private Integer numProcedencia;
	private Integer indNivel1;
	private Integer indNivel2;
	private Integer indNivel3;
	private Integer numCapituloCie9;
	private Integer numGrupoCie9;
	private Integer numCodigoConsecutivo;
	private String refBilateralidad;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getCveCie9() {
		return cveCie9;
	}

	public void setCveCie9(String cveCie9) {
		this.cveCie9 = cveCie9;
	}

	public String getDesCie9() {
		return desCie9;
	}

	public void setDesCie9(String desCie9) {
		this.desCie9 = desCie9;
	}

	public Integer getCveSexo() {
		return cveSexo;
	}

	public void setCveSexo(Integer cveSexo) {
		this.cveSexo = cveSexo;
	}

	public Integer getNumEdadInferiorAnios() {
		return numEdadInferiorAnios;
	}

	public void setNumEdadInferiorAnios(Integer numEdadInferiorAnios) {
		this.numEdadInferiorAnios = numEdadInferiorAnios;
	}

	public Integer getNumEdadInferiorSemanas() {
		return numEdadInferiorSemanas;
	}

	public void setNumEdadInferiorSemanas(Integer numEdadInferiorSemanas) {
		this.numEdadInferiorSemanas = numEdadInferiorSemanas;
	}

	public Integer getNumEdadSuperiorAnios() {
		return numEdadSuperiorAnios;
	}

	public void setNumEdadSuperiorAnios(Integer numEdadSuperiorAnios) {
		this.numEdadSuperiorAnios = numEdadSuperiorAnios;
	}

	public Integer getNumEdadSuperiorSemanas() {
		return numEdadSuperiorSemanas;
	}

	public void setNumEdadSuperiorSemanas(Integer numEdadSuperiorSemanas) {
		this.numEdadSuperiorSemanas = numEdadSuperiorSemanas;
	}

	public Date getFecBaja() {
		return fecBaja;
	}

	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	public Integer getNumFiltro() {
		return numFiltro;
	}

	public void setNumFiltro(Integer numFiltro) {
		this.numFiltro = numFiltro;
	}

	public String getDesCie9M() {
		return desCie9M;
	}

	public void setDesCie9M(String desCie9M) {
		this.desCie9M = desCie9M;
	}

	public Integer getNumAniosVigencia() {
		return numAniosVigencia;
	}

	public void setNumAniosVigencia(Integer numAniosVigencia) {
		this.numAniosVigencia = numAniosVigencia;
	}

	public String getRefUso() {
		return refUso;
	}

	public void setRefUso(String refUso) {
		this.refUso = refUso;
	}

	public String getRefActualizacion() {
		return refActualizacion;
	}

	public void setRefActualizacion(String refActualizacion) {
		this.refActualizacion = refActualizacion;
	}

	public Integer getIndVigente() {
		return indVigente;
	}

	public void setIndVigente(Integer indVigente) {
		this.indVigente = indVigente;
	}

	public Integer getNumProcedencia() {
		return numProcedencia;
	}

	public void setNumProcedencia(Integer numProcedencia) {
		this.numProcedencia = numProcedencia;
	}

	public Integer getIndNivel1() {
		return indNivel1;
	}

	public void setIndNivel1(Integer indNivel1) {
		this.indNivel1 = indNivel1;
	}

	public Integer getIndNivel2() {
		return indNivel2;
	}

	public void setIndNivel2(Integer indNivel2) {
		this.indNivel2 = indNivel2;
	}

	public Integer getIndNivel3() {
		return indNivel3;
	}

	public void setIndNivel3(Integer indNivel3) {
		this.indNivel3 = indNivel3;
	}

	public Integer getNumCapituloCie9() {
		return numCapituloCie9;
	}

	public void setNumCapituloCie9(Integer numCapituloCie9) {
		this.numCapituloCie9 = numCapituloCie9;
	}

	public Integer getNumGrupoCie9() {
		return numGrupoCie9;
	}

	public void setNumGrupoCie9(Integer numGrupoCie9) {
		this.numGrupoCie9 = numGrupoCie9;
	}

	public Integer getNumCodigoConsecutivo() {
		return numCodigoConsecutivo;
	}

	public void setNumCodigoConsecutivo(Integer numCodigoConsecutivo) {
		this.numCodigoConsecutivo = numCodigoConsecutivo;
	}

	public String getRefBilateralidad() {
		return refBilateralidad;
	}

	public void setRefBilateralidad(String refBilateralidad) {
		this.refBilateralidad = refBilateralidad;
	}

}
