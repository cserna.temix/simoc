package mx.gob.imss.simo.consultaexterna.dao.impl;



import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;

import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import org.springframework.stereotype.Repository;
import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.entity.negocio.SitControlMedicoEncabezado;
import mx.gob.imss.simo.consultaexterna.dao.ControlMedicoEncabezadoDao;


	/**
	 * @author martin.jimenez
	 */
	@Repository("controlMedicoEncabezadoDao")
	public class ControlMedicoEncabezadoDaoImpl implements ControlMedicoEncabezadoDao {

	    Logger logger = Logger.getLogger(ControlMedicoEncabezadoDaoImpl.class);


	    // @Autowired
	    // private ConnectionMongo connectionMongo;

	    @Override
	    public void insertar(SitControlMedicoEncabezado controlMedico) {

	        // SimoDatastore simoDatastore = new SimoDatastore();
	        try {
	            Datastore ds = MongoManager.INSTANCE.getDatastore();
	            ds.save(controlMedico);
	        } catch (Exception e) {

	        	logger.error(e.getMessage(),e);	        	
	            e.printStackTrace();
	        }
	    }

	    @Override
	    public Boolean actualizar(SitControlMedicoEncabezado controlMedico) {

	        Boolean actualizado = Boolean.FALSE;
	        try {
	            Datastore ds = MongoManager.INSTANCE.getDatastore();

	            Query<SitControlMedicoEncabezado> updateQuery = ds.createQuery(SitControlMedicoEncabezado.class).field("cveMatricula")
	                    .equal(controlMedico.getCveMatricula());

	            UpdateOperations<SitControlMedicoEncabezado> ops = ds.createUpdateOperations(SitControlMedicoEncabezado.class)
	                    .set("numIntento", controlMedico.getNumIntento());

	            UpdateResults controlMedicoRes = ds.update(updateQuery, ops);
	            if (controlMedicoRes.getUpdatedExisting() || controlMedicoRes.getInsertedCount() > 0) {
	                actualizado = Boolean.TRUE;
	            }

	        } catch (Exception e) {
	        	
	        	logger.error(e.getMessage(),e);
	            e.printStackTrace();
	        }
	        return actualizado;
	    }

	    @Override
	    public SitControlMedicoEncabezado buscar(String matriculaMedico) {

	    	SitControlMedicoEncabezado controlMedico = null;
	        try {
	            Datastore ds = MongoManager.INSTANCE.getDatastore();

	            controlMedico=ds.createQuery(SitControlMedicoEncabezado.class)
	                    .filter("cveMatricula", matriculaMedico).get();
	        } catch (Exception e) {

	        	logger.error(e.getMessage(),e);
	            e.printStackTrace();	            
	        }
	        return controlMedico;
	    }

}
