/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_TIPO_INTERV_QUIRURGICA")
public class SicTipoIntervQuirurgica {

	@Id
	private ObjectId id;
	private Integer cveTipoIntervQuirurgica;
	private String desTipoIntervQuirurgica;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveTipoIntervQuirurgica
	 */
	public Integer getCveTipoIntervQuirurgica() {
		return cveTipoIntervQuirurgica;
	}

	/**
	 * @param cveTipoIntervQuirurgica
	 *            the cveTipoIntervQuirurgica to set
	 */
	public void setCveTipoIntervQuirurgica(Integer cveTipoIntervQuirurgica) {
		this.cveTipoIntervQuirurgica = cveTipoIntervQuirurgica;
	}

	/**
	 * @return the desTipoIntervQuirurgica
	 */
	public String getDesTipoIntervQuirurgica() {
		return desTipoIntervQuirurgica;
	}

	/**
	 * @param desTipoIntervQuirurgica
	 *            the desTipoIntervQuirurgica to set
	 */
	public void setDesTipoIntervQuirurgica(String desTipoIntervQuirurgica) {
		this.desTipoIntervQuirurgica = desTipoIntervQuirurgica;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
