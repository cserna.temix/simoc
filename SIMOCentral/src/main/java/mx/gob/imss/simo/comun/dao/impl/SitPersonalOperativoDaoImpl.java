package mx.gob.imss.simo.comun.dao.impl;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SitPersonalOperativoDao;
import mx.gob.imss.simo.comun.entity.negocio.SitPersonalOperativo;

@Repository("sitPersonalOperativoDao")
public class SitPersonalOperativoDaoImpl implements SitPersonalOperativoDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public SitPersonalOperativo loadByUsername(String username) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		SitPersonalOperativo usuario = null;
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			usuario = dsEsp.createQuery(SitPersonalOperativo.class)
					.field("refCuentaActiveD").equal(username).get();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return usuario;
	}

	@Override
	public void updateIntento(String activeDirectoryAccount, int intento) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SitPersonalOperativo> updateQuery = ds
					.createQuery(SitPersonalOperativo.class)
					.field("refCuentaActiveD").equal(activeDirectoryAccount);

			UpdateOperations<SitPersonalOperativo> ops = ds
					.createUpdateOperations(SitPersonalOperativo.class).set(
							"caIntentos", intento);

			ds.update(updateQuery, ops);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateBloqueo(String activeDirectoryAccount, int bloqueo) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SitPersonalOperativo> updateQuery = ds
					.createQuery(SitPersonalOperativo.class)
					.field("refCuentaActiveD").equal(activeDirectoryAccount);

			UpdateOperations<SitPersonalOperativo> ops = ds
					.createUpdateOperations(SitPersonalOperativo.class).set(
							"indBloqueado", bloqueo);

			ds.update(updateQuery, ops);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public int getIntento(String activeDirectoryAccount) {
		return loadByUsername(activeDirectoryAccount).getCaIntentos();
	}
	
	@Override
	public int getBloqueo(String activeDirectoryAccount) {
		return loadByUsername(activeDirectoryAccount).getIndBloqueado();
	}

//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}
//
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}

	@Override
	public void updateDesbloqueo(int indDesloqueo) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();

			UpdateOperations<SitPersonalOperativo> ops = ds
					.createUpdateOperations(SitPersonalOperativo.class).set(
							"indBloqueado", indDesloqueo);

			ds.update(ds.createQuery(SitPersonalOperativo.class), ops);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
