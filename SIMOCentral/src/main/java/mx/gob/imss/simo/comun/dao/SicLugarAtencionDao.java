/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicLugarAtencion;

/**
 * @author francisco.rrios
 *
 */
public interface SicLugarAtencionDao {
	/**
	 * @return
	 */
	List<SicLugarAtencion> burcarTodos();

	/**
	 * @param cveConsultorio
	 * @return
	 */
	List<SicLugarAtencion> burcarPorId(Integer cveConsultorio);
}
