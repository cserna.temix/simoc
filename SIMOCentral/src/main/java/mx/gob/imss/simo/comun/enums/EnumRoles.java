/**
 * 
 */
package mx.gob.imss.simo.comun.enums;

/**
 * @author janiel.mb
 *
 */
public enum EnumRoles {

	 //ROLE		
	/**
	 * Role: ADMINISTRADOR
	 * Subrole: ADMINISTRADOR CENTRAL
	 * Nivel de atencion:Nivel Central
	 * Subrole: ADMINISTRADOR DELEGACIONAL
	 * Nivel de atencion:"Nivel Delegación CIAE/UMAE (OIMAC) "
	 */
	ROLE_ADMIN_CENTRAL(1, "ROLE_ADMIN_CENTRAL"),  
	ROLE_ADMIN_DELEG(2, "ROLE_ADMIN_DELEG"), 
	
	//ROLE
	/**
	 * Role: ARIMAC
	 * Subrole:JEFE DE ARIMAC
	 * Nivel de atencion:2do. y 3ro
	 * Subrole: USUARIO DE CAPTURA	
	 * Nivel de atencion:2do. y 3ro.
	 * Subrole: USUARIO DE CONSULTA	
	 * Nivel de atencion:2do. y 3ro. 
	 */
	ROLE_ARIMAC_JEFE(3, "ROLE_ARIMAC_JEFE"),    
	ROLE_ARIMAC_CONSULTA(4, "ROLE_ARIMAC_CONSULTA"),
	ROLE_ARIMAC_CAPTURA(5, "ROLE_ARIMAC_CAPTURA"); 
	
	private Integer valor;
	private String descripcion;

	private EnumRoles(Integer valor, String descripcion) {
		this.valor = valor;
		this.descripcion=descripcion;
	}

	public Integer getValor() {
		return valor;
	}
	public String getDescripcion() {
		return descripcion;
	}
}