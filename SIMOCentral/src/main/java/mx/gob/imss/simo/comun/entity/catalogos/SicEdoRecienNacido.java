/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_EDO_RECIEN_NACIDO")
public class SicEdoRecienNacido {

	@Id
	private ObjectId id;
	private Integer cveEdoRecienNacido;
	private String desEdoRecienNacido;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveEdoRecienNacido
	 */
	public Integer getCveEdoRecienNacido() {
		return cveEdoRecienNacido;
	}

	/**
	 * @param cveEdoRecienNacido
	 *            the cveEdoRecienNacido to set
	 */
	public void setCveEdoRecienNacido(Integer cveEdoRecienNacido) {
		this.cveEdoRecienNacido = cveEdoRecienNacido;
	}

	/**
	 * @return the desEdoRecienNacido
	 */
	public String getDesEdoRecienNacido() {
		return desEdoRecienNacido;
	}

	/**
	 * @param desEdoRecienNacido
	 *            the desEdoRecienNacido to set
	 */
	public void setDesEdoRecienNacido(String desEdoRecienNacido) {
		this.desEdoRecienNacido = desEdoRecienNacido;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
