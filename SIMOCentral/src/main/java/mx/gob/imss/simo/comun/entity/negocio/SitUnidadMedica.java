package mx.gob.imss.simo.comun.entity.negocio;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Janiel
 * 
 */
@Entity("SIT_UNIDAD_MEDICA")
public class SitUnidadMedica {

	@Id
	private ObjectId id;
	private String cvePresupuestal;
	private String desUnidadMedica;
	private String cveDelegacion;
	private String desDelegacion;
	private String cveUnidadPresupuestal;
	private int indTitular;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cvePresupuestal
	 */
	public String getCvePresupuestal() {
		return cvePresupuestal;
	}

	/**
	 * @param cvePresupuestal
	 *            the cvePresupuestal to set
	 */
	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}

	/**
	 * @return the desUnidadMedica
	 */
	public String getDesUnidadMedica() {
		return desUnidadMedica;
	}

	/**
	 * @param desUnidadMedica
	 *            the desUnidadMedica to set
	 */
	public void setDesUnidadMedica(String desUnidadMedica) {
		this.desUnidadMedica = desUnidadMedica;
	}

	/**
	 * @return the cveDelegacion
	 */
	public String getCveDelegacion() {
		return cveDelegacion;
	}

	/**
	 * @param cveDelegacion
	 *            the cveDelegacion to set
	 */
	public void setCveDelegacion(String cveDelegacion) {
		this.cveDelegacion = cveDelegacion;
	}

	/**
	 * @return the desDelegacion
	 */
	public String getDesDelegacion() {
		return desDelegacion;
	}

	/**
	 * @param desDelegacion
	 *            the desDelegacion to set
	 */
	public void setDesDelegacion(String desDelegacion) {
		this.desDelegacion = desDelegacion;
	}

	public int getIndTitular() {
		return indTitular;
	}

	public void setIndTitular(int indTitular) {
		this.indTitular = indTitular;
	}

	public String getCveUnidadPresupuestal() {
		return cveUnidadPresupuestal;
	}

	public void setCveUnidadPresupuestal(String cveUnidadPresupuestal) {
		this.cveUnidadPresupuestal = cveUnidadPresupuestal;
	}

}