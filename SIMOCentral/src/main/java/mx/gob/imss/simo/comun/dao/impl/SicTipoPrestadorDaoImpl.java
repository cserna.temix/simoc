/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicTipoPrestadorDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicTipoPrestador;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicTipoPrestadorDao")
public class SicTipoPrestadorDaoImpl implements SicTipoPrestadorDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicTipoPrestador> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoPrestador> listTipoPrestador = new ArrayList<SicTipoPrestador>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoPrestador = dsEsp.createQuery(SicTipoPrestador.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		}
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listTipoPrestador;
	}

	@Override
	public List<SicTipoPrestador> burcarPorId(Integer cveTipoPrestador) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicTipoPrestador> listTipoPrestador = new ArrayList<SicTipoPrestador>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listTipoPrestador = dsEsp.createQuery(SicTipoPrestador.class)
					.field("cveTipoPrestador").equal(cveTipoPrestador)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listTipoPrestador;
	}

//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}
	

}
