package mx.gob.imss.simo.comun.enums;

public enum EnumColores {

	
	PRINCIPAL("back_principal"),
	ADMIN("back_administracion"),
	CATALOGOS("back_catalogos"),
	CONSULTA_EXTERNA("back_consultaExterna"),
	HOSPITALIZACION("back_hospitalizacion"),
	REPORTES("back_reportes"),
	ENLACES("back_enlaces"),
	UTILERIAS("back_utilerias"),
	INTERFACES("back_interfaces");
	
	
	private String valor;

	private EnumColores(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}

	
	
}
