/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicScRiesgo;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicScRiesgoDao {
	
	/**
	 * @return
	 */
	List<SicScRiesgo> burcarTodos();

	/**
	 * @param cveRiesgo
	 * @return
	 */
	List<SicScRiesgo> burcarPorId(Integer cveRiesgo);
}
