package mx.gob.imss.simo.comun.enums;

public enum EnumRamaAseguramiento {

	NO_DERECHOHABIENTE(0, "NO DERECHOHABIENTE"),
	ENFERMEDAD_GENERAL(1, "ENFERMEDAD GENERAL"),
	MATERNIDAD(2,"MATERNIDAD"),
	RIESGO_DE_TRABAJO_CONFIRMADO(3,"RIESGO DE TRABAJO CONFIRMADO"),
	PROBABLE_RIESGO_DE_TRABAJO(4,"PROBABLE RIESGO DE TRABAJO"),
	PENSIONADO(5,"PENSIONADO"),
	SEGURO_FAMILIAR(6,"SEGURO FAMILIAR"),
	OTROS(9,"OTROS");
	
	private int clave;
	private String valor;
	
	
	private EnumRamaAseguramiento(int clave, String valor) {
		this.clave = clave;
		this.valor = valor;
	}
	public int getClave() {
		return clave;
	}
	public void setClave(int clave) {
		this.clave = clave;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
