/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicServicioCuneroDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicServicioCunero;

/**
 * @author francisco.rodriguez
 *
 */
public class SicServicioCuneroDaoImpl implements SicServicioCuneroDao {

//	@Autowired 
//	private ConnectionMongo connectionMongo;
	
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicServicioCuneroDao#burcarTodos()
	 */
	@Override
	public List<SicServicioCunero> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicServicioCunero> listServicioCunero = new ArrayList<SicServicioCunero>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listServicioCunero = dsEsp.createQuery(SicServicioCunero.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listServicioCunero;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.SicServicioCuneroDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<SicServicioCunero> burcarPorId(Integer cveServicioCunero) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicServicioCunero> listServicioCunero = new ArrayList<SicServicioCunero>();
		try {
			
			Datastore dsEsp =  MongoManager.INSTANCE.getDatastore();

			listServicioCunero = dsEsp.createQuery(SicServicioCunero.class)
					.field("cveServicioCunero").equal(cveServicioCunero).asList();

		} catch (Exception e) {
			e.getMessage();
		}
		return listServicioCunero;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
