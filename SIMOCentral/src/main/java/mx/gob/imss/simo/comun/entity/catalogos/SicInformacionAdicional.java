/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_INFORMACION_ADICIONAL")
public class SicInformacionAdicional {

	@Id
	private ObjectId id;
	private String cveInformacionAdicional;
	private String desInformacionAdicional;
	private String refRangoCie10;
	private Integer indCveCie10;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveInformacionAdicional
	 */
	public String getCveInformacionAdicional() {
		return cveInformacionAdicional;
	}

	/**
	 * @param cveInformacionAdicional
	 *            the cveInformacionAdicional to set
	 */
	public void setCveInformacionAdicional(String cveInformacionAdicional) {
		this.cveInformacionAdicional = cveInformacionAdicional;
	}

	/**
	 * @return the desInformacionAdicional
	 */
	public String getDesInformacionAdicional() {
		return desInformacionAdicional;
	}

	/**
	 * @param desInformacionAdicional
	 *            the desInformacionAdicional to set
	 */
	public void setDesInformacionAdicional(String desInformacionAdicional) {
		this.desInformacionAdicional = desInformacionAdicional;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the refRangoCie10
	 */
	public String getRefRangoCie10() {
		return refRangoCie10;
	}

	/**
	 * @param refRangoCie10
	 *            the refRangoCie10 to set
	 */
	public void setRefRangoCie10(String refRangoCie10) {
		this.refRangoCie10 = refRangoCie10;
	}

	/**
	 * @return the indCveCie10
	 */
	public Integer getIndCveCie10() {
		return indCveCie10;
	}

	/**
	 * @param indCveCie10
	 *            the indCveCie10 to set
	 */
	public void setIndCveCie10(Integer indCveCie10) {
		this.indCveCie10 = indCveCie10;
	}

}
