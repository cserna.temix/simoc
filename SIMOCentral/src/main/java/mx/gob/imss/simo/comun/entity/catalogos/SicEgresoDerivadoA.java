/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_EGRESO_DERIVADO_A")
public class SicEgresoDerivadoA {

	@Id
	private ObjectId id;
	private Integer cveEgresoDerivadoA;
	private String desEgresoDerivadoA;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveEgresoDerivadoA
	 */
	public Integer getCveEgresoDerivadoA() {
		return cveEgresoDerivadoA;
	}

	/**
	 * @param cveEgresoDerivadoA
	 *            the cveEgresoDerivadoA to set
	 */
	public void setCveEgresoDerivadoA(Integer cveEgresoDerivadoA) {
		this.cveEgresoDerivadoA = cveEgresoDerivadoA;
	}

	/**
	 * @return the desEgresoDerivadoA
	 */
	public String getDesEgresoDerivadoA() {
		return desEgresoDerivadoA;
	}

	/**
	 * @param desEgresoDerivadoA
	 *            the desEgresoDerivadoA to set
	 */
	public void setDesEgresoDerivadoA(String desEgresoDerivadoA) {
		this.desEgresoDerivadoA = desEgresoDerivadoA;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
