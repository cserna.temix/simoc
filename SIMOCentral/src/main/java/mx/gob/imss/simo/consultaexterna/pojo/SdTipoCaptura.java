package mx.gob.imss.simo.consultaexterna.pojo;

public class SdTipoCaptura {
	
	private Integer cveTipoCaptura;
	private String desTipoCaptura;
	
	public SdTipoCaptura() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SdTipoCaptura(Integer cveTipoCaptura, String desTipoCaptura) {
		super();
		this.setCveTipoCaptura(cveTipoCaptura);
		this.setDesTipoCaptura(desTipoCaptura);
	}

	/**
	 * @return the cveTipoCaptura
	 */
	public Integer getCveTipoCaptura() {
		return cveTipoCaptura;
	}

	/**
	 * @param cveTipoCaptura the cveTipoCaptura to set
	 */
	public void setCveTipoCaptura(Integer cveTipoCaptura) {
		this.cveTipoCaptura = cveTipoCaptura;
	}

	/**
	 * @return the desTipoCaptura
	 */
	public String getDesTipoCaptura() {
		return desTipoCaptura;
	}

	/**
	 * @param desTipoCaptura the desTipoCaptura to set
	 */
	public void setDesTipoCaptura(String desTipoCaptura) {
		this.desTipoCaptura = desTipoCaptura;
	}
	
	

}
