/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicCie9Dao;
import mx.gob.imss.simo.comun.entity.catalogos.SicCie9;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicCie9Dao")
public class SicCie9DaoImpl implements SicCie9Dao {

	final static Logger logger = Logger.getLogger(SicCie9DaoImpl.class.getName());

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.comun.dao.SicCie9Dao#burcarTodos()
	 */
	@Override
	public List<SicCie9> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicCie9> listCie9MC = new ArrayList<SicCie9>();
		try {

			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listCie9MC = dsEsp.createQuery(SicCie9.class).asList();

		} catch (Exception e) {
			e.getMessage();
		}

		return listCie9MC;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.comun.dao.SicCie9Dao#burcarPorId(java.lang.String)
	 */
	@Override
	public SicCie9 buscarPorId(String cveProcedimiento) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		SicCie9 cie9Item = null;
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();
			Query<SicCie9> query = ds.createQuery(SicCie9.class);
			query.and(query.criteria("cveCie9").equal(cveProcedimiento), query.criteria("indVigente").equal(1));
			cie9Item = query.get();
		} catch (Exception e) {
			e.getMessage();
		}

		return cie9Item;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SicCie9Dao#burcarEdadSexo(java.lang.String,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<SicCie9> burcarEdadSexo(String cveProcedimiento, int edad, Integer sexo, boolean edadAnn) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicCie9> listCie9MC = new ArrayList<SicCie9>();
		try {

			Datastore ds = MongoManager.INSTANCE.getDatastore();

			Query<SicCie9> query = ds.createQuery(SicCie9.class);
			query.field("cveCie9").equal(cveProcedimiento);
			if (edadAnn) {
				query.filter("numEdadInferiorAnios <=", edad);
				query.filter("numEdadSuperiorAnios >=", edad);
			} else {
				query.filter("numEdadInferiorSemanas <=", edad);
				query.filter("numEdadSuperiorSemanas >=", edad);
			}
			query.or(query.criteria("indSexo").equal(sexo), query.criteria("indSexo").equal(0));
			listCie9MC = query.asList();

		} catch (Exception e) {
			logger.info("ERROR: " + e);
		}

		return listCie9MC;
	}
//
//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
