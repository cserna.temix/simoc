package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.MenuDao;
import mx.gob.imss.simo.comun.dto.MenuDto;

@Repository("menuDao")
public class MenuDaoImpl implements MenuDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<MenuDto> getMenu(String rol) {
//		SimoDatastore simoDatastore = new SimoDatastore();
		List<MenuDto> menu = new ArrayList<>();
		try {

			Datastore datastore = MongoManager.INSTANCE.getDatastore();

			menu = (datastore.createQuery(MenuDto.class).field("cveRol")
					.equal(rol).order("cveMenuAplicativo").asList());

		} catch (Exception e) {
			e.getMessage();
		}
		return menu;
	}

//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
