/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author francisco.rrios
 * 
 */
public class SdFormato {
	
	private String desTipoFormato;
	private Integer numConsecutivo;
	private List<SdPaciente> sdPaciente = new ArrayList<SdPaciente>();	
	private Integer indCitado;
	private Integer ind1EraVez;
	private List<SdTipoUrgencia> sdTipoUrgencia = new ArrayList<SdTipoUrgencia>();
	private List<SdPaseServicio> sdPaseServicio = new ArrayList<SdPaseServicio>();
	private Integer numRecetas;
	private Integer numDiasIncapacidad;
	private Integer indVisita;
	private List<SdDiagnosticos> sdDiagnosticos = new ArrayList<SdDiagnosticos>();
	private List<SdProcedimientos> sdProcedimientos = new ArrayList<SdProcedimientos>();
	private List<SdRiesgoTrabajo> sdRiesgoTrabajo = new ArrayList<SdRiesgoTrabajo>();
	private List<SdMetodoAnticonceptivo> sdMetodoAnticonceptivo = new ArrayList<SdMetodoAnticonceptivo>();
	private Date stpCaptura;	
	private SdRamaAseguramiento sdRamaAseguramiento;
	private Integer numEstadoFormato;
	private Date stpActualiza;
	
	public SdFormato() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the desTipoFormato
	 */
	public String getDesTipoFormato() {
		return desTipoFormato;
	}

	/**
	 * @param desTipoFormato
	 *            the desTipoFormato to set
	 */
	public void setDesTipoFormato(String desTipoFormato) {
		this.desTipoFormato = desTipoFormato;
	}

	/**
	 * @return the numConsecutivo
	 */
	public Integer getNumConsecutivo() {
		return numConsecutivo;
	}

	/**
	 * @param numConsecutivo
	 *            the numConsecutivo to set
	 */
	public void setNumConsecutivo(Integer numConsecutivo) {
		this.numConsecutivo = numConsecutivo;
	}

	/**
	 * @return the sdPaciente
	 */
	public List<SdPaciente> getSdPaciente() {
		return sdPaciente;
	}

	/**
	 * @param sdPaciente
	 *            the sdPaciente to set
	 */
	public void setSdPaciente(List<SdPaciente> sdPaciente) {
		this.sdPaciente = sdPaciente;
	}

	/**
	 * @return the indCitado
	 */
	public Integer getIndCitado() {
		return indCitado;
	}

	/**
	 * @param indCitado
	 *            the indCitado to set
	 */
	public void setIndCitado(Integer indCitado) {
		this.indCitado = indCitado;
	}

	/**
	 * @return the ind1EraVez
	 */
	public Integer getInd1EraVez() {
		return ind1EraVez;
	}

	/**
	 * @param ind1EraVez
	 *            the ind1EraVez to set
	 */
	public void setInd1EraVez(Integer ind1EraVez) {
		this.ind1EraVez = ind1EraVez;
	}

	/**
	 * @return the sdTipoUrgencia
	 */
	public List<SdTipoUrgencia> getSdTipoUrgencia() {
		return sdTipoUrgencia;
	}

	/**
	 * @param sdTipoUrgencia
	 *            the sdTipoUrgencia to set
	 */
	public void setSdTipoUrgencia(List<SdTipoUrgencia> sdTipoUrgencia) {
		this.sdTipoUrgencia = sdTipoUrgencia;
	}

	

	/**
	 * @return the sdPaseServicio
	 */
	public List<SdPaseServicio> getSdPaseServicio() {
		return sdPaseServicio;
	}

	/**
	 * @param sdPaseServicio the sdPaseServicio to set
	 */
	public void setSdPaseServicio(List<SdPaseServicio> sdPaseServicio) {
		this.sdPaseServicio = sdPaseServicio;
	}

	/**
	 * @return the numRecetas
	 */
	public Integer getNumRecetas() {
		return numRecetas;
	}

	/**
	 * @param numRecetas
	 *            the numRecetas to set
	 */
	public void setNumRecetas(Integer numRecetas) {
		this.numRecetas = numRecetas;
	}

	/**
	 * @return the numDiasIncapacidad
	 */
	public Integer getNumDiasIncapacidad() {
		return numDiasIncapacidad;
	}

	/**
	 * @param numDiasIncapacidad
	 *            the numDiasIncapacidad to set
	 */
	public void setNumDiasIncapacidad(Integer numDiasIncapacidad) {
		this.numDiasIncapacidad = numDiasIncapacidad;
	}

	/**
	 * @return the indVisita
	 */
	public Integer getIndVisita() {
		return indVisita;
	}

	/**
	 * @param indVisita
	 *            the indVisita to set
	 */
	public void setIndVisita(Integer indVisita) {
		this.indVisita = indVisita;
	}

	/**
	 * @return the sdDiagnosticos
	 */
	public List<SdDiagnosticos> getSdDiagnosticos() {
		return sdDiagnosticos;
	}

	/**
	 * @param sdDiagnosticos
	 *            the sdDiagnosticos to set
	 */
	public void setSdDiagnosticos(List<SdDiagnosticos> sdDiagnosticos) {
		this.sdDiagnosticos = sdDiagnosticos;
	}

	/**
	 * @return the sdProcedimientos
	 */
	public List<SdProcedimientos> getSdProcedimientos() {
		return sdProcedimientos;
	}

	/**
	 * @param sdProcedimientos
	 *            the sdProcedimientos to set
	 */
	public void setSdProcedimientos(List<SdProcedimientos> sdProcedimientos) {
		this.sdProcedimientos = sdProcedimientos;
	}

	/**
	 * @return the sdRiesgoTrabajo
	 */
	public List<SdRiesgoTrabajo> getSdRiesgoTrabajo() {
		return sdRiesgoTrabajo;
	}

	/**
	 * @param sdRiesgoTrabajo
	 *            the sdRiesgoTrabajo to set
	 */
	public void setSdRiesgoTrabajo(List<SdRiesgoTrabajo> sdRiesgoTrabajo) {
		this.sdRiesgoTrabajo = sdRiesgoTrabajo;
	}

	/**
	 * @return the SdMetodoAnticonceptivo
	 */
	public List<SdMetodoAnticonceptivo> getSdMetodoAnticonceptivo() {
		return sdMetodoAnticonceptivo;
	}

	/**
	 * @param SdMetodoAnticonceptivo
	 *            the SdMetodoAnticonceptivo to set
	 */
	public void setSdMetodoAnticonceptivo(
			List<SdMetodoAnticonceptivo> sdMetodoAnticonceptivo) {
		this.sdMetodoAnticonceptivo = sdMetodoAnticonceptivo;
	}

	/**
	 * @return the stpCaptura
	 */
	public Date getStpCaptura() {
		return stpCaptura;
	}

	/**
	 * @param stpCaptura
	 *            the stpCaptura to set
	 */
	public void setStpCaptura(Date stpCaptura) {
		this.stpCaptura = stpCaptura;
	}

	public SdRamaAseguramiento getSdRamaAseguramiento() {
		return sdRamaAseguramiento;
	}

	public void setSdRamaAseguramiento(SdRamaAseguramiento sdRamaAseguramiento) {
		this.sdRamaAseguramiento = sdRamaAseguramiento;
	}

	public Integer getNumEstadoFormato() {
		return numEstadoFormato;
	}

	public void setNumEstadoFormato(Integer numEstadoFormato) {
		this.numEstadoFormato = numEstadoFormato;
	}

	public Date getStpActualiza() {
		return stpActualiza;
	}

	public void setStpActualiza(Date stpActualiza) {
		this.stpActualiza = stpActualiza;
	}

}
