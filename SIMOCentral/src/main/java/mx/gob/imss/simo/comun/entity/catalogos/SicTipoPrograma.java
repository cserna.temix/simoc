/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rodriguez
 *
 */
@Entity("SIC_TIPO_PROGRAMA")
public class SicTipoPrograma {

	@Id
	private ObjectId id;
	private Integer cveTipoPrograma;
	private String desTipoPrograma;
	private Date fecBaja;
	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}
	/**
	 * @return the cveTipoPrograma
	 */
	public Integer getCveTipoPrograma() {
		return cveTipoPrograma;
	}
	/**
	 * @param cveTipoPrograma the cveTipoPrograma to set
	 */
	public void setCveTipoPrograma(Integer cveTipoPrograma) {
		this.cveTipoPrograma = cveTipoPrograma;
	}
	/**
	 * @return the desTipoPrograma
	 */
	public String getDesTipoPrograma() {
		return desTipoPrograma;
	}
	/**
	 * @param desTipoPrograma the desTipoPrograma to set
	 */
	public void setDesTipoPrograma(String desTipoPrograma) {
		this.desTipoPrograma = desTipoPrograma;
	}
	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}
	/**
	 * @param fecBaja the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}
	
	
}
