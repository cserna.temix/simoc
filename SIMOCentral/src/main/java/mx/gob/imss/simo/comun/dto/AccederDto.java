package mx.gob.imss.simo.comun.dto;

import java.util.Date;

public class AccederDto {

	private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private String unidadAdscripcion;
	private String cveUnidadMed;
	private String DescUnidadMed;
	private String fecNacimiento;
	private boolean vigencia;
	private Boolean encontrado;
	private String cveCurp;
	private Integer indVigente;
	private Integer indPacEncontrado;
	private Date stpUltConsultaVig;
	private String cveIdee;
	private String agregadoMedico;



	private Integer indice;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return apePaterno;
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return apeMaterno;
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getUnidadAdscripcion() {
		return unidadAdscripcion;
	}

	public void setUnidadAdscripcion(String unidadAdscripcion) {
		this.unidadAdscripcion = unidadAdscripcion;
	}

	public String getCveUnidadMed() {
		return cveUnidadMed;
	}

	public void setCveUnidadMed(String cveUnidadMed) {
		this.cveUnidadMed = cveUnidadMed;
	}

	public String getDescUnidadMed() {
		return DescUnidadMed;
	}

	public void setDescUnidadMed(String descUnidadMed) {
		DescUnidadMed = descUnidadMed;
	}

	public String getFecNacimiento() {
		return fecNacimiento;
	}

	public void setFecNacimiento(String fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

	public boolean isVigencia() {
		return vigencia;
	}

	public void setVigencia(boolean vigencia) {
		this.vigencia = vigencia;
	}

	public Boolean getEncontrado() {
		return encontrado;
	}

	public void setEncontrado(Boolean encontrado) {
		this.encontrado = encontrado;
	}


	public String getCveCurp() {
		return cveCurp;
	}

	public void setCveCurp(String cveCurp) {
		this.cveCurp = cveCurp;
	}

	public Integer getIndVigente() {
		return indVigente;
	}

	public void setIndVigente(Integer indVigente) {
		this.indVigente = indVigente;
	}

	public Integer getIndPacEncontrado() {
		return indPacEncontrado;
	}

	public void setIndPacEncontrado(Integer indPacEncontrado) {
		this.indPacEncontrado = indPacEncontrado;
	}

	public Date getStpUltConsultaVig() {
		return stpUltConsultaVig;
	}

	public void setStpUltConsultaVig(Date stpUltConsultaVig) {
		this.stpUltConsultaVig = stpUltConsultaVig;
	}

	public String getCveIdee() {
		return cveIdee;
	}

	public void setCveIdee(String cveIdee) {
		this.cveIdee = cveIdee;
	}

	public Integer getIndice() {
		return indice;
	}

	public void setIndice(Integer indice) {
		this.indice = indice;
	}
	public String getAgregadoMedico() {
		return agregadoMedico;
	}

	public void setAgregadoMedico(String agregadoMedico) {
		this.agregadoMedico = agregadoMedico;
	}
}
