package mx.gob.imss.simo.comun.dto;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("SIC_MENU_APLICATIVO")
public class MenuDto {

	@Id
	private ObjectId id;

	private String cveRol;

	private String cveMenuAplicativo;
	private String desMenuAplicativo;
	private String refAccion;

	private List<MenuItemDto> sdAdministracion;
	private List<MenuItemDto> sdCatalogos;
	private List<MenuItemDto> sdConsultaExterna;
	private List<MenuItemDto> sdHospitalizacion;
	private List<MenuItemDto> sdReportes;
	private List<MenuItemDto> sdEnlace;
	private List<MenuItemDto> sdUtilerias;
	private List<MenuItemDto> sdInterfaces;
	private List<MenuItemDto> sdConfiguracionUsuario;

	public String getCveMenuAplicativo() {
		return cveMenuAplicativo;
	}

	public void setCveMenuAplicativo(String cveMenuAplicativo) {
		this.cveMenuAplicativo = cveMenuAplicativo;
	}

	public String getDesMenuAplicativo() {
		return desMenuAplicativo;
	}

	public void setDesMenuAplicativo(String desMenuAplicativo) {
		this.desMenuAplicativo = desMenuAplicativo;
	}

	public String getRefAccion() {
		return refAccion;
	}

	public void setRefAccion(String refAccion) {
		this.refAccion = refAccion;
	}

	public List<MenuItemDto> getSdAdministracion() {
		return sdAdministracion;
	}

	public void setSdAdministracion(List<MenuItemDto> sdAdministracion) {
		this.sdAdministracion = sdAdministracion;
	}

	public List<MenuItemDto> getSdConsultaExterna() {
		return sdConsultaExterna;
	}

	public void setSdConsultaExterna(List<MenuItemDto> sdConsultaExterna) {
		this.sdConsultaExterna = sdConsultaExterna;
	}

	public List<MenuItemDto> getSdReportes() {
		return sdReportes;
	}

	public void setSdReportes(List<MenuItemDto> sdReportes) {
		this.sdReportes = sdReportes;
	}

	public List<MenuItemDto> getSdEnlace() {
		return sdEnlace;
	}

	public void setSdEnlace(List<MenuItemDto> sdEnlace) {
		this.sdEnlace = sdEnlace;
	}

	public List<MenuItemDto> getSdUtilerias() {
		return sdUtilerias;
	}

	public void setSdUtilerias(List<MenuItemDto> sdUtilerias) {
		this.sdUtilerias = sdUtilerias;
	}

	public List<MenuItemDto> getSdHospitalizacion() {
		return sdHospitalizacion;
	}

	public void setSdHospitalizacion(List<MenuItemDto> sdHospitalizacion) {
		this.sdHospitalizacion = sdHospitalizacion;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getCveRol() {
		return cveRol;
	}

	public void setCveRol(String cveRol) {
		this.cveRol = cveRol;
	}

	public List<MenuItemDto> getSdInterfaces() {
		return sdInterfaces;
	}

	public void setSdInterfaces(List<MenuItemDto> sdInterfaces) {
		this.sdInterfaces = sdInterfaces;
	}

	public List<MenuItemDto> getSdCatalogos() {
		return sdCatalogos;
	}

	public void setSdCatalogos(List<MenuItemDto> sdCatalogos) {
		this.sdCatalogos = sdCatalogos;
	}
	
	

	public List<MenuItemDto> getSdConfiguracionUsuario() {
		return sdConfiguracionUsuario;
	}

	public void setSdConfiguracionUsuario(List<MenuItemDto> sdConfiguracionUsuario) {
		this.sdConfiguracionUsuario = sdConfiguracionUsuario;
	}

	@Override
	public String toString() {
		return "MenuDto [id=" + id + ", cveRol=" + cveRol
				+ ", cveMenuAplicativo=" + cveMenuAplicativo
				+ ", desMenuAplicativo=" + desMenuAplicativo + ", refAccion="
				+ refAccion + ", sdAdministracion=" + sdAdministracion
				+ ", sdCatalogos=" + sdCatalogos + ", sdConsultaExterna="
				+ sdConsultaExterna + ", sdHospitalizacion="
				+ sdHospitalizacion + ", sdReportes=" + sdReportes
				+ ", sdEnlace=" + sdEnlace + ", sdUtilerias=" + sdUtilerias
				+ ", sdInterfaces=" + sdInterfaces + "]";
	}

}
