package mx.gob.imss.simo.comun.entity.negocio;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import lombok.Data;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;

@Data
@Entity("SIT_CONTROL_CAPTURA_CE")
public class SitControlCapturaCe {

	@Id
	private ObjectId id;
	private SdUnidadMedica sdUnidadMedica;
	private Integer cvePeriodo;
	private Integer indControl;
	private Date fecActualizaControl;
	
	
	
}
