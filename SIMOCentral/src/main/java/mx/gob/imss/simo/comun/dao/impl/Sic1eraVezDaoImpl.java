/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.Sic1eraVezDao;
import mx.gob.imss.simo.comun.entity.catalogos.Sic1eraVez;

/**
 * @author francisco.rrios
 *
 */
@Repository("sic1eraVezDao")
public class Sic1eraVezDaoImpl implements Sic1eraVezDao {
//	@Autowired
//	private ConnectionMongo connectionMongo;
	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.Sic1eraVezDao#burcarTodos()
	 */
	@Override
	public List<Sic1eraVez> burcarTodos() {
//		SimoDatastore simoDatastore = new SimoDatastore();
		List<Sic1eraVez> list1eraVez = new ArrayList<Sic1eraVez>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			list1eraVez = dsEsp.createQuery(Sic1eraVez.class)
					.asList();

		} catch (Exception e) {
			e.getMessage();
		} 
		return list1eraVez;
	}

	/* (non-Javadoc)
	 * @see mx.gob.imss.simo.comun.dao.Sic1eraVezDao#burcarPorId(java.lang.Integer)
	 */
	@Override
	public List<Sic1eraVez> burcarPorId(Integer cve1eraVez) {
//		SimoDatastore simoDatastore = new SimoDatastore();
		List<Sic1eraVez> list1eraVez = new ArrayList<Sic1eraVez>();
		try {
//			connectionMongo = new ConnectionMongo();
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			
			list1eraVez = dsEsp.createQuery(Sic1eraVez.class)
					.field("cve1eraVez").equal(cve1eraVez).asList();

		} catch (Exception e) {
			e.getMessage();
		} 

		return list1eraVez;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
