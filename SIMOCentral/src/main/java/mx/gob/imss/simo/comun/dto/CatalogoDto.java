/**
 * 
 */
package mx.gob.imss.simo.comun.dto;

/**
 * @author francisco.rrios
 *
 */
public class CatalogoDto {

	private Integer clave;
	private String claveDes;
    private String descripcion;
    private Integer numTipoServicio;
	
    
	/**
	 * @return the claveDes
	 */
	public String getClaveDes() {
		return claveDes;
	}
	/**
	 * @param claveDes the claveDes to set
	 */
	public void setClaveDes(String claveDes) {
		this.claveDes = claveDes;
	}
	/**
	 * @return the clave
	 */
	public Integer getClave() {
		return clave;
	}
	/**
	 * @param clave the clave to set
	 */
	public void setClave(Integer clave) {
		this.clave = clave;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the numTipoServicio
	 */
	public Integer getNumTipoServicio() {
		return numTipoServicio;
	}
	/**
	 * @param numTipoServicio the numTipoServicio to set
	 */
	public void setNumTipoServicio(Integer numTipoServicio) {
		this.numTipoServicio = numTipoServicio;
	}

    
}
