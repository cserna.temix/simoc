/**
 * 
 */
package mx.gob.imss.simo.consultaexterna.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.entity.negocio.SitBitacoraEnvio;
import mx.gob.imss.simo.consultaexterna.dao.BitacoraEnvioDao;

@Repository("bitacoraEnvioDao")
public class BitacoraEnvioDaoImpl implements BitacoraEnvioDao {

	private static final Logger logger = LoggerFactory.getLogger(BitacoraEnvioDao.class);

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public void insertar(SitBitacoraEnvio bitacoraEnvio) {
		// SimoDatastore simoDatastore = new SimoDatastore();

		try {
			Datastore ds = MongoManager.INSTANCE.getDatastore();
			
			ds.save(bitacoraEnvio);
		} catch (Exception e) {
			logger.error("{}", e.getMessage());
		}

	}

	public List<SitBitacoraEnvio> consultarBitacora() {
		List<SitBitacoraEnvio> listaBitacora = new ArrayList<SitBitacoraEnvio>();

		try {
			Datastore datastore = MongoManager.INSTANCE.getDatastore();

			listaBitacora = datastore.createQuery(SitBitacoraEnvio.class).filter("indEnvio", 1).asList();

			logger.info("Bitacora " + listaBitacora.size());
		} catch (Exception e) {
			logger.error(e.toString());
		}
		
		return listaBitacora;
	}
}
