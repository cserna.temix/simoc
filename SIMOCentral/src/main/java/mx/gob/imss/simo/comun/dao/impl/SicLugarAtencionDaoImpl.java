/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicLugarAtencionDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicLugarAtencion;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicLugarAtencionDao")
public class SicLugarAtencionDaoImpl implements SicLugarAtencionDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.gob.imss.simo.comun.dao.SdConsultorioDao#burcarTodos()
	 */
	@Override
	public List<SicLugarAtencion> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicLugarAtencion> listLugarAtencion = new ArrayList<SicLugarAtencion>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listLugarAtencion = dsEsp.createQuery(SicLugarAtencion.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listLugarAtencion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.gob.imss.simo.comun.dao.SdConsultorioDao#burcarPorId(java.lang.Integer
	 * )
	 */
	@Override
	public List<SicLugarAtencion> burcarPorId(Integer cveConsultorio) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicLugarAtencion> listLugarAtencion = new ArrayList<SicLugarAtencion>();
		try {
			
			Datastore ds = MongoManager.INSTANCE.getDatastore();

			listLugarAtencion = ds.createQuery(SicLugarAtencion.class)
					.field("cveLugarAtencion").equal(cveConsultorio).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listLugarAtencion;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
