/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicEdoRecienNacido;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicEdoRecienNacidoDao {

	/**
	 * @return
	 */
	List<SicEdoRecienNacido> burcarTodos();

	/**
	 * @param cveEdoRecienNacido
	 * @return
	 */
	List<SicEdoRecienNacido> burcarPorId(Integer cveEdoRecienNacido);
}
