/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicMetodoAnticonceptivo;

/**
 * @author francisco.rrios
 * 
 */
public interface SicMetodoAnticonceptivoDao {

	/**
	 * @return
	 */
	List<SicMetodoAnticonceptivo> burcarTodos();

	/**
	 * @param cveMetodoPpf
	 * @return
	 */
	List<SicMetodoAnticonceptivo> burcarPorId(Integer cveMetodoPpf);
	
	/**
	 * @param sexo
	 * @return
	 */
	List<SicMetodoAnticonceptivo> burcarXSexo(Integer sexo);
	
	List<SicMetodoAnticonceptivo> burcarXSexoXEdad(Integer sexo, Integer edad);
}
