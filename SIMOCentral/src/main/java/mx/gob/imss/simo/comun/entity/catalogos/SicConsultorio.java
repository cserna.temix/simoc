/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rodriguez
 *
 */
@Entity("SIC_CONSULTORIO")
public class SicConsultorio {

	@Id
	private ObjectId id;
	private String cveConsultorio;
	private String desConsultorio;
	private String cvePresupuestal;
	private Integer numDivision;
	private String cveEspecialidad;
	private String desEspecialidad;
	private Integer indComparteEspecialidad;
	private Integer estatus;
	private Integer numTipoServicio;
	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}
	/**
	 * @return the cveConsultorio
	 */
	public String getCveConsultorio() {
		return cveConsultorio;
	}
	/**
	 * @param cveConsultorio the cveConsultorio to set
	 */
	public void setCveConsultorio(String cveConsultorio) {
		this.cveConsultorio = cveConsultorio;
	}
	/**
	 * @return the desConsultorio
	 */
	public String getDesConsultorio() {
		return desConsultorio;
	}
	/**
	 * @param desConsultorio the desConsultorio to set
	 */
	public void setDesConsultorio(String desConsultorio) {
		this.desConsultorio = desConsultorio;
	}
	/**
	 * @return the cvePresupuestal
	 */
	public String getCvePresupuestal() {
		return cvePresupuestal;
	}
	/**
	 * @param cvePresupuestal the cvePresupuestal to set
	 */
	public void setCvePresupuestal(String cvePresupuestal) {
		this.cvePresupuestal = cvePresupuestal;
	}
	/**
	 * @return the numDivision
	 */
	public Integer getNumDivision() {
		return numDivision;
	}
	/**
	 * @param numDivision the numDivision to set
	 */
	public void setNumDivision(Integer numDivision) {
		this.numDivision = numDivision;
	}
	/**
	 * @return the cveEspecialidad
	 */
	public String getCveEspecialidad() {
		return cveEspecialidad;
	}
	/**
	 * @param cveEspecialidad the cveEspecialidad to set
	 */
	public void setCveEspecialidad(String cveEspecialidad) {
		this.cveEspecialidad = cveEspecialidad;
	}
	/**
	 * @return the desEspecialidad
	 */
	public String getDesEspecialidad() {
		return desEspecialidad;
	}
	/**
	 * @param desEspecialidad the desEspecialidad to set
	 */
	public void setDesEspecialidad(String desEspecialidad) {
		this.desEspecialidad = desEspecialidad;
	}
	/**
	 * @return the indComparteEspecialidad
	 */
	public Integer getIndComparteEspecialidad() {
		return indComparteEspecialidad;
	}
	/**
	 * @param indComparteEspecialidad the indComparteEspecialidad to set
	 */
	public void setIndComparteEspecialidad(Integer indComparteEspecialidad) {
		this.indComparteEspecialidad = indComparteEspecialidad;
	}
	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the numTipoServicio
	 */
	public Integer getNumTipoServicio() {
		return numTipoServicio;
	}
	/**
	 * @param numTipoServicio the numTipoServicio to set
	 */
	public void setNumTipoServicio(Integer numTipoServicio) {
		this.numTipoServicio = numTipoServicio;
	}
	
	
}
