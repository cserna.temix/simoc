package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "SIC_INFORMACION_ADICIONAL_VIH")
public class SicInformacionAdicionalVIH {

	@Id
	private org.bson.types.ObjectId _id;
	private Integer cveInfoAdicionalVIH;
	private String desInfoAdicionalVIH;
	private Date fecBaja;
	
	public SicInformacionAdicionalVIH() {
	}

	public org.bson.types.ObjectId get_id() {
		return _id;
	}

	public void set_id(org.bson.types.ObjectId _id) {
		this._id = _id;
	}

	public java.lang.Integer getCveInfoAdicionalVIH() {
		return cveInfoAdicionalVIH;
	}

	public void setCveInfoAdicionalVIH(java.lang.Integer cveInfoAdicionalVIH) {
		this.cveInfoAdicionalVIH = cveInfoAdicionalVIH;
	}

	public java.lang.String getDesInfoAdicionalVIH() {
		return desInfoAdicionalVIH;
	}

	public void setDesInfoAdicionalVIH(java.lang.String desInfoAdicionalVIH) {
		this.desInfoAdicionalVIH = desInfoAdicionalVIH;
	}

	public Date getFecBaja() {
		return fecBaja;
	}

	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}