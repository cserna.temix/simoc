/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_TIPO_DIARREA")
public class SicTipoDiarrea {

	@Id
	private ObjectId id;
	private String cveTipoDiarrea;
	private String desTipoDiarrea;
	private Date fecBaja;
	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}
	/**
	 * @return the cveTipoDiarrea
	 */
	public String getCveTipoDiarrea() {
		return cveTipoDiarrea;
	}
	/**
	 * @param cveTipoDiarrea the cveTipoDiarrea to set
	 */
	public void setCveTipoDiarrea(String cveTipoDiarrea) {
		this.cveTipoDiarrea = cveTipoDiarrea;
	}
	/**
	 * @return the desTipoDiarrea
	 */
	public String getDesTipoDiarrea() {
		return desTipoDiarrea;
	}
	/**
	 * @param desTipoDiarrea the desTipoDiarrea to set
	 */
	public void setDesTipoDiarrea(String desTipoDiarrea) {
		this.desTipoDiarrea = desTipoDiarrea;
	}
	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}
	/**
	 * @param fecBaja the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	

}
