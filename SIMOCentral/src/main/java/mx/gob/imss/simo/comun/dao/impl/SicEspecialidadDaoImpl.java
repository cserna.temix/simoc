package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicEspecialidadDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;

/**
 * @author francisco.rrios
 * 
 */
@Component("sicEspecialidadDao")
public class SicEspecialidadDaoImpl implements SicEspecialidadDao {

	static Logger logger = Logger.getLogger(SicEspecialidadDaoImpl.class);
	private static final String CVE_ESPECIALIDAD = "cveEspecialidad";
	private static final String NUM_CATEGORIA_STR = "numCategoria";
	private static final String DES_ESPECIALIDAD ="desEspecialidad";
	

	@Override
	public List<SicEspecialidad> burcarTodos() {
		List<SicEspecialidad> listEspecialidad = new ArrayList<>();
		try {			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			listEspecialidad = dsEsp.createQuery(SicEspecialidad.class)
					.asList();
		} catch (Exception e) {
			e.getMessage();
		} 
		return listEspecialidad;
	}
	@Override
	public List<SicEspecialidad> burcarPorId(String cveEspecialidad) {
		List<SicEspecialidad> listEspecialidad = new ArrayList<>();
		try {			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			listEspecialidad = dsEsp.createQuery(SicEspecialidad.class)
					.field(CVE_ESPECIALIDAD).equal(cveEspecialidad).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
		return listEspecialidad;
	}
	
	@Override
	public SicEspecialidad buscarPorId(String cveEspecialidad) {
		SicEspecialidad especialidad = null;
		try {
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			especialidad = dsEsp.createQuery(SicEspecialidad.class).field(CVE_ESPECIALIDAD).equal(cveEspecialidad)
					.get();
		} catch (Exception e) {
			e.getMessage();
		}
		return especialidad;
		
	}


	public List<String> burcarcvEspecialidad() {
		final int NUM_CATEGORIA = 6;
		final String TOCO = "A600";
		List<String> listEspecialidad = new ArrayList<>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			BasicDBObject cveEspecialidad = new BasicDBObject(CVE_ESPECIALIDAD, TOCO);
			BasicDBObject numCategoria = new BasicDBObject(NUM_CATEGORIA_STR, NUM_CATEGORIA);
			BasicDBList lista = new BasicDBList();
			lista.add(cveEspecialidad);
			lista.add(numCategoria);
			DBObject query = new BasicDBObject( "$or", lista );
			DBObject fields = new BasicDBObject(CVE_ESPECIALIDAD, 1);
			DBCursor cursor = dsEsp.getCollection(SicEspecialidad.class).find(query, fields);
			for (DBObject dbObject : cursor) {
				listEspecialidad.add((String) dbObject.get(CVE_ESPECIALIDAD));
			}

		} catch (Exception e) {
			logger.error("Error al obtener las especialidades: ", e);
		}
		return listEspecialidad;
	}
	
	public List<SicEspecialidad> burcarEspecialidadesPorCategoria(Integer numCategoria) {
		List<SicEspecialidad> listEspecialidad = new ArrayList<>();
		try {
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			listEspecialidad = dsEsp.createQuery(SicEspecialidad.class).order(CVE_ESPECIALIDAD).field(NUM_CATEGORIA_STR).equal(numCategoria).asList();

		} catch (Exception e) {
			logger.error("Error en burcarEspecialidadesPorCategoria de SicEspecialidadDaoImpl: ", e );
		} 
		return listEspecialidad;
	}
	
	
	public List<SicEspecialidad> burcarEspecialidadesPorCategoriaOrdenAlfabetico(Integer numCategoria) {
		List<SicEspecialidad> listEspecialidad = new ArrayList<>();
		try {
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();
			listEspecialidad = dsEsp.createQuery(SicEspecialidad.class).order(DES_ESPECIALIDAD).field(NUM_CATEGORIA_STR).equal(numCategoria).asList();

		} catch (Exception e) {
			logger.error("Error en burcarEspecialidadesPorCategoria de SicEspecialidadDaoImpl: ", e );
		} 
		return listEspecialidad;
	}
		

}
