/**
 * 
 */
package mx.gob.imss.simo.comun.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.comun.core.MongoManager;
import mx.gob.imss.simo.comun.dao.SicPaseServicioDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicPaseServicio;

/**
 * @author francisco.rrios
 * 
 */
@Repository("sicPaseServicioDao")
public class SicPaseServicioDaoImpl implements SicPaseServicioDao {

//	@Autowired
//	private ConnectionMongo connectionMongo;

	@Override
	public List<SicPaseServicio> burcarTodos() {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicPaseServicio> listPases = new ArrayList<SicPaseServicio>();
		try {
			
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listPases = dsEsp.createQuery(SicPaseServicio.class).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listPases;
	}

	@Override
	public List<SicPaseServicio> burcarPorId(
			Integer cvePaseServicio) {
		//SimoDatastore simoDatastore = new SimoDatastore();
		List<SicPaseServicio> listPases = new ArrayList<SicPaseServicio>();
		try {
		
			Datastore dsEsp = MongoManager.INSTANCE.getDatastore();

			listPases = dsEsp.createQuery(SicPaseServicio.class)
					.field("cvePaseServicio")
					.equal(cvePaseServicio).asList();

		} catch (Exception e) {
			e.getMessage();
		} 
//		finally {
//		connectionMongo.cerrarConexion();
//	}

		return listPases;
	}

//	/**
//	 * @return the connectionMongo
//	 */
//	public ConnectionMongo getConnectionMongo() {
//		return connectionMongo;
//	}
//
//	/**
//	 * @param connectionMongo
//	 *            the connectionMongo to set
//	 */
//	public void setConnectionMongo(ConnectionMongo connectionMongo) {
//		this.connectionMongo = connectionMongo;
//	}

}
