/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoAnestesia;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicTipoAnestesiaDao {
	/**
	 * @return
	 */
	List<SicTipoAnestesia> burcarTodos();

	/**
	 * @param cveTipoAnestecia
	 * @return
	 */
	List<SicTipoAnestesia> burcarPorId(Integer cveTipoAnestecia);
}
