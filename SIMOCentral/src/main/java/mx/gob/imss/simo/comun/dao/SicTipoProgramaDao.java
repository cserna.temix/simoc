/**
 * 
 */
package mx.gob.imss.simo.comun.dao;

import java.util.List;

import mx.gob.imss.simo.comun.entity.catalogos.SicTipoPrograma;

/**
 * @author francisco.rodriguez
 *
 */
public interface SicTipoProgramaDao {

	/**
	 * @return
	 */
	List<SicTipoPrograma> burcarTodos();

	/**
	 * @param cveTipoPrograma
	 * @return
	 */
	List<SicTipoPrograma> burcarPorId(Integer cveTipoPrograma);
}
