/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author francisco.rrios
 * 
 */
@Entity("SIC_CIE10_MORFOLOGIA")
public class SicCie10Morfologia {

	@Id
	private ObjectId id;
	private String cveCie10Morfologia;
	private String desCie10Morfologia;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveCie10Morfologia
	 */
	public String getCveCie10Morfologia() {
		return cveCie10Morfologia;
	}

	/**
	 * @param cveCie10Morfologia
	 *            the cveCie10Morfologia to set
	 */
	public void setCveCie10Morfologia(String cveCie10Morfologia) {
		this.cveCie10Morfologia = cveCie10Morfologia;
	}

	/**
	 * @return the desCie10Morfologia
	 */
	public String getDesCie10Morfologia() {
		return desCie10Morfologia;
	}

	/**
	 * @param desCie10Morfologia
	 *            the desCie10Morfologia to set
	 */
	public void setDesCie10Morfologia(String desCie10Morfologia) {
		this.desCie10Morfologia = desCie10Morfologia;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja
	 *            the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
