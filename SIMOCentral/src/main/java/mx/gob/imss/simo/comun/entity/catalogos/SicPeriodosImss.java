/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Frank
 * 
 */
@Entity("SIC_PERIODOS_IMSS")
public class SicPeriodosImss {

	@Id
	private ObjectId id;
	private Integer cvePeriodo;
	private Integer numAnioPeriodo;
	private String numMesPeriodo;
	private Date fecInicial;
	private Date fecFinal;
	private Date fecBaja;
	private Integer indVigente;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cvePeriodo
	 */
	public Integer getCvePeriodo() {
		return cvePeriodo;
	}

	/**
	 * @param cvePeriodo
	 *            the cvePeriodo to set
	 */
	public void setCvePeriodo(Integer cvePeriodo) {
		this.cvePeriodo = cvePeriodo;
	}

	/**
	 * @return the numAnioPeriodo
	 */
	public Integer getNumAnioPeriodo() {
		return numAnioPeriodo;
	}

	/**
	 * @param numAnioPeriodo
	 *            the numAnioPeriodo to set
	 */
	public void setNumAnioPeriodo(Integer numAnioPeriodo) {
		this.numAnioPeriodo = numAnioPeriodo;
	}

	/**
	 * @return the numMesPeriodo
	 */
	public String getNumMesPeriodo() {
		return numMesPeriodo;
	}

	/**
	 * @param numMesPeriodo
	 *            the numMesPeriodo to set
	 */
	public void setNumMesPeriodo(String numMesPeriodo) {
		this.numMesPeriodo = numMesPeriodo;
	}

	/**
	 * @return the fecInicial
	 */
	public Date getFecInicial() {
		return fecInicial;
	}

	/**
	 * @param fecInicial
	 *            the fecInicial to set
	 */
	public void setFecInicial(Date fecInicial) {
		this.fecInicial = fecInicial;
	}

	/**
	 * @return the fecFinal
	 */
	public Date getFecFinal() {
		return fecFinal;
	}

	/**
	 * @param fecFinal
	 *            the fecFinal to set
	 */
	public void setFecFinal(Date fecFinal) {
		this.fecFinal = fecFinal;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * @return the indVigente
	 */
	public Integer getIndVigente() {
		return indVigente;
	}

	/**
	 * @param indVigente the indVigente to set
	 */
	public void setIndVigente(Integer indVigente) {
		this.indVigente = indVigente;
	}
	
	

}
