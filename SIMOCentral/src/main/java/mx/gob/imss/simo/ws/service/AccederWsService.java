package mx.gob.imss.simo.ws.service;

import mx.gob.imss.simo.comun.dto.AccederDto;
import mx.gob.imss.simo.comun.dto.SiapDto;

import java.util.List;

public interface AccederWsService {

	public List<AccederDto> getInfo(String nss, String cpid, String aMedico);

	public SiapDto consultarSiap(String matricula, String delegacion);
}