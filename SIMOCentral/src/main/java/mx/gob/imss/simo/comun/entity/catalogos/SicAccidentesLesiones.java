/**
 * 
 */
package mx.gob.imss.simo.comun.entity.catalogos;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * @author Frank
 * 
 */
@Entity("SIC_ACCIDENTES_LESIONES")
public class SicAccidentesLesiones {

	@Id
	private ObjectId id;
	private Integer cveAccidenteLesion;
	private String desAccidenteLesion;
	private Date fecBaja;

	/**
	 * @return the id
	 */
	public ObjectId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(ObjectId id) {
		this.id = id;
	}

	/**
	 * @return the cveAccidenteLesion
	 */
	public Integer getCveAccidenteLesion() {
		return cveAccidenteLesion;
	}

	/**
	 * @param cveAccidenteLesion
	 *            the cveAccidenteLesion to set
	 */
	public void setCveAccidenteLesion(Integer cveAccidenteLesion) {
		this.cveAccidenteLesion = cveAccidenteLesion;
	}

	/**
	 * @return the desAccidenteLesion
	 */
	public String getDesAccidenteLesion() {
		return desAccidenteLesion;
	}

	/**
	 * @param desAccidenteLesion
	 *            the desAccidenteLesion to set
	 */
	public void setDesAccidenteLesion(String desAccidenteLesion) {
		this.desAccidenteLesion = desAccidenteLesion;
	}

	/**
	 * @return the fecBaja
	 */
	public Date getFecBaja() {
		return fecBaja;
	}

	/**
	 * @param fecBaja the fecBaja to set
	 */
	public void setFecBaja(Date fecBaja) {
		this.fecBaja = fecBaja;
	}

}
