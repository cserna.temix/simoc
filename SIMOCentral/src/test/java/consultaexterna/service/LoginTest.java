package consultaexterna.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.comun.dao.SicDelegacionImssDao;
import mx.gob.imss.simo.comun.dao.SitPersonalOperativoDao;
import mx.gob.imss.simo.comun.dto.DelegacionesUmaesDto;
import mx.gob.imss.simo.comun.entity.catalogos.SicDelegacionImss;
import mx.gob.imss.simo.comun.service.impl.CatalogosServiceImpl;
import mx.gob.imss.simo.comun.service.impl.SitPersonalOperativoServiceImpl;
import mx.gob.imss.simo.consultaexterna.service.impl.ConsultaExternaServiceImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConsultaExternaServiceImpl.class)
public class LoginTest {

    @InjectMocks
    private CatalogosServiceImpl catalogosServiceImpl;

    @Mock
    SicDelegacionImssDao sicDelegacionDao;

    @Mock
    SitPersonalOperativoDao personaOperativoDao;

    @InjectMocks
    SitPersonalOperativoServiceImpl personalOperativoService;

    final static Logger logger = LoggerFactory.getLogger(LoginTest.class);

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        catalogosServiceImpl = new CatalogosServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void buscarDelegaciones() {

        List<SicDelegacionImss> sicDelegaciones = new ArrayList<SicDelegacionImss>();
        SicDelegacionImss sicDelegacion = new SicDelegacionImss();
        sicDelegacion.setCveDelegacion("04");
        sicDelegacion.setDesDelegacion("Campeche");
        sicDelegacion.setId(new ObjectId());
        try {

            when(sicDelegacionDao.burcarTodos()).thenReturn(sicDelegaciones);
            List<DelegacionesUmaesDto> resulTest = catalogosServiceImpl.llenaComboDelegacion();
            assertNotNull(resulTest);
            logger.info("Test buscarDelegaciones");
            logger.info("Se obtuvo la lista de delegaciones correctamente");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void getBloqueo() {

        try {

            when(personaOperativoDao.getBloqueo(anyString())).thenReturn(0);
            int bloqueo = personalOperativoService.getBloqueo("teresita.miranda");
            assertThat(bloqueo, is(0));
            logger.info("Test getBloqueo");
            logger.info("Obtener indicador de bloque: {}", bloqueo);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void updateIntento() {

        String usuario = "teresita.miranda";
        try {
            doNothing().when(personaOperativoDao).updateIntento(anyString(), anyInt());
            personalOperativoService.updateIntento(usuario, 2);
            logger.info("Test updateIntento");
            logger.info("Actualizar intento indicador bloqueo exitoso");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
