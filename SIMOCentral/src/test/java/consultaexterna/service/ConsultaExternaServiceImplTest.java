package consultaexterna.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mongodb.morphia.query.UpdateResults;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.WriteResult;
import com.mongodb.client.result.UpdateResult;

import mx.gob.imss.simo.comun.dao.impl.SicEspecialidadDaoImpl;
import mx.gob.imss.simo.comun.entity.catalogos.SicEspecialidad;
import mx.gob.imss.simo.comun.entity.catalogos.SicPeriodosImss;
import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;
import mx.gob.imss.simo.comun.entity.negocio.SitControlCapturaCe;
import mx.gob.imss.simo.consultaexterna.dao.ConsultaExternaDao;
import mx.gob.imss.simo.consultaexterna.dao.impl.ConsultaExternaDaoImpl;
import mx.gob.imss.simo.consultaexterna.pojo.SdConsultorio;
import mx.gob.imss.simo.consultaexterna.pojo.SdDiagnosticoPrincipal;
import mx.gob.imss.simo.consultaexterna.pojo.SdDiagnosticos;
import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdEspecialidades;
import mx.gob.imss.simo.consultaexterna.pojo.SdFormato;
import mx.gob.imss.simo.consultaexterna.pojo.SdInformacionAdicional;
import mx.gob.imss.simo.consultaexterna.pojo.SdMedico;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoPrestador;
import mx.gob.imss.simo.consultaexterna.pojo.SdTurno;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;
import mx.gob.imss.simo.consultaexterna.service.impl.ConsultaExternaServiceImpl;
import mx.gob.imss.simo.model.ConsultaExternaDto;
import mx.gob.imss.simo.model.UsuarioDetalleDto;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConsultaExternaServiceImpl.class)
public class ConsultaExternaServiceImplTest {

	@InjectMocks
	ConsultaExternaServiceImpl consultaExternaService;

	@Mock
	ConsultaExternaDaoImpl consultaExternaDaoImpl;
	
	@Mock
	SicEspecialidadDaoImpl sicEspecialidadDao;
	
	@Mock
	ConsultaExternaDao consultaExternaDao;
	

	final static Logger logger = LoggerFactory.getLogger(ConsultaExternaServiceImplTest.class);

	SimpleDateFormat sdf = new SimpleDateFormat("YYYY/MM/DD");

	@BeforeClass
	public static void ini() {
		BasicConfigurator.configure();
	}

	@Before
	public void setup() {
		consultaExternaService = PowerMockito.spy(new ConsultaExternaServiceImpl());
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void insertarTest() {

		ConsultaExternaDto consultaExternaDtoOp = new ConsultaExternaDto();
		try {
			consultaExternaDtoOp.setFecha(sdf.parse("1991/03/15"));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		List<SdEncabezado> sdEncabezadoList = new ArrayList<SdEncabezado>();
		SdEncabezado sdEncabezado = new SdEncabezado();
		sdEncabezado.setNumHorasTrabajadas("3");
		sdEncabezado.setNumCitasNoCumplidas(5);
		sdEncabezado.setNumConsultasNoOtorgadas(2);
		sdEncabezadoList.add(sdEncabezado);

		List<SdEspecialidades> sdEspecialidadesList = new ArrayList<SdEspecialidades>();
		SdEspecialidades sdEspecialidades = new SdEspecialidades();
		sdEspecialidades.setNumTipoServicio(1);
		sdEspecialidades.setDesEspecialidad("Podología");
		sdEspecialidades.setCveEspecialidad("O12");
		sdEspecialidadesList.add(sdEspecialidades);

		List<SdTipoPrestador> SdTipoprestadorList = new ArrayList<SdTipoPrestador>();
		SdTipoPrestador sdTipoPrestador = new SdTipoPrestador();
		sdTipoPrestador.setCveTipoPrestador(2);
		sdTipoPrestador.setDesTipoPrestador("Or");
		SdTipoprestadorList.add(sdTipoPrestador);

		List<SdTurno> sdTurnoList = new ArrayList<SdTurno>();
		SdTurno sdTurno = new SdTurno();
		sdTurno.setCveTurno(4);
		sdTurno.setDesTurno("Vespertino");
		sdTurnoList.add(sdTurno);

		sdEncabezado.setSdEspecialidad(sdEspecialidadesList);
		sdEncabezado.setSdTipoPrestador(SdTipoprestadorList);
		sdEncabezado.setSdTurno(sdTurnoList);

		UsuarioDetalleDto usuarioDetalleDtoIn = new UsuarioDetalleDto();
		usuarioDetalleDtoIn.setCvePresupuestal("3");
		usuarioDetalleDtoIn.setAppPaterno("Vizcaya");
		usuarioDetalleDtoIn.setAppMaterno("Sanchez");
		usuarioDetalleDtoIn.setNombre("Jose");
		usuarioDetalleDtoIn.setCvePerfil(8);
		usuarioDetalleDtoIn.setPerfil("administrador");
		usuarioDetalleDtoIn.setCveDelegacionUmae("O23");
		usuarioDetalleDtoIn.setDesDelegacionAct("Venustiano Carranza");
		usuarioDetalleDtoIn.setCvePresupuestal("14253623");
		usuarioDetalleDtoIn.setDesUnidadMedicaAct("UMF Cartagena");

		try {
			// Cuando se llama al método privado cargaEncabezado que hace 3 llamados a los DAO mejor se regresa directamente la lista de encabezados
			PowerMockito.doReturn(sdEncabezadoList).when(consultaExternaService, "cargaEncabezado",
					any(SdEncabezado.class), any(ConsultaExternaDto.class), anyString());
			
			// Cuando se hace el insert en mongo, él asigna un id a la Entidad por lo que al usar mock es necesario asignar ese Id como parte de un Answer
			Mockito.doAnswer(new Answer<Void>() {
				public Void answer(InvocationOnMock invocation) {
					Object[] args = invocation.getArguments();
					SitConsultaExterna sce = (SitConsultaExterna) args[0];
					sce.setId(new ObjectId());
					return null;
				}
			}).when(consultaExternaDao).insertar(any(SitConsultaExterna.class));

			consultaExternaService.insertar(consultaExternaDtoOp, usuarioDetalleDtoIn);

			logger.info("Tipo prestador: {}", consultaExternaDtoOp.getTipoPrestador());
			logger.info("Descripcion Especialidad: {}", consultaExternaDtoOp.getDesEspecialidad());
			logger.info("Turno: {}", consultaExternaDtoOp.getDesTurno());
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void buscarPeriodoLiberacion(){
		String valida = "CORRECTO";
		try {
			when(consultaExternaService.buscarPeriodoLiberacion("PERIODO_LIBERACION")).thenReturn(valida);
			logger.info(valida);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void pruebaCambios(){
		List<ConsultaExternaDto> listResul = new ArrayList<ConsultaExternaDto>();
		ConsultaExternaDto consulExtDto = getConsultaExternaDto();
		UsuarioDetalleDto usuarioDetalleDto = new UsuarioDetalleDto();
		usuarioDetalleDto.setCvePresupuestal("010101012151");
		try {
			when(consultaExternaService.buscarAtenciones(consulExtDto, usuarioDetalleDto)).thenReturn(listResul);
			when(consultaExternaService.actualizaNombreMedico(consulExtDto)).thenReturn(true);
			when(consultaExternaService.buscarAtencionesParaActualiza(consulExtDto, usuarioDetalleDto)).thenReturn(listResul);
			logger.info("SIN ERRORES");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void actualizarTest() {

		ConsultaExternaDto consultaExternaDtoOn = getConsultaExternaDto();
		SitConsultaExterna sitConsultaExterna = getSitConsultaExterna();

		try {
			
			when(consultaExternaDao.buscar(any(Date.class), anyString(), anyString(), anyInt()))
					.thenReturn(sitConsultaExterna);
			when(consultaExternaDaoImpl.actualizar(any(SitConsultaExterna.class))).thenReturn(Boolean.TRUE);
			
			consultaExternaService.actualizar(consultaExternaDtoOn);
			verify(consultaExternaService, times(1)).actualizar(consultaExternaDtoOn);

			PowerMockito.verifyPrivate(consultaExternaService, times(1)).invoke("cargaActualizacion",
					consultaExternaDtoOn, sitConsultaExterna);

			logger.info("Matricula: {}", consultaExternaDtoOn.getMatriculaMedico());
			logger.info("Fecha: {}", consultaExternaDtoOn.getFecha());
			logger.info("Turno: {}", consultaExternaDtoOn.getTurno());
			logger.info("Consulta: {}", consultaExternaDtoOn.getNumConsulta());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private SitConsultaExterna getSitConsultaExterna() {
		SitConsultaExterna sitConsultaExterna = new SitConsultaExterna();
		ObjectId bjectId = new ObjectId();
		sitConsultaExterna.setId(bjectId);

		List<SdFormato> sdFormatoList = new ArrayList<SdFormato>();
		SdFormato sdFormato = new SdFormato();
		sdFormato.setDesTipoFormato("ehdhiede");
		sdFormato.setInd1EraVez(2);
		sdFormato.setIndCitado(1);
		sdFormato.setIndVisita(2);
		sdFormato.setNumConsecutivo(3);
		sdFormato.setNumDiasIncapacidad(4);
		sdFormato.setNumEstadoFormato(1);
		sdFormato.setNumRecetas(3);
		sdFormato.setStpActualiza(new Date());
		sdFormato.setStpCaptura(new Date());
		sdFormatoList.add(sdFormato);

		List<SdEncabezado> sdEncabezadoList = new ArrayList<SdEncabezado>();
		SdEncabezado sdEncabezado = new SdEncabezado();
		sdEncabezado.setFecAltaEncabezado(new Date());
		sdEncabezado.setNumCitasNoCumplidas(3);
		sdEncabezado.setNumConsultasNoOtorgadas(1);
		sdEncabezado.setNumHorasTrabajadas("4");
		sdEncabezadoList.add(sdEncabezado);

		List<SdDiagnosticos> sdDiagnosticosList = new ArrayList<SdDiagnosticos>();
		SdDiagnosticos sdDiagnosticos = new SdDiagnosticos();
		sdDiagnosticosList.add(sdDiagnosticos);

		List<SdInformacionAdicional> sdInformacionAdicionalList = new ArrayList<SdInformacionAdicional>();
		SdInformacionAdicional sdInformacionAdicional = new SdInformacionAdicional();
		sdInformacionAdicionalList.add(sdInformacionAdicional);
		sdInformacionAdicional.setCveInformacionAdicional("ijdsdjsl");
		sdInformacionAdicional.setCveTipoInformacionAdicional(2);
		sdInformacionAdicional.setDesInformacionAdicional("huihfdjf");

		List<SdDiagnosticoPrincipal> SdDiagnosticoPrincipalList = new ArrayList<SdDiagnosticoPrincipal>();
		SdDiagnosticoPrincipal sdDiagnosticoPrincipal = new SdDiagnosticoPrincipal();
		sdDiagnosticoPrincipal.setCveCie10("473");
		SdDiagnosticoPrincipalList.add(sdDiagnosticoPrincipal);
		sdDiagnosticoPrincipal.setDesCie10("8dhdjdhs");
		sdDiagnosticoPrincipal.setInd1EraVez(2);

		sitConsultaExterna.setSdEncabezado(sdEncabezadoList);
		return sitConsultaExterna;
	}

	private ConsultaExternaDto getConsultaExternaDto() {
		ConsultaExternaDto consultaExternaDtoOn = new ConsultaExternaDto();
		try {
			consultaExternaDtoOn.setFecha(sdf.parse("1991/03/15"));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		consultaExternaDtoOn.setMatriculaMedico("439435730");
		consultaExternaDtoOn.setEspecialidad("1800 Cardiología");
		consultaExternaDtoOn.setTurno("2");
		consultaExternaDtoOn.setNumConsulta(new Integer(5));
		return consultaExternaDtoOn;
	}

	
	@Test
	public void buscarAtenEdicionTest() {

		// 1.- Dto que espera el método buscar del dao consultaExternaDaoImpl
		ConsultaExternaDto consultaExternaDtoIn = new ConsultaExternaDto();

		try {
			consultaExternaDtoIn.setFecha(sdf.parse("1990/01/01"));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		consultaExternaDtoIn.setMatriculaMedico("34567890"); // poner matricula
		consultaExternaDtoIn.setEspecialidad("1100 Angiologia"); // poner especialidad
		consultaExternaDtoIn.setTurno("1");
		consultaExternaDtoIn.setNumConsulta(new Integer(7));

		// 2.- Objeto que regresa la consulta de la base de datos
		ObjectId bjectId = new ObjectId();

		SitConsultaExterna sitConsultaExterna = new SitConsultaExterna();
		sitConsultaExterna.setId(bjectId);
		List<SdEncabezado> sdEncabezadoList = new ArrayList<SdEncabezado>();
		SdEncabezado sdEncabezado = new SdEncabezado();
		List<SdMedico> sdMedicoList = new ArrayList<SdMedico>();
		SdMedico sdMedico = new SdMedico();
		List<SdConsultorio> sdConsultorioList = new ArrayList<SdConsultorio>();
		SdConsultorio sdConsultorio = new SdConsultorio();
		sdConsultorio.setCveConsultorio("120076768768");
		sdMedico.setCveMatricula("2374384389");
		sdEncabezadoList.add(sdEncabezado);
		sdMedicoList.add(sdMedico);
		sdConsultorioList.add(sdConsultorio);
		sdEncabezado.setFecAltaEncabezado(new Date());
		sdEncabezado.setSdMedico(sdMedicoList);
		sdEncabezado.setSdConsultorio(sdConsultorioList);
		sdEncabezado.setNumHorasTrabajadas("1");
		sdEncabezado.setNumCitasNoCumplidas(2);
		sdEncabezado.setNumConsultasNoOtorgadas(3);

		sitConsultaExterna.setSdEncabezado(sdEncabezadoList);

		List<SdEspecialidades> especialidades = new ArrayList<SdEspecialidades>();
		SdEspecialidades especialidad = new SdEspecialidades();
		especialidad.setCveEspecialidad("24238746");
		especialidad.setDesEspecialidad("pediatria");
		especialidad.setNumTipoServicio(1);
		especialidades.add(especialidad);
		sdEncabezado.setSdEspecialidad(especialidades);

		List<SdTipoPrestador> sdTipoPrestadorList = new ArrayList<SdTipoPrestador>();
		SdTipoPrestador sdTipoPrestador = new SdTipoPrestador();
		sdTipoPrestador.setCveTipoPrestador(3);
		sdTipoPrestador.setDesTipoPrestador("enfermero");
		sdTipoPrestadorList.add(sdTipoPrestador);
		sdEncabezado.setSdTipoPrestador(sdTipoPrestadorList);

		List<SdTurno> sdTurnoList = new ArrayList<SdTurno>();
		SdTurno sdTurno = new SdTurno();
		sdTurno.setCveTurno(7);
		sdTurno.setDesTurno("Vespertino");
		sdTurnoList.add(sdTurno);
		sdEncabezado.setSdTurno(sdTurnoList);

		// 3.- Objeto que esperamos como resultado de la ejecución del método
		// buscarAtenEdicion del servicio ConsultaExternaServiceImpl
		ConsultaExternaDto consultaExternaDtoOut = new ConsultaExternaDto();

		try {
			when(consultaExternaDao.buscar(consultaExternaDtoIn.getFecha(),
					consultaExternaDtoIn.getMatriculaMedico(), consultaExternaDtoIn.getEspecialidad(),
					Integer.parseInt(consultaExternaDtoIn.getTurno()))).thenReturn(sitConsultaExterna);

			consultaExternaDtoOut = consultaExternaService.buscarAtenEdicion(consultaExternaDtoIn);
			Assert.assertNotNull(consultaExternaDtoOut);
			Assert.assertEquals(consultaExternaDtoOut.getEspecialidadSelc(), "1");

			// 4.- Validad que los campos del consultaExternaDtoOut correspondan
			// con los que regresa la base de datos en sitConsultaExterna
			// Assert.assertEquals(consultaExternaDtoOut.getHorasTrabajadas(),
			// "2");

			logger.info("Clave Especialidad: {}", consultaExternaDtoOut.getCveEspecialidad());
			logger.info("Especialidad : {}", consultaExternaDtoOut.getDesEspecialidad());
			logger.info("Matrícula: {}", consultaExternaDtoOut.getMatriculaMedico());
			logger.info("EspecialidadSelc: {}", consultaExternaDtoOut.getEspecialidadSelc());
			logger.info("Turno : {}", consultaExternaDtoOut.getDesTurno());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	@Test
	public void buscarRepetidosTest() {

		Boolean valida = Boolean.TRUE;

		try {

			when(consultaExternaService.buscarAtencionRepetida("2288720062", "1M1972OR", "G473",
					"5ab2b2e9763d22226087a7ff")).thenReturn(valida);

			Boolean validarBusquedaResultado = consultaExternaService.buscarAtencionRepetida("2288720062", "1M1972OR",
					"G473", "5ab2b2e9763d22226087a7ff");

			Assert.assertTrue(validarBusquedaResultado);

			logger.info("Valida búsqueda repetida: {}", validarBusquedaResultado);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void buscarEspecialidadTest(){
		
		SicEspecialidad sicEspecialidadIn = new SicEspecialidad();
		
		sicEspecialidadIn.setCveEspecialidad("A600");
		sicEspecialidadIn.setCvePresupuestal("3435363738");
		sicEspecialidadIn.setDesEspecialidad("Tococirugia");
		
		List<SicEspecialidad> especialidadListSic = new  ArrayList<SicEspecialidad>();
		
	    SicEspecialidad especialidadsic = new SicEspecialidad();
	    especialidadsic.setCveEspecialidad("A600");
	    especialidadsic.setCvePresupuestal("3435363738");
	    especialidadsic.setDesEspecialidad("Cardiologia");
		
		
		
		try{
			when(consultaExternaService.buscarEspecialidad(anyString())).thenReturn(sicEspecialidadIn);
			when(sicEspecialidadDao.burcarPorId(anyString())).thenReturn(especialidadListSic);
			
			
			SicEspecialidad sicEspecialidadOn = consultaExternaService.buscarEspecialidad("A600");
			
			Assert.assertNotNull(sicEspecialidadIn);
			
			logger.info("Clave Especialidad: {}", sicEspecialidadIn.getCveEspecialidad());
			
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		}
	
	@Test
	public void actualizarControlCapturaCeTest(){
		
		ObjectId bjectId = new ObjectId();
		SicPeriodosImss sicPeriodoImss  = new SicPeriodosImss();
		
		sicPeriodoImss.setCvePeriodo(201801);
		sicPeriodoImss.setFecBaja(null);		
		sicPeriodoImss.setNumAnioPeriodo(2018);
		sicPeriodoImss.setNumMesPeriodo("01");
		sicPeriodoImss.setIndVigente(1);
		sicPeriodoImss.setId(bjectId);
		
		List<SdEncabezado> sdEncabezadoList = new ArrayList<SdEncabezado>();
		SdEncabezado sdEncabezado = new SdEncabezado();
		sdEncabezado.setFecAltaEncabezado(new Date());
		sdEncabezado.setNumCitasNoCumplidas(3);
		sdEncabezado.setNumConsultasNoOtorgadas(1);
		sdEncabezado.setNumHorasTrabajadas("4");
		sdEncabezadoList.add(sdEncabezado);
		
		SdUnidadMedica sdUnidadMedica = new SdUnidadMedica();
		sdUnidadMedica.setCvePresupuestal("030103022151");
		sdUnidadMedica.setDesUnidadMedica("HGZMF 1 La Paz");
		
		SitControlCapturaCe sitControlCapturaCe = new SitControlCapturaCe();
		sitControlCapturaCe.setId(bjectId);
		sitControlCapturaCe.setCvePeriodo(201801);	
		sitControlCapturaCe.setIndControl(1);
		sitControlCapturaCe.setSdUnidadMedica(sdUnidadMedica);
		
		try {
			sicPeriodoImss.setFecFinal(sdf.parse("26/01/2018"));
			sicPeriodoImss.setFecInicial(sdf.parse("25/12/2017"));
			sitControlCapturaCe.setFecActualizaControl(sdf.parse("21/08/2018"));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		List<SdUnidadMedica> sdUnidadMedicaList = new ArrayList<SdUnidadMedica>();
		sdUnidadMedicaList.add(sdUnidadMedica);
		
		SitConsultaExterna sitConsultaExterna = new SitConsultaExterna();
		sitConsultaExterna.setId(bjectId);
		sitConsultaExterna.setSdUnidadMedica(sdUnidadMedicaList);
		sitConsultaExterna.setSdEncabezado(sdEncabezadoList);
			
		try{
			when(consultaExternaDao.obtenerPeriodImssPorFecha(any(Date.class))).thenReturn(sicPeriodoImss);
			when(consultaExternaDao.actualizarControlCapturaCE(any(SitControlCapturaCe.class))).thenReturn(sitControlCapturaCe);
			consultaExternaService.actualizarControlCapturaCe(true, sitConsultaExterna);
			verify(consultaExternaService, times(1)).actualizarControlCapturaCe(true, sitConsultaExterna);
			
			Assert.assertEquals(sitControlCapturaCe.getSdUnidadMedica().getCvePresupuestal(), sdUnidadMedica.getCvePresupuestal());
			Assert.assertEquals(sitControlCapturaCe.getSdUnidadMedica().getDesUnidadMedica(), sdUnidadMedica.getDesUnidadMedica());
			
			logger.info("Clave Presupuestal: {}", sitControlCapturaCe.getSdUnidadMedica().getCvePresupuestal());
			logger.info("Unidad Medica: {}", sitControlCapturaCe.getSdUnidadMedica().getDesUnidadMedica());
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void validarConsecutivoConsultaTest(){
		ObjectId objectId = new ObjectId();
		
		SitConsultaExterna sitConsultaExterna = new SitConsultaExterna();
		sitConsultaExterna.setId(objectId);
		List<SdEncabezado> sdEncabezadoList = new ArrayList<SdEncabezado>();
		SdEncabezado sdEncabezado = new SdEncabezado();
		List<SdMedico> sdMedicoList = new ArrayList<SdMedico>();
		SdMedico sdMedico = new SdMedico();
		List<SdConsultorio> sdConsultorioList = new ArrayList<SdConsultorio>();
		SdConsultorio sdConsultorio = new SdConsultorio();
		sdConsultorio.setCveConsultorio("120076768768");
		sdMedico.setCveMatricula("2374384389");
		sdEncabezadoList.add(sdEncabezado);
		sdMedicoList.add(sdMedico);
		sdConsultorioList.add(sdConsultorio);
		sdEncabezado.setFecAltaEncabezado(new Date());
		sdEncabezado.setSdMedico(sdMedicoList);
		sdEncabezado.setSdConsultorio(sdConsultorioList);
		sdEncabezado.setNumHorasTrabajadas("1");
		sdEncabezado.setNumCitasNoCumplidas(2);
		sdEncabezado.setNumConsultasNoOtorgadas(3);
		
		List<SdFormato> sdFormatoList = new ArrayList<SdFormato>();
		SdFormato sdFormato = new SdFormato();
		sdFormato.setDesTipoFormato("4306");
		sdFormato.setInd1EraVez(2);
		sdFormato.setIndCitado(1);
		sdFormato.setIndVisita(2);
		sdFormato.setNumConsecutivo(3);
		sdFormato.setNumDiasIncapacidad(4);
		sdFormato.setNumEstadoFormato(1);
		sdFormato.setNumRecetas(3);
		sdFormato.setStpActualiza(new Date());
		sdFormato.setStpCaptura(new Date());
		sdFormatoList.add(sdFormato);
		
		sdEncabezado.setSdFormato(sdFormatoList);
		sitConsultaExterna.setSdEncabezado(sdEncabezadoList);
		
		when(consultaExternaDao.buscar(any(Date.class),anyString(), anyString(), any(Integer.class))).thenReturn(sitConsultaExterna);
		Integer resultTest = consultaExternaService.validarConsecutivoConsulta(getConsultaExternaDto());
		
		Assert.assertEquals(resultTest, new Integer(1));
		logger.info("Test validarConsecutivoConsulta");
		logger.info("Número de Atenciones: {}", resultTest);
		
	}
		
		
	

}
