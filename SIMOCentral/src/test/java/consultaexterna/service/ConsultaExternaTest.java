package consultaexterna.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.comun.entity.negocio.SitConsultaExterna;
import mx.gob.imss.simo.consultaexterna.dao.ConsultaExternaDao;
import mx.gob.imss.simo.consultaexterna.pojo.SdCapturista;
import mx.gob.imss.simo.consultaexterna.pojo.SdDelegacion;
import mx.gob.imss.simo.consultaexterna.pojo.SdEncabezado;
import mx.gob.imss.simo.consultaexterna.pojo.SdEspecialidades;
import mx.gob.imss.simo.consultaexterna.pojo.SdFormato;
import mx.gob.imss.simo.consultaexterna.pojo.SdPerfil;
import mx.gob.imss.simo.consultaexterna.pojo.SdTipoPrestador;
import mx.gob.imss.simo.consultaexterna.pojo.SdTurno;
import mx.gob.imss.simo.consultaexterna.pojo.SdUnidadMedica;
import mx.gob.imss.simo.consultaexterna.service.impl.ConsultaExternaServiceImpl;
import mx.gob.imss.simo.model.ConsultaExternaDto;
import mx.gob.imss.simo.model.UsuarioDetalleDto;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConsultaExternaServiceImpl.class)

public class ConsultaExternaTest {

    @InjectMocks
    private ConsultaExternaServiceImpl consultaExternaServiceImpl;

    @Mock
    private ConsultaExternaDao consultaExternaDao;

    final static Logger logger = LoggerFactory.getLogger(ConsultaExternaTest.class);

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        consultaExternaServiceImpl = new ConsultaExternaServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void buscarAtencionRepetidaTest() {

        try {

            when(consultaExternaDao.buscarAtencionRepetida(anyString(), anyString(), anyString(), anyString()))
                    .thenReturn(Boolean.TRUE);
            Boolean resultTest = consultaExternaServiceImpl.buscarAtencionRepetida("1207875424", "4M1963OR", "I200",
                    "5a983718b76fa12861e47dff");
            Assert.assertEquals(resultTest, Boolean.TRUE);

            logger.info("buscarAtencionRepetidaTest");
            logger.info("Existen atenciones repeticas: {}", resultTest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void buscarTest() throws ParseException {

        ConsultaExternaDto consulExtDto = new ConsultaExternaDto();
        Date fechaEncabezado = new SimpleDateFormat("dd/MM/yyyy").parse("10/09/2016");

        consulExtDto.setFecha(fechaEncabezado);
        consulExtDto.setMatriculaMedico("98352294");
        consulExtDto.setCveEspecialidad("5000");
        consulExtDto.setTurno("2");
        consulExtDto.setId("156");

        List<SdCapturista> sdCapturista = new ArrayList<SdCapturista>();
        List<SdPerfil> sdPerfil = new ArrayList<SdPerfil>();
        List<SdDelegacion> sdDelegacion = new ArrayList<SdDelegacion>();
        List<SdUnidadMedica> sdUnidadMedica = new ArrayList<SdUnidadMedica>();
        List<SdEncabezado> sdEncabezado = new ArrayList<SdEncabezado>();

        List<SdEspecialidades> especialidades = new ArrayList<SdEspecialidades>();
        SdEspecialidades especialidad = new SdEspecialidades();
        especialidad.setCveEspecialidad("24238746");
        especialidad.setDesEspecialidad("pediatria");
        especialidad.setNumTipoServicio(1);
        especialidades.add(especialidad);

        List<SdTipoPrestador> sdTipoPrestadorList = new ArrayList<SdTipoPrestador>();
        SdTipoPrestador sdTipoPrestador = new SdTipoPrestador();
        sdTipoPrestador.setCveTipoPrestador(3);
        sdTipoPrestador.setDesTipoPrestador("enfermero");
        sdTipoPrestadorList.add(sdTipoPrestador);

        List<SdTurno> sdTurnoList = new ArrayList<SdTurno>();
        SdTurno sdTurno = new SdTurno();
        sdTurno.setCveTurno(7);
        sdTurno.setDesTurno("Vespertino");
        sdTurnoList.add(sdTurno);

        SdEncabezado encabezado = new SdEncabezado();
        encabezado.setFecAltaEncabezado(fechaEncabezado);
        encabezado.setNumCitasNoCumplidas(2);
        encabezado.setNumConsultasNoOtorgadas(3);
        encabezado.setSdEspecialidad(especialidades);
        encabezado.setSdTipoPrestador(sdTipoPrestadorList);
        encabezado.setSdTurno(sdTurnoList);
        sdEncabezado.add(encabezado);

        SdPerfil perfil = new SdPerfil();
        perfil.setCvePerfil(234);
        perfil.setDesPerfil("Descripción");
        sdPerfil.add(perfil);

        SitConsultaExterna sitCE = new SitConsultaExterna();
        sitCE.setSdCapturista(sdCapturista);
        sitCE.setFecCaptura(fechaEncabezado);
        sitCE.setSdDelegacion(sdDelegacion);
        sitCE.setSdEncabezado(sdEncabezado);
        sitCE.setSdPerfil(sdPerfil);
        sitCE.setSdUnidadMedica(sdUnidadMedica);
        sitCE.setId(new ObjectId());

        try {
            when(consultaExternaDao.buscar(any(Date.class), anyString(), anyString(), anyInt())).thenReturn(sitCE);
            Integer resultTest = consultaExternaServiceImpl.buscar(consulExtDto, new UsuarioDetalleDto());
            Assert.assertEquals(resultTest, new Integer(1));

            logger.info("buscarTest");
            logger.info("Número de encabezados encontrados: {}", resultTest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void buscarCurrentService() throws ParseException {

        ConsultaExternaDto consulExtDto = new ConsultaExternaDto();
        Date fechaEncabezado = new SimpleDateFormat("dd/MM/yyyy").parse("10/09/2016");

        consulExtDto.setFecha(fechaEncabezado);
        consulExtDto.setMatriculaMedico("23412892");
        consulExtDto.setCveEspecialidad("1600");
        consulExtDto.setTurno("2");

        List<SdCapturista> sdCapturista = new ArrayList<SdCapturista>();
        List<SdPerfil> sdPerfil = new ArrayList<SdPerfil>();
        List<SdDelegacion> sdDelegacion = new ArrayList<SdDelegacion>();
        List<SdUnidadMedica> sdUnidadMedica = new ArrayList<SdUnidadMedica>();
        List<SdEncabezado> sdEncabezado = new ArrayList<SdEncabezado>();

        List<SdEspecialidades> especialidades = new ArrayList<SdEspecialidades>();
        SdEspecialidades especialidad = new SdEspecialidades();
        especialidad.setCveEspecialidad("1600");
        especialidad.setDesEspecialidad("Cirugía General");
        especialidad.setNumTipoServicio(1);
        especialidades.add(especialidad);

        List<SdTipoPrestador> sdTipoPrestadorList = new ArrayList<SdTipoPrestador>();
        SdTipoPrestador sdTipoPrestador = new SdTipoPrestador();
        sdTipoPrestador.setCveTipoPrestador(3);
        sdTipoPrestador.setDesTipoPrestador("Médico");
        sdTipoPrestadorList.add(sdTipoPrestador);

        List<SdTurno> sdTurnoList = new ArrayList<SdTurno>();
        SdTurno sdTurno = new SdTurno();
        sdTurno.setCveTurno(7);
        sdTurno.setDesTurno("Vespertino");
        sdTurnoList.add(sdTurno);

        List<SdFormato> sdFormato = new ArrayList<SdFormato>();
        SdFormato primerFormato = new SdFormato();
        primerFormato.setDesTipoFormato("4360");
        SdFormato segundoFormato = new SdFormato();
        segundoFormato.setDesTipoFormato("5310");
        sdFormato.add(primerFormato);
        sdFormato.add(segundoFormato);

        SdEncabezado encabezado = new SdEncabezado();
        encabezado.setFecAltaEncabezado(fechaEncabezado);
        encabezado.setNumCitasNoCumplidas(2);
        encabezado.setNumConsultasNoOtorgadas(3);
        encabezado.setSdEspecialidad(especialidades);
        encabezado.setSdTipoPrestador(sdTipoPrestadorList);
        encabezado.setSdTurno(sdTurnoList);
        encabezado.setSdFormato(sdFormato);
        sdEncabezado.add(encabezado);

        SdPerfil perfil = new SdPerfil();
        perfil.setCvePerfil(234);
        perfil.setDesPerfil("Descripción");
        sdPerfil.add(perfil);

        SitConsultaExterna sitCE = new SitConsultaExterna();
        sitCE.setSdCapturista(sdCapturista);
        sitCE.setFecCaptura(fechaEncabezado);
        sitCE.setSdDelegacion(sdDelegacion);
        sitCE.setSdEncabezado(sdEncabezado);
        sitCE.setSdPerfil(sdPerfil);
        sitCE.setSdUnidadMedica(sdUnidadMedica);
        sitCE.setId(new ObjectId());

        try {
            when(consultaExternaDao.buscar(any(Date.class), anyString(), anyString(), anyInt())).thenReturn(sitCE);
            Integer resultTest = consultaExternaServiceImpl.buscar(consulExtDto, new UsuarioDetalleDto());
            Assert.assertEquals(resultTest, new Integer(3));

            logger.info("buscarCurrentService");
            logger.info("Número de encabezados encontrados: {}", resultTest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
