package consultaexterna.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;

import mx.gob.imss.simo.consultaexterna.service.ConsultaExternaService;
import mx.gob.imss.simo.consultaexterna.service.impl.GenerarDocumentoVigEpiServiceImpl;

@Configuration
@ComponentScan(basePackages = {"consultaexterna.service","mx.gob.imss.simo.comun","mx.gob.imss.simo.consultaexterna.dao","mx.gob.imss.simo.consultaexterna.helper"}, 
	basePackageClasses = ConsultaExternaService.class,excludeFilters=@ComponentScan.Filter( type = FilterType.ASSIGNABLE_TYPE,value = GenerarDocumentoVigEpiServiceImpl.class))
public class AppConfig {
}