package consultaexterna.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import mx.gob.imss.simo.comun.dao.SicCie10VigilanciaEpiDao;
import mx.gob.imss.simo.comun.dao.SicEspecialidadDao;
import mx.gob.imss.simo.comun.entity.catalogos.SicSemanaVigilaciaEpi;
import mx.gob.imss.simo.comun.entity.negocio.SitBitacoraEnvio;
import mx.gob.imss.simo.consultaexterna.dao.BitacoraEnvioDao;
import mx.gob.imss.simo.consultaexterna.dao.CatalogoSemanaVigilanciaEpiDao;
import mx.gob.imss.simo.consultaexterna.dao.ConsultaExternaDao;
import mx.gob.imss.simo.consultaexterna.service.impl.GenerarDocumentoVigEpiServiceImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(GenerarDocumentoVigEpiServiceImpl.class)
public class CrearDocumentoTest {

    static Logger logger = Logger.getLogger(CrearDocumentoTest.class);

    @InjectMocks
    private GenerarDocumentoVigEpiServiceImpl generarDocumentoVigEpiServiceImpl;

    @Mock
    private SicCie10VigilanciaEpiDao sicCie10VigilanciaEpiDao;

    @Mock
    private BitacoraEnvioDao bitacoraEnvioDao;

    @Mock
    private ConsultaExternaDao consultaExternaDao;

    @Mock
    private SicEspecialidadDao sicEspecialidadDao;

    @Mock
    private CatalogoSemanaVigilanciaEpiDao catalogoSemanaVigilanciaEpiDao;

    @BeforeClass
    public static void ini() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {

        generarDocumentoVigEpiServiceImpl = new GenerarDocumentoVigEpiServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void buscarcatalogoVigEpi() {

        List<String> listCatalogoVigEpi = new ArrayList<String>();
        String vigEpiUno = "A000";
        String vigEpiDos = "A001";
        String vigEpiTres = "A009";
        listCatalogoVigEpi.add(vigEpiUno);
        listCatalogoVigEpi.add(vigEpiDos);
        listCatalogoVigEpi.add(vigEpiTres);

        try {
            when(sicCie10VigilanciaEpiDao.obtenerCatalogoVigEpi()).thenReturn(listCatalogoVigEpi);
            List<String> prueba = generarDocumentoVigEpiServiceImpl.obtenerCatalogoVigEpi();
            Assert.assertEquals(prueba, listCatalogoVigEpi);
            logger.info("Test buscarcatalogoVigEpi ");
            logger.info("Longitud de cátalogo: {} " + prueba.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void generarArchivosUnidadesMedicas() {

        List<SitBitacoraEnvio> listBitacoraEnvio = new ArrayList<SitBitacoraEnvio>();
        SitBitacoraEnvio sitBitacoraEnvio = new SitBitacoraEnvio();
        sitBitacoraEnvio.setCvePresupuestal("040138UA2151");
        sitBitacoraEnvio.setDesUnidadMedica("UMAA Campeche (Aut)");
        sitBitacoraEnvio.setNumRegistros(3);
        sitBitacoraEnvio.setIndEnvio(34);

        List<String> unidades = new ArrayList<String>();
        String unidadUno = "HGSMF 3 Chilpancingo";
        String unidadDos = "HGZ 23 Teziutlán";
        String unidadTres = "HGZ 83 Morelia";
        unidades.add(unidadUno);
        unidades.add(unidadDos);
        unidades.add(unidadTres);

        List<String> especialidades = new ArrayList<String>();
        String especialidadUno = "1600";
        String especialidadDos = "1000";
        especialidades.add(especialidadUno);
        especialidades.add(especialidadDos);

        List<SicSemanaVigilaciaEpi> sicSemanaVigilanciaEpi = new ArrayList<SicSemanaVigilaciaEpi>();
        SicSemanaVigilaciaEpi sicSemana = new SicSemanaVigilaciaEpi();
        sicSemana.setNumAnioPeriodo(2018);
        sicSemana.setNumMesPeriodo("01");
        sicSemana.setNumSemanaEPI(4);
        sicSemanaVigilanciaEpi.add(sicSemana);

        logger.info("Test generarArchivosUnidadesMedicas ");

        try {
            when(bitacoraEnvioDao.consultarBitacora()).thenReturn(listBitacoraEnvio);
            when(consultaExternaDao.buscarUnidadesMedicasConConsultasExternas(any(List.class), any(List.class)))
                    .thenReturn(unidades);
            when(consultaExternaDao.obtenerUrlConstante(anyString())).thenReturn("INACTIVO");
            when(sicEspecialidadDao.burcarcvEspecialidad()).thenReturn(especialidades);
            when(catalogoSemanaVigilanciaEpiDao.burcarCatalogoSemanaVigiEpi()).thenReturn(sicSemanaVigilanciaEpi);
            doNothing().when(consultaExternaDao).actualizaConstantes(anyString(), anyString());
            generarDocumentoVigEpiServiceImpl.generarArchivosUnidadesMedicas();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public GenerarDocumentoVigEpiServiceImpl getGenerarDocumentoVigEpiServiceImpl() {

        return generarDocumentoVigEpiServiceImpl;
    }

    public void setGenerarDocumentoVigEpiServiceImpl(
            GenerarDocumentoVigEpiServiceImpl generarDocumentoVigEpiServiceImpl) {

        this.generarDocumentoVigEpiServiceImpl = generarDocumentoVigEpiServiceImpl;
    }

}
