package mx.gob.imss.simo.comun.core;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractBaseBeanTest extends AbstractBaseBean {

    private static final long serialVersionUID = 1L;

    final static Logger logger = LoggerFactory.getLogger(AbstractBaseBeanTest.class);

    @Test
    public void testConvertirFecha() throws ParseException {

        Date fecha = new SimpleDateFormat("yyyy/MM/dd").parse("2019/01/25");
        String fechaAlterna = convertirFecha(fecha);
        assertEquals(fechaAlterna, "25/01/2019");

        logger.info("Fecha con cambio de formato: {}", fechaAlterna);
    }

    @Test
    public void testCalculoSemanasEdad() {

        String fecha = "20/02/2016";
        Integer semanas = calculoSemanasEdad(fecha);
        assertThat(semanas, greaterThan(0));
        logger.info("Calculo de Edad en Semanas: {}", semanas);

        fecha = "11/03/2015";
        semanas = calculoSemanasEdad(fecha);
        assertThat(semanas, greaterThan(0));
        logger.info("Calculo de Edad en Semanas: {}", semanas);

    }

    @Test
    public void testCalculoMesesEdad() {

        String fecha = "20/02/2015";
        Integer meses = calculoMesesEdad(fecha);
        assertThat(meses, greaterThan(0));
        logger.info("Calculo de Edad en Mes: {}", meses);
    }

    @Test
    public void testCalcularEdadAnnos() {

        String fechaNacimiento = "28/03/1985";
        Date fechaActual = new Date();
        Integer meses = calcularEdadAnnos(fechaNacimiento, fechaActual);
        assertThat(meses, greaterThan(0));
        logger.info("Calculo de Edad en Años: {}", meses);
    }

    @Test
    public void testCalcularEdad() {

        String fechaNacimiento = "28-03-1980";
        Integer meses = calcularEdad(fechaNacimiento);
        assertThat(meses, greaterThan(0));
        logger.info("Calculo de Edad: {}", meses);
    }

    @Test
    public void testIsNumero() {

        Boolean valida = isNumero("90");
        logger.info("Es número valido: {}", valida);
        assertTrue(valida);
    }

}
