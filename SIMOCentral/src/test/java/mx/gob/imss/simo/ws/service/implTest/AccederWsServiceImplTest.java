package mx.gob.imss.simo.ws.service.implTest;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.comun.dto.AccederDto;
import mx.gob.imss.simo.comun.dto.SiapDto;
import mx.gob.imss.simo.utils.ConectaWsSiap;
import mx.gob.imss.simo.ws.service.impl.AccederWsServiceImpl;


public class AccederWsServiceImplTest {
	
	@InjectMocks
	AccederWsServiceImpl consulta;
	
	@Mock
	AccederWsServiceImpl consulta1;
	
	@Mock
	ConectaWsSiap conectaWsSiap;
	
	final static Logger logger = LoggerFactory.getLogger(AccederWsServiceImplTest.class);
	
	private String matricula = "10863885";
	private String delegacion = "16";
	private final String  SIMO_OSB = "SIMO_OSB_PRUEBA";
	
	@BeforeClass
	public static void ini() {
		BasicConfigurator.configure();
	}
	
	@Before
	public void setup() {
		conectaWsSiap = new ConectaWsSiap(matricula, delegacion,SIMO_OSB);
		consulta = new AccederWsServiceImpl();
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void consultarSiapTest(){
		
		doNothing().when(conectaWsSiap).start();
		SiapDto resultTest = consulta.consultarSiap(matricula,delegacion);
		if(resultTest != null){
			logger.info("Consulta correcta a SIAP: {}", resultTest);
		}else{
			logger.info("No se recibio respuesta de SIAP: {}");
		}
		
	}
	
	@Test
	public void getInfo(){
		List<AccederDto> regreso = new ArrayList<AccederDto>();
		String nss = "2216996918";
		String cpid = "24";
		String aMedico = "1M1999OR";
		try {
			Mockito.when(consulta1.getInfo(nss, cpid, aMedico)).thenReturn(regreso);
			logger.info("PROCESO CORRECTO");
		} catch (Exception e) {	
			e.printStackTrace();
		}
	}

}
