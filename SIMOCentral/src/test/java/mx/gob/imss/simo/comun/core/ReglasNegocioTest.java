package mx.gob.imss.simo.comun.core;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReglasNegocioTest {

	@InjectMocks
	ReglasNegocio reglasNegocio;

	final static Logger logger = LoggerFactory.getLogger(ReglasNegocioTest.class);

	@BeforeClass
	public static void ini() {
		BasicConfigurator.configure();
	}

	@Before
	public void setup() {
	}
	  
	@Test
	public void validaSerParamedicosTest() {

		Boolean validaParamedico = Boolean.TRUE;
		Boolean validaSerParamedico = reglasNegocio.validaSerParamedicos("1");
		Assert.assertFalse(validaSerParamedico);
		logger.info("Ser paramédico: {}",validaSerParamedico );

	}
	
	@Test
	    public void validaHorasTrabDifDeGral(){
		Boolean validaHorasTrab = reglasNegocio.validaHorasTrabajadas(4, "04:30", "TURNO_NOCTURNO");
		
		Assert.assertTrue(validaHorasTrab);
		
		logger.info("Horas consulta: {}",validaHorasTrab );
	}
	  
    @Test
    public void validaHorasTrabIgualOnce(){
    	Boolean validaHorasTrab = reglasNegocio.validaHorasTrabajadas(4, "11:10", "TURNO_MATUTINO");
    	
    	Assert.assertFalse(validaHorasTrab);
    	logger.info("Horas consulta: {}",validaHorasTrab );
    }
	
	@Test
	public void validaHorasTrabNumtipoServicio() {
		Boolean validaHorasTrab = reglasNegocio.validaHorasTrabajadas(5, "05:31", "TURNO_MATUTINO");
		Assert.assertTrue(validaHorasTrab);
		
		logger.info("Horas consulta: {}",validaHorasTrab );
	}
	  
	@Test
	public void validaHorasTrabIgualSeis() {
		Boolean validaHorasTrab = reglasNegocio.validaHorasTrabajadas(2, "06:31", "TURNO_NOCTURNO");
		Assert.assertFalse(validaHorasTrab);
		
		logger.info("Horas consulta: {}",validaHorasTrab );
	}
	
	@Test
	public void validaConsulVsCitasTest() {
		
		Boolean validaNumConsulCitas  = Boolean.TRUE;
		
		Boolean validaConsulVsCitas = reglasNegocio.validaConsulVsCitas(5, 7);
		
		Assert.assertFalse(validaConsulVsCitas);
		
		logger.info("Consultas y citas: {}",validaConsulVsCitas );

	}
	
	@Test
	public void validaEdadUrgenciasTocoTest(){
		
		Boolean  validaEdadToco = Boolean.FALSE;
		
		Boolean validaEdadUrgenciasToco = reglasNegocio.validaEdadUrgenciasToco("Tococirugia", 17, 50, 12);
		
		Assert.assertTrue(validaEdadUrgenciasToco);
		
		logger.info("Edad urgencias Toco: {}", validaEdadUrgenciasToco);
	}

	
	
	

}
