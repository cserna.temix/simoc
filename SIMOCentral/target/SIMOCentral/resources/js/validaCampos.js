/*
 * Provee funciones para el control de los caracteres que se ingresan.
 * 
 * letrasNumeros: función que solamente permite el ingreso de letras, números y el Tab por parte del usuario.
 * 
 * soloNumeros: función que permite el ingreso solamente de numeros y el Tab.
 * 
 * soloLetras: función que permite el ingreso de letras y el Tab.
 * 
 */	
	

	letrasNumeros = function() {
		$(this).keypress( function(e) {
//			alert("Letras Numeros");
					// Letras con tildes (mayúsculas y minúculas)
						if (e.which == 193 || e.which == 201 || e.which == 205
								|| e.which == 211 || e.which == 218
								|| e.which == 225 || e.which == 233
								|| e.which == 237 || e.which == 243
								|| e.which == 250 || e.which == 241
								|| e.which == 209) {
							return true;
						} else {
							// Permite letras [a-z][A-Z] con sus respectivos tíldes, 
							// numeros[0-9] y la enie
							if (e.which == 8
									|| (e.which >= 97 && e.which <= 122)
									|| (e.which >= 65 && e.which <= 90)
									|| (e.which >= 48 && e.which <= 57)
									|| e.which == 164 || e.which == 165) {
								return true;
							} else {
								return false;
							}
						}
					});
	};
	
	soloNumeros = function() {
	console.log("solo numeros");
		$(this).keypress( function(e) {
			// Permite numeros[0-9]
				if (e.which >= 48 && e.which <= 57
						|| e.which == 9 || e.keyCode == 37 || e.keyCode == 39) {
					return true;
				} else {
					return false;
				}
			});
	};
	
	soloLetras = function() {
//		alert("solo letras");
		$(this).keypress( function(e) {
					// Letras con tildes (mayúsculas y minúculas)
						if (e.which == 193 || e.which == 201 || e.which == 205
								|| e.which == 211 || e.which == 218
								|| e.which == 225 || e.which == 233
								|| e.which == 237 || e.which == 243
								|| e.which == 250 || e.which == 241
								|| e.which == 209) {
							return true;
						} else {
							// Permite letras [a-z][A-Z] con sus respectivos tíldes 
							// y la enie
							if (e.which == 8
									|| (e.which >= 97 && e.which <= 122)
									|| (e.which >= 65 && e.which <= 90)
									|| e.which == 164 || e.which == 165) {
								return true;
							} else {
								return false;
							}
						}
					});
	};
