PrimeFaces.locales['es'] = {
				    closeText: 'Cerrar',
				    prevText: 'Anterior',
				    nextText: 'Siguiente',
				    monthNames: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
				    dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
				    dayNamesShort: ['Dom','Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
				    dayNamesMin: ['D','L','M','X','J','V','S'],
				    weekHeader: 'Semana',
				    firstDay: 1,
				    isRTL: false,
				    showMonthAfterYear: false,
				    yearSuffix: '',
				    timeOnlyTitle: 'Sólo hora',
				    timeText: 'Tiempo',
				    hourText: 'Hora',
				    minuteText: 'Minuto',
				    secondText: 'Segundo',
				    currentText: 'Fecha actual',
				    ampm: false,
				    month: 'Mes',
				    week: 'Semana',
				    day: 'Día',
				    allDayText : 'Todo el día'
				};

/*
--Funcion general, enter, tabulador change input--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ 
*funcionStr= Nombre de la función (en este caso id del remoteCommand) que queramos ejecutar
*/
function valkeyET(id,funcionStr){
	
	$(id).on('keydown change',function( event){					
		 if (event.shiftKey && event.which ==9) {
			 //event.shiftKey &amp;&amp; event.which ==9
			  // Shift-tab pressed
			 console.log("entra &&");
		 }else{
			  if (event.which ==13 || event.which ==9 || event.which ==null) {
				  var func=window[funcionStr];
				  console.log("-> simoScript.js id:"+id+"    funcionStr:"+funcionStr);
				  func();	
			  }
		 }
	});
}
/*
--Maximo autosalto input--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ 
*sig_id=id del siguiente componente (algunos componente como los selectone menu requieren _input) 
formCexBusqEd:idRegimen_focus sin # y sin \\
al que enviara el foco, para q se marque un evento change y ejecute 
en la otra funcion de valkeyET
*max= valor con el cual se ejecutará la acción
*/
function valKeyMax(id,sig_id,max){
	$(id).keyup(function(){
		if (this.value.length == max){
			document.getElementById(sig_id).focus();
		}
	});
}
/*SELECTONE MENU
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\
*idsf= mismo id del componente con todo y form formCexBusqEd:idEspecialidadAc quitar # y \\
*funcionStr= Nombre de la función (en este caso id del remoteCommand) que queramos ejecutar
*/
function selcetOneMenu_valKeyET(id,idsf,funcionStr){
	
	$(id+"_focus").on('keydown change',function( event){
		var elem = document.getElementById(id+"_panel");
		var theCSSprop = window.getComputedStyle(elem,null).getPropertyValue("display");
		 if(theCSSprop=='none'){
			 if (event.shiftKey && event.which ==9) {
				 //(event.shiftKey &amp;&amp; event.which ==9
				 console.log("entra &&");
			 // Shift-tab pressed
			 }else{
				  if (event.which ==13 || event.which ==9 || event.which ==null) {						  								 
					  var func=window[funcionStr];
					  console.log("id:"+id+"    funcionStr:"+funcionStr);
					  func();	
				  }
			 }
		 }
	});
}	


			

/*
--autocomplete_valkeyMax--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\
*idsf= mismo id del componente con todo y form formCexBusqEd:idEspecialidadAc quitar # y \\
*funcionStr= Nombre de la función (en este caso id del remoteCommand) que queramos ejecutar
*max= valor con el cual se ejecutará la acción
*/
function autocomplete_valKeyMax(id,idsf,funcionStr,max){	
	console.log("autocomplete_valKeyMax id:"+id+"  idsf:"+idsf+"    funcionStr:"+funcionStr+"    max:"+max);
	$(id+"_input").keyup(function(){
	var str=document.getElementById(idsf+'_input');	
	 var func=window[funcionStr];
	 
	if (str.value.length == max){	
		func();				
	}else if(str.value.length > max){
		//supr 46
		//8 backspace
		 if (event.which ==8 || event.which ==46 || event.which ==null) {	
			 document.getElementById(idsf+'_input').value="";
			 func();
			
		  }
	}
	
});	

	
	
}
/*Funcion que funciona para deshabilitar la ejecución del boton en
 * primefaces , solo recibe el nombre del formulario donde se encunetra
 * 
 * */
function btn_click(formulario){
	 $(formulario).keydown(function(event){
		 if(event.keyCode == 13 ) {
		      event.preventDefault();					      					   
		      if(document.activeElement.tagName=="BUTTON"){		    	 
			    	 var activo=document.getElementById(document.activeElement.getAttribute("id"));
			    	 activo.click();
			    	 activo.disabled = true;						    					    	
			  }
		    }
		  });
}


/*
--Funcion general, enter, tabulador change input--
*id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ 
*sig_id=id del siguiente componente (algunos componente como los selectone menu requieren _input) 
formCexBusqEd:idRegimen_focus sin # y sin \\
*/
function valkeyET_changefoco(id,sig_id){
	
	$(id).on('keydown change',function( event){					
		 if (event.shiftKey && event.which ==9) {
			 //event.shiftKey &amp;&amp; event.which ==9
			  // Shift-tab pressed
			 console.log("entra &&");
		 }else{
			  if (event.which ==13 || event.which ==null) {				  
				  document.getElementById(sig_id).focus();
			  }
		 }
	});

}

//Valida formato de fecha
function validarFecha(fecha){
	//console.log("fecha:"+fecha);
	fecha=fecha.split("/"); 
	var dia=fecha[0]; 
	var mes=parseInt(fecha[1])-1; 
	var ano=fecha[2]; 
//	console.log("ano:"+ano);
//	console.log("mes:"+mes);
//	console.log("dia:"+dia);
	 
	var valor = new Date(ano, mes, dia);
	//console.log("valor:"+valor); 
	if(valor.getMonth()!=mes) 
	{ 
	//	console.log("mal fecha");
		return false;
	} 
	else if(valor.getUTCDate()!=dia) 
	{ 
	//	console.log("mal fecha"); 
		return false;
	} 
	else{ 
	//	console.log("bien la fecha");
		return true;
	} 

}
