/*
 * Script Comunes para la funcionalidad en vista de SIMO .... janiel
 */
function activaLog(activo) {

    var log;
    if (activo) {
        log = log4javascript.getDefaultLogger();
    } else {
        log = log4javascript.getNullLogger();
    }
    return log;
}

PrimeFaces.locales['es'] = {
    closeText : 'Cerrar',
    prevText : 'Anterior',
    nextText : 'Siguiente',
    monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
            'Noviembre', 'Diciembre' ],
    monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
    dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
    dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
    dayNamesMin : [ 'D', 'L', 'M', 'X', 'J', 'V', 'S' ],
    weekHeader : 'Semana',
    firstDay : 1,
    isRTL : false,
    showMonthAfterYear : false,
    yearSuffix : '',
    timeOnlyTitle : 'Sólo hora',
    timeText : 'Tiempo',
    hourText : 'Hora',
    minuteText : 'Minuto',
    secondText : 'Segundo',
    currentText : 'Fecha actual',
    ampm : false,
    month : 'Mes',
    week : 'Semana',
    day : 'Día',
    allDayText : 'Todo el día'
};

/**
 * Scripts de SIMOCentral
 */
// Nombre de Eventos
var $_KEYDOWN = "keydown";
var $_KEYUP = "keyup";
var $_CHANGE = "change";
var $_KEYDOWN_CHANGE = "keydown change";
var $_KEYUP_KEYDOWN_CHANGE = "keyup keydown change";
var $_KEYUP_KEYDOWN = "keyup keydown";
var $_INPUT = "_input";
var $_PANEL = "_panel";
var $_BUTTON = "BUTTON";
var $_DISPLAY = "display";
var $_ID = "id";
var $ENTER = 13;
var $TAB = 9;
var $ESC = 27;
var $SPACE = 32;

// $TIEMPO_ESPERA es el tiempo en que se bloqueara para qu no se ejecute un $_KEYUP, $_KEYDOWN o $_CHANGE dos veces
var $TIEMPO_ESPERA = 100;
// variable contador para bloquear un eo Change inecesario
var $C = 0;

// Desacrtivar el submit con Enter de botones de primefaces por cada form
function botonNoSubmit(idForm) {

    $(document).on("keydown", "#" + idForm, function(event) {

        if (event.keyCode == 13) {
            event.preventDefault();
            if (document.activeElement.tagName == "BUTTON") {
                var activo = document.getElementById(document.activeElement.getAttribute("id"));
                activo.click();
                if ($C < 1) {
                    $C = $C + 1;
                    setTimeout(reiniciaContador, $TIEMPO_ESPERA);
                    activo.click();
                }
            }
        }
    });
}

// funcion para actualizar el foco desde Javascript como parametro usa el id del elemento que tiene el valor del foco
// actualizarFoco(procedimientosForm:idHiddenFoco) se realizar update sobre el id del elemento
function actualizarFoco(formFoco) {

    document.getElementById(document.getElementById(formFoco).value).focus()

}

function asignarFoco(formFoco){
    document.getElementById(formFoco).focus();
}

// FUNCIONES GENERALES
function eventKeydownChange(e, valor, fStr, max,tiempo) {

    if (e.type == $_KEYDOWN) {
        if (e.shiftKey && e.which == $TAB) {
            // Shift-tab pressed
        } else {
            if (e.which == $ENTER || e.which == $TAB) {
                var f = window[fStr];
                log.info("->ET eventKeydownChange :" + fStr);
                f();
                $C = $C + 1;
                setTimeout(reiniciaContador, tiempo);
            }

        }
    } else if (e.type == $_CHANGE && valor.length != max) {
            var fn = window[fStr];
            log.info("->CHANGEeventKeydownChange :" + fStr);
            fn();
    }
}
function eventKeydownChangeBlock(e, valor, fStr, max) {

    if (e.type == $_KEYDOWN) {
        if (e.shiftKey && e.which == $TAB) {
            // Shift-tab pressed
        } else {
            if (e.which == $ENTER || e.which == $TAB) {
                var f = window[fStr];
                log.info("->ET eventKeydownChangeBlock :" + fStr);
                scriptBloquear();
                f();
                $C = $C + 1;
                setTimeout(reiniciaContador, $TIEMPO_ESPERA);
            }

        }
    } else if (e.type == $_CHANGE && valor.length != max) {
        var fn = window[fStr];
        log.info("->CHANGE eventKeydownChangeBlock:" + fStr);
        scriptBloquear();
        fn();
    }
}
function eventKeydownChangeMask(e, valor, fStr, max) {

    if (e.type == $_KEYDOWN) {
        if (e.shiftKey && e.which == $TAB) {
            // Shift-tab pressed
        } else {
            if (e.which == $ENTER || e.which == $TAB) {
                var f = window[fStr];
                log.info("->CHANGE eventKeydownChangeMask:" + fStr);
                f();
                $C = $C + 1;
                setTimeout(reiniciaContador, $TIEMPO_ESPERA);
            }

        }
    } else if (e.type == $_CHANGE) {
        var s = valor.split('_').join('');
        if (s.length != max) {
            var fn = window[fStr];
            log.info("->CHANGE eventKeydownChangeMask:" + fStr);
            fn();
        }
    }
}

function eventKeyup(e, valor, fStr, max) {

    if (e.type == $_KEYUP) {
        if (valor.length == max) {
            var f = window[fStr];
            f();
            return true;
        }
    } else {
        return false;
    }
}

function eventKeyupBlock(e, valor, fStr, max) {

    if (e.type == $_KEYUP) {
        if (valor.length == max) {
            var f = window[fStr];
            scriptBloquear();
            f();
            return true;
        }
    } else {
        return false;
    }
}

function eventKeyupMask(e, valor, fStr, max) {

    if (e.type == $_KEYUP) {
        var s = valor.split('_').join('');
        if (s.length == max) {
            var f = window[fStr];
            f();
            return true;
        }
    } else {
        return false;
    }
}

// Funcion que sirve que no se permita poner ningun simbolo con las teclas alt gr o alt+ctrl
$(document).on($_KEYDOWN, "input:enabled", function(e) {

    if (e.ctrlKey && e.altKey && e.which) {
           return false;
    }
});



function reiniciaContador() {

    $C = 0;
    log.info("->Reinicia Contador C:" + $C);
}
var $DESBLOQ = true;
function scriptBloquear() {

    $DESBLOQ = false;
    log.info("scriptBloquear $DESBLOQ:" + $DESBLOQ);
}
function scriptDesbloquear() {

    $DESBLOQ = true;
    log.info("scriptDesbloquear $DESBLOQ:" + $DESBLOQ);
}

// INPUT
/*
 * input_valMaxKeyET --Funcion general en un input al $_CHANGE o un maximo de caracteres--
 * id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ fStr= Nombre de la fión (en
 * este caso id del remoteCommand) que queramos ejecutar max= valor con el cual se ejecutará la acción. Es el numero
 * máxim de caracteres
 */
function validateInputChangeOrMaxlength(id, fStr, max) {
	$(document).on($_KEYUP_KEYDOWN_CHANGE, id, function(e) {
		if (e.which != $ENTER && e.which != $TAB && e.which != $SPACE) {
			eventKeyup(e, this.value, fStr, max)	
		}		
	});
}
/*
 * input_valKeyET --Funcion general en un input al presionar enter, tabulador $_CHANGE o un maximo de caracteres-- id=id
 * del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ fStr= Nombre de la fión (en este
 * caso id del remoteCommand) que queramos ejecutar
 */

function validateInputChangeOrPressEnterTab(id, fStr) {

    $(document).on($_KEYDOWN_CHANGE, id, function(e) {
        if ($C < 1) {
            eventKeydownChange(e, this.value, fStr, -1);
        }
    });
}

function validateInputChangeOrPressEnterTabBlock(id, fStr) {

    $(document).on($_KEYDOWN_CHANGE, id, function(e) {
        if ($C < 1) {
            eventKeydownChange(e, this.value, fStr, -1,500);
        }
    });
}

/*
 * input_valMaxKeyET --Funcion general en un input al presionar enter, tabulador $_CHANGE o un maximo de caracteres--
 * id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ fStr= Nombre de la fión (en
 * este caso id del remoteCommand) que queramos ejecutar max= valor con el cual se ejecutará la acción. Es el numero
 * máxim de caracteres
 */

function validateInputChangeOrPressEnterTabOrMaxlength(id, fStr, max) {

    $(document).on($_KEYUP_KEYDOWN_CHANGE, id, function(e) {

        if (!eventKeyup(e, this.value, fStr, max) && $C < 1) {
               eventKeydownChange(e, this.value, fStr, max);            
        }
    });
}

// input_valMaxKeyET block
function validateInputChangeOrPressEnterTabOrMaxlengthAndBlock(id, fStr, max) {

    $(document).on($_KEYUP_KEYDOWN_CHANGE, id, function(e) {
            if ($DESBLOQ &&!eventKeyupBlock(e, this.value, fStr, max) && $C < 1) {
                    eventKeydownChangeBlock(e, this.value, fStr, max);
            }
    });
}

/*
 * input_valMaxKeyETMask --Funcion general en un input al presionar enter, tabulador $_CHANGE o un maximo de
 * caracteres-- id =id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ fStr = Nombre de
 * la fión (en este caso id del remoteCommand) que queramos ejecutar max = valor con el cual se ejecutará la acción. Es
 * el numero máxim de caracteres
 */

function validateInputMaskChangeOrPressEnterTabOrMaxlength(id, fStr, max) {

    $(document).on($_KEYUP_KEYDOWN_CHANGE, id, function(e) {        
        if (!eventKeyupMask(e, this.value, fStr, max) && $C < 1) {
            eventKeydownChangeMask(e, this.value, fStr, max);
        }
    });
}

/*
 * SELECTONE MENU id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\ idsf= mismo id
 * del componente con todo y form formCexBusqEd:idEspecialidadAc quitar # y \\ fStr= Nombre de la fión (en este caso id
 * del remoteCommand) que queramos ejecutar
 */
// selectOneMenu_valKeyET
function validateSelectOneMenuPressEnterTab(id, idsf, fStr) {

    $(document).on($_KEYUP_KEYDOWN, id + "_focus", function(e) {

        var elem = document.getElementById(idsf + "_panel");
        log.info("-> " + id + "_focus");
        log.info("->elem " + elem);

        var theCSSprop = window.getComputedStyle(elem, null).getPropertyValue($_DISPLAY);

        if (theCSSprop == 'none' && $C < 1) {
                if (e.type == $_KEYUP) {
                    if (e.which == $ENTER) {
                        var f = window[fStr];
                        log.info("ENTER->id:" + id + "    fStr:" + fStr);
                        f();
                        $C = $C + 1;
                        setTimeout(reiniciaContador, $TIEMPO_ESPERA);
                    }
                } else if (e.type == $_KEYDOWN) {
                    if (e.shiftKey && e.which == $TAB) {
                        // Shift-tab pressed
                    } else {
                        if (e.which == $TAB) {
                            var f = window[fStr];
                            log.info("TAB->id:" + id + "    fStr:" + fStr);
                            f();
                            $C = $C + 1;
                            setTimeout(reiniciaContador, $TIEMPO_ESPERA);
                        }
                    }
                }
        }
    });
}

/*
 * --autocomplete_valkeyMax-- id=id del componente con todo y form #formCexBusqEd\\:idEspecialidadAc poner el # y \\
 * idsf= mismo id del componente con todo y form formCexBusqEd:idEspecialidadAc quitar # y \\ fStr= Nombre de la fión
 * (en este caso id del remoteCommand) que queramos ejecutar max= valor con el cual se ejecutará la acción
 */
// autocomplete_valKeyETMax
function validateAutocompletePressEnterTabOrMaxlength(id, idsf, fStr, max) {

    $(document).on($_KEYUP_KEYDOWN_CHANGE, id + $_INPUT, function(e) {
        var s = document.getElementById(idsf + $_INPUT);

        if (e.type == $_KEYUP) {
            if (s.value.length == max) {
                var f = window[fStr];
                log.info("->KEYUPautocomplete_valKeyMax $_KEYUP MAX id:" + id + "    fStr:" + fStr);
                f();
            }
            if (s.value.length > max && (e.which == 8 || e.which == 46 || e.which == null)) {
                // supr 46
                // 8 backspace
                document.getElementById(idsf + $_INPUT).value = "";
                f();
            }
        } else if ($C < 1) {
            if (e.type == $_KEYDOWN) {
                if (e.shiftKey && e.which == $TAB) {
                    // e.shiftKey &amp;&amp; e.which ==$TAB
                    // Shift-tab pressed
                    log.info("entra &&");
                } else {
                    if (e.which == $ENTER || e.which == $TAB) {
                        var f = window[fStr];
                        log.info("->KEYDOWN.js id:" + id + "    fStr:" + fStr);
                        f();
                        $C = $C + 1;
                        setTimeout(reiniciaContador, $TIEMPO_ESPERA);
                    }
                }
            } else if (e.type == $_CHANGE && s.value.length != max) {
                var f = window[fStr];
                log.info("->input_valMaxKeyET2 CHANGE id:" + id + "    fStr:" + fStr);
                f();
            }
        }
    });
}

// Input en dialogs
function validateInputDialogChangeOrPressEnterTabOrMaxlength(id, fStr, max, growl_widget, mensaje) {

    $(document).on($_KEYUP_KEYDOWN, id, function(e) {

        if (e.type == $_KEYUP) {
            var s = this.value;
            if (s.length == max && e.keyCode != 27) {
                log.info("LONGITUDC");
                var f = window[fStr];
                f();
            }
        }
        if (e.type == $_KEYDOWN) {
            if (e.shiftKey && e.which == $TAB) {
                // Shift-tab pressed
            } else {
                if (e.which == $ENTER || e.which == $TAB) {
                    if (this.value.length == 0) {
                        log.info("ENTERa REQUERIDO");
                        rendereaMensaje(growl_widget, mensaje);
                    } else {
                        log.info("ENTERa CON VALOR");
                        var f = window[fStr];
                        f();
                    }
                }
            }
            if (e.which == $ESC) {
                log.info("TECLAa ESC");
                var f = window[fStr];
                f([ {
                    name : 'param',
                    value : 'true'
                } ]);
            }
        }
    });
}

function validateInputDialogChangeOrPressEnterTab(id, fStr, growl_widget, mensaje) {

    $(document).on($_KEYUP_KEYDOWN, id, function(e) {

        if (e.type == $_KEYDOWN) {
            if (e.shiftKey && e.which == $TAB) {
                // Shift-tab pressed
            } else {
                if (e.which == $ENTER || e.which == $TAB) {
                    if (this.value.length == 0) {
                        log.info("ENTERa REQUERIDO");
                        rendereaMensaje(growl_widget, mensaje);
                    } else {
                        log.info("ENTERa CON VALOR");
                        var f = window[fStr];
                        f();
                    }
                }
            }
            if (e.which == $ESC) {
                log.info("TECLAa ESC");
                var f = window[fStr];
                f([ {
                    name : 'param',
                    value : 'true'
                } ]);
            }
        }
    });
}

// INPUT MASK EN UN DIALOG
function validateMaskDialogChangeOrPressEnterTabOrMaxlength(id, fStr, max, growl_widget, mensaje) {

    $(document).on($_KEYUP_KEYDOWN, id, function(e) {

        var s = "";
        if (!eventKeyupMask(e, this.value, fStr, max) && $C < 1) {
            eventKeydownChangeMask(e, this.value, fStr, max);
        }
        if (e.type == $_KEYDOWN) {
            if (e.shiftKey && e.which == $TAB) {
                // Shift-tab pressed
            } else {
                if (e.which == $ENTER || e.which == $TAB) {
                    if (this.value.length == 0) {
                        log.info("ENTERa REQUERIDO");
                        rendereaMensaje(growl_widget, mensaje);
                    } else {
                        log.info("ENTERa CON VALOR");
                        var f = window[fStr];
                        f();
                    }
                }
            }
            if (e.which == $ESC) {
                log.info("TECLAa ESC");
                var f = window[fStr];
                f([ {
                    name : 'param',
                    value : 'true'
                } ]);
            }
        }
    });
}

function rendereaMensaje(growl_widget, str) {

    PF(growl_widget).renderMessage({
        "summary" : str,
        "detail" : "",
        "severity" : "error"
    });
}

// Bloquear
// autocomplete_valKeyETMaxBloquear
function validateAutocompletePressEnterTabOrMaxlengthAndBlock(id, idsf, fStr, max) {

    $(document).on($_KEYUP_KEYDOWN_CHANGE, id + $_INPUT, function(e) {
        var s = document.getElementById(idsf + $_INPUT);
        if ($DESBLOQ) {
            if (e.type == $_KEYUP) {
                if (s.value.length == max) {
                    var f = window[fStr];
                    log.info("->KEYUPautocomplete_valKeyMax $_KEYUP MAX id:" + id + "    fStr:" + fStr);
                    scriptBloquear();
                    f();
                }
                if (s.value.length > max && (e.which == 8 || e.which == 46 || e.which == null)) {
                    // supr 46
                    // 8 backspace
                        document.getElementById(idsf + $_INPUT).value = "";
                        scriptBloquear();
                        f();
                }
            } else if ($C < 1) {
                if (e.type == $_KEYDOWN) {
                    if (e.shiftKey && e.which == $TAB) {
                        // e.shiftKey &amp;&amp; e.which ==$TAB
                        // Shift-tab pressed
                        log.info("entra &&");
                    } else {
                        if (e.which == $ENTER || e.which == $TAB) {
                            var f = window[fStr];
                            log.info("->KEYDOWN.js id:" + id + "    fStr:" + fStr);
                            scriptBloquear();
                            f();
                            $C = $C + 1;
                            setTimeout(reiniciaContador, $TIEMPO_ESPERA);
                        }
                    }
                } else if (e.type == $_CHANGE && s.value.length != max) {
                        var f = window[fStr];
                        log.info("->input_valMaxKeyET2 CHANGE id:" + id + "    fStr:" + fStr);
                        scriptBloquear();
                        f();
                }
            }
        }
    });
}

/*
 * Funcion que sirve para deshabilitar la ejecución del boton enter en primefaces , solo recibe el nombre del formulario donde
 * se encunetra
 */
function btn_click(formulario) {

    $(formulario).keydown(function(e) {

        if (e.keyCode == $ENTER) {
            e.preventDefault();
            if (document.activeElement.tagName == $_BUTTON) {
                var activo = document.getElementById(document.activeElement.getAttribute($_ID));
                activo.click();
                activo.disabled = true;
            }
        }
    });
}

// Valida formato de fecha
function validarFecha(fecha) {

    // log.info("fecha:"+fecha);
    fecha = fecha.split("/");
    var dia = fecha[0];
    var mes = parseInt(fecha[1]) - 1;
    var ano = fecha[2];
    // log.info("ano:"+ano);
    // log.info("mes:"+mes);
    // log.info("dia:"+dia);

    var valor = new Date(ano, mes, dia);
    // log.info("valor:"+valor);
    if (valor.getMonth() != mes) {
        // log.info("mal fecha");
        return false;
    } else if (valor.getUTCDate() != dia) {
        // log.info("mal fecha");
        return false;
    } else {
        // log.info("bien la fecha");
        return true;
    }
}

/*
 * --Funcion general, enter, tabulador change input-- id=id del componente con todo y form
 * #formCexBusqEd\\:idEspecialidadAc poner el # y \\ sig_id=id del siguiente componente (algunos componente como los
 * selectone menu requieren _input) formCexBusqEd:idRegimen_focus sin # y sin \\
 */
function valkeyET_changefoco(id, sig_id) {

    $(id).on('keydown change', function(event) {

        if (event.shiftKey && event.which == $TAB) {
            // event.shiftKey &amp;&amp; event.which ==$TAB
            // Shift-tab pressed
            log.info("entra &&");
        } else {
            if (event.which == $ENTER || event.which == null) {
                document.getElementById(sig_id).focus();
            }
        }
    });

}

function escDialog() {

    $(document).keyup(function(e) {

        if (e.keyCode == 27) { // esc code is 27
            PF('dlgPacientes').hide();
            PF('dlgCamaPacientes').hide();
            PF('idDlgCantidad').hide();
        }
    });

}

function pressEscDialog(idWidget,fStr) {
    $(document).keyup(function(e) {               
                if (e.which == $ESC && PF(idWidget).isVisible()) {                    
                        var f = window[fStr];
                        f([ {
                            name : 'param',
                            value : 'true'
                        } ]);    
                    }                                             
            });
}

function setClassEncabezadoPieDePagina(encabezado, pieDePagina){
    document.getElementById('encabezado').removeAttribute('class');
    document.getElementById('pieDePagina').removeAttribute('class');
    document.getElementById('encabezado').setAttribute("class", encabezado);
    document.getElementById('pieDePagina').setAttribute("class", pieDePagina);
}

function deshabilitaBotonesEncabezado(){
    if(document.getElementById("idHeaderGeneral:btnp_usuario")==null){
     
    }else{
  
        document.getElementById("idHeaderGeneral:btnp_usuario").style.visibility = "collapse";
    }
     if(document.getElementById("idHeaderGeneral:btnp_ayuda")==null){
       
        }else{
          
            document.getElementById("idHeaderGeneral:btnp_ayuda").style.visibility = "collapse";
        }
     if(document.getElementById("idHeaderGeneral:btnp_salir")==null){
        
     }else{
         
         document.getElementById("idHeaderGeneral:btnp_salir").style.visibility = "collapse";
     }   
}

/*Funcion que sirve para la configuración visual de las pantalas
 * Color de encabezado y pie de página, periodo, botones
 */
function configuraPantalla(pantallaModulo, tipoServicio, pantallaDeCaptura){
   
    //Diferente a portada
    setClassEncabezadoPieDePagina('encabezadoCaptura', 'pieDePaginaCaptura');
    cambiarColorPantalla(pantallaModulo);    
    evaluarTipoServicio(tipoServicio);   
    if(pantallaDeCaptura){
        deshabilitaBotonesEncabezado();
    }
}

function evaluarTipoServicio(tipoServicio){
    log.info("tipoServicio:"+tipoServicio);   
    switch(tipoServicio){
        //consulta externa
        case 1:
            
//            document.getElementById('idHeaderGeneral:servicioHospi').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='none';
            if(document.getElementById('idHeaderGeneral:servicioCex').value==""){
                document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='none';
                document.getElementById('idHeaderGeneral:servicioCex').style.display='none';
            }else{
                document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='inline';
                document.getElementById('idHeaderGeneral:servicioCex').style.display='inline';
            }
            break;
        //hospitalizacion
        case 2:
           
//            document.getElementById('idHeaderGeneral:servicioCex').style.display='none';
//            
//            document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='none';
        	/*if(document.getElementById('idHeaderGeneral:monitoreoServicioHospi').value == null){
        		 document.getElementById('idHeaderGeneral:labelTipoMonitoreo').style.display='none';
                 document.getElementById('idHeaderGeneral:monitoreoServicioHospi').style.display='none';
        	}else{
        		 document.getElementById('idHeaderGeneral:labelTipoMonitoreo').style.display='inline';
                 document.getElementById('idHeaderGeneral:monitoreoServicioHospi').style.display='inline';
        	}*/
            if(document.getElementById('idHeaderGeneral:servicioHospi').value==""){
                document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='none';
                document.getElementById('idHeaderGeneral:servicioHospi').style.display='none';
            }else{
                document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='inline';
                document.getElementById('idHeaderGeneral:servicioHospi').style.display='inline';
            }
            break;
        case 3:
//            document.getElementById('idHeaderGeneral:servicioCex').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioHospi').style.display='none';
//            
//            document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='none';
            if(document.getElementById('idHeaderGeneral:servicioInfoComplementaria').value==""){
                document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='none';
                document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='none';
            }else if(document.getElementById('idHeaderGeneral:labelTipoServicio') && document.getElementById('idHeaderGeneral:servicioInfoComplementaria')){
                document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='inline';
                document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='inline';
            }
            break;
        case 4:
//            document.getElementById('idHeaderGeneral:servicioCex').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioHospi').style.display='none';
//            document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='none';
            
            if(document.getElementById('idHeaderGeneral:servicioSubrogados').value==""){
                document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='none';
                document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='none';
            }else if(document.getElementById('idHeaderGeneral:labelTipoServicio') && document.getElementById('idHeaderGeneral:servicioSubrogados')){
                document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='inline';
                document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='inline';
            }
            break;
         default:
//             document.getElementById('idHeaderGeneral:labelTipoServicio').style.display='none';
//             document.getElementById('idHeaderGeneral:servicioCex').style.display='none';
//             document.getElementById('idHeaderGeneral:servicioHospi').style.display='none';
//             document.getElementById('idHeaderGeneral:servicioInfoComplementaria').style.display='none';
//             document.getElementById('idHeaderGeneral:servicioSubrogados').style.display='none';
         break;
    }
    
}

function cambiarColorPantalla(pantallaModulo) {
    log.info("pantallaModulo:"+pantallaModulo);
    switch(pantallaModulo){
        //Administracion
        case 1:
            document.getElementById('encabezado').className += " encabezadoyPieAdministracion";
            document.getElementById('pieDePagina').className += " encabezadoyPieAdministracion";
            break;
        //Catalogos
        case 2:
            document.getElementById('encabezado').className += " encabezadoyPieCatalogos";
            document.getElementById('pieDePagina').className += " encabezadoyPieCatalogos";
            break;
        //Consulta Externa
        case 3:
            document.getElementById('encabezado').className += " encabezadoyPieConsultaExterna";
            document.getElementById('pieDePagina').className += " encabezadoyPieConsultaExterna";
            break;
        //Hospitalizacion
        case 4:
            document.getElementById('encabezado').className += " encabezadoyPieHospitalizacion";
            document.getElementById('pieDePagina').className += " encabezadoyPieHospitalizacion";
            break; 
        //Reporte
        case 5:
            document.getElementById('encabezado').className += " encabezadoyPieReportes";
            document.getElementById('pieDePagina').className += " encabezadoyPieReportes";
            break;
        //Enlaces
        case 6:
            document.getElementById('encabezado').className += " encabezadoyPieEnlaces";
            document.getElementById('pieDePagina').className += " encabezadoyPieEnlaces";
            break;
        //Utilerias
        case 7:
            document.getElementById('encabezado').className += " encabezadoyPieUtilerias";
            document.getElementById('pieDePagina').className += " encabezadoyPieUtilerias";
            break;
        //Interfaces
        case 8:
            document.getElementById('encabezado').className += " encabezadoyPieInterfaces";
            document.getElementById('pieDePagina').className += " encabezadoyPieInterfaces";
            break;
        default:
            document.getElementById('encabezado').className += " encabezadoyPiePrincipal";
            document.getElementById('pieDePagina').className += " encabezadoyPiePrincipal";
            break;
    }
    
}

function ejecutarRemoteCommand(fStr) {
        var f = window[fStr];        
        f();              
}

