/**
 * var log se implenta generar log4j mediante la libreria importada en el head para deshabilitar utilizar var log =
 * log4javascript.getNullLogger(); y comentar var log = log4javascript.getDefaultLogger();
 */
//var log = log4javascript.getDefaultLogger();
 var log = log4javascript.getNullLogger();

$(document).ready(
        function() {
            btn_click("#datosPacienteAnterForm")
            
            validateInputMaskChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idFechaModificacion", "rcFechaModificacion", 10);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idNssOriginal", "rcNss", 10);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idNumeroPaciente",
                    "rcMapeoPaciente", 4);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idNumeroPacienteNuevo",
                    "rcMapeoPacienteNuevo", 4);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idNssMod", "rcNssNw", 10);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idAgregadoMedicoNew",
                    "rcAgregadoRegimen", 8);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idNombreMod", "rcNombre", 50);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idApellidoPaternoMod",
                    "rcApellidoPaterno", 50);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idApellidoMaternoMod",
                    "rcApellidoMaterno", 50);
            validateInputChangeOrPressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idNumero", "rcNumero", 3);
            validateAutocompletePressEnterTabOrMaxlength("#datosPacienteAnterForm\\:idDelegacionMod","datosPacienteAnterForm:idDelegacionMod","rcDelegacion",2);
            
            validateSelectOneMenuPressEnterTab("#datosPacienteAnterForm\\:idUbicacion", "datosPacienteAnterForm:idUbicacion",
            "rcUbicacion");
            //setColorEncabezado();
            configuraPantalla(4,2,true);
            escDialog();
        });

function setColorEncabezado(){
    setClassEncabezadoPieDePagina('encabezadoCaptura','pieDePaginaCaptura');
}

function verificarDialogo() {

    rcObtenerPaciente();
    
}

function deshabilitaEnter() {

    btn_click("#datosPacienteAnterForm");
}

function limpiarCampoDelegacion() { 
	deshabilitaEnter();
    document.getElementById('datosPacienteAnterForm:idDelegacionMod_input').value = "";
  
}

function desplazarFocoGuardar(){
    var foco = $('[id="datosPacienteAnterForm:idHiddenFoco"]').val();
    if (foco == "datosPacienteAnterForm:idGuardar") {
        $('[id="datosPacienteAnterForm:idGuardar"]').focus();
    } 
}   

var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {
                    
                    var focoNombre = document.getElementById('datosPacienteAnterForm:idHiddenFoco').value == 'datosPacienteAnterForm:idNombreMod';
                    var focoPaterno = document.getElementById('datosPacienteAnterForm:idHiddenFoco').value == 'datosPacienteAnterForm:idApellidoPaternoMod';
                    var focoMaterno = document.getElementById('datosPacienteAnterForm:idHiddenFoco').value == 'datosPacienteAnterForm:idApellidoMaternoMod';                    

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if ((focoNombre || focoPaterno || focoMaterno) && (e.keyCode > 47 && e.keyCode < 58)) {
                        return false;
                    }
                    if ((e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223)) {
                        return false;
                    }

                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }

                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }

                    if (e.keyCode == 226) {
                        return false;
                    }

                    if (shiftPresionado && e.keyCode == 226) {
                        return false;
                    }
                });