var log = log4javascript.getNullLogger();

$(document).ready(function() {

    btn_click("#infoCompForm");

    var $_FORM = "#infoCompForm\\:";
    var $_FORM_SF = "infoCompForm:";

    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idPeriodo", "rcPeriodo", 6);
    validateSelectOneMenuPressEnterTab($_FORM + "idSubsistema", $_FORM_SF + "idSubsistema", "rcSubsistema");
    validateSelectOneMenuPressEnterTab($_FORM + "idRCV", $_FORM_SF + "idRCV", "rcRCV");
    validateSelectOneMenuPressEnterTab($_FORM + "idPoblacion", $_FORM_SF + "idPoblacion", "rcPoblacion");
    validateSelectOneMenuPressEnterTab($_FORM + "idPor", $_FORM_SF + "idPor", "rcPor");
    validateSelectOneMenuPressEnterTab($_FORM + "idEn", $_FORM_SF + "idEn", "rcEn");
    validateSelectOneMenuPressEnterTab($_FORM + "idHoja", $_FORM_SF + "idHoja", "rcHoja");
    validateSelectOneMenuPressEnterTab($_FORM + "idSubclave", $_FORM_SF + "idSubclave", "rcSubclave");
    
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato1", "rcDato1", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato2", "rcDato2", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato3", "rcDato3", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato4", "rcDato4", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato5", "rcDato5", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato6", "rcDato6", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato7", "rcDato7", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato8", "rcDato8", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato9", "rcDato9", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idCifraDeControl", "rcCifraDeControl", 9);
    
    
    cierradialogo();

});

var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if ((e.keyCode > 218 && e.keyCode < 223)) {
                        return false;
                    }
                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }

                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                });

function cierradialogo() {

    $(document).keyup(function(e) {

        if (e.keyCode == 27) { // esc code is 27
            cerrarJustificacionConfirm();
        }
    });

}

function cerrarJustificacionConfirm() {

    var modal = $('[id="infoCompForm:idHiddenJustificacionConfirm"]').val();
    if (modal == "CONFIRMACIONJUSTIFICACION") {
        PF('justificacionConfirm').hide();

    }
}

function actualizarFocoGuardar() {

    var foco = $('[id="infoCompForm:idHiddenFoco"]').val();
    if(foco == "infoCompForm:idNo"){
        $('[id="infoCompForm:idNo"]').focus();
    }
    if (foco == "infoCompForm:idGuardar") {
        $('[id="infoCompForm:idGuardar"]').focus();
    } else {
        $('[id="infoCompForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}