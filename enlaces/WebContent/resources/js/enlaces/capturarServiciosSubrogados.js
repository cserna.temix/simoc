/**
 * var log se implenta generar log4j mediante la libreria importada en el head para deshabilitar utilizar var log =
 * log4javascript.getNullLogger(); y comentar var log = log4javascript.getDefaultLogger();
 */
// var log = log4javascript.getDefaultLogger();
var log = log4javascript.getNullLogger();

$(document).ready(
        function() {

            var $_FORM = "#capturarSubrogadosForm\\:";
            var $_FORM_SF = "capturarSubrogadosForm:";

            deshabilitaEnter();
            escDialog();
            escDialogPeriodo();
            // setColorEncabezado();

            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNss", "rcNss", 10);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idAgregadoMedico", "rcAgregadoMedico", 8);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idEdad", "rcEdad", 3);
            validateInputChangeOrPressEnterTab($_FORM + "idNombre", "rcNombre");
            validateInputChangeOrPressEnterTab($_FORM + "idApellidoPaterno", "rcApellidoPaterno");
            validateInputChangeOrPressEnterTab($_FORM + "idApellidoMaterno", "rcApellidoMaterno");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idNumero", "rcNumero", 3);
            validateAutocompletePressEnterTabOrMaxlength($_FORM + "idDelegacion", $_FORM_SF + "idDelegacion",
                    "rcDelegacion", 2);
            validateSelectOneMenuPressEnterTab($_FORM + "idTipoContrato", $_FORM_SF + "idTipoContrato",
                    "rcTipoContrato");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idFolioContrato", "rcFolioContrato", 14);
            validateSelectOneMenuPressEnterTab($_FORM + "idTipoServicio", $_FORM_SF + "idTipoServicio",
                    "rcTipoServicio");
            validateSelectOneMenuPressEnterTab($_FORM + "idMotivoSubrogacion", $_FORM_SF + "idMotivoSubrogacion",
                    "rcMotivoSubrogacion");
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idServiciosSubrogar", "rcServiciosSubrogar", 3);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCantidadEventos", "rcCantidadEventos", 7);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCostoUnitario_input", "rcCostoUnitario", 9);
            validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idMatricula", "rcMatricula", 10);
            validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idFechaServicio", "rcFechaServicio", 10);
            validateInputChangeOrPressEnterTab($_FORM + "idNumeroPaciente", "rcNumeroPaciente");
            configuraPantalla(6,4,true);
        });

function deshabilitaEnter() {

    btn_click("#capturarSubrogadosForm");
}

function escDialogPeriodo() {
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            PF('dlgCierreMes').hide();
        }
    });
}
//function setColorEncabezado() { 
//    setClassEncabezadoPieDePagina('encabezadoEnlaces', 'pieDePaginaEnlaces'); 
//    }



function actualizarFocoBotonSi() {
		
    var foco = $('[id="capturarSubrogadosForm:idHiddenFoco"]').val();
    if (foco == "capturarSubrogadosForm:idSi") {
        $('[id="capturarSubrogadosForm:idSi"]').focus();
    } else {
        $('[id="capturarSubrogadosForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

function actualizarFocoGuardar() {

    var foco = $('[id="capturarSubrogadosForm:idHiddenFoco"]').val();
    if (foco == "capturarSubrogadosForm:idGuardar") {
        $('[id="capturarSubrogadosForm:idGuardar"]').focus();
    } else {
        $('[id="capturarSubrogadosForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

function actualizarFocoPeriodoCerrado() {

    var foco = $('[id="capturarSubrogadosForm:idHiddenFoco"]').val();

    if (foco == "capturarSubrogadosForm:idPeriodoCerrado") {
        $('[id="capturarSubrogadosForm:idPeriodoCerrado"]').focus();
    } else {
        $('[id="capturarSubrogadosForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }

}

var shiftPresionado;

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    var focoFolio = $('[id="capturarSubrogadosForm:idFolioContrato"]').is(":focus");
                    var focoCostoUnitario = document.getElementById('capturarSubrogadosForm:idHiddenFoco').value == 'capturarSubrogadosForm:idCostoUnitario';

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if (focoFolio && e.keyCode == 190) {
                        return true;
                    }
                    if (focoCostoUnitario && e.keyCode == 190) {
                        return true;
                    }
                    
                    if (focoCostoUnitario && e.keyCode == 173){
                        return false;
                    }
                    if ((e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223)) {
                        return false;
                    }
                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }
                    if (focoFolio && shiftPresionado && e.keyCode == 55) {
                        return true;
                    }
                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                    if (e.keyCode == 226) {
                        return false;
                    }
                    if (shiftPresionado && e.keyCode == 226) {
                        return false;
                    }
                });
