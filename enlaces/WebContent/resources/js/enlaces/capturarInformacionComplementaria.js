var log = log4javascript.getNullLogger();

$(document).ready(function() {

    log.debug('Entra en INICIAL');
    btn_click("#infoCompForm");

    var $_FORM = "#infoCompForm\\:";
    var $_FORM_SF = "infoCompForm:";

    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idPeriodo", "rcPeriodo", 6);
    validateAutocompletePressEnterTabOrMaxlength($_FORM + "idSubsistema", $_FORM_SF
            + "idSubsistema", "rcSubsistema", 2);
    validateAutocompletePressEnterTabOrMaxlength($_FORM + "idRCV", $_FORM_SF + "idRCV", "rcRCV", 3);
    validateSelectOneMenuPressEnterTab($_FORM + "idPoblacion", $_FORM_SF + "idPoblacion", "rcPoblacion");
    validateSelectOneMenuPressEnterTab($_FORM + "idPor", $_FORM_SF + "idPor", "rcPor");
    validateSelectOneMenuPressEnterTab($_FORM + "idEn", $_FORM_SF + "idEn", "rcEn");
    validateSelectOneMenuPressEnterTab($_FORM + "idHoja", $_FORM_SF + "idHoja", "rcHoja");
    validateSelectOneMenuPressEnterTab($_FORM + "idSubclave", $_FORM_SF + "idSubclave", "rcSubclave");

    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato1", "rcDato1", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato2", "rcDato2", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato3", "rcDato3", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato4", "rcDato4", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato5", "rcDato5", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato6", "rcDato6", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato7", "rcDato7", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato8", "rcDato8", 9);
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idDato9", "rcDato9", 9);
    validateInputChangeOrPressEnterTabOrMaxlength($_FORM + "idCifraControl", "rcCifraControl", 11);
    
    validateInputMaskChangeOrPressEnterTabOrMaxlength($_FORM + "idJustificacion", "rcJustificacion", 200);

    cierradialogo();
    configuraPantalla(6,3,true);

});

var shiftPresionado

$(document)
        .on(
                "keydown",
                "input:enabled",
                function(e) {

                    if (e.ctrlKey && e.altKey) {
                        if (e.which) {
                            return false;
                        }
                    }
                    if ((e.keyCode > 218 && e.keyCode < 223)) {
                        return false;
                    }
                    if (e.shiftKey) {
                        shiftPresionado = true;
                    } else {
                        shiftPresionado = false;
                    }

                    if (shiftPresionado
                            && ((e.keyCode > 47 && e.keyCode < 65) || (e.keyCode > 185 && e.keyCode < 192) || (e.keyCode > 218 && e.keyCode < 223))) {
                        return false;
                    }
                });

function cierradialogo() {
    
    log.debug('Entra en cierradialogo');
    
    $(document).keyup(function(e) {

        if (e.keyCode == 27) { // esc code is 27
            cerrarJustificacionConfirm();
        }
    });

}

function cerrarJustificacionConfirm() {
    
    log.debug('Entra en cerrarJustificacionConfirm');

    var modal = $('[id="infoCompForm:idHiddenJustificacionConfirm"]').val();
    if (modal == "CONFIRMACIONJUSTIFICACION") {
        document.getElementById('infoCompForm:idSi').disabled = true;
        PF('justificacionConfirm').hide();
        

    }
}

function crearSaltoLinea(){
    
    var element = document.getElementById('infoCompForm:idDato');
    var saltoLinea = document.createElement("br");
    element.appendChild(saltoLinea);
}

function btn_bloquear(){
    btn_click("#infoCompForm");
}

function actualizarFocoGuardar() {

    log.debug('Entra en actualizarFocoGuardar');
    btn_click("#infoCompForm");
    var foco = $('[id="infoCompForm:idHiddenFoco"]').val();
    if (foco == "infoCompForm:idNo") {
        $('[id="infoCompForm:idNo"]').focus();
    }
    if (foco == "infoCompForm:idGuardar") {
        $('[id="infoCompForm:idGuardar"]').focus();
    } else {
        $('[id="infoCompForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

/**
 * Metodo que actualiza el foco para el boton agregar 
 */
function actualizarFocoGuardarConDato() {

    log.debug('Entra en actualizarFocoGuardarConDato');
    btn_click("#infoCompForm");
    var foco = $('[id="infoCompForm:idHiddenFoco"]').val();
    if (foco == "infoCompForm:idGuardar") {
        $('[id="infoCompForm:idGuardar"]').focus();
    } else {
        $('[id="infoCompForm:idHiddenFoco"]').addClass('ui-state-disabled').attr('disabled', 'disabled');
    }
}

function validarDato(event, object) {
    
    log.debug('Entra en validarDato');
    btn_click("#infoCompForm");
    try {
        var shift = event.shiftKey;
        var tab = event.which == 9;
        var enter = event.which == 13;

        if (tab || enter) {
            log.info("-----------------------> validar Dato Informacion Complementaria :" + "------------");
            var idComponente = object.id;
            var valorComponente = object.value;
            $("#infoCompForm\\:idDato").val(idComponente + ":" + valorComponente);
            log.info("-----------------------> validar Dato id Componente : " + idComponente + " valor : "
                    + valorComponente);
            scriptBloquear();
            rcDato();
        }
    } catch (e) {
    }

}


function asignarFocoDato() {
    
    log.debug('Entra en asignarFocoDato');
    btn_click("#infoCompForm");
    setTimeout(colocarFocoDato, 400);

}

function colocarFocoDato() {
    
    log.debug('Entra en colocarFocoDato');
    btn_click("#infoCompForm");
    $('[id="' + $("#infoCompForm\\:idFocoDato").val() + '"]').focus();
}

function asignarFocoSeccionDatos() {
    
    log.debug('Entra en asignarFocoSeccionDatos');
    log.info("-----------------------> Asignar foco a seccion dinamica Datos " + "------------");
    var objetosEdicion = new Array();
    objetosEdicion = $(".edicion_tabla");
    var text = "";
    var inicioIdFoco = "'[id=\"";
    var finIdFoco = "\"]'";
    var focoTabla = "";
    var cadena = "";

    for (var k = 0; k < objetosEdicion.length; k++) {
        text = objetosEdicion[k].id;
        log.info("-----------------------> Id " + " ------------ " + text);
        focoTabla = inicioIdFoco + text + finIdFoco;
        $("#infoCompForm\\:idFocoDato").val(text);
        log.info("=============== " + $("#infoCompForm\\:idFocoDato").val());
        rcFocoDato();
        break;
    }
}