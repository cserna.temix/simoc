package test;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicVariableTest {

	// Create two integer objects � one normal and another atomic � with same initial value
	private static Integer integer = new Integer(0);
	private static AtomicInteger atomicInteger = new AtomicInteger(0);
	
	static class IntegerIncrementer extends Thread {
		public void run() {
			System.out.println("Incremented value of integer is: " + ++integer);
		}
	}
	
	static class AtomicIntegerIncrementer extends Thread {
		public void run() {
			System.out.println("Incremented value of atomic integer is: " + atomicInteger.incrementAndGet());
		}
	}
	
	public static void main(String []args) {
		// create three threads each for incrementing atomic and "normal" integers
		for(int i = 0; i < 5; i++) {
			new IntegerIncrementer().start();
			new AtomicIntegerIncrementer().start();
		}
	}

}
