package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.gob.imss.simo.enlaces.serviciossubrogados.services.CapturarServiciosSubrogadosServices;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-main-context.xml"})

public class CapturarServiciosSubrogadosController {

	@Autowired
	private CapturarServiciosSubrogadosServices capturarServiciosSubrogadosServices;
	
	
	@Test
	public void capturarServiciosSubrogadosControllerTest(){
		
		String validarNss="4808853653";
		String agregado="1F19855F";
		String tipoContrato="1";
		String folio="2456755555555";
		String tipoServicio="1";
		String motivo="1";
		String servicio="104";
		String cantidad="8765456";
		String costo="99";
		String validarMatricula="9941932";
		String fecha="26/02/2017";
		String presupuestal="030101022151";
		
		try{
			capturarServiciosSubrogadosServices.validarNss(validarNss);
			capturarServiciosSubrogadosServices.validarAgregadoMedico(agregado);
			capturarServiciosSubrogadosServices.validarTipoContrato(presupuestal);
			capturarServiciosSubrogadosServices.validarFolioContrato(folio);
			capturarServiciosSubrogadosServices.validarMotivoSubrogacion(motivo);
			capturarServiciosSubrogadosServices.validarServiciosSubrogar(servicio);
			capturarServiciosSubrogadosServices.validarCantidadEventos(cantidad);
			capturarServiciosSubrogadosServices.validarCostoUnitario(costo);
			capturarServiciosSubrogadosServices.validarMatricula(validarMatricula);
			capturarServiciosSubrogadosServices.validarFechaServicio(fecha, presupuestal);
			
		}catch(HospitalizacionException e){
			System.err.println(e.getMessage());
			
		}
	}
	
}
