package test;

import java.util.Arrays;

public class Gamma extends Beta{

	public static void main(String[] args) {
//		Gamma g1 = new Alpha();
//		 Gamma g2 = new Beta();
//		 System.out.println(g1.getType() + " " + g2.getType());
		String s1 = "hi";
		String s2 = new String("hi");
		String s3 = "hi";
		if (s1 == s2) {
			System.out.println("s1 and s2 equal");
		} else {
			System.out.println("s1 and s2 not equal");
		}
		if (s1 == s3) {
			System.out.println("s1 and s3 equal");
		} else {
			System.out.println("s1 and s3 not equal");
		}
		
		int []arr1 = {1, 2, 3, 4, 5};
		int []arr2 = {1, 2, 3, 4, 5};
		System.out.println("arr1 == arr2 is " + (arr1 == arr2));
		System.out.println("arr1.equals(arr2) is " + arr1.equals(arr2));
		System.out.println("Arrays.equals(arr1, arr2) is " + Arrays.equals(arr1, arr2));
   
	}

}

class Alpha {
	 String getType() {
		 return "alpha";
	 }
}
	
class Beta extends Alpha {
	 String getType() {
		 return "beta";
	 }
}

