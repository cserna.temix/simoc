package test;

public class Cougar extends Feline{
	
	public Cougar() {
		super();
		 System.out.print("cougar " + super.type);
	}
	
	void go() {
		type = "c ";
		System.out.print(this.type + super.type);
	}

	public static void main(String[] args) {
		new Cougar().go();
	}

}

class Feline {
	 public String type = "f ";
	 public Feline() {
		 System.out.print("feline ");
	 }
}

