package test;

public class MyStuff implements Rideable{
	
	int weight = 2;
	String name;
	MyStuff(String n) {
		name = n;
	}

	public static void main(String[] args) {
//		MyStuff m1 = new MyStuff("guitar");
//		MyStuff m2 = new MyStuff("tv");
//		System.out.println(m2.equals(m1));
		String[] sa = { "tom ", "jerry " };
		for (int x = 0; x < 3; x++) {
			for (String s : sa) {
				System.out.print(x + " " + s);
				if (x == 1) {
					break;
				}
			}
		}
		
		//new MyStuff().go(8);
	}

	@Override
	public boolean equals(Object obj) {
		MyStuff m = (MyStuff) obj;
		if (m.name != null) {
			return true;
		}
		return false;
	}
	
	public String getGait() {
		 return " mph, lope";
	}
	
	void go(int speed) {
		 ++speed; weight++;
		 int walkrate = speed * weight;
		 System.out.print(walkrate + getGait());
	}

}

interface Rideable { String getGait(); }

