package mx.gob.imss.simo.enlaces.capturarInformacionComplementaria.service.impl.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.enlaces.common.model.CierreSubsistema;
import mx.gob.imss.simo.enlaces.common.model.ComponenteCatalogoInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.DatoFormula;
import mx.gob.imss.simo.enlaces.common.model.FormulaInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaConsulta;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaDato;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCVDato;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.helper.CapturarInformacionComplementariaHelper;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.repository.impl.CapturarInformacionComplementariaRepositoryJDBCImpl;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.rules.CapturarInformacionComplementariaRules;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.services.impl.CapturarInformacionComplementariaServicesImpl;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.EstructuraDatoInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.model.DatosUsuario;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CapturarInformacionComplementariaServicesImpl.class)
public class CapturarInformacionComplementariaServiceImplTest {

	@InjectMocks
	CapturarInformacionComplementariaServicesImpl capturarInformacionComplementariaServices;

	@Mock
	CapturarInformacionComplementariaRules capturarInformacionComplementariaRules;

	@Mock
	CapturarInformacionComplementariaHelper capturarInformacionComplementariaHelper;

	@Mock
	CapturarInformacionComplementariaRepositoryJDBCImpl capturarInformacionComplementariaRepositoryJDBCImpl;

	@Mock
	CapturarInformacionComplementariaServicesImpl capturarInformacionComplementariaServicesMock;

	@Mock
	HospitalizacionCommonServicesImpl hospitalizacionCommonServicesImpl;

	@Mock
	HospitalizacionCommonRepositoryJDBCImpl hospitalizacionCommonRepositoryJDBCImpl;

	final static Logger logger = LoggerFactory.getLogger(CapturarInformacionComplementariaServiceImplTest.class);

	@BeforeClass
	public static void ini() {

		BasicConfigurator.configure();
	}

	@Before
	public void setup() {

		capturarInformacionComplementariaRules = new CapturarInformacionComplementariaRules();
		capturarInformacionComplementariaServices = new CapturarInformacionComplementariaServicesImpl();
		capturarInformacionComplementariaHelper = new CapturarInformacionComplementariaHelper();
		capturarInformacionComplementariaRepositoryJDBCImpl = new CapturarInformacionComplementariaRepositoryJDBCImpl();
		capturarInformacionComplementariaServicesMock = new CapturarInformacionComplementariaServicesImpl();
		hospitalizacionCommonRepositoryJDBCImpl = new HospitalizacionCommonRepositoryJDBCImpl();
		hospitalizacionCommonServicesImpl = new HospitalizacionCommonServicesImpl();

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void validarSubsistemaTest() {

		Object cveSubsistema = "3546";

		try {
			capturarInformacionComplementariaServices.validarSubsistema(cveSubsistema);
			logger.info("Fecha Atención:{}", cveSubsistema);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void validarRCVTest() {

		String cveRCV = "23ED3";

		try {
			doNothing().when(capturarInformacionComplementariaRules).validarRCV(anyString());
			capturarInformacionComplementariaServices.validarRCV(cveRCV);

			Assert.assertEquals(cveRCV, "23ED3");
			logger.info("RCV: {}", cveRCV);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void validarPoblacionTest() {

		int cvePoblacion = 3;

		try {
			doNothing().when(capturarInformacionComplementariaRules).validarPoblacion(anyInt());
			capturarInformacionComplementariaServices.validarPoblacion(cvePoblacion);

			Assert.assertEquals(cvePoblacion, 3);
			logger.info("Población: {}", cvePoblacion);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void validarPorTest() {

		int cvePor = 7;

		try {
			doNothing().when(capturarInformacionComplementariaRules).validarPor(anyInt());
			capturarInformacionComplementariaServices.validarPor(cvePor);

			Assert.assertEquals(cvePor, 7);
			logger.info("Clave 1: {}", cvePor);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	@Test
	public void validarEnTest() {

		int cveEn = 5;

		try {

			doNothing().when(capturarInformacionComplementariaRules).validarEn(anyInt());
			capturarInformacionComplementariaServices.validarEn(cveEn);

			Assert.assertEquals(cveEn, 5);
			logger.info("Clave 2: {}", cveEn);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	@Test
	public void validarHojaTest() {

		int cveHoja = 12;

		try {

			doNothing().when(capturarInformacionComplementariaRules).validarHoja(anyInt());
			capturarInformacionComplementariaServices.validarHoja(cveHoja);

			Assert.assertEquals(cveHoja, 12);
			logger.info("Hoja: {}", cveHoja);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	@Test
	public void validarSubclaveTest() {

		int cveSubclave = 24;

		try {

			doNothing().when(capturarInformacionComplementariaRules).validarSubclave(anyInt());
			capturarInformacionComplementariaServices.validarSubclave(cveSubclave);

			Assert.assertEquals(cveSubclave, 24);
			logger.info("Sub-clave: {}", cveSubclave);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void validarJustificacionTest() {

		String justificacion = "dictaduria";

		try {

			doNothing().when(capturarInformacionComplementariaRules).validarJustificacion(anyString());
			capturarInformacionComplementariaServices.validarJustificacion(justificacion);

			Assert.assertEquals(justificacion, "dictaduria");
			logger.info("Justificación: {}", justificacion);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void obtenerCatalogoSubsistemaTest() {

		List<DatosCatalogo> catalogosSubsistema;
		String cvePeriodo = "201702";

		List<DatosCatalogo> datosCatalogoList = new ArrayList<DatosCatalogo>();
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave("1");

		DatosCatalogo datosCatalogo2 = new DatosCatalogo();
		datosCatalogo2.setClave("123");
		datosCatalogoList.add(datosCatalogo);

		boolean valueM = true;

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerCatalogoSubsistemas())
					.thenReturn(datosCatalogoList);
			Mockito.when(capturarInformacionComplementariaRules.validarMesSubsistemaMarzo(anyString()))
					.thenReturn(valueM);

			catalogosSubsistema = capturarInformacionComplementariaServices.obtenerCatalogoSubsistema(cvePeriodo);
			logger.info("catalogos", catalogosSubsistema.toString());

			logger.info("Primer elemento", catalogosSubsistema.get(0).getClave());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void obtenerCatalogoRCVTest() {
		int cveSubsistema = 23;

		List<DatosCatalogo> datosCatalogoList = new ArrayList<DatosCatalogo>();
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave("2343");
		datosCatalogoList.add(datosCatalogo);

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerCatalogoRCV(anyInt()))
					.thenReturn(datosCatalogoList);
			capturarInformacionComplementariaServices.obtenerCatalogoRCV(cveSubsistema);

			logger.info("Subsistema: {}", cveSubsistema);
			logger.info("Clave catálogo: {}", datosCatalogo.getClave());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void existeInformacionComplementariaTest() {

		int cveSubsistema = 12;
		String cvePresupuestal = "354567";
		String cvePeriodoIMSS = "201502";
		String cvePeriodo = "769";
		long value = 21L;
		boolean ban = true;
		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString()))
					.thenReturn(cvePeriodoIMSS);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.obtenerCountInformacionComplementariaPorSubsistema(anyInt(), anyString(), anyString()))

					.thenReturn(value);
			capturarInformacionComplementariaServices.existeInformacionComplementaria(cveSubsistema, cvePresupuestal,
					cvePeriodoIMSS);

			logger.info("Subsistema: {}", cveSubsistema);
			logger.info("Presupuestal: {}", cvePresupuestal);
			logger.info("Periodo: {}", cvePeriodoIMSS);
			logger.info("Clave periodo: {}", cvePeriodo);
			logger.info("valor: {}", value);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void sePuedeActivarComponentesTest() {

		Boolean req[] = { true, true, false };

		boolean result = capturarInformacionComplementariaServices.sePuedeActivarComponentes(req);
		Assert.assertTrue(result);

		logger.info("Se puede activar: {}", result);

	}

	@Test
	public void consultarSubsistemaRCVTest() {

		DatosInformacionComplementaria datosInfoComplementaria = new DatosInformacionComplementaria();
		String periodo = "201603";
		String cvePresupuestal = "12344421";
		String cvePeriodo = "123";

		SubsistemaRCV value = new SubsistemaRCV();
		value.setClave(45);
		value.setDesSubsistemaRCV("COMPLEMENTOS");		
		
		List<SubsistemaRCVDato> subsistemaRCVDatoList = new ArrayList<SubsistemaRCVDato>();
		SubsistemaRCVDato subsistemaRCVDato = new SubsistemaRCVDato();
		subsistemaRCVDato.setClave(56);
		subsistemaRCVDatoList.add(subsistemaRCVDato);

		InformacionComplementaria infoComple = new InformacionComplementaria();
		infoComple.setCvePresupuestal("32567");

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(periodo);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerSubsistemaRCV(anyInt(), anyInt(),
					anyInt(), anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(value);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerSubsistemaRCVDato(anyInt()))
					.thenReturn(subsistemaRCVDatoList);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.buscarInformacionExistente(anyInt(),
					anyString(), anyString(), anyInt(), anyInt())).thenReturn(infoComple);
			Mockito.when(capturarInformacionComplementariaHelper.armarDatosInformacionComplementaria(
					any(DatosInformacionComplementaria.class), anyList(), anyString(), anyString(),
					any(InformacionComplementaria.class), anyString())).thenReturn(datosInfoComplementaria);
			capturarInformacionComplementariaServices.consultarSubsistemaRCV(datosInfoComplementaria, cvePeriodo,
					cvePresupuestal);

			logger.info("Periodo: {}", periodo);
			logger.info("Presupuestal: {}", cvePresupuestal);
			logger.info("Clave Periodo: {}", cvePeriodo);
			logger.info("Clave: {}", value.getClave());
			logger.info("Descripcion: {}", value.getDesSubsistemaRCV());
			logger.info("valor: {}", infoComple.getCvePresupuestal());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void sePuedeConsultarSubsistemaTest() {

		Boolean catalogosRequeridosVista[] = { true, true, false };
		Boolean catalogosRequeridos[] = { true, true, false };
		boolean ban = true;

		try {

			Mockito.when(capturarInformacionComplementariaRules.sePuedeConsultarSubsistema(any(Boolean[].class),
					any(Boolean[].class))).thenReturn(ban);

			boolean resultado = capturarInformacionComplementariaServices
					.sePuedeConsultarSubsistema(catalogosRequeridosVista, catalogosRequeridos);
			Assert.assertTrue(resultado);

			logger.info("Se puede consultar subsistema: {}", resultado);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void obtenerSubsistemaTest() {
		int subsistema = 78;
		Subsistema subsIstema = new Subsistema();
		subsIstema.setClave(68);

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerSubsistema(anyInt()))
					.thenReturn(subsIstema);
			capturarInformacionComplementariaServices.obtenerSubsistema(subsistema);

			logger.info("Subsistema: {}", subsistema);
			logger.info("Clave: {}", subsIstema.getClave());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void ultimoCatalogoRequeridoTest() {
		Subsistema subsistemaIn = new Subsistema();
		String comboId = "012";
		String[] componentes = { "Último catálogo" };
		boolean we = true;

		List<ComponenteCatalogoInformacionComplementaria> componenteCatalogoInformacionComplementariaList = new ArrayList<ComponenteCatalogoInformacionComplementaria>();
		ComponenteCatalogoInformacionComplementaria componenteCatalogoInformacionComplementaria = new ComponenteCatalogoInformacionComplementaria();
		componenteCatalogoInformacionComplementariaList.add(componenteCatalogoInformacionComplementaria);

		try {
			Mockito.when(capturarInformacionComplementariaHelper
					.generarComponenteCatalogoInfComplementariaList(any(Subsistema.class), any(String[].class)))
					.thenReturn(componenteCatalogoInformacionComplementariaList);
			Mockito.when(capturarInformacionComplementariaRules.ultimoCatalogoRequerido(anyList(), anyString()))
					.thenReturn(we);
			capturarInformacionComplementariaServices.ultimoCatalogoRequerido(subsistemaIn, comboId, componentes);

			logger.info("Combo: {}", comboId);
			logger.info("Componentes: {}", componentes);
			logger.info("verifica: {}", we);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void obtenerCatalogosInformacionComplementariaTest() {
		long clave = 89L;
		int cveRCV = 98;

		List<SubsistemaRCV> subsistemaRCVList = new ArrayList<SubsistemaRCV>();
		SubsistemaRCV subsistemaRCV = new SubsistemaRCV();
		subsistemaRCV.setCveHoja(97);
		subsistemaRCVList.add(subsistemaRCV);

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.obtenerCatalogosInformacionComplementaria(anyLong(), anyInt())).thenReturn(subsistemaRCVList);
			capturarInformacionComplementariaServices.obtenerCatalogosInformacionComplementaria(clave, cveRCV);

			logger.info("Clave: {}", clave);
			logger.info("Clave RCV: {}", cveRCV);
			logger.info("Clave Hoja: {}", subsistemaRCV.getCveHoja());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void armarDatosCatalogoPoblacionTest() {
		List<SubsistemaRCV> subsistemaOn = new ArrayList<SubsistemaRCV>();
		Subsistema subsistema = new Subsistema();
		subsistema.setDescripcion("urbana");

		List<DatosCatalogo> datosCatalogoList = new ArrayList<DatosCatalogo>();
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave("24354");
		datosCatalogoList.add(datosCatalogo);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.armarDatosCatalogoPoblacion(anyList()))
					.thenReturn(datosCatalogoList);
			capturarInformacionComplementariaServices.armarDatosCatalogoPoblacion(subsistemaOn);

			logger.info("Descripción: {}", subsistema.getDescripcion());
			logger.info("Clave: {}", datosCatalogo.getClave());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void armarDatosCatalogoPorTest() {
		List<SubsistemaRCV> subsistemaRCVList = new ArrayList<SubsistemaRCV>();
		SubsistemaRCV subsistemaRCV = new SubsistemaRCV();
		subsistemaRCV.setCvePoblacion(001);
		subsistemaRCVList.add(subsistemaRCV);

		List<DatosCatalogo> datosCatalogoList = new ArrayList<DatosCatalogo>();
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave("24354");
		datosCatalogoList.add(datosCatalogo);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.armarDatosCatalogoPor(anyList()))
					.thenReturn(datosCatalogoList);
			capturarInformacionComplementariaServices.armarDatosCatalogoPor(subsistemaRCVList);

			logger.info("Clave población: {}", subsistemaRCV.getCvePoblacion());
			logger.info("Clave: {}", datosCatalogo.getClave());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void armarDatosCatalogoEnTest() {
		List<SubsistemaRCV> subsistemaRCVList = new ArrayList<SubsistemaRCV>();
		SubsistemaRCV subsistemaRCV = new SubsistemaRCV();
		subsistemaRCV.setCvePoblacion(001);
		subsistemaRCVList.add(subsistemaRCV);

		List<DatosCatalogo> datosCatalogoList = new ArrayList<DatosCatalogo>();
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave("24354");
		datosCatalogoList.add(datosCatalogo);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.armarDatosCatalogoEn(anyList()))
					.thenReturn(datosCatalogoList);
			capturarInformacionComplementariaServices.armarDatosCatalogoEn(subsistemaRCVList);

			logger.info("Clave población: {}", subsistemaRCV.getCvePoblacion());
			logger.info("Clave: {}", datosCatalogo.getClave());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void armarDatosCatalogoHojaTest() {
		List<SubsistemaRCV> subsistemaRCVList = new ArrayList<SubsistemaRCV>();
		SubsistemaRCV subsistemaRCV = new SubsistemaRCV();
		subsistemaRCV.setCvePoblacion(001);
		subsistemaRCVList.add(subsistemaRCV);

		List<DatosCatalogo> datosCatalogoList = new ArrayList<DatosCatalogo>();
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave("24354");
		datosCatalogoList.add(datosCatalogo);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.armarDatosCatalogoHoja(anyList()))
					.thenReturn(datosCatalogoList);
			capturarInformacionComplementariaServices.armarDatosCatalogoHoja(subsistemaRCVList);

			logger.info("Población: {}", subsistemaRCV.getCvePoblacion());
			logger.info("Clave: {}", datosCatalogo.getClave());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void armarDatosCatalogoSubclaveTest() {
		List<SubsistemaRCV> subsistemaRCVList = new ArrayList<SubsistemaRCV>();
		SubsistemaRCV subsistemaRCV = new SubsistemaRCV();
		subsistemaRCV.setCvePoblacion(001);
		subsistemaRCVList.add(subsistemaRCV);

		List<DatosCatalogo> datosCatalogoList = new ArrayList<DatosCatalogo>();
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave("2435ww");
		datosCatalogoList.add(datosCatalogo);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.armarDatosCatalogoSubclave(anyList()))
					.thenReturn(datosCatalogoList);
			capturarInformacionComplementariaServices.armarDatosCatalogoSubclave(subsistemaRCVList);

			logger.info("Clave población: {}", subsistemaRCV.getCvePoblacion());
			logger.info("Clave: {}", datosCatalogo.getClave());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void validarDatoTest() {

		int valorDato = 03;

		try {

			doNothing().when(capturarInformacionComplementariaRules).validarDato(anyInt());
			capturarInformacionComplementariaServices.validarDato(valorDato);

			Assert.assertEquals(valorDato, 03);
			logger.info("Dato: {}", valorDato);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void obtenerFormulasAEjecutarTest() {
		long cveSubsistemaRCV = 21L;

		List<EstructuraDatoInformacionComplementaria> estructuraDatoInformacionComplementariaList = new ArrayList<EstructuraDatoInformacionComplementaria>();
		EstructuraDatoInformacionComplementaria estructuraDatoInformacionComplementaria = new EstructuraDatoInformacionComplementaria();
		estructuraDatoInformacionComplementaria.setValor("10");
		estructuraDatoInformacionComplementaria.setClave(new Integer(1));
		estructuraDatoInformacionComplementariaList.add(estructuraDatoInformacionComplementaria);

		List<FormulaInformacionComplementaria> formulaInformacionComplementariaList = new ArrayList<FormulaInformacionComplementaria>();
		FormulaInformacionComplementaria formulaInformacionComplementaria = new FormulaInformacionComplementaria();
		formulaInformacionComplementaria.setRegla("Puntualidad");
		formulaInformacionComplementariaList.add(formulaInformacionComplementaria);

		List<DatoFormula> datoFormulaList = new ArrayList<DatoFormula>();
		DatoFormula datoFormula = new DatoFormula();
		datoFormula.setClave(34L);
		datoFormulaList.add(datoFormula);

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerDatoFormula(anyLong(), anyInt()))
					.thenReturn(datoFormulaList);
			Mockito.when(capturarInformacionComplementariaHelper.transformarModeloAReglaCompuesta(anyList()))
					.thenReturn(formulaInformacionComplementariaList);

			capturarInformacionComplementariaServices.obtenerFormulasAEjecutar(cveSubsistemaRCV,
					estructuraDatoInformacionComplementariaList);

			logger.info("Clave: {}", estructuraDatoInformacionComplementaria.getValor());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void obtenerSubsistemaRCVTest() {
		DatosInformacionComplementaria datosInformacionComplementaria = new DatosInformacionComplementaria();
		datosInformacionComplementaria.setCveEn(55);
		SubsistemaRCV subsistemaRCV = new SubsistemaRCV();
		subsistemaRCV.setIndCaptura(123);

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerSubsistemaRCV(anyInt(), anyInt(),
					anyInt(), anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(subsistemaRCV);
			capturarInformacionComplementariaServices.obtenerSubsistemaRCV(datosInformacionComplementaria);

			logger.info("Clave: {}", datosInformacionComplementaria.getCveEn());
			logger.info("Captura: {}", subsistemaRCV.getIndCaptura());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void validarCifradeControlTest() {
		DatosInformacionComplementaria datosInformacionComplementaria = new DatosInformacionComplementaria();
		datosInformacionComplementaria.setCvePeriodo("201508");

		try {
			capturarInformacionComplementariaServices.validarCifradeControl(datosInformacionComplementaria);

			logger.info("Clave: {}", datosInformacionComplementaria.getCvePeriodo());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void validarCifradeControlSimpleTest() {
		DatosInformacionComplementaria datosInformacionComplementaria = new DatosInformacionComplementaria();
		datosInformacionComplementaria.setCvePeriodo("201508");

		try {
			capturarInformacionComplementariaServices.validarCifradeControlSimple(datosInformacionComplementaria);

			logger.info("Clave: {}", datosInformacionComplementaria.getCvePeriodo());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void guardarInfoComplementariaPorJustificacionTest() {
		boolean existeInformacionComplementaria = true;
		DatosUsuario datosUsuario = new DatosUsuario();
		DatosInformacionComplementaria datosInformacionComplementaria = new DatosInformacionComplementaria();
		String cvePeriodoAux = "34rf56";
		String periodo = "4r76tgy";

		List<InformacionComplementaria> informacionComplementariaList = new ArrayList<InformacionComplementaria>();
		InformacionComplementaria informacionComplementaria = new InformacionComplementaria();
		informacionComplementariaList.add(informacionComplementaria);

		CierreSubsistema cierreSubsistema = new CierreSubsistema();
		cierreSubsistema.setCveCapturista("213213er");
		cierreSubsistema.setClavePresupuestal("434423");
		cierreSubsistema.setIndicadorCerrado(1233);
		cierreSubsistema.setCveSubsistema(12);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvePeriodoAux);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.consultarInformacionComplementariaPorSubsistema(anyString(), anyString(), anyInt()))
					.thenReturn(informacionComplementariaList);
			doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl)
					.eliminarInformacionComplementariaDato(anyInt(), anyString(), anyString());
			doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl)
					.eliminarInformacionComplementaria(anyInt(), anyString(), anyString());
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.buscarCierreSubsistemaExistente(anyString(), anyString(), anyInt())).thenReturn(cierreSubsistema);
			Mockito.when(capturarInformacionComplementariaHelper.prepararActualizarCierreSubsistema(
					any(CierreSubsistema.class), anyString(), any(DatosInformacionComplementaria.class)))
					.thenReturn(cierreSubsistema);
			doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl)
					.actualizarCierreSubsistema(any(CierreSubsistema.class));
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(periodo);
			Mockito.when(capturarInformacionComplementariaHelper.prepararCierreSubsistema(any(DatosUsuario.class),
					any(DatosInformacionComplementaria.class))).thenReturn(cierreSubsistema);
			doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl)
					.guardarCierreSubsistema(any(CierreSubsistema.class));
			capturarInformacionComplementariaServices.guardarInfoComplementariaPorJustificacion(
					existeInformacionComplementaria, datosUsuario, datosInformacionComplementaria);

			logger.info("Periodo Auxiliar: {}", cvePeriodoAux);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Test
	public void cargarSubsistemasTest() {
		List<DatosCatalogo> catalogoSubsistemaList = new ArrayList<DatosCatalogo>();
		DatosCatalogo datosCatalogo = new DatosCatalogo();
		datosCatalogo.setClave("34rt56");
		catalogoSubsistemaList.add(datosCatalogo);

		DatosInformacionComplementaria datosInformacionComplementaria = new DatosInformacionComplementaria();
		datosInformacionComplementaria.setCvePeriodo("201602");
		String cvePresupuestal = "4763846238";

		List<CierreSubsistema> subsistemasCerradosList = new ArrayList<CierreSubsistema>();
		CierreSubsistema cierreSubsistema = new CierreSubsistema();
		cierreSubsistema.setClavePresupuestal("4537453483");
		subsistemasCerradosList.add(cierreSubsistema);

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerSubsistemasCerrados(anyString(),
					anyString())).thenReturn(subsistemasCerradosList);
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString()))
					.thenReturn(cvePresupuestal);

			Map<String, Boolean> mapaSubsistemas = capturarInformacionComplementariaServices
					.cargarSubsistemas(catalogoSubsistemaList, datosInformacionComplementaria, cvePresupuestal);

			logger.info("Clave: {}", datosCatalogo.getClave());
			logger.info("Periodo: {}", datosInformacionComplementaria.getCvePeriodo());
			logger.info("Presupuestal: {}", cierreSubsistema.getClavePresupuestal());

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void informacionComplementariaConsultaTest() {
		DatosUsuario datosUsuario = new DatosUsuario();

		DatosInformacionComplementaria datosInformacionComplementaria = new DatosInformacionComplementaria();

		String cvePeriodoAux = "38389rgru";

		List<InformacionComplementariaConsulta> informacionComplementariaConsultaList = new ArrayList<InformacionComplementariaConsulta>();
		InformacionComplementariaConsulta infoConsulta = new InformacionComplementariaConsulta();
		infoConsulta.setCveRCV("3rfrt4");
		informacionComplementariaConsultaList.add(infoConsulta);

		List<InformacionComplementaria> informacionComplementariaList = new ArrayList<InformacionComplementaria>();
		InformacionComplementaria informacionComplementaria = new InformacionComplementaria();
		informacionComplementaria.setCveCapturistaActualiza("brnr213");
		informacionComplementariaList.add(informacionComplementaria);

		List<InformacionComplementariaDato> informacionComplementariaDatoList = new ArrayList<InformacionComplementariaDato>();
		InformacionComplementariaDato informacionComplementariaDato = new InformacionComplementariaDato();
		informacionComplementariaDatoList.add(informacionComplementariaDato);

		InformacionComplementariaConsulta informacionComplementariaConsulta = new InformacionComplementariaConsulta();

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvePeriodoAux);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.consultarInformacionComplementariaPorSubsistemaAsc(anyString(), anyString(), anyInt()))
					.thenReturn(informacionComplementariaList);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.consultarInformacionComplementariaDatoPorSubsistemaRCV(anyString(), anyString(), anyInt()))
					.thenReturn(informacionComplementariaDatoList);
			Mockito.when(capturarInformacionComplementariaHelper
					.armarDatosInformacionAConsulta(any(InformacionComplementaria.class), anyList(), anyList()))
					.thenReturn(informacionComplementariaConsulta);
			Mockito.when(capturarInformacionComplementariaHelper.prepararDatosConsultaComp(anyList()))
					.thenReturn(informacionComplementariaConsultaList);
			capturarInformacionComplementariaServices.informacionComplementariaConsulta(datosUsuario,
					datosInformacionComplementaria);

			logger.info("Periodo Auxiliar: {}", cvePeriodoAux);
			logger.info("RCV: {}", infoConsulta.getCveRCV());
			logger.info("Capturista: {}", informacionComplementaria.getCveCapturistaActualiza());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void validarCapturaCifra99Test() {

		int cveSubsistema = 23;
		String cvePresupuestal = "365434538";
		String cvePeriodoImss = "201805";
		String cvPeriodo = "343";

		SubsistemaRCV subsistemaRCV = new SubsistemaRCV();
		subsistemaRCV.setCveSubsistema(28);

		Long totalRCV = 23L;

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvPeriodo);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerCantidadRCVParaCifra99(anyInt()))
					.thenReturn(subsistemaRCV);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.obtenerTotalRCVCapturadosPorSubsistema(anyInt(), anyString(), anyString())).thenReturn(totalRCV);
			capturarInformacionComplementariaServices.validarCapturaCifra99(cveSubsistema, cvePresupuestal,
					cvePeriodoImss);

			logger.info("Subsistema: {}", cveSubsistema);
			logger.info("Presupuestal:{}", cvePresupuestal);
			logger.info("Periodo:{}", cvePeriodoImss);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// @Test
	// public void actualizarInformacionComplementariaTest() {
	//
	// InformacionComplementaria informacionExistente = new
	// InformacionComplementaria();
	//
	// DatosUsuario datosUsuario = new DatosUsuario();
	// datosUsuario.setUsuario("Juan");
	//
	// DatosInformacionComplementaria datosInformacionComplementaria = new
	// DatosInformacionComplementaria();
	// // datosInformacionComplementaria.setCifraDeControl(56L);
	//
	// List<InformacionComplementariaDato> informacionComplementariaDatoList =
	// new ArrayList<InformacionComplementariaDato>();
	// InformacionComplementariaDato informacionComplementariaDato = new
	// InformacionComplementariaDato();
	// informacionComplementariaDato.setCvePresupuestal("34434535");
	// informacionComplementariaDatoList.add(informacionComplementariaDato);
	//
	// List<EstructuraDatoInformacionComplementaria>
	// estructuraDatoInformacionComplementariaList = new
	// ArrayList<EstructuraDatoInformacionComplementaria>();
	// EstructuraDatoInformacionComplementaria
	// estructuraDatoInformacionComplementaria = new
	// EstructuraDatoInformacionComplementaria();
	// estructuraDatoInformacionComplementaria.setValor("eres");
	// estructuraDatoInformacionComplementariaList.add(estructuraDatoInformacionComplementaria);
	//
	// try {
	//
	// Mockito.when(capturarInformacionComplementariaHelper
	// .actualizarInformacionComplementaria(any(InformacionComplementaria.class),
	// anyString(), anyLong()))
	// .thenReturn(informacionExistente);
	//
	// doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl)
	// .actualizarInformacionComplementaria(any(InformacionComplementaria.class));
	//
	// Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
	// .obtenerDatosInformacionComplementaria(any(InformacionComplementaria.class)))
	// .thenReturn(informacionComplementariaDatoList);
	// Mockito.when(capturarInformacionComplementariaHelper.prepararDatosActualizados(anyList(),
	// anyList()))
	// .thenReturn(informacionComplementariaDatoList);
	//
	// doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl)
	// .actualizarDatosInformacionComplementaria(anyList());
	//
	// capturarInformacionComplementariaServices.actualizarInformacionComplementaria(informacionExistente,
	// datosUsuario, datosInformacionComplementaria);
	//
	// logger.info("Subsistema: {}", informacionExistente);
	// logger.info("Presupuestal:{}", datosUsuario);
	// logger.info("Periodo:{}", datosInformacionComplementaria);
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	//
	// }

	@Test
	public void validarCifraControl99Test() {

		int cveSubsistema = 12;
		Long cifraControl99 = 13L;
		String cvePresupuestal = "47348584";
		String cvePeriodo = "234";

		String cvePeriodoAux = "201807";

		List<InformacionComplementaria> informacionComplementariaList = new ArrayList<InformacionComplementaria>();
		InformacionComplementaria informacionComplementaria = new InformacionComplementaria();
		informacionComplementaria.setCvePresupuestal("35455");
		informacionComplementariaList.add(informacionComplementaria);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvePeriodoAux);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerCifraControlPorSubsistema(anyInt(),
					anyString(), anyString())).thenReturn(informacionComplementariaList);
			capturarInformacionComplementariaRules.validarCifraControl99(anyList(), anyLong(), anyInt());

			logger.info("Subsistema: {}", cveSubsistema);
			logger.info("Cifra control: {}", cifraControl99);
			logger.info("Presupuestal:{}", cvePresupuestal);
			logger.info("Clave Periodo:{}", cvePeriodo);
			logger.info("Periodo:{}", cvePeriodoAux);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void descomponerCadenaFocoParaErrorCifraCOntrolTest() {
		String componenteCifraControl99 = "ER34E";

		try {

			Mockito.when(
					capturarInformacionComplementariaHelper.descomponerCadenaFocoParaErrorCifraCOntrol(anyString()))
					.thenReturn(componenteCifraControl99);
			capturarInformacionComplementariaServices
					.descomponerCadenaFocoParaErrorCifraCOntrol(componenteCifraControl99);

			logger.info("Usuario: {}", componenteCifraControl99);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void abrirSubsistemaTest() {

		String cvePresupuestal = "35345345";
		String cvePeriodo = "234";
		int cveSubsistema = 123;
		String usuarioActualiza = "Jimena";

		try {
			capturarInformacionComplementariaServices.abrirSubsistema(anyString(), anyString(), anyInt(), anyString());

			logger.info("Presupuestal: {}", cvePresupuestal);
			logger.info("Periodo: {}", cvePeriodo);
			logger.info("Subsistema: {}", cveSubsistema);
			logger.info("Usuario: {}", usuarioActualiza);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Test
	public void actualizarCifraControlInformacionComplementariaPorCierreTest() {

		InformacionComplementaria informacionComplementariaPorCierre = new InformacionComplementaria();
		informacionComplementariaPorCierre.setCveCapturistaActualiza("Luis12");

		try {
			capturarInformacionComplementariaServices
					.actualizarCifraControlInformacionComplementariaPorCierre(informacionComplementariaPorCierre);
			capturarInformacionComplementariaRepositoryJDBCImpl
					.actualizarCifraControlInformacionComplementariaDatoPorCierre(informacionComplementariaPorCierre);

			logger.info("Presupuestal: {}", informacionComplementariaPorCierre.getCveCapturistaActualiza());

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Test
	public void buscarInformacionExistenteTest() {
		int cveSubsistemaRcv = 45;

		DatosUsuario datosUsuario = new DatosUsuario();
		datosUsuario.setDesDelegacionAct("Cuahutemoc");

		DatosInformacionComplementaria datosInformacionComplementaria = new DatosInformacionComplementaria();
		datosInformacionComplementaria.setCvePeriodo("erte");
		datosInformacionComplementaria.setCveSubsistema(2);
		datosInformacionComplementaria.setCveRCV(4);

		InformacionComplementaria informacionComplementaria = new InformacionComplementaria();
		informacionComplementaria.setCveCapturista("AS23");

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.buscarInformacionExistente(anyInt(),
					anyString(), anyString(), anyInt(), anyInt())).thenReturn(informacionComplementaria);
			capturarInformacionComplementariaServices.buscarInformacionExistente(cveSubsistemaRcv, datosUsuario,
					datosInformacionComplementaria);

			logger.info("Subsistema: {}", cveSubsistemaRcv);
			logger.info("Delegación: {}", datosUsuario.getDesDelegacionAct());
			logger.info("Capturista: {}", informacionComplementaria.getCveCapturista());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void controlMapaCierreTest() {

		String cvePresupuestal = "3535235";
		String cvePeriodo = "213";
		int cveSubsistema = 45;
		String usuarioActualiza = "Robert";

		String cvePeriodoAux = "24";

		InformacionComplementaria informacionComplementariaPorCierre = new InformacionComplementaria();

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvePeriodoAux);
			doNothing().when(capturarInformacionComplementariaServicesMock).abrirSubsistema(anyString(), anyString(),
					anyInt(), anyString());
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.buscarInformacionComplementariaPorCierre(anyString(), anyString(), anyInt()))
					.thenReturn(informacionComplementariaPorCierre);
			doNothing().when(capturarInformacionComplementariaServicesMock)
					.actualizarCifraControlInformacionComplementariaPorCierre(any(InformacionComplementaria.class));
			capturarInformacionComplementariaServices.controlMapaCierre(cvePresupuestal, cvePeriodoAux, cveSubsistema,
					usuarioActualiza);

			logger.info("Presupuestal: {}", cvePresupuestal);
			logger.info("Periodo: {}", cvePeriodo);
			logger.info("Subsistema: {}", cveSubsistema);
			logger.info("Usuario Actualiza: {}", usuarioActualiza);
			logger.info("Periodo Auxiliar : {}", cvePeriodoAux);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void validarCierrePeriodoTest() {
		String cvePeriodo = "34cd";
		String cvePresupuestal = "34r67";
		String periodoOrdenado = "201708";
		int totalSubsistemasCerrados = 4;
		long valueLo = 34L;
		int mesPeriodo = 2;
		int referenciaParametro = 12;

		Parametro parametro = new Parametro();
		parametro.setDescripcionParametro("Intermedio");

		try {

			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerParamtro(anyString()))
					.thenReturn(parametro);
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString()))
					.thenReturn(periodoOrdenado);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.obtenerTotalSubsistemasCerradosPorPresupuestal(anyString(), anyString())).thenReturn(valueLo);
			Mockito.when(capturarInformacionComplementariaRules.sePuedeCerrarPeriodo(anyInt(), anyInt()));

			logger.info("Parámetro: {}", parametro.getDescripcionParametro());
			logger.info("Referencia: {}", referenciaParametro);
			logger.info("Mes: {}", mesPeriodo);
			logger.info("valor: {}", valueLo);
			logger.info("Subistema cerrado: {}", totalSubsistemasCerrados);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void cerrarPeriodoTest() {

		String clavePresupuestal = "784874619";
		int tipoCaptura = 21;
		String clavePeriodoImss = "er2334";
		String cvePeriodoAux = "201602";
		String periodoSiguienteStr = "201703";
		boolean isIndicadorMonitoreo = false;

		PeriodosImss periodosImss = new PeriodosImss();
		periodosImss.setDescripcionMesPeriodo("Marzo");

		PeriodoOperacion periodoOperacion = new PeriodoOperacion();
		periodoOperacion.setTipoCaptura(4);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvePeriodoAux);
			doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl).cerrarPeriodo(anyString(), anyInt(),
					anyString());
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerSiguientePeriodoImss(anyString()))
					.thenReturn(periodosImss);
			// Mockito.when(capturarInformacionComplementariaServicesMock)
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.buscarPeriodoOperacionPorClavePeriodo(anyString(), anyString(), anyInt()))
					.thenReturn(periodoOperacion);
			doNothing().when(capturarInformacionComplementariaServicesMock)
					.generarPeriodoOperacionSiguiente(anyString(), anyInt(), anyString());
			
			capturarInformacionComplementariaServices.cerrarPeriodo(clavePresupuestal, tipoCaptura, clavePeriodoImss, isIndicadorMonitoreo);
						
			logger.info("Periodo Imss: {}", clavePeriodoImss);
			logger.info("Tipo Captura: {}", tipoCaptura);
			logger.info("Presupuestal: {}", clavePresupuestal);
			logger.info("Periodo Auxiliar: {}", cvePeriodoAux);
			logger.info("Periodo siguiente: {}", periodoSiguienteStr);
			logger.info("Mes: {}", periodosImss.getDescripcionMesPeriodo());
			logger.info("Indicador Monitoreo: {}", isIndicadorMonitoreo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void abrirPeriodoTest() {

		String clavePresupuestal = "3768462";
		int tipoCaptura = 98;
		String clavePeriodoImss = "67";
		String cvePeriodoAux = "201805";

		PeriodoOperacion periodoOperacion = new PeriodoOperacion();
		periodoOperacion.setClavePresupuestal("23455");
		periodoOperacion.setTipoCaptura(3);
		periodoOperacion.setClavePeriodoIMSS("123");
		;

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.buscarPeriodoActual(anyString(), anyInt()))
					.thenReturn(periodoOperacion);
			doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl).cerrarPeriodo(anyString(), anyInt(),
					anyString());
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvePeriodoAux);
			doNothing().when(capturarInformacionComplementariaRepositoryJDBCImpl)
					.actualizarPeriodoOperacionSiguienteActual(anyString(), anyInt(), anyString());
			capturarInformacionComplementariaServices.abrirPeriodo(clavePresupuestal, tipoCaptura, clavePeriodoImss);

			logger.info("Presupuestal: {}", clavePresupuestal);
			logger.info("Captura: {}", tipoCaptura);
			logger.info("Periodo: {}", clavePeriodoImss);
			logger.info("Periodo Auxiliar: {}", cvePeriodoAux);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void estadoPeriodoOperacionTest() {
		String cvePeriodo = "201508";
		String cvePresupuestal = "3555449";
		int tipoCaptura = 45;
		String cvePeriodoAux = "201812";

		PeriodoOperacion periodoOperacion = new PeriodoOperacion();
		periodoOperacion.setClavePresupuestal("23455");

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvePeriodo);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.buscarPeriodoOperacionPorClavePeriodo(anyString(), anyString(), anyInt()))
					.thenReturn(periodoOperacion);
			capturarInformacionComplementariaServices.estadoPeriodoOperacion(cvePeriodoAux, cvePresupuestal,
					tipoCaptura);
			logger.info("Periodo: {}", cvePeriodo);
			logger.info("Presupuestal: {}", cvePresupuestal);
			logger.info("Tipo captura: {}", tipoCaptura);
			logger.info("Periodo Auxiliar: {}", cvePeriodoAux);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void generarPeriodoOperacionSiguienteTest() {

		String cvePresupuestal = "476462384";
		int cveTipoCaptura = 78;
		String cvePeriodoIMSS = "890";

		try {
			capturarInformacionComplementariaServices.generarPeriodoOperacionSiguiente(anyString(), anyInt(),
					anyString());

			logger.info("Presupuestal: {}", cvePresupuestal);
			logger.info("Captura: {}", cveTipoCaptura);
			logger.info("Periodo: {}", cvePeriodoIMSS);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void obtenerSiguientePeriodoImssTest() {
		String cvePeriodo = "201306";

		PeriodosImss periodosImss = new PeriodosImss();
		periodosImss.setDescripcionMesPeriodo("Febrero");

		try {
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerSiguientePeriodoImss(anyString()))
					.thenReturn(periodosImss);
			capturarInformacionComplementariaServices.obtenerSiguientePeriodoImss(cvePeriodo);

			logger.info("Captura: {}", cvePeriodo);
			logger.info("Periodo: {}", periodosImss.getDescripcionMesPeriodo());

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Test
	public void reemplazarMensajeConfirmTest() {
		String mensaje = "En proceso";
		String subsistema = "hospitalización";

		try {
			Mockito.when(
					capturarInformacionComplementariaHelper.reemplazarMensajeJustificacion(anyString(), anyString()))
					.thenReturn(subsistema);
			capturarInformacionComplementariaServices.reemplazarMensajeConfirm(mensaje, subsistema);

			logger.info("Captura: {}", mensaje);
			logger.info("Periodo: {}", subsistema);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	@Test
	public void datosIgualACeroTest(){
		boolean datosIgualACero = Boolean.TRUE;
		List<EstructuraDatoInformacionComplementaria> datos = new ArrayList<>();
		EstructuraDatoInformacionComplementaria dato1 = new EstructuraDatoInformacionComplementaria();
		dato1.setClave(1);
		dato1.setValor("0");
		datos.add(dato1);
		EstructuraDatoInformacionComplementaria dato2 = new EstructuraDatoInformacionComplementaria();
		dato2.setClave(2);
		dato2.setValor("0");
		datos.add(dato2);
		EstructuraDatoInformacionComplementaria dato3 = new EstructuraDatoInformacionComplementaria();
		dato3.setClave(3);
		dato3.setValor("0");
		datos.add(dato3);
		EstructuraDatoInformacionComplementaria dato4 = new EstructuraDatoInformacionComplementaria();
		dato4.setClave(4);
		dato4.setValor("0");
		datos.add(dato4);
		EstructuraDatoInformacionComplementaria dato5 = new EstructuraDatoInformacionComplementaria();
		dato5.setClave(5);
		dato5.setValor("0");
		datos.add(dato5);

		try {			
			Mockito.when(
					capturarInformacionComplementariaRules.datosIgualaCero(anyList()))
				.thenReturn(datosIgualACero);
			boolean resultTest = capturarInformacionComplementariaServices.datosIgualACero(datos);
			
			Assert.assertEquals(Boolean.TRUE, resultTest);
			
			logger.info("DatosIgualACeroTest");
			logger.info("Datos: {}", datos);
			logger.info("Resultado: {}", resultTest);

		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Test
	public void estadoPeriodoOperacionMonitoreoTest() {
		String cvePeriodo = "052022";
		String cvePresupuestal = "3555001449";
		int tipoCaptura = 3;
		String cvePeriodoAux = "202205";

		PeriodoOperacion periodoOperacion = new PeriodoOperacion();
		periodoOperacion.setClavePresupuestal(cvePresupuestal);

		try {
			Mockito.when(capturarInformacionComplementariaHelper.ordenarPeriodo(anyString())).thenReturn(cvePeriodoAux);
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl
					.buscarPeriodoOperacionPorClavePeriodo(anyString(), anyString(), anyInt()))
					.thenReturn(periodoOperacion);
			boolean resultTest = capturarInformacionComplementariaServices.estadoPeriodoOperacionMonitoreo(cvePeriodo, cvePresupuestal,
					tipoCaptura);
			
			Assert.assertEquals(Boolean.FALSE, resultTest);

			logger.info("Periodo: {}", cvePeriodo);
			logger.info("Presupuestal: {}", cvePresupuestal);
			logger.info("Tipo captura: {}", tipoCaptura);
			logger.info("Periodo Auxiliar: {}", cvePeriodoAux);
			logger.info("Resultado: {}", resultTest);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	@Test
	public void tienePeriodosMonitoreoTest() {
		Long periodosCantidad = 1L;
		String cvePresupuestal = "089346782913";
		int tipoCaptura = 3;
		try {		
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.obtenerCountPeriodosMonitoreo(anyString(), anyInt()))
				.thenReturn(periodosCantidad);
			
			boolean resultTest = capturarInformacionComplementariaServices.tienePeriodosMonitoreo(cvePresupuestal,
					tipoCaptura);
			
			Assert.assertEquals(Boolean.TRUE, resultTest);
			
			logger.info("TienePeriodosMonitoreoTest");
			logger.info("Periodos: {}", periodosCantidad);
			logger.info("Resultado: {}", resultTest);

		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	@Test
	public void obtenerPeriodoActualTest() {
		String periodoOrdenado = "202207";
		PeriodoOperacion periodoOperacion = new PeriodoOperacion();
		periodoOperacion.setClavePeriodoIMSS(periodoOrdenado);
		
		String cvePresupuestal = "089346782913";
		int tipoCaptura = 3;
		
		String periodoDesordenado = "072022";
		try {			
			Mockito.when(capturarInformacionComplementariaRepositoryJDBCImpl.buscarPeriodoActual(anyString(),
					anyInt())).thenReturn(periodoOperacion);
			Mockito.when(capturarInformacionComplementariaHelper.desordenarPeriodo(anyString())).
					thenReturn(periodoDesordenado);
			Mockito.when(capturarInformacionComplementariaServices.obtenerPeriodoActual(anyString(),
					anyInt())).thenReturn(periodoDesordenado);
			
			String periodo = capturarInformacionComplementariaServices.obtenerPeriodoActual(cvePresupuestal, tipoCaptura);
			
			logger.info("ObtenerPeriodoActualTest");
			logger.info("Periodo: {}", periodo);

		} catch (Exception e) {
			e.printStackTrace();
		}	

	}
}
