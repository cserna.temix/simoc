package mx.gob.imss.simo.enlaces;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.enlaces.informacioncomplementaria.rules.CapturarInformacionComplementariaRules;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.services.impl.CapturarInformacionComplementariaServicesImpl;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.EstructuraDatoInformacionComplementaria;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CapturarInformacionComplementariaServicesImpl.class)
public class InformacionComplementariaTest {
	
	final static Logger logger = LoggerFactory.getLogger(CapturarInformacionComplementariaServicesImpl.class);

	@InjectMocks
    private CapturarInformacionComplementariaServicesImpl capturarInformacionComplementariaServices;
	
	@Mock
	private CapturarInformacionComplementariaRules capturarInformacionComplementariaRules;
	
    @BeforeClass
    public static void init() {

        BasicConfigurator.configure();
    }

    @Before
    public void setup() {
    	capturarInformacionComplementariaServices = PowerMockito.spy(new CapturarInformacionComplementariaServicesImpl());
    	MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void validarCifraDeControlTest() {

    	logger.info("Test validarCifraDeControlTest");
        Long cifraDeControl = new Long(10);
        Integer cveRCV = 100;
        List<EstructuraDatoInformacionComplementaria> datosList = new ArrayList<EstructuraDatoInformacionComplementaria>();
        for (int i = 0; i < 8; i++) {
            EstructuraDatoInformacionComplementaria estructura = new EstructuraDatoInformacionComplementaria();
            estructura.setValor("" + i);
            datosList.add(estructura);
        }
        DatosInformacionComplementaria datos = new DatosInformacionComplementaria();
        datos.setCifraDeControl(cifraDeControl);
        datos.setCveRCV(cveRCV);
        datos.setDatos(datosList);

        try {
        	doNothing().when(capturarInformacionComplementariaRules).validarCifradeControl(datos);
        	capturarInformacionComplementariaServices.validarCifradeControl(datos);
            logger.info("M�todo ejecutado correctamente: {}");
        } catch (HospitalizacionException e) {
            e.printStackTrace();
        }
    }

}
