package mx.gob.imss.simo.enlaces.serviciosSubrogados.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogado;
import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogar;
import mx.gob.imss.simo.enlaces.serviciossubrogados.controller.CapturarServiciosSubrogadosController;
import mx.gob.imss.simo.enlaces.serviciossubrogados.helper.CapturarServiciosSubrogadosHelper;
import mx.gob.imss.simo.enlaces.serviciossubrogados.repository.CapturarServiciosSubrogadosRepository;
import mx.gob.imss.simo.enlaces.serviciossubrogados.repository.impl.CapturarServiciosSubrogadosRepositoryJDBCImpl;
import mx.gob.imss.simo.enlaces.serviciossubrogados.rules.CapturarServiciosSubrogadosRules;
import mx.gob.imss.simo.enlaces.serviciossubrogados.services.impl.CapturarServiciosSubrogadosServicesImpl;
import mx.gob.imss.simo.hospitalizacion.busquedas.rules.AccederRules;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.model.DatosUsuario;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CapturarServiciosSubrogadosServicesImpl.class)
public class CapturarServiciosSubrogadosServiceImplTest {

	@InjectMocks
	CapturarServiciosSubrogadosServicesImpl capturarServiciosSubrogadosService;

	@Mock
	CapturarServiciosSubrogadosRules capturarServiciosSubrogadosRules;

	@Mock
	HospitalizacionCommonHelper hospitalizacionCommonHelper;

	@Mock
	CapturarServiciosSubrogadosHelper capturarServiciosSubrogadosHelper;

	@Mock
	CapturarServiciosSubrogadosRepositoryJDBCImpl capturarServiciosSubrogadosRepositoryJDBCImpl;

	@Mock
	CapturarServiciosSubrogadosRepository capturarServiciosSubrogadosRepository;

	@Mock
	CapturarServiciosSubrogadosController capturarServiciosSubrogadosController;

	@Mock
	AccederRules accederRules;

	final static Logger logger = LoggerFactory.getLogger(CapturarServiciosSubrogadosServiceImplTest.class);

	@BeforeClass
	public static void ini() {

		BasicConfigurator.configure();
	}

	@Before
	public void setup() {

		capturarServiciosSubrogadosService = new CapturarServiciosSubrogadosServicesImpl();
		capturarServiciosSubrogadosRules = new CapturarServiciosSubrogadosRules();
		capturarServiciosSubrogadosHelper = new CapturarServiciosSubrogadosHelper();
		capturarServiciosSubrogadosRepositoryJDBCImpl = new CapturarServiciosSubrogadosRepositoryJDBCImpl();
		capturarServiciosSubrogadosController = new CapturarServiciosSubrogadosController();

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void validarNssTest() {

		String nssIn = "82738495876";

		try {
			doNothing().when(capturarServiciosSubrogadosRules).validarNss(anyString());
			capturarServiciosSubrogadosService.validarNss(nssIn);

			Assert.assertEquals(nssIn, "82738495876");
			logger.info("Nss: {}", nssIn);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void validarEdadTest() {

		String edad = "27";

		try {
			doNothing().when(capturarServiciosSubrogadosRules).validarEdad(anyString());
			capturarServiciosSubrogadosService.validarEdad(edad);

			Assert.assertEquals(edad, "27");
			logger.info("Edad: {}", edad);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void validarNombreTest() {

		String nombre = "Eulalia";

		try {
			doNothing().when(capturarServiciosSubrogadosRules).validarNombre(anyString());
			capturarServiciosSubrogadosService.validarNombre(nombre);

			Assert.assertEquals(nombre, "Eulalia");
			logger.info("Nombre: {}", nombre);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void validarApellidoPaternoTest() {

		String apePaterno = "Renteral";

		try {
			doNothing().when(capturarServiciosSubrogadosRules).validarApellidoPaterno(anyString());
			capturarServiciosSubrogadosService.validarApellidoPaterno(apePaterno);

			Assert.assertEquals(apePaterno, "Renteral");
			logger.info("Apellido Paterno: {}", apePaterno);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void validarNumeroTest() {
		String validaNum = "12";

		try {
			doNothing().when(capturarServiciosSubrogadosRules).validarNumero(anyString());
			capturarServiciosSubrogadosService.validarNumero(validaNum);

			Assert.assertEquals(validaNum, "12");
			logger.info("N�mero: {}", validaNum);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void validarTipoContratoTest() {

		String tipoContrato = "P�blico";

		try {
			doNothing().when(capturarServiciosSubrogadosRules).validarTipoContrato(anyString());
			capturarServiciosSubrogadosService.validarTipoContrato(tipoContrato);

			Assert.assertEquals(tipoContrato, "P�blico");
			logger.info("Tipo contrato: {}", tipoContrato);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void validarFolioContratoTest() {
		String folioContrato = "13123ER334ED";

		try {
			doNothing().when(capturarServiciosSubrogadosRules).validarFolioContrato(anyString());
			capturarServiciosSubrogadosService.validarFolioContrato(folioContrato);

			Assert.assertEquals(folioContrato, "13123ER334ED");
			logger.info("Folio contrato: {}", folioContrato);

		} catch (HospitalizacionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void validarTipoServicioTest() {

		String tipoServicio = "Privado";

		try {

			doNothing().when(capturarServiciosSubrogadosRules).validarTipoServicio(anyString());
			capturarServiciosSubrogadosService.validarTipoServicio(tipoServicio);

			Assert.assertEquals(tipoServicio, "Privado");
			logger.info("Tipo servicio: {}", tipoServicio);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void validarMotivoSubrogacionTest() {
		String motivoSubrogacion = "pruebaSubrogacion";

		try {
			doNothing().when(capturarServiciosSubrogadosController).validarMotivoSubrogacion();
			doNothing().when(capturarServiciosSubrogadosRules).validarMotivoSubrogacion(motivoSubrogacion);
			capturarServiciosSubrogadosService.validarMotivoSubrogacion(motivoSubrogacion);

			Assert.assertEquals(motivoSubrogacion, "pruebaSubrogacion");
			logger.info("Motivo subrogaci�n: {}", motivoSubrogacion);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void validarServiciosSubrogarTest() {
		String motivoSubrogacion = "pruebaServicios";
		try {
			doNothing().when(capturarServiciosSubrogadosController).validarServiciosSubrogar();
			doNothing().when(capturarServiciosSubrogadosRules).validarServiciosSubrogar(motivoSubrogacion);
			capturarServiciosSubrogadosService.validarServiciosSubrogar(motivoSubrogacion);

			Assert.assertEquals(motivoSubrogacion, "pruebaServicios");
			logger.info("Servicio a subrogar: {}", motivoSubrogacion);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void validarCantidadEventosTest() {
		String cantidadEventos = "5";
		try {
			doNothing().when(capturarServiciosSubrogadosController).validarCantidadEventos();
			doNothing().when(capturarServiciosSubrogadosRules).validarCantidadEventos(cantidadEventos);
			capturarServiciosSubrogadosService.validarCantidadEventos(cantidadEventos);

			Assert.assertEquals(cantidadEventos, "5");
			logger.info("Cantidad eventos: {}", cantidadEventos);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void validarCostoUnitarioTest() {
		String costoUnitario = "123456.50";
		try {
			capturarServiciosSubrogadosController.validarCostoUnitario();
			capturarServiciosSubrogadosRules.validarCostoUnitario(costoUnitario);
			capturarServiciosSubrogadosService.validarCostoUnitario(costoUnitario);

			Assert.assertEquals(costoUnitario, "123456.50");
			logger.info("Costo unitario: {}", costoUnitario);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void validarFechaServicioTest() {
		String fechaServicio = "10/10/2018";
		String clavePresupuestal = "030103022151";
		boolean ban2 = Boolean.TRUE;
		PeriodoOperacion periodoOperacion = new PeriodoOperacion();
		periodoOperacion.setClavePresupuestal("030103022151");
		periodoOperacion.setTipoCaptura(1);
		periodoOperacion.setClavePeriodoIMSS("201710");
		periodoOperacion.setIndicadorCerrado(Boolean.FALSE);
		periodoOperacion.setIndicadorActual(Boolean.TRUE);

		try {
			doNothing().when(capturarServiciosSubrogadosRules).validarFechaServicio(fechaServicio);
			doNothing().when(capturarServiciosSubrogadosRules).validarFormatoFecha(fechaServicio);
			doNothing().when(capturarServiciosSubrogadosRules).validarFechaActual(fechaServicio);
			Mockito.when(
					capturarServiciosSubrogadosRepository.obtenerPeriodoOperacion(anyString(), anyInt(), anyString()))
					.thenReturn(periodoOperacion);
			String dato = capturarServiciosSubrogadosService.validarFechaServicio(fechaServicio, clavePresupuestal);

			Assert.assertNotNull(dato);
			logger.info("Resultado: {}", dato);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test
	public void habilitarEdadTest() {

		boolean ban = Boolean.TRUE;
		try {
			Mockito.when(capturarServiciosSubrogadosRules.habilitarEdad(anyString(), anyString())).thenReturn(ban);

			Assert.assertEquals(ban, true);
			logger.info("Resultado: {}", ban);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void calcularEdadPacienteNoEncontradoTest() {

		Map<TipoCalculoEdadEnum, Integer> calcular = new HashMap<TipoCalculoEdadEnum, Integer>();
		calcular.put(TipoCalculoEdadEnum.ANIOS, 1991);
		calcular.put(TipoCalculoEdadEnum.SEMANAS, 3);

		String anio = "2018";
		String fechaIngreso = "1991";
		Date fechaAnio;

		Map<TipoCalculoEdadEnum, Integer> respuesta = new HashMap<TipoCalculoEdadEnum, Integer>();

		try {

			Mockito.when(accederRules.calcularEdadDerechohabiente(any(Date.class), anyString())).thenReturn(calcular);
			respuesta = capturarServiciosSubrogadosService.calcularEdadPacienteNoEncontrado(anio, fechaIngreso);

			Assert.assertEquals(anio, "2018");
			logger.info("A�o: {}", calcular);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void sePuedeActivarGuardarTest() {

		boolean ban = Boolean.TRUE;
		Boolean[] requeridos = new Boolean[1];

		requeridos[0] = true;

		try {
			Mockito.when(capturarServiciosSubrogadosRules.sePuedeActivarGuardar(requeridos)).thenReturn(ban);

			Assert.assertEquals(ban, true);
			logger.info("Resultado: {}", ban);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void buscarServicioSubrogarTest() {

		ServicioSubrogar servicio = new ServicioSubrogar();
		servicio.setCveServicioSubrogar(23);

		try {
			Mockito.when(capturarServiciosSubrogadosRepositoryJDBCImpl.buscarServicioSubrogar(anyString()))
					.thenReturn(servicio);

			logger.info("Buscar servicio: {}", servicio);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void cerrarPeriodoActivoTest() {

		PeriodosImss periodoIMSS = new PeriodosImss();
		periodoIMSS.setClavePeriodo("2");

		PeriodoOperacion periodoOpe = new PeriodoOperacion();
		periodoOpe.setClavePresupuestal("123456789");

		try {
			capturarServiciosSubrogadosRepository.cerrarPeriodo(anyString(),
					eq(TipoCapturaEnum.SERVICIOS_SUBROGADOS.getClave()), anyString());
			Mockito.when(capturarServiciosSubrogadosRepository.obtenerSiguientePeriodo(anyString()))
					.thenReturn(periodoIMSS);
			Mockito.when(
					capturarServiciosSubrogadosRepository.buscarPeriodoOperacion(anyString(), anyInt(), anyString()))
					.thenReturn(periodoOpe);
			capturarServiciosSubrogadosRepository.insertarPeriodoOperacion(anyString(), anyInt(), anyString(), anyInt(),
					anyInt(), anyInt());
			capturarServiciosSubrogadosRepository.abrirPeriodoCerrado(anyString(), anyInt(), anyString());

			logger.info("Cerrar periodo: {}", periodoIMSS);
			logger.info("Periodo: {}", periodoOpe);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void guardarTest() {

		DatosHospitalizacion datosHospi = new DatosHospitalizacion();

		DatosPaciente datosPaciente = new DatosPaciente();
		datosPaciente.setCvePaciente(332L);
		datosHospi.setDatosPaciente(datosPaciente);

		DatosUsuario datosUsuario = new DatosUsuario();
		datosUsuario.setUsuario("Cuahutemoc");

		Paciente paciente = new Paciente();
		paciente.setCvePaciente(33L);

		ServicioSubrogado servicioSubrogado = new ServicioSubrogado();
		servicioSubrogado.setCveServicioSubrogado(34L);

		try {
			Mockito.when(capturarServiciosSubrogadosHelper.prepararServicio(any(DatosHospitalizacion.class),
					any(DatosUsuario.class))).thenReturn(servicioSubrogado);

			doNothing().when(capturarServiciosSubrogadosRepositoryJDBCImpl).guardarServicioSubrogado(servicioSubrogado,
					datosHospi, datosUsuario);
			Mockito.when(capturarServiciosSubrogadosHelper.prepararPaciente(any(DatosPaciente.class)))
					.thenReturn(paciente);
			capturarServiciosSubrogadosService.guardar(datosHospi, datosUsuario);

			logger.info("Consecutivo: {}", datosHospi.getDatosPaciente());
			logger.info("Delegaci�n:{}", datosUsuario.getUsuario());

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
