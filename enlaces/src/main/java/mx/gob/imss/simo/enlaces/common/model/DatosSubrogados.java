package mx.gob.imss.simo.enlaces.common.model;

import lombok.Data;

@Data
public class DatosSubrogados {

    private String tipoContrato;
    private String folioContrato;
    private String tipoServicio;
    private String motivoSubrogacion;
    private String claveServicioSubrogar;
    private String descripcionServicioSubrogar;
    private String descripcionGrupoSubrogar;
    private String cantidadEventos;
    private String costoUnitario;
    private String fechaServicio;

}
