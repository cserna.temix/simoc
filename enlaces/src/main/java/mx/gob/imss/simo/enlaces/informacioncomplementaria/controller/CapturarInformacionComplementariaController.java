package mx.gob.imss.simo.enlaces.informacioncomplementaria.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.context.RequestContext;
import org.springframework.dao.EmptyResultDataAccessException;

import com.ibm.icu.util.Calendar;

import lombok.Data;
import mx.gob.imss.simo.enlaces.common.constant.CapturarInformacionComplementariaConstants;
import mx.gob.imss.simo.enlaces.common.constant.MensajesEnlacesGeneralesConstants;
import mx.gob.imss.simo.enlaces.common.model.FormulaInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.EstructuraDatoInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.model.DatosSesion;

@Data
@ViewScoped
@ManagedBean(name = "informacionComplementariaController")
public class CapturarInformacionComplementariaController extends CapturarInformacionComplementariaCommonController {

    private static final long serialVersionUID = 7225952393539122086L;

    String textoIngresadoSubsistema;
    String mensajeJustificarConfirm;

    @Override
    @PostConstruct
    public void init() {

        super.init();
    }

    private void limpiarCatalogos() {

        getDatosHospitalizacion().getDatosInfoComplementaria().setCveRCV(null);
        reiniciarCatalogosComplementarios();
    }

    private void limpiarInformacionSubsistema() {

        setVisibleComplementarios(Boolean.FALSE);
        limpiarCatalogos();
        getDatosHospitalizacion().getDatosInfoComplementaria().setDatos(null);
        getDatosHospitalizacion().getDatosInfoComplementaria().setDesSubsistemaRCV(null);
        getAutoCompleteRCV().resetValue();
    }

    private void cargarCatalogosDependientes(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        setCatalogoPoblacion(getCapturarInformacionComplementariaServices()
                .armarDatosCatalogoPoblacion(catalogosInformacionComplementariaList));
        setCatalogoPor(getCapturarInformacionComplementariaServices()
                .armarDatosCatalogoPor(catalogosInformacionComplementariaList));
        setCatalogoEn(getCapturarInformacionComplementariaServices()
                .armarDatosCatalogoEn(catalogosInformacionComplementariaList));
        setCatalogoHoja(getCapturarInformacionComplementariaServices()
                .armarDatosCatalogoHoja(catalogosInformacionComplementariaList));
        setCatalogoSubclave(getCapturarInformacionComplementariaServices()
                .armarDatosCatalogoSubclave(catalogosInformacionComplementariaList));
    }

    public void validarPeriodo() {

        try {
            getCapturarInformacionComplementariaServices().validarPeriodo(
                    getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                    obtenerDatosUsuario().getCvePresupuestal());
            
            
            cargarCatalogoSubsistema();
            requeridos[0] = Boolean.TRUE;
            habilitarComponentes(getSubsistema());
            setTieneFoco(getAutoCompleteSubsistema().getClientId());
            
            
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosInfoComplementaria()
                    .setCvePeriodo(CapturarInformacionComplementariaConstants.CADENA_VACIA);
            requeridos[0] = Boolean.FALSE;
            getButtonCancelar().setDisabled(Boolean.TRUE);
            setTieneFoco(getPeriodoIM().getClientId());
        }

    }

    public void validarSubsistema() {

        setVisibleCifraDeControl(Boolean.FALSE);
        setVisibleSubsistemas(Boolean.TRUE);
        setVisibleJustificar(Boolean.FALSE);
        setVisibleComplementarios(Boolean.FALSE);
        setVisibleConsultar(Boolean.FALSE);
        limpiarInformacionSubsistema();
        try {
            getCapturarInformacionComplementariaServices().validarSubsistema(getAutoCompleteSubsistema().getValue());
            setTextoIngresadoSubsistema(getAutoCompleteSubsistema().getValue() == null ? ""
                    : getAutoCompleteSubsistema().getValue().toString());
            DatosCatalogo datosCatalogo = validaAutoComplete(getAutoCompleteSubsistema(),
                    CapturarInformacionComplementariaConstants.LONGITUD_AUTOCOMPLETE_SUBSISTEMA,
                    getListaSubsistemaAutoComplete(), getCatalogoSubsistema());
            if (null != datosCatalogo) {
                getDatosHospitalizacion().getDatosInfoComplementaria()
                        .setCveSubsistema(Integer.parseInt(datosCatalogo.getClave()));
                setSubsistema(getCapturarInformacionComplementariaServices()
                        .obtenerSubsistema(getDatosHospitalizacion().getDatosInfoComplementaria().getCveSubsistema()));
                if (!(getCapturarInformacionComplementariaServices().validarSubsistemaCerrado(
                        getDatosHospitalizacion().getDatosInfoComplementaria(),
                        obtenerDatosUsuario().getCvePresupuestal(),
                        getAutoCompleteSubsistema().getValue().toString()))) {
                    setCatalogoRCV(getCapturarInformacionComplementariaServices().obtenerCatalogoRCV(
                            getDatosHospitalizacion().getDatosInfoComplementaria().getCveSubsistema()));
                    setListaRCVAutoComplete(convertirCatalogoString(getCatalogoRCV()));
                    requeridos[1] = Boolean.TRUE;
                    habilitarComponentes(getSubsistema());
                    setTieneFoco(getAutoCompleteRCV().getClientId());
                } else {
                    requeridos[1] = Boolean.FALSE;
                    habilitarComponentes(getSubsistema());
                    getButtonConsultar().setDisabled(Boolean.FALSE);

                }

                getDatosHospitalizacion().getDatosInfoComplementaria()
                        .setSubsistemas(getCapturarInformacionComplementariaServices().cargarSubsistemas(
                                getCatalogoSubsistema(), getDatosHospitalizacion().getDatosInfoComplementaria(),
                                obtenerDatosUsuario().getCvePresupuestal()));
                getDatosHospitalizacion().getDatosInfoComplementaria()
                        .setEstadoSubsistema(getCapturarInformacionComplementariaServices().cargarSubsistemas(
                                getCatalogoSubsistema(), getDatosHospitalizacion().getDatosInfoComplementaria(),
                                obtenerDatosUsuario().getCvePresupuestal()));
                
                
                setCierrePeriodo(getCapturarInformacionComplementariaServices().validarCierrePeriodo(
                        getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                        obtenerDatosUsuario().getCvePresupuestal()));
               
                
                
                
                setEstadoPeriodo(getCapturarInformacionComplementariaServices().estadoPeriodoOperacion(
                        getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                        obtenerDatosUsuario().getCvePresupuestal(),
                        TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave()));
            } else {
                getButtonJustificar().setDisabled(Boolean.TRUE);
                getButtonConsultar().setDisabled(Boolean.TRUE);
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_154),
                        getArray(getTextoIngresadoSubsistema()));

            }
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosInfoComplementaria().setCveSubsistema(null);
            getAutoCompleteSubsistema().resetValue();
            requeridos[1] = Boolean.FALSE;
            setTieneFoco(getAutoCompleteSubsistema().getClientId());
        }
    }

    private boolean ultimoCatalogoRequerido(Subsistema subsistema, String comboId) {

        return getCapturarInformacionComplementariaServices().ultimoCatalogoRequerido(subsistema, comboId,
                getComponentes());
    }

    public void validarSelectRCV() {

        reiniciarCatalogosComplementarios();
        boolean esCifraDeControl99 = false;
        boolean esUltimoRequerido = false;
        setVisibleJustificar(Boolean.FALSE);
        String[] comp = { getAutoCompleteRCV().getClientId(), getSelectPoblacion().getClientId(),
                getSelectPor().getClientId(), getSelectEn().getClientId(), getSelectHoja().getClientId(),
                getSelectSubclave().getClientId() };
        setComponentes(comp);
        setVisibleComplementarios(Boolean.FALSE);
        setVisibleJustificar(Boolean.FALSE);
        setVisibleConsultar(Boolean.FALSE);
        try {
            DatosCatalogo datosCatalogo = validaAutoComplete(getAutoCompleteRCV(),
                    CapturarInformacionComplementariaConstants.LONGITUD_AUTOCOMPLETE_SUBSISTEMA,
                    getListaRCVAutoComplete(), getCatalogoRCV());
            logger.info("DATOS CATALOGOS");
            logger.info(datosCatalogo.toString());
            if (null != datosCatalogo) {
                getCapturarInformacionComplementariaServices()
                        .validarRCV((getAutoCompleteRCV().getValue() == null) ? null : datosCatalogo.getClave());
                getDatosHospitalizacion().getDatosInfoComplementaria()
                        .setCveRCV(Integer.parseInt(datosCatalogo.getClave()));
                
                logger.info("GETTTTTTTTTTTTTTTTCVERCV");
                logger.info(getDatosHospitalizacion().getDatosInfoComplementaria().getCveRCV());
                
                if (CapturarInformacionComplementariaConstants.CVE_CIFRA_CONTROL_99_INT == getDatosHospitalizacion()
                        .getDatosInfoComplementaria().getCveRCV()) {
                    setVisibleCifraDeControl(Boolean.FALSE);
                    esCifraDeControl99 = true;
                    ocultarCombosComplementarios();
                    setLongitudCifraControl(11);
                } else {
                    habilitarComponentes(getSubsistema());

                }
                List<SubsistemaRCV> catalogosInformacionComplementariaList = getCapturarInformacionComplementariaServices()
                        .obtenerCatalogosInformacionComplementaria(
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCveSubsistema(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCveRCV());
                cargarCatalogosDependientes(catalogosInformacionComplementariaList);
                if (esCifraDeControl99
                        || ultimoCatalogoRequerido(getSubsistema(), getAutoCompleteRCV().getClientId())) {

                    getDatosHospitalizacion().setDatosInfoComplementaria(getCapturarInformacionComplementariaServices()
                            .consultarSubsistemaRCV(getDatosHospitalizacion().getDatosInfoComplementaria(),
                                    getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                                    obtenerDatosUsuario().getCvePresupuestal()));
                    RequestContext.getCurrentInstance()
                            .execute(CapturarInformacionComplementariaConstants.ASIGNAR_FOCO_SECCION_DATOS);
                    seccionComplementarios();
                    validarFuncionParaFoco();
                    esUltimoRequerido = true;
                } else {
                    cambiarFoco(getAutoCompleteRCV().getClientId());
                }                
                if (!esCifraDeControl99 && esUltimoRequerido) {
                    setVisibleCifraDeControl(Boolean.TRUE);
                    setLongitudCifraControl(9);
                } else {
                    setVisibleCifraDeControl(Boolean.FALSE);
                    setLongitudCifraControl(11);
                }
            } else {

                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                        getArray(CapturarInformacionComplementariaConstants.REQUERIDO_RCV));
            }
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosInfoComplementaria().setCveRCV(null);
            setVisibleJustificar(Boolean.FALSE);
            getAutoCompleteRCV().resetValue();
            setTieneFoco(getAutoCompleteRCV().getClientId());
        }
    }

    private void cambiarFoco(String comboId) {

        /**
         * pos y componentes tienen relacion en orden a los catalogos en la vista
         */
        Boolean[] pos;
        if (comboId == getAutoCompleteRCV().getClientId()) {
            pos = mapearPosicionesInicial(getPosiciones());
        } else {
            pos = mapearPosiciones(getPosiciones());
        }

        boolean flag;
        for (int i = 0; i <= pos.length - 1; i++) {
            flag = pos[i];
            if (flag) {
                setTieneFoco(getComponentes()[i + 1]);
                break;
            }
        }

        if (getTieneFoco().equals(getSelectPoblacion().getClientId())) {
            posiciones[0] = Boolean.FALSE;
        }
        if (getTieneFoco().equals(getSelectPor().getClientId())) {
            posiciones[1] = Boolean.FALSE;
        }
        if (getTieneFoco().equals(getSelectEn().getClientId())) {
            posiciones[2] = Boolean.FALSE;
        }
        if (getTieneFoco().equals(getSelectHoja().getClientId())) {
            posiciones[3] = Boolean.FALSE;
        }
        if (getTieneFoco().equals(getSelectSubclave().getClientId())) {
            posiciones[4] = Boolean.FALSE;
        }
    }

    private Boolean[] mapearPosicionesInicial(Boolean[] pos) {

        if (getSubsistema().getIndPoblacion() == 1) {
            pos[0] = Boolean.TRUE;
        }
        if (getSubsistema().getIndPor() == 1) {
            pos[1] = Boolean.TRUE;
        }
        if (getSubsistema().getIndEn() == 1) {
            pos[2] = Boolean.TRUE;
        }
        if (getSubsistema().getIndHoja() == 1) {
            pos[3] = Boolean.TRUE;
        }
        if (getSubsistema().getIndSubclave() == 1) {
            pos[4] = Boolean.TRUE;
        }
        return pos;
    }

    private Boolean[] mapearPosiciones(Boolean[] pos) {

        if (posiciones[0] == Boolean.TRUE) {
            pos[0] = Boolean.TRUE;
        }
        if (posiciones[1] == Boolean.TRUE) {
            pos[1] = Boolean.TRUE;
        }
        if (posiciones[2] == Boolean.TRUE) {
            pos[2] = Boolean.TRUE;
        }
        if (posiciones[3] == Boolean.TRUE) {
            pos[3] = Boolean.TRUE;
        }
        if (posiciones[4] == Boolean.TRUE) {
            pos[4] = Boolean.TRUE;
        }
        return pos;
    }

    public void validarPoblacion() {

        try {
            getCapturarInformacionComplementariaServices()
                    .validarPoblacion(getDatosHospitalizacion().getDatosInfoComplementaria().getCvePoblacion());
            if (ultimoCatalogoRequerido(getSubsistema(), getSelectPoblacion().getClientId())) {
                getDatosHospitalizacion().setDatosInfoComplementaria(getCapturarInformacionComplementariaServices()
                        .consultarSubsistemaRCV(getDatosHospitalizacion().getDatosInfoComplementaria(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                                obtenerDatosUsuario().getCvePresupuestal()));
                RequestContext.getCurrentInstance()
                        .execute(CapturarInformacionComplementariaConstants.ASIGNAR_FOCO_SECCION_DATOS);
                seccionComplementarios();
                validarFuncionParaFoco();
                setVisibleCifraDeControl(Boolean.TRUE);
            } else {
                cambiarFoco(getSelectPoblacion().getClientId());
            }

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosInfoComplementaria().setCvePoblacion(null);
            setTieneFoco(getSelectPoblacion().getClientId());
        }

    }

    public void validarPor() {

        try {
            getCapturarInformacionComplementariaServices()
                    .validarPor(getDatosHospitalizacion().getDatosInfoComplementaria().getCvePor());
            if (ultimoCatalogoRequerido(getSubsistema(), getSelectPor().getClientId())) {
                getDatosHospitalizacion().setDatosInfoComplementaria(getCapturarInformacionComplementariaServices()
                        .consultarSubsistemaRCV(getDatosHospitalizacion().getDatosInfoComplementaria(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                                obtenerDatosUsuario().getCvePresupuestal()));
                RequestContext.getCurrentInstance()
                        .execute(CapturarInformacionComplementariaConstants.ASIGNAR_FOCO_SECCION_DATOS);
                seccionComplementarios();
                validarFuncionParaFoco();
                setVisibleCifraDeControl(Boolean.TRUE);
            } else {
                cambiarFoco(getSelectPor().getClientId());
            }

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosInfoComplementaria().setCvePor(null);
            setTieneFoco(getSelectPor().getClientId());
        }

    }

    public void validarEn() {

        try {
            getCapturarInformacionComplementariaServices()
                    .validarEn(getDatosHospitalizacion().getDatosInfoComplementaria().getCveEn());
            if (ultimoCatalogoRequerido(getSubsistema(), getSelectEn().getClientId())) {
                getDatosHospitalizacion().setDatosInfoComplementaria(getCapturarInformacionComplementariaServices()
                        .consultarSubsistemaRCV(getDatosHospitalizacion().getDatosInfoComplementaria(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                                obtenerDatosUsuario().getCvePresupuestal()));
                RequestContext.getCurrentInstance()
                        .execute(CapturarInformacionComplementariaConstants.ASIGNAR_FOCO_SECCION_DATOS);
                seccionComplementarios();
                validarFuncionParaFoco();
                setVisibleCifraDeControl(Boolean.TRUE);
            } else {
                cambiarFoco(getSelectEn().getClientId());
            }

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosInfoComplementaria().setCveEn(null);
            setTieneFoco(getSelectEn().getClientId());
        }

    }

    public void validarHoja() {

        try {
            getCapturarInformacionComplementariaServices()
                    .validarHoja(getDatosHospitalizacion().getDatosInfoComplementaria().getCveHoja());
            if (ultimoCatalogoRequerido(getSubsistema(), getSelectHoja().getClientId())) {
                getDatosHospitalizacion().setDatosInfoComplementaria(getCapturarInformacionComplementariaServices()
                        .consultarSubsistemaRCV(getDatosHospitalizacion().getDatosInfoComplementaria(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                                obtenerDatosUsuario().getCvePresupuestal()));
                RequestContext.getCurrentInstance()
                        .execute(CapturarInformacionComplementariaConstants.ASIGNAR_FOCO_SECCION_DATOS);
                seccionComplementarios();
                validarFuncionParaFoco();
                setVisibleCifraDeControl(Boolean.TRUE);
            } else {
                cambiarFoco(getSelectHoja().getClientId());
            }

        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosInfoComplementaria().setCveHoja(null);
            setTieneFoco(getSelectHoja().getClientId());
        }

    }

    public void validarSubclave() {

        try {
            getCapturarInformacionComplementariaServices()
                    .validarSubclave(getDatosHospitalizacion().getDatosInfoComplementaria().getCveSubclave());
            if (ultimoCatalogoRequerido(getSubsistema(), getSelectSubclave().getClientId())) {
                getDatosHospitalizacion().setDatosInfoComplementaria(getCapturarInformacionComplementariaServices()
                        .consultarSubsistemaRCV(getDatosHospitalizacion().getDatosInfoComplementaria(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                                obtenerDatosUsuario().getCvePresupuestal()));
                RequestContext.getCurrentInstance()
                        .execute(CapturarInformacionComplementariaConstants.ASIGNAR_FOCO_SECCION_DATOS);
                seccionComplementarios();
                validarFuncionParaFoco();
                setVisibleCifraDeControl(Boolean.TRUE);
            }
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            getDatosHospitalizacion().getDatosInfoComplementaria().setCveSubclave(null);
            setTieneFoco(getSelectSubclave().getClientId());
        }
    }

    public void validarCifraDeControl() {

        try {
            getCapturarInformacionComplementariaServices()
                    .validarCifradeControl(getDatosHospitalizacion().getDatosInfoComplementaria());
            habilitarBotonGuardar();
            setTieneFoco(getButtonGuardar().getClientId());
        } catch (HospitalizacionException e) {
            getDatosHospitalizacion().getDatosInfoComplementaria().setCifraDeControl(null);
            agregarMensajeError(e);
            setTieneFoco(getTextCifraDeControl().getClientId());
        }
    }

    public void justificar() {

        if (getCapturarInformacionComplementariaServices().existeInformacionComplementaria(
                getDatosHospitalizacion().getDatosInfoComplementaria().getCveSubsistema(),
                obtenerDatosUsuario().getCvePresupuestal(),
                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo())) {
            getConfirmSi().setDisabled(Boolean.FALSE);
            setMensajeJustificarConfirm(getCapturarInformacionComplementariaServices().reemplazarMensajeConfirm(
                    CapturarInformacionComplementariaConstants.MENSAJE_JUSTIFICAR_CONFIRM,
                    getTextoIngresadoSubsistema()));
            setExisteInformacionComplementaria(Boolean.TRUE);
            setBanderaJustificacionConfirm(CapturarInformacionComplementariaConstants.JUSTIFICACION_CONFIRM);
            RequestContext.getCurrentInstance()
                    .execute(CapturarInformacionComplementariaConstants.ABRIR_CONFIRM_JUSTIFICACION);
            setTieneFoco(getConfirmNo().getClientId());
        } else {
            setGuardarJustificacion(Boolean.TRUE);
            setVisibleJustificar(Boolean.TRUE);
            setVisibleComplementarios(Boolean.FALSE);
            setVisibleConsultar(Boolean.FALSE);
            setVisibleCifraDeControl(Boolean.FALSE);
            setTieneFoco(getTextJustificacion().getClientId());
        }

    }

    private void seccionConsultar() {

        setVisibleJustificar(Boolean.FALSE);
        setVisibleComplementarios(Boolean.FALSE);
        setVisibleConsultar(Boolean.TRUE);
        setVisibleBotones(Boolean.TRUE);
        setVisibleCifraDeControl(Boolean.FALSE);
        setVisibleSubsistemas(Boolean.FALSE);
        getDatosHospitalizacion().getDatosInfoComplementaria().setCveRCV(null);
        getAutoCompleteRCV().resetValue();
        getAutoCompleteRCV().setDisabled(Boolean.TRUE);
        ocultarCombosComplementarios();
    }

    public void consultar() {

        seccionConsultar();
        getDatosHospitalizacion().getDatosInfoComplementaria().setDesSubsistemaRCV(null);
        setInformacionExistenteConsultaList(
                getCapturarInformacionComplementariaServices().informacionComplementariaConsulta(obtenerDatosUsuario(),
                        getDatosHospitalizacion().getDatosInfoComplementaria()));
    }

    private void seccionComplementarios() {

        setVisibleJustificar(Boolean.FALSE);
        setVisibleComplementarios(Boolean.TRUE);
        setVisibleConsultar(Boolean.FALSE);
    }

    public void guardar() {

        if (isGuardarJustificacion()) {
            getCapturarInformacionComplementariaServices().guardarInfoComplementariaPorJustificacion(
                    isExisteInformacionComplementaria(), obtenerDatosUsuario(),
                    getDatosHospitalizacion().getDatosInfoComplementaria());
            agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
            cancelar();
        } else {
            setSubsistemaRCV(getCapturarInformacionComplementariaServices()
                    .obtenerSubsistemaRCV(getDatosHospitalizacion().getDatosInfoComplementaria()));

            boolean datosIgualACero = getCapturarInformacionComplementariaServices().datosIgualACero(
            		getDatosHospitalizacion().getDatosInfoComplementaria().getDatos());
                	
            List<FormulaInformacionComplementaria> formulasValidadas = getCapturarInformacionComplementariaServices()
	                    .validarFormulaDato(getSubsistemaRCV().getClave(),
	                            getDatosHospitalizacion().getDatosInfoComplementaria());
            String[] errores = new String[formulasValidadas.size()];

            int i = 0;
            for (FormulaInformacionComplementaria formula : formulasValidadas) {
                if (formula.getMensajeError() != null && !formula.getValida()) {
                    errores[i] = formula.getMensajeError();
                    i += 1;
                }
            }	            
            
            if (errores.length > 0 && errores[0] != null && !datosIgualACero) {
                for (String err : errores) {
                    if (null != err) {
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, err, BaseConstants.CADENA_VACIA));
                    }

                }
                setTieneFoco(getButtonGuardar().getClientId());
            } else {
                try {
                    if (isVisibleCifraDeControl()) { /*
                                                      * isVisibleCifraDeControl tambien se ocupa para saber que es una
                                                      * cifra de control 99
                                                      */
                        getCapturarInformacionComplementariaServices()
                                .validarCifradeControl(getDatosHospitalizacion().getDatosInfoComplementaria());
                        agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
                    } else {
                        getCapturarInformacionComplementariaServices().validarCifraControl99(
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCveSubsistema(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCifraDeControl(),
                                obtenerDatosUsuario().getCvePresupuestal(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo());
                        agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_027),
                                getArray(getAutoCompleteSubsistema().getValue().toString()));
                    }                    
                    getCapturarInformacionComplementariaServices().guardarInfoComplementaria(
                            getSubsistemaRCV().getClave(), obtenerDatosUsuario(),
                            getDatosHospitalizacion().getDatosInfoComplementaria(), isVisibleCifraDeControl(),
                            datosIgualACero);  
                    cancelar();
                } catch (HospitalizacionException e) {
                    agregarMensajeError(e);
                    if (!isVisibleCifraDeControl()) {
                        setTieneFoco(getCapturarInformacionComplementariaServices()
                                .descomponerCadenaFocoParaErrorCifraCOntrol(getComponenteCifraControl99()));
                        getDatosHospitalizacion().getDatosInfoComplementaria().getDatos().get(0).setValor(null);
                    } else {
                        setTieneFoco(getTextCifraDeControl().getClientId());
                    }

                }

            }
        }

    }

    public PeriodosImss obtenerPeriodoImss(String mes, String anio) {

        return getCatalogosHospitalizacionServices().obtenerPeriodo(mes, anio);
    }

    public boolean existePeriodo(String mes, String anio) {

        boolean existe = true;
        PeriodosImss periodosImss = null;
        try {
            periodosImss = getCatalogosHospitalizacionServices().obtenerPeriodo(mes, anio);
        } catch (EmptyResultDataAccessException e) {
            logger.error("{}", e);
        }
        if (periodosImss == null) {
            existe = false;
        }
        return existe;
    }

    private boolean validaFecha(String mes, String anio) {

        boolean valida = true;

        Calendar cal = Calendar.getInstance();
        int mesPeriodo = Integer.parseInt(mes);
        int anioPeriodo = Integer.parseInt(anio);
        int mesActual = cal.get(Calendar.MONTH) + 1;
        int anioActual = cal.get(Calendar.YEAR);
        if (mesPeriodo > mesActual || anioPeriodo > anioActual) {
            valida = false;
        }
        return valida;
    }

    public void confirmAcepto() {

        setGuardarJustificacion(Boolean.TRUE);
        setVisibleCifraDeControl(Boolean.FALSE);
        getConfirmSi().setDisabled(Boolean.TRUE);
        setBanderaJustificacionConfirm(CapturarInformacionComplementariaConstants.CADENA_VACIA);
        RequestContext.getCurrentInstance()
                .execute(CapturarInformacionComplementariaConstants.CERRAR_CONFIRM_JUSTIFICACION);
        setVisibleJustificar(Boolean.TRUE);
        setVisibleComplementarios(Boolean.FALSE);

        setVisibleConsultar(Boolean.FALSE);
        setTieneFoco(getTextJustificacion().getClientId());
    }

    public void confirmRechazo() {

        setGuardarJustificacion(Boolean.FALSE);

        setBanderaJustificacionConfirm(CapturarInformacionComplementariaConstants.CADENA_VACIA);
        RequestContext.getCurrentInstance()
                .execute(CapturarInformacionComplementariaConstants.CERRAR_CONFIRM_JUSTIFICACION);
        getConfirmSi().setDisabled(true);

    }

    @Override
    public String obtenerNombrePagina() {

        return "";
    }

    @Override
    public void salir() throws IOException {

        DatosSesion datosSesion = super.getDatosSesionService().getDatosSesion(super.getObjetosSs().getUserInfo(),
                super.getObjetosSs().getUsername());
        datosSesion.setMostrarDialogoDelegacion(Boolean.FALSE);
        super.getDatosSesionService().save(datosSesion);

        Properties properties = new Properties();
        properties
                .load(getClass().getClassLoader().getResourceAsStream(CapturarInformacionComplementariaConstants.PATH));
        String url = properties.getProperty(CapturarInformacionComplementariaConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext()
                .redirect(url + CapturarInformacionComplementariaConstants.PAGINA_INICIO);
    }

    public void controlFocoDato() {

        String datosVista[];
        if (null != getValidarDato()) {

            try {
                setCantidadDatos(getDatosHospitalizacion().getDatosInfoComplementaria().getDatos().size());
                datosVista = getValidarDato().split(":");
                if (datosVista.length == 5) {
                    if (!isVisibleCifraDeControl()) {
                        getDatosHospitalizacion().getDatosInfoComplementaria()
                                .setCifraDeControl(Long.parseLong(datosVista[4]));
                        setComponenteCifraControl99(obtenerFocoTabla(datosVista[2], getValidarDato(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getDatos(), true));
                        getCapturarInformacionComplementariaServices().validarCifraControl99(
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCveSubsistema(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCifraDeControl(),
                                obtenerDatosUsuario().getCvePresupuestal(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo());
                    }
                    getCapturarInformacionComplementariaServices().validarDato(Integer.parseInt(datosVista[4]));
                    EstructuraDatoInformacionComplementaria estructuraDatoInformacionComplementariaAux = getDatosHospitalizacion()
                            .getDatosInfoComplementaria().getDatos().get(Integer.parseInt(datosVista[2]));
                    estructuraDatoInformacionComplementariaAux.setValor(datosVista[4]);
                    getDatosHospitalizacion().getDatosInfoComplementaria().getDatos()
                            .set(Integer.parseInt(datosVista[2]), estructuraDatoInformacionComplementariaAux);

                    habilitarBotonGuardar();

                    String foco = null;
                    if (Integer.parseInt(datosVista[2]) < (getCantidadDatos() - 1)) {
                        foco = obtenerFocoTabla(datosVista[2], getValidarDato(),
                                getDatosHospitalizacion().getDatosInfoComplementaria().getDatos(), false);
                    }
                    if (null != foco && foco.length() > 0) {
                        setFocoDato(foco);
                    } else if (isVisibleCifraDeControl()) {
                        setTieneFoco(getTextCifraDeControl().getClientId());
                        setFocoDato(BaseConstants.CADENA_VACIA);
                    }

                    logger.info("regresa foco " + getTieneFoco());
                } else {
                    getButtonGuardar().setDisabled(Boolean.TRUE);
                    throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                            getArray(CapturarInformacionComplementariaConstants.REQUERIDO_DATO));
                }

            }

            catch (HospitalizacionException e) {
                agregarMensajeError(e);
                datosVista = getValidarDato().split(":");
                /* Limpiar Datos de la vista */
                getDatosHospitalizacion().getDatosInfoComplementaria().getDatos().get(Integer.parseInt(datosVista[2]))
                        .setValor(null);
                setFocoDato(obtenerFocoTabla(datosVista[2], getValidarDato(),
                        getDatosHospitalizacion().getDatosInfoComplementaria().getDatos(), true));
            }

        }

    }

    /**
     * Validar que los datos dinamicos no esten vacios para habilitar el boton guardar
     * bandera = true. Se deshabilita el boton gaurdar
     * bandera = false. Se habilita el boton guardar
     */
    private void habilitarBotonGuardar() {

        boolean bandera = false;

        if (isVisibleComplementarios() && isVisibleCifraDeControl()) {
            // Valida los campos dinamicos
            for (int i = 0; i < getDatosHospitalizacion().getDatosInfoComplementaria().getDatos().size(); i++) {
                EstructuraDatoInformacionComplementaria dato = (EstructuraDatoInformacionComplementaria) getDatosHospitalizacion()
                        .getDatosInfoComplementaria().getDatos().get(i);
                if (dato.getValor() == null || dato.getValor().isEmpty())
                    bandera = true;
            }

            if (getTextCifraDeControl().getValue() == null || getTextCifraDeControl().getValue().toString().isEmpty()) {
                bandera = true; // campo vacio en el formulario
            }
        }

        if (isVisibleJustificar()) {
            if (getTextJustificacion().getValue() == null || getTextJustificacion().getValue().toString().isEmpty()) {
                bandera = true; // existe campo vacio en el formulario
            }
        } else {
            setTieneFoco(getButtonGuardar().getClientId());
        }

        if (bandera) { // campo vacio en el formulario
            getButtonGuardar().setDisabled(Boolean.TRUE);
        } else {
            getButtonGuardar().setDisabled(Boolean.FALSE);
        }

    }

    public void validarJustificacion() {

        try {
            getCapturarInformacionComplementariaServices().validarJustificacion(
                    (getTextJustificacion().getValue() == null) ? null : getTextJustificacion().getValue().toString());
            habilitarBotonGuardar();
            setTieneFoco(getButtonGuardar().getClientId());
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            setTieneFoco(getTextJustificacion().getClientId());
        }
    }

    private String obtenerFocoTabla(String numeroDato, String validarDato2,
            List<EstructuraDatoInformacionComplementaria> datosDinamicos, boolean error) {

        StringBuilder cadena = new StringBuilder();
        String datos[] = validarDato2.split(":");
        int siguienteDato = Integer.parseInt(numeroDato) + 1;
        String cadenaReturn = "";
        if (!error) {
            datos[2] = String.valueOf(siguienteDato);
            if (null != datosDinamicos.get(siguienteDato)) {
                for (int i = 0; i < datos.length - 1; i++) {
                    cadenaReturn = cadenaReturn.concat(datos[i]).concat(":");
                }
            }
            cadena.append(cadenaReturn);
            cadenaReturn = cadena.deleteCharAt(cadenaReturn.length() - 1).toString();
            logger.info("foco tabla: " + cadenaReturn);
        } else {
            for (int i = 0; i < datos.length; i++) {
                cadenaReturn = cadenaReturn.concat(datos[i]).concat(":");
            }
            cadena.append(cadenaReturn);
            cadenaReturn = cadena.deleteCharAt(cadenaReturn.length() - 1).toString();
            logger.info("foco tabla: " + cadenaReturn);
        }
        return cadenaReturn;
    }

    public void actualizarFocoDato() {

        logger.info(getTieneFoco());
        logger.info(getFocoDato());
        setTieneFoco(getFocoDato());
    }

    public void controlMapaCierre() {
    	logger.error("MAPA CIERRE PERIODO");
        FacesContext fc = FacesContext.getCurrentInstance();
        setLlaveMapaSubsistema(getCountryParam(fc));        
        getCapturarInformacionComplementariaServices().controlMapaCierre(obtenerDatosUsuario().getCvePresupuestal(),
                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                Integer.valueOf(getLlaveMapaSubsistema()), obtenerDatosUsuario().getUsuario());
        boolean isMonitoreo = getCapturarInformacionComplementariaServices().estadoPeriodoOperacionMonitoreo(getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(), 
    			obtenerDatosUsuario().getCvePresupuestal(), TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave());
        try{
        	if(!isMonitoreo){
		        getCapturarInformacionComplementariaServices().abrirPeriodo(obtenerDatosUsuario().getCvePresupuestal(),
		                TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave(),
		                getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo());		        
            }
        	setCierrePeriodo(Boolean.FALSE);
	        setEstadoPeriodo(Boolean.FALSE);
        } catch (HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("Ocurrio un error al abrir el periodo");
        }
        agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_026), getArray(getLlaveMapaSubsistema()));
        getDatosHospitalizacion().getDatosInfoComplementaria().getEstadoSubsistema().replace(getLlaveMapaSubsistema(),
                Boolean.FALSE);
        
    }

    public String getCountryParam(FacesContext fc) {

        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        return params.get("mapaSubsistema");

    }

    public void controlCierrePeriodo() {
    	logger.error("Cierra periodo");
    	setCierrePeriodo(true);
        try {
            boolean estadoPerido = isEstadoPeriodo();
            if (estadoPerido) {
            	boolean isMonitoreo = getCapturarInformacionComplementariaServices().estadoPeriodoOperacionMonitoreo(getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(), 
             			obtenerDatosUsuario().getCvePresupuestal(), TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave());            	
                getCapturarInformacionComplementariaServices().cerrarPeriodo(obtenerDatosUsuario().getCvePresupuestal(),
                        TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave(),
                        getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo(),
                        isMonitoreo);
                if(isMonitoreo){
                	getPeriodoIM().setDisabled(Boolean.TRUE);
                	getAutoCompleteSubsistema().setDisabled(Boolean.TRUE);
                	setCierrePeriodo(Boolean.FALSE);
                }else{
                	setCierrePeriodo(Boolean.TRUE);
                }
                agregarMensajeInformativo(getArray(MensajesEnlacesGeneralesConstants.MG_028),
                        getArray(getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo()));
                cambiarEstadoSubsistema(Boolean.FALSE);
                // setCierrePeriodo(Boolean.FALSE);
                // setEstadoPeriodo(Boolean.FALSE);
                
            } else {
                getCapturarInformacionComplementariaServices().abrirPeriodo(obtenerDatosUsuario().getCvePresupuestal(),
                        TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave(),
                        getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo());
                cambiarEstadoSubsistema(Boolean.TRUE);
                setCierrePeriodo(Boolean.TRUE);
                // agregarMensajeInformativo(getArray(MensajesEnlacesGeneralesConstants.MG_028),
                // getArray(getObjetosSs().getPeriodoActual()));

            }

            setVisibleSubsistemas(Boolean.TRUE);
            // setEstadoPeriodo(Boolean.TRUE);
            cancelarCierre();
        } catch (

        HospitalizacionException e) {
            agregarMensajeError(e);
            logger.error("Ocurrio un error al cerrar el periodo");
        }
    }

    public void cambiarEstadoSubsistema(Boolean estado) {

        for (Entry<String, Boolean> estadoSubsistema : getDatosHospitalizacion().getDatosInfoComplementaria()
                .getEstadoSubsistema().entrySet()) {
            estadoSubsistema.setValue(estado);
        }
    }

    @Override
    public List<String> convertirCatalogoString(List<DatosCatalogo> datosCatalogo) {

        List<String> datos = new ArrayList<>();
        for (DatosCatalogo dato : datosCatalogo) {
            datos.add(dato.getClave());
        }
        return datos;
    }

    public List<String> filtrarCatalogoSubsistema(String query) {

        return filtrarCatalogo(getListaSubsistemaAutoComplete(), query,
                CapturarInformacionComplementariaConstants.LONGITUD_AUTOCOMPLETE_SUBSISTEMA);
    }

    public List<String> filtrarCatalogoRCV(String query) {
    	 logger.info("QUERYYYYYYYY" + query);
        return filtrarCatalogo(getListaRCVAutoComplete(), query,
                CapturarInformacionComplementariaConstants.LONGITUD_AUTOCOMPLETE_RCV);
    }

    /**
     * Metodo que valida el nombre de la funcion para asignar el foco, ya sea
     * al boton guardar a los campos de captura para los datos. Esta funcion se
     * utiliza en el atributo oncomplete del remoteCommand
     */
    private void validarFuncionParaFoco() {

        if (null != getDatosHospitalizacion().getDatosInfoComplementaria().getDatos()
                && getDatosHospitalizacion().getDatosInfoComplementaria().getDatos().size() > 1) {
            funcionOnCompleteFocoDato = "asignarFocoDato();";
        } else {
            funcionOnCompleteFocoDato = "actualizarFocoGuardarConDato();";
        }
    }
    
    
    public DatosCatalogo validaAutoComplete(AutoComplete componente, Integer max, List<String> listaString,
            List<DatosCatalogo> listaCatalogo) {

        DatosCatalogo objeto = null;
        String textoIngresado;
        if (componente.getValue() != null) {
            textoIngresado = componente.getValue().toString();            
            if(StringUtils.isNotEmpty(textoIngresado) && textoIngresado.length() >= max){
            		List<String> resultado = new ArrayList<>();
                    String cadena;
                    Iterator<String> it = listaString.iterator();
                    while (it.hasNext()) {
                        cadena = it.next();
                        /*if (StringUtils.containsIgnoreCase(cadena,  textoIngresado)) {
                            resultado.add(cadena);                             
                        }*/
                        if (cadena.equalsIgnoreCase(textoIngresado)) {
                            resultado.add(cadena);                             
                        }
                    }
                    if (!resultado.isEmpty()) {
                        componente.setValue(resultado.get(0));
                        int index;
                        if (componente.getValue() != null) {
                            index = listaString.indexOf(componente.getValue().toString());
                            objeto = listaCatalogo.get(index);                            
                        }
                    } else {
                        componente.setValue(null);
                    }
            	}
        }
        return objeto;
    }
    
}
