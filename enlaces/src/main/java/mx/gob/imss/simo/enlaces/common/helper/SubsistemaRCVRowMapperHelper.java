package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

public class SubsistemaRCVRowMapperHelper extends BaseHelper implements RowMapper<SubsistemaRCV> {

    @Override
    public SubsistemaRCV mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        SubsistemaRCV subsistemaRCV = new SubsistemaRCV();
        subsistemaRCV.setClave(resultSet.getInt(SQLColumnasConstants.CVE_SUBSISTEMA_RCV));
        subsistemaRCV.setCveSubsistema(resultSet.getInt(SQLColumnasConstants.CVE_SUBSISTEMA));
        subsistemaRCV.setCveRCV(resultSet.getInt(SQLColumnasConstants.CVE_RCV));
        subsistemaRCV.setCvePoblacion(resultSet.getInt(SQLColumnasConstants.CVE_POBLACION));
        subsistemaRCV.setCvePor(resultSet.getInt(SQLColumnasConstants.CVE_POR));
        subsistemaRCV.setCveEn(resultSet.getInt(SQLColumnasConstants.CVE_EN));
        subsistemaRCV.setCveHoja(resultSet.getInt(SQLColumnasConstants.CVE_HOJA));
        subsistemaRCV.setCveSubclave(resultSet.getInt(SQLColumnasConstants.CVE_SUBCLAVE));
        subsistemaRCV.setIndCaptura(resultSet.getInt(SQLColumnasConstants.IND_CAPTURA));
        subsistemaRCV.setFechaBaja(validarFechaNula(resultSet, SQLColumnasConstants.FEC_BAJA));
        subsistemaRCV.setTotalRCV(resultSet.getInt(SQLColumnasConstants.NUM_TOTAL_RCV));
        subsistemaRCV.setDesSubsistemaRCV(resultSet.getString(SQLColumnasConstants.DES_SUBSISTEMA_RCV));
        return subsistemaRCV;
    }

}
