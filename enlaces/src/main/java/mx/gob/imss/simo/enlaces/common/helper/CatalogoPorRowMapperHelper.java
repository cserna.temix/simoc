package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

public class CatalogoPorRowMapperHelper extends BaseHelper implements RowMapper<DatosCatalogo> {

    @Override
    public DatosCatalogo mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        DatosCatalogo por = new DatosCatalogo();
        por.setClave(resultSet.getString(SQLColumnasConstants.CVE_POR));
        por.setDescripcion(resultSet.getString(SQLColumnasConstants.CVE_POR));
        return por;
    }

}
