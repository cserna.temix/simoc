package mx.gob.imss.simo.enlaces.serviciossubrogados.services.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogado;
import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogar;
import mx.gob.imss.simo.enlaces.serviciossubrogados.helper.CapturarServiciosSubrogadosHelper;
import mx.gob.imss.simo.enlaces.serviciossubrogados.repository.CapturarServiciosSubrogadosRepository;
import mx.gob.imss.simo.enlaces.serviciossubrogados.rules.CapturarServiciosSubrogadosRules;
import mx.gob.imss.simo.enlaces.serviciossubrogados.services.CapturarServiciosSubrogadosServices;
import mx.gob.imss.simo.hospitalizacion.busquedas.rules.AccederRules;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarIngresosConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.model.DatosUsuario;

@Service("capturarServiciosSubrogadosServices")
public class CapturarServiciosSubrogadosServicesImpl extends HospitalizacionCommonServicesImpl
        implements CapturarServiciosSubrogadosServices {

    @Autowired
    private CapturarServiciosSubrogadosRepository capturarServiciosSubrogadosRepository;

    @Autowired
    private CapturarServiciosSubrogadosHelper capturarServiciosSubrogadosHelper;

    @Autowired
    private CapturarServiciosSubrogadosRules capturarServiciosSubrogadosRules;

    @Autowired
    private AccederRules accederRules;

    @Override
    public void validarNss(String nss) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarNss(nss);
    }

    @Override
    public void validarEdad(String edad) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarEdad(edad);
    }

    @Override
    public void validarNombre(String nombre) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarNombre(nombre);
    }

    @Override
    public void validarApellidoPaterno(String apellidoPaterno) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarApellidoPaterno(apellidoPaterno);
    }

    @Override
    public void validarNumero(String numero) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarNumero(numero);
    }

    @Override
    public void validarTipoContrato(String tipoContrato) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarTipoContrato(tipoContrato);
    }

    @Override
    public void validarFolioContrato(String folioContrato) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarFolioContrato(folioContrato);
    }

    @Override
    public void validarTipoServicio(String tipoServicio) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarTipoServicio(tipoServicio);
    }

    @Override
    public void validarMotivoSubrogacion(String motivoSubrogacion) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarMotivoSubrogacion(motivoSubrogacion);
    }

    @Override
    public void validarServiciosSubrogar(String claveServiciosSubrogar) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarServiciosSubrogar(claveServiciosSubrogar);
    }

    @Override
    public void validarCantidadEventos(String cantidadEventos) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarCantidadEventos(cantidadEventos);
    }

    @Override
    public void validarCostoUnitario(String costoUnitario) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarCostoUnitario(costoUnitario);
    }

    @Override
    public String validarFechaServicio(String fechaServicio, String clavePresupuestal) throws HospitalizacionException {

        capturarServiciosSubrogadosRules.validarFechaServicio(fechaServicio);
        capturarServiciosSubrogadosRules.validarFormatoFecha(fechaServicio);
        capturarServiciosSubrogadosRules.validarFechaActual(fechaServicio);
        PeriodoOperacion periodoOperacion = capturarServiciosSubrogadosRepository.obtenerPeriodoOperacion(
                clavePresupuestal, TipoCapturaEnum.SERVICIOS_SUBROGADOS.getClave(), fechaServicio);
        if(!periodoOperacion.isIndicadorCerrado() && periodoOperacion.isIndicadorMonitoreo()) {
        	return periodoOperacion.getClavePeriodoIMSS();
        }else if(periodoOperacion.isIndicadorCerrado()
                || (!periodoOperacion.isIndicadorCerrado() && !periodoOperacion.isIndicadorActual())) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_009);
        }
        return periodoOperacion.getClavePeriodoIMSS();
    }

    @Override
    public boolean habilitarEdad(String fechaIngreso, String anio) {

        return capturarServiciosSubrogadosRules.habilitarEdad(fechaIngreso, anio);
    }

    @Override
    public Map<TipoCalculoEdadEnum, Integer> calcularEdadPacienteNoEncontrado(String anio, String fechaIngreso) {

        Date fechaAnio;
        try {
            fechaAnio = new SimpleDateFormat(CapturarIngresosConstants.FORMATO_FECHA_AGREGADO_ANIO)
                    .parse(anio.concat("/01/01"));
        } catch (ParseException e) {
            fechaAnio = new Date();
        }
        return accederRules.calcularEdadDerechohabiente(fechaAnio, fechaIngreso);
    }

    @Override
    public boolean sePuedeActivarGuardar(Boolean[] requeridos) {

        return capturarServiciosSubrogadosRules.sePuedeActivarGuardar(requeridos);
    }

    @Override
    public ServicioSubrogar buscarServicioSubrogar(String claveServiciosSubrogar) throws HospitalizacionException {

        ServicioSubrogar servicioSubrogar = capturarServiciosSubrogadosRepository
                .buscarServicioSubrogar(claveServiciosSubrogar);
        if (servicioSubrogar == null) {
        	 throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_018),
                     getArray(BaseConstants.REQUERIDO_SERVICIOS_SUBROGAR));
        }
        return servicioSubrogar;
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void cerrarPeriodoActivo(String cvePresupuestal, String cvePeriodoImss) throws HospitalizacionException {

        capturarServiciosSubrogadosRepository.cerrarPeriodo(cvePresupuestal,
                TipoCapturaEnum.SERVICIOS_SUBROGADOS.getClave(), cvePeriodoImss);
        PeriodosImss periodoIMSS = capturarServiciosSubrogadosRepository.obtenerSiguientePeriodo(cvePeriodoImss);
        PeriodoOperacion periodo = capturarServiciosSubrogadosRepository.buscarPeriodoOperacion(cvePresupuestal,
                TipoCapturaEnum.SERVICIOS_SUBROGADOS.getClave(), periodoIMSS.getClavePeriodo());
        if (periodo == null) {
            capturarServiciosSubrogadosRepository.insertarPeriodoOperacion(cvePresupuestal,
                    TipoCapturaEnum.SERVICIOS_SUBROGADOS.getClave(), periodoIMSS.getClavePeriodo(), 0, 1, 0);
        } else {
            capturarServiciosSubrogadosRepository.abrirPeriodoCerrado(cvePresupuestal,
                    TipoCapturaEnum.SERVICIOS_SUBROGADOS.getClave(), periodoIMSS.getClavePeriodo());
        }
    }

    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario)
            throws HospitalizacionException {

        try {
            Long cvePaciente = datosHospitalizacion.getDatosPaciente().getCvePaciente();
            if (null == cvePaciente || cvePaciente == 0) {
                Paciente paciente = capturarServiciosSubrogadosHelper
                        .prepararPaciente(datosHospitalizacion.getDatosPaciente());
                cvePaciente = getBuscarPacienteService().guardarPaciente(paciente);
                logger.debug("Paciente creado - cvePaciente: " + cvePaciente);
                datosHospitalizacion.getDatosPaciente().setCvePaciente(cvePaciente);
            } else {
                logger.debug("Paciente encontrado - cvePaciente: " + cvePaciente);
            }
            ServicioSubrogado servicio = capturarServiciosSubrogadosHelper.prepararServicio(datosHospitalizacion,
                    datosUsuario);
            capturarServiciosSubrogadosRepository.guardarServicioSubrogado(servicio, datosHospitalizacion,
                    datosUsuario);
        } catch (Exception e) {
            logger.error("Error al guardar Servicio Subrogado: ", e);
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_020), e);
        }
    }

}
