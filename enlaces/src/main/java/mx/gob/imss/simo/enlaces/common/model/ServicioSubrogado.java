package mx.gob.imss.simo.enlaces.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class ServicioSubrogado {

    private long cveServicioSubrogado;
    private String refFolioContrato;
    private long cvePaciente;
    private String cvePresupuestal;
    private int cveTipoContrato;
    private int cveTipoServicio;
    private int cveMotivoSubrogacion;
    private int cveServicioSubrogar;
    private int numCantidadEventos;
    private double numCostoUnitario;
    private String cveMedico;
    private Date fecServicio;
    private String cveCapturista;

}
