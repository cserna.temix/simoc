package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

public class InformacionComplementariaRowMapperHelper extends BaseHelper
        implements RowMapper<InformacionComplementaria> {

    @Override
    public InformacionComplementaria mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        InformacionComplementaria informacionComplementaria = new InformacionComplementaria();
        informacionComplementaria.setCveSubsistemaRcv(resultSet.getInt(SQLColumnasConstants.CVE_SUBSISTEMA_RCV));
        informacionComplementaria.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        informacionComplementaria.setCvePeriodoImss(resultSet.getString(SQLColumnasConstants.CVE_PERIODO_IMSS));
        informacionComplementaria.setNumCifraControl(resultSet.getLong(SQLColumnasConstants.NUM_CIFRA_CONTROL));
        informacionComplementaria.setFecCreacion(resultSet.getDate(SQLColumnasConstants.FEC_CREACION));
        informacionComplementaria.setCveCapturista(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA));
        informacionComplementaria.setFecActualizacion(resultSet.getDate(SQLColumnasConstants.FEC_ACTUALIZACION));
        informacionComplementaria
                .setCveCapturistaActualiza(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA_ACTUALIZA));
        informacionComplementaria.setRefCveSubsistema(resultSet.getInt(SQLColumnasConstants.REF_CVE_SUBSISTEMA));
        informacionComplementaria.setRefCveRCV(resultSet.getInt(SQLColumnasConstants.REF_CVE_RCV));

        return informacionComplementaria;
    }

}
