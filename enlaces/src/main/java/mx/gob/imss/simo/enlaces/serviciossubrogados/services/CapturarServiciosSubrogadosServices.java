package mx.gob.imss.simo.enlaces.serviciossubrogados.services;

import java.util.Map;

import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogar;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.services.HospitalizacionCommonServices;
import mx.gob.imss.simo.model.DatosUsuario;

public interface CapturarServiciosSubrogadosServices extends HospitalizacionCommonServices {

    void validarNss(String nss) throws HospitalizacionException;

    void validarEdad(String edad) throws HospitalizacionException;

    void validarNombre(String nombre) throws HospitalizacionException;

    void validarApellidoPaterno(String apellidoPaterno) throws HospitalizacionException;

    void validarNumero(String numero) throws HospitalizacionException;

    void validarTipoContrato(String tipoContrato) throws HospitalizacionException;

    void validarFolioContrato(String folioContrato) throws HospitalizacionException;

    void validarTipoServicio(String tipoServicio) throws HospitalizacionException;

    void validarMotivoSubrogacion(String motivoSubrogacion) throws HospitalizacionException;

    void validarServiciosSubrogar(String cveServiciosSubrogados) throws HospitalizacionException;

    void validarCantidadEventos(String cantidadEventos) throws HospitalizacionException;

    void validarCostoUnitario(String costoUnitario) throws HospitalizacionException;

    String validarFechaServicio(String fechaActual, String cvePresupuestal) throws HospitalizacionException;

    boolean habilitarEdad(String fechaIngreso, String substring);

    Map<TipoCalculoEdadEnum, Integer> calcularEdadPacienteNoEncontrado(String substring, String fechaIngreso);

    boolean sePuedeActivarGuardar(Boolean[] requeridos);

    ServicioSubrogar buscarServicioSubrogar(String cveServiciosSubrogados) throws HospitalizacionException;

    void cerrarPeriodoActivo(String cvePresupuestal, String cvePeriodoImss) throws HospitalizacionException;

    void guardar(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario) throws HospitalizacionException;

}
