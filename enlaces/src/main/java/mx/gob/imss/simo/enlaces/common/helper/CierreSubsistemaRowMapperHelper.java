package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.enlaces.common.model.CierreSubsistema;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

public class CierreSubsistemaRowMapperHelper extends BaseHelper implements RowMapper<CierreSubsistema> {

    @Override
    public CierreSubsistema mapRow(ResultSet resultSet, int rowId) throws SQLException {

        CierreSubsistema cierreSubsistema = new CierreSubsistema();
        cierreSubsistema.setClavePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        cierreSubsistema.setPeriodo(resultSet.getString(SQLColumnasConstants.CVE_PERIODO_IMSS));
        cierreSubsistema.setCveSubsistema(resultSet.getInt(SQLColumnasConstants.CVE_SUBSISTEMA));
        cierreSubsistema.setRefJustificacion(resultSet.getString(SQLColumnasConstants.REF_JUSTIFICACION));
        cierreSubsistema.setIndicadorCerrado(resultSet.getInt(SQLColumnasConstants.IND_CERRADO));
        cierreSubsistema.setFechaCierre(resultSet.getDate(SQLColumnasConstants.FEC_CIERRE));
        cierreSubsistema.setCveCapturista(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA));
        cierreSubsistema.setFechaActualizacion(validarFechaNula(resultSet, SQLColumnasConstants.FEC_ACTUALIZACION));
        cierreSubsistema.setCveCapturistaActualiza(resultSet.getString(SQLColumnasConstants.CVE_CAPTURISTA_ACTUALIZA));
        return cierreSubsistema;
    }

}
