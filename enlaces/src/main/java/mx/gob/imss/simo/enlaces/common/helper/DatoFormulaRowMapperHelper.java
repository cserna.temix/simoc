package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.enlaces.common.model.DatoFormula;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

public class DatoFormulaRowMapperHelper extends BaseHelper implements RowMapper<DatoFormula> {

    @Override
    public DatoFormula mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        DatoFormula datoFormula = new DatoFormula();
        datoFormula.setClave(resultSet.getLong(SQLColumnasConstants.CVE_SUBSISTEMA_RCV));
        datoFormula.setCveDato(resultSet.getInt(SQLColumnasConstants.CVE_DATO));
        datoFormula.setNumIteracionDato(resultSet.getInt(SQLColumnasConstants.NUM_ITERACION_DATO));
        datoFormula.setOperacion(resultSet.getString(SQLColumnasConstants.REF_OPERACION));
        datoFormula.setMensajeError(resultSet.getString(SQLColumnasConstants.DES_MENSAJE_ERROR));
        datoFormula.setFechaAlta(resultSet.getDate(SQLColumnasConstants.FEC_ALTA));
        datoFormula.setMesInicio(resultSet.getInt(SQLColumnasConstants.NUM_MES_INICIO));
        datoFormula.setAnioInicio(resultSet.getInt(SQLColumnasConstants.NUM_ANIO_INICIO));
        datoFormula.setMesFin(resultSet.getInt(SQLColumnasConstants.NUM_MES_FIN));
        datoFormula.setAnioFin(resultSet.getInt(SQLColumnasConstants.NUM_ANIO_FIN));
        datoFormula.setFechaBaja(validarFechaNula(resultSet, SQLColumnasConstants.FEC_BAJA));
        return datoFormula;
    }

}
