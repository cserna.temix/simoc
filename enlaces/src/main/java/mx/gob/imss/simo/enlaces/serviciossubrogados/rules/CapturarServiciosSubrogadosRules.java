package mx.gob.imss.simo.enlaces.serviciossubrogados.rules;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.enlaces.common.constant.MensajesErrorEnlacesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;

@Component
public class CapturarServiciosSubrogadosRules extends HospitalizacionCommonRules {

    public void validarEdad(String edad) throws HospitalizacionException {

        if (edad == null || edad.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_EDAD));
        }
        if (Integer.valueOf(edad) < 0 || Integer.valueOf(edad) > 103) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_138);
        }
    }

    public void validarTipoContrato(String tipoContrato) throws HospitalizacionException {

        if (tipoContrato == null || tipoContrato.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_TIPO_CONTRATO_CONVENIO));
        }
    }

    public void validarFolioContrato(String folioContrato) throws HospitalizacionException {

    	
        
    	if (folioContrato == null || folioContrato.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_FOLIO_CONTRATO_CONVENIO));
        }else if (folioContrato.length() < 14) {
        	  throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_018),
                      getArray(BaseConstants.REQUERIDO_FOLIO_CONTRATO_CONVENIO));
        }
    }

    public void validarTipoServicio(String tipoServicio) throws HospitalizacionException {

        if (tipoServicio == null || tipoServicio.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_TIPO_SERVICIO));
        }
    }

    public void validarMotivoSubrogacion(String motivoSubrogacion) throws HospitalizacionException {

        if (motivoSubrogacion == null || motivoSubrogacion.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(BaseConstants.REQUERIDO_MOTIVO_SUBROGACION));
        }
    }

    public void validarServiciosSubrogar(String claveServiciosSubrogar) throws HospitalizacionException {

        if (claveServiciosSubrogar == null || claveServiciosSubrogar.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_SERVICIOS_SUBROGAR));
        }
    }

    public void validarCantidadEventos(String cantidadEventos) throws HospitalizacionException {

        if (cantidadEventos == null || cantidadEventos.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_CANTIDAD_EVENTOS));
        } else if (cantidadEventos.equals(BaseConstants.NUMERO_CERO_STRING)) {
            throw new HospitalizacionException(getArray(MensajesErrorEnlacesConstants.ME_202),
                    getArray(BaseConstants.REQUERIDO_CANTIDAD_EVENTOS));
        }
    }

    public void validarCostoUnitario(String costoUnitario) throws HospitalizacionException {

        if (costoUnitario == null || costoUnitario.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_COSTO_UNITARIO));
        } else if (costoUnitario.equals(BaseConstants.NUMERO_CERO_STRING)) {
            throw new HospitalizacionException(getArray(MensajesErrorEnlacesConstants.ME_202),
                    getArray(BaseConstants.REQUERIDO_COSTO_UNITARIO));
        }
    }

    public void validarFechaServicio(String fechaServicio) throws HospitalizacionException {

        if (fechaServicio == null || fechaServicio.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_FECHA_SERVICIO));
        }
    }

    public boolean habilitarEdad(String fechaIngreso, String anio) {

        int anioAgregado = Integer.parseInt(anio);
        int anioIngreso = Integer.parseInt(fechaIngreso.substring(6));
        if (anioAgregado == anioIngreso || (anioAgregado == (anioIngreso - 1))) {
            return true;
        }
        return false;
    }

    public boolean sePuedeActivarGuardar(Boolean[] requeridos) {

        for (boolean req : requeridos) {
            if (!req) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

}
