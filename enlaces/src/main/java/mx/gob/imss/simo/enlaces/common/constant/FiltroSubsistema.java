package mx.gob.imss.simo.enlaces.common.constant;

public enum FiltroSubsistema {
    /*
     * Los subsistemas 14 y 19 no se deben de contemplar para Informacion Complementaria (Se eliminaron de BD)
     * El subsistema 43 s�lo debe de permitirse capturar en el mes de Marzo
     */
    SUBSISTEMA_14(14, "Subsistema 14"), SUBSISTEMA_43(43, "Subsistema 43");

    private int clave;
    private String descripcion;

    private FiltroSubsistema(int clave, String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

    public int getClave() {

        return clave;
    }

    public String getDescripcion() {

        return descripcion;
    }

    public static FiltroSubsistema find(int clave) {

        FiltroSubsistema right = null;
        for (FiltroSubsistema item : values()) {
            if (item.getClave() == clave) {
                right = item;
                break;
            }
        }
        return right;
    }

}
