package mx.gob.imss.simo.enlaces.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class SubsistemaRCVDato {

    private int clave;
    private int cveDato;
    private String refConcepto;
    private String refModulo;
    private Date fecAlta;
    private Date fecBaja;

}
