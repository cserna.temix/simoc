package mx.gob.imss.simo.enlaces.common.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class FormulaInformacionComplementaria {

    private String regla;
    private String mensajeError;
    private Boolean valida;
    private boolean compuesta;
    private List<List<String>> reglasCompuestas;

    public FormulaInformacionComplementaria() {
        this.regla = "";
        this.mensajeError = "";
        this.compuesta = false;
        this.reglasCompuestas = new ArrayList<List<String>>();
    }

    @Override
    public String toString() {

        return "FormulaInformacionComplementaria [regla=" + regla + ", compuesta=" + compuesta + ", reglasCompuestas="
                + reglasCompuestas + ", mensajeError=" + mensajeError + ", valida=" + valida + "]";
    }

}
