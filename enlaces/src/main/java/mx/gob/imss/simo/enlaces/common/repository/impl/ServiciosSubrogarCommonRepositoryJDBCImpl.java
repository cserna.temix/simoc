package mx.gob.imss.simo.enlaces.common.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.enlaces.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoMotivoSubrogacionRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoTipoContratoRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoTipoServicioRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.repository.ServiciosSubrogarCommonRepository;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.CatalogosHospitalizacionRepositoryJDBCImpl;

@Repository
public class ServiciosSubrogarCommonRepositoryJDBCImpl extends CatalogosHospitalizacionRepositoryJDBCImpl
        implements ServiciosSubrogarCommonRepository {

    /**
     * Informacion Complementaria
     */

    /**
     * Servicios Subrogados
     */

    @Override
    public List<DatosCatalogo> obtenerCatalogoMotivoSubrogacion() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_MOTIVO_SUBROGACION,
                new CatalogoMotivoSubrogacionRowMapperHelper());
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoTipoContrato() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_TIPO_CONTRATO,
                new CatalogoTipoContratoRowMapperHelper());
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoTipoServicio() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_TIPO_SERVICIO,
                new CatalogoTipoServicioRowMapperHelper());
    }
}
