package mx.gob.imss.simo.enlaces.serviciossubrogados.helper;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogado;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.Paciente;
import mx.gob.imss.simo.model.DatosUsuario;

@Component
public class CapturarServiciosSubrogadosHelper extends HospitalizacionCommonHelper {

    public Paciente prepararPaciente(DatosPaciente datosPaciente) {

        String regimen = datosPaciente.getAgregadoMedico().substring(BaseConstants.INICIO_AGREGADO_REGIMEN);
        Paciente paciente = new Paciente();
        paciente.setCveIdPersona(datosPaciente.getIdPersona());
        paciente.setRefNss(datosPaciente.getNss());
        paciente.setRefAgregadoMedico(datosPaciente.getAgregadoMedico());
        paciente.setRefNombre(datosPaciente.getNombre());
        paciente.setRefApellidoPaterno(datosPaciente.getApellidoPaterno());
        paciente.setRefApellidoMaterno(datosPaciente.getApellidoMaterno());
        paciente.setFecConsulta(datosPaciente.getFechaConsulta());
        paciente.setFecNacimiento(datosPaciente.getFechaNacimiento());
        paciente.setNumEdadSemanas(datosPaciente.getEdadSemanas());
        paciente.setNumEdadAnios(datosPaciente.getEdadAnios());
        paciente.setCveCurp(datosPaciente.getCurp());
        paciente.setCveIdee(datosPaciente.getIdee());
        paciente.setIndAcceder(datosPaciente.isMarcaAcceder());
        paciente.setIndVigencia(datosPaciente.isVigencia());
        paciente.setCveSexo(datosPaciente.getSexo());
        paciente.setRefTipoPension(datosPaciente.getTipoPension());
        paciente.setRefRegistroPatronal(datosPaciente.getRegistroPatronal());
        paciente.setCveUnidadAdscripcion(datosPaciente.getClavePresupuestal());
        paciente.setIndDerechohabiente(
                regimen.equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave()) ? false : true);
        paciente.setIndUltimoRegistro(true);
        return paciente;
    }

    public ServicioSubrogado prepararServicio(DatosHospitalizacion datosHospitalizacion, DatosUsuario datosUsuario) {

        ServicioSubrogado servicioSubrogado = new ServicioSubrogado();
        servicioSubrogado.setRefFolioContrato(datosHospitalizacion.getDatosSubrogados().getFolioContrato());
        servicioSubrogado.setCvePaciente(datosHospitalizacion.getDatosPaciente().getCvePaciente());
        servicioSubrogado.setCvePresupuestal(datosUsuario.getCvePresupuestal());
        servicioSubrogado
                .setCveTipoContrato(Integer.parseInt(datosHospitalizacion.getDatosSubrogados().getTipoContrato()));
        servicioSubrogado
                .setCveTipoServicio(Integer.parseInt(datosHospitalizacion.getDatosSubrogados().getTipoServicio()));
        servicioSubrogado.setCveMotivoSubrogacion(
                Integer.parseInt(datosHospitalizacion.getDatosSubrogados().getMotivoSubrogacion()));
        servicioSubrogado.setCveServicioSubrogar(
                Integer.parseInt(datosHospitalizacion.getDatosSubrogados().getClaveServicioSubrogar()));
        servicioSubrogado.setNumCantidadEventos(
                Integer.parseInt(datosHospitalizacion.getDatosSubrogados().getCantidadEventos()));
        servicioSubrogado
                .setNumCostoUnitario(Double.parseDouble(datosHospitalizacion.getDatosSubrogados().getCostoUnitario()));
        servicioSubrogado.setCveMedico((datosHospitalizacion.getDatosMedico().getMatricula()));
        servicioSubrogado
                .setFecServicio(convertStringToDate(datosHospitalizacion.getDatosSubrogados().getFechaServicio()));
        servicioSubrogado.setCveCapturista(datosUsuario.getUsuario());
        return servicioSubrogado;
    }

}
