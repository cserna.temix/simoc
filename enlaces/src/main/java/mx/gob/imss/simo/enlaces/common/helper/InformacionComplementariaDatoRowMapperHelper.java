package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaDato;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

public class InformacionComplementariaDatoRowMapperHelper extends BaseHelper
        implements RowMapper<InformacionComplementariaDato> {

    @Override
    public InformacionComplementariaDato mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        InformacionComplementariaDato informacionComplementariaDato = new InformacionComplementariaDato();
        informacionComplementariaDato.setCveSubsistemaRcv(resultSet.getInt(SQLColumnasConstants.CVE_SUBSISTEMA_RCV));
        informacionComplementariaDato.setCvePresupuestal(resultSet.getString(SQLColumnasConstants.CVE_PRESUPUESTAL));
        informacionComplementariaDato.setCvePeriodoImss(resultSet.getString(SQLColumnasConstants.CVE_PERIODO_IMSS));
        informacionComplementariaDato.setCveDato(resultSet.getInt(SQLColumnasConstants.CVE_DATO));
        informacionComplementariaDato.setNumValorDato(resultSet.getInt(SQLColumnasConstants.NUM_VALOR_DATO));
        return informacionComplementariaDato;
    }

}
