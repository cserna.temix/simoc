package mx.gob.imss.simo.enlaces.informacioncomplementaria.services.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.imss.simo.enlaces.common.constant.CapturarInformacionComplementariaConstants;
import mx.gob.imss.simo.enlaces.common.constant.FiltroSubsistema;
import mx.gob.imss.simo.enlaces.common.model.CierreSubsistema;
import mx.gob.imss.simo.enlaces.common.model.ComponenteCatalogoInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.DatoFormula;
import mx.gob.imss.simo.enlaces.common.model.FormulaInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaConsulta;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaDato;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCVDato;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.helper.CapturarInformacionComplementariaHelper;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.repository.CapturarInformacionComplementariaRepository;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.rules.CapturarInformacionComplementariaRules;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.services.CapturarInformacionComplementariaServices;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.EstructuraDatoInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.Parametro;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.services.impl.HospitalizacionCommonServicesImpl;
import mx.gob.imss.simo.model.DatosUsuario;

@Service("capturarInformacionComplementariaServices")
public class CapturarInformacionComplementariaServicesImpl extends HospitalizacionCommonServicesImpl
        implements CapturarInformacionComplementariaServices {

    protected final Logger logger = Logger.getLogger(getClass());

    @Autowired
    private CapturarInformacionComplementariaRules capturarInformacionComplementariaRules;
    @Autowired
    private CapturarInformacionComplementariaHelper capturarInformacionComplementariaHelper;
    @Autowired
    private CapturarInformacionComplementariaRepository capturarInformacionComplementariaRepository;

    @Override
    public void validarPeriodo(String periodo, String clavePresupuestal) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarFormatoPeriodo(periodo);
        String fechaAtencion = capturarInformacionComplementariaHelper.prepararFechaPeriodo(periodo);
        capturarInformacionComplementariaRules.validarFormatoFecha(fechaAtencion);
        validarPeriodoAnteriorCerrado(clavePresupuestal, fechaAtencion,
                TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave());
        validarFechaEnPeriodoActualActivo(fechaAtencion,
                capturarInformacionComplementariaHelper.convertStringToDate(fechaAtencion), clavePresupuestal,
                TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave());
    }

    @Override
    public void validarSubsistema(Object cveSubsistema) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarSubsistema(cveSubsistema);
    }

    @Override
    public void validarRCV(String cveRCV) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarRCV(cveRCV);
    }

    @Override
    public void validarPoblacion(Integer cvePoblacion) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarPoblacion(cvePoblacion);

    }

    @Override
    public void validarPor(Integer cvePor) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarPor(cvePor);

    }

    @Override
    public void validarEn(Integer cveEn) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarEn(cveEn);

    }

    @Override
    public void validarHoja(Integer cveHoja) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarHoja(cveHoja);

    }

    @Override
    public void validarSubclave(Integer cveSubclave) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarSubclave(cveSubclave);

    }

    @Override
    public void validarJustificacion(String justificacion) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarJustificacion(justificacion);
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoSubsistema(String cvePeriodo) {

        List<DatosCatalogo> catalogoSubsistema = capturarInformacionComplementariaRepository
                .obtenerCatalogoSubsistemas();
        List<DatosCatalogo> catalogoSubsistemaFiltro = new ArrayList<DatosCatalogo>();
        for (DatosCatalogo catalogo : catalogoSubsistema) {
            if (null == FiltroSubsistema.find(Integer.parseInt(catalogo.getClave()))) {
                catalogoSubsistemaFiltro.add(catalogo);
            }
            if (catalogo.getClave().equals(Integer.toString(FiltroSubsistema.SUBSISTEMA_43.getClave()))
                    && capturarInformacionComplementariaRules.validarMesSubsistemaMarzo(cvePeriodo)) {
                catalogoSubsistemaFiltro.add(catalogo);
            }
        }
        return catalogoSubsistemaFiltro;
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoRCV(int cveSubsistema) {

        List<DatosCatalogo> catalogoRCV = capturarInformacionComplementariaRepository.obtenerCatalogoRCV(cveSubsistema);
        return capturarInformacionComplementariaRules.ordenarCifra99(catalogoRCV);
    }

    @Override
    public boolean existeInformacionComplementaria(int cveSubsistema, String cvePresupuestal, String cvePeriodoIMSS) {

        String cvePeriodo = capturarInformacionComplementariaHelper.ordenarPeriodo(cvePeriodoIMSS);
        long registrosInformacionComplementaria = capturarInformacionComplementariaRepository
                .obtenerCountInformacionComplementariaPorSubsistema(cveSubsistema, cvePresupuestal, cvePeriodo);
        if (registrosInformacionComplementaria > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean sePuedeActivarComponentes(Boolean[] requeridos) {

        if (Boolean.TRUE == requeridos[0] && Boolean.TRUE == requeridos[1]) {
            return true;
        }
        return false;
    }

    @Override
    public DatosInformacionComplementaria consultarSubsistemaRCV(DatosInformacionComplementaria datosInfoComplementaria,
            String periodo, String cvePresupuestal) {

        String cvePeriodo = capturarInformacionComplementariaHelper.ordenarPeriodo(periodo);

        SubsistemaRCV informacionComplementaria = capturarInformacionComplementariaRepository.obtenerSubsistemaRCV(
                datosInfoComplementaria.getCveSubsistema(), datosInfoComplementaria.getCveRCV(),
                datosInfoComplementaria.getCvePoblacion(), datosInfoComplementaria.getCvePor(),
                datosInfoComplementaria.getCveEn(), datosInfoComplementaria.getCveHoja(),
                datosInfoComplementaria.getCveSubclave());
        List<SubsistemaRCVDato> informacionComplementariaDatosList = capturarInformacionComplementariaRepository
                .obtenerSubsistemaRCVDato(informacionComplementaria.getClave());

        InformacionComplementaria informacionExistente = capturarInformacionComplementariaRepository
                .buscarInformacionExistente(informacionComplementaria.getClave(), cvePresupuestal, cvePeriodo,
                        datosInfoComplementaria.getCveSubsistema(), datosInfoComplementaria.getCveRCV());
        return capturarInformacionComplementariaHelper.armarDatosInformacionComplementaria(datosInfoComplementaria,
                informacionComplementariaDatosList, cvePresupuestal, cvePeriodo, informacionExistente,
                informacionComplementaria.getDesSubsistemaRCV());
    }

    @Override
    public boolean sePuedeConsultarSubsistema(Boolean[] catalogosRequeridosVista, Boolean[] catalogosRequeridos) {

        return capturarInformacionComplementariaRules.sePuedeConsultarSubsistema(catalogosRequeridosVista,
                catalogosRequeridos);
    }

    @Override
    public Subsistema obtenerSubsistema(int subsistema) {

        return capturarInformacionComplementariaRepository.obtenerSubsistema(subsistema);
    }

    @Override
    public boolean ultimoCatalogoRequerido(Subsistema subsistema, String comboId, String[] componentes) {

        List<ComponenteCatalogoInformacionComplementaria> componenteCatalogoInfComplementariaList = capturarInformacionComplementariaHelper
                .generarComponenteCatalogoInfComplementariaList(subsistema, componentes);

        return capturarInformacionComplementariaRules.ultimoCatalogoRequerido(componenteCatalogoInfComplementariaList,
                comboId);
    }

    @Override
    public List<SubsistemaRCV> obtenerCatalogosInformacionComplementaria(long clave, int cveRCV) {

        return capturarInformacionComplementariaRepository.obtenerCatalogosInformacionComplementaria(clave, cveRCV);
    }

    @Override
    public List<DatosCatalogo> armarDatosCatalogoPoblacion(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        return capturarInformacionComplementariaHelper
                .armarDatosCatalogoPoblacion(catalogosInformacionComplementariaList);
    }

    @Override
    public List<DatosCatalogo> armarDatosCatalogoPor(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        return capturarInformacionComplementariaHelper.armarDatosCatalogoPor(catalogosInformacionComplementariaList);
    }

    @Override
    public List<DatosCatalogo> armarDatosCatalogoEn(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        return capturarInformacionComplementariaHelper.armarDatosCatalogoEn(catalogosInformacionComplementariaList);
    }

    @Override
    public List<DatosCatalogo> armarDatosCatalogoHoja(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        return capturarInformacionComplementariaHelper.armarDatosCatalogoHoja(catalogosInformacionComplementariaList);
    }

    @Override
    public List<DatosCatalogo> armarDatosCatalogoSubclave(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        return capturarInformacionComplementariaHelper
                .armarDatosCatalogoSubclave(catalogosInformacionComplementariaList);
    }

    @Override
    public void validarDato(Integer valorDato) throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarDato(valorDato);

    }

    @Override
    public List<FormulaInformacionComplementaria> obtenerFormulasAEjecutar(long cveSubsistemaRCV,
            List<EstructuraDatoInformacionComplementaria> datos) {

        List<FormulaInformacionComplementaria> formulaInformacionComplementariaList = new ArrayList<FormulaInformacionComplementaria>();
        for (EstructuraDatoInformacionComplementaria estructuraDatoInformacionComplementaria : datos) {
            List<DatoFormula> datoFormulaList = capturarInformacionComplementariaRepository
                    .obtenerDatoFormula(cveSubsistemaRCV, estructuraDatoInformacionComplementaria.getClave());
            for (DatoFormula datoFormula : datoFormulaList) {
                formulaInformacionComplementariaList.add(capturarInformacionComplementariaHelper
                        .transformarDatoFormulaToFormulaInformacionComplementaria(datoFormula));
            }
        }

        return capturarInformacionComplementariaHelper
                .transformarModeloAReglaCompuesta(formulaInformacionComplementariaList);
    }

    @Override
    public SubsistemaRCV obtenerSubsistemaRCV(DatosInformacionComplementaria datosInfoComplementaria) {

        return capturarInformacionComplementariaRepository.obtenerSubsistemaRCV(
                datosInfoComplementaria.getCveSubsistema(), datosInfoComplementaria.getCveRCV(),
                datosInfoComplementaria.getCvePoblacion(), datosInfoComplementaria.getCvePor(),
                datosInfoComplementaria.getCveEn(), datosInfoComplementaria.getCveHoja(),
                datosInfoComplementaria.getCveSubclave());
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void guardarInfoComplementaria(int cveSubsistemaRcv, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria,
            boolean isVisibleCifraDeControl /* si es falso es cifra de control 99 */,
            boolean datosIgualACero) {

        datosInformacionComplementaria.setCvePeriodo(
                capturarInformacionComplementariaHelper.ordenarPeriodo(datosInformacionComplementaria.getCvePeriodo()));
        InformacionComplementaria informacionExistente = capturarInformacionComplementariaRepository
                .buscarInformacionExistente(cveSubsistemaRcv, datosUsuario.getCvePresupuestal(),
                        datosInformacionComplementaria.getCvePeriodo(),
                        datosInformacionComplementaria.getCveSubsistema(), datosInformacionComplementaria.getCveRCV());
        CierreSubsistema cierreSubsistemaExistente = capturarInformacionComplementariaRepository
                .buscarCierreSubsistemaExistente(datosUsuario.getCvePresupuestal(),
                        datosInformacionComplementaria.getCvePeriodo(),
                        datosInformacionComplementaria.getCveSubsistema());
        if (informacionExistente == null) {
            if (!isVisibleCifraDeControl) {
                datosInformacionComplementaria
                        .setCifraDeControl(Long.parseLong(datosInformacionComplementaria.getDatos().get(0).getValor()));
                guardarInformacionComplentaria(cveSubsistemaRcv, datosUsuario, datosInformacionComplementaria);
                if (null == cierreSubsistemaExistente) {
                    guardarCierreSubsistema(datosUsuario, datosInformacionComplementaria);
                } else {
                    actualizarCierreSubsistema(cierreSubsistemaExistente, datosUsuario, datosInformacionComplementaria);
                }

            } else {            	
            	//si no existe y los datos son 0 entonces no guardar
            	if(!datosIgualACero){
            		guardarInformacionComplentaria(cveSubsistemaRcv, datosUsuario, datosInformacionComplementaria);
            	}
            }
        } else {
            if (!isVisibleCifraDeControl) {

                datosInformacionComplementaria
                        .setCifraDeControl(Long.parseLong(datosInformacionComplementaria.getDatos().get(0).getValor()));
                actualizarInformacionComplementaria(informacionExistente, datosUsuario, datosInformacionComplementaria);
                if (null == cierreSubsistemaExistente) {
                    guardarCierreSubsistema(datosUsuario, datosInformacionComplementaria);
                } else {
                    actualizarCierreSubsistema(cierreSubsistemaExistente, datosUsuario, datosInformacionComplementaria);
                }
            } else {
            	//si ya existe la info en la base y los datos ahora son 0, se eliminan los registros del RCV
            	if(!datosIgualACero){
                    actualizarInformacionComplementaria(informacionExistente, datosUsuario, datosInformacionComplementaria);
            	} else{
            		capturarInformacionComplementariaRepository.eliminarInformacionComplementariaDato(
            				cveSubsistemaRcv, datosUsuario.getCvePresupuestal(),
            				datosInformacionComplementaria.getCvePeriodo());
                    capturarInformacionComplementariaRepository.eliminarInformacionComplementaria(
                    		cveSubsistemaRcv, datosUsuario.getCvePresupuestal(),
                    		datosInformacionComplementaria.getCvePeriodo());
            	}
            }
        }
    }

    @Override
    public List<FormulaInformacionComplementaria> validarFormulaDato(long cveSubsistemaRCV,
            DatosInformacionComplementaria datosInfo) {

        List<FormulaInformacionComplementaria> formulaInformacionComplementariaList = obtenerFormulasAEjecutar(
                cveSubsistemaRCV, datosInfo.getDatos());

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");

        String regla = "";
        int i = 0;

        for (FormulaInformacionComplementaria formulaInformacionComplementaria : formulaInformacionComplementariaList) {
            try {

                if (formulaInformacionComplementaria.isCompuesta()) {
                    for (List<String> listaFormulas : formulaInformacionComplementaria.getReglasCompuestas()) {
                        for (String reglaDescompuesta : listaFormulas) {
                            regla = capturarInformacionComplementariaHelper
                                    .transformarFormulaJavascript(reglaDescompuesta, datosInfo);
                            Boolean obj;
                            obj = (Boolean) engine.eval(regla);
                            formulaInformacionComplementaria.setValida(obj);
                            if (obj) {
                                break;
                            }
                        }
                    }
                    formulaInformacionComplementariaList.set(i, formulaInformacionComplementaria);
                    i++;
                } else {
                    regla = capturarInformacionComplementariaHelper
                            .transformarFormulaJavascript(formulaInformacionComplementaria.getRegla(), datosInfo);
                    formulaInformacionComplementaria.setRegla(formulaInformacionComplementaria.getRegla());
                    Boolean obj;
                    obj = (Boolean) engine.eval(regla);
                    formulaInformacionComplementaria.setValida(obj);
                    formulaInformacionComplementariaList.set(i, formulaInformacionComplementaria);
                    i++;
                }

            } catch (ScriptException e) {
                logger.error("Error al ejecutar la formula javascript");
            }
        }

        return formulaInformacionComplementariaList;
    }

    @Override
    public void validarCifradeControl(DatosInformacionComplementaria datosInformacionComplementaria)
            throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarCifradeControl(datosInformacionComplementaria);
    }

    @Override
    public void validarCifradeControlSimple(DatosInformacionComplementaria datosInformacionComplementaria)
            throws HospitalizacionException {

        capturarInformacionComplementariaRules.validarCifradeControlSimple(datosInformacionComplementaria);
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void guardarInfoComplementariaPorJustificacion(boolean existeInformacionComplementaria,
            DatosUsuario datosUsuario, DatosInformacionComplementaria datosInformacionComplementaria) {

        String cvePeriodoAux = capturarInformacionComplementariaHelper
                .ordenarPeriodo(datosInformacionComplementaria.getCvePeriodo());
        if (existeInformacionComplementaria) {
            List<InformacionComplementaria> informacionComplementariaList = capturarInformacionComplementariaRepository
                    .consultarInformacionComplementariaPorSubsistema(datosUsuario.getCvePresupuestal(), cvePeriodoAux,
                            datosInformacionComplementaria.getCveSubsistema());

            for (InformacionComplementaria infoComplementaria : informacionComplementariaList) {
                capturarInformacionComplementariaRepository.eliminarInformacionComplementariaDato(
                        infoComplementaria.getCveSubsistemaRcv(), infoComplementaria.getCvePresupuestal(),
                        infoComplementaria.getCvePeriodoImss());
                capturarInformacionComplementariaRepository.eliminarInformacionComplementaria(
                        infoComplementaria.getCveSubsistemaRcv(), infoComplementaria.getCvePresupuestal(),
                        infoComplementaria.getCvePeriodoImss());
            }
        }

        CierreSubsistema cierreSubsistema = capturarInformacionComplementariaRepository.buscarCierreSubsistemaExistente(
                datosUsuario.getCvePresupuestal(), cvePeriodoAux, datosInformacionComplementaria.getCveSubsistema());
        if (null != cierreSubsistema) {
            CierreSubsistema cierreSubsistemaAux = capturarInformacionComplementariaHelper
                    .prepararActualizarCierreSubsistema(cierreSubsistema, datosUsuario.getUsuario(),
                            datosInformacionComplementaria);
            capturarInformacionComplementariaRepository.actualizarCierreSubsistema(cierreSubsistemaAux);
        } else {
            datosInformacionComplementaria.setCvePeriodo(capturarInformacionComplementariaHelper
                    .ordenarPeriodo(datosInformacionComplementaria.getCvePeriodo()));
            cierreSubsistema = capturarInformacionComplementariaHelper.prepararCierreSubsistema(datosUsuario,
                    datosInformacionComplementaria);
            capturarInformacionComplementariaRepository.guardarCierreSubsistema(cierreSubsistema);
        }

    }

    @Override
    public Map<String, Boolean> cargarSubsistemas(List<DatosCatalogo> catalogoSubsistema,
            DatosInformacionComplementaria datosInformacionComplementaria, String cvePresupuestal) {

        Map<String, Boolean> mapaSubsistemas = new LinkedHashMap<String, Boolean>();

        List<CierreSubsistema> subsistemasCerrados = capturarInformacionComplementariaRepository
                .obtenerSubsistemasCerrados(cvePresupuestal, capturarInformacionComplementariaHelper
                        .ordenarPeriodo(datosInformacionComplementaria.getCvePeriodo()));

        for (DatosCatalogo catalogo : catalogoSubsistema) {
            mapaSubsistemas.put(catalogo.getClave(), Boolean.FALSE);

            for (CierreSubsistema subsistema : subsistemasCerrados) {
                if (catalogo.getClave().equals(Integer.toString(subsistema.getCveSubsistema()))) {
                    mapaSubsistemas.replace(catalogo.getClave(), Boolean.TRUE);

                }
            }
        }
        return mapaSubsistemas;
    }

    @Override
    public boolean validarSubsistemaCerrado(DatosInformacionComplementaria datosInformacionComplementaria,
            String cvePresupuestal, String claveSubsistema) {

        List<CierreSubsistema> subsistemasCerrados = capturarInformacionComplementariaRepository
                .obtenerSubsistemasCerrados(cvePresupuestal, capturarInformacionComplementariaHelper
                        .ordenarPeriodo(datosInformacionComplementaria.getCvePeriodo()));
        Boolean esCerrado = Boolean.FALSE;
        for (CierreSubsistema subsistema : subsistemasCerrados) {
            if (claveSubsistema.equals(Integer.toString(subsistema.getCveSubsistema()))) {
                esCerrado = Boolean.TRUE;
                // break;
            }
        }
        return esCerrado;
    }

    @Override
    public List<InformacionComplementariaConsulta> informacionComplementariaConsulta(DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria) {

        String cvePeriodoAux = capturarInformacionComplementariaHelper
                .ordenarPeriodo(datosInformacionComplementaria.getCvePeriodo());

        List<InformacionComplementariaConsulta> informacionComplementariaConsultaList = new ArrayList<InformacionComplementariaConsulta>();
        List<InformacionComplementaria> informacionComplementariaList = capturarInformacionComplementariaRepository
                .consultarInformacionComplementariaPorSubsistemaAsc(datosUsuario.getCvePresupuestal(), cvePeriodoAux,
                        datosInformacionComplementaria.getCveSubsistema());
        List<SubsistemaRCVDato> listaSubsistemaRCVDato = new ArrayList<>();
        if (null != informacionComplementariaList) {
            for (InformacionComplementaria informacionComplementaria : informacionComplementariaList) {
            	if(datosInformacionComplementaria.getCveSubsistema() == 10 ||
            			datosInformacionComplementaria.getCveSubsistema() == 12 || 
            			datosInformacionComplementaria.getCveSubsistema() == 19){
            		listaSubsistemaRCVDato = capturarInformacionComplementariaRepository
                			.obtenerDatoPorSubsistemaRCV(informacionComplementaria.getRefCveSubsistema(), 
                					informacionComplementaria.getRefCveRCV());
            	}      	
            	List<InformacionComplementariaDato> informacionComplementariaDatoList = capturarInformacionComplementariaRepository
                        .consultarInformacionComplementariaDatoPorSubsistemaRCV(datosUsuario.getCvePresupuestal(),
                                cvePeriodoAux, informacionComplementaria.getCveSubsistemaRcv());
                InformacionComplementariaConsulta informacionComplementariaConsulta = capturarInformacionComplementariaHelper
                        .armarDatosInformacionAConsulta(informacionComplementaria, informacionComplementariaDatoList,
                        		listaSubsistemaRCVDato);
                informacionComplementariaConsultaList.add(informacionComplementariaConsulta);
            }
            informacionComplementariaConsultaList = capturarInformacionComplementariaHelper
                    .prepararDatosConsultaComp(informacionComplementariaConsultaList);
        }
        return informacionComplementariaConsultaList;
    }

    @Override
    public void validarCapturaCifra99(int cveSubsistema, String cvePresupuestal, String cvePeriodoImss)
            throws HospitalizacionException {

        String cvePeriodo = capturarInformacionComplementariaHelper.ordenarPeriodo(cvePeriodoImss);
        SubsistemaRCV subsistemaRCV = capturarInformacionComplementariaRepository
                .obtenerCantidadRCVParaCifra99(cveSubsistema);
        Long totalRCV = capturarInformacionComplementariaRepository
                .obtenerTotalRCVCapturadosPorSubsistema(cveSubsistema, cvePresupuestal, cvePeriodo);

        capturarInformacionComplementariaRules.validarCapturaCifra99(totalRCV.intValue(), subsistemaRCV.getTotalRCV());
    }

    private void guardarInformacionComplentaria(int cveSubsistemaRcv, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria) {

        InformacionComplementaria informacionComplementaria = capturarInformacionComplementariaHelper
                .prepararInformacionComplementaria(cveSubsistemaRcv, datosUsuario, datosInformacionComplementaria);
        capturarInformacionComplementariaRepository.guardarInformacionComplementaria(informacionComplementaria,
                datosInformacionComplementaria.getCveSubsistema(), datosInformacionComplementaria.getCveRCV());
        List<InformacionComplementariaDato> datosInfromacionComplementaria = capturarInformacionComplementariaHelper
                .preparaDatosInformacionComplementaria(cveSubsistemaRcv, datosUsuario, datosInformacionComplementaria);
        capturarInformacionComplementariaRepository
                .guardarDatosInformacionComplementaria(datosInfromacionComplementaria);
    }

    @Override
    public void actualizarInformacionComplementaria(InformacionComplementaria informacionExistente,
            DatosUsuario datosUsuario, DatosInformacionComplementaria datosInformacionComplementaria) {

        informacionExistente = capturarInformacionComplementariaHelper.actualizarInformacionComplementaria(
                informacionExistente, datosUsuario.getUsuario(), datosInformacionComplementaria.getCifraDeControl());
        capturarInformacionComplementariaRepository.actualizarInformacionComplementaria(informacionExistente);
        List<InformacionComplementariaDato> capturarInformacionComplementariaDato = capturarInformacionComplementariaRepository
                .obtenerDatosInformacionComplementaria(informacionExistente);
        List<InformacionComplementariaDato> datosActualizados = capturarInformacionComplementariaHelper
                .prepararDatosActualizados(capturarInformacionComplementariaDato,
                        datosInformacionComplementaria.getDatos());
        capturarInformacionComplementariaRepository.actualizarDatosInformacionComplementaria(datosActualizados);
    }

    @Override
    public void validarCifraControl99(int cveSubsistema, Long cifraControl99, String cvePresupuestal, String cvePeriodo)
            throws HospitalizacionException {

        String cvePeriodoAux = capturarInformacionComplementariaHelper.ordenarPeriodo(cvePeriodo);
        List<InformacionComplementaria> informacionComplementariaList = capturarInformacionComplementariaRepository
                .obtenerCifraControlPorSubsistema(cveSubsistema, cvePresupuestal, cvePeriodoAux);
        capturarInformacionComplementariaRules.validarCifraControl99(informacionComplementariaList, cifraControl99,
                cveSubsistema);
    }

    @Override
    public String descomponerCadenaFocoParaErrorCifraCOntrol(String componenteCifraControl99) {

        return capturarInformacionComplementariaHelper
                .descomponerCadenaFocoParaErrorCifraCOntrol(componenteCifraControl99);
    }

    private void guardarCierreSubsistema(DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria) {

        CierreSubsistema cierreSubsistema = capturarInformacionComplementariaHelper
                .prepararCierreSubsistema(datosUsuario, datosInformacionComplementaria);
        capturarInformacionComplementariaRepository.guardarCierreSubsistema(cierreSubsistema);

    }

    private void actualizarCierreSubsistema(CierreSubsistema cierreSubsistemaExistente, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria) {

        cierreSubsistemaExistente = capturarInformacionComplementariaHelper.prepararActualizarCierreSubsistema(
                cierreSubsistemaExistente, datosUsuario.getUsuario(), datosInformacionComplementaria);

        capturarInformacionComplementariaRepository.actualizarCierreSubsistema(cierreSubsistemaExistente);
    }

    @Override
    public void abrirSubsistema(String cvePresupuestal, String cvePeriodo, int cveSubsistema, String usuarioActualiza) {

        capturarInformacionComplementariaRepository.abrirSubsistema(cvePresupuestal, cvePeriodo, cveSubsistema,
                usuarioActualiza);

    }

    @Override
    public void actualizarCifraControlInformacionComplementariaPorCierre(
            InformacionComplementaria informacionComplementariaPorCierre) {

    	 capturarInformacionComplementariaRepository
         .actualizarCifraControlInformacionComplementariaDatoPorCierre(informacionComplementariaPorCierre);

    	
        capturarInformacionComplementariaRepository
                .actualizarCifraControlInformacionComplementariaPorCierre(informacionComplementariaPorCierre);
         }

    @Override
    public InformacionComplementaria buscarInformacionExistente(int cveSubsistemaRcv, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria) {

        return capturarInformacionComplementariaRepository.buscarInformacionExistente(cveSubsistemaRcv,
                datosUsuario.getCvePresupuestal(), datosInformacionComplementaria.getCvePeriodo(),
                datosInformacionComplementaria.getCveSubsistema(), datosInformacionComplementaria.getCveRCV());
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void controlMapaCierre(String cvePresupuestal, String cvePeriodo, int cveSubsistema,
            String usuarioActualiza) {

        String cvePeriodoAux = capturarInformacionComplementariaHelper.ordenarPeriodo(cvePeriodo);
        abrirSubsistema(cvePresupuestal, cvePeriodoAux, cveSubsistema, usuarioActualiza);
        InformacionComplementaria informacionComplementariaPorCierre = capturarInformacionComplementariaRepository
                .buscarInformacionComplementariaPorCierre(cvePresupuestal, cvePeriodoAux, cveSubsistema);
        if (null != informacionComplementariaPorCierre) {
            actualizarCifraControlInformacionComplementariaPorCierre(informacionComplementariaPorCierre);
        }

    }

    @Override
    public boolean validarCierrePeriodo(String cvePeriodo, String cvePresupuestal) {

        Parametro parametro = capturarInformacionComplementariaRepository.obtenerParamtro(
                CapturarInformacionComplementariaConstants.PARAMETRO_NUM_SUBSISTEMAS_INF_COMPLEMENTARIA);

        String periodoOrdenado = capturarInformacionComplementariaHelper.ordenarPeriodo(cvePeriodo);

        int totalSubsistemasCerrados = capturarInformacionComplementariaRepository
                .obtenerTotalSubsistemasCerradosPorPresupuestal(periodoOrdenado, cvePresupuestal).intValue();

        int mesPeriodo = Integer.parseInt(cvePeriodo.substring(1, 2));
        int referenciaParametro = Integer.parseInt(parametro.getReferenciaParametro());

        if (mesPeriodo == CapturarInformacionComplementariaConstants.MES_MARZO) {
            return capturarInformacionComplementariaRules.sePuedeCerrarPeriodo(referenciaParametro,
                    totalSubsistemasCerrados);
        } else {

            return capturarInformacionComplementariaRules.sePuedeCerrarPeriodo(referenciaParametro - 1,
                    totalSubsistemasCerrados);

        }
    }
    
    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void cerrarPeriodo(String clavePresupuestal, int tipoCaptura, String clavePeriodoImss, 
    		boolean isIndicadorMonitoreo)
            throws HospitalizacionException {
    	
    	String cvePeriodoAux = capturarInformacionComplementariaHelper.ordenarPeriodo(clavePeriodoImss);
        capturarInformacionComplementariaRepository.cerrarPeriodo(clavePresupuestal, tipoCaptura, cvePeriodoAux);

        if(isIndicadorMonitoreo){
        	capturarInformacionComplementariaRepository.actualizarIndicadorMonitoreo(clavePresupuestal, tipoCaptura, cvePeriodoAux);        	
        } else{
        	PeriodosImss periodoSiguiente = capturarInformacionComplementariaRepository
                    .obtenerSiguientePeriodoImss(cvePeriodoAux);
            String periodoSiguienteStr = periodoSiguiente.getClavePeriodo();
            PeriodoOperacion operacion = capturarInformacionComplementariaRepository
                    .buscarPeriodoOperacionPorClavePeriodo(periodoSiguienteStr, clavePresupuestal, tipoCaptura);
            if (null == operacion) {
                generarPeriodoOperacionSiguiente(clavePresupuestal, tipoCaptura, periodoSiguienteStr);

            } else {
                capturarInformacionComplementariaRepository.actualizarPeriodoOperacionSiguienteActual(clavePresupuestal,
                        tipoCaptura, operacion.getClavePeriodoIMSS());
            }
        }
    }

    @Override
    @Transactional(rollbackFor = { HospitalizacionException.class })
    public void abrirPeriodo(String clavePresupuestal, int tipoCaptura, String clavePeriodoImss)
            throws HospitalizacionException {

        PeriodoOperacion periodoOperacion = capturarInformacionComplementariaRepository
                .buscarPeriodoActual(clavePresupuestal, tipoCaptura);
        String periodoAbierto = periodoOperacion.getClavePeriodoIMSS();
        capturarInformacionComplementariaRepository.cerrarPeriodo(clavePresupuestal, tipoCaptura, periodoAbierto);
        String cvePeriodoAux = capturarInformacionComplementariaHelper.ordenarPeriodo(clavePeriodoImss);
        capturarInformacionComplementariaRepository.actualizarPeriodoOperacionSiguienteActual(clavePresupuestal,
                tipoCaptura, cvePeriodoAux);

    }

    @Override
    public boolean estadoPeriodoOperacion(String cvePeriodo, String cvePresupuestal, int tipoCaptura) {

        String cvePeriodoAux = capturarInformacionComplementariaHelper.ordenarPeriodo(cvePeriodo);
        PeriodoOperacion periodo = capturarInformacionComplementariaRepository
                .buscarPeriodoOperacionPorClavePeriodo(cvePeriodoAux, cvePresupuestal, tipoCaptura);
        return periodo.isIndicadorCerrado();
    }

    @Override
    public void generarPeriodoOperacionSiguiente(String cvePresupuestal, int cveTipoCaptura, String cvePeriodoIMSS) {

        capturarInformacionComplementariaRepository.generarPeriodoOperacionSiguiente(cvePresupuestal, cveTipoCaptura,
                cvePeriodoIMSS);

    }

    @Override
    public PeriodosImss obtenerSiguientePeriodoImss(String cvePeriodo) throws HospitalizacionException {

        return capturarInformacionComplementariaRepository.obtenerSiguientePeriodoImss(cvePeriodo);

    }

    @Override
    public String reemplazarMensajeConfirm(String mensaje, String subsistema) {

        return capturarInformacionComplementariaHelper.reemplazarMensajeJustificacion(mensaje, subsistema);
    }

    @Override
	public boolean datosIgualACero(List<EstructuraDatoInformacionComplementaria> datos){
		
		return capturarInformacionComplementariaRules.datosIgualaCero(datos);
		
	}
    
	@Override
	public boolean estadoPeriodoOperacionMonitoreo(String cvePeriodo, String cvePresupuestal, int tipoCaptura) {
    	
		String cvePeriodoAux = capturarInformacionComplementariaHelper.ordenarPeriodo(cvePeriodo);
		PeriodoOperacion periodo = capturarInformacionComplementariaRepository
                .buscarPeriodoOperacionPorClavePeriodo(cvePeriodoAux, cvePresupuestal, tipoCaptura);
		return periodo.isIndicadorMonitoreo();
	}

	@Override
	public boolean tienePeriodosMonitoreo(String cvePresupuestal, int tipoCaptura) {
		// TODO Mejora 4)Deshabilitar �Mes y a�o�.
		Long periodosMonitero = capturarInformacionComplementariaRepository.obtenerCountPeriodosMonitoreo(cvePresupuestal, tipoCaptura);
		
		if(periodosMonitero > 0){
			return true;
		}
		
		return false;
	}

	@Override
	public String obtenerPeriodoActual(String cvePresupuestal, int tipoCaptura) {
		// TODO Mejora 4)Deshabilitar �Mes y a�o�.
		PeriodoOperacion periodoOperacion = capturarInformacionComplementariaRepository
				.buscarPeriodoActual(cvePresupuestal, tipoCaptura);
		return capturarInformacionComplementariaHelper
				.desordenarPeriodo(periodoOperacion.getClavePeriodoIMSS());

	}
}
