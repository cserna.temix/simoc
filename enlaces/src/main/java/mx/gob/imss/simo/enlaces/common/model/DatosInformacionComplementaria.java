package mx.gob.imss.simo.enlaces.common.model;

import java.util.Map;

import lombok.Data;

@Data
public class DatosInformacionComplementaria {

    private String cvePeriodo;
    private Integer cveSubsistema;
    private Integer cveRCV;
    private Integer cvePoblacion;
    private Integer cvePor;
    private Integer cveEn;
    private Integer cveHoja;
    private Integer cveSubclave;
    private String justificacion;
    private Map<String, Integer> datos;
    private Integer cifraDeControl;
    private Map<String, Boolean> subsistemas;
    private Boolean[] catalogosRequeridos;
    private Subsistema subsistema;

    public DatosInformacionComplementaria() {
        this.cvePeriodo = "";
        this.cveSubsistema = 0;
        this.cveRCV = 0;
        this.cvePoblacion = 0;
        this.cvePor = 0;
        this.cveEn = 0;
        this.cveHoja = 0;
        this.cveSubclave = 0;
        this.justificacion = "";
        this.datos = null;
        this.cifraDeControl = 0;
        this.subsistemas = null;
        /*
         * posiciones:
         * Periodo
         * Subsistema
         * RCV
         * Poblacion
         * Por
         * En
         * Hoja
         * Subclave
         */
        this.catalogosRequeridos = new Boolean[] { true, true, true, false, false, false, false, false };
        this.subsistema = new Subsistema();
    }

}
