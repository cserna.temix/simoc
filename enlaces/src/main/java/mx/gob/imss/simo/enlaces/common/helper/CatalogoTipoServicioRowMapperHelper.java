package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

public class CatalogoTipoServicioRowMapperHelper extends BaseHelper implements RowMapper<DatosCatalogo> {

    @Override
    public DatosCatalogo mapRow(ResultSet resultSet, int rowId) throws SQLException {

        DatosCatalogo datosCatalogo = new DatosCatalogo();
        datosCatalogo.setClave(resultSet.getString(SQLColumnasConstants.CVE_TIPO_SERVICIO));
        datosCatalogo.setDescripcion(resultSet.getString(SQLColumnasConstants.DES_TIPO_SERVICIO));
        return datosCatalogo;
    }

}
