package mx.gob.imss.simo.enlaces.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class DatoFormula {

    private long clave;
    private int cveDato;
    private int numIteracionDato;
    private String operacion;
    private String mensajeError;
    private Date fechaAlta;
    private int mesInicio;
    private int anioInicio;
    private int mesFin;
    private int anioFin;
    private Date fechaBaja;

}
