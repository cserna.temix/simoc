
package mx.gob.imss.simo.enlaces.informacioncomplementaria.services;

import java.util.List;
import java.util.Map;

import mx.gob.imss.simo.enlaces.common.model.FormulaInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaConsulta;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.EstructuraDatoInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.model.DatosUsuario;

public interface CapturarInformacionComplementariaServices {

    void validarPeriodo(String periodo, String clavePresupuestal) throws HospitalizacionException;

    void validarSubsistema(Object cveSubsistema) throws HospitalizacionException;

    public void validarRCV(String cveRCV) throws HospitalizacionException;

    void validarPoblacion(Integer cvePoblacion) throws HospitalizacionException;

    void validarPor(Integer cvePor) throws HospitalizacionException;

    void validarEn(Integer cveEn) throws HospitalizacionException;

    void validarHoja(Integer cveHoja) throws HospitalizacionException;

    void validarSubclave(Integer cveSubclave) throws HospitalizacionException;

    void validarJustificacion(String justificacion) throws HospitalizacionException;

    List<DatosCatalogo> obtenerCatalogoSubsistema(String cvePeriodo);

    List<DatosCatalogo> obtenerCatalogoRCV(int cveSubsistema);

    boolean existeInformacionComplementaria(int cveSubsistema, String cvePresupuestal, String cvePeriodoIMSS);

    boolean sePuedeActivarComponentes(Boolean[] requeridos);

    DatosInformacionComplementaria consultarSubsistemaRCV(DatosInformacionComplementaria datosInfoComplementaria,
            String periodo, String cvePresupuestal);

    boolean sePuedeConsultarSubsistema(Boolean[] catalogosRequeridosVista, Boolean[] catalogoRequeridos);

    Subsistema obtenerSubsistema(int cveSubsistema);

    boolean ultimoCatalogoRequerido(Subsistema subsistema, String comboId, String[] componentes);

    List<SubsistemaRCV> obtenerCatalogosInformacionComplementaria(long clave, int cveRCV);

    List<DatosCatalogo> armarDatosCatalogoPoblacion(List<SubsistemaRCV> catalogosInformacionComplementariaList);

    List<DatosCatalogo> armarDatosCatalogoPor(List<SubsistemaRCV> catalogosInformacionComplementariaList);

    List<DatosCatalogo> armarDatosCatalogoEn(List<SubsistemaRCV> catalogosInformacionComplementariaList);

    List<DatosCatalogo> armarDatosCatalogoHoja(List<SubsistemaRCV> catalogosInformacionComplementariaList);

    List<DatosCatalogo> armarDatosCatalogoSubclave(List<SubsistemaRCV> catalogosInformacionComplementariaList);

    void validarDato(Integer valorDato) throws HospitalizacionException;

    List<FormulaInformacionComplementaria> obtenerFormulasAEjecutar(long cveSubsistema,
            List<EstructuraDatoInformacionComplementaria> datos);

    SubsistemaRCV obtenerSubsistemaRCV(DatosInformacionComplementaria datosInfoComplementaria);

    List<FormulaInformacionComplementaria> validarFormulaDato(long cveSubsistemaRCV,
            DatosInformacionComplementaria datosInfo);

    void validarCifradeControl(DatosInformacionComplementaria datosInformacionComplementaria)
            throws HospitalizacionException;

    void validarCifradeControlSimple(DatosInformacionComplementaria datosInformacionComplementaria)
            throws HospitalizacionException;

    void guardarInfoComplementaria(int cveSubsistemaRcv, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria, boolean isVisibleCifraDeControl,
            boolean datosIgualACero);

    void guardarInfoComplementariaPorJustificacion(boolean existeInformacionComplementaria, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria);

    Map<String, Boolean> cargarSubsistemas(List<DatosCatalogo> catalogoSubsistema,
            DatosInformacionComplementaria datosInformacionComplementaria, String cvePresupuestal);

    List<InformacionComplementariaConsulta> informacionComplementariaConsulta(DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria);

    void validarCapturaCifra99(int cveSubsistema, String cvePresupuestal, String cvePeriodoImss)
            throws HospitalizacionException;

    void validarCifraControl99(int cveSubsistema, Long cifraControl99String, String cvePresupuestal, String cvePeriodo)
            throws HospitalizacionException;

    String descomponerCadenaFocoParaErrorCifraCOntrol(String componenteCifraControl99);

    void abrirSubsistema(String cvePresupuestal, String cvePeriodo, int cveSubsistema, String usuarioActualiza);

    void actualizarCifraControlInformacionComplementariaPorCierre(
            InformacionComplementaria informacionComplementariaPorCierre);

    InformacionComplementaria buscarInformacionExistente(int cveSubsistemaRcv, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria);

    void actualizarInformacionComplementaria(InformacionComplementaria informacionExistente, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria);

    void controlMapaCierre(String cvePresupuestal, String cvePeriodo, int cveSubsistema, String usuarioActualiza);

    boolean validarCierrePeriodo(String cvePeriodo, String cvePresupuestal);

    void cerrarPeriodo(String clavePresupuestal, int tipoCaptura, String clavePeriodoImss, boolean isIndicadorMonitoreo)
            throws HospitalizacionException;

    void abrirPeriodo(String clavePresupuestal, int tipoCaptura, String clavePeriodoImss)
            throws HospitalizacionException;

    boolean estadoPeriodoOperacion(String cvePeriodo, String cvePresupuestal, int tipoCaptura);

    void generarPeriodoOperacionSiguiente(String cvePresupuestal, int cveTipoCaptura, String cvePeriodoIMSS);

    PeriodosImss obtenerSiguientePeriodoImss(String cvePeriodo) throws HospitalizacionException;

    String reemplazarMensajeConfirm(String mensaje, String subsistema);

    boolean validarSubsistemaCerrado(DatosInformacionComplementaria datosInformacionComplementaria,
            String cvePresupuestal, String claveSubsistema);

    boolean datosIgualACero(List<EstructuraDatoInformacionComplementaria> datos);
    
    boolean estadoPeriodoOperacionMonitoreo(String cvePeriodo, String cvePresupuestal, int tipoCaptura);

    boolean tienePeriodosMonitoreo(String cvePresupuestal, int tipoCaptura);

    String obtenerPeriodoActual(String cvePresupuestal, int tipoCaptura);
}
