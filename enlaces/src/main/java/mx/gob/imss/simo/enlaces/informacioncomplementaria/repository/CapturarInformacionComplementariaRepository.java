package mx.gob.imss.simo.enlaces.informacioncomplementaria.repository;

import java.util.List;

import mx.gob.imss.simo.enlaces.common.model.CierreSubsistema;
import mx.gob.imss.simo.enlaces.common.model.DatoFormula;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaDato;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCVDato;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;

public interface CapturarInformacionComplementariaRepository extends HospitalizacionCommonRepository {

    Subsistema obtenerSubsistema(int cveSubsistema);

    List<DatosCatalogo> obtenerCatalogoSubsistemas();

    List<DatosCatalogo> obtenerCatalogoRCV(int cveSubsistema);

    List<DatosCatalogo> obtenerCatalogoPoblacion();

    List<DatosCatalogo> obtenerCatalogoPor();

    List<DatosCatalogo> obtenerCatalogoEn();

    List<DatosCatalogo> obtenerCatalogoHoja();

    List<DatosCatalogo> obtenerCatalogoSubclave();

    SubsistemaRCV obtenerSubsistemaRCV(Integer cveSubsistema, Integer cveRCV, Integer cvePoblacion, Integer cvePor,
            Integer cveEn, Integer cveHoja, Integer cveSubclave);

    List<SubsistemaRCVDato> obtenerSubsistemaRCVDato(long clave);

    List<SubsistemaRCV> obtenerCatalogosInformacionComplementaria(long clave, int cveRCV);

    InformacionComplementariaDato obtenerInformacionComplementariaDato(long cveSubsistema, String cvePresupuestal,
            String cvePeriodoIMSS, int cveDato);

    List<DatoFormula> obtenerDatoFormula(long cveSubsistemaRCV, int cveDato);

    List<CierreSubsistema> obtenerSubsistemasCerrados(String cvePresupuestal, String cvePeriodoIMSS);

    void guardarInformacionComplementaria(InformacionComplementaria informacionComplementaria, int cveSubsistema,
            int cveRCV);

    void guardarDatosInformacionComplementaria(List<InformacionComplementariaDato> datosInfromacionComplementaria);

    InformacionComplementaria buscarInformacionExistente(Integer cveSubsistemaRcv, String cvePresupuestal,
            String cvePeriodo, Integer cveSubsistema, Integer cveRCV);

    void actualizarInformacionComplementaria(InformacionComplementaria informacionExistente);

    List<InformacionComplementariaDato> obtenerDatosInformacionComplementaria(
            InformacionComplementaria informacionExistente);

    void actualizarDatosInformacionComplementaria(List<InformacionComplementariaDato> datosActualizados);

    List<InformacionComplementaria> consultarInformacionComplementariaPorSubsistema(String cvePresupuestal,
            String cvePeriodo, int cveSubsistema);

    List<InformacionComplementaria> consultarInformacionComplementariaPorSubsistemaAsc(String cvePresupuestal,
            String cvePeriodo, int cveSubsistema);

    List<InformacionComplementariaDato> consultarInformacionComplementariaDatoPorSubsistemaRCV(String cvePresupuestal,
            String cvePeriodo, int cveSubsistemaRCV);

    SubsistemaRCV obtenerCantidadRCVParaCifra99(int cveSubsistema);

    Long obtenerTotalRCVCapturadosPorSubsistema(int cveSubsistema, String cvePresupuestal, String cvePeriodo);

    List<InformacionComplementaria> obtenerCifraControlPorSubsistema(int cveSubsistema, String cvePresupuestal,
            String cvePeriodo);

    void guardarCierreSubsistema(CierreSubsistema cierreSubsistema);

    void actualizarCierreSubsistema(CierreSubsistema cierreSubsistemaExistente);

    CierreSubsistema buscarCierreSubsistemaExistente(String cvePresupuestal, String cvePeriodo, Integer cveSubsistema);

    void abrirSubsistema(String cvePresupuestal, String cvePeriodo, int cveSubsistema, String usuarioActualiza);

    void actualizarCifraControlInformacionComplementariaPorCierre(
            InformacionComplementaria informacionComplementariaPorCierre);

    void actualizarCifraControlInformacionComplementariaDatoPorCierre(
            InformacionComplementaria informacionComplementariaPorCierre);

    InformacionComplementaria buscarInformacionComplementariaPorCierre(String cvePresupuestal, String cvePeriodo,
            Integer cveSubsistema);

    Long obtenerTotalSubsistemasCerrados();

    Long obtenerTotalSubsistemasCerradosPorPresupuestal(String cvePeriodo, String cvePresupuestal);

    Long obtenerCountInformacionComplementariaPorSubsistema(int cveSubsistema, String cvePresupuestal,
            String cvePeriodoIMSS);

    void eliminarInformacionComplementariaDato(int cveSubsistemaRCV, String cvePresupuestal, String cvePeriodoIMSS);

    void eliminarInformacionComplementaria(int cveSubsistemaRCV, String cvePresupuestal, String cvePeriodoIMSS);

    void generarPeriodoOperacionSiguiente(String cvePresupuestal, int cveTipoCaptura, String cvePeriodoIMSS);

    PeriodosImss obtenerSiguientePeriodoImss(String cvePeriodo) throws HospitalizacionException;

    void actualizarPeriodoOperacionSiguienteActual(String cvePresupuestal, int tipoCaptura, String cvePeriodoIMSS);

    PeriodoOperacion buscarPeriodoActual(String cvePresupuestal, int tipoCaptura);
    
    void actualizarIndicadorMonitoreo(String cvePresupuestal, int tipoCaptura, String cvePeriodoIMSS);

    Long obtenerCountPeriodosMonitoreo(String cvePresupuestal, int tipoCaptura);

    List<SubsistemaRCVDato> obtenerDatoPorSubsistemaRCV(int cveSubsistema, int cveRCV);
}
