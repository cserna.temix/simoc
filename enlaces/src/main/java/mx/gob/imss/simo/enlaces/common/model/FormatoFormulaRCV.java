package mx.gob.imss.simo.enlaces.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class FormatoFormulaRCV {

    private String subsistema;
    private String rcv;
    private Integer estatusFormula;
    private Long idFormula;
    private Date fecAlta;
    private Date fecBaja;
    private String descError;
    private Integer mesInicio;
    private Integer anioInicio;
    private Integer mesFin;
    private Integer anioFin;

    private String c1;
    private String c2;
    private String c3;
    private String c4;
    private String c5;
    private String c6;
    private String c7;
    private String c8;
    private String c9;
    private String c10;
    private String c11;
    private String c12;
    private String c13;
    private String c14;
    private String c15;
    private String c16;
    private String c17;
    private String c18;
    private String c19;

}
