package mx.gob.imss.simo.enlaces.common.constant;

import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;

public class MensajesErrorEnlacesConstants extends MensajesErrorConstants {

    public static final String ME_182 = "me182";

    public static final String ME_202 = "me202";

}
