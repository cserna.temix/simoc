package mx.gob.imss.simo.enlaces.informacioncomplementaria.helper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.gob.imss.simo.enlaces.common.constant.CapturarInformacionComplementariaConstants;
import mx.gob.imss.simo.enlaces.common.constant.OperadoresFormulas;
import mx.gob.imss.simo.enlaces.common.model.CierreSubsistema;
import mx.gob.imss.simo.enlaces.common.model.ComponenteCatalogoInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.DatoFormula;
import mx.gob.imss.simo.enlaces.common.model.FormulaInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaConsulta;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaDato;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCVDato;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.repository.CapturarInformacionComplementariaRepository;
import mx.gob.imss.simo.hospitalizacion.common.helper.HospitalizacionCommonHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.EstructuraDatoInformacionComplementaria;
import mx.gob.imss.simo.model.DatosUsuario;

@Component
public class CapturarInformacionComplementariaHelper extends HospitalizacionCommonHelper {

    @Autowired
    private CapturarInformacionComplementariaRepository capturarInformacionComplementariaRepository;

    public String prepararFechaPeriodo(String periodo) {

        return "01/".concat(periodo.substring(0, 2).concat("/").concat(periodo.substring(2)));
    }

    public List<ComponenteCatalogoInformacionComplementaria> generarComponenteCatalogoInfComplementariaList(
            Subsistema subsistema, String[] componentes) {

        List<ComponenteCatalogoInformacionComplementaria> componenteCatalogoInfComplementariaList = new ArrayList<>();
        ComponenteCatalogoInformacionComplementaria componenteCatalogoInformacionComplementaria1 = new ComponenteCatalogoInformacionComplementaria();
        componenteCatalogoInformacionComplementaria1.setComboId(componentes[0]);
        componenteCatalogoInformacionComplementaria1.setRequerido(Boolean.TRUE);
        componenteCatalogoInformacionComplementaria1.setUltimoRequerido(validarEsRCVUltimoRequerido(subsistema));
        ComponenteCatalogoInformacionComplementaria componenteCatalogoInformacionComplementaria2 = new ComponenteCatalogoInformacionComplementaria();
        componenteCatalogoInformacionComplementaria2.setComboId(componentes[1]);
        componenteCatalogoInformacionComplementaria2.setRequerido(subsistema.getIndPoblacion() == 1 ? true : false);
        componenteCatalogoInformacionComplementaria2
                .setUltimoRequerido(esUltimoDatoRequerido(subsistema, componentes[1], componentes));
        ComponenteCatalogoInformacionComplementaria componenteCatalogoInformacionComplementaria3 = new ComponenteCatalogoInformacionComplementaria();
        componenteCatalogoInformacionComplementaria3.setComboId(componentes[2]);
        componenteCatalogoInformacionComplementaria3.setRequerido(subsistema.getIndPor() == 1 ? true : false);
        componenteCatalogoInformacionComplementaria3
                .setUltimoRequerido(esUltimoDatoRequerido(subsistema, componentes[2], componentes));
        ComponenteCatalogoInformacionComplementaria componenteCatalogoInformacionComplementaria4 = new ComponenteCatalogoInformacionComplementaria();
        componenteCatalogoInformacionComplementaria4.setComboId(componentes[3]);
        componenteCatalogoInformacionComplementaria4.setRequerido(subsistema.getIndEn() == 1 ? true : false);
        componenteCatalogoInformacionComplementaria4
                .setUltimoRequerido(esUltimoDatoRequerido(subsistema, componentes[3], componentes));
        ComponenteCatalogoInformacionComplementaria componenteCatalogoInformacionComplementaria5 = new ComponenteCatalogoInformacionComplementaria();
        componenteCatalogoInformacionComplementaria5.setComboId(componentes[4]);
        componenteCatalogoInformacionComplementaria5.setRequerido(subsistema.getIndHoja() == 1 ? true : false);
        componenteCatalogoInformacionComplementaria5
                .setUltimoRequerido(esUltimoDatoRequerido(subsistema, componentes[4], componentes));
        ComponenteCatalogoInformacionComplementaria componenteCatalogoInformacionComplementaria6 = new ComponenteCatalogoInformacionComplementaria();
        componenteCatalogoInformacionComplementaria6.setComboId(componentes[5]);
        componenteCatalogoInformacionComplementaria6.setRequerido(subsistema.getIndSubclave() == 1 ? true : false);
        componenteCatalogoInformacionComplementaria6
                .setUltimoRequerido(esUltimoDatoRequerido(subsistema, componentes[5], componentes));
        componenteCatalogoInfComplementariaList.add(componenteCatalogoInformacionComplementaria1);
        componenteCatalogoInfComplementariaList.add(componenteCatalogoInformacionComplementaria2);
        componenteCatalogoInfComplementariaList.add(componenteCatalogoInformacionComplementaria3);
        componenteCatalogoInfComplementariaList.add(componenteCatalogoInformacionComplementaria4);
        componenteCatalogoInfComplementariaList.add(componenteCatalogoInformacionComplementaria5);
        componenteCatalogoInfComplementariaList.add(componenteCatalogoInformacionComplementaria6);
        return componenteCatalogoInfComplementariaList;
    }

    private boolean validarEsRCVUltimoRequerido(Subsistema subsistema) {

        if (esUltimoRequerido(subsistema)) {
            return false;
        }
        return true;

    }

    private boolean esUltimoRequerido(Subsistema subsistema) {

        return validarRequeridoPoblacionYPor(subsistema) || esIndicadorEncendido(subsistema.getIndEn())
                || esIndicadorEncendido(subsistema.getIndPor()) || esIndicadorEncendido(subsistema.getIndPoblacion());
    }

    private boolean validarRequeridoPoblacionYPor(Subsistema subsistema) {

        return esIndicadorEncendido(subsistema.getIndSubclave()) || esIndicadorEncendido(subsistema.getIndHoja());
    }

    private boolean esIndicadorEncendido(int indicador) {

        return indicador == 1;
    }

    private boolean esUltimoDatoRequerido(Subsistema subsistema, String comboId, String[] componentes) {

        if (ultimoComponenteRequerido(subsistema, componentes).equals(comboId)) {
            return true;
        }
        return false;
    }

    private String ultimoComponenteRequerido(Subsistema subsistema, String[] componentes) {

        return ultimoRequeridoSubclaveYHoja(subsistema, componentes);
    }

    private String ultimoRequeridoSubclaveYHoja(Subsistema subsistema, String[] componentes) {

        if (esIndicadorEncendido(subsistema.getIndSubclave())) {
            return componentes[5];
        } else if (esIndicadorEncendido(subsistema.getIndHoja())) {
            return componentes[4];
        } else {
            return ultimoRequeridoEnPorPoblacion(subsistema, componentes);
        }
    }

    private String ultimoRequeridoEnPorPoblacion(Subsistema subsistema, String[] componentes) {

        if (esIndicadorEncendido(subsistema.getIndEn())) {
            return componentes[3];
        } else if (esIndicadorEncendido(subsistema.getIndPor())) {
            return componentes[2];
        } else if (esIndicadorEncendido(subsistema.getIndPoblacion())) {
            return componentes[1];
        } else {
            return componentes[0];
        }
    }

    public DatosInformacionComplementaria armarDatosInformacionComplementaria(
            DatosInformacionComplementaria datosInfoComplementaria,
            List<SubsistemaRCVDato> informacionComplementariaDatoList, String cvePresupuestal, String cvePeriodoIMSS,
            InformacionComplementaria informacionExistente, String desSubsistemaRCV) {

    	
        List<EstructuraDatoInformacionComplementaria> datos = new ArrayList<>();
        for (SubsistemaRCVDato informacionComplementariaDatos : informacionComplementariaDatoList) {
            EstructuraDatoInformacionComplementaria dato = new EstructuraDatoInformacionComplementaria();
            InformacionComplementariaDato informacionComplementariaDato = obtenerInformacionComplementariaDato(
                    informacionComplementariaDatos.getClave(), cvePresupuestal, cvePeriodoIMSS,
                    informacionComplementariaDatos.getCveDato());
            dato.setClave(informacionComplementariaDatos.getCveDato());
            dato.setValor((informacionComplementariaDato == null) ? null
                    : String.valueOf(informacionComplementariaDato.getNumValorDato()));  
            if(informacionComplementariaDatos.getRefConcepto() != null){
            	dato.setConcepto(informacionComplementariaDatos.getRefConcepto()
                		.replace(informacionComplementariaDatos.getRefConcepto().substring(1),
                				informacionComplementariaDatos.getRefConcepto().substring(1).toLowerCase()));
            }           		
            datos.add(dato);

        }
        datosInfoComplementaria.setDatos(datos);
        datosInfoComplementaria
                .setCifraDeControl(informacionExistente == null ? null : informacionExistente.getNumCifraControl());
        datosInfoComplementaria.setDesSubsistemaRCV(desSubsistemaRCV);
        return datosInfoComplementaria;
        
    }

    private InformacionComplementariaDato obtenerInformacionComplementariaDato(int cveSubsistema,
            String cvePresupuestal, String cvePeriodoIMSS, int cveDato) {

        return capturarInformacionComplementariaRepository.obtenerInformacionComplementariaDato(cveSubsistema,
                cvePresupuestal, cvePeriodoIMSS, cveDato);
    }

    private String armarLabelDato(String clave) {

        StringBuilder label = new StringBuilder(CapturarInformacionComplementariaConstants.INICIO_LABEL_DATO);
        if (clave.length() == 1) {
            label.append("0");
            label.append(clave);
        }
        return label.toString();
    }

    public List<DatosCatalogo> armarDatosCatalogoPoblacion(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        List<DatosCatalogo> datosCatalogoList = new ArrayList<>();
        for (SubsistemaRCV info : catalogosInformacionComplementariaList) {
            DatosCatalogo datosCatalogo = new DatosCatalogo();
            if (!validarClaveRepetida(datosCatalogoList, info.getCvePoblacion().toString())) {
                datosCatalogo.setClave(info.getCvePoblacion().toString());
                datosCatalogo.setDescripcion(info.getCvePoblacion().toString());
                datosCatalogoList.add(datosCatalogo);
            }

        }
        return datosCatalogoList;
    }

    public List<DatosCatalogo> armarDatosCatalogoPor(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        List<DatosCatalogo> datosCatalogoList = new ArrayList<>();
        for (SubsistemaRCV info : catalogosInformacionComplementariaList) {
            DatosCatalogo datosCatalogo = new DatosCatalogo();
            if (!validarClaveRepetida(datosCatalogoList, info.getCvePor().toString())) {
                datosCatalogo.setClave(info.getCvePor().toString());
                datosCatalogo.setDescripcion(info.getCvePor().toString());
                datosCatalogoList.add(datosCatalogo);
            }

        }
        return datosCatalogoList;
    }

    public List<DatosCatalogo> armarDatosCatalogoEn(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        List<DatosCatalogo> datosCatalogoList = new ArrayList<>();
        for (SubsistemaRCV info : catalogosInformacionComplementariaList) {
            DatosCatalogo datosCatalogo = new DatosCatalogo();
            if (!validarClaveRepetida(datosCatalogoList, info.getCveEn().toString())) {
                datosCatalogo.setClave(info.getCveEn().toString());
                datosCatalogo.setDescripcion(info.getCveEn().toString());
                datosCatalogoList.add(datosCatalogo);
            }

        }
        return datosCatalogoList;
    }

    public List<DatosCatalogo> armarDatosCatalogoHoja(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        List<DatosCatalogo> datosCatalogoList = new ArrayList<>();
        for (SubsistemaRCV info : catalogosInformacionComplementariaList) {
            DatosCatalogo datosCatalogo = new DatosCatalogo();
            if (!validarClaveRepetida(datosCatalogoList, info.getCveHoja().toString())) {
                datosCatalogo.setClave(info.getCveHoja().toString());
                datosCatalogo.setDescripcion(info.getCveHoja().toString());
                datosCatalogoList.add(datosCatalogo);
            }

        }
        return datosCatalogoList;
    }

    public List<DatosCatalogo> armarDatosCatalogoSubclave(List<SubsistemaRCV> catalogosInformacionComplementariaList) {

        List<DatosCatalogo> datosCatalogoList = new ArrayList<>();
        for (SubsistemaRCV info : catalogosInformacionComplementariaList) {
            DatosCatalogo datosCatalogo = new DatosCatalogo();
            if (!validarClaveRepetida(datosCatalogoList, info.getCveSubclave().toString())) {
                datosCatalogo.setClave(info.getCveSubclave().toString());
                datosCatalogo.setDescripcion(info.getCveSubclave().toString());
                datosCatalogoList.add(datosCatalogo);
            }

        }
        return datosCatalogoList;
    }

    private boolean validarClaveRepetida(List<DatosCatalogo> datosCatalogoList, String clave) {

        for (DatosCatalogo datos : datosCatalogoList) {
            if (datos.getClave().contains(clave)) {
                return true;
            }
        }

        return false;
    }

    public FormulaInformacionComplementaria transformarDatoFormulaToFormulaInformacionComplementaria(
            DatoFormula datoFormula) {

        FormulaInformacionComplementaria formulaInformacionComplementaria = new FormulaInformacionComplementaria();
        formulaInformacionComplementaria.setRegla(datoFormula.getOperacion());
        formulaInformacionComplementaria.setMensajeError(datoFormula.getMensajeError());
        return formulaInformacionComplementaria;
    }

    public String transformarFormulaJavascript(String formula, DatosInformacionComplementaria datosInfo) {

        String[] partsFormula = formula.split(" ");
        String formulaJavascript = formula;
        for (String partes : partsFormula) {
            if (OperadoresFormulas.IGUAL.getClave().equals(partes)) {
                formulaJavascript = formulaJavascript.replaceAll(partes,
                        CapturarInformacionComplementariaConstants.OPERADOR_IGUAL);
            }

            if (partes.startsWith("V")) {
                for (int i = 0; i < datosInfo.getDatos().size(); i++) {
                    if (datosInfo.getDatos().get(i).getClave() == obtenerClaveDato(partes)) {
                        formulaJavascript = formulaJavascript.replaceAll(partes,
                                datosInfo.getDatos().get(i).getValor());
                    }
                }

            }

        }

        if (formulaJavascript.contains(CapturarInformacionComplementariaConstants.BETWEEN)) {
            formulaJavascript = transformarBetween(formulaJavascript, partsFormula);
        }

        return formulaJavascript;
    }

    public String transformarBetween(String formulaJavascript, String[] partsFormula) {

        for (String partes : partsFormula) {
            if (OperadoresFormulas.AND.getClave().equals(partes)) {
                formulaJavascript = formulaJavascript.replaceAll(partes,
                        CapturarInformacionComplementariaConstants.AND);
            }

        }

        String[] formulaSeparada = formulaJavascript.split(CapturarInformacionComplementariaConstants.BETWEEN);
        StringBuilder formula = new StringBuilder();
        String[] rango = formulaSeparada[1].trim().split(" ");
        formula.append(formulaSeparada[0]).append(">=").append(rango[0]).append(rango[1]).append(formulaSeparada[0])
                .append("<=").append(rango[2]);

        return formula.toString();
    }

    private int obtenerClave(String partes) {

        if (partes.substring(1).startsWith("0")) {
            return Integer.parseInt(partes.substring(2)) - 1;
        }
        return Integer.parseInt(partes.substring(1)) - 1;
    }

    private int obtenerClaveDato(String partes) {

        if (partes.substring(1).startsWith("0")) {
            return Integer.parseInt(partes.substring(2));
        }
        return Integer.parseInt(partes.substring(1));
    }

    public List<FormulaInformacionComplementaria> transformarModeloAReglaCompuesta(
            List<FormulaInformacionComplementaria> formulaInformacionComplementariaList) {

        for (FormulaInformacionComplementaria formulaInformacionComplementaria : formulaInformacionComplementariaList) {
            if (formulaInformacionComplementaria.getRegla().contains(CapturarInformacionComplementariaConstants.BETWEEN)
                    && formulaInformacionComplementaria.getRegla()
                            .contains(CapturarInformacionComplementariaConstants.OR)) {
                formulaInformacionComplementaria.setCompuesta(true);
                List<String> reglaCompuestaList = new ArrayList<>();
                String[] reglaCompuesta = formulaInformacionComplementaria.getRegla()
                        .split(CapturarInformacionComplementariaConstants.OR);
                for (int i = 0; i <= reglaCompuesta.length - 1; i++) {
                    reglaCompuestaList.add(reglaCompuesta[i]);
                }
                formulaInformacionComplementaria.getReglasCompuestas().add(reglaCompuestaList);
            }
        }
        return formulaInformacionComplementariaList;
    }

    public InformacionComplementaria prepararInformacionComplementaria(Integer cveRcv, DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria) {

        InformacionComplementaria informacionComplementaria = new InformacionComplementaria();
        informacionComplementaria.setCveSubsistemaRcv(cveRcv);
        informacionComplementaria.setCvePresupuestal(datosUsuario.getCvePresupuestal());
        informacionComplementaria.setCvePeriodoImss(datosInformacionComplementaria.getCvePeriodo());
        informacionComplementaria.setNumCifraControl(datosInformacionComplementaria.getCifraDeControl());
        informacionComplementaria.setFecCreacion(new Date());
        informacionComplementaria.setCveCapturista(datosUsuario.getUsuario());
        informacionComplementaria.setRefCveSubsistema(datosInformacionComplementaria.getCveSubsistema());
        return informacionComplementaria;
    }

    public List<InformacionComplementariaDato> preparaDatosInformacionComplementaria(Integer cveRcv,
            DatosUsuario datosUsuario, DatosInformacionComplementaria datosInformacionComplementaria) {

        List<InformacionComplementariaDato> informacionComplementariaDatos = new ArrayList<>();
        for (EstructuraDatoInformacionComplementaria dato : datosInformacionComplementaria.getDatos()) {
            InformacionComplementariaDato complementariaDato = new InformacionComplementariaDato();
            complementariaDato.setCveSubsistemaRcv(cveRcv);
            complementariaDato.setCvePresupuestal(datosUsuario.getCvePresupuestal());
            complementariaDato.setCvePeriodoImss(datosInformacionComplementaria.getCvePeriodo());
            complementariaDato.setCveDato(dato.getClave());
            complementariaDato.setNumValorDato(Integer.parseInt(dato.getValor()));
            informacionComplementariaDatos.add(complementariaDato);
        }

        return informacionComplementariaDatos;
    }

    public InformacionComplementaria actualizarInformacionComplementaria(InformacionComplementaria informacionExistente,
            String usuario, Long cifraDeControl) {

        informacionExistente.setNumCifraControl(cifraDeControl);
        informacionExistente.setFecActualizacion(new Date());
        informacionExistente.setCveCapturistaActualiza(usuario);

        return informacionExistente;
    }

    public List<InformacionComplementariaDato> prepararDatosActualizados(
            List<InformacionComplementariaDato> capturarInformacionComplementariaDato,
            List<EstructuraDatoInformacionComplementaria> datos) {

        for (InformacionComplementariaDato datoOriginal : capturarInformacionComplementariaDato) {
            for (EstructuraDatoInformacionComplementaria datoNuevo : datos) {
                if (datoOriginal.getCveDato() == datoNuevo.getClave()) {
                    datoOriginal.setNumValorDato(Integer.parseInt(datoNuevo.getValor()));
                }
            }
        }
        return capturarInformacionComplementariaDato;
    }

    public String ordenarPeriodo(String periodo) {

        return periodo.substring(2).concat(periodo.substring(0, 2));
    }

    public InformacionComplementariaConsulta armarDatosInformacionAConsulta(
            InformacionComplementaria informacionComplementaria,
            List<InformacionComplementariaDato> informacionComplementariaDatoList,
            List<SubsistemaRCVDato> listaSubsistemaRCVDato) {

        InformacionComplementariaConsulta informacionComplementariaConsulta = new InformacionComplementariaConsulta();
        informacionComplementariaConsulta.setCveRCV(String.valueOf(informacionComplementaria.getRefCveRCV()));
        if (informacionComplementaria.getRefCveRCV() != 99) {
            informacionComplementariaConsulta
                    .setDatos(armarInformacionComplementariaDatoAEstructuraDatoInformacionComplementaria(
                            informacionComplementariaDatoList, listaSubsistemaRCVDato));
        }
        informacionComplementariaConsulta
                .setCifraDeControl(String.valueOf(informacionComplementaria.getNumCifraControl()));
        return informacionComplementariaConsulta;
    }

    private List<EstructuraDatoInformacionComplementaria> armarInformacionComplementariaDatoAEstructuraDatoInformacionComplementaria(
            List<InformacionComplementariaDato> informacionComplementariaDatoList, 
            List<SubsistemaRCVDato> listaSubsistemaRCVDato) {

        List<EstructuraDatoInformacionComplementaria> estructuraDatoInformacionComplementariaList = new ArrayList<>();
        for (InformacionComplementariaDato informacionComplementariaDato : informacionComplementariaDatoList) {
            EstructuraDatoInformacionComplementaria estructuraDatoInformacionComplementaria = new EstructuraDatoInformacionComplementaria();
            estructuraDatoInformacionComplementaria.setClave(informacionComplementariaDato.getCveDato());
            estructuraDatoInformacionComplementaria
                    .setValor(String.valueOf(informacionComplementariaDato.getNumValorDato()));
            if(!listaSubsistemaRCVDato.isEmpty()) {
            	listaSubsistemaRCVDato.forEach((o) -> {
            		if(o.getCveDato() == informacionComplementariaDato.getCveDato()){
            			estructuraDatoInformacionComplementaria.setConcepto(o.getRefConcepto()
            				.replace(o.getRefConcepto().substring(1), o.getRefConcepto().substring(1).toLowerCase()));
            		}
            	});            	
            }            
            estructuraDatoInformacionComplementariaList.add(estructuraDatoInformacionComplementaria);
        }
        return estructuraDatoInformacionComplementariaList;
    }

    public List<InformacionComplementariaConsulta> prepararDatosConsulta(
            List<InformacionComplementariaConsulta> informacionComplementariaConsultaList) {

        int valorMaximo = 0;
        for (InformacionComplementariaConsulta datosConsulta : informacionComplementariaConsultaList) {
            if (datosConsulta.getDatos().size() > valorMaximo) {
                valorMaximo = datosConsulta.getDatos().size();
            }
        }
        for (InformacionComplementariaConsulta datosConsulta : informacionComplementariaConsultaList) {
            List<EstructuraDatoInformacionComplementaria> datosOrdenados = new ArrayList<>();
            for (int i = 0; i < valorMaximo; i++) {
                try {
                    if (datosConsulta.getDatos().get(i).getClave() == (i + 1)) {
                        datosOrdenados.add(datosConsulta.getDatos().get(i));
                    } else {
                        EstructuraDatoInformacionComplementaria nuevo = new EstructuraDatoInformacionComplementaria();
                        nuevo.setClave(i + 1);
                        datosOrdenados.add(nuevo);
                    }
                } catch (Exception e) {
                    logger.error("Error al ordenar los datos: " + e);
                    EstructuraDatoInformacionComplementaria nuevo = new EstructuraDatoInformacionComplementaria();
                    nuevo.setClave(i + 1);
                    datosOrdenados.add(nuevo);
                }
            }
            datosConsulta.setDatos(datosOrdenados);
        }
        return informacionComplementariaConsultaList;
    }

    public int obtenerValorMaximoDato(List<InformacionComplementariaConsulta> informacionComplementariaConsultaList) {

        int valorMaximo = 0;
        for (InformacionComplementariaConsulta datosConsulta : informacionComplementariaConsultaList) {
            if (datosConsulta.getDatos() != null) {
                int datosConsultaTamano = datosConsulta.getDatos().size() - 1;
                // List<Integer> listDatoClave = obtenerDatoClave(datosConsulta.getDatos());
                if (datosConsulta.getDatos().get(datosConsultaTamano).getClave() > valorMaximo) {
                    valorMaximo = datosConsulta.getDatos().get(datosConsultaTamano).getClave();

                }
            }
        }

        return valorMaximo;

    }

    public List<InformacionComplementariaConsulta> prepararDatosConsultaComp(
            List<InformacionComplementariaConsulta> informacionComplementariaConsultaList) {

        int valorMaximo = obtenerValorMaximoDato(informacionComplementariaConsultaList);

        for (InformacionComplementariaConsulta datosConsulta : informacionComplementariaConsultaList) {
            if (datosConsulta.getDatos() != null) {
                List<EstructuraDatoInformacionComplementaria> datosOrdenados = new ArrayList<>();
                List<Integer> listDatoClave = obtenerDatoClave(datosConsulta.getDatos());

                for (int i = 0; i < valorMaximo; i++) {
                    try {
                        int clave;
                        if (listDatoClave.contains(i + 1)) {
                            clave = listDatoClave.indexOf(i + 1);
                            datosOrdenados.add(datosConsulta.getDatos().get(clave));
                        } else {
                            EstructuraDatoInformacionComplementaria nuevo = new EstructuraDatoInformacionComplementaria();
                            clave = i + 1;
                            nuevo.setClave(i + 1);
                            datosOrdenados.add(nuevo);
                        }

                    } catch (Exception e) {
                        logger.error("Error al ordenar los datos: " + e);
                        EstructuraDatoInformacionComplementaria nuevo = new EstructuraDatoInformacionComplementaria();
                        nuevo.setClave(i + 1);
                        datosOrdenados.add(nuevo);
                    }

                }
                datosConsulta.setDatos(datosOrdenados);
            }
        }
        return informacionComplementariaConsultaList;
    }

    public List<Integer> obtenerDatoClave(List<EstructuraDatoInformacionComplementaria> datoConsulta) {

        List<Integer> listDatoClave = new ArrayList<>();
        for (EstructuraDatoInformacionComplementaria estructuraInfoComplementaria : datoConsulta) {
            listDatoClave.add(estructuraInfoComplementaria.getClave());
        }
        return listDatoClave;

    }

    public String descomponerCadenaFocoParaErrorCifraCOntrol(String componenteCifraControl99) {

        StringBuilder cadena = new StringBuilder();
        String cadenaReturn = "";
        cadena.append(componenteCifraControl99);
        String[] datos = componenteCifraControl99.split(":");
        if (datos.length > 4) {
            int tamanoAreducir = datos[4].length();
            int max = cadena.length() - tamanoAreducir - 1;
            cadenaReturn = cadena.substring(0, max);
        }
        return cadenaReturn;
    }

    public CierreSubsistema prepararCierreSubsistema(DatosUsuario datosUsuario,
            DatosInformacionComplementaria datosInformacionComplementaria) {

        CierreSubsistema cierreSubsistema = new CierreSubsistema();
        cierreSubsistema.setClavePresupuestal(datosUsuario.getCvePresupuestal());
        cierreSubsistema.setPeriodo(datosInformacionComplementaria.getCvePeriodo());
        cierreSubsistema.setCveSubsistema(datosInformacionComplementaria.getCveSubsistema());
        cierreSubsistema.setRefJustificacion(datosInformacionComplementaria.getJustificacion());
        cierreSubsistema.setIndicadorCerrado(1);
        cierreSubsistema.setFechaCierre(new Date());
        cierreSubsistema.setCveCapturista(datosUsuario.getUsuario());
        cierreSubsistema.setFechaActualizacion(null);
        cierreSubsistema.setCveCapturistaActualiza(null);
        return cierreSubsistema;
    }

    public CierreSubsistema prepararActualizarCierreSubsistema(CierreSubsistema cierreSubsistemaExistente,
            String usuario, DatosInformacionComplementaria datosInformacionComplementaria) {

        cierreSubsistemaExistente.setRefJustificacion(datosInformacionComplementaria.getJustificacion());
        cierreSubsistemaExistente.setFechaActualizacion(new Date());
        cierreSubsistemaExistente.setCveCapturistaActualiza(usuario);

        return cierreSubsistemaExistente;
    }

    public String reemplazarMensajeJustificacion(String mensaje, String subsistema) {

        return mensaje.replace("%1s", subsistema);
    }

    
    public String desordenarPeriodo(String periodo) {

        return periodo.substring(4).concat(periodo.substring(0, 4));
    }
}
