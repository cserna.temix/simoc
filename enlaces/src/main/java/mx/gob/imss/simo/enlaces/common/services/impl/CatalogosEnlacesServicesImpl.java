package mx.gob.imss.simo.enlaces.common.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.imss.simo.enlaces.common.repository.ServiciosSubrogarCommonRepository;
import mx.gob.imss.simo.enlaces.common.services.CatalogosEnlacesServices;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

@Service("catalogosEnlacesServices")
public class CatalogosEnlacesServicesImpl implements CatalogosEnlacesServices {

    @Autowired
    private ServiciosSubrogarCommonRepository catalogosEnlacesRepository;

    /**
     * Informacion Complementaria
     */

    /**
     * Servicios Subrogados
     */

    @Override
    public List<DatosCatalogo> obtenerCatalogoMotivoSubrogacion() {

        return catalogosEnlacesRepository.obtenerCatalogoMotivoSubrogacion();
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoTipoContrato() {

        return catalogosEnlacesRepository.obtenerCatalogoTipoContrato();
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoTipoServicio() {

        return catalogosEnlacesRepository.obtenerCatalogoTipoServicio();
    }

}
