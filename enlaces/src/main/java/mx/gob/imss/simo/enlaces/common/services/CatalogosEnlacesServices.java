package mx.gob.imss.simo.enlaces.common.services;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;

public interface CatalogosEnlacesServices {

    /**
     * Informacion Complementaria
     */

    /**
     * Servicios Subrogados
     */

    List<DatosCatalogo> obtenerCatalogoMotivoSubrogacion();

    List<DatosCatalogo> obtenerCatalogoTipoContrato();

    List<DatosCatalogo> obtenerCatalogoTipoServicio();

}
