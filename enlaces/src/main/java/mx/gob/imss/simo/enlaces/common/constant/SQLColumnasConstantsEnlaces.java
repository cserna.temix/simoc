package mx.gob.imss.simo.enlaces.common.constant;

public class SQLColumnasConstantsEnlaces {

    private SQLColumnasConstantsEnlaces() {
        super();
    }

    /**
     * SIT_INGRESO
     */
    public static final String CVE_MEDICO = "CVE_MEDICO";

    /**
     * SIC_UNIDAD_MEDICA
     */
    public static final String CVE_PRESUPUESTAL = "CVE_PRESUPUESTAL";

    /**
     * SIC_PACIENTE
     */
    public static final String CVE_PACIENTE = "CVE_PACIENTE";

    /**
     * SIC_TIPO_CONTRATO
     */
    public static final String CVE_TIPO_CONTRATO = "CVE_TIPO_CONTRATO";
    public static final String DES_TIPO_CONTRATO = "DES_TIPO_CONTRATO";

    /**
     * SIC_TIPO_SERVICIO
     */
    public static final String CVE_TIPO_SERVICIO = "CVE_TIPO_SERVICIO";
    public static final String DES_TIPO_SERVICIO = "DES_TIPO_SERVICIO";

    /**
     * SIC_MOTIVO_SUBROGACION
     */
    public static final String CVE_MOTIVO_SUBROGACION = "CVE_MOTIVO_SUBROGACION";
    public static final String REF_INICIAL_MOTIVO = "REF_INICIAL_MOTIVO";

    /**
     * SIC_SERVICIO_SUBROGAR
     */
    public static final String CVE_SERVICIO_SUBROGAR = "CVE_SERVICIO_SUBROGAR";
    public static final String DES_SERVICIO_SUBROGAR = "DES_SERVICIO_SUBROGAR";
    public static final String CVE_GRUPO_SUBROGAR = "CVE_GRUPO_SUBROGAR";

    /**
     * SIT_SERVICIO_SUBROGADO
     */
    public static final String CVE_SERVICIO_SUBROGADO = "CVE_SERVICIO_SUBROGADO";
    public static final String REF_FOLIO_CONTRATO = "REF_FOLIO_CONTRATO";
    public static final String NUM_COSTO_UNITARIO = "NUM_COSTO_UNITARIO";
    public static final String NUM_CANTIDAD_EVENTO = "NUM_CANTIDAD_EVENTO";
    public static final String FEC_SERVICIO = "FEC_SERVICIO";

    /**
     * SIC_GRUPO_SUBROGAR
     */
    public static final String DES_GRUPO_SUBROGAR = "DES_GRUPO_SUBROGAR";

}
