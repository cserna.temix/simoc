package mx.gob.imss.simo.enlaces.common.constant;

import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;

public class CapturarInformacionComplementariaConstants extends BaseConstants {

    /**
     * Caracteres Unicode
     * � - \u00C1 � - \u00E1
     * � - \u00C9 � - \u00E9
     * � - \u00CD � - \u00ED
     * � - \u00D3 � - \u00F3
     * � - \u00DA � - \u00FA
     * � - \u00D1 � - \u00F1
     **/

    public static final String FOCO_PERIODO = "idPeriodo";
    public static final String FOCO_SUBSISTEMA = "idSubsistema";
    public static final String PERIODO_VACIO = "______";
    public static final String REQUERIDO_PERIODO = "Mes y a\u00F1o";
    public static final String REQUERIDO_SUBSISTEMA = "Subsistema";
    public static final String REQUERIDO_RCV = "RCV";
    public static final String REQUERIDO_POBLACION = "Poblaci�n";
    public static final String REQUERIDO_POR = "Por";
    public static final String REQUERIDO_EN = "En";
    public static final String REQUERIDO_HOJA = "Hoja";
    public static final String REQUERIDO_SUBCLAVE = "Subclave";
    public static final String REQUERIDO_DATO = "Dato";
    public static final String REQUERIDO_CIFRA_CONTROL = "Cifra de control";

    public static final String JUSTIFICACION_CONFIRM = "CONFIRMACIONJUSTIFICACION";

    public static final String ABRIR_CONFIRM_JUSTIFICACION = "PF('justificacionConfirm').show();";
    public static final String CERRAR_CONFIRM_JUSTIFICACION = "PF('justificacionConfirm').hide();";

    /* Operadores */

    public static final String IGUAL = "=";
    public static final String OPERADOR_IGUAL = "==";
    public static final String MENOR_IGUAL = "<=";
    public static final String MAYOR_IGUAL = ">=";
    public static final String ENTRE = "/";
    public static final String MAS = "+";
    public static final String AND = "&&";

    public static final String SALTO_LINEA = "\n";
    public static final String TAB = "\t";

    public static final String CLASS_NAME = "Formula";
    public static final String OPERANDO1 = "valor1";
    public static final String OPERANDO2 = "valor2";
    public static final String OPERADOR = "operador";

    public static final String BASIC_FORMULA = "if(valor1 operador valor2){" + "\n"
            + "System.out.println(\"entro condicion \"+valor1);" + " \n" + "valida = true;" + "\n" + "}";
    public static final String BASIC_OPERACION = "int resultado = valor1 operador valor2;";

    /* Query Dinamico Obtener Informacion Complementaria */

    public static final String AND_CVE_POBLACION = "AND CVE_POBLACION = ?";
    public static final String AND_CVE_POR = "AND CVE_POR = ?";
    public static final String AND_CVE_EN = "AND CVE_EN = ?";
    public static final String AND_CVE_HOJA = "AND CVE_HOJA = ?";
    public static final String AND_CVE_SUBCLAVE = "AND CVE_SUBCLAVE = ?";

    public static final String INICIO_LABEL_DATO = "V";

    public static final String ORDER_BY_CVE_SUBSISTEMA_RCV = "ORDER BY CVE_SUBSISTEMA_RCV";

    public static final String ASIGNAR_FOCO_SECCION_DATOS = "asignarFocoSeccionDatos();";

    public static final int MES_MARZO = 3;

    public static final String CVE_CIFRA_CONTROL_99 = "99";

    public static final int CVE_CIFRA_CONTROL_99_INT = 99;

    public static final String PARAMETRO_NUM_SUBSISTEMAS_INF_COMPLEMENTARIA = "NUM_SUBSISTEMAS_INF_COMPLEMENTARIA";

    public static final int LONGITUD_AUTOCOMPLETE_SUBSISTEMA = 2;

    public static final int LONGITUD_AUTOCOMPLETE_RCV = 4;

    public static final String BETWEEN = "BETWEEN";
    public static final String OR = "OR";

    public static final String MENSAJE_JUSTIFICAR_CONFIRM = "El subsistema %1s ya tiene informaci\u00F3n complementaria, si confirma se eliminar\u00E1 dicha informaci\u00F3n.�Desea confirmar capturar la justificaci\u00F3n?";

}
