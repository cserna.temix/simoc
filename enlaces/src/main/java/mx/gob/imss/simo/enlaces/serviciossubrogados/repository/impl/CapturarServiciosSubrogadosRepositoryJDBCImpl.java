package mx.gob.imss.simo.enlaces.serviciossubrogados.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.enlaces.common.constant.SQLConstants;
import mx.gob.imss.simo.enlaces.common.helper.ServicioSubrogarRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogado;
import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogar;
import mx.gob.imss.simo.enlaces.serviciossubrogados.repository.CapturarServiciosSubrogadosRepository;
import mx.gob.imss.simo.hospitalizacion.common.constant.SecuenciasEnum;
import mx.gob.imss.simo.hospitalizacion.common.helper.PeriodoOperacionImssRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.PeriodosImssRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;
import mx.gob.imss.simo.model.DatosUsuario;

@Repository("capturarServiciosSubrogadosRepository")
public class CapturarServiciosSubrogadosRepositoryJDBCImpl extends HospitalizacionCommonRepositoryJDBCImpl
        implements CapturarServiciosSubrogadosRepository {

    @Override
    public ServicioSubrogar buscarServicioSubrogar(String claveServiciosSubrogar) {

        List<ServicioSubrogar> serviciosSubrogar = jdbcTemplate.query(SQLConstants.OBTENER_SERVICIO_SUBROGAR,
                new Object[] { claveServiciosSubrogar }, new ServicioSubrogarRowMapperHelper());
        return !serviciosSubrogar.isEmpty() ? serviciosSubrogar.get(0) : null;
    }

    @Override
    public void guardarServicioSubrogado(ServicioSubrogado servicioSubrogado, DatosHospitalizacion datosHospitalizacion,
            DatosUsuario datosUsuario) {

        final long secuencia = obtenerSiguienteValorSecuencia(SecuenciasEnum.SERVICIOSUBROGAR);
        jdbcTemplate.update(SQLConstants.GUARDAR_SERVICIO_SUBROGAR,
                new Object[] { secuencia, servicioSubrogado.getRefFolioContrato(), servicioSubrogado.getCvePaciente(),
                        servicioSubrogado.getCvePresupuestal(), servicioSubrogado.getCveTipoContrato(),
                        servicioSubrogado.getCveTipoServicio(), servicioSubrogado.getCveMotivoSubrogacion(),
                        servicioSubrogado.getCveServicioSubrogar(), servicioSubrogado.getNumCantidadEventos(),
                        servicioSubrogado.getNumCostoUnitario(), servicioSubrogado.getCveMedico(),
                        servicioSubrogado.getFecServicio(), servicioSubrogado.getCveCapturista() });
    }

    public PeriodoOperacion obtenerPeriodoOperacion(String clavePresupuestal, Integer claveTipoCaptura,
            String fechaActual) {

        return jdbcTemplate.queryForObject(SQLConstants.OBTENER_PERIODO_ACTIVO,
                new Object[] { clavePresupuestal, claveTipoCaptura, fechaActual },
                new PeriodoOperacionImssRowMapperHelper());
    }

    public PeriodosImss obtenerSiguientePeriodo(String cvePeriodoImss) {

        List<PeriodosImss> listaPeriodos = jdbcTemplate.query(SQLConstants.OBTENER_SIGUIENTE_PERIODO_IMSS,
                new Object[] { cvePeriodoImss }, new PeriodosImssRowMapperHelper());

        return !listaPeriodos.isEmpty() ? listaPeriodos.get(0) : null;
    }

    public PeriodoOperacion buscarPeriodoOperacion(String cvePresupuestal, Integer claveTipoCaptura,
            String clavePeriodoIMSS) {

        List<PeriodoOperacion> listaPeriodos = jdbcTemplate.query(SQLConstants.BUSCAR_PERIODO_OPERACION,
                new Object[] { clavePeriodoIMSS, claveTipoCaptura, cvePresupuestal },
                new PeriodoOperacionImssRowMapperHelper());
        return !listaPeriodos.isEmpty() ? listaPeriodos.get(0) : null;
    }

    @Override
    public void abrirPeriodoCerrado(String cvePresupuestal, Integer claveTipoCaptura, String clavePeriodoIMSS) {

        jdbcTemplate.update(SQLConstants.ABRIR_PERIODO_NUEVO_SERVICIO_SUBROGAR,
                new Object[] { cvePresupuestal, claveTipoCaptura, clavePeriodoIMSS });

    }

}
