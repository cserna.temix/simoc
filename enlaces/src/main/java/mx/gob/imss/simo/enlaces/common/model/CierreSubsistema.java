package mx.gob.imss.simo.enlaces.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class CierreSubsistema {

    String clavePresupuestal;
    String periodo;
    int cveSubsistema;
    String refJustificacion;
    int indicadorCerrado;
    Date fechaCierre;
    String cveCapturista;
    Date fechaActualizacion;
    String cveCapturistaActualiza;

}
