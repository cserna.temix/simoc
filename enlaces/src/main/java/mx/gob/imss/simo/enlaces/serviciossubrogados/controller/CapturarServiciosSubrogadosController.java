package mx.gob.imss.simo.enlaces.serviciossubrogados.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;

import com.ibm.icu.text.SimpleDateFormat;

import lombok.Data;
import mx.gob.imss.simo.enlaces.common.controller.EnlacesCommonController;
import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogar;
import mx.gob.imss.simo.enlaces.common.services.CatalogosEnlacesServices;
import mx.gob.imss.simo.enlaces.serviciossubrogados.services.CapturarServiciosSubrogadosServices;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.IdentificadorConexionEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesAdvertenciaConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.SexoEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCalculoEdadEnum;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoRegimenAgregadoMedicoEnum;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosSubrogados;
import mx.gob.imss.simo.hospitalizacion.common.model.UnidadMedica;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;
import mx.gob.imss.simo.model.DatosSesion;

@Data
@ViewScoped
@ManagedBean(name = "serviciosSubrogadosController")
public class CapturarServiciosSubrogadosController extends EnlacesCommonController {

    private static final long serialVersionUID = 5046746945073129865L;

    @ManagedProperty("#{capturarServiciosSubrogadosServices}")
    private CapturarServiciosSubrogadosServices capturarServiciosSubrogadosServices;

    @ManagedProperty("#{catalogosEnlacesServices}")
    private CatalogosEnlacesServices catalogosEnlacesServices;

    @ManagedProperty("#{catalogosHospitalizacionServices}")
    protected CatalogosHospitalizacionServices catalogosHospitalizacionServices;

    private boolean periodoCerrado;
    private Boolean[] requeridos;
    private Date fechaActual;
    private String edad;
    private String delegacion;
    private String fechaActualString;
    private String focoAnterior;
    private String cvePeriodoImss;

    private List<DatosCatalogo> catalogoMotivoSubrogacion;
    private List<DatosCatalogo> catalogoTipoContrato;
    private List<DatosCatalogo> catalogoTipoServicio;
    private List<DatosPaciente> pacientes;

    private CommandButton buttonNo;
    private CommandButton buttonSi;
    private InputNumber numberCostoUnitario;
    private InputText textCantidadEventos;
    private InputText textClave;
    private InputText textDelegacion;
    private InputText textDescripcion;
    private InputText textDescripcionServicioSubrogar;
    private InputText textEdad;
    private InputText textFolioContrato;
    private InputText textGrupoSubrogar;
    private InputText textNumero;
    private InputText textServicioSubrogar;
    private SelectBooleanCheckbox checkboxPeriodoCerrado;
    private SelectOneMenu selectMotivoSubrogacion;
    private SelectOneMenu selectTipoContrato;
    private SelectOneMenu selectTipoServicio;

    @Override
    @PostConstruct
    public void init() {

        super.init();
        setPeriodoCerrado(Boolean.FALSE);
        setFechaActual(new Date());
        setFechaActualString(new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getFechaActual()));
        setCatalogoMotivoSubrogacion(catalogosEnlacesServices.obtenerCatalogoMotivoSubrogacion());
        setCatalogoTipoContrato(catalogosEnlacesServices.obtenerCatalogoTipoContrato());
        setCatalogoTipoServicio(catalogosEnlacesServices.obtenerCatalogoTipoServicio());
        setButtonNo(new CommandButton());
        setButtonSi(new CommandButton());
        setNumberCostoUnitario(new InputNumber());
        setTextCantidadEventos(new InputText());
        setTextClave(new InputText());
        setTextDelegacion(new InputText());
        setTextDescripcion(new InputText());
        setTextDescripcionServicioSubrogar(new InputText());
        setTextEdad(new InputText());
        setTextFolioContrato(new InputText());
        setTextGrupoSubrogar(new InputText());
        setTextNumero(new InputText());
        setTextServicioSubrogar(new InputText());
        setCheckboxPeriodoCerrado(new SelectBooleanCheckbox());
        setSelectMotivoSubrogacion(new SelectOneMenu());
        setSelectTipoContrato(new SelectOneMenu());
        setSelectTipoServicio(new SelectOneMenu());
        setCvePeriodoImss(BaseConstants.CADENA_VACIA);
        deshabilitarCamposInicio();
        inicializarRequeridos();
        setTieneFoco(BaseConstants.FOCO_NSS);

    }

    public void inicializarRequeridos() {

        setRequeridos(new Boolean[11]);
        for (int i = 0; i < getRequeridos().length; i++) {
            getRequeridos()[i] = Boolean.FALSE;
        }
    }

    public void validarNss() {

        try {
            capturarServiciosSubrogadosServices.validarNss(getDatosHospitalizacion().getDatosPaciente().getNss());
            getButtonCancelar().setDisabled(Boolean.FALSE);
            getRequeridos()[0] = Boolean.TRUE;
            habilitarBotonGuardar();
            
            checkboxPeriodoCerrado.setDisabled(Boolean.TRUE);
            setTieneFoco(getTextAgregadoMedico().getClientId());
        } catch (HospitalizacionException ex) {
            getDatosHospitalizacion().getDatosPaciente().setNss(null);
            getRequeridos()[0] = Boolean.FALSE;
            agregarMensajeError(ex);
            setTieneFoco(getTextNss().getClientId());
        }
    }

    @Override
    public void validarAgregadoMedico() {

        try {
            super.validarAgregadoMedico();
            setPacientes(getHospitalizacionCommonServices().buscarPaciente(getDatosHospitalizacion().getDatosPaciente(),
                    new SimpleDateFormat(BaseConstants.FORMATO_FECHA).format(getFechaActual())));
            if (getPacientes().size() < 2) {
                getDatosHospitalizacion().setDatosPaciente(getPacientes().get(0));
                if (getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())
                        || getDatosHospitalizacion().getDatosPaciente().getNombre().isEmpty()) {
                    // Derechohabiente no encontrado / No derechohabiente
                    habilitarCamposNoDerechohabiente();
                    limpiarCatalogos();
                    habilitarCamposNoEncontradoNoDerechohabiente();
                } else {
                    // Derechohabiente encontrado
                    deshabilitarCamposDerechohabiente();
                    habilitarCampoEdad();
                    setDelegacion(getDatosHospitalizacion().getDatosPaciente().getDelegacion());
                    List<DatosCatalogo> delegaciones = catalogosHospitalizacionServices
                            .obtenerCatalogoDelegacion(getDatosHospitalizacion().getDatosPaciente().getNumero());
                    obtenerDelegacion(delegaciones);
                    setTieneFoco(getSelectTipoContrato().getClientId());
                }
                getDatosHospitalizacion().getDatosPaciente()
                        .setSexo(SexoEnum
                                .parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                        .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                                .getClave());
            } else {
                RequestContext.getCurrentInstance().execute(BaseConstants.ABRIR_MODAL_PACIENTES);
                setTieneFoco(getTextNumeroPaciente().getClientId());
            }
            getRequeridos()[1] = Boolean.TRUE;
            habilitarBotonGuardar();
        } catch (HospitalizacionException ex) {
            DatosPaciente pacienteTemp = new DatosPaciente();
            pacienteTemp.setNss(getDatosHospitalizacion().getDatosPaciente().getNss());
            getDatosHospitalizacion().setDatosPaciente(pacienteTemp);
            getDatosHospitalizacion().setDatosSubrogados(new DatosSubrogados());
            setEdad(null);
            deshabilitarCamposInicio();
            getButtonCancelar().setDisabled(Boolean.FALSE);
            getRequeridos()[1] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getTextAgregadoMedico().getClientId());
        } catch (Exception e) {
            logger.error("Ha ocurrido un error: ", e);
        }
    }

    public void validarEdad() {

        try {
            capturarServiciosSubrogadosServices.validarEdad(getEdad());
            getDatosHospitalizacion().getDatosPaciente().setEdadSemanas(Integer.valueOf(getEdad()));
            getDatosHospitalizacion().getDatosPaciente()
                    .setEdadAnios(Integer.valueOf(getEdad()) / BaseConstants.SEMANAS_ANIO);
            setTieneFoco(getTextNombre().getClientId());
        } catch (HospitalizacionException ex) {
            setEdad(null);
            agregarMensajeError(ex);
            setTieneFoco(getTextEdad().getClientId());
        }
    }

    public void validarNombre() {

        try {
            getDatosHospitalizacion().getDatosPaciente()
                    .setNombre(getDatosHospitalizacion().getDatosPaciente().getNombre().toUpperCase());
            capturarServiciosSubrogadosServices.validarNombre(getDatosHospitalizacion().getDatosPaciente().getNombre());
            setTieneFoco(getTextApellidoPaterno().getClientId());
        } catch (HospitalizacionException ex) {
            agregarMensajeError(ex);
            setTieneFoco(getTextNombre().getClientId());
        }
    }

    public void validarApellidoPaterno() {

        try {
            getDatosHospitalizacion().getDatosPaciente().setApellidoPaterno(
                    getDatosHospitalizacion().getDatosPaciente().getApellidoPaterno().toUpperCase());
            capturarServiciosSubrogadosServices
                    .validarApellidoPaterno(getDatosHospitalizacion().getDatosPaciente().getApellidoPaterno());
            setTieneFoco(getTextApellidoMaterno().getClientId());
        } catch (HospitalizacionException ex) {
            agregarMensajeError(ex);
            setTieneFoco(getTextApellidoPaterno().getClientId());
        }
    }

    public void validarApellidoMaterno() {

        getDatosHospitalizacion().getDatosPaciente()
                .setApellidoMaterno(getDatosHospitalizacion().getDatosPaciente().getApellidoMaterno().toUpperCase());
        setTieneFoco(getTextNumero().getClientId());
    }

    public void validarNumero() {

        try {
            getDatosHospitalizacion().getDatosPaciente()
                    .setNumero(getDatosHospitalizacion().getDatosPaciente().getNumero().toUpperCase());
            capturarServiciosSubrogadosServices.validarNumero(getDatosHospitalizacion().getDatosPaciente().getNumero());
            setCatalogoDelegacion(catalogosHospitalizacionServices
                    .obtenerCatalogoDelegacion(getDatosHospitalizacion().getDatosPaciente().getNumero()));
            setCatalogoDelegacionString(convertirCatalogoString(getCatalogoDelegacion()));
            setTieneFoco(getAutoDelegacion().getClientId());
        } catch (HospitalizacionException ex) {
            getDatosHospitalizacion().getDatosPaciente().setNumero(null);
            agregarMensajeError(ex);
            setTieneFoco(getTextNumero().getClientId());
        }
    }

    public void validarDelegacion() {

        try {
            DatosCatalogo datosCatalogo;
            datosCatalogo = validaAutoComplete(getAutoDelegacion(), BaseConstants.LONGITUD_DELEGACION,
                    getCatalogoDelegacionString(), getCatalogoDelegacion());
            if (null != datosCatalogo) {
                getDatosHospitalizacion().getDatosPaciente().setDelegacion(datosCatalogo.getClave());
                capturarServiciosSubrogadosServices.validarDelegacion(
                        getDatosHospitalizacion().getDatosPaciente().getDelegacion(), getCatalogoDelegacion());
                if (!getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                        .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())) {
                    UnidadMedica unidadMedica = capturarServiciosSubrogadosServices.buscarUnidadMedica(
                            getDatosHospitalizacion().getDatosPaciente().getNumero(),
                            getDatosHospitalizacion().getDatosPaciente().getDelegacion());
                    getDatosHospitalizacion().getDatosPaciente()
                            .setClavePresupuestal(unidadMedica.getCvePresupuestal());
                    getDatosHospitalizacion().getDatosPaciente()
                            .setDescripcionUnidad(unidadMedica.getDesUnidadMedica());
                    setTieneFoco(getSelectTipoContrato().getClientId());
                }
            } else {
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                        getArray(BaseConstants.REQUERIDO_DELEGACION));
            }
        } catch (HospitalizacionException ex) {
            if (!getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                    .substring(BaseConstants.INICIO_AGREGADO_REGIMEN)
                    .equals(TipoRegimenAgregadoMedicoEnum.NO_DERECHOHABIENTE.getClave())) {
                getDatosHospitalizacion().getDatosPaciente().setClavePresupuestal(null);
                getDatosHospitalizacion().getDatosPaciente().setDescripcionUnidad(null);
            }
            agregarMensajeError(ex);
            setTieneFoco(getAutoDelegacion().getClientId());
        }
    }

    public void validarTipoContrato() {

        try {
            capturarServiciosSubrogadosServices
                    .validarTipoContrato(getDatosHospitalizacion().getDatosSubrogados().getTipoContrato());
            getRequeridos()[2] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getTextFolioContrato().getClientId());
        } catch (HospitalizacionException ex) {
            getRequeridos()[2] = Boolean.FALSE;
            habilitarBotonGuardar();
            setTieneFoco(getSelectTipoContrato().getClientId());
            agregarMensajeError(ex);
        }
    }

    public void validarFolioContrato() {

        try {
            getDatosHospitalizacion().getDatosSubrogados()
                    .setFolioContrato(getDatosHospitalizacion().getDatosSubrogados().getFolioContrato().toUpperCase());
            capturarServiciosSubrogadosServices
                    .validarFolioContrato(getDatosHospitalizacion().getDatosSubrogados().getFolioContrato());
            getRequeridos()[3] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getSelectTipoServicio().getClientId());
        } catch (HospitalizacionException ex) {
            getDatosHospitalizacion().getDatosSubrogados().setFolioContrato(null);
            getRequeridos()[3] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getTextFolioContrato().getClientId());
        }
    }

    public void validarTipoServicio() {

        try {
            capturarServiciosSubrogadosServices
                    .validarTipoServicio(getDatosHospitalizacion().getDatosSubrogados().getTipoServicio());
            getRequeridos()[4] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getSelectMotivoSubrogacion().getClientId());
        } catch (HospitalizacionException ex) {
            getRequeridos()[4] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getSelectTipoServicio().getClientId());
        }
    }

    public void validarMotivoSubrogacion() {

        try {
            capturarServiciosSubrogadosServices
                    .validarMotivoSubrogacion(getDatosHospitalizacion().getDatosSubrogados().getMotivoSubrogacion());
            getRequeridos()[5] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getTextServicioSubrogar().getClientId());
        } catch (HospitalizacionException ex) {
            getRequeridos()[5] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getSelectMotivoSubrogacion().getClientId());
        }
    }

    public void validarServiciosSubrogar() {

        try {
            capturarServiciosSubrogadosServices.validarServiciosSubrogar(
                    getDatosHospitalizacion().getDatosSubrogados().getClaveServicioSubrogar());
            ServicioSubrogar servicioSubrogar = capturarServiciosSubrogadosServices
                    .buscarServicioSubrogar(getDatosHospitalizacion().getDatosSubrogados().getClaveServicioSubrogar());
            getDatosHospitalizacion().getDatosSubrogados()
                    .setDescripcionServicioSubrogar(servicioSubrogar.getDesServicioSubrogar());
            getDatosHospitalizacion().getDatosSubrogados()
                    .setDescripcionGrupoSubrogar(servicioSubrogar.getDesGrupoSubrogar());
            getRequeridos()[6] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getTextCantidadEventos().getClientId());
        } catch (HospitalizacionException ex) {
            getDatosHospitalizacion().getDatosSubrogados().setClaveServicioSubrogar(null);
            getDatosHospitalizacion().getDatosSubrogados().setDescripcionServicioSubrogar(null);
            getDatosHospitalizacion().getDatosSubrogados().setDescripcionGrupoSubrogar(null);
            getRequeridos()[6] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getTextServicioSubrogar().getClientId());
        }
    }

    public void validarCantidadEventos() {

        try {

            capturarServiciosSubrogadosServices
                    .validarCantidadEventos(getDatosHospitalizacion().getDatosSubrogados().getCantidadEventos());
            getRequeridos()[7] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getNumberCostoUnitario().getClientId());
        } catch (HospitalizacionException ex) {
            getDatosHospitalizacion().getDatosSubrogados().setCantidadEventos(null);
            getRequeridos()[7] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getTextCantidadEventos().getClientId());
        }
    }

    public void validarCostoUnitario() {

        try {
            capturarServiciosSubrogadosServices
                    .validarCostoUnitario(getDatosHospitalizacion().getDatosSubrogados().getCostoUnitario());
            getRequeridos()[8] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getTextMatricula().getClientId());
        } catch (HospitalizacionException ex) {
            getDatosHospitalizacion().getDatosSubrogados().setCostoUnitario(null);
            getRequeridos()[8] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getNumberCostoUnitario().getClientId());
        }
    }

    public void validarMatricula() {

        try {
            capturarServiciosSubrogadosServices
                    .validarMatricula(getDatosHospitalizacion().getDatosMedico().getMatricula());
            getDatosHospitalizacion().setDatosMedico(capturarServiciosSubrogadosServices.buscarPersonalOperativo(
                    getDatosHospitalizacion().getDatosMedico(), obtenerDatosUsuario().getCvePresupuestal()));
            if (getDatosHospitalizacion().getDatosMedico().getIdentificadorConexion()
                    .equals(IdentificadorConexionEnum.ROJO.getRuta())) {
                agregarMensajeAdvertencia(getArray(MensajesAdvertenciaConstants.MA_013), null);
            }
            getRequeridos()[9] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getMaskFecha().getClientId());
        } catch (HospitalizacionException ex) {
            getDatosHospitalizacion().setDatosMedico(new DatosMedico());
            getRequeridos()[9] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getTextMatricula().getClientId());
        }
    }

    public void validarFechaServicio() {

        try {
            cvePeriodoImss = capturarServiciosSubrogadosServices.validarFechaServicio(
                    getDatosHospitalizacion().getDatosSubrogados().getFechaServicio(),
                    obtenerDatosUsuario().getCvePresupuestal());
            getButtonGuardar().setDisabled(Boolean.FALSE);
            //getCheckboxPeriodoCerrado().setDisabled(Boolean.FALSE);
            //setPeriodoCerrado(Boolean.FALSE);
            getRequeridos()[10] = Boolean.TRUE;
            habilitarBotonGuardar();
            setTieneFoco(getButtonGuardar().getClientId());
        } catch (HospitalizacionException ex) {
            getDatosHospitalizacion().getDatosSubrogados().setFechaServicio(null);
            getButtonGuardar().setDisabled(Boolean.TRUE);
            getCheckboxPeriodoCerrado().setDisabled(Boolean.TRUE);
//            if (ex.getClaves()[0].equals(MensajesErrorConstants.ME_009)) {
//                setPeriodoCerrado(Boolean.TRUE);
//            }
            getRequeridos()[10] = Boolean.FALSE;
            habilitarBotonGuardar();
            agregarMensajeError(ex);
            setTieneFoco(getMaskFecha().getClientId());
        }
    }

    public void validarPeriodoCerrado() {

        if (isPeriodoCerrado()) {
            RequestContext.getCurrentInstance().execute(BaseConstants.MOSTRAR_MODAL_CIERRE_MES);
            setTieneFoco(getButtonSi().getClientId());
        }
    }

    public void cerrarPeriodoCaptura() {

        try {
            capturarServiciosSubrogadosServices.cerrarPeriodoActivo(obtenerDatosUsuario().getCvePresupuestal(),
                    getObjetosSs().getPeriodoActual());
            cerrarModal(Boolean.FALSE);
        } catch (HospitalizacionException e) {
            logger.error("Error al cerrar periodo: " + e);
        }
        getCheckboxPeriodoCerrado().setDisabled(Boolean.TRUE);
        setTieneFoco(getTextNss().getClientId());
    }

    public void cerrarModal(boolean desactivarCheck) {

        RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_CIERRE_MES);
        if (desactivarCheck) {
            setPeriodoCerrado(Boolean.FALSE);
        }
        setTieneFoco(getTextNss().getClientId());
    }

    public void guardar() {

        try {
            capturarServiciosSubrogadosServices.guardar(getDatosHospitalizacion(), obtenerDatosUsuario());
            agregarMensajeInformativo(getArray(MensajesGeneralesConstants.MG_002), null);
            limpiarDatos();
        } catch (HospitalizacionException ex) {
            agregarMensajeError(ex);
            setTieneFoco(getButtonGuardar().getClientId());
        }
    }

    public void limpiarDatos() {

        setPeriodoCerrado(Boolean.FALSE);
        setEdad(null);
        setDelegacion(null);
        setDatosHospitalizacion(new DatosHospitalizacion());
        getSelectMotivoSubrogacion().resetValue();
        getSelectTipoContrato().resetValue();
        getSelectTipoServicio().resetValue();
        setSelectMotivoSubrogacion(new SelectOneMenu());
        setSelectTipoContrato(new SelectOneMenu());
        setSelectTipoServicio(new SelectOneMenu());
        deshabilitarCamposInicio();
        limpiarCatalogos();
        inicializarRequeridos();
        getCheckboxPeriodoCerrado().setDisabled(Boolean.FALSE);
        setTieneFoco(getTextNss().getClientId());
    }

    public void habilitarBotonGuardar() {

        if (capturarServiciosSubrogadosServices.sePuedeActivarGuardar(getRequeridos())) {
            getButtonGuardar().setDisabled(Boolean.FALSE);
            setTieneFoco(getButtonGuardar().getClientId());
        } else {
            getButtonGuardar().setDisabled(Boolean.TRUE);
        }
    }

    public void habilitarPeriodoCerrado() {

        if (capturarServiciosSubrogadosServices.sePuedeActivarGuardar(getRequeridos())) {
            getCheckboxPeriodoCerrado().setDisabled(Boolean.FALSE);
            setTieneFoco(getCheckboxPeriodoCerrado().getClientId());
        } else {
            getCheckboxPeriodoCerrado().setDisabled(Boolean.TRUE);
        }
    }

    public void habilitarCamposNoDerechohabiente() {

        if (getCapturarServiciosSubrogadosServices().habilitarEdad(getFechaActualString(),
                getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
            setEdad(null);
            getTextEdad().setDisabled(Boolean.FALSE);
            setTieneFoco(getTextEdad().getClientId());
        } else {
            Map<TipoCalculoEdadEnum, Integer> edades = getCapturarServiciosSubrogadosServices()
                    .calcularEdadPacienteNoEncontrado(
                            getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                    .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO),
                            getFechaActualString());
            getDatosHospitalizacion().getDatosPaciente().setEdadAnios(edades.get(TipoCalculoEdadEnum.ANIOS));
            getDatosHospitalizacion().getDatosPaciente().setEdadSemanas(edades.get(TipoCalculoEdadEnum.SEMANAS));
            setEdad(getDatosHospitalizacion().getDatosPaciente().getEdadAnios().toString());
            getTextEdad().setDisabled(Boolean.TRUE);
            setTieneFoco(getTextNombre().getClientId());
        }
    }

    public void limpiarCatalogos() {

        setCatalogoDelegacion(new ArrayList<DatosCatalogo>());
    }

    public void habilitarCamposNoEncontradoNoDerechohabiente() {

        getTextNombre().setDisabled(Boolean.FALSE);
        getTextApellidoPaterno().setDisabled(Boolean.FALSE);
        getTextApellidoMaterno().setDisabled(Boolean.FALSE);
        getTextNumero().setDisabled(Boolean.FALSE);
        getAutoDelegacion().setDisabled(Boolean.FALSE);
    }

    public void deshabilitarCamposInicio() {

        getTextEdad().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getTextNumero().setDisabled(Boolean.TRUE);
        getTextGrupoSubrogar().setDisabled(Boolean.TRUE);
        getTextClave().setDisabled(Boolean.TRUE);
        getTextDescripcion().setDisabled(Boolean.TRUE);
        getTextDescripcionServicioSubrogar().setDisabled(Boolean.TRUE);
        getTextMedico().setDisabled(Boolean.TRUE);
        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);
        getAutoDelegacion().setDisabled(Boolean.TRUE);
    }

    public void deshabilitarCamposDerechohabiente() {

        getTextEdad().setDisabled(Boolean.TRUE);
        getTextNombre().setDisabled(Boolean.TRUE);
        getTextApellidoPaterno().setDisabled(Boolean.TRUE);
        getTextApellidoMaterno().setDisabled(Boolean.TRUE);
        getTextNumero().setDisabled(Boolean.TRUE);
        getAutoDelegacion().setDisabled(Boolean.TRUE);
    }

    public void habilitarCampoEdad() {

        if (getCapturarServiciosSubrogadosServices().habilitarEdad(getFechaActualString(),
                getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
            setEdad(getDatosHospitalizacion().getDatosPaciente().getEdadSemanas().toString());
        } else {
            setEdad(getDatosHospitalizacion().getDatosPaciente().getEdadAnios().toString());
        }
    }

    public void obtenerDelegacion(List<DatosCatalogo> delegacion) {

        for (DatosCatalogo delegacionAux : delegacion) {
            if (delegacionAux.getClave().equals(getDelegacion())) {
                getDatosHospitalizacion().getDatosPaciente().setDelegacion(
                        delegacionAux.getClave() + BaseConstants.ESPACIO + delegacionAux.getDescripcion());
            }
        }
    }

    public void mostrarPacienteSeleccionado() {

        if (getNumeroPaciente() == null || getNumeroPaciente() > getPacientes().size()) {
            setNumeroPaciente(null);
            setTieneFoco(getTextNumeroPaciente().getClientId());
        } else if (getNumeroPaciente() == 0) {
            habilitarPacienteFueraDeNucleo();
        } else {
            try {
                getDatosHospitalizacion().setDatosPaciente(getPacientes().get(getNumeroPaciente() - 1));
                deshabilitarCamposDerechohabiente();
                habilitarCampoEdad();
                setDelegacion(getDatosHospitalizacion().getDatosPaciente().getDelegacion());
                List<DatosCatalogo> delegaciones = catalogosHospitalizacionServices
                        .obtenerCatalogoDelegacion(getDatosHospitalizacion().getDatosPaciente().getNumero());
                obtenerDelegacion(delegaciones);
                getDatosHospitalizacion().getDatosPaciente()
                        .setSexo(SexoEnum
                                .parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                        .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                                .getClave());
                setPacientes(new ArrayList<DatosPaciente>());
                setNumeroPaciente(null);
                RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES);
                setTieneFoco(getSelectTipoContrato().getClientId());
            } catch (HospitalizacionException e) {
                DatosPaciente pacienteTemp = new DatosPaciente();
                pacienteTemp.setNss(getDatosHospitalizacion().getDatosPaciente().getNss());
                getDatosHospitalizacion().setDatosPaciente(pacienteTemp);
                getDatosHospitalizacion().setDatosSubrogados(new DatosSubrogados());
                setNumeroPaciente(null);
                agregarMensajeError(e);
                setTieneFoco(getTextNumeroPaciente().getClientId());
            }
        }
    }

    public void habilitarPacienteFueraDeNucleo() {

        getDatosHospitalizacion().getDatosPaciente().setCvePaciente(null);
        getDatosHospitalizacion().getDatosPaciente().setNombre(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setApellidoPaterno(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setApellidoMaterno(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setNumero(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setDelegacion(BaseConstants.CADENA_VACIA);
        getAutoDelegacion().resetValue();
        getDatosHospitalizacion().getDatosPaciente().setClavePresupuestal(BaseConstants.CADENA_VACIA);
        getDatosHospitalizacion().getDatosPaciente().setDescripcionUnidad(BaseConstants.CADENA_VACIA);
        if (capturarServiciosSubrogadosServices.habilitarEdad(getFechaActualString(),
                getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                        .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO))) {
            setEdad(BaseConstants.CADENA_VACIA);
            getTextEdad().setDisabled(Boolean.FALSE);
            setTieneFoco(getTextEdad().getClientId());
        } else {
            Map<TipoCalculoEdadEnum, Integer> edades = capturarServiciosSubrogadosServices
                    .calcularEdadPacienteNoEncontrado(
                            getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                    .substring(BaseConstants.INICIO_AGREGADO_ANIO, BaseConstants.FIN_AGREGADO_ANIO),
                            getFechaActualString());
            getDatosHospitalizacion().getDatosPaciente().setEdadAnios(edades.get(TipoCalculoEdadEnum.ANIOS));
            getDatosHospitalizacion().getDatosPaciente().setEdadSemanas(edades.get(TipoCalculoEdadEnum.SEMANAS));
            setEdad(getDatosHospitalizacion().getDatosPaciente().getEdadAnios().toString());
            getTextEdad().setDisabled(Boolean.TRUE);
            setTieneFoco(getTextNombre().getClientId());
        }
        getDatosHospitalizacion().getDatosPaciente()
                .setSexo(SexoEnum
                        .parse(getDatosHospitalizacion().getDatosPaciente().getAgregadoMedico()
                                .substring(BaseConstants.INICIO_AGREGADO_SEXO, BaseConstants.FIN_AGREGADO_SEXO))
                        .getClave());
        limpiarCatalogos();
        habilitarCamposNoEncontradoNoDerechohabiente();
        RequestContext.getCurrentInstance().execute(BaseConstants.CERRAR_MODAL_PACIENTES);
    }

    @Override
    public String obtenerNombrePagina() {

        return "";
        // LoginController.setRenderIcons(false);
        // return PagesCommonConstants.SERVICIOS_SUBROGADOS;
    }

    @Override
    public void salir() throws IOException {

        DatosSesion datosSesion = super.getDatosSesionService().getDatosSesion(super.getObjetosSs().getUserInfo(),
                super.getObjetosSs().getUsername());
        datosSesion.setMostrarDialogoDelegacion(Boolean.FALSE);
        super.getDatosSesionService().save(datosSesion);
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(BaseConstants.PATH));
        String url = properties.getProperty(BaseConstants.CVE_URL_CE);
        FacesContext.getCurrentInstance().getExternalContext().redirect(url + BaseConstants.PAGINA_INICIO);
    }

}
