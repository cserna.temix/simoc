package mx.gob.imss.simo.enlaces.common.model;

import java.util.List;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.model.EstructuraDatoInformacionComplementaria;

@Data
public class InformacionComplementariaConsulta {

    private String cveRCV;
    private List<EstructuraDatoInformacionComplementaria> datos;
    private String cifraDeControl;

}
