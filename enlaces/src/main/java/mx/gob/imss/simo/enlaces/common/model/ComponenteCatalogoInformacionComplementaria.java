package mx.gob.imss.simo.enlaces.common.model;

import lombok.Data;

@Data
public class ComponenteCatalogoInformacionComplementaria {

    private boolean requerido;
    private String comboId;
    private boolean ultimoRequerido;

}
