package mx.gob.imss.simo.enlaces.common.model;

import lombok.Data;

@Data
public class InformacionComplementariaDato {

	private int cveSubsistemaRcv;
	private String cvePresupuestal;
	private String cvePeriodoImss;
	private int cveDato;
	private Integer numValorDato;
}
