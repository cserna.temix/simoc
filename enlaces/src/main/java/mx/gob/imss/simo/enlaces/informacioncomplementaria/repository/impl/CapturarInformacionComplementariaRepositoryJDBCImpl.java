package mx.gob.imss.simo.enlaces.informacioncomplementaria.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import mx.gob.imss.simo.enlaces.common.constant.CapturarInformacionComplementariaConstants;
import mx.gob.imss.simo.enlaces.common.constant.SQLCatalogosConstants;
import mx.gob.imss.simo.enlaces.common.constant.SQLConstants;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoEnRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoHojaRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoPoblacionRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoPorRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoRCVRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoSubclaveRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CatalogoSubsistemaRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.CierreSubsistemaRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.DatoFormulaRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.InformacionComplementariaDatoRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.InformacionComplementariaRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.SubsistemaRCVDatoRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.SubsistemaRCVRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.helper.SubsistemaRowMapperHelper;
import mx.gob.imss.simo.enlaces.common.model.CierreSubsistema;
import mx.gob.imss.simo.enlaces.common.model.DatoFormula;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaDato;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCVDato;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.repository.CapturarInformacionComplementariaRepository;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.helper.ConsultaCountRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.PeriodoOperacionImssRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.helper.PeriodosImssRowMapperHelper;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.repository.impl.HospitalizacionCommonRepositoryJDBCImpl;

@Repository
public class CapturarInformacionComplementariaRepositoryJDBCImpl extends HospitalizacionCommonRepositoryJDBCImpl
        implements CapturarInformacionComplementariaRepository {

    @Override
    public Subsistema obtenerSubsistema(int subsistema) {

        List<Subsistema> subsistemas = jdbcTemplate.query(SQLConstants.BUSCAR_SUBSISTEMA, new Object[] { subsistema },
                new SubsistemaRowMapperHelper());

        if (null != subsistemas && !subsistemas.isEmpty()) {
            return subsistemas.get(0);
        }
        return null;
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoSubsistemas() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_SUBSISTEMA,
                new CatalogoSubsistemaRowMapperHelper());
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoRCV(int cveSubsistema) {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_RCV, new Object[] { cveSubsistema },
                new CatalogoRCVRowMapperHelper());
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoPoblacion() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_POBLACION,
                new CatalogoPoblacionRowMapperHelper());
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoPor() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_POR, new CatalogoPorRowMapperHelper());
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoEn() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_EN, new CatalogoEnRowMapperHelper());
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoHoja() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_HOJA, new CatalogoHojaRowMapperHelper());
    }

    @Override
    public List<DatosCatalogo> obtenerCatalogoSubclave() {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGO_SUBCLAVE,
                new CatalogoSubclaveRowMapperHelper());
    }

    @Override
    public SubsistemaRCV obtenerSubsistemaRCV(Integer cveSubsistema, Integer cveRCV, Integer cvePoblacion,
            Integer cvePor, Integer cveEn, Integer cveHoja, Integer cveSubclave) {

        StringBuilder query = new StringBuilder(SQLConstants.OBTENER_SUBSISTEMA_RCV);

        List<Object> params = new ArrayList<Object>();

        if (null != cvePoblacion) {
            query.append(CapturarInformacionComplementariaConstants.AND_CVE_POBLACION);
            params.add(cvePoblacion);
        }
        if (null != cvePor) {
            query.append(CapturarInformacionComplementariaConstants.AND_CVE_POR);
            params.add(cvePor);
        }
        if (null != cveEn) {
            query.append(CapturarInformacionComplementariaConstants.AND_CVE_EN);
            params.add(cveEn);
        }
        if (null != cveHoja) {
            query.append(CapturarInformacionComplementariaConstants.AND_CVE_HOJA);
            params.add(cveHoja);
        }
        if (null != cveSubclave) {
            query.append(CapturarInformacionComplementariaConstants.AND_CVE_SUBCLAVE);
            params.add(cveSubclave);
        }
        query.append(CapturarInformacionComplementariaConstants.ORDER_BY_CVE_SUBSISTEMA_RCV);

        Object[] parametros = new Object[params.size() + 2];
        parametros[0] = cveSubsistema;
        parametros[1] = cveRCV;
        int i = 2;
        for (Object obj : params) {
            parametros[i] = obj;
            i += 1;
        }

        List<SubsistemaRCV> informacionComplementariaList = jdbcTemplate.query(query.toString(), parametros,
                new SubsistemaRCVRowMapperHelper());

        if (null != informacionComplementariaList && !informacionComplementariaList.isEmpty()) {
            return informacionComplementariaList.get(0);
        }
        return null;
    }

    @Override
    public List<SubsistemaRCVDato> obtenerSubsistemaRCVDato(long clave) {

        return jdbcTemplate.query(SQLConstants.OBTENER_SUBSISTEMA_RCV_DATO, new Object[] { clave },
                new SubsistemaRCVDatoRowMapperHelper());

    }

    @Override
    public List<SubsistemaRCV> obtenerCatalogosInformacionComplementaria(long clave, int cveRCV) {

        return jdbcTemplate.query(SQLCatalogosConstants.OBTENER_CATALOGOS_SUBSISTEMA_RCV,
                new Object[] { clave, cveRCV }, new SubsistemaRCVRowMapperHelper());
    }

    @Override
    public InformacionComplementariaDato obtenerInformacionComplementariaDato(long cveSubsistema,
            String cvePresupuestal, String cvePeriodoIMSS, int cveDato) {

        List<InformacionComplementariaDato> informacionComplementariaDatoList = jdbcTemplate.query(
                SQLConstants.OBTENER_INFORMACION_COMPLEMENTARIA_DATO,
                new Object[] { cveSubsistema, cvePresupuestal, cvePeriodoIMSS, cveDato },
                new InformacionComplementariaDatoRowMapperHelper());

        if (null != informacionComplementariaDatoList && !informacionComplementariaDatoList.isEmpty()) {
            return informacionComplementariaDatoList.get(0);
        }
        return null;
    }

    @Override
    public List<DatoFormula> obtenerDatoFormula(long cveSubsistemaRCV, int cveDato) {

        return jdbcTemplate.query(SQLConstants.OBTENER_FORMULA_DATO, new Object[] { cveSubsistemaRCV, cveDato },
                new DatoFormulaRowMapperHelper());

    }

    @Override
    public List<CierreSubsistema> obtenerSubsistemasCerrados(String cvePresupuestal, String cvePeriodoIMSS) {

        return jdbcTemplate.query(SQLConstants.OBTENER_SUBSISTEMAS_CIERRE,
                new Object[] { cvePresupuestal, cvePeriodoIMSS }, new CierreSubsistemaRowMapperHelper());
    }

    @Override
    public void guardarInformacionComplementaria(InformacionComplementaria informacionComplementaria, int cveSubsistema,
            int cveRCV) {

        jdbcTemplate.update(SQLConstants.GUARDAR_INFORMACION_COMPLEMENTARIA,
                new Object[] { informacionComplementaria.getCveSubsistemaRcv(),
                        informacionComplementaria.getCvePresupuestal(), informacionComplementaria.getCvePeriodoImss(),
                        informacionComplementaria.getNumCifraControl(), informacionComplementaria.getFecCreacion(),
                        informacionComplementaria.getCveCapturista(), cveSubsistema, cveRCV });
    }

    @Override
    public void guardarDatosInformacionComplementaria(
            List<InformacionComplementariaDato> datosInfromacionComplementaria) {

        for (InformacionComplementariaDato informacionComplementariaDato : datosInfromacionComplementaria) {
            jdbcTemplate.update(SQLConstants.GUARDAR_DATOS_INFORMACION_COMPLEMENTARIA,
                    new Object[] { informacionComplementariaDato.getCveSubsistemaRcv(),
                            informacionComplementariaDato.getCvePresupuestal(),
                            informacionComplementariaDato.getCvePeriodoImss(),
                            informacionComplementariaDato.getCveDato(),
                            informacionComplementariaDato.getNumValorDato() });

        }
    }

    public InformacionComplementaria buscarInformacionExistente(Integer cveSubsistemaRcv, String cvePresupuestal,
            String cvePeriodo, Integer cveSubsistema, Integer cveRCV) {

        List<InformacionComplementaria> informacionComplementaria = jdbcTemplate.query(
                SQLConstants.BUSCAR_INFORMACION_EXISTENTE,
                new Object[] { cveSubsistemaRcv, cvePresupuestal, cvePeriodo, cveSubsistema, cveRCV },
                new InformacionComplementariaRowMapperHelper());

        if (informacionComplementaria != null && !informacionComplementaria.isEmpty()) {
            return informacionComplementaria.get(0);
        }
        return null;
    }

    @Override
    public void actualizarInformacionComplementaria(InformacionComplementaria informacionExistente) {

        jdbcTemplate.update(SQLConstants.ACTUALIZAR_INFORMACION_COMPLEMENTARIA,
                new Object[] { informacionExistente.getNumCifraControl(), informacionExistente.getFecActualizacion(),
                        informacionExistente.getCveCapturistaActualiza(), informacionExistente.getCveSubsistemaRcv(),
                        informacionExistente.getCvePresupuestal(), informacionExistente.getCvePeriodoImss() });
    }

    @Override
    public List<InformacionComplementariaDato> obtenerDatosInformacionComplementaria(
            InformacionComplementaria informacionExistente) {

        List<InformacionComplementariaDato> informacionComplementariaDatos = jdbcTemplate
                .query(SQLConstants.OBTENER_DATOS_INFORMACION_COMPLEMENTARIA,
                        new Object[] { informacionExistente.getCveSubsistemaRcv(),
                                informacionExistente.getCvePresupuestal(), informacionExistente.getCvePeriodoImss() },
                        new InformacionComplementariaDatoRowMapperHelper());

        return informacionComplementariaDatos;
    }

    public void actualizarDatosInformacionComplementaria(List<InformacionComplementariaDato> datosActualizados) {

        for (InformacionComplementariaDato dato : datosActualizados) {

            jdbcTemplate.update(SQLConstants.ACTUALIZAR_DATOS_INFORMACION_COMPLEMENTARIA,
                    new Object[] { dato.getNumValorDato(), dato.getCveSubsistemaRcv(), dato.getCvePresupuestal(),
                            dato.getCvePeriodoImss(), dato.getCveDato() });
        }
    }

    @Override
    public List<InformacionComplementaria> consultarInformacionComplementariaPorSubsistema(String cvePresupuestal,
            String cvePeriodo, int cveSubsistema) {

        List<InformacionComplementaria> informacionComplementaria = jdbcTemplate.query(
                SQLConstants.CONSULTAR_INFORMACION_COMPLEMENTARIA_POR_SUBSISTEMA,
                new Object[] { cvePresupuestal, cvePeriodo, cveSubsistema },
                new InformacionComplementariaRowMapperHelper());

        if (informacionComplementaria != null && !informacionComplementaria.isEmpty()) {
            return informacionComplementaria;
        }
        return null;
    }

    @Override
    public List<InformacionComplementaria> consultarInformacionComplementariaPorSubsistemaAsc(String cvePresupuestal,
            String cvePeriodo, int cveSubsistema) {

        List<InformacionComplementaria> informacionComplementaria = jdbcTemplate.query(
                SQLConstants.CONSULTAR_INFORMACION_COMPLEMENTARIA_POR_SUBSISTEMA_ASC,
                new Object[] { cvePresupuestal, cvePeriodo, cveSubsistema },
                new InformacionComplementariaRowMapperHelper());

        if (informacionComplementaria != null && !informacionComplementaria.isEmpty()) {
            return informacionComplementaria;
        }
        return null;
    }

    @Override
    public List<InformacionComplementariaDato> consultarInformacionComplementariaDatoPorSubsistemaRCV(
            String cvePresupuestal, String cvePeriodo, int cveSubsistemaRCV) {

        List<InformacionComplementariaDato> informacionComplementariaDatoList = jdbcTemplate.query(
                SQLConstants.CONSULTAR_INFORMACION_COMPLEMENTARIA_DATO_POR_SUBSISTEMA,
                new Object[] { cvePresupuestal, cvePeriodo, cveSubsistemaRCV },
                new InformacionComplementariaDatoRowMapperHelper());

        if (informacionComplementariaDatoList != null && !informacionComplementariaDatoList.isEmpty()) {
            return informacionComplementariaDatoList;
        }
        return null;
    }

    @Override
    public SubsistemaRCV obtenerCantidadRCVParaCifra99(int cveSubsistema) {

        List<SubsistemaRCV> subsistemaRCV = jdbcTemplate.query(SQLConstants.CONSULTAR_CANTIDAD_RCV_PARA_CIFRA_99,
                new Object[] { cveSubsistema }, new SubsistemaRCVRowMapperHelper());

        if (subsistemaRCV != null && !subsistemaRCV.isEmpty()) {
            return subsistemaRCV.get(0);
        }
        return null;
    }

    @Override
    public Long obtenerTotalRCVCapturadosPorSubsistema(int cveSubsistema, String cvePresupuestal, String cvePeriodo) {

        return jdbcTemplate
                .query(SQLConstants.OBTENER_TOTAL_RCV_CAPTURADOS_POR_SUBSISTEMA_PERIODO,
                        new Object[] { cveSubsistema, cvePresupuestal, cvePeriodo }, new ConsultaCountRowMapperHelper())
                .get(0);

    }

    @Override
    public List<InformacionComplementaria> obtenerCifraControlPorSubsistema(int cveSubsistema, String cvePresupuestal,
            String cvePeriodo) {

        return jdbcTemplate.query(SQLConstants.OBTENER_CIFRA_CONTROL_POR_SUBSISTEMA,
                new Object[] { cveSubsistema, cvePresupuestal, cvePeriodo },
                new InformacionComplementariaRowMapperHelper());
    }

    @Override
    public void guardarCierreSubsistema(CierreSubsistema cierreSubsistema) {

        jdbcTemplate.update(SQLConstants.GUARDAR_CIERRE_SUBSISTEMA,
                new Object[] { cierreSubsistema.getClavePresupuestal(), cierreSubsistema.getPeriodo(),
                        cierreSubsistema.getCveSubsistema(), cierreSubsistema.getRefJustificacion(),
                        cierreSubsistema.getIndicadorCerrado(), cierreSubsistema.getFechaCierre(),
                        cierreSubsistema.getCveCapturista(), null, null });
    }

    @Override
    public void actualizarCierreSubsistema(CierreSubsistema cierreSubsistemaExistente) {

        jdbcTemplate.update(SQLConstants.ACTUALIZAR_CIERRE_SUBSISTEMA,
                new Object[] { cierreSubsistemaExistente.getRefJustificacion(),
                        cierreSubsistemaExistente.getFechaActualizacion(),
                        cierreSubsistemaExistente.getCveCapturistaActualiza(),
                        cierreSubsistemaExistente.getClavePresupuestal(), cierreSubsistemaExistente.getPeriodo(),
                        cierreSubsistemaExistente.getCveSubsistema() });
    }

    @Override
    public CierreSubsistema buscarCierreSubsistemaExistente(String cvePresupuestal, String cvePeriodo,
            Integer cveSubsistema) {

        List<CierreSubsistema> cierreSubsistemaExistente = jdbcTemplate.query(
                SQLConstants.BUSCAR_CIERRE_SUBSISTEMA_EXISTENTE,
                new Object[] { cvePresupuestal, cvePeriodo, cveSubsistema }, new CierreSubsistemaRowMapperHelper());

        if (cierreSubsistemaExistente != null && !cierreSubsistemaExistente.isEmpty()) {
            return cierreSubsistemaExistente.get(0);
        }
        return null;
    }

    @Override
    public void abrirSubsistema(String cvePresupuestal, String cvePeriodo, int cveSubsistema, String usuarioActualiza) {

        jdbcTemplate.update(SQLConstants.ABRIR_SUBSISTEMA,
                new Object[] { new Date(), usuarioActualiza, cvePresupuestal, cvePeriodo, cveSubsistema });

    }

    @Override
    public void actualizarCifraControlInformacionComplementariaPorCierre(
            InformacionComplementaria informacionComplementariaPorCierre) {

        jdbcTemplate.update(SQLConstants.ACTUALIZAR_CIFRA_CONTROL_INFORMACION_COMPLEMENTARIA_POR_CIERRE,
                new Object[] { informacionComplementariaPorCierre.getCveSubsistemaRcv(),
                        informacionComplementariaPorCierre.getCvePresupuestal(),
                        informacionComplementariaPorCierre.getCvePeriodoImss() });
    }

    @Override
    public void actualizarCifraControlInformacionComplementariaDatoPorCierre(
            InformacionComplementaria informacionComplementariaPorCierre) {

        jdbcTemplate.update(SQLConstants.ACTUALIZAR_CIFRA_CONTROL_INFORMACION_COMPLEMENTARIA_DATO_POR_CIERRE,
                new Object[] { informacionComplementariaPorCierre.getCveSubsistemaRcv(),
                        informacionComplementariaPorCierre.getCvePresupuestal(),
                        informacionComplementariaPorCierre.getCvePeriodoImss() });
    }

    @Override
    public InformacionComplementaria buscarInformacionComplementariaPorCierre(String cvePresupuestal, String cvePeriodo,
            Integer cveSubsistema) {

        List<InformacionComplementaria> informacionComplementaria = jdbcTemplate.query(
                SQLConstants.CONSULTAR_INFORMACION_COMPLEMENTARIA_POR_CIERRE,
                new Object[] { cvePresupuestal, cvePeriodo, cveSubsistema },
                new InformacionComplementariaRowMapperHelper());

        if (informacionComplementaria != null && !informacionComplementaria.isEmpty()) {
            return informacionComplementaria.get(0);
        }
        return null;
    }

    @Override
    public Long obtenerTotalSubsistemasCerrados() {

        return jdbcTemplate.query(SQLConstants.CONSULTAR_TOTAL_SUBSISTEMAS_CERRADOS, new Object[] {},
                new ConsultaCountRowMapperHelper()).get(0);

    }

    @Override
    public Long obtenerTotalSubsistemasCerradosPorPresupuestal(String cvePeriodo, String cvePresupuestal) {

        return jdbcTemplate.query(SQLConstants.CONSULTAR_TOTAL_SUBSISTEMAS_CERRADOS_PRESUPUESTAL,
                new Object[] { cvePeriodo, cvePresupuestal }, new ConsultaCountRowMapperHelper()).get(0);

    }

    @Override
    public Long obtenerCountInformacionComplementariaPorSubsistema(int cveSubsistema, String cvePresupuestal,
            String cvePeriodoIMSS) {

        return jdbcTemplate.query(SQLConstants.COUNT_INFORMACION_COMPLEMENTARIA_POR_SUBSISTEMA,
                new Object[] { cveSubsistema, cvePresupuestal, cvePeriodoIMSS }, new ConsultaCountRowMapperHelper())
                .get(0);

    }

    @Override
    public void eliminarInformacionComplementariaDato(int cveSubsistemaRCV, String cvePresupuestal,
            String cvePeriodoIMSS) {

        jdbcTemplate.update(SQLConstants.ELIMINAR_INFORMACION_COMPLEMENTARIA_DATO,
                new Object[] { cveSubsistemaRCV, cvePresupuestal, cvePeriodoIMSS });

    }

    @Override
    public void eliminarInformacionComplementaria(int cveSubsistemaRCV, String cvePresupuestal, String cvePeriodoIMSS) {

        jdbcTemplate.update(SQLConstants.ELIMINAR_INFORMACION_COMPLEMENTARIA,
                new Object[] { cveSubsistemaRCV, cvePresupuestal, cvePeriodoIMSS });

    }

    @Override
    public void generarPeriodoOperacionSiguiente(String cvePresupuestal, int cveTipoCaptura, String cvePeriodoIMSS) {

        jdbcTemplate.update(SQLConstants.GENERAR_PERIODO_OPERACION_SIGUIENTE,
                new Object[] { cvePresupuestal, cveTipoCaptura, cvePeriodoIMSS, 0, 1, 0 });

    }

    @Override
    public PeriodosImss obtenerSiguientePeriodoImss(String cvePeriodo) throws HospitalizacionException {

        return jdbcTemplate.queryForObject(SQLConstants.BUSCAR_SIGUIENTE_PERIODO_IMSS, new Object[] { cvePeriodo },
                new PeriodosImssRowMapperHelper());

    }

    @Override
    public void actualizarPeriodoOperacionSiguienteActual(String cvePresupuestal, int tipoCaptura,
            String cvePeriodoIMSS) {

        jdbcTemplate.update(SQLConstants.ACTUALIZAR_INDICADOR_ACTUAL_PERIODO,
                new Object[] { null, cvePresupuestal, tipoCaptura, cvePeriodoIMSS });
    }

    @Override
    public PeriodoOperacion buscarPeriodoActual(String clavePresupuestal, int tipoCaptura) {

        return jdbcTemplate.queryForObject(SQLConstants.BUSCAR_PERIODO_ABIERTO_NUEVO,
                new Object[] { clavePresupuestal, tipoCaptura}, new PeriodoOperacionImssRowMapperHelper());

    }

    @Override
	public void actualizarIndicadorMonitoreo(String cvePresupuestal, int tipoCaptura, 
			String cvePeriodoIMSS) {

		jdbcTemplate.update(SQLConstants.ACTUALIZAR_INDICADOR_MONITOREO,
                new Object[] { cvePresupuestal, tipoCaptura, cvePeriodoIMSS });
	}

	@Override
	public Long obtenerCountPeriodosMonitoreo(String cvePresupuestal, int tipoCaptura) {
		return jdbcTemplate.query(SQLConstants.COUNT_PERIODOS_MONITOREO,
                new Object[] { cvePresupuestal, tipoCaptura }, new ConsultaCountRowMapperHelper())
        .get(0);
		
	}

	@Override
	public List<SubsistemaRCVDato> obtenerDatoPorSubsistemaRCV(int cveSubsistema, int cveRCV) {
		return jdbcTemplate.query(SQLConstants.OBTENER_DATO_POR_SUBSISTEMA_RCV,
                new Object[] { cveSubsistema, cveRCV }, new SubsistemaRCVDatoRowMapperHelper());
	}
	
}
