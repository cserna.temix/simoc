package mx.gob.imss.simo.enlaces.informacioncomplementaria.rules;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import mx.gob.imss.simo.enlaces.common.constant.CapturarInformacionComplementariaConstants;
import mx.gob.imss.simo.enlaces.common.constant.MensajesErrorEnlacesConstants;
import mx.gob.imss.simo.enlaces.common.model.ComponenteCatalogoInformacionComplementaria;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.constant.BaseConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.CapturarEgresoConstants;
import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesErrorConstants;
import mx.gob.imss.simo.hospitalizacion.common.exception.HospitalizacionException;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.model.EstructuraDatoInformacionComplementaria;
import mx.gob.imss.simo.hospitalizacion.common.rules.HospitalizacionCommonRules;

@Component
public class CapturarInformacionComplementariaRules extends HospitalizacionCommonRules {

    public void validarFormatoPeriodo(String periodo) throws HospitalizacionException {

        if (null == periodo || periodo.isEmpty()
                || periodo.equals(CapturarInformacionComplementariaConstants.PERIODO_VACIO)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_PERIODO));
        }
    }

    public void validarSubsistema(Object subsistema) throws HospitalizacionException {

        if (null == subsistema || subsistema.equals(CapturarInformacionComplementariaConstants.CADENA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_SUBSISTEMA));
        }
    }

    public void validarRCV(String rcv) throws HospitalizacionException {

        if (null == rcv || rcv.isEmpty()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_RCV));
        }
    }

    public void validarPoblacion(Integer poblacion) throws HospitalizacionException {

        if (null == poblacion) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_POBLACION));
        }
    }

    public void validarPor(Integer por) throws HospitalizacionException {

        if (null == por) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_POR));
        }
    }

    public void validarEn(Integer cveEn) throws HospitalizacionException {

        if (null == cveEn) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_EN));
        }
    }

    public void validarHoja(Integer cveHoja) throws HospitalizacionException {

        if (null == cveHoja) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_HOJA));
        }
    }

    public void validarSubclave(Integer cveSubclave) throws HospitalizacionException {

        if (null == cveSubclave) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001B),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_SUBCLAVE));
        }
    }

    public void validarJustificacion(String justificacion) throws HospitalizacionException {

        if (justificacion == null || justificacion.isEmpty()) {
            throw new HospitalizacionException(getArray("enlace.msg.justificacion"));
        }
    }

    public boolean sePuedeConsultarSubsistema(Boolean[] catalogosRequeridosVista, Boolean[] catalogosRequeridos) {

        for (boolean requerido : catalogosRequeridosVista) {
            if (!requerido) {
                return false;
            }
        }
        return true;

    }

    public boolean ultimoCatalogoRequerido(
            List<ComponenteCatalogoInformacionComplementaria> componenteCatalogoInfComplementariaList, String comboId) {

        for (ComponenteCatalogoInformacionComplementaria componenteCatalogoInfComplementaria : componenteCatalogoInfComplementariaList) {
            if (componenteCatalogoInfComplementaria.getComboId().equals(comboId)) {
            	  logger.info("COMPONNETE CARALOGO");
            	  logger.info(componenteCatalogoInfComplementaria.getComboId());;
            	  
                return componenteCatalogoInfComplementaria.isUltimoRequerido();
            }
        }

        return false;
    }

    public void validarDato(Integer valorDato) throws HospitalizacionException {

        if (null == valorDato) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_DATO));
        }
    }

    public void validarCifradeControl(DatosInformacionComplementaria datosInformacionComplementaria)
            throws HospitalizacionException {

        if (null != datosInformacionComplementaria.getCifraDeControl()) {
            Long cifraDeControl = datosInformacionComplementaria.getCifraDeControl();
            Long sumaDatosRCV = new Long(datosInformacionComplementaria.getCveRCV());
            for (EstructuraDatoInformacionComplementaria estructuraDatosInformacionComplementaria : datosInformacionComplementaria
                    .getDatos()) {
                sumaDatosRCV += Integer.parseInt(estructuraDatosInformacionComplementaria.getValor());
            }
            if (!cifraDeControl.equals(sumaDatosRCV)) {
                throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_156),
                        getArray(datosInformacionComplementaria.getCveRCV().toString()));
            }
        } else {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_CIFRA_CONTROL));
        }

    }

    public void validarCifradeControlSimple(DatosInformacionComplementaria datosInformacionComplementaria)
            throws HospitalizacionException {

        if (null == datosInformacionComplementaria.getCifraDeControl()) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(CapturarInformacionComplementariaConstants.REQUERIDO_CIFRA_CONTROL));
        }

    }

    public boolean validarMesSubsistema43() {

        Calendar fechaActual = new GregorianCalendar();
        /* El mes de Calendar empieza en 0 */
        if ((fechaActual.get(Calendar.MONTH) + 1) == CapturarInformacionComplementariaConstants.MES_MARZO) {
            return true;
        }
        return false;
    }

    public boolean validarMesSubsistemaMarzo(String cvePeriodo) {

        int mesPeriodo = Integer.parseInt(cvePeriodo.substring(1, 2));
        if (mesPeriodo == CapturarInformacionComplementariaConstants.MES_MARZO) {
            return true;
        }

        return false;
    }

    public List<DatosCatalogo> ordenarCifra99(List<DatosCatalogo> catalogoRCV) {

        DatosCatalogo datoEliminado = new DatosCatalogo();
        for (Iterator<DatosCatalogo> datosCatalogoIterator = catalogoRCV.iterator(); datosCatalogoIterator.hasNext();) {
            DatosCatalogo datosCatalogo = datosCatalogoIterator.next();
            if (datosCatalogo.getClave().equals(CapturarInformacionComplementariaConstants.CVE_CIFRA_CONTROL_99)) {
                datoEliminado = datosCatalogo;
                datosCatalogoIterator.remove();
            }
        }
        if (null != datoEliminado.getClave()) {
            catalogoRCV.add(datoEliminado);
        }

        return catalogoRCV;
    }

    public void validarCapturaCifra99(int totalRCVCapturados, int totalRCVSubsistema) throws HospitalizacionException {

        if (totalRCVSubsistema != totalRCVCapturados) {
            /* se comenta para pruebas */
            throw new HospitalizacionException(MensajesErrorEnlacesConstants.ME_182);
        }

    }

    public void validarCifraControl99(List<InformacionComplementaria> informacionComplementariaList,
            Long cifraControl99, int cveSubsistema) throws HospitalizacionException {

        int totalCifraControl = 0;
        for (InformacionComplementaria informacionComplementaria : informacionComplementariaList) {
            totalCifraControl += informacionComplementaria.getNumCifraControl();
        }
        if (totalCifraControl != cifraControl99) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_155),
                    getArray(Integer.toString(cveSubsistema)));
        }
    }

    public boolean sePuedeCerrarPeriodo(int totalCantidadSubsistemas, int totalCantidadSubsistemasCerrados) {
    
        if (totalCantidadSubsistemas == totalCantidadSubsistemasCerrados) {
            return false;
        }
        return true;
    }

    @Override
    public void validarFormatoFecha(String fecha) throws HospitalizacionException {

        if (fecha == null || fecha.isEmpty() || fecha.equals(BaseConstants.FECHA_VACIA)) {
            throw new HospitalizacionException(getArray(MensajesErrorConstants.ME_001A),
                    getArray(BaseConstants.REQUERIDO_FECHA));
        }
        try {
            Integer dia = Integer.valueOf(fecha.substring(0, 2));
            Integer mes = Integer.valueOf(fecha.substring(3, 5));
            Integer anio = Integer.valueOf(fecha.substring(6));
            if (BaseConstants.INDEX_CERO == dia || BaseConstants.INDEX_CERO == mes
                    || BaseConstants.INDEX_CERO == anio) {
                throw new HospitalizacionException(MensajesErrorConstants.ME_109, MensajesErrorConstants.ME_146);
            } else {
                if (dia > 31 || mes > 12) {
                    throw new HospitalizacionException(MensajesErrorConstants.ME_109, MensajesErrorConstants.ME_146);
                }
                validarMesFebrero(dia, mes, anio);
                validarMes30dias(dia, mes);
            }
        } catch (NumberFormatException e) {
            throw new HospitalizacionException(MensajesErrorConstants.ME_109, MensajesErrorConstants.ME_146);
        }
    }

    private void validarMesFebrero(Integer dia, Integer mes, Integer anio) throws HospitalizacionException {

        if (mes == 2) {
            if (dia > 29) {
                throw new HospitalizacionException(MensajesErrorConstants.ME_136, MensajesErrorConstants.ME_146);
            }
            GregorianCalendar calendar = new GregorianCalendar();
            if (dia == 29 && !calendar.isLeapYear(anio)) {
                throw new HospitalizacionException(MensajesErrorConstants.ME_136, MensajesErrorConstants.ME_146);
            }
        }
    }

    private void validarMes30dias(Integer dia, Integer mes) throws HospitalizacionException {

        switch (mes) {
            case 4:
            case 6:
            case 9:
            case 11:
                if (CapturarEgresoConstants.TREINTADIAS < dia) {
                    throw new HospitalizacionException(MensajesErrorConstants.ME_136, MensajesErrorConstants.ME_146);
                }
                break;
            default:
                break;
        }
    }

    public boolean datosIgualaCero(List<EstructuraDatoInformacionComplementaria> datos) {
    	
    	return datos.stream().allMatch(d -> "0".equals(d.getValor()));
	}
}
