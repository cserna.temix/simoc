package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

public class SubsistemaRowMapperHelper extends BaseHelper implements RowMapper<Subsistema> {

    @Override
    public Subsistema mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        Subsistema subsistema = new Subsistema();
        subsistema.setClave(resultSet.getInt(SQLColumnasConstants.CVE_SUBSISTEMA));
        subsistema.setDescripcion(resultSet.getString(SQLColumnasConstants.DES_SUBSISTEMA));
        subsistema.setIndPoblacion(resultSet.getInt(SQLColumnasConstants.IND_POBLACION));
        subsistema.setIndPor(resultSet.getInt(SQLColumnasConstants.IND_POR));
        subsistema.setIndEn(resultSet.getInt(SQLColumnasConstants.IND_EN));
        subsistema.setIndHoja(resultSet.getInt(SQLColumnasConstants.IND_HOJA));
        subsistema.setIndSubclave(resultSet.getInt(SQLColumnasConstants.IND_SUBCLAVE));
        return subsistema;

    }

}
