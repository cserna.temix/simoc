package mx.gob.imss.simo.enlaces.informacioncomplementaria.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.view.ViewScoped;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.confirmdialog.ConfirmDialog;
import org.primefaces.component.fieldset.Fieldset;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import lombok.Data;
import mx.gob.imss.simo.enlaces.common.constant.CapturarInformacionComplementariaConstants;
import mx.gob.imss.simo.enlaces.common.controller.EnlacesCommonController;
import mx.gob.imss.simo.enlaces.common.model.InformacionComplementariaConsulta;
import mx.gob.imss.simo.enlaces.common.model.Subsistema;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCV;
import mx.gob.imss.simo.enlaces.informacioncomplementaria.services.CapturarInformacionComplementariaServices;
import mx.gob.imss.simo.hospitalizacion.common.constant.TipoCapturaEnum;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.services.CatalogosHospitalizacionServices;

@Data
@ViewScoped
@ManagedBean(name = "informacionComplementariaCommonController")
public class CapturarInformacionComplementariaCommonController extends EnlacesCommonController {

    private static final long serialVersionUID = -60286679411596683L;

    @ManagedProperty("#{capturarInformacionComplementariaServices}")
    private CapturarInformacionComplementariaServices capturarInformacionComplementariaServices;
    @ManagedProperty("#{catalogosHospitalizacionServices}")
    protected CatalogosHospitalizacionServices catalogosHospitalizacionServices;

    private InputMask periodoIM;
    private AutoComplete autoCompleteSubsistema;
    private AutoComplete autoCompleteRCV;
    private SelectOneMenu selectPoblacion;
    private SelectOneMenu selectPor;
    private SelectOneMenu selectEn;
    private SelectOneMenu selectHoja;
    private SelectOneMenu selectSubclave;
    private InputText textJustificacion;
    private Fieldset fieldSubsistemas;
    private Fieldset fieldCierrePeriodo;
    private InputText textCifraDeControl;

    private List<DatosCatalogo> catalogoSubsistema;
    private List<DatosCatalogo> catalogoRCV;
    private List<DatosCatalogo> catalogoPoblacion;
    private List<DatosCatalogo> catalogoPor;
    private List<DatosCatalogo> catalogoEn;
    private List<DatosCatalogo> catalogoHoja;
    private List<DatosCatalogo> catalogoSubclave;

    private ConfirmDialog justificacionConfirm;
    private String banderaJustificacionConfirm;
    private CommandButton confirmSi;
    private CommandButton confirmNo;

    private CommandButton buttonJustificar;
    private CommandButton buttonConsultar;
    private CommandButton buttonRegresar;

    protected Boolean[] requeridos = { Boolean.FALSE, Boolean.FALSE };

    private boolean visibleJustificar;
    private boolean visibleComplementarios;
    private boolean visibleConsultar;
    private boolean visibleBotones;
    private boolean visibleCifraDeControl; /* Tambien se usara para Cifra de control 99 */
    private boolean visibleSubsistemas;

    private Subsistema subsistema;
    private SubsistemaRCV subsistemaRCV;
    private int longitudCifraControl;

    /* Poblacion, Por, En, Hoja, Subclave */
    protected Boolean[] posiciones = { false, false, false, false, false };
    /* Se define el orden de los componentes de los catalogos en la vista */
    String[] componentes = { "", "", "", "", "", "" };

    private int cantidadDatos;

    private boolean guardarJustificacion;

    private List<InformacionComplementariaConsulta> informacionExistenteConsultaList;

    private List<InformacionComplementariaConsulta> encabezado;

    private String componenteCifraControl99;
    private String llaveMapaSubsistema;
    private String validarDato;
    private String focoDato;
    protected String funcionOnCompleteFocoDato;

    private boolean cierrePeriodo;
    private boolean estadoPeriodo;

    private List<String> listaSubsistemaAutoComplete;
    private List<String> listaRCVAutoComplete;

    private boolean existeInformacionComplementaria;

    @Override
    @PostConstruct
    public void init() {

        super.init();
        iniciar();

    }

    private void iniciar() {

        logger.info("Iniciar Informacion Complementaria");

        setDatosHospitalizacion(new DatosHospitalizacion());

        inicializarComponentes();
        
        cargarCatalogos();
        limpiaCamposRequeridos();

        setGuardarJustificacion(Boolean.FALSE);
        setExisteInformacionComplementaria(Boolean.FALSE);
        
        cargarPeriodoOperacion();
    }

    private void inicializarComponentes() {

        setPeriodoIM(new InputMask());
        setAutoCompleteSubsistema(new AutoComplete());
        setSelectSubclave(new SelectOneMenu());
        setAutoCompleteRCV(new AutoComplete());
        setSelectPoblacion(new SelectOneMenu());
        setSelectPor(new SelectOneMenu());
        setSelectEn(new SelectOneMenu());
        setSelectHoja(new SelectOneMenu());
        setSelectSubclave(new SelectOneMenu());
        setTextJustificacion(new InputText());
        setFieldSubsistemas(new Fieldset());
        setTextCifraDeControl(new InputText());

        getAutoCompleteRCV().setDisabled(Boolean.TRUE);
        getSelectPoblacion().setDisabled(Boolean.TRUE);
        getSelectPor().setDisabled(Boolean.TRUE);
        getSelectEn().setDisabled(Boolean.TRUE);
        getSelectHoja().setDisabled(Boolean.TRUE);
        getSelectSubclave().setDisabled(Boolean.TRUE);

        justificacionConfirm = new ConfirmDialog();

        buttonJustificar = new CommandButton();
        buttonConsultar = new CommandButton();
        buttonRegresar = new CommandButton();

        setButtonGuardar(new CommandButton());
        setButtonCancelar(new CommandButton());

        getButtonJustificar().setDisabled(Boolean.TRUE);
        getButtonConsultar().setDisabled(Boolean.TRUE);
        getButtonGuardar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);

        setVisibleComplementarios(Boolean.FALSE);
        setVisibleJustificar(Boolean.FALSE);
        setVisibleConsultar(Boolean.FALSE);
        setVisibleBotones(Boolean.TRUE);
        setVisibleCifraDeControl(Boolean.FALSE);
        
    }

    private void cargarCatalogos() {

        setCatalogoRCV(new ArrayList<DatosCatalogo>());
        // setCatalogoSubsistema(capturarInformacionComplementariaServices.obtenerCatalogoSubsistema(getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo()));
        setCatalogoPoblacion(new ArrayList<DatosCatalogo>());
        setCatalogoPor(new ArrayList<DatosCatalogo>());
        setCatalogoEn(new ArrayList<DatosCatalogo>());
        setCatalogoHoja(new ArrayList<DatosCatalogo>());
        setCatalogoSubclave(new ArrayList<DatosCatalogo>());
        // setListaSubsistemaAutoComplete(convertirCatalogoString(getCatalogoSubsistema()));

    }

    public void cargarCatalogoSubsistema() {

        setCatalogoSubsistema(capturarInformacionComplementariaServices
                .obtenerCatalogoSubsistema(getDatosHospitalizacion().getDatosInfoComplementaria().getCvePeriodo()));
        setListaSubsistemaAutoComplete(convertirCatalogoString(getCatalogoSubsistema()));

    }

    private void limpiaCamposRequeridos() {

        requeridos[0] = Boolean.FALSE;
        requeridos[1] = Boolean.FALSE;
    }

    public void cancelar() {

        reiniciarCampos();
        desHabilitarCampos();
        desHabilitarBotones();
        iniciar();

    }

    public void cancelarCierre() {

        reiniciarCampos();
        desHabilitarCampos();
        desHabilitarBotones();
        // setTieneFoco(CapturarInformacionComplementariaConstants.FOCO_PERIODO);
        setGuardarJustificacion(Boolean.FALSE);
        setExisteInformacionComplementaria(Boolean.FALSE);
    }

    public void regresar() {

        cancelar();

    }

    
    private void reiniciarCampos() {

        getPeriodoIM().resetValue();
        getAutoCompleteSubsistema().resetValue();
        getSelectSubclave().resetValue();
        getAutoCompleteRCV().resetValue();
        getSelectPoblacion().resetValue();
        getSelectPor().resetValue();
        getSelectEn().resetValue();
        getSelectHoja().resetValue();
        getSelectSubclave().resetValue();
        setSubsistema(null);
        
    }

    private void desHabilitarCampos() {

        getAutoCompleteRCV().setDisabled(Boolean.TRUE);
        getSelectPoblacion().setDisabled(Boolean.TRUE);
        getSelectPor().setDisabled(Boolean.TRUE);
        getSelectEn().setDisabled(Boolean.TRUE);
        getSelectHoja().setDisabled(Boolean.TRUE);
        getSelectSubclave().setDisabled(Boolean.TRUE);
    }

    private void desHabilitarBotones() {

        getButtonJustificar().setDisabled(Boolean.TRUE);
        getButtonConsultar().setDisabled(Boolean.TRUE);
        getButtonCancelar().setDisabled(Boolean.TRUE);
        getButtonGuardar().setDisabled(Boolean.TRUE);
    }

    public void habilitarComponentes(Subsistema subsistema) {

        if (capturarInformacionComplementariaServices.sePuedeActivarComponentes(getRequeridos())) {

            habilitarBotones();
            habilitarCampos(subsistema);

        } else {
            desHabilitarBotones();
            desHabilitarCampos();
            limpiarCampos();
        }

        if (periodoIM.getValue() == null || periodoIM.getValue().toString().isEmpty()) {
            getButtonCancelar().setDisabled(Boolean.TRUE);
        } else {
            getButtonCancelar().setDisabled(Boolean.FALSE);
        }

    }

    private void habilitarBotones() {

        getButtonJustificar().setDisabled(Boolean.FALSE);
        getButtonConsultar().setDisabled(Boolean.FALSE);

    }

    private void habilitarCampos(Subsistema subsistema) {

        getAutoCompleteRCV().setDisabled(Boolean.FALSE);
        if (subsistema.getIndPoblacion() == 1) {
            getSelectPoblacion().setDisabled(Boolean.FALSE);
        } else {
            getSelectPoblacion().setDisabled(Boolean.TRUE);
        }
        if (subsistema.getIndPor() == 1) {
            getSelectPor().setDisabled(Boolean.FALSE);
        } else {
            getSelectPor().setDisabled(Boolean.TRUE);
        }
        if (subsistema.getIndEn() == 1) {
            getSelectEn().setDisabled(Boolean.FALSE);
        } else {
            getSelectEn().setDisabled(Boolean.TRUE);
        }
        if (subsistema.getIndHoja() == 1) {
            getSelectHoja().setDisabled(Boolean.FALSE);
        } else {
            getSelectHoja().setDisabled(Boolean.TRUE);
        }
        if (subsistema.getIndSubclave() == 1) {
            getSelectSubclave().setDisabled(Boolean.FALSE);
        } else {
            getSelectSubclave().setDisabled(Boolean.TRUE);
        }

    }

    private void limpiarCampos() {

        getDatosHospitalizacion().getDatosInfoComplementaria().setCveRCV(null);
        reiniciarCatalogosComplementarios();
    }

    protected void ocultarCombosComplementarios() {

        reiniciarCatalogosComplementarios();
        getSelectPoblacion().resetValue();
        getSelectPoblacion().setDisabled(Boolean.TRUE);
        getSelectPor().resetValue();
        getSelectPor().setDisabled(Boolean.TRUE);
        getSelectEn().resetValue();
        getSelectEn().setDisabled(Boolean.TRUE);
        getSelectHoja().resetValue();
        getSelectHoja().setDisabled(Boolean.TRUE);
        getSelectSubclave().resetValue();
        getSelectSubclave().setDisabled(Boolean.TRUE);
    }

    protected void mostrarCombosComplementarios() {

        reiniciarCatalogosComplementarios();
        getSelectPoblacion().resetValue();
        getSelectPoblacion().setDisabled(Boolean.FALSE);
        getSelectPor().resetValue();
        getSelectPor().setDisabled(Boolean.FALSE);
        getSelectEn().resetValue();
        getSelectEn().setDisabled(Boolean.FALSE);
        getSelectHoja().resetValue();
        getSelectHoja().setDisabled(Boolean.FALSE);
        getSelectSubclave().resetValue();
        getSelectSubclave().setDisabled(Boolean.FALSE);
    }

    protected void reiniciarCatalogosComplementarios() {

        getDatosHospitalizacion().getDatosInfoComplementaria().setCvePoblacion(null);
        getDatosHospitalizacion().getDatosInfoComplementaria().setCvePor(null);
        getDatosHospitalizacion().getDatosInfoComplementaria().setCveEn(null);
        getDatosHospitalizacion().getDatosInfoComplementaria().setCveHoja(null);
        getDatosHospitalizacion().getDatosInfoComplementaria().setCveSubclave(null);
    }

    private void cargarPeriodoOperacion(){
    	
    	String periodo = capturarInformacionComplementariaServices.obtenerPeriodoActual(
    			obtenerDatosUsuario().getCvePresupuestal(), 
    			TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave());
		getDatosHospitalizacion().getDatosInfoComplementaria().setCvePeriodo(periodo);
        setTieneFoco(CapturarInformacionComplementariaConstants.FOCO_SUBSISTEMA);
        cargarCatalogoSubsistema();
        requeridos[0] = Boolean.TRUE;
        habilitarComponentes(getSubsistema());
        getPeriodoIM().setDisabled(Boolean.TRUE);
    	/*
        boolean tienePeriodosMonitoreo = capturarInformacionComplementariaServices.tienePeriodosMonitoreo(
        		obtenerDatosUsuario().getCvePresupuestal(), 
        		TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave());
        
		if(tienePeriodosMonitoreo){
			setTieneFoco(CapturarInformacionComplementariaConstants.FOCO_PERIODO);
		} else {
			String periodo = capturarInformacionComplementariaServices.obtenerPeriodoActual(
	    			obtenerDatosUsuario().getCvePresupuestal(), 
	    			TipoCapturaEnum.INFORMACION_COMPLEMENTARIA.getClave());
			getDatosHospitalizacion().getDatosInfoComplementaria().setCvePeriodo(periodo);
	        setTieneFoco(CapturarInformacionComplementariaConstants.FOCO_SUBSISTEMA);
	        cargarCatalogoSubsistema();
            requeridos[0] = Boolean.TRUE;
            habilitarComponentes(getSubsistema());
	        getPeriodoIM().setDisabled(Boolean.TRUE);
		}* 
    	 */
    }
}
