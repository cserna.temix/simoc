package mx.gob.imss.simo.enlaces.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class InformacionComplementaria {

    private int cveSubsistemaRcv;
    private String cvePresupuestal;
    private String cvePeriodoImss;
    private Long numCifraControl;
    private Date fecCreacion;
    private String cveCapturista;
    private Date fecActualizacion;
    private String cveCapturistaActualiza;
    private int refCveSubsistema;
    private int refCveRCV;
}
