package mx.gob.imss.simo.enlaces.common.repository;

import java.util.List;

import mx.gob.imss.simo.hospitalizacion.common.model.DatosCatalogo;
import mx.gob.imss.simo.hospitalizacion.common.repository.CatalogosHospitalizacionRepository;

public interface ServiciosSubrogarCommonRepository extends CatalogosHospitalizacionRepository {

    /**
     * Informacion Complementaria
     */

    /**
     * Servicios Subrogados
     */

    List<DatosCatalogo> obtenerCatalogoMotivoSubrogacion();

    List<DatosCatalogo> obtenerCatalogoTipoContrato();

    List<DatosCatalogo> obtenerCatalogoTipoServicio();
}
