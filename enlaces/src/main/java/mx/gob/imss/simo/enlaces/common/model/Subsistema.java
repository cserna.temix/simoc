package mx.gob.imss.simo.enlaces.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class Subsistema {

    private int clave;
    private String descripcion;
    private int indPoblacion;
    private int indPor;
    private int indEn;
    private int indHoja;
    private int indSubclave;
    private Date fechaBaja;

}
