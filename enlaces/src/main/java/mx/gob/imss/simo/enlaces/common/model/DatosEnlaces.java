package mx.gob.imss.simo.enlaces.common.model;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosMedico;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosPaciente;

@Data
public class DatosEnlaces {

    private DatosInformacionComplementaria datosInfoComplementaria;
    private DatosSubrogados datosSubrogados;
    private DatosPaciente datosPaciente;
    private DatosMedico datosMedico;

    public DatosEnlaces() {
        datosInfoComplementaria = new DatosInformacionComplementaria();
        datosSubrogados = new DatosSubrogados();
        datosMedico = new DatosMedico();
        datosPaciente = new DatosPaciente();
    }

}
