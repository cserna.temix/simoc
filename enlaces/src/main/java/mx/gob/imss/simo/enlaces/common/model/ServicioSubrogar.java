package mx.gob.imss.simo.enlaces.common.model;

import lombok.Data;

@Data
public class ServicioSubrogar {

    private int cveServicioSubrogar;
    private String desServicioSubrogar;
    private String desGrupoSubrogar;
}
