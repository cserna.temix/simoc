package mx.gob.imss.simo.enlaces.common.model;

import java.util.Date;

import lombok.Data;

@Data
public class SubsistemaRCV {

    private int clave;
    private int cveSubsistema;
    private int cveRCV;
    private Integer cvePoblacion;
    private Integer cvePor;
    private Integer cveEn;
    private Integer cveHoja;
    private Integer cveSubclave;
    private int indCaptura;
    private Date fechaBaja;
    private int totalRCV;
    private String desSubsistemaRCV;
}
