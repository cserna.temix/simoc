package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.enlaces.common.model.SubsistemaRCVDato;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

public class SubsistemaRCVDatoRowMapperHelper extends BaseHelper
        implements RowMapper<SubsistemaRCVDato> {

    @Override
    public SubsistemaRCVDato mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        SubsistemaRCVDato informacionComplementariaDato = new SubsistemaRCVDato();
        informacionComplementariaDato.setClave(resultSet.getInt(SQLColumnasConstants.CVE_SUBSISTEMA_RCV));
        informacionComplementariaDato.setCveDato(resultSet.getInt(SQLColumnasConstants.CVE_DATO));
        informacionComplementariaDato.setRefConcepto(resultSet.getString(SQLColumnasConstants.REF_CONCEPTO));
        informacionComplementariaDato.setRefModulo(resultSet.getString(SQLColumnasConstants.REF_MODULO));
        informacionComplementariaDato.setFecAlta(resultSet.getDate(SQLColumnasConstants.FEC_ALTA));
        informacionComplementariaDato.setFecBaja(validarFechaNula(resultSet, SQLColumnasConstants.FEC_BAJA));
        return informacionComplementariaDato;
    }

}
