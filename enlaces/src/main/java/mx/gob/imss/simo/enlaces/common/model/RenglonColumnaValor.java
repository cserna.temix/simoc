package mx.gob.imss.simo.enlaces.common.model;

import lombok.Data;

@Data
public class RenglonColumnaValor {

    private Integer rcv;
    private Integer vector1;
    private Integer vector2;
    private Integer vector3;
    private Integer vector4;
    private Integer vector5;
    private Integer vector6;
    private Integer vector7;
    private Integer vector8;
    private Integer vector9;
    private Integer cifraControl;

}
