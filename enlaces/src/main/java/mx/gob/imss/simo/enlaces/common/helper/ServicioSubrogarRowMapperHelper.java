package mx.gob.imss.simo.enlaces.common.helper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogar;
import mx.gob.imss.simo.hospitalizacion.common.constant.SQLColumnasConstants;
import mx.gob.imss.simo.hospitalizacion.common.helper.BaseHelper;

public class ServicioSubrogarRowMapperHelper extends BaseHelper implements RowMapper<ServicioSubrogar> {

    @Override
    public ServicioSubrogar mapRow(ResultSet resultSet, int rowId) throws SQLException {

        ServicioSubrogar servicioSubrogar = new ServicioSubrogar();
        servicioSubrogar.setCveServicioSubrogar(resultSet.getInt(SQLColumnasConstants.CVE_SERVICIO_SUBROGAR));
        servicioSubrogar.setDesServicioSubrogar(resultSet.getString(SQLColumnasConstants.DES_SERVICIO_SUBROGAR));
        servicioSubrogar.setDesGrupoSubrogar(resultSet.getString(SQLColumnasConstants.DES_GRUPO_SUBROGAR));
        return servicioSubrogar;
    }

}
