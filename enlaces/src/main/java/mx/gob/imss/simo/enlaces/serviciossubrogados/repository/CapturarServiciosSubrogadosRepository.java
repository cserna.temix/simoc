package mx.gob.imss.simo.enlaces.serviciossubrogados.repository;

import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogado;
import mx.gob.imss.simo.enlaces.common.model.ServicioSubrogar;
import mx.gob.imss.simo.hospitalizacion.common.model.DatosHospitalizacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodoOperacion;
import mx.gob.imss.simo.hospitalizacion.common.model.PeriodosImss;
import mx.gob.imss.simo.hospitalizacion.common.repository.HospitalizacionCommonRepository;
import mx.gob.imss.simo.model.DatosUsuario;

public interface CapturarServiciosSubrogadosRepository extends HospitalizacionCommonRepository {

    ServicioSubrogar buscarServicioSubrogar(String cveServiciosSubrogados);

    void guardarServicioSubrogado(ServicioSubrogado servicioSubrogado, DatosHospitalizacion datosHospitalizacion,
            DatosUsuario datosUsuario);

    PeriodoOperacion obtenerPeriodoOperacion(String clavePresupuestal, Integer clave, String fechaActual);

    PeriodosImss obtenerSiguientePeriodo(String cvePeriodoImss);

    PeriodoOperacion buscarPeriodoOperacion(String cvePresupuestal, Integer claveTipoCaptura, String clavePeriodoIMSS);

    void abrirPeriodoCerrado(String cvePresupuestal, Integer claveTipoCaptura, String clavePeriodoIMSS);

}
