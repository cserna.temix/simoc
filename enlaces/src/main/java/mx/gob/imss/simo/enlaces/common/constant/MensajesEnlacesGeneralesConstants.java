package mx.gob.imss.simo.enlaces.common.constant;

import mx.gob.imss.simo.hospitalizacion.common.constant.MensajesGeneralesConstants;

public class MensajesEnlacesGeneralesConstants extends MensajesGeneralesConstants {

    public static final String MG_028 = "mg028";

}
