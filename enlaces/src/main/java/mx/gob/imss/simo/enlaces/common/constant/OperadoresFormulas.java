package mx.gob.imss.simo.enlaces.common.constant;


public enum OperadoresFormulas {
    
    BETWEEN("BETWEEN", "BETWEEN"), AND("AND", "AND"), OR("OR", "OR"), MAYOR_IGUAL(">=", ">="), MENOR_IGUAL("<=",
            "<="), MAYOR(">", ">"), MENOR("<", "<"), IGUAL("=", "=");

    private String clave;
    private String descripcion;

    private OperadoresFormulas(String clave, String descripcion) {
        this.clave = clave;
        this.descripcion = descripcion;
    }

    public String getClave() {

        return clave;
    }

    public String getDescripcion() {

        return descripcion;
    }

    public static OperadoresFormulas find(String clave) {

        OperadoresFormulas right = null;
        for (OperadoresFormulas item : values()) {
            if (item.getClave().equals(clave)) {
                right = item;
                break;
            }
        }
        return right;
    }

}
