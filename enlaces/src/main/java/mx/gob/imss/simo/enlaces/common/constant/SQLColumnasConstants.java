package mx.gob.imss.simo.enlaces.common.constant;

public class SQLColumnasConstants {

    private SQLColumnasConstants() {
        super();
    }

    /**
     * SIC_SUBSISTEMA
     */
    public static final String CVE_SUBSISTEMA = "CVE_SUBSISTEMA";
    public static final String DES_SUBSISTEMA = "DES_SUBSISTEMA";
    public static final String IND_POBLACION = "IND_POBLACION";
    public static final String IND_POR = "IND_POR";
    public static final String IND_EN = "IND_EN";
    public static final String IND_HOJA = "IND_HOJA";
    public static final String IND_SUBCLAVE = "IND_SUBCLAVE";
    public static final String FEC_BAJA = "FEC_BAJA";

    /**
     * SIC_RCV
     */

    public static final String CVE_RCV = "CVE_RCV";
    public static final String DES_RCV = "DES_RCV";

    /**
     * SIC_POBLACION
     */

    public static final String CVE_POBLACION = "CVE_POBLACION";
    public static final String DES_POBLACION = "DES_POBLACION";

    /**
     * SIC_POR
     */

    public static final String CVE_POR = "CVE_POR";
    public static final String DES_POR = "DES_POR";

    /**
     * SIC_EN
     */

    public static final String CVE_EN = "CVE_EN";
    public static final String DES_EN = "DES_EN";

    /**
     * SIC_HOJA
     */

    public static final String CVE_HOJA = "CVE_HOJA";
    public static final String DES_HOJA = "DES_HOJA";

    /**
     * SIC_SUBCLAVE
     */

    public static final String CVE_SUBCLAVE = "CVE_SUBCLAVE";
    public static final String DES_SUBCLAVE = "DES_SUBCLAVE";

    /**
     * SIT_CIERRE_SUBSISTEMA
     */
    public static final String CVE_PRESUPUESTAL = "CVE_PRESUPUESTAL";
    public static final String CVE_PERIODO_IMSS = "CVE_PERIODO_IMSS";
    public static final String IND_CERRADO = "IND_CERRADO";
    public static final String FEC_CIERRE = "FEC_CIERRE";
    public static final String REF_JUSTIFICACION = "REF_JUSTIFICACION";
    public static final String CVE_CAPTURISTA = "CVE_CAPTURISTA";
    public static final String FEC_ACTUALIZACION = "FEC_ACTUALIZACION";
    public static final String CVE_CAPTURISTA_ACTUALIZA = "CVE_CAPTURISTA_ACTUALIZA";

    /**
     * SIC_SUBSISTEMA_RCV
     */

    public static final String CVE_SUBSISTEMA_RCV = "CVE_SUBSISTEMA_RCV";
    public static final String IND_CAPTURA = "IND_CAPTURA";
    public static final String NUM_TOTAL_RCV = "NUM_TOTAL_RCV";
    public static final String DES_SUBSISTEMA_RCV = "DES_SUBSISTEMA_RCV";
    
    /**
     * SIC_INF_COMPLEMENTARIA
     */

    public static final String CVE_DATO = "CVE_DATO";
    public static final String REF_CONCEPTO = "REF_CONCEPTO";
    public static final String REF_MODULO = "REF_MODULO";
    public static final String FEC_ALTA = "FEC_ALTA";
    public static final String NUM_VALOR_DATO = "NUM_VALOR_DATO";

    public static final String NUM_CIFRA_CONTROL = "NUM_CIFRA_CONTROL";
    public static final String FEC_CREACION = "FEC_CREACION";
    public static final String REF_CVE_SUBSISTEMA = "REF_CVE_SUBSISTEMA";
    public static final String REF_CVE_RCV = "REF_CVE_RCV";

    /*
     * SIC_DATO_FORMULA
     */

    public static final String NUM_ITERACION_DATO = "NUM_ITERACION_DATO";
    public static final String REF_OPERACION = "REF_OPERACION";
    public static final String DES_MENSAJE_ERROR = "DES_MENSAJE_ERROR";
    public static final String NUM_MES_INICIO = "NUM_MES_INICIO";
    public static final String NUM_ANIO_INICIO = "NUM_ANIO_INICIO";
    public static final String NUM_MES_FIN = "NUM_MES_FIN";
    public static final String NUM_ANIO_FIN = "NUM_ANIO_FIN";

}
