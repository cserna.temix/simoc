package mx.gob.imss.simo.enlaces.common.controller;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import lombok.Data;
import mx.gob.imss.simo.hospitalizacion.common.controller.HospitalizacionCommonController;

@Data
@ViewScoped
@ManagedBean(name = "EnlacesCommonController")
public class EnlacesCommonController extends HospitalizacionCommonController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3629600512465681262L;

	private ResourceBundle bundleEnlaces;
	private static final String BUNDLES_ENLACES = "bundle.messages_enlaces";

	@Override
	public void init() {
		super.init();
		bundleEnlaces = ResourceBundle.getBundle(BUNDLES_ENLACES, getLocale());
	}

	@Override
	public String obtenerMensaje(String clave) {

		String mensaje;
		try {
			mensaje = super.obtenerMensaje(clave);
		} catch (Exception e) {
			mensaje = bundleEnlaces.getString(clave);
		}
		return mensaje;
	}

	@Override
	public String obtenerNombrePagina() {
		return null;
	}

	@Override
	public void salir() throws IOException {
	}
}
